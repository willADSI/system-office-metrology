<%-- 
    Document   : ExecReportes
    Created on : 12/01/2017, 09:33:50 AM
    Author     : Juan.Rodriguez
--%>

<%@page import="java.nio.file.Files"%>
<%@page import="DB.Globales"%>
<%@ page contentType="application/pdf" language="java" %>
<%@page import="java.io.*" %>
<%@page import="java.util.*" %>
<%@page import="net.sf.jasperreports.engine.*" %>
<%@page import="net.sf.jasperreports.view.JasperViewer" %>
<%@page trimDirectiveWhitespaces="true" %>

<%@page import="javax.servlet.ServletResponse"%>
<jsp:useBean id="Connec" class="DB.Globales" scope="application" />
<jsp:setProperty name="Connec" property="*" />
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>

        <%

            HttpSession misession = request.getSession();
            if (misession.getAttribute("codusu") == null) {
                response.setContentType("text/html; charset=UTF-8");
                out.println("<center><h3>SESION FINALIZADA</h3></center>");
            } else {
                
                
                
                String nbReporte = "", nbArchivo = "", Barra = System.getProperty("file.separator"), urlArchivo = request.getRealPath(Barra).replace(Barra + "build", ""), txEncriptado = "";

                if (request.getQueryString().contains("nRepEjecutar")) {
                    nbReporte = request.getParameter("nRepEjecutar");
                    if (nbReporte.toLowerCase().contains(".jasper")) {
                        if (nbReporte.toLowerCase().endsWith(".jasper")) {
                            nbArchivo = nbReporte;
                            nbReporte = nbReporte.substring(0, nbReporte.indexOf(".jasper"));
                        } else {
                            nbReporte = nbReporte.replaceAll(".jasper", "");
                            nbArchivo = nbReporte + ".jasper";
                        }
                    } else {
                        nbArchivo = nbReporte + ".jasper";
                    }
                }
                
                String documento = request.getParameter("DOCUMENTO");
                String ruta_gestion = Connec.ValidarRutaGestion(nbArchivo.split("\\.")[0],documento);
                if (ruta_gestion == ""){
                    response.setContentType("text/html; charset=UTF-8");
                    out.println("<center><h3>DEBE DE CONFIGURAR LA RUTA DE GESTI�N PARA EL DOCUMENTO " + nbArchivo.split("\\.")[0] + " EN LA BASE DE DATOS</h3></center>");
                    return; 
                }

                String version = request.getParameter("version");
                String gestion = request.getParameter("gestion");

                if (request.getQueryString().contains("&")) {
                    txEncriptado = request.getQueryString().toString();
                }
                System.out.println("QUERY: " + txEncriptado);
                if (new File(urlArchivo + "Reportes").exists() && new File(urlArchivo + "Reportes").isDirectory()) {//si la carpeta de los reportes existe
                    urlArchivo = urlArchivo + "Reportes" + Barra;
                    if (new File(urlArchivo + nbReporte).exists() && new File(urlArchivo + nbReporte + Barra + version).isDirectory() && new File(urlArchivo + nbReporte + Barra + version + Barra + nbArchivo).exists()) {//si la carpeta existe y si contiene los archivos
                        System.out.println("El archivo si existe...");
                        urlArchivo = urlArchivo + nbReporte + Barra + version + Barra;
                        System.out.println("El archivo: " + urlArchivo);
                        Map<String, Object> pReporte = new HashMap<String, Object>();//instancio el MAPA  de parametros que se enviaran en caso de que los contenga
                        if (txEncriptado.contains("&")) {
                            String[] VparamReporte = txEncriptado.split("&");//Separo los parametros en toda la cadena recibida
                            if (VparamReporte.length > 0) {
                                for (int i = 0; i < VparamReporte.length; i++) {//recorro la variable DATA y veo que tenga parametros
                                    if (VparamReporte[i].toString().split("=").length < 2) {
                                        break;
                                    } else {
                                        String nbCampo = VparamReporte[i].toString().split("=")[0];//nombre de la variable a enviar
                                        String valCampo = VparamReporte[i].toString().split("=")[1]; //valor de la variable a enviar 
                                        System.out.println(nbCampo + " -- " + valCampo);
                                        if (nbCampo.toLowerCase().equals("nrepejecutar")) {
                                            //si el nombre del parametro es igual a nombre no lo toma en cuenta
                                            //se trabajara con la palabra nrepejecutar como variable reservada para el nombre del reporte
                                        } else if (valCampo.length() < 1) {//si recibe un parametro con null se rompre el ciclo
                                            break;
                                        } else if (nbCampo.equals("SUBREPORT_DIR") && valCampo.equals("*")) {//envio la ruta delreporte a ejecutar si no es relativa
                                            pReporte.put(nbCampo, new String(urlArchivo));
                                        } else if (nbCampo.toString().equals("QUERY")) {//envio el query si el reporte recibe el query completo                                        
                                            pReporte.put(nbCampo, new String(valCampo.replaceAll(":", "=")));
                                        } else {//se envian los parametros en general, los casos anteriores son casos especiales
                                            pReporte.put(nbCampo, new String((valCampo.equals("*")) ? " " : valCampo));

                                        }
                                    }
                                }
                            }
                        }
                        
                        byte[] bReporte = JasperRunManager.runReportToPdf(urlArchivo + nbArchivo, pReporte, Connec.DBReportes(request));//hago la variable byte de manera que conrtuyo el contenido del archivo PDF que voy a mostrar ejecutando el archivo jasper
                        
                        String nombre_archivo = nbArchivo.split("\\.")[0] + "_" +  Globales.FechaActual(3) + ".pdf";
                                                                        
                        FileOutputStream fos = new FileOutputStream(Connec.ruta_archivo + "DocumPDF/" + nombre_archivo);
                        fos.write(bReporte);
                        misession.setAttribute("ArchivoPDF", nombre_archivo);
                        
                        File origen = new File(Connec.ruta_archivo + "DocumPDF/" + nombre_archivo);
                        File destion = new File(ruta_gestion + nombre_archivo);
                        
                        Files.copy(origen.toPath(), destion.toPath());
                        
                        
                        response.setHeader("Content-Disposition", "inline; filename=" + nbReporte + ".pdf");
                        response.setContentLength(bReporte.length);//mido el peso del archivo ejecutado
                        ServletOutputStream osReporte = response.getOutputStream();//realizo el output de bytes del archivo
                        osReporte.write(bReporte, 0, bReporte.length);//monto el archivo en la pagina jsp                    
                        osReporte.flush();//limpio los flujos del archivo para la proxima instancia
                        osReporte.close();//cierro los flujos de los archivos
                        return;
                    } else {//si no existe la carpeta y los archivos
                        response.setContentType("application/pdf");
                    }

                } else {//si la carpeta de reportes no existe
                    response.setContentType("application/pdf");
                }
            }

        %>





    </body>
</html>
