package DB;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author eurid
 */
@WebServlet(urlPatterns = {"/Ubicacion"})
public class Ubicacion extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    String sql;
    String idusuario = "";
    String usuario = "";
    ResultSet datos = null;
    String iplocal = "";
    String url_archivo = Globales.url_archivo;
    String archivoadjunto = null;

    public String TablaUbicacionIngreso(HttpServletRequest request) {
        int numero = Globales.Validarintnonull(request.getParameter("numero"));
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        String equipo = request.getParameter("equipo");
        int entregado = Globales.Validarintnonull(request.getParameter("entregado"));

        String busqueda = "WHERE eliminado = 0 and (ma.descripcion ilike '%" + equipo + "%' or mo.descripcion ilike '%" + equipo + "%' or e.descripcion ilike '%" + equipo + "%')";

        if (cliente > 0) {
            busqueda += " and c.id = " + cliente;
        }
        if (numero > 0) {
            busqueda += " and r.ingreso = " + numero;
        }
        if (entregado != -1) {
            busqueda += " and coalesce(entregado,0) = " + entregado;
        }

        sql = "select row_number() OVER(order by r.ingreso) as fila, r.ingreso, y || x as ubicacion, to_char(re.fecha,'yyyy/MM/dd') as fecha, r.fotos,\n"
                + "'<b>' || m.descripcion || '</b><br>' || e.descripcion || '<br><b>' || ma.descripcion || '</b><br>' || mo.descripcion || '<br> ' || case when r.idintervalo = 0 then 'VER ESPECIFICACIONES' ELSE '(' || desde || ' a ' || hasta || ') ' || medida end || '<br>' || r.serie as equipo,\n"
                + "c.nombrecompleto as cliente, case when coalesce(entregado,0) = 0 then 'NO' ELSE 'SI' END as entregado\n"
                + "from ingreso_ubicacion u inner join remision_detalle r on r.ingreso = u.ingreso \n"
                + "inner join remision re on re.id = r.idremision\n"
                + "inner join equipo e on r.idequipo = e.id\n"
                + "inner join magnitudes m on m.id = e.idmagnitud\n"
                + "inner join modelos mo on mo.id = r.idmodelo  \n"
                + "inner join marcas ma on ma.id = mo.idmarca \n"
                + "inner join magnitud_intervalos i on i.id = r.idintervalo\n"
                + "inner join clientes c on c.id = r.idcliente " + busqueda + " ORDER BY r.ingreso";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->TablaUbicacionIngreso");
    }

    public String BuscarIngreso(HttpServletRequest request) {
        int numero = Globales.Validarintnonull(request.getParameter("numero"));

        sql = "select to_char(re.fecha,'yyyy/MM/dd') as fecha, r.fotos, r.ingreso,\n"
                + "m.descripcion as magnitud, e.descripcion as equipo,  ma.descripcion as marca, mo.descripcion as modelo, \n"
                + "case when r.idintervalo = 0 then 'VER ESPECIFICACIONES' ELSE '(' || desde || ' a ' || hasta || ') ' || medida end as medida, r.serie,\n"
                + "(SELECT y || x from ingreso_ubicacion u where u.ingreso = r.ingreso) as ubicacion\n"
                + "from remision_detalle r inner join remision re on re.id = r.idremision\n"
                + "inner join equipo e on r.idequipo = e.id\n"
                + "inner join magnitudes m on m.id = e.idmagnitud\n"
                + "inner join modelos mo on mo.id = r.idmodelo  \n"
                + "inner join marcas ma on ma.id = mo.idmarca \n"
                + "inner join magnitud_intervalos i on i.id = r.idintervalo\n"
                + "WHERE r.ingreso = " + numero;
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->BuscarIngreso");
    }

    public String DetalleUbicacionIngreso(HttpServletRequest request) {
        String x = request.getParameter("x");
        String y = request.getParameter("y");
        int numero = Globales.Validarintnonull(request.getParameter("numero"));
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        String equipo = request.getParameter("equipo");
        int entregado = Globales.Validarintnonull(request.getParameter("entregado"));

        String busqueda = "WHERE eliminado = 0 and u.x='" + x + "' and u.y = '" + y + "' and (ma.descripcion ilike '%" + equipo + "%' or mo.descripcion ilike '%" + equipo + "%' or e.descripcion ilike '%" + equipo + "%') ";

        if (cliente > 0) {
            busqueda += " and c.id = " + cliente;
        }
        if (numero > 0) {
            busqueda += " and r.ingreso = " + numero;
        }
        if (entregado != -1) {
            busqueda += " and coalesce(entregado,0) = " + entregado;
        }

        sql = "select row_number() OVER(order by r.ingreso) as fila, to_char(re.fecha,'yyyy/MM/dd') as fecha, r.fotos,\n"
                + "'<b>' || m.descripcion || '</b><br>' || e.descripcion || '<br><b>' || ma.descripcion || '</b><br>' || mo.descripcion || '<br> ' || case when r.idintervalo = 0 then 'VER ESPECIFICACIONES' ELSE '(' || desde || ' a ' || hasta || ') ' || medida end || '<br>' || r.serie as equipo,\n"
                + "e.descripcion as equipos, ma.descripcion || ' ' || mo.descripcion as modelo, coalesce(entregado,0) as entregado, u.y, u.x,\n"
                + "'<b>' || us.nomusu  || '</b><br>' || to_char(u.fecha,'dd/MM/yyyy HH24:MI') as ubicacion, r.ingreso, c.nombrecompleto as cliente,\n"
                + "(SELECT '<b>' || d.devolucion || '</b><br>' ||  to_char(d.fecha,'dd/MM/yyyy HH24:MI') FROM devolucion_detalle dd inner join devolucion d on d.id = dd.iddevolucion where dd.ingreso = u.ingreso ORDER BY dd.id desc limit 1) as devolucion,\n"
                + "(SELECT '<b>' || d1.nombreentrega || '</b><br>' ||  to_char(d1.fechaentrega,'dd/MM/yyyy HH24:MI') FROM devolucion_detalle dd1 inner join devolucion d1 on d1.id = dd1.iddevolucion where dd1.ingreso = u.ingreso ORDER BY dd1.id desc limit 1) as entregado,\n"
                + "'<button class=''btn btn-glow btn-primary'' title=''Editar Ubicacion'' type=''button'' onclick=' || chr(34) || 'EditarUbicacion(' || r.ingreso || ',' || r.entregado || ',''' || x || ''',''' || y || ''')' ||chr(34) || '><span data-icon=''&#xe064;''></span></button>' as editar,\n"
                + "'<button class=''btn btn-glow btn-danger'' title=''Eliminar Ubicacion'' type=''button'' onclick=' || chr(34) || 'EliminarUbicacion(' || r.ingreso || ',' || r.entregado || ',''' || e.descripcion || ' ' || ma.descripcion || ' ' || mo.descripcion || ''',''' || x || ''',''' || y || ''')' ||chr(34) || '><span data-icon=''&#xe0d8;''></span></button>' as eliminar\n"
                + "\n"
                + "from ingreso_ubicacion u inner join remision_detalle r on r.ingreso = u.ingreso \n"
                + "inner join remision re on re.id = r.idremision\n"
                + "inner join equipo e on r.idequipo = e.id\n"
                + "inner join magnitudes m on m.id = e.idmagnitud\n"
                + "inner join modelos mo on mo.id = r.idmodelo  \n"
                + "inner join marcas ma on ma.id = mo.idmarca \n"
                + "inner join magnitud_intervalos i on i.id = r.idintervalo\n"
                + "inner join seguridad.rbac_usuario us on u.idusuario = us.idusu\n"
                + "inner join clientes c on c.id = r.idcliente " + busqueda + " ORDER BY r.ingreso";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->DetalleUbicacionIngreso");
    }

    public String BusquedaUbicacionIngreso(HttpServletRequest request) {
        try {

            String x = request.getParameter("x");
            String y = request.getParameter("y");
            int numero = Globales.Validarintnonull(request.getParameter("numero"));
            int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
            String equipo = request.getParameter("equipo");
            int entregado = Globales.Validarintnonull(request.getParameter("entregado"));

            String Ingreso = "";
            String busqueda = " and (ma.descripcion ilike '%" + equipo + "%' or mo.descripcion ilike '%" + equipo + "%' or e.descripcion ilike '%" + equipo + "%')";

            if (cliente > 0) {
                busqueda += " and rd.idcliente = " + cliente;
            }
            if (numero > 0) {
                busqueda += " and rd.ingreso = " + numero;
            }
            if (entregado != -1) {
                busqueda += " and coalesce(entregado,0) = " + entregado;
            }

            if (cliente == 0 && numero == 0) {
                sql = "SELECT count(rd.ingreso) as cantidad, coalesce(entregado,0) as entregado\n"
                        + "FROM public.ingreso_ubicacion u inner join remision_detalle rd on u.ingreso = rd.ingreso\n"
                        + "inner join equipo e on e.id = rd.idequipo\n"
                        + "inner join modelos mo on mo.id = rd.idmodelo\n"
                        + "inner join marcas ma on ma.id = mo.idmarca\n"
                        + "WHERE x = '" + x + "' and y = '" + y + "' AND eliminado = 0 and (ma.descripcion ilike '%" + equipo + "%' or mo.descripcion ilike '%" + equipo + "%' or e.descripcion ilike '%" + equipo + "%') " + (entregado != -1 ? " and coalesce(entregado, 0) = " + entregado : "") + "  \n"
                        + "group by coalesce(entregado,0) ORDER BY coalesce(entregado,0)";
                datos = Globales.Obtenerdatos(sql, this.getClass() + "-->BusquedaUbicacionIngreso", 1);

                while (datos.next()) {
                    if (datos.getString(2).equals("0")) {
                        Ingreso += "<span class='text-success'>" + datos.getString(1) + " equipos </span>";
                    } else {
                        if (!Ingreso.equals("")) {
                            Ingreso += "<br>";
                        }
                        Ingreso += "<span class='text-danger'>" + datos.getString(1) + " entregados</span>";
                    }
                }

            } else {
                sql = "SELECT rd.ingreso, coalesce(entregado,0) as entregado\n"
                        + "FROM public.ingreso_ubicacion u inner join remision_detalle rd on u.ingreso = rd.ingreso\n"
                        + "inner join equipo e on e.id = rd.idequipo\n"
                        + "inner join modelos mo on mo.id = rd.idmodelo\n"
                        + "inner join marcas ma on ma.id = mo.idmarca\n"
                        + "WHERE x = '" + x + "' and y = '" + y + "' " + busqueda + " \n"
                        + "ORDER BY coalesce(entregado,0)";
                datos = Globales.Obtenerdatos(sql, this.getClass() + "-->BusquedaUbicacionIngreso", 1);

                while (datos.next()) {
                    if (datos.getString(2).equals("0")) {
                        Ingreso += "<span class='text-success'>" + datos.getString(1) + " </span>";
                    } else {
                        Ingreso += "<span class='text-danger'>" + datos.getString(1) + " </span>";
                    }
                }

            }
            if (Ingreso.equals("")) {
                Ingreso = "&nbsp;";
            }
            return Ingreso;
        } catch (SQLException ex) {
            return "1|" + ex.getMessage();
        }

    }

    public String Operaciones(HttpServletRequest request) {
        int numero = Globales.Validarintnonull(request.getParameter("numero"));
        String ubicacion = request.getParameter("ubicacion");
        int operacion = Globales.Validarintnonull(request.getParameter("operacion"));
        String resultado = "";
        if (operacion == 1) {
            String x = ubicacion.substring(1, 2);
            String y = ubicacion.substring(0, 1);

            sql = "SELECT entregado FROM remision_detalle where ingreso = " + numero;
            int encontrado = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
            if (encontrado > 0) {
                return "1|No se puede cambiar la ubiciación a un equipo entregado";
            }

            sql = "SELECT id FROM ingreso_ubicacion where ingreso = " + numero;
            encontrado = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
            sql = "";
            if (encontrado == 0) {
                sql = "INSERT INTO ingreso_ubicacion(ingreso, idusuario, x, y) "
                        + "VALUES(" + numero + "," + idusuario + ",'" + x + "','" + y + "')";
                resultado = "0|Ubicación agregada con éxito";
            } else {
                sql = "UPDATE ingreso_ubicacion "
                        + "SET x='" + x + "', y='" + y + "', eliminado = 0, fecha= now(), idusuario=" + idusuario + " WHERE ingreso = " + numero + "";
                resultado = "0|Ubicación actualizada con éxito";
            }

            Globales.Obtenerdatos(sql, this.getClass() + "-->Operaciones", 2);
        } else {
            sql = "UPDATE ingreso_ubicacion SET eliminado = 1 WHERE ingreso = " + numero;
            Globales.Obtenerdatos(sql, this.getClass() + "-->Operaciones", 2);
            resultado = "0|Ubicación eliminada con éxito";
        }

        return resultado;
    }

    protected void Servidor(HttpServletRequest request, HttpServletResponse response) {

        try {
            response.setContentType("text/html;charset=UTF-8");
            String ruta_origen = request.getHeader("Origin");

            HttpSession misession = request.getSession();
            idusuario = misession.getAttribute("idusuario").toString();
            iplocal = misession.getAttribute("IPLocal").toString();
            usuario = misession.getAttribute("codusu").toString();
            if (misession.getAttribute("ArchivosAdjuntos") != null) {
                archivoadjunto = misession.getAttribute("ArchivosAdjuntos").toString();
            }
            response.setHeader("Access-Control-Allow-Origin", request.getHeader("Origin"));
            response.setHeader("Access-Control-Allow-Credentials", "true");

            PrintWriter out = response.getWriter();
            if (Globales.ValidarRuta(ruta_origen) == false) {
                out.print("[{\"error\":\"505\",\"mensaje\":\"Usted no tiene permiso para acceder a esta ruta\"}]");
            } else if (misession.getAttribute("codusu") == null) {
                out.println("cerrada");
            } else if (!Globales.IpUsuario(misession.getAttribute("idusuario").toString()).equals(misession.getAttribute("IPLocal").toString())) {
                out.println("cambio-ip");
            } else {
                String opcion = request.getParameter("opcion");
                switch (opcion) {
                    //ANDRES
                    case "TablaUbicacionIngreso":
                        out.println(TablaUbicacionIngreso(request));
                        break;
                    case "BuscarIngreso":
                        out.println(BuscarIngreso(request));
                        break;
                    case "DetalleUbicacionIngreso":
                        out.println(DetalleUbicacionIngreso(request));
                        break;
                    case "BusquedaUbicacionIngreso":
                        out.println(BusquedaUbicacionIngreso(request));
                        break;
                    case "Operaciones":
                        out.println(Operaciones(request));
                        break;

                    //FIN
                    default:
                        throw new AssertionError();
                }
            }
        } catch (Exception ex) {
            try {
                PrintWriter out = response.getWriter();
                out.println(ex.getMessage());

            } catch (IOException exl) {
                Globales.GuardarLogger(exl.getMessage(), Laboratorio.class
                        .getName());
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        Servidor(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * param request servlet request param response servlet response throws
     * ServletException if a servlet-specific error occurs throws IOException if
     * an I/O error occurs
     *
     * @param request
     * @param response
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        Servidor(request, response);
    }

}
