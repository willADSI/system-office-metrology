package DB;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

/**
 *
 * @author leudi
 */
@WebServlet(urlPatterns = {"/AdjuntarArchivo"})
@MultipartConfig(
        fileSizeThreshold = 1024 * 1024 * 1, // 1 MB
        maxFileSize = 1024 * 1024 * 10, // 10 MB
        maxRequestSize = 1024 * 1024 * 15//, // 15 MB
//location            = "C:/upload" 
)
public class AdjuntarArchivo extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    String sql;
    String idusuario = "";
    String usuario = "";
    String iplocal = "";
    String url_archivo = Globales.url_archivo;

    private boolean validateArchivo(String imageName) {
        
        String[] archivo = imageName.split("\\.");
        String fileExt = archivo[archivo.length-1];
        if ("jpg".equals(fileExt) || "png".equals(fileExt) || "gif".equals(fileExt) || "pdf".equals(fileExt) || "doc".equals(fileExt) || "docx".equals(fileExt) || "ppt".equals(fileExt) || "pptx".equals(fileExt) || "xls".equals(fileExt) || "xlsx".equals(fileExt) ) {
            return true;
        }
        return false;
    }

    protected void Servidor(HttpServletRequest request, HttpServletResponse response) {
        try {

            response.setContentType("text/html;charset=UTF-8");
            String ruta_origen = request.getHeader("Origin");
            if (ruta_origen == null) {
                ruta_origen = "http://localhost:8090";
            }

            HttpSession misession = request.getSession();
            idusuario = misession.getAttribute("idusuario").toString();
            iplocal = misession.getAttribute("IPLocal").toString();
            usuario = misession.getAttribute("codusu").toString();

            response.setHeader("Access-Control-Allow-Origin", request.getHeader("Origin"));
            response.setHeader("Access-Control-Allow-Credentials", "true");
            String Error = "";
            String Almacenados = "";
            String Variable = "";
            PrintWriter out = response.getWriter();
            if (Globales.ValidarRuta(ruta_origen) == false) {
                out.print("[{\"error\":\"505\",\"mensaje\":\"Usted no tiene permiso para acceder a esta ruta\"}]");
            } else {
                if (misession.getAttribute("codusu") == null) {
                    out.println("cerrada");
                } else {
                    if (!Globales.IpUsuario(misession.getAttribute("idusuario").toString()).equals(misession.getAttribute("IPLocal").toString())) {
                        out.println("cambio-ip");
                    } else {
                        
                        File uploads = new File(Globales.ruta_archivo + "uploads/");
                        
                        Part part;
                        String imageName;
                        DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
                        Date fecha = new Date();
                        String fechalarga = dateFormat.format(fecha);
                        String ArchivoRuta = "";
                        InputStream input;
                        File file;
                        String[] id = request.getParameterValues("id");
                        for (int x = 0; x < id.length; x++) {
                            part = request.getPart(id[x]);
                            imageName = part.getSubmittedFileName();
                            if (this.validateArchivo(imageName)) {
                                try {
                                    input = part.getInputStream();
                                    file = new File(uploads, fechalarga + "_" +imageName);
                                    Files.copy(input, file.toPath());
                                    if (!Almacenados.equals("")){
                                        Almacenados += "<br>";
                                        Variable += "|";
                                        ArchivoRuta += "<br>";
                                    }
                                    Almacenados += imageName;
                                    Variable += fechalarga + "_" +imageName;
                                    ArchivoRuta += fechalarga + "_" +imageName;
                                } catch (IOException ex) {
                                    Globales.GuardarLogger(ex.getMessage(), "Subir Foto");
                                    out.println(ex.getMessage());
                                }
                            } else {
                                Error += "Formato incorrecto del archivo " + imageName + "<br>";
                            }
                        }
                        misession.setAttribute("ArchivosAdjuntos", Variable);
                        out.println("0||<b>Archivos Subidos:</b> <br>" + Almacenados + "<br><br><b>Archivos no permitidos:</b><br>" + Error + "||" + ArchivoRuta );
                    }
                }
            }
        } catch (Exception ex) {
            try {
                PrintWriter out = response.getWriter();
                out.println("1||" + ex.getMessage());
            } catch (IOException exl) {
                Globales.GuardarLogger(exl.getMessage(), Laboratorio.class.getName());
            }
        }
    }
    

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        Servidor(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        Servidor(request, response);
    }

}
