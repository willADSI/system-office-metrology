package DB;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Sistemas
 */
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.servlet.http.HttpServletRequest;

public class Globales {

    public static String url_db = "jdbc:postgresql://localhost:5433/calibracion";
    public static String usuario_db = "postgres";
    public static String contrasena_db = "CONTROLDB";

    public static String url_archivo = ObtenerUnValor("select url_archivo from empresa");
    public static String ruta_archivo = ObtenerUnValor("select ruta_archivo from empresa");
    public static String ruta_gestion = ObtenerUnValor("select ruta_gestion from empresa");
    public static String ruta_respaldo = ObtenerUnValor("select ruta_respaldo from empresa");
    public static String ruta_media_linux = ObtenerUnValor("select ruta_media_linux from empresa");

    private static String smtp_host = ObtenerUnValor("select smtp_host from empresa");
    public static String smtp_port = ObtenerUnValor("select smtp_port from empresa");
    public static String Nombre_Empresa = ObtenerUnValor("select razonsocial from empresa");

    public static int MAX_WIDTH = 400;
    public static int MAX_HEIGHT = 400;
    public static List<String> filesListInDir = null;
    public static String[] DiasSemana = new String[]{"Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"};

    @SuppressWarnings("UseSpecificCatch")
    public static Connection BaseDatos() {
        try {
            Class.forName("org.postgresql.Driver");
            return DriverManager.getConnection(url_db, usuario_db, contrasena_db);
        } catch (Exception e) {
            GuardarLogger(e.getMessage(), "CONEXION A BASE DE DATOS");
            return null;
        }
    }

    public static Connection DBReportes(HttpServletRequest request) {
        try {
            Class.forName("org.postgresql.Driver");
            return DriverManager.getConnection(url_db, usuario_db, contrasena_db);
        } catch (Exception e) {
            GuardarLogger(e.getMessage(), "CONEXION A BASE DE DATOS");
            return null;
        }
    }

    public static void GuardarLogger(String error, String metodo) {
        try {

            Logger logger = Logger.getLogger("LogdeNacho");
            String pathLog = ruta_archivo + "log/MyLog_" + FechaActual(1) + ".log";

            File logDir = new File("./logs/");
            if (!(logDir.exists())) {
                logDir.mkdirs();
            }

            FileHandler fhandler = new FileHandler(pathLog);
            logger.addHandler(fhandler);
            SimpleFormatter formatter = new SimpleFormatter();
            fhandler.setFormatter(formatter);
            logger.info(metodo + "\n" + error + "\n-----------------------------------------------------------------------------------------------------\n");

        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static ResultSet Obtenerdatos(String sql, String metodo, int opcion) {
        Connection conec = BaseDatos();
        Statement st = null;
        try {

            if (conec == null) {
                return null;
            }
            ResultSet datos = null;

            if (opcion == 1) {
                st = conec.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
                datos = st.executeQuery(sql);
            } else {
                st = conec.createStatement();
                st.executeUpdate(sql);
            }
            conec.close();
            return datos;
        } catch (SQLException ex) {
            GuardarLogger(ex.getMessage() + "\n SQL: " + sql, metodo);
            return null;
        }
    }

    private static String TextoAuditoria(String sql) {
        sql = sql.replace("INSERT INTO", "INSERTANDO").replace("UPDATE", "ACTUALIZANDO").replace("DELETE", "ELIMINADO")
                .replace("FROM", "TABLA").replace("SET", "CAMBIAR").replace("WHERE", "DONDE").replace("'", "");
        sql = sql.replace("insert into", "INSERTANDO").replace("update", "ACTUALIZANDO").replace("delete", "ELIMINADO")
                .replace("from", "TABLA").replace("set", "CAMBIAR").replace("where", "DONDE").replace("'", "");
        return sql;
    }

    public static Boolean DatosAuditoria(String sql, String modulo, String accion, String idusuario, String iplocal, String metodo) {
        Connection conec = BaseDatos();
        Statement st = null;
        try {
            if (conec == null) {
                return null;
            }
            st = conec.createStatement();
            st.executeUpdate(sql);

            sql = "INSERT INTO seguridad.auditoria(idususario, modulo, accion, consulta, ip) "
                    + "VALUES (" + idusuario + ",'" + modulo + "','" + accion + "','" + TextoAuditoria(sql) + "','" + iplocal + "')";
            Obtenerdatos(sql, "GUARDAR AUDITORIA", 2);

            return true;

        } catch (SQLException ex) {
            GuardarLogger(ex.getMessage() + "\n SQL: " + sql, metodo);
            return false;
        }
    }

    /*public static String DatosAuditoriaValor(String sql, String modulo, String accion, String idusuario, String iplocal) {
        Connection conec = BaseDatos();
        Statement st = null;
        String valor = "";
        try {

            ResultSet datos = null;
            st = conec.createStatement();

            datos = st.executeQuery(sql);

            sql = "INSERT INTO seguridad.auditoria(idususario, modulo, accion, consulta, ip) "
                    + "  VALUES (" + idusuario + ",'" + modulo + "','" + accion + "','" + TextoAuditoria(sql) + "','" + iplocal + "')";
            Obtenerdatos(sql, "DatosAuditoriaValor", 2);
            if (datos.next()) {
                valor = datos.getString(1).trim();
            }

        } catch (SQLException ex) {
            GuardarLogger(ex.getMessage(), "DatosAuditoriaValor");

        }
        return valor;
    }*/
    public static String AntiJaquer(String cadena) {
        cadena = cadena.replaceAll("DROP ", "");
        cadena = cadena.replaceAll("SELECT ", "");
        cadena = cadena.replaceAll("ALTER ", "");
        cadena = cadena.replaceAll("DELETE ", "");
        cadena = cadena.replaceAll("UPDATE ", "");
        cadena = cadena.replaceAll("OR ", "");
        cadena = cadena.replaceAll("AND ", "");
        cadena = cadena.replaceAll("INSERT ", "");
        cadena = cadena.replaceAll("ORDER BY ", "");
        return cadena;

    }

    public static String ObtenerDatosJSon(String sql, String metodo) {

        try {

            Connection conec = BaseDatos();
            Statement st = null;
            String Resultado = "[]";

            if (conec == null) {
                return null;
            }

            st = conec.createStatement();
            sql = "select json_agg(t.*) as json from (" + sql + ") as t";
            ResultSet datos = st.executeQuery(sql);

            if (datos.next()) {
                if (datos.getString(1) != null) {
                    Resultado = datos.getString(1);
                }
            }
            datos.close();
            st.close();
            conec.close();
            return Resultado;
        } catch (SQLException ex) {
            GuardarLogger(ex.getMessage() + "\n SQL: " + sql, metodo);
            return null;
        }
    }

    public static String ObtenerUnValor(String sql) {

        try {

            Connection conec = BaseDatos();
            Statement st = null;
            String Resultado = "";

            if (conec == null) {
                return null;
            }

            st = conec.createStatement();
            ResultSet datos = st.executeQuery(sql);

            if (datos.next()) {
                if (datos.getString(1) != null) {
                    Resultado = datos.getString(1);
                }
            }
            datos.close();
            st.close();
            conec.close();
            return Resultado;
        } catch (SQLException ex) {
            GuardarLogger(ex.getMessage() + "\n SQL: " + sql, "OBTENER UN VALOR");
            return null;
        }
    }

    public static String IpUsuario(String idusuario) {
        String sql = "SELECT sesion from seguridad.rbac_usuario where idusu = " + idusuario;
        return ObtenerUnValor(sql);
    }

    public static int Validarintnonull(String numero) {
        int resultado = 0;
        if (isNumeric(numero)) {
            resultado = Integer.parseInt(numero);
        }
        return resultado;
    }

    public static double ValidardoublenoNull(String numero) {
        double resultado = 0;
        if (isNumeric(numero)) {
            resultado = Double.parseDouble(numero);
        }
        return resultado;
    }

    public static boolean isNumeric(String strNum) {
        if (strNum == null) {
            return false;
        }
        try {
            double d = Double.parseDouble(strNum);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    public static Date ValidarFecha(String fecha) {
        Date resultado = null;
        try {
            SimpleDateFormat formatoFecha = new SimpleDateFormat("yyyy-MM-dd");
            formatoFecha.setLenient(false);
            resultado = formatoFecha.parse(fecha);
            return resultado;
        } catch (ParseException ex) {
            GuardarLogger(ex.getMessage(), "Validar-Fecha");
            return null;
        }
    }

    public static String FechaActual(int tipo) {
        Date fecha = new Date();
        DateFormat hourFormat = new SimpleDateFormat("HH:mm:ss");
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat dateFormatym = new SimpleDateFormat("yyyy-MM");
        DateFormat dateFormat3 = new SimpleDateFormat("yyyyMMddHHmmss");
        SimpleDateFormat formato = new SimpleDateFormat("EEEE d 'de' MMMM 'de' yyyy", new Locale("es_ES"));
        String resultado = "";
        switch (tipo) {
            case 1:
                resultado = dateFormat.format(fecha);
                break;
            case 2:
                resultado = dateFormat.format(fecha) + " " + hourFormat.format(fecha);
                break;
            case 3:
                resultado = dateFormat3.format(fecha);
                break;
            case 4:
                resultado = formato.format(fecha) + " " + hourFormat.format(fecha);
                break;
            case 5:
                resultado = dateFormatym.format(fecha);
                break;
        }
        return resultado;
    }

    public static String EncryptKey(String input) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] messageDigest = md.digest(input.getBytes());
            BigInteger no = new BigInteger(1, messageDigest);
            String hashtext = no.toString(16);
            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }
            return hashtext;
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    public static String ObtenerCombo(String sql, int inicial, int titulo, int inactivo) {
        String combo = "";
        if (inicial == 1) {
            combo = "<option value='' idioma='--Seleccione--'>--Seleccione--</option>";
        }
        if (inicial == 2) {
            combo = "<option value='' idioma='TODOS'>TODOS</option>";
        }
        if (inicial == 3) {
            combo = "<option value='0' idioma='VER ESPECIFICACIONES'>VER ESPECIFICACIONES</option>";
        }
        if (inicial == 4) {
            combo = "<option value='0' idioma='TODOS'>TODOS</option>";
        }
        if (inicial == 5) {
            combo = "<option value='11' idioma='CALIBRATION SERVICE SAS'>CALIBRATION SERVICE SAS</option>";
        }
        if (inicial == 6) {
            combo = "<option value='AUTORIZACIÓN'>AUTORIZACIÓN</option>";
        }
        if (inicial == 7) {
            combo = "<option value='0'>NINGUNO</option>";
        }
        if (inicial == 8) {
            combo = "<option value='0'>AMBOS SENTIDOS</option>";
        }
        if (inicial == 9) {
            combo = "<option value='0'>SIN ASIGNAR</option>";
        }

        ResultSet datos = Obtenerdatos(sql, "ObtenerCombo", 1);
        try {
            while (datos.next()) {
                combo += "<option value='" + datos.getString(1) + "' idioma='" + datos.getString(2).trim() + "'" + (titulo != 0 ? " title='" + datos.getString(3) + "'" : "") + (inactivo == 1 ? (datos.getString(4).equals("1") ? " disabled " : "") : "") + ">" + datos.getString(2) + "</option>";
            }
        } catch (SQLException ex) {
            GuardarLogger(ex.getMessage(), "ObtenerCombo");
        }
        return combo;
    }

    public static String ObtenerComboDatos(ResultSet datos, int inicial) {
        String combo = "";
        if (inicial == 1) {
            combo = "<option value='' idioma='--Seleccione--'>--Seleccione--</option>";
        }
        if (inicial == 2) {
            combo = "<option value='' idioma='TODOS'>TODOS</option>";
        }
        if (inicial == 3) {
            combo = "<option value='0' idioma='VER ESPECIFICACIONES'>VER ESPECIFICACIONES</option>";
        }
        if (inicial == 4) {
            combo = "<option value='0' idioma='TODOS'>TODOS</option>";
        }
        if (inicial == 5) {
            combo = "<option value='11' idioma='CALIBRATION SERVICE SAS'>CALIBRATION SERVICE SAS</option>";
        }
        if (inicial == 6) {
            combo = "<option value='AUTORIZACIÓN'>AUTORIZACIÓN</option>";
        }
        if (inicial == 7) {
            combo = "<option value='0'>NINGUNO</option>";
        }
        try {
            while (datos.next()) {
                combo += "<option value='" + datos.getString(1) + "' idioma='" + datos.getString(2).trim() + "'>" + datos.getString(2).trim() + "</option>";
            }
        } catch (SQLException ex) {
            GuardarLogger(ex.getMessage(), "ObtenerComboDatos");
        }
        return combo;
    }

    public static int PermisosSistemas(String Accion, String usuario) {
        int permiso = 0;
        String sql = "SELECT max(idgruper) "
                + " FROM seguridad.rbac_grupo_permiso gp INNER JOIN seguridad.rbac_usuario_grupo ug using (idgru) "
                + "		                                         INNER JOIN seguridad.rbac_modulo_accion ma using(idmodacc) "
                + "                                     INNER JOIN seguridad.rbac_grupo g using(idgru) "
                + "                                     INNER JOIN seguridad.rbac_modulo m using(idmod) "
                + "                                     INNER JOIN seguridad.rbac_usuario u using (idusu) "
                + " WHERE estgru = 'ACTIVO' AND ((estmod = 'ACTIVO' AND desacc = '" + Accion + "') or (supusu =1)) AND idusu = " + usuario;
        permiso = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
        return permiso;
    }

    public static String ValidarRutaGestion(String documento, String numero) {
        String ruta = ObtenerUnValor("select ruta from empresa_ruta_gestion where documento = '" + documento + "'");
        ResultSet datos = null;
        String resultado = "";
        String Anio = "";
        String Mes = "";
        String Mes_Corto = "";
        String Magnitud = "";
        String sql;
        if (ruta.equals("")) {
            return "";
        }
        try {
            switch (documento) {
                case "Cotizacion":
                case "Cotizacion_Servicio":
                    sql = "select to_char(fecha,'yyyy') as anio, obtener_mes(to_char(fecha,'MM')::int) as mes, obtener_mes_corto(to_char(fecha,'MM')::int) as mes_corto, '' as magnitud \n"
                            + "from cotizacion where cotizacion = " + numero;
                    datos = Obtenerdatos(sql, "ValidarRutaGestion", 1);
                    break;
                case "Remision_Cliente":
                case "Remision":
                    sql = "select to_char(fecha,'yyyy') as anio, obtener_mes(to_char(fecha,'MM')::int) as mes, obtener_mes_corto(to_char(fecha,'MM')::int) as mes_corto, '' as magnitud \n"
                            + "from remision where remision = " + numero;
                    datos = Obtenerdatos(sql, "ValidarRutaGestion", 1);
                    break;
                case "Devolucion":
                    sql = "select to_char(fecha,'yyyy') as anio, obtener_mes(to_char(fecha,'MM')::int) as mes, obtener_mes_corto(to_char(fecha,'MM')::int) as mes_corto, '' as magnitud \n"
                            + "from devolucion where devolucion = " + numero;
                    datos = Obtenerdatos(sql, "ValidarRutaGestion", 1);
                    break;
                case "EtiquetaDevolucion":
                    sql = "select to_char(fecha,'yyyy') as anio, obtener_mes(to_char(fecha,'MM')::int) as mes, obtener_mes_corto(to_char(fecha,'MM')::int) as mes_corto, '' as magnitud \n"
                            + "from imprimir_etiqueta where devolucion = " + numero + " limit 1";
                    datos = Obtenerdatos(sql, "ValidarRutaGestion", 1);
                    break;
                case "Solicitud":
                    sql = "select to_char(fecha,'yyyy') as anio, obtener_mes(to_char(fecha,'MM')::int) as mes, obtener_mes_corto(to_char(fecha,'MM')::int) as mes_corto, '' as magnitud \n"
                            + "from web.solicitud where solicitud = " + numero;
                    datos = Obtenerdatos(sql, "ValidarRutaGestion", 1);
                    break;
                case "Salida":
                    sql = "select to_char(fecha,'yyyy') as anio, obtener_mes(to_char(fecha,'MM')::int) as mes, obtener_mes_corto(to_char(fecha,'MM')::int) as mes_corto, '' as magnitud \n"
                            + "from salida where salida = " + numero;
                    datos = Obtenerdatos(sql, "ValidarRutaGestion", 1);
                    break;
                case "OrdenCompra":
                    sql = "select to_char(fecha,'yyyy') as anio, obtener_mes(to_char(fecha,'MM')::int) as mes, obtener_mes_corto(to_char(fecha,'MM')::int) as mes_corto, '' as magnitud \n"
                            + "from ordencompra_tercero where orden = " + numero;
                    datos = Obtenerdatos(sql, "ValidarRutaGestion", 1);
                    break;
                case "Factura":
                    sql = "select to_char(fecha,'yyyy') as anio, obtener_mes(to_char(fecha,'MM')::int) as mes, obtener_mes_corto(to_char(fecha,'MM')::int) as mes_corto, '' as magnitud \n"
                            + "from factura where factura = " + numero;
                    datos = Obtenerdatos(sql, "ValidarRutaGestion", 1);
                    break;
                case "InformeTecnico":
                    sql = "select to_char(if.fecha,'yyyy') as anio, obtener_mes(to_char(if.fecha,'MM')::int) as mes, obtener_mes_corto(to_char(if.fecha,'MM')::int) as mes_corto, m.descripcion as magnitud \n"
                            + "from informetecnico_dev if inner join remision_detalle rd on rd.ingreso = if.ingreso "
                            + "                           inner join equipo e on e.id = rd.idequipo "
                            + "                           inner join magnitudes m on m.id = e.idmagnitud "
                            + "where if.id = " + numero;
                    datos = Obtenerdatos(sql, "ValidarRutaGestion", 1);
                    break;

            }
            if (datos != null) {
                datos.next();
                Anio = datos.getString("anio");
                Mes = datos.getString("mes");
                Mes_Corto = datos.getString("mes_corto");
                Magnitud = datos.getString("magnitud");

                resultado = ruta_gestion + ruta.replaceAll("\\[ANIO]", Anio);
                resultado = resultado.replaceAll("\\[MES]", Mes);
                resultado = resultado.replaceAll("\\[MES_CORTO]", Mes_Corto);
                resultado = resultado.replaceAll("\\[MAGNITUD]", Magnitud);
                File archivo = new File(resultado);
                if (!archivo.exists()) {
                    if (archivo.mkdirs()) {
                        return resultado;
                    } else {
                        return "";
                    }
                } else {
                    return resultado;
                }
            } else {
                return "";
            }
        } catch (Exception e) {
            GuardarLogger(e.getMessage(), "ValidarRutaGestion");
            return "";
        }
    }

    public static boolean ValidarRuta(String ruta) {

        try {
            String sql = "select ruta from empresa_rutaspermitidas";
            ResultSet datos = Obtenerdatos(sql, "ValidarRuta", 1);
            while (datos.next()) {
                if (datos.getString("ruta").equals(ruta)) {
                    return true;
                }
            }
            return false;
        } catch (SQLException ex) {
            GuardarLogger(ex.getMessage(), "ValidarRuta");
            return false;
        }
    }

    public static String ExtensionArchivo(String imageName) {
        String[] archivo = imageName.split("\\.");
        return archivo[archivo.length - 1];
    }

    //public static void enviarConGMail(String destinatario, String asunto, String cuerpo) {
    public static String EnviarEmail(String Destinatario, String laboratorio, String[] DestinatariosCCO, String Asunto, String Mensaje, String CorreoEmpresa, String Adjunto, String correoempenv, String clavecorreoempenv, String nombrearchivo) {
        // Esto es lo que va delante de @gmail.com en tu cuenta de correo. Es el remitente también.

        Properties props = System.getProperties();
        props.put("mail.smtp.host", Globales.smtp_host);  //El servidor SMTP de Google
        props.put("mail.smtp.user", correoempenv);
        props.put("mail.smtp.clave", clavecorreoempenv);    //La clave de la cuenta
        props.put("mail.smtp.auth", "true");    //Usar autenticación mediante usuario y clave
        props.put("mail.smtp.starttls.enable", "false"); //Para conectar de manera segura al servidor SMTP
        props.put("mail.smtp.port", Globales.smtp_port); //El puerto SMTP seguro de Google

        Session session = Session.getDefaultInstance(props);
        MimeMessage message = new MimeMessage(session);

        try {
            message.setFrom(new InternetAddress(correoempenv));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(Destinatario));

            message.addHeader("Content-Type", "text/html; charset=UTF-8");
            for (int x = 0; x < DestinatariosCCO.length; x++) {
                if (!DestinatariosCCO[x].trim().equals("")) {
                    if (!DestinatariosCCO[x].trim().equals(Destinatario.trim()) && !DestinatariosCCO[x].trim().equals(Destinatario)) {
                        message.addRecipients(Message.RecipientType.BCC, new InternetAddress[]{new InternetAddress(DestinatariosCCO[x].trim())});
                    }
                }
            }
            message.setSubject(Asunto);

            BodyPart texto = new MimeBodyPart();
            texto.setContent(Mensaje, "text/html");

            BodyPart adjunto;
            MimeMultipart multiParte = new MimeMultipart();

            if (!Adjunto.equals("")) {

                String[] a_adjunto = Adjunto.split(",");
                String[] a_nombre = nombrearchivo.split(",");
                for (int x = 0; x < a_adjunto.length; x++) {

                    adjunto = new MimeBodyPart();
                    adjunto.setDataHandler(new DataHandler(new FileDataSource(a_adjunto[x])));
                    adjunto.setFileName(a_nombre[x]);
                    multiParte.addBodyPart(adjunto);
                }
            }

            multiParte.addBodyPart(texto);
            message.setContent(multiParte);

            Transport transport = session.getTransport("smtp");
            transport.connect(Globales.smtp_host, correoempenv, clavecorreoempenv);
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();
            return "";
        } catch (MessagingException me) {
            return me.getMessage();   //Si se produce un error
        }
    }

    public static boolean ComprimirIamgen(String Copia, String Original) {
        try {
            File input = new File(Copia);
            BufferedImage image = ImageIO.read(input);
            File compressedImageFile = new File(Original);
            OutputStream os = new FileOutputStream(compressedImageFile);
            Iterator<ImageWriter> writers = ImageIO.getImageWritersByFormatName("jpeg");
            ImageWriter writer = (ImageWriter) writers.next();
            ImageOutputStream ios = ImageIO.createImageOutputStream(os);
            writer.setOutput(ios);
            ImageWriteParam param = writer.getDefaultWriteParam();
            param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
            param.setCompressionQuality(0.75f);  // Change the quality value you prefer
            writer.write(null, new IIOImage(image, null, null), param);
            //input.delete();
            os.close();
            ios.close();
            writer.dispose();
            return true;
        } catch (IOException ex) {
            GuardarLogger(ex.getMessage(), "ComprimirIamgen");
            return false;
        }
    }

    public static boolean ConvertirJPG(File input, File output) {

        try {

            BufferedImage image = ImageIO.read(input);
            BufferedImage result = new BufferedImage(
                    image.getWidth(),
                    image.getHeight(),
                    BufferedImage.TYPE_INT_RGB);
            result.createGraphics().drawImage(image, 0, 0, Color.WHITE, null);
            ImageIO.write(result, "jpg", output);

            return true;

        } catch (IOException e) {
            GuardarLogger(e.getMessage(), "ConvertirJPG");
            return false;
        }
    }

    public static boolean CortarImagen(File input, File output, int x1, int y1, int w, int h) {

        try {

            BufferedImage image = ImageIO.read(input);
            BufferedImage result = ((BufferedImage) image).getSubimage(x1, y1, w, h);
            result.createGraphics().drawImage(image, 0, 0, Color.WHITE, null);
            ImageIO.write(result, "jpg", output);

            return true;

        } catch (IOException e) {
            GuardarLogger(e.getMessage(), "CortarImagen");
            return false;
        }
    }

    public static boolean zipDirectory(File dir, String zipDirName) {
        try {
            filesListInDir = new ArrayList<String>();
            populateFilesList(dir);
            //now zip files one by one
            //create ZipOutputStream to write to the zip file
            FileOutputStream fos = new FileOutputStream(zipDirName);
            ZipOutputStream zos = new ZipOutputStream(fos);
            for (String filePath : filesListInDir) {
                System.out.println("Zipping " + filePath);
                //for ZipEntry we need to keep only relative file path, so we used substring on absolute path
                ZipEntry ze = new ZipEntry(filePath.substring(dir.getAbsolutePath().length() + 1, filePath.length()));
                zos.putNextEntry(ze);
                //read the file and write to ZipOutputStream
                FileInputStream fis = new FileInputStream(filePath);
                byte[] buffer = new byte[1024];
                int len;
                while ((len = fis.read(buffer)) > 0) {
                    zos.write(buffer, 0, len);
                }
                zos.closeEntry();
                fis.close();
            }
            zos.close();
            fos.close();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    private static void populateFilesList(File dir) throws IOException {
        File[] files = dir.listFiles();
        for (File file : files) {
            if (file.isFile()) {
                filesListInDir.add(file.getAbsolutePath());
            } else {
                populateFilesList(file);
            }
        }
    }

    public static void copiarDirectorio(String origen, String destino) {
        comprobarCrearDirectorio(destino);
        File directorio = new File(origen);
        File f;
        if (directorio.isDirectory()) {
            comprobarCrearDirectorio(destino);
            String[] files = directorio.list();
            if (files.length > 0) {
                for (String archivo : files) {
                    f = new File(origen + File.separator + archivo);
                    if (f.isDirectory()) {
                        comprobarCrearDirectorio(destino + File.separator + archivo + File.separator);
                        copiarDirectorio(origen + File.separator + archivo + File.separator, destino + File.separator + archivo + File.separator);
                    } else { //Es un archivo
                        copiarArchivo(origen + File.separator + archivo, destino + File.separator + archivo);
                    }
                }
            }
        }
    }

    private static void comprobarCrearDirectorio(String ruta) {
        File directorio = new File(ruta);
        if (!directorio.exists()) {
            directorio.mkdirs();
        }
    }

    private static void copiarArchivo(String sOrigen, String sDestino) {
        try {
            File origen = new File(sOrigen);
            File destino = new File(sDestino);
            InputStream in = new FileInputStream(origen);
            OutputStream out = new FileOutputStream(destino);

            byte[] buf = new byte[1024];
            int len;

            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }

            in.close();
            out.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
