package DB;

import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.DatasetRenderingOrder;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.XYPlot;

import net.sf.jasperreports.engine.JRAbstractChartCustomizer;
import net.sf.jasperreports.engine.JRChart;


public class MultiAxisOrderCustomizer extends JRAbstractChartCustomizer {

	@Override
	public void customize(JFreeChart chart, JRChart jasperChart) {
		Plot plot = chart.getPlot();
		if (plot instanceof CategoryPlot) {
			((CategoryPlot) plot).setDatasetRenderingOrder(DatasetRenderingOrder.FORWARD);
		} else if (plot instanceof XYPlot) {
			((XYPlot) plot).setDatasetRenderingOrder(DatasetRenderingOrder.FORWARD);
		}
	}

}

