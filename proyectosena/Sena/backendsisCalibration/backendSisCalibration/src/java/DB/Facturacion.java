package DB;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author leudi
 */
@WebServlet(urlPatterns = {"/Facturacion"})
public class Facturacion extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    String sql;
    String idusuario = "";
    String usuario = "";
    ResultSet datos = null;
    String iplocal = "";
    String url_archivo = Globales.url_archivo;
    String archivoadjunto = null;
    String correoenvio;
    String clavecorreo;

    public String TablaFacturaImportada(HttpServletRequest request) {
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        sql = "select r.ingreso, ri.factura, ri.id, "
                + "      '<b>' || m.descripcion || '</b><br>' || e.descripcion || '<br><b>' || ma.descripcion || '</b><br>' || mo.descripcion || '<br><b>' || case when r.idintervalo = 0 then 'VER ESPECIFICACIONES' ELSE '(' || desde || ' a ' || hasta || ') ' || medida end || '</b><br>' || r.serie as equipo, "
                + "          to_char(ri.fecha, 'yyyy/MM/dd') as fecha, c.nombrecompleto || ' (<b>' || c.documento || '</b>)' as cliente, c.id as idcliente, ri.subtotal, ri.descuento, ri.suministro, ri.ajuste, "
                + "      '<button class=''btn btn-glow btn-success imprimir'' title=''Imprimir Factura'' type=''button'' onclick=' || chr(34) || 'ImprimirFactura(' || ri.factura || ')' ||chr(34) || '><span data-icon=''&#xe0f7;''></span></button>' || "
                + "      '<button class=''btn btn-glow btn-danger'' title=''Eliminar Factura'' type=''button'' onclick=' || chr(34) || 'EliminarImporFactura(' || ri.id || ',' || r.ingreso || ',' || ri.factura || ')' ||chr(34) || '><span data-icon=''&#xe0d7;''></span></button>' as eliminar "
                + "  from remision_detalle r inner join equipo e on r.idequipo = e.id "
                + "			                    inner join magnitudes m on m.id = e.idmagnitud "
                + "				                    inner join modelos mo on mo.id = r.idmodelo "
                + "				                    inner join marcas ma on ma.id = mo.idmarca "
                + "                          inner join magnitud_intervalos i on i.id = r.idintervalo "
                + "                          inner join importado_factura ri on ri.ingreso = r.ingreso "
                + "                          inner join clientes c on c.id = r.idcliente " + (cliente > 0 ? " WHERE r.idcliente = " + cliente : "")
                + " ORDER BY r.ingreso";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->TablaFacturaImportada");
    }

    public String ConsultarFacturaPen(HttpServletRequest request) {
        int remision = Globales.Validarintnonull(request.getParameter("remision"));
        int cotizacion = Globales.Validarintnonull(request.getParameter("cotizacion"));
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        int ingreso = Globales.Validarintnonull(request.getParameter("ingreso"));
        String fechad = (request.getParameter("fechad"));
        String fechah = (request.getParameter("fechah"));
        int magnitud = Globales.Validarintnonull(request.getParameter("magnitud"));
        String equipo = (request.getParameter("equipo"));
        int marca = Globales.Validarintnonull(request.getParameter("marca"));
        String modelo = (request.getParameter("modelo"));
        String intervalo = (request.getParameter("intervalo"));
        String serie = (request.getParameter("serie"));
        int usuarios = Globales.Validarintnonull(request.getParameter("usuario"));
        String recepcion = (request.getParameter("recepcion"));
        int orden = Globales.Validarintnonull(request.getParameter("orden"));
        {

            String busqueda = "WHERE 1=1 ";
            if (remision > 0) {
                busqueda += " AND r.remision = " + remision;
            } else if (cotizacion > 0) {
                busqueda += " AND r.cotizacion = " + cotizacion;
            } else if (ingreso > 0) {
                busqueda += " AND rd.ingreso = " + ingreso;
            } else {
                busqueda += " AND WHERE to_char(r.fecha,'yyyy-MM-dd') >= '" + fechad + "' and to_char(r.fecha,'yyyy-MM-dd') <= '" + fechah + "'";
                if (cliente > 0) {
                    busqueda += " AND r.idcliente = " + cliente;
                }
                if (ingreso > 0) {
                    busqueda += " AND rd.ingreso = " + ingreso;
                }
                if (!equipo.equals("")) {
                    busqueda += " AND e.descripcion = '" + equipo + "'";
                }
                if (marca > 0) {
                    busqueda += " AND mo.idmarca = " + marca;
                }
                if (!modelo.equals("")) {
                    busqueda += " AND mo.descripcion = '" + modelo + "'";
                }
                if (!intervalo.equals("")) {
                    busqueda += " AND '(' || desde || ' a ' || hasta || ') ' || medida = '" + intervalo + "'";
                }
                if (!serie.trim().equals("")) {
                    busqueda += " AND rd.serie = '" + serie.trim() + "'";
                }
                if (usuarios > 0) {
                    busqueda += " AND r.idusuario = " + usuarios;
                }
                if (recepcion.equals("") || recepcion.isEmpty()) {
                    busqueda += " AND r.recepcion = '" + recepcion + "'";
                }
            }
            sql = "select cd.cantidad, cd.precio, cd.descuento,  (cd.cantidad*cd.precio) - (cd.cantidad*cd.precio*cd.descuento/100) as subtotal, "
                    + "          s.nombre || ' DE ' || e.descripcion || ', MARCA: ' || ma.descripcion || ', MODELO: ' || mo.descripcion || ', SERIE: ' || coalesce(cd.serie,'NA') "
                    + "           || ', RANGO: ' || case when cd.idintervalo = 0 then 'VER ESPECIFICACIONES' ELSE medida || ' ' || desde || ' - ' || hasta end || ', MÉTODO: ' || metodo "
                    + "          || ', PUNTOS ESPECÍFICOS DE CALIBRACION: ' || punto || ', DECLARACIÓN DE CONFORMIDAD: ' || declaracion || ', CERTIFICADO ' || cd.certificado ||  "
                    + "          ' A NOMBRE DE ' || cli.nombrecompleto || ' CON DIRECCION ' || cd.direccion 	|| ' Y CO PROXIMIA CALIBRACION ' || coalesce(proxima,'NA') || "
                    + "          ', TIEMPO DE ENTREGA HASTA ' || entrega || ' DÍAS HÁBILES.'   "
                    + "           as servicio, c.id, cd.ingreso, ce.numero as certificado, c.cotizacion, oc.numero as orden, idserviciodep, r.remision, cd.iva   "
                    + "  from cotizacion c INNER JOIN cotizacion_detalle cd on c.id = cd.idcotizacion "
                    + "                    inner join clientes cli on cli.id = c.idcliente "
                    + "                    inner join remision_detalle rd on rd.ingreso = cd.ingreso  "
                    + "                    inner join remision r on r.id = rd.idremision "
                    + "	                              inner join equipo e on cd.idequipo = e.id "
                    + "			                      inner join magnitudes m on m.id = e.idmagnitud "
                    + "			                      inner join seguridad.rbac_usuario u on u.idusu = cd.idusuario "
                    + "			                      inner join servicio s on s.id = cd.idservicio "
                    + "			                      inner join modelos mo on mo.id = cd.idmodelo   "
                    + "			                      inner join marcas ma on ma.id = mo.idmarca  "
                    + "                     inner join magnitud_intervalos mi on mi.id = cd.idintervalo "
                    + "                     left join orden_compra oc on oc.ingreso = cd.ingreso  "
                    + "                     left join certificados ce on ce.ingreso = cd.ingreso  "
                    + "			                      left join proveedores p on p.id = cd.idproveedor  "
                    + "     where c.idcliente = " + cliente + " and  idserviciodep = 0 and cd.facturado = 0 " + busqueda + "   "
                    + "    union "
                    + "    select cd.cantidad, cd.precio, cd.descuento, (cd.cantidad*cd.precio) - (cd.cantidad*cd.precio*cd.descuento/100) as subtotal,  "
                    + "            s.descripcion || case when cd.observacion is null then '' else ' DETALLE: ' || coalesce(cd.observacion,'NA') end as servicio, "
                    + "        c.id, cd.ingreso, ce.numero as certificado, c.cotizacion, oc.numero as orden, idserviciodep, r.remision, cd.iva   "
                    + "    from cotizacion c INNER JOIN cotizacion_detalle cd on c.id = cd.idcotizacion "
                    + "	                              inner join equipo e on cd.idequipo = e.id "
                    + "                      inner join remision_detalle rd on rd.ingreso = cd.ingreso  "
                    + "                      inner join remision r on r.id = rd.idremision "
                    + "			                      inner join magnitudes m on m.id = e.idmagnitud "
                    + "			                      inner join seguridad.rbac_usuario u on u.idusu = cd.idusuario "
                    + "			                      inner join otros_servicios s on s.id = cd.idservicio "
                    + "			                      inner join modelos mo on mo.id = cd.idmodelo   "
                    + "			                      inner join marcas ma on ma.id = mo.idmarca  "
                    + "                      inner join magnitud_intervalos mi on mi.id = cd.idintervalo "
                    + "                      left join orden_compra oc on oc.ingreso = cd.ingreso  "
                    + "                      left join certificados ce on ce.ingreso = cd.ingreso  "
                    + "			                      left join proveedores p on p.id = cd.idproveedor "
                    + "     where c.idcliente = " + cliente + " and  idserviciodep > 0  and cd.facturado = 0 " + busqueda
                    + "     UNION"
                    + "     select cd.cantidad, cd.precio, cd.descuento, (cd.cantidad*cd.precio) - (cd.cantidad*cd.precio*cd.descuento/100) as subtotal,  "
                    + "            s.descripcion || case when cd.observacion is null then '' else ' DETALLE: ' || coalesce(cd.observacion,'NA') end as servicio, "
                    + "        c.id, cd.ingreso, '' as certificado, c.cotizacion, '' as orden, idserviciodep, 0 AS remision, cd.iva   "
                    + "    from cotizacion c INNER JOIN cotizacion_detalle cd on c.id = cd.idcotizacion "
                    + "	                                inner join otros_servicios s on s.id = cd.idservicio "
                    + "        where c.idcliente =  " + cliente + " and  idserviciodep = 99999 and cd.facturado = 0 and c.cotizacion in  "
                    + "         (SELECT cotizacion  "
                    + "					FROM cotizacion c1 INNER JOIN cotizacion_detalle cd1 on c1.id = cd1.idcotizacion "
                    + "					 				    inner join remision_detalle rd on rd.ingreso = cd1.ingreso "
                    + "									    inner join orden_compra oc on oc.ingreso = cd1.ingreso  "
                    + "                      	    inner join certificados ce on ce.ingreso = cd1.ingreso "
                    + "					where c1.idcliente =  " + cliente + " and idserviciodep = 0 and cd1.facturado = 0)  "
                    + "    ORDER BY ingreso desc, idserviciodep";
            return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ConsultarFacturaPen");

        }
    }

    public String ConsultarFacRadicada(HttpServletRequest request) {
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        int factura = Globales.Validarintnonull(request.getParameter("factura"));
        int radicacion = Globales.Validarintnonull(request.getParameter("radicacion"));
        String fechad = (request.getParameter("fechad"));
        String fechah = (request.getParameter("fechah"));
        {

            String busqueda = "WHERE fr.idusuariorec = 0 and f.estado <> 'Anulado' and f.saldo > 1";
            if (radicacion > 0) {
                busqueda += " AND fr.radicacion = " + radicacion;
            } else if (factura > 0) {
                busqueda += " AND f.factura = " + factura;
            } else {
                busqueda += " AND to_char(fr.fecharad,'yyyy-MM-dd') >= '" + fechad + "' and to_char(fr.fecharad,'yyyy-MM-dd') <= '" + fechah + "'";
                if (cliente > 0) {
                    busqueda += " AND f.idcliente = " + cliente;
                }
            }

            sql = "SELECT fr.id, row_number() OVER(order by f.factura) as fila, radicacion,"
                    + " cli.nombrecompleto || ' (' || cli.documento || ')' AS cliente, f.factura, f.subtotal, f.descuento, f.iva, f.total,"
                    + " to_char(f.fecha,'yyyy/MM/dd') as fecha,"
                    + " to_char(fr.fecharad,'yyyy-MM-dd') as fecharad,"
                    + " to_char(f.vencimiento,'yyyy/MM/dd') as vencimiento, tc.descripcion as tipopago, saldo,"
                    + " to_char(fr.fechareg,'yyyy/MM/dd HH24:MI') || '<br>' || u.nombrecompleto as registro,"
                    + " um.nombrecompleto as mensajero,"
                    + " '<button class=''btn btn-glow btn-success imprimir'' title=''Imprimir Factura'' type=''button'' onclick=' || chr(34) || 'ImprimirFactura(' || f.factura || ')' ||chr(34) || '><span data-icon=''&#xe0f7;''></span></button>' || "
                    + " '<button class=''btn btn-glow btn-danger'' title=''Eliminar Radicacion'' type=''button'' onclick=' || chr(34) || 'EliminarRadicacion(' || fr.id || ',' || f.factura || ',' || cli.nombrecompleto || ')' ||chr(34) || '><span data-icon=''&#xe0d7;''></span></button>' as opciones"
                    + " FROM factura_radicacion fr inner join factura f on f.factura = fr.factura "
                    + "               inner join clientes cli on cli.id = f.idcliente "
                    + "              inner join tipocliente tc on tc.id = f.tipopago::integer"
                    + "               inner join seguridad.rbac_usuario u on u.idusu = fr.idusuario "
                    + "               inner join seguridad.rbac_usuario um on um.idusu = fr.idmensajero " + busqueda + " order by f.factura";
            return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ConsultarFacRadicada");
        }
    }

    public String ConsultarFactura(HttpServletRequest request) {
        int cotizacion = Globales.Validarintnonull(request.getParameter("cotizacion"));
        String orden = request.getParameter("orden");
        int factura = Globales.Validarintnonull(request.getParameter("factura"));
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        int ingreso = Globales.Validarintnonull(request.getParameter("ingreso"));
        String fechad = request.getParameter("fechad");
        String fechah = request.getParameter("fechah");
        int magnitud = Globales.Validarintnonull(request.getParameter("magnitud"));
        String equipo = request.getParameter("equipo");
        int marca = Globales.Validarintnonull(request.getParameter("marca"));
        String modelo = request.getParameter("modelo");
        String intervalo = request.getParameter("intervalo");
        String serie = request.getParameter("serie");
        int usuarios = Globales.Validarintnonull(request.getParameter("usuario"));
        String estado = request.getParameter("estado");

        String busqueda = "WHERE 1=1 ";
        if (factura > 0) {
            busqueda += " AND f.factura = " + factura;
        } else if (!orden.equals("")) {
            busqueda += " AND f.orden = '" + orden + "'";
        } else if (cotizacion > 0) {
            busqueda += " AND fd.cotizacion = '" + cotizacion + "'";
        } else if (ingreso > 0) {
            busqueda += " AND rd.ingreso = " + ingreso;
        } else {
            busqueda += " AND to_char(f.fecha,'yyyy-MM-dd') >= '" + fechad + "' and to_char(f.fecha,'yyyy-MM-dd') <= '" + fechah + "'";
            if (cliente > 0) {
                busqueda += " AND f.idcliente = " + cliente;
            }
            if (magnitud > 0) {
                busqueda += " AND e.idmagnitud = " + magnitud;
            }
            if (!equipo.equals("")) {
                busqueda += " AND e.descripcion = '" + equipo + "'";
            }
            if (marca > 0) {
                busqueda += " AND mo.idmarca = " + marca;
            }
            if (!modelo.equals("")) {
                busqueda += " AND mo.descripcion = '" + modelo + "'";
            }
            if (!intervalo.equals("")) {
                busqueda += " AND '(' || desde || ' a ' || hasta || ') ' || medida = '" + intervalo + "'";
            }
            if (!serie.trim().equals("")) {
                busqueda += " AND rd.serie = '" + serie.trim() + "'";
            }
            if (usuarios > 0) {
                busqueda += " AND f.idusuario = " + usuarios;
            }
            if (!estado.equals("Todos")) {
                busqueda += " AND f.estado = '" + estado + "'";
            }
        }

        sql = "SELECT row_number() OVER(order by f.factura) as fila, "
                + " cli.nombrecompleto || ' (' || cli.documento || ')' AS cliente, factura, f.subtotal, f.descuento, f.iva, f.total,"
                + "   ci.descripcion  as ciudad,"
                + "   f.orden, cli.Telefono || '<br>' || cli.direccion as direccion, cli.email,"
                + "   to_char(f.fecha,'yyyy/MM/dd HH24:MI') as fecha,"
                + "   tiempo(f.vencimiento, now(), 5) as tiempo,"
                + "   to_char(f.vencimiento,'yyyy/MM/dd') as vencimiento,"
                + "   to_char(f.fecharad,'yyyy/MM/dd') as fecharad,"
                + "   to_char(f.fechaenvio,'yyyy/MM/dd HH24:MI')  || '<br>' || ue.nombrecompleto as envio,"
                + "   to_char(f.fechaanula,'yyyy/MM/dd HH24:MI')  || '<br>' || ua.nombrecompleto as anula,"
                + "   u.nombrecompleto AS usuario, tc.descripcion as tipo, diaspago, saldo, f.estado,"
                + "   (select to_char(fr.fechareg,'yyyy/MM/dd HH24:MI')  || '<br>' || urr.nombrecompleto "
                + "       from factura_radicacion fr inner join seguridad.rbac_usuario urr on urr.idusu = fr.idusuario and fr.factura = f.factura) as registrorad,"
                + "   (select to_char(fr.fecharec,'yyyy/MM/dd HH24:MI')  || '<br>' || urc.nombrecompleto "
                + "       from factura_radicacion fr inner join seguridad.rbac_usuario urc on urc.idusu = fr.idusuariorec and fr.factura = f.factura) as recibidorad,"
                + " '<button class=''btn btn-glow btn-success imprimir'' title=''Imprimir Factura'' type=''button'' onclick=' || chr(34) || 'ImprimirFactura(' || f.factura || ')' ||chr(34) || '><span data-icon=''&#xe0f7;''></span></button>' as imprimir"
                + "   FROM factura f inner join factura_datelle fd on f.id = fd.idfactura"
                + "		   inner JOIN remision_detalle rd ON rd.ingreso = fd.ingreso"
                + "		   inner JOIN clientes cli on cli.id = f.idcliente"
                + "		   inner JOIN equipo e ON e.id = rd.idequipo"
                + "		   inner JOIN modelos mo ON mo.id = rd.idmodelo"
                + "		   inner JOIN ciudad ci ON ci.id = cli.idciudad"
                + "  inner join magnitud_intervalos i on i.id = rd.idintervalo  "
                + "		   inner JOIN seguridad.rbac_usuario u on u.idusu = f.idusuario "
                + "		   inner JOIN seguridad.rbac_usuario ua on ua.idusu = f.idusuarioanula "
                + "		   inner JOIN seguridad.rbac_usuario ue on ue.idusu = f.idusuarioenvia"
                + "   inner join tipocliente tc on tc.id = f.tipopago::integer " + busqueda
                + "   group by cli.nombrecompleto, cli.documento, "
                + "           cli.telefono, ci.descripcion,f.factura, f.subtotal, f.descuento, f.iva, f.total, f.orden,"
                + "           cli.direccion, cli.Telefono, cli.email, f.fecha, f.vencimiento, f.fechaenvio, f.fechaanula,"
                + "           ue.nombrecompleto, ua.nombrecompleto, u.nombrecompleto, f.fecharad, tc.descripcion, diaspago, saldo, f.estado";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ConsultarFactura");
    }

    public String ConsultarFacPorRadicar(HttpServletRequest request) {
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        int factura = Globales.Validarintnonull(request.getParameter("factura"));
        String fechad = request.getParameter("fechad");
        String fechah = request.getParameter("fechah");
        {

            String busqueda = "WHERE f.estado <> 'Anulado' and f.saldo >= 1 and fecharad is null and radicado = 0";
            if (factura > 0) {
                busqueda += " AND f.factura = " + factura;
            } else {
                busqueda += " AND to_char(f.fecha,'yyyy-MM-dd') >= '" + fechad + "' and to_char(f.fecha,'yyyy-MM-dd') <= '" + fechah + "'";
                if (cliente > 0) {
                    busqueda += " AND f.idcliente = " + cliente;
                }
            }
            sql = "SELECT row_number() OVER(order by f.factura) as fila, "
                    + "cli.nombrecompleto || ' (' || cli.documento || ')' AS cliente, factura, f.subtotal, f.descuento, f.iva, f.total,"
                    + "to_char(f.fecha,'yyyy/MM/dd') as fecha,"
                    + "to_char(f.vencimiento,'yyyy/MM/dd') as vencimiento,"
                    + "f.reteiva, f.reteica, f.retefuente,saldo, tc.descripcion as tipopago,"
                    + "to_char(f.fechareg,'yyyy/MM/dd HH24:MI') || '<br>' || u.nombrecompleto as registro,"
                    + "'<button class=''btn btn-glow btn-success imprimir'' title=''Imprimir Factura'' type=''button'' onclick=' || chr(34) || 'ImprimirFactura(' || f.factura || ')' ||chr(34) || '><span data-icon=''&#xe0f7;''></span></button>' as imprimir"
                    + " FROM factura f inner join clientes cli on cli.id = f.idcliente "
                    + "               inner join tipocliente tc on tc.id = f.tipopago::integer"
                    + "               inner join seguridad.rbac_usuario u on u.idusu = f.idusuario " + busqueda + " order by factura";
            return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ConsultarFacPorRadicar");
        }
    }

    public String ConsultarIngresoFac(HttpServletRequest request) {
        int remision = Globales.Validarintnonull(request.getParameter("remision"));
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        String ingresos = request.getParameter("ingresos");
        String estado = request.getParameter("estado");
        String fechad = request.getParameter("fechad");
        String fechah = request.getParameter("fechah");
        int magnitud = Globales.Validarintnonull(request.getParameter("magnitud"));
        int equipo = Globales.Validarintnonull(request.getParameter("equipo"));
        int marca = Globales.Validarintnonull(request.getParameter("marca"));
        int modelo = Globales.Validarintnonull(request.getParameter("modelo"));
        int intervalo = Globales.Validarintnonull(request.getParameter("intervalo"));
        String serie = request.getParameter("serie");
        int usuarios = Globales.Validarintnonull(request.getParameter("usuario"));
        String recepcion = request.getParameter("recepcion");
        {
            int ingreso = Globales.Validarintnonull(ingresos);
            String[] a_ingreso = ingresos.split("-");

            String busqueda = "WHERE rd.facturado = 0 ";
            if (remision > 0) {
                busqueda += " AND r.remision = " + remision;
            } else if (ingreso > 0) {
                busqueda += " AND rd.ingreso = " + ingreso;
            } else if (Globales.Validarintnonull(a_ingreso[0]) > 0 && Globales.Validarintnonull(a_ingreso[1]) > 0) {
                busqueda += " AND rd.ingreso >= " + a_ingreso[0] + " and rd.ingreso <= " + a_ingreso[1];
            } else {
                busqueda += " AND to_char(r.fecha,'yyyy-MM-dd') >= '" + fechad + "' and to_char(r.fecha,'yyyy-MM-dd') <= '" + fechah + "'";
                if (cliente > 0) {
                    busqueda += " AND r.idcliente = " + cliente;
                }
                if (magnitud > 0) {
                    busqueda += " AND e.idmagnitud = " + magnitud;
                }
                if (equipo > 0) {
                    busqueda += " AND rd.idequipo = " + equipo;
                }
                if (marca > 0) {
                    busqueda += " AND mo.idmarca = " + marca;
                }
                if (modelo > 0) {
                    busqueda += " AND rd.idmodelo = " + modelo;
                }
                if (intervalo > 0) {
                    busqueda += " AND rd.idintervalo = " + intervalo;
                }
                if (!serie.trim().equals("")) {
                    busqueda += " AND rd.serie ilike '%" + serie.trim() + "%'";
                }
                if (usuarios > 0) {
                    busqueda += " AND r.idusuario = " + usuarios;
                }
                if (!recepcion.equals("") || !recepcion.isEmpty()) {
                    busqueda += " AND r.recepcion = '" + recepcion + "'";
                }
            }
            sql = "select r.id, r.remision, rd.ingreso, r.cotizacion, r.estado, rd.observacion, to_char(fecha, 'yyyy/MM/dd HH24:MI') as fecha, u.nombrecompleto as usuario, r.recepcion,"
                    + "  '<b>' || c.nombrecompleto || '</b><br>' || c.documento as cliente,  "
                    + "  '<b>' || cs.nombre || '</b><br>' || cs.direccion || ', ' || ci.descripcion || ' (' ||d.descripcion ||'), ' || pa.descripcion as direccion, "
                    + "  '<b>' || cc.nombres || '</b><br>' || coalesce(cc.telefonos,'') || ' / ' || coalesce(cc.fax,'') || '<br>' || cc.email as contacto,"
                    + "  e.descripcion as equipo, m.descripcion as magnitud, ma.descripcion as marca, mo.descripcion as modelo, serie, "
                    + "  case when rd.idintervalo = 0 then 'VER ESPECIFICACIONES' ELSE '(' || i.desde || ' a ' || i.hasta || ') ' || i.medida end as rango1,"
                    + "  case when rd.idintervalo2 = 0 then '' ELSE '(' || i2.desde || ' a ' || i2.hasta || ') ' || i2.medida end as rango2,"
                    + "  case when rd.idintervalo3 = 0 then '' ELSE '(' || i3.desde || ' a ' || i3.hasta || ') ' || i3.medida end as rango3"
                    + "  from remision r inner join seguridad.rbac_usuario u on r.idusuario = u.idusu"
                    + "	                    inner join clientes c on c.id = r.idcliente"
                    + "	                    inner join clientes_sede cs on cs.id = r.idsede"
                    + "	                    inner join clientes_contac cc on cc.id = r.idcontacto"
                    + "	                    inner join ciudad ci on ci.id = cs.idciudad"
                    + "	                    inner join departamento d on d.id = ci.iddepartamento"
                    + "	                    inner join pais pa on pa.id = d.idpais"
                    + "	                    inner join remision_detalle rd on rd.idremision = r.id "
                    + "            inner join equipo e on e.id = rd.idequipo"
                    + "            inner join magnitudes m on m.id = e.idmagnitud"
                    + "	                    inner join modelos mo on mo.id = rd.idmodelo "
                    + "            inner join marcas ma on ma.id = mo.idmarca"
                    + "            inner join magnitud_intervalos i on i.id = rd.idintervalo "
                    + "            inner join magnitud_intervalos i2 on i2.id = rd.idintervalo2"
                    + "            inner join magnitud_intervalos i3 on i3.id = rd.idintervalo3 " + busqueda + " "
                    + "ORDER BY rd.ingreso ";
            return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ConsultarIngresoFac");
        }
    }

    public String ConsultarRadicada(HttpServletRequest request) {
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        int factura = Globales.Validarintnonull(request.getParameter("factura"));
        int radicacion = Globales.Validarintnonull(request.getParameter("radicacion"));
        String fechad = request.getParameter("fechad");
        String fechah = request.getParameter("fechah");
        String recibido = request.getParameter("recibido");

        String busqueda = "WHERE 1 = 1";
        if (radicacion > 0) {
            busqueda += " AND fr.radicacion = " + radicacion;
        } else {
            if (factura > 0) {
                busqueda += " AND f.factura = " + factura;
            } else {
                busqueda += " AND to_char(fr.fecharad,'yyyy-MM-dd') >= '" + fechad + "' and to_char(fr.fecharad,'yyyy-MM-dd') <= '" + fechah + "'";
                if (cliente > 0) {
                    busqueda += " AND f.idcliente = " + cliente;
                }
            }
            if (!recibido.equals("")) {
                if (recibido.equals("1")) {
                    busqueda += " And fr.idusuariorec > 0";
                } else {
                    busqueda += " And fr.idusuariorec = 0";
                }
            }

        }

        sql = "SELECT fr.id, row_number() OVER(order by f.factura) as fila, radicacion,"
                + "       cli.nombrecompleto || ' (' || cli.documento || ')' AS cliente, f.factura, f.subtotal, f.descuento, f.iva, f.total,"
                + "       to_char(f.fecha,'yyyy/MM/dd') as fecha,"
                + "       to_char(fr.fecharad,'yyyy-MM-dd') as fecharad,"
                + "       to_char(f.vencimiento,'yyyy/MM/dd') as vencimiento, tc.descripcion as tipopago, saldo,"
                + "       to_char(fr.fechareg,'yyyy/MM/dd HH24:MI') || '<br>' || u.nombrecompleto as registro,"
                + "               to_char(fr.fecharec,'yyyy/MM/dd HH24:MI') || '<br>' || u.nombrecompleto as recibido, "
                + "       um.nombrecompleto as mensajero,"
                + "               '<button class=''btn btn-glow btn-success imprimir'' title=''Imprimir Factura'' type=''button'' onclick=' || chr(34) || 'ImprimirFactura(' || f.factura || ')' ||chr(34) || '><span data-icon=''&#xe0f7;''></span></button>' || "
                + "       '<button class=''btn btn-glow btn-info imprimir'' title=''Imprimir Radicación'' type=''button'' onclick=' || chr(34) || 'ImprimirRadicacion(' || fr.radicacion || ')' ||chr(34) || '><span data-icon=''&#xe0f7;''></span></button>'as opciones"
                + " FROM factura_radicacion fr inner join factura f on f.factura = fr.factura "
                + "               inner join clientes cli on cli.id = f.idcliente "
                + "              inner join tipocliente tc on tc.id = f.tipopago::integer"
                + "              inner join seguridad.rbac_usuario u on u.idusu = fr.idusuario "
                + "	   inner join seguridad.rbac_usuario ur on ur.idusu = fr.idusuariorec  "
                + "           inner join seguridad.rbac_usuario um on um.idusu = fr.idmensajero " + busqueda + " order by f.factura";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ConsultarRadicada");
    }

    public String GuardaTempFactura(HttpServletRequest request) {
        int ingreso = Globales.Validarintnonull(request.getParameter("ingreso"));
        String orden = request.getParameter("orden");
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        int idfactura = Globales.Validarintnonull(request.getParameter("idfactura"));

        try {
            if (idfactura > 0) {
                sql = "SELECT estado FROM factura where id = " + idfactura;
                String estado = Globales.ObtenerUnValor(sql);
                if (!estado.equals("Facturado") && !estado.equals("Temporal")) {
                    return "1|No se puede editar una factura con estado " + estado;
                }
            }

            int cuenta = Globales.Validarintnonull(Globales.ObtenerUnValor("select idcuecontable from servicio where id=16"));

            sql = "select 'CALIBRACIÓN DE ' || e.descripcion || ', <b>MARCA:</b> ' || ma.descripcion || ', <b>MODELO:</b> ' || mo.descripcion || ', <b>SERIE:</b> ' || coalesce(rd.serie,'NA')"
                    + "              || ', <b>INTERVALO:</b> ' || case when rd.idintervalo = 0 then 'VER ESPECIFICACIONES' ELSE medida || ' ' || desde || ' - ' || hasta end as servicio, rd.facturado, idcentro"
                    + "from remision_detalle rd inner join equipo e on rd.idequipo = e.id"
                    + "			                     inner join magnitudes m on m.id = e.idmagnitud"
                    + "			                     inner join modelos mo on mo.id = rd.idmodelo  "
                    + "			                     inner join marcas ma on ma.id = mo.idmarca "
                    + "                     inner join magnitud_intervalos mi on mi.id = rd.idintervalo"
                    + " WHERE rd.ingreso = " + ingreso;
            datos = Globales.Obtenerdatos(sql, this.getClass() + "-->GuardaTempFactura", 1);
            String descripcion = datos.getString(1);
            int facturado = Globales.Validarintnonull(datos.getString(2));
            int idcentro = Globales.Validarintnonull(datos.getString("idcentro"));

            if (facturado != 0) {
                return "1|Este ingreso ya se encuentra facturado";
            }

            sql = "INSERT INTO factura_temporal(idfactura, idcliente, ingreso, descripcion, cotizacion, orden, certificado, precio, descuento, cantidad, subtotal, idcentro, idservicio, idcuecontable)"
                    + "  VALUES(" + idfactura + "," + cliente + "," + ingreso + ",'" + descripcion + "',0,'" + orden + "','',0,0,1,0," + idcentro + ",16," + cuenta + ");";
            sql += "UPDATE remision_detalle SET facturado = -1 where ingreso = " + ingreso + ";";

            /*if (idfactura > 0)
                {
                    sql = @"INSERT INTO factura_datelle(idfactura, ingreso, descripcion, cotizacion, orden, certificado, precio, descuento, cantidad, subtotal)
                        VALUES(" + idfactura + "," + ingreso + ",'" + descripcion + "',0,'" + orden + "','',0,0,1,0);";
                    sql += "UPDATE factura SET estado = 'Temporal' where id = " + idfactura + ";";
                }*/
            if (Globales.DatosAuditoria(sql, "FACTURACION", "GUARDAR TEMPORAL", idusuario, iplocal, this.getClass() + "-->GuardaTempFactura")) {
                return "0|Ingreso agregado con éxito";
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }

        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String FacturasAnuladas(HttpServletRequest request) {
        int dias = Globales.Validarintnonull(request.getParameter("dias"));
        String fechad = request.getParameter("fechad");
        String fechah = request.getParameter("fechah");
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));

        {

            String Busqueda2 = "";
            if (dias > 0) {
                Date fechaf = new Date();
                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(fechaf);
                calendar.add(Calendar.DAY_OF_YEAR, (dias * -1));
                Date fechai = calendar.getTime();
                fechad = dateFormat.format(fechai);
                fechah = dateFormat.format(fechaf);
            }

            if (cliente > 0) {
                Busqueda2 += " and idcliente = " + cliente;
            }

            sql = "SELECT f.subtotal-f.descuento AS anulado, factura, orden, to_char(fechaanula,'dd/MM/yyyy HH24:MI') as fechaanula,"
                    + "      to_char(fecha,'dd/MM/yyyy') as fecha, c.nombrecompleto as cliente, ua.nombrecompleto as usuario, obseranula,"
                    + "          f.subtotal, f.descuento, f.iva, f.reteica, f.reteiva, f.retefuente, f.total"
                    + "      FROM factura f inner join clientes c on c.id = f.idcliente"
                    + "                     inner join seguridad.rbac_usuario ua on idusuarioanula = ua.idusu"
                    + "      where f.estado = 'Anulado' and to_char(fechaanula,'yyyy-MM-dd') >= '" + fechad + "' and to_char(fechaanula,'yyyy-MM-dd') <= '" + fechah + "' and to_char(fecha,'yyyy-MM-dd') < '" + fechad + "' " + Busqueda2;

            return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->FacturasAnuladas");
        }
    }

    public String RemisionNoFacturado(HttpServletRequest request) {
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        String fechad = request.getParameter("fechad");
        String fechah = request.getParameter("fechah");
        {

            String busqueda = "where rd.id > 0 and rd.ingreso > 0 and r.remision > 0";
            if (cliente > 0) {
                busqueda += " and c.id = " + cliente;
            }
            if (!fechad.equals("")) {
                busqueda += " and to_char(r.fecha,'yyyy-MM-dd') >= '" + fechad + "'";
            }
            if (!fechah.equals("")) {
                busqueda += " and to_char(r.fecha,'yyyy-MM-dd') <= '" + fechah + "'";
            }

            sql = "select remision,  c.nombrecompleto as cliente, to_char(r.fecha,'yyyy/MM/dd HH24:MI') || '<br>' || u.nomusu as registro, \n"
                    + "          count(distinct rd.ingreso) as cantingreso,"
                    + "          (select count(distinct rd2.ingreso) from remision_detalle rd2 where rd2.idremision = r.id and (ip.certificado = 1 or ip.informetecnico > 0 or noautorizado = 1 or informetercerizado <> '0' or convenio = 1)) listos, \n"
                    + "          (select count(distinct rd2.ingreso) from remision_detalle rd2 where rd2.idremision = r.id and (ip.certificado = 0 and ip.informetecnico = 0 and noautorizado = 0 and informetercerizado = '0' and convenio = 0)) pendiente, \n"
                    + "          (select count(distinct rd2.ingreso) from remision_detalle rd2 where rd2.idremision = r.id and facturado = 0 ) nofacturado \n"
                    + "  from remision r inner join remision_detalle rd on rd.idremision = r.id \n"
                    + "                  inner join ingreso_plantilla ip on ip.ingreso = rd.ingreso \n"
                    + "                  inner join clientes c on c.id = r.idcliente \n"
                    + "                  inner join seguridad.rbac_usuario u on u.idusu = r.idusuario " + busqueda + " \n"
                    + "  group by remision, ip.certificado, ip.informetecnico,  c.nombrecompleto, r.fecha, u.nomusu, registro, r.id, idremision, noautorizado  \n"
                    + "  having count(distinct rd.ingreso) <> (select count(distinct rd2.ingreso) from remision_detalle rd2 where rd2.idremision = r.id and facturado = 0) \n"
                    + "  order by remision";
            return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->RemisionNoFacturado");
        }
    }

    public String ClientesIngresos(HttpServletRequest request) {
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        sql = "SELECT ingreso, ingreso FROM remision_detalle WHERE idcliente = " + cliente + " and ingreso not in (select ingreso from importado_factura) order by ingreso";
        String resultado = Globales.ObtenerCombo(sql, 1, 0, 0);
        if (resultado.trim().equals("")) {
            resultado = "XX";
        }
        return resultado;
    }

    public String BuscarFactura(HttpServletRequest request) {
        int factura = Globales.Validarintnonull(request.getParameter("factura"));
        {

            try {
                sql = "SELECT idcliente, orden, estado, id FROM factura where factura = " + factura;
                datos = Globales.Obtenerdatos(sql, this.getClass() + "-->BuscarFactura", 1);
                if (!datos.next()) {
                    return "[]";
                }

                String estado = datos.getString("estado");
                String cliente = datos.getString("idcliente");
                String orden = datos.getString("orden");
                String id = datos.getString("id");
                String sql2 = "";
                if (estado.equals("Facturado") || estado.equals("Temporal")) {
                    sql = "update remision_detalle rd set facturado = 0 from factura_temporal ft where ft.ingreso = rd.ingreso and idcliente = " + cliente + " and orden = '" + orden + "' and idfactura = " + id + ";";
                    sql = "DELETE FROM factura_temporal where idcliente = " + cliente + " and orden = '" + orden + "' and idfactura = " + id + ";";
                    sql += "INSERT INTO factura_temporal(idfactura, idcliente, ingreso, descripcion, cotizacion, orden, certificado, precio, descuento, cantidad, subtotal, iva, idcentro, idservicio, idcuecontable, idarticulo)"
                            + " SELECT " + id + "," + cliente + ", ingreso, descripcion, cotizacion, orden, certificado, precio, descuento, cantidad, subtotal, iva, idcentro, idservicio, idcuecontable, idarticulo"
                            + " FROM factura_datelle where idfactura = " + id + " order by id";
                    Globales.Obtenerdatos(sql, this.getClass() + "-->BuscarFactura", 1);
                }

                sql = "select factura, f.id, f.idcliente,  c.nombrecompleto as cliente, c.documento, u.nombrecompleto as usuario, f.estado, to_char(f.fechareg,'dd/MM/yyyy HH24:MI') as fechareg,"
                        + "     c.direccion, c.telefono || ' ' || coalesce(c.celular) as telefono, c.email, f.orden, ci.descripcion || '/' || d.descripcion as ciudad, c.plazopago,  "
                        + "     tp.descripcion as precio, tc.descripcion as tipo, to_char(f.vencimiento,'dd/MM/yyyy') as vencimiento, saldo, observacion, to_char(f.fecha,'yyyy-MM-dd') as fecha, 2 as tipo,"
                        + "   case when f.estado = 'Anulado' then 'Anulado por ' || ua.nombrecompleto || ' <b>fecha:</b> ' || to_char(f.fechaanula,'dd/MM/yyyy') || ' <b>observación:</b> ' || f.obseranula else '' end as anulado,"
                        + "   c.reteiva as idreteiva, c.retefuente as idretefuente, c.reteica as idreteica, f.porreteiva, f.porretefuente, f.porreteica,"
                        + "   f.reteiva, f.retefuente, f.reteica"
                        + " from factura f inner join clientes c on c.id = f.idcliente"
                        + "                inner join seguridad.rbac_usuario u on u.idusu = f.idusuario"
                        + "                inner join ciudad ci on ci.id = c.idciudad"
                        + "                inner join departamento d on d.id = ci.iddepartamento"
                        + "                inner join tipocliente tc on tc.id = c.idtipcli"
                        + "                inner join tablaprecios tp on tp.id = c.tablaprecio"
                        + "            inner join seguridad.rbac_usuario ua on ua.idusu = f.idusuarioanula"
                        + " WHERE factura = " + factura;
                if (estado.equals("Facturado") || estado.equals("Temporal")) {
                    sql2 = "SELECT *, (select max(remision) FROM remision r INNER JOIN remision_detalle rd on rd.idremision = r.id where rd.ingreso = f.ingreso) as remision"
                            + " FROM  factura_temporal f WHERE  f.idfactura = " + id + " order by f.id";
                } else {
                    sql2 = "SELECT *, (select max(remision) FROM remision r INNER JOIN remision_detalle rd on rd.idremision = r.id where rd.ingreso = f.ingreso) as remision "
                            + " FROM factura_datelle f WHERE idfactura = " + id + " order by id";
                }
                return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->BuscarFactura") + "|" + Globales.ObtenerDatosJSon(sql2, this.getClass() + "-->BuscarFactura");
            } catch (Exception ex) {
                Globales.GuardarLogger(ex.getMessage(), this.getClass() + "-->BuscarFactura");
                return "[]";
            }
        }
    }

    public String ResumenGraficaFactura(HttpServletRequest request) {
        int dias = Globales.Validarintnonull(request.getParameter("dias"));
        String fechad = request.getParameter("fechad");
        String fechah = request.getParameter("fechah");
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        int asesor = Globales.Validarintnonull(request.getParameter("asesor"));
        int departamento = Globales.Validarintnonull(request.getParameter("departamento"));
        int ciudad = Globales.Validarintnonull(request.getParameter("ciudad"));
        String estado = request.getParameter("estado");
        int usuarios = Globales.Validarintnonull(request.getParameter("usuario"));
        int factura = Globales.Validarintnonull(request.getParameter("factura"));
        String orden = request.getParameter("orden");
        int tipo = Globales.Validarintnonull(request.getParameter("tipo"));
        {

            String Busqueda = " where 1 = 1 ";
            String Busqueda2 = "";

            if (dias > 0) {
                Date fechaf = new Date();
                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(fechaf);
                calendar.add(Calendar.DAY_OF_YEAR, (dias * -1));
                Date fechai = calendar.getTime();
                fechad = dateFormat.format(fechai);
                fechah = dateFormat.format(fechaf);
            }

            String titulo = "Fecha Inicial " + fechad + " Fecha Final " + fechah;

            if (factura != 0) {
                Busqueda += " AND f.factura = " + factura;
                titulo += ", Facura: " + factura;
            } else if (!orden.equals("")) {
                Busqueda += " AND f.orden = '" + orden + "'";
                titulo += ", Orden: " + orden;
            } else {
                if (cliente > 0) {
                    Busqueda += " and f.idcliente = " + cliente;
                    Busqueda2 += " and idcliente = " + cliente;
                }

                if (!estado.equals("T")) {
                    if (estado.equals("Vencida")) {
                        Busqueda += " and extract(days from (now()-vencimiento)) > 0";
                    } else {
                        Busqueda += " and f.estado = '" + estado + "'";
                    }

                    titulo += ", estado: " + estado;
                }

                if (tipo != 0) {
                    Busqueda += " and tc.id = " + tipo;
                }

                if (!fechad.trim().equals("")) {
                    Busqueda += " and to_char(f.fecha,'yyyy-MM-dd') >= '" + fechad + "'";
                }
                if (!fechah.trim().equals("")) {
                    Busqueda += " and to_char(f.fecha,'yyyy-MM-dd') <= '" + fechah + "'";
                }
                if (asesor > 0) {
                    Busqueda += " AND cli.asesor = " + asesor;
                }

                if (ciudad > 0) {
                    Busqueda += " AND ci.id = " + ciudad;
                }

                if (departamento > 0) {
                    Busqueda += " AND d.id = " + departamento;
                }

                if (usuarios > 0) {
                    Busqueda += " AND cf.idusuario = " + usuarios;
                }
            }

            sql = "SELECT sum(f.subtotal) as subtotal, sum(f.descuento) as descuento, sum(f.iva) as iva, sum(f.saldo) as saldo, to_char(f.fecha,'yyyy/MM') as fecha,"
                    + "         (select sum(f1.subtotal-f1.descuento) FROM factura f1 "
                    + "         where f1.estado = 'Anulado' and to_char(f1.fecha,'yyyy/MM') = max(to_char(f.fecha,'yyyy/MM'))) as anulado,"
                    + "         (select sum(f1.iva) FROM factura f1 "
                    + "         where f1.estado = 'Anulado' and to_char(f1.fecha,'yyyy/MM') = max(to_char(f.fecha,'yyyy/MM'))) as ivaanulado"
                    + " FROM factura f inner JOIN clientes cli on cli.id = f.idcliente"
                    + "	                   inner JOIN ciudad ci ON ci.id = cli.idciudad"
                    + "	                   inner JOIN departamento d ON d.id = ci.iddepartamento " + Busqueda + ""
                    + "   group by to_char(f.fecha,'yyyy/MM')  ORDER BY 5";
            return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ResumenGraficaFactura");
        }
    }

    public String EliminarTempFactura(HttpServletRequest request) {
        int id = Globales.Validarintnonull(request.getParameter("id"));
        int ingreso = Globales.Validarintnonull(request.getParameter("ingreso"));
        {

            try {

                if (Globales.PermisosSistemas("FACTURA ELIMINAR TEMPORAL", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }

                sql = "DELETE FROM factura_temporal WHERE id = " + id + ";";
                sql += "UPDATE remision_detalle SET facturado = 0 where ingreso = " + ingreso + ";";
                if (Globales.DatosAuditoria(sql, "FACTURA", "ELIMINAR TEMPORAL", idusuario, iplocal, this.getClass() + "-->BuscarFactura")) {
                    return "0|Ingreso eliminado con éxito";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }

            } catch (Exception ex) {

                return "1|" + ex.getMessage();
            }

        }
    }

    public String TablaFacturaSinRelacionar(HttpServletRequest request) {
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        String fechad = request.getParameter("fechad");
        String fechah = request.getParameter("fechah");

        {

            String busqueda = "where fd.ingreso = 0 AND (case when f.estado <> 'Anulado' then 1 else case when to_char(f.fechaanula,'yyyy-MM') > to_char(f.fecha,'yyyy-MM') then 1 else 0 end end) = 1 and f.factura not in (select factura from importado_factura)";
            if (cliente > 0) {
                busqueda += " and c.id = " + cliente;
            }
            if (!fechad.equals("")) {
                busqueda += " and to_char(f.fecha,'yyyy-MM-dd') >= '" + fechad + "'";
            }
            if (!fechah.equals("")) {
                busqueda += " and to_char(f.fecha,'yyyy-MM-dd') <= '" + fechah + "'";
            }
            sql = "select '<a href=''javascript:ImprimirFactura(' || factura || ')''>' || factura || '</a>' as factura,  to_char(f.fecha,'yyyy/MM/dd'), f.subtotal, f.descuento, f.iva, f.reteica + f.reteiva + f.retefuente as retenciones,  f.total,"
                    + "         c.nombrecompleto as cliente, to_char(f.fechareg,'yyyy/MM/dd HH24:MI') || '<br>' || u.nomusu as registro"
                    + " from factura_datelle fd inner join factura f on f.id = fd.idfactura"
                    + "                                inner join clientes c on c.id = f.idcliente"
                    + "                                inner join seguridad.rbac_usuario u on u.idusu = f.idusuario " + busqueda
                    + "    group by factura, f.fecha, f.fechareg, u.nomusu, c.nombrecompleto, f.subtotal, f.descuento, f.iva, f.reteica,"
                    + "                 f.reteiva, f.retefuente,  f.total"
                    + "  order by factura";
            return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->TablaFacturaSinRelacionar");
        }
    }

    public String Contabilizar_Factura(HttpServletRequest request) {
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        double subtotal = Globales.ValidardoublenoNull(request.getParameter("subtotal"));
        double total = Globales.ValidardoublenoNull(request.getParameter("total"));
        double iva = Globales.ValidardoublenoNull(request.getParameter("iva"));
        double reteiva = Globales.ValidardoublenoNull(request.getParameter("reteiva"));
        double retefuente = Globales.ValidardoublenoNull(request.getParameter("retefuente"));
        double reteica = Globales.ValidardoublenoNull(request.getParameter("reteica"));
        String a_cuenta = request.getParameter("a_cuenta");
        String a_monto = request.getParameter("a_monto");
        String a_centro = request.getParameter("a_centro");
        int buscar = Globales.Validarintnonull(request.getParameter("buscar"));
        int idreteiva = Globales.Validarintnonull(request.getParameter("idreteiva"));
        int idretefuente = Globales.Validarintnonull(request.getParameter("idretefuente"));
        int idreteica = Globales.Validarintnonull(request.getParameter("idreteica"));
        {

            try {

                sql = "SELECT _idcuenta, _cuenta, _concepto, sum(_debito) as _debito, sum(_credito) as _credito, _error, _mensaje, _idcentro, _centrocosto "
                        + "  FROM contabilidad.contabilizacion_factura(" + cliente + "," + subtotal + "," + total + "," + iva + "," + reteiva + ","
                        + retefuente + "," + reteica + "," + idreteiva + "," + idretefuente + "," + idreteica + "," + a_cuenta + "," + a_monto + "," + a_centro + "," + idusuario + "," + buscar + ")"
                        + "group by _idcuenta, _cuenta, _orden, _error, _mensaje, _idcentro, _centrocosto, _concepto order by _orden";

                return "0||" + Globales.ObtenerDatosJSon(sql, this.getClass() + "-->Contabilizar_Factura");
            } catch (Exception ex) {
                return "1||" + ex.getMessage();
            }
        }
    }

    public String AnularContabilizacion(HttpServletRequest request) {
        int factura = Globales.Validarintnonull(request.getParameter("factura"));
        {

            try {
                sql = "SELECT estado, idcomprobante, to_char(fecha,'yyyy-MM') as fecha FROM factura where factura = " + factura;
                datos = Globales.Obtenerdatos(sql, this.getClass() + "-->AnularContabilizacion", 1);
                datos.next();
                String estado = datos.getString("estado").trim();
                String fecha = datos.getString("fecha").trim();
                Date fechaactual = new Date();
                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM");

                int comprobante = Globales.Validarintnonull(datos.getString("idcomprobante").trim());

                if (!estado.equals("Contabilizado")) {
                    return "1|No se anular un comprobante contable de una factura en estado " + estado;
                }

                if (Globales.PermisosSistemas("FACTURA CONTABILIZACION ANULAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }

                if (!fecha.equals(dateFormat.format(fechaactual))) {
                    return "1|No se puede anular un comprobante que no pertenezca al mes actual";
                }

                sql = "update factura set estado = 'Facturado',idusuarioconta=0, fechaconta=null, idcomprobante=0 where factura = " + factura + "";
                sql += "update contabilidad.comprobante set estado = 'Anulado', idusuarioanula=" + idusuario + ", fechaanula=now()  where id = " + comprobante;
                if (Globales.DatosAuditoria(sql, "FACTURA", "CONTABILIZACION ELIMINAR", idusuario, iplocal, this.getClass() + "-->GuardaTempFactura")) {
                    return "0|Contabilización de factura anulado con éxito";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            } catch (Exception ex) {
                return "1|" + ex.getMessage();
            }

        }
    }

    public String BuscarProveedor(HttpServletRequest request) {
        String documento = request.getParameter("documento");
        {
            try {
                //ActulizarImagenes();

                String Busqueda = "WHERE documento = '" + documento + "' ";

                sql = "select pr.*, ci.descripcion as ciudad, d.descripcion as departamento, riva.porcentaje as porreteiva, rica.porcentaje as porreteica,"
                        + "      rfuente.porcentaje as porretefuente"
                        + "  from proveedores pr inner join ciudad ci on ci.id = pr.idciudad"
                        + "            inner join departamento d on d.id = ci.iddepartamento"
                        + "            inner join contabilidad.retenciones riva on riva.id = reteiva"
                        + "            inner join contabilidad.retenciones rica on rica.id = reteica"
                        + "            inner join contabilidad.retenciones rfuente on rfuente.id = retefuente " + Busqueda + " ORDER BY 2 ";

                datos = Globales.Obtenerdatos(sql, this.getClass() + "-->BuscarProveedor", 1);

                if (datos.next()) {

                    String sql2 = "select DISTINCT id, nombrecompleto || ' (' || documento || ')' as cliente"
                            + "  from clientes WHERE id > 0"
                            + "  ORDER BY 2";
                    return "0|" + Globales.ObtenerDatosJSon(sql, this.getClass() + "-->BuscarProveedor") + "|" + Globales.ObtenerCombo(sql2, 5, 0, 0);
                } else {
                    return "99|";
                }
            } catch (Exception ex) {
                Globales.GuardarLogger(ex.getMessage(), this.getClass() + "-->BuscarProveedor");
                return "99|";
            }
        }
    }

    public String AnularOrden(HttpServletRequest request) {
        int orden = Globales.Validarintnonull(request.getParameter("factura"));
        String observaciones = request.getParameter("observaciones");
        {

            try {

                if (Globales.PermisosSistemas("ORDEN DE COMPRA ANULAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }

                sql = "SELECT * FROM anular_ordencompra(" + idusuario + "," + orden + ",'" + observaciones + "')";
                datos = Globales.Obtenerdatos(sql, this.getClass() + "-->AnularOrden", 1);
                datos.next();
                if (!datos.getString("_numerror").equals("0")) {
                    return "1|" + datos.getString("_mensaje");
                } else {
                    return "0|Orden de compra anulada con éxito";
                }
            } catch (Exception ex) {
                return "9|" + ex.getMessage();
            }
        }
    }

    public String ReemplazarOrden(HttpServletRequest request) {
        int orden = Globales.Validarintnonull(request.getParameter("orden"));
        String observaciones = request.getParameter("observaciones");
        {

            try {

                if (Globales.PermisosSistemas("ORDEN DE COMPRA REEMPLAZAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }

                sql = "SELECT * FROM reemplazar_ordencompra(" + idusuario + "," + orden + ",'" + observaciones + "')";
                datos = Globales.Obtenerdatos(sql, this.getClass() + "-->ReemplazarOrden", 1);
                if (!datos.getString("_numerror").equals("0")) {
                    return "1|" + datos.getString("_mensaje");
                } else {
                    return "0|Orden de compra reemplazada con éxito|" + datos.getString("_ordenreem");
                }
            } catch (Exception ex) {
                return "9|" + ex.getMessage();
            }
        }
    }

    public String BuscarOrdenTercero(HttpServletRequest request) {
        int orden = Globales.Validarintnonull(request.getParameter("orden"));
        {

            sql = "select o.*, p.documento, idproveedor "
                    + "  from ordencompra_tercero o inner join proveedores p on o.idproveedor = p.id"
                    + "  where orden = " + orden;
            return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->BuscarOrdenTercero");
        }
    }

    /*   public String EnvioOrdenCompra(HttpServletRequest request) {

        String principal = request.getParameter("principal");
        String e_email = request.getParameter("e_mail");
        String archivo = request.getParameter("archivo");
        int id = Globales.Validarintnonull(request.getParameter("id"));
        String observacion = request.getParameter("observacion");
        int error = 0;
        try {
            sql = "SELECT orden, oc.estado, p.nombrecompleto as proveedor, u.nombrecompleto as usuario, u.cargo"
                    + "  from proveedores p inner join ordencompra_tercero oc on oc.idproveedor = p.id "
                    + "                 INNER JOIN seguridad.rbac_usuario u on u.idusu = oc.idusuario"
                    + " where oc.id = " + id;
            datos = Globales.Obtenerdatos(sql, this.getClass() + "-->EnvioOrdenCompra", 1);
            String proveedor = datos.getString("proveedor");
            String nomusuario = datos.getString("usuario");
            String cargo = datos.getString("cargo");
            String orden = datos.getString("orden");
            String estado = datos.getString("estado");

            if (estado.equals("Anulado")) {
                return "1|No se puede enviar una órden de compra en estado anulado";
            }

            e_email += ";" + correoenvio;
            String[] email = e_email.split(";");

            String correoemp = correoenvio;

            int hora = Globales.Validarintnonull(DateTime.Now.ToString("HH"));
            String saludo = "";
            if (hora <= 11) {
                saludo = "Buenos días";
            } else {
                if (hora <= 18) {
                    saludo = "Buenas tardes";
                } else {
                    saludo = "Buenas noches";
                }
            }

            String nombrearchivo = "DID-98-" + orden + ".pdf";

           if ((correoemp.trim() == "") || (clavecorreo.trim() == ""))
                return "1|Debe configurar los parámetros: Correo envio, Clave correo envio";

            String mensaje = saludo + "
                    <br><br>
                <b>SEÑORES</b><br>" + proveedor + @"<br><br>
                
                Reciba un cordial saludo por parte de nuestro equipo de trabajo" +
                (DateTime.Now.Month >= 11 ? ", esperamos que las metas planteadas para este año " + DateTime.Now.Year + " se hayan cumplido satisfactoriamente." : ".") + @"<br>  
                Adjunto encontrará la órden de compra <b>DID-98-" + orden + @"</b> correspondiente al servicio que nos va a brindar.<br><br>" +


                (DateTime.Now.Month >= 11 ? "Les deseamos unas felices fiestas, una feliz navidad y un próspero año nuevo." : "");


            if (!(String.IsNullOrEmpty(observacion)))
                mensaje += "<b>OBSERVACIÓN DE ENVÍO:</b><br><br>" + observacion + "<br>";

            mensaje += "<br><br>Cordialmente<br><br>";

            mensaje += "<img src='http://190.144.188.100:82/imagenes/logo.png' width='30%'><br><b>" + nomusuario + "</b><br>Calibration Service SAS<br>" + cargo + "<br><font color='blue'>+57 (1) 2047699 - 7285146 <br>3138141058</font><br><br>";

            String directorio = "~/DocumPDF/";

            String adjunto = String.Format("{0}{1}",
                                    this.HttpContext.Server.MapPath(directorio),
                                    archivo);

            String correo = Globales.EnviarEmail(principal, empresa, email, "Envío de Órden de Compra Número " + orden, mensaje, correoemp, adjunto, correoemp, clavecorreo, nombrearchivo);

            if (!correo.trim().equals(""))
            {
                return "1|" + correo;
            }

            sql = " UPDATE ordencompra_tercero set estado = 'Enviado', fechaenvio = now(),  idusuaenvio = " + idusuario + ", obserenvio='" + observacion + "',  "+
                    "    correoenvio = '" + principal + " " + e_email + "' WHERE id = " + id;

            Globales.DatosAuditoria(sql, "ORDEN DE COMPRA", "ENVIAR CORREO", idusuario, iplocal);
            return error + "|Correo enviado a|" + e_email;

        }
    }
    
    
    
     */
    public String GuardarOrdenTercero(HttpServletRequest request) {
        int id = Globales.Validarintnonull(request.getParameter("id"));
        int proveedor = Globales.Validarintnonull(request.getParameter("proveedor"));
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        String plazopago = request.getParameter("plazopago");
        int diasentrega = Globales.Validarintnonull(request.getParameter("diasentrega"));
        String lugarenvio = request.getParameter("lugarenvio");
        double subtotal = Globales.ValidardoublenoNull(request.getParameter("subtotal"));
        double descuento = Globales.ValidardoublenoNull(request.getParameter("descuento"));
        double iva = Globales.ValidardoublenoNull(request.getParameter("iva"));
        double gravable = Globales.ValidardoublenoNull(request.getParameter("gravable"));
        double total = Globales.ValidardoublenoNull(request.getParameter("total"));
        String observacion = request.getParameter("observacion");
        String cotizacion = request.getParameter("cotizacion");
        String a_ingreso = request.getParameter("a_ingreso");
        String a_precio = request.getParameter("a_precio");
        String a_descuento = request.getParameter("a_descuento");
        String a_subtotal = request.getParameter("a_subtotal");
        String a_cotizacion = request.getParameter("a_cotizacion");
        String a_id = request.getParameter("a_id");
        String a_cantidad = request.getParameter("a_cantidad");
        String a_iva = request.getParameter("a_iva");

        try {

            if (Globales.PermisosSistemas("ORDEN COMPRA TERCERO GUARDAR", idusuario) == 0) {
                return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
            }

            sql = " SELECT * FROM guardar_ordencomp_tercero(" + id + "," + proveedor + "," + cliente + ",'" + plazopago + "'," + diasentrega + ",'"
                    + lugarenvio + "'," + subtotal + "," + descuento + "," + iva + "," + gravable + "," + total + ",'" + observacion + "  ','" + cotizacion + "  '," + idusuario + ","
                    + a_id + "," + a_ingreso + "," + a_precio + "," + a_cantidad + "," + a_descuento + "," + a_subtotal + "," + a_cotizacion + "," + a_iva + ")";
            datos = Globales.Obtenerdatos(sql, this.getClass() + "-->GuardarOrdenTercero", 1);
            datos.next();
            if (datos.getString("_orden").equals("0")) {
                return "1|" + datos.getString("_mensaje");
            } else {
                return "0|Orden de compra guardada con el número|" + datos.getString("_orden") + "|" + datos.getString("_idorden");
            }
        } catch (Exception ex) {
            return "9|" + ex.getMessage();
        }
    }

    /*  public String EnvioFactura(String principal, int factura, String e_email, String archivo, String observacion)
        {
            int error = 0;
            if (Inicializar() != "0") return validarinicio;
            sql = @"SELECT clave, c.nombrecompleto as cliente, u.nombrecompleto as usuario, u.cargo 
                    from clientes c inner join factura fa on fa.idcliente = c.id 
                                    INNER JOIN seguridad.rbac_usuario u on u.idusu = fa.idusuario
                    where factura = " + factura;
            datos = Globales.Obtenerdatos(sql, conexion);
            String ClaveAcceso = datos.Rows[0]["clave"].ToString();
            String cliente = datos.Rows[0]["cliente"].ToString();
            String nomusuario = datos.Rows[0]["usuario"].ToString();
            String cargo = datos.Rows[0]["cargo"].ToString();

            e_email += ";" + correoenvio;
            String[] email = e_email.Split(';');

            String correoemp = correoenvio;

            int hora = Globales.Validarintnonull(DateTime.Now.ToString("HH"));
            String saludo = "";
            if (hora <= 11)
                saludo = "Buenos días";
            else
            {
                if (hora <= 18)
                    saludo = "Buenas tardes";
                else
                    saludo = "Buenas noches";
            }

            String nombrearchivo = "FCS-" + factura + ".pdf";


            if ((correoemp.trim() == "") || (clavecorreo.trim() == ""))
                return "1|Debe configurar los parámetros: Correo envio, Clave correo envio";

            String mensaje = saludo + @"
                    <br><br>
                <b>SEÑORES</b><br>" + cliente + @"<br><br>
                
                Reciba un cordial saludo por parte de nuestro equipo de trabajo.<br><br>" +
                @"Se adjunta factura número <b>" + factura + @"</b> para su pago oportuno.
                <br><br>Cordialmente.<br><br>" +

                "<img src='http://190.144.188.100:82/imagenes/logo.png' width='30%'><br><b>" + nomusuario + "</b><br>Calibration Service SAS<br>" + cargo + "<br><font color='blue'>+57 (1) 2047699 - 7285146 <br>3138141058</font><br><br>";


            if (!(String.IsNullOrEmpty(observacion)))
                mensaje += "<b>OBSERVACIÓN DE ENVÍO:</b><br>" + observacion;

            /*mensaje += "<br><br>Debe de ingresar a su cuenta de <b>CALIBRATION SERVICE SAS</b> para aprobar o no la cotización a través del link <br><br" +
                       "<a href='http://190.144.188.100:8080/usuarios/index.php'>http://190.144.188.100:8080/usuarios/index.php</a><br><br>" +
                       "Sus datos para ingresar son los siguientes: <br><b>usuario</b> " + principal +
                       "<br><b>Clave de acceso</b> " + ClaveAcceso;*/

 /*            String directorio = "~/DocumPDF/";

            String adjunto = String.Format("{0}{1}",
                                    this.HttpContext.Server.MapPath(directorio),
                                    archivo);

            String correo = Globales.EnviarEmail(principal, empresa, email, "Envío de Factura Número " + factura, mensaje, correoemp, adjunto, correoemp, clavecorreo, nombrearchivo);

            if (correo.trim() != "")
            {
                return "1|" + correo;
            }

            sql = " UPDATE factura set estado = 'Enviado', fechaenvio = now(),  idusuarioenvia = " + idusuario + ", correoenvia = '" + principal + " " + e_email + "' WHERE factura = " + factura + " and estado = 'Facturado';";

            Globales.Obtenerdatos(sql, conexion);
            return error + "|Correo enviado a|" + e_email;

        }
     */
 /*   public String EnvioSoliCert(int id, String cliente, String correocli)
        {
            int error = 0;
            if (Inicializar() != "0") return validarinicio;
            sql = @"SELECT nombrecompleto as usuario, cargo
                    from seguridad.rbac_usuario
                    where idusu = " + idusuario;
            datos = Globales.Obtenerdatos(sql, conexion);
            String nomusuario = datos.Rows[0]["usuario"].ToString();
            String cargo = datos.Rows[0]["cargo"].ToString();

            String[] email = correoenvio.Split(';');

            String correoemp = correoenvio;

            int hora = Globales.Validarintnonull(DateTime.Now.ToString("HH"));
            String saludo = "";
            if (hora <= 11)
                saludo = "Buenos días";
            else
            {
                if (hora <= 18)
                    saludo = "Buenas tardes";
                else
                    saludo = "Buenas noches";
            }

            String nombrearchivo = "";


            if ((correoemp.trim() == "") || (clavecorreo.trim() == ""))
                return "1|Debe configurar los parámetros: Correo envio, Clave correo envio";

            String mensaje = saludo + @"
                    <br><br>
                <b>SEÑORES</b><br>" + cliente + @"<br>DEPARTAMENTO DE CONTABILIDAD<br><br><br>

                <b>ASUNTO: SOLICITUD DEL CERTIFICADO DE RETENCIÓN EN LA FUENTE, ICA E IVA</b><br><br>
                
                Reciba un cordial saludo por parte de todo el equipo de trabajo de CALIBRATION SERVICE S.A.S.<br>
                Por medio del presente comunicado <b><i>requerimos a ustedes, de manera inmediata</i></b> nos sea enviado<br>
                el certificado de retención en la fuente, Ica e Iva, del año gravable 2018, dando cumplimiento al<br>
                <b>artículo 43 del Decreto 2243 del 2015.</b><br><br>
                Favor enviar los certificados correspondientes al correo electrónico<br>
                cartera@calibrationservicesas.com o cartera1@calibrationservicesas.com.<br><br>

                <b>RAZON SOCIAL: CALIBRATION SERVICE S.A.S.<br>
                NIT: 900.073.613-2<br>
                TEL: 2047699-7591310</b><br><br><br>
                Agradezco la atención prestada y su pronta respuesta.
                
                <br><br>Cordialmente.<br><br>" +

                "<img src='http://190.144.188.100:82/imagenes/logo.png' width='30%'><br><b>" + nomusuario + "</b><br>Calibration Service SAS<br>" + cargo + "<br><font color='blue'>+57 (1) 2047699 - 7285146 <br>3138141058</font><br><br>";



            String adjunto = "";
            String correo = Globales.EnviarEmail(correocli, empresa, email, "Envío de solicitud de certificado de retención EN LA FUENTE, ICA, IVA", mensaje, correoemp, adjunto, correoemp, clavecorreo, nombrearchivo);

            if (correo.trim() != "")
            {
                return "1|" + correo;
            }
            return error + "|Correo enviado a|" + correocli;

        }
     */
    public String BuscarOIngreso(HttpServletRequest request) {
        int id = Globales.Validarintnonull(request.getParameter("id"));
        int tipo = Globales.Validarintnonull(request.getParameter("tipo"));
        int orden = Globales.Validarintnonull(request.getParameter("orden"));
        {

            sql = "SELECT * FROM otro_registro WHERE tipo = " + tipo + " and id = " + id;
            return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->BuscarOIngreso");
        }
    }

    public String EliminarOIngreso(HttpServletRequest request) {
        int id = Globales.Validarintnonull(request.getParameter("id"));
        int tipo = Globales.Validarintnonull(request.getParameter("tipo"));
        int orden = Globales.Validarintnonull(request.getParameter("orden"));

        if (Globales.PermisosSistemas("ORDEN DE COMPRA ELIMINAR OTRO INGRESO", idusuario) == 0) {
            return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
        }

        sql = "SELECT idusuaenvio FROM ordencompra_tercero WHERE orden = " + orden;
        int enviada = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
        if (enviada > 1) {
            return "1|No se puede eliminar un registro de una órden de compra enviada al cliente";
        }

        sql = "delete from otro_registro WHERE id = " + id + ";";

        if (Globales.DatosAuditoria(sql, "ORDEN DE COMPRA", "ELIMINAR OTRO INGRESO", idusuario, iplocal, this.getClass() + "-->EliminarOIngreso")) {
            return "0|Registro eliminado";
        } else {
            return "1|Error inesperado... Revise el log de errores";
        }
    }

    public String DescripcionDetalleOrden(HttpServletRequest request) {
        int id = Globales.Validarintnonull(request.getParameter("id"));
        sql = "SELECT descripcion FROM otro_registro WHERE id = " + id;
        return Globales.ObtenerUnValor(sql);
    }

    public String TablaOrdenTercer(HttpServletRequest request) {
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        int proveedor = Globales.Validarintnonull(request.getParameter("proveedor"));
        int orden = Globales.Validarintnonull(request.getParameter("orden"));
        int tipo = Globales.Validarintnonull(request.getParameter("tipo"));

        if (orden == 0) {
            sql = "select cd.id, c.cotizacion,rd.observacion, "
                    + "      '<b>' || m.descripcion || '</b><br>' || e.descripcion || '<br><b>' || ma.descripcion || '</b><br>' || mo.descripcion || '<br> ' || case when rd.idintervalo = 0 then 'VER ESPECIFICACIONES' ELSE '(' || desde || ' a ' || hasta || ') ' || medida end || '<br>' || rd.serie as equipo,"
                    + "      rd.ingreso, to_char(rd.fechaing, 'dd/MM/yyyy HH24:MI') as fechaing, "
                    + "      u.nombrecompleto as usuarioing, coalesce('<b>' || s.nombre  || '</b><br>' || me.descripcion,'NO COTIZADO') as servicio, rd.fotos,"
                    + "      cd.cantidad, cd.descuento, cd.precio, cd.iva, (cd.cantidad*cd.precio) - (cd.cantidad*cd.precio*cd.descuento/100) as subtotal, 1 as otro, recibidocome"
                    + "  from remision_detalle rd INNER JOIN remision r on rd.idremision = r.id"
                    + "                inner join equipo e on rd.idequipo = e.id"
                    + "                inner join magnitudes m on m.id = e.idmagnitud"
                    + "                inner join modelos mo on mo.id = rd.idmodelo"
                    + "                inner join seguridad.rbac_usuario ur on ur.idusu = r.idusuario"
                    + "                inner join marcas ma on ma.id = mo.idmarca "
                    + "                inner join magnitud_intervalos mi on mi.id = rd.idintervalo "
                    + "                inner join seguridad.rbac_usuario u on u.idusu = r.idusuario"
                    + "                inner JOIN  cotizacion_detalle cd ON cd.ingreso = rd.ingreso and cd.estado = ('Cotizado')"
                    + "                inner JOIN  cotizacion c ON c.id = cd.idcotizacion AND c.estado in ('Cerrado','Aprobado','Facturado','POR REEMPLAZAR')"
                    + "                inner join servicio s on s.id = cd.idservicio "
                    + "                inner join metodo me on me.id = cd.metodo  "
                    + "WHERE r.idcliente = " + cliente + " and cd.idproveedor = " + proveedor + " and rd.envtercero = 0  "
                    + " UNION"
                    + " select id, 0 as cotizacion, '' as observacion, descripcion as equipo, ingreso, '' as fechaing, '' as usuarioing, '' as servicio, 0 as fotos,"
                    + "       cantidad, descuento, precio, iva, subtotal, 2 as  otro, 1 as recibidocome"
                    + " from otro_registro  "
                    + " where idproveedor = " + proveedor + " and tipo = " + tipo + " and numorden = " + orden + " and  idcliente=" + cliente + " ORDER BY ingreso ";
        } else {
            sql = "select 0 as id, c.cotizacion,rd.observacion, "
                    + "     '<b>' || m.descripcion || '</b><br>' || e.descripcion || '<br><b>' || ma.descripcion || '</b><br>' || mo.descripcion || '<br> ' || case when rd.idintervalo = 0 then 'VER ESPECIFICACIONES' ELSE '(' || desde || ' a ' || hasta || ') ' || medida end || '<br>' || rd.serie as equipo,"
                    + "     rd.ingreso, to_char(rd.fechaing, 'dd/MM/yyyy HH24:MI') as fechaing, "
                    + "    u.nombrecompleto as usuarioing, coalesce('<b>' || s.nombre  || '</b><br>' || me.descripcion,'NO COTIZADO') as servicio, rd.fotos,"
                    + "     ocd.cantidad, ocd.descuento, ocd.precio, ocd.iva, (ocd.cantidad*ocd.precio) - (ocd.cantidad*ocd.precio*ocd.descuento/100) as subtotal, 1 as otro"
                    + " from remision_detalle rd INNER JOIN remision r on rd.idremision = r.id"
                    + "                 inner join equipo e on rd.idequipo = e.id"
                    + "                 inner join magnitudes m on m.id = e.idmagnitud"
                    + "                 inner join modelos mo on mo.id = rd.idmodelo"
                    + "                 inner join seguridad.rbac_usuario ur on ur.idusu = r.idusuario"
                    + "                 inner join marcas ma on ma.id = mo.idmarca "
                    + "                inner join magnitud_intervalos mi on mi.id = rd.idintervalo  "
                    + "                 inner join seguridad.rbac_usuario u on u.idusu = r.idusuario"
                    + "         inner join ordencompter_detalle ocd on ocd.ingreso = rd.ingreso"
                    + "         inner join ordencompra_tercero oc on oc.id = ocd.idorden"
                    + "                 left JOIN  cotizacion_detalle cd ON cd.ingreso = rd.ingreso and cd.estado = ('Cotizado')"
                    + "                 left JOIN  cotizacion c ON c.id = cd.idcotizacion AND c.estado in ('Cerrado','Aprobado','Facturado')"
                    + "                 left join servicio s on s.id = cd.idservicio "
                    + "                 left join metodo me on me.id = cd.metodo "
                    + "   WHERE rd.ingreso > 0 and oc.orden = " + orden + "  "
                    + "    UNION"
                    + " select id, 0 as cotizacion, '' as observacion, descripcion as equipo, ingreso, '' as fechaing, '' as usuarioing, '' as servicio, 0 as fotos,"
                    + "       cantidad, descuento, precio, iva, subtotal, 2 as otro"
                    + " from otro_registro  "
                    + " where idproveedor = " + proveedor + " and ingreso = 0 and  tipo = " + tipo + " and numorden = " + orden + " and idcliente=" + cliente + " ORDER BY ingreso ";
        }

        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->TablaOrdenTercer");
    }

    public String GuardarOtroRegistro(HttpServletRequest request) {
        int ingreso = Globales.Validarintnonull(request.getParameter("ingreso"));
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        int proveedor = Globales.Validarintnonull(request.getParameter("proveedor"));
        int id = Globales.Validarintnonull(request.getParameter("id"));
        String descripcion = (request.getParameter("descripcion"));
        int cantidad = Globales.Validarintnonull(request.getParameter("cantidad"));
        int descuento = Globales.Validarintnonull(request.getParameter("descuento"));
        double precio = Globales.ValidardoublenoNull(request.getParameter("precio"));
        double subtotal = Globales.ValidardoublenoNull(request.getParameter("subtotal"));
        int tipo = Globales.Validarintnonull(request.getParameter("tipo"));
        int orden = Globales.Validarintnonull(request.getParameter("orden"));
        int articulo = Globales.Validarintnonull(request.getParameter("articulo"));
        int iva = request.getParameter("iva").isEmpty() ? 19 : Globales.Validarintnonull(request.getParameter("iva"));

        /*sql = @"SELECT count(rd.ingreso) 
                    FROM ordencompra_tercero oc INNER JOIN ordencompter_detalle ocd on oc.id = ocd.idorden 
                                                inner join remision_detalle rd on rd.ingreso = ocd.ingreso
                    where salidaterce > 0 and oc.orden = " + orden;
            int equipos = Globales.Validarintnonull(Globales.ObtenerUnValor(sql, conexion, 0));
            if (equipos > 0)
                return "1|No se puede editar una órden de compra porque está relacionado con salidas de equipos activas";*/
        sql = "SELECT estado FROM ordencompra_tercero WHERE orden = " + orden;
        String estado = Globales.ObtenerUnValor(sql);
        if (!estado.equals("Registrado") && !estado.equals("")) {
            return "1|No se puede editar un registro de una órden de compra con estado " + estado;
        }

        if (ingreso > 0 && id == 0) {

            sql = "SELECT recibidocome FROM remision_detalle WHERE ingreso = " + ingreso;
            int recibido = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
            if (recibido == 0) {
                return "1|Este ingreso no ha sido recibido en comercial para tercerizar";
            }

            sql = "SELECT ingreso FROM otro_registro WHERE ingreso = " + ingreso + " and (numorden = 0 or numorden = " + orden + ") and tipo = " + tipo;
            int enviada = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
            if (enviada > 1) {
                return "1|No se puede asignar este ingreso porque ya fue agregado anteriormente";
            }
        }

        String mensaje = "";
        try {
            if (id == 0) {
                sql = "INSERT INTO otro_registro(numorden, ingreso, descripcion, precio, cantidad, descuento, subtotal, idproveedor, idcliente, tipo, idarticulo, iva)"
                        + "VALUES (" + orden + "," + ingreso + ",'" + descripcion + "'," + precio + "," + cantidad + "," + descuento + "," + subtotal + "," + proveedor + "," + cliente + "," + tipo + "," + articulo + "," + iva + ");";
                if (Globales.DatosAuditoria(sql, "FACTURACION", "AGREGAR OTRO REGISTRO", idusuario, iplocal, this.getClass() + "-->GuardarOtroRegistro")) {
                    id = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT MAX(id) FROM otro_registro;"));
                    mensaje = "0|Ingreso agregado con éxito|" + id;
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            } else {
                sql = "UPDATE otro_registro"
                        + "SET descripcion='" + descripcion + "', precio=" + precio + ", cantidad=" + cantidad + ", descuento=" + descuento + ",subtotal=" + subtotal + ", ingreso = " + ingreso + ", iva=" + iva
                        + " WHERE id = " + id;
                if (Globales.DatosAuditoria(sql, "FACTURACION", "EDITAR OTRO REGISTRO", idusuario, iplocal, this.getClass() + "-->GuardarOtroRegistro")) {
                    mensaje = "0|Ingreso actualizado con éxito|" + id;
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            }
            return mensaje;
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }

    }

    public String ConsultarOrdenTercero(HttpServletRequest request) {
        int orden = Globales.Validarintnonull(request.getParameter("orden"));
        int cotizacion = Globales.Validarintnonull(request.getParameter("cotizacion"));
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        int proveedor = Globales.Validarintnonull(request.getParameter("proveedor"));
        int ingreso = Globales.Validarintnonull(request.getParameter("ingreso"));
        String fechad = (request.getParameter("fechad"));
        String fechah = (request.getParameter("fechah"));
        int magnitud = Globales.Validarintnonull(request.getParameter("magnitud"));
        String equipo = (request.getParameter("equipo"));
        int marca = Globales.Validarintnonull(request.getParameter("marca"));
        String modelo = (request.getParameter("modelo"));
        String intervalo = (request.getParameter("intervalo"));
        String serie = (request.getParameter("serie"));
        int usuarios = Globales.Validarintnonull(request.getParameter("usuario"));

        String busqueda = "WHERE 1=1 ";
        String busqueda2 = "WHERE oc.estado <> 'Anulado' and to_char(oc.fecha, 'yyyy-MM-dd') >= '" + fechad + "' and to_char(oc.fecha,'yyyy-MM-dd') <= '" + fechah + "'";
        if (orden > 0) {
            busqueda += " AND oc.orden = " + orden;
        } else if (cotizacion > 0) {
            busqueda += " AND oc.numcotizacion = " + cotizacion;
        } else if (ingreso > 0) {
            busqueda += " AND rd.ingreso = " + ingreso;
        } else {
            busqueda += " AND to_char(oc.fecha,'yyyy-MM-dd') >= '" + fechad + "' and to_char(oc.fecha,'yyyy-MM-dd') <= '" + fechah + "'";
            if (cliente > 0) {
                busqueda += " AND oc.idcliente = " + cliente;
                busqueda2 += " AND oc.idcliente = " + cliente;
            }
            if (proveedor > 0) {
                busqueda += " AND oc.idproveedor = " + proveedor;
                busqueda2 += " AND oc.idproveedor = " + proveedor;
            }
            if (ingreso > 0) {
                busqueda += " AND rd.ingreso = " + ingreso;
            }
            if (magnitud > 0) {
                busqueda += " AND e.idmagnitud = " + magnitud;
            }
            if (!equipo.equals("")) {
                busqueda += " AND e.descripcion = '" + equipo + "'";
            }
            if (marca > 0) {
                busqueda += " AND mo.idmarca = " + marca;
            }
            if (!modelo.equals("")) {
                busqueda += " AND mo.descripcion = '" + modelo + "'";
            }
            if (!intervalo.equals("")) {
                busqueda += " AND '(' || desde || ' a ' || hasta || ') ' || medida = '" + intervalo + "'";
            }
            if (!serie.trim().equals("")) {
                busqueda += " AND rd.serie ilike '%" + serie.trim() + "%'";
            }
            if (usuarios > 0) {
                busqueda += " AND oc.idusuario = " + usuarios;
                busqueda2 += " AND oc.idusuario = " + usuarios;
            }

        }

        sql = "SELECT p.nombrecompleto || ' (' || p.documento || ')' AS proveedor,"
                + " cli.nombrecompleto || ' (' || cli.documento || ')' AS cliente,"
                + " p.email,"
                + " p.telefono || ' / ' || p.celular as telefono,"
                + " ci.descripcion  as ciudad,"
                + " d.descripcion as departamento,"
                + " oc.orden, oc.estado,"
                + " numcotizacion,"
                + " oc.subtotal, oc.descuento, oc.iva, oc.total,"
                + "  to_char(oc.fecha,'yyyy/MM/dd HH24:MI') || '<br>' || u.nombrecompleto as registro,"
                + "  to_char(oc.fechaenvio,'yyyy/MM/dd HH24:MI') || '<br>' || ue.nombrecompleto ||'<br>' || oc.correoenvio as envio,"
                + "  to_char(oc.fechaanula,'yyyy/MM/dd HH24:MI') || '<br>' || ua.nombrecompleto ||'<br>' || oc.observacionanula as anulado,"
                + "  oc.plazopago, diasentrega, lugarentrega, oc.correoenvio, count(distinct ocd.id) as cantidad,"
                + "  '<button class=''btn btn-glow btn-success imprimir'' title=''Imprimir Orden de Compra'' type=''button'' onclick=' || chr(34) || 'ImprimirOrden(' || oc.orden || ',0)' ||chr(34) || '><span data-icon=''&#xe0f7;''></span></button>' as imprimir"
                + "  FROM ordencompra_tercero oc inner join ordencompter_detalle ocd on oc.id = ocd.idorden"
                + "      JOIN proveedores p ON oc.idproveedor = p.id"
                + "      JOIN clientes cli on cli.id = oc.idcliente"
                + "      JOIN ciudad ci ON ci.id = p.idciudad"
                + "      JOIN departamento d ON d.id = ci.iddepartamento"
                + "      JOIN pais pa on pa.id = d.idpais"
                + "      JOIN seguridad.rbac_usuario u on u.idusu = oc.idusuario"
                + "      JOIN seguridad.rbac_usuario ue on ue.idusu = oc.idusuaenvio"
                + "      JOIN seguridad.rbac_usuario ua on ua.idusu = oc.idusuarioanula"
                + "      LEFT JOIN remision_detalle rd ON rd.ingreso = ocd.ingreso"
                + "      left join magnitud_intervalos i on i.id = rd.idintervalo "
                + "      LEFT JOIN equipo e ON e.id = rd.idequipo"
                + "      LEFT JOIN modelos mo ON mo.id = rd.idmodelo " + busqueda + " "
                + "  group by p.nombrecompleto, p.documento, cli.nombrecompleto, cli.documento, p.email,"
                + "          p.telefono, p.celular,ci.descripcion,d.descripcion,oc.orden,numcotizacion, oc.subtotal, oc.descuento, oc.iva, oc.total,"
                + "      oc.fecha,oc.fechaenvio,u.nombrecompleto,oc.plazopago, diasentrega, lugarentrega, oc.correoenvio, oc.estado,"
                + "      ue.nombrecompleto, ua.nombrecompleto, oc.correoenvio, oc.observacionanula, oc.fechaanula";
        String sql2 = "SELECT  sum(oc.subtotal) as subtotal, sum(oc.descuento) as descuento, sum(oc.iva) as iva, sum(oc.total) as total, 0 as facturado,"
                + "      to_char(oc.fecha,'yyyy/MM') as fecha"
                + "  FROM ordencompra_tercero oc " + busqueda2 + " "
                + "  group by to_char(oc.fecha,'yyyy/MM')"
                + "  order by fecha";

        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ConsultarOrdenTercero") + "||" + Globales.ObtenerDatosJSon(sql2, this.getClass() + "-->ConsultarOrdenTercero");
    }

    public String ConsultarIngresoOC(HttpServletRequest request) {
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        int proveedor = Globales.Validarintnonull(request.getParameter("proveedor"));
        String fechad = (request.getParameter("fechad"));
        String fechah = (request.getParameter("fechah"));
        int magnitud = Globales.Validarintnonull(request.getParameter("magnitud"));
        String equipo = (request.getParameter("equipo"));
        int marca = Globales.Validarintnonull(request.getParameter("marca"));
        String modelo = (request.getParameter("modelo"));
        String intervalo = (request.getParameter("intervalo"));
        String serie = (request.getParameter("serie"));
        int usuarios = Globales.Validarintnonull(request.getParameter("usuario"));

        String busqueda = "WHERE rd.ingreso > 0 and oc.estado <> 'Anulado' AND to_char(oc.fecha,'yyyy-MM-dd') >= '" + fechad + "' and to_char(oc.fecha,'yyyy-MM-dd') <= '" + fechah + "'";
        String busqueda2 = "WHERE rd.ingreso > 0 and oc.estado <> 'Anulado' AND oc.estado <> 'Anulado' and to_char(oc.fecha, 'yyyy-MM-dd') >= '" + fechad + "' and to_char(oc.fecha,'yyyy-MM-dd') <= '" + fechah + "'";

        if (cliente > 0) {
            busqueda += " AND oc.idcliente = " + cliente;
            busqueda2 += " AND oc.idcliente = " + cliente;
        }
        if (proveedor > 0) {
            busqueda += " AND oc.idproveedor = " + proveedor;
            busqueda2 += " AND oc.idproveedor = " + proveedor;
        }

        if (magnitud > 0) {
            busqueda += " AND e.idmagnitud = " + magnitud;
        }
        if (!equipo.equals("")) {
            busqueda += " AND e.descripcion = '" + equipo + "'";
        }
        if (marca > 0) {
            busqueda += " AND mo.idmarca = " + marca;
        }
        if (!modelo.equals("")) {
            busqueda += " AND mo.descripcion = '" + modelo + "'";
        }
        if (!intervalo.equals("")) {
            busqueda += " AND '(' || desde || ' a ' || hasta || ') ' || medida = '" + intervalo + "'";
        }
        busqueda += " AND rd.serie ilike '%" + serie.trim() + "%'";
        if (usuarios > 0) {
            busqueda += " AND oc.idusuario = " + usuarios;
            busqueda2 += " AND oc.idusuario = " + usuarios;
        }

        sql = "select rd.ingreso, to_char(oc.fecha, 'yyyy/MM/dd') as fecha, oc.orden,"
                + "      ma.descripcion as magnitud,  e.descripcion as equipo, m.descripcion as marca, mo.descripcion as modelo,"
                + "      p.nombrecompleto as proveedor, c.nombrecompleto as cliente, "
                + "      to_char(oc.fecha,'yyyy/MM/dd HH24:MI') || '<br>' || u.nombrecompleto as registro,"
                + "      case when rd.idintervalo = 0 then 'VER ESPECIFICACIONES' ELSE '(' || i.desde || ' a ' || i.hasta || ') ' || i.medida END as intervalo "
                + "  from ordencompra_tercero oc inner join ordencompter_detalle ocd on oc.id = ocd.idorden"
                + "                                  inner join remision_detalle rd on rd.ingreso = ocd.ingreso"
                + "                              inner join remision r on r.id = rd.idremision"
                + "                              inner join seguridad.rbac_usuario u on r.idusuario = u.idusu"
                + "                                  inner join equipo e on e.id = rd.idequipo"
                + "                              inner join modelos mo on mo.id = rd.idmodelo"
                + "                                  inner join marcas m on m.id = mo.idmarca"
                + "                                  inner join magnitudes ma on ma.id = e.idmagnitud"
                + "                                  inner join magnitud_intervalos i on i.id = rd.idintervalo "
                + "                                  inner join clientes c on c.id = oc.idcliente"
                + "                                  inner join proveedores p on p.id = oc.idproveedor " + busqueda;

        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ConsultarIngresoOC");
    }

    public String ActualizarTemporalFac(HttpServletRequest request) {
        double precio = Globales.ValidardoublenoNull(request.getParameter("precio"));
        int cantidad = Globales.Validarintnonull(request.getParameter("cantidad"));
        int descuento = Globales.Validarintnonull(request.getParameter("descuento"));
        double subtotal = Globales.ValidardoublenoNull(request.getParameter("subtotal"));
        int factura = Globales.Validarintnonull(request.getParameter("factura"));
        int id = Globales.Validarintnonull(request.getParameter("id"));
        int idcentro = Globales.Validarintnonull(request.getParameter("idcentro"));

        sql = "SELECT estado, id from factura where factura = " + factura;
        String estado = Globales.ObtenerUnValor(sql).trim();
        if (!estado.equals("Temporal") && !estado.equals("Facturado") && !estado.equals("")) {
            return "1";
        }
        sql = "UPDATE factura_temporal"
                + "     SET precio=" + precio + ", cantidad=" + cantidad + ", descuento=" + descuento + ",subtotal=" + subtotal + ",idcentro=" + idcentro
                + " WHERE id = " + id + ";UPDATE factura SET estado = 'Temporal' WHERE factura = " + factura + ";";
        sql += "UPDATE factura set estado = 'Temporal' where factura = " + factura;
        /*if (id > 0)
            {
                sql += @"UPDATE factura_datelle
                        SET precio=" + precio + ", cantidad=" + cantidad + ", descuento=" + descuento + ",subtotal=" + subtotal +
                        " WHERE id = " + id + ";UPDATE factura SET estado = 'Temporal' WHERE factura = " + factura + ";" +
                        "UPDATE factura SET estado = 'Temporal' WHERE id = " + id;
            }*/
        Globales.Obtenerdatos(sql, this.getClass() + "-->DetRemisionNoFacturado", 2);
        return "0";

    }

    public String GuardarDetFactura(HttpServletRequest request) {
        int ingreso = Globales.Validarintnonull(request.getParameter("ingreso"));
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        String orden = (request.getParameter("orden"));
        int id = Globales.Validarintnonull(request.getParameter("id"));
        String descripcion = (request.getParameter("descripcion"));
        int cantidad = Globales.Validarintnonull(request.getParameter("cantidad"));
        int descuento = Globales.Validarintnonull(request.getParameter("descuento"));
        int iva = Globales.Validarintnonull(request.getParameter("iva"));
        double precio = Globales.ValidardoublenoNull(request.getParameter("precio"));
        double subtotal = Globales.ValidardoublenoNull(request.getParameter("subtotal"));
        int tipo = Globales.Validarintnonull(request.getParameter("tipo"));
        int idfactura = Globales.Validarintnonull(request.getParameter("idfactura"));
        int idcentro = Globales.Validarintnonull(request.getParameter("idcentro"));
        int servicio = Globales.Validarintnonull(request.getParameter("servicio"));
        int idarticulo = Globales.Validarintnonull(request.getParameter("idarticulo"));
        int cuentacontable = Globales.Validarintnonull(request.getParameter("cuentacontable"));

        String mensaje = "";
        try {
            int cuenta = 0;
            if (cuentacontable == 0) {
                cuenta = Globales.Validarintnonull(Globales.ObtenerUnValor("select idcuecontable from otros_servicios where id=" + servicio));
            } else {
                cuenta = Globales.Validarintnonull(Globales.ObtenerUnValor("select id from contabilidad.cuenta where cuenta=" + cuentacontable));
            }

            if (cuenta == 0) {
                return "1|El artículo ó servicio no posee una cuenta contable asignada";
            }

            if (id == 0) {

                sql = "INSERT INTO factura_temporal(idfactura, ingreso, descripcion, precio, cantidad, descuento, subtotal, idcliente, cotizacion, orden, certificado, iva, idcentro, idservicio, idcuecontable, idarticulo)"
                        + " VALUES (" + idfactura + "," + ingreso + ",'" + descripcion + "'," + precio + "," + cantidad + "," + descuento + "," + subtotal + "," + cliente + ",0,'" + orden.trim() + "',''," + iva + "," + idcentro + "," + servicio + "," + cuenta + "," + idarticulo + "); "
                        + "  SELECT MAX(id) FROM factura_temporal;";

                id = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
                mensaje = "0|Detalle agregado con éxito|" + id;

                /*if (idfactura > 0)
                    {
                        sql = @"INSERT INTO factura_datelle(idfactura, ingreso, descripcion, precio, cantidad, descuento, subtotal, cotizacion, orden, certificado)
                            VALUES (" + idfactura + ",0,'" + descripcion + "'," + precio + "," + cantidad + "," + descuento + "," + subtotal + ",0,'" + orden.trim() + @"',''); 
                            UPDATE factura SET estado = 'Temporal' WHERE id = " + idfactura;
                    }*/
            } else {
                sql
                        = "UPDATE factura_temporal"
                        + "        SET idservicio = " + servicio + ", idcuecontable = " + cuenta + ", descripcion ='" + descripcion + "'  , ingreso=" + ingreso + "  ,precio=" + precio + "  , cantidad=" + cantidad + "  , descuento=" + descuento + "  ,subtotal=" + subtotal + "  ,idcentro=" + idcentro + "  ,idarticulo=" + idarticulo
                        + " WHERE id = " + id + ";";
                /*if (idfactura > 0)
                    {
                        sql += @"UPDATE factura_datelle
                                    SET descripcion='" + descripcion + "', precio=" + precio + ", cantidad=" + cantidad + ", descuento=" + descuento + ",subtotal=" + subtotal +
                                " WHERE id = " + id + ";UPDATE factura SET estado = 'Temporal' WHERE id = " + idfactura;
                                
                    }*/
                mensaje = "0|Detalle actualizado con éxito|" + id;
                Globales.ObtenerDatosJSon(sql, this.getClass() + "-->GuardarDetFactura");
            }

            return mensaje;
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }

    }

    public String GraficaFechaOC(HttpServletRequest request) {
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        int proveedor = Globales.Validarintnonull(request.getParameter("proveedor"));
        String fechad = (request.getParameter("fechad"));
        String fechah = (request.getParameter("fechah"));
        int magnitud = Globales.Validarintnonull(request.getParameter("magnitud"));
        int equipo = Globales.Validarintnonull(request.getParameter("equipo"));
        int marca = Globales.Validarintnonull(request.getParameter("marca"));
        int modelo = Globales.Validarintnonull(request.getParameter("modelo"));
        int intervalo = Globales.Validarintnonull(request.getParameter("intervalo"));
        String serie = (request.getParameter("serie"));
        int usuarios = Globales.Validarintnonull(request.getParameter("usuario"));

        String busqueda = "WHERE rd.ingreso > 0 and oc.estado <> 'Anulado' AND to_char(oc.fecha,'yyyy-MM-dd') >= '" + fechad + "' and to_char(oc.fecha,'yyyy-MM-dd') <= '" + fechah + "'";

        if (cliente > 0) {
            busqueda += " AND oc.idcliente = " + cliente;
        }
        if (proveedor > 0) {
            busqueda += " AND oc.idproveedor = " + proveedor;
        }

        if (magnitud > 0) {
            busqueda += " AND e.idmagnitud = " + magnitud;
        }
        if (equipo > 0) {
            busqueda += " AND rd.idequipo = " + equipo;
        }
        if (marca > 0) {
            busqueda += " AND mo.idmarca = " + marca;
        }
        if (modelo > 0) {
            busqueda += " AND rd.idmodelo = " + modelo;
        }
        if (intervalo > 0) {
            busqueda += " AND rd.idintervalo = " + intervalo;
        }
        if (!serie.trim().equals("")) {
            busqueda += " AND rd.serie ilike '%" + serie.trim() + "%'";
        }
        if (usuarios > 0) {
            busqueda += " AND oc.idusuario = " + usuarios;
        }
        sql = "select to_char(oc.fecha, 'yyyy/MM') as fecha"
                + "  from ordencompra_tercero oc inner join ordencompter_detalle ocd on oc.id = ocd.idorden"
                + "                                    inner join remision_detalle rd on rd.ingreso = ocd.ingreso"
                + "                                inner join remision r on r.id = rd.idremision"
                + "                                inner join seguridad.rbac_usuario u on r.idusuario = u.idusu"
                + "                                    inner join equipo e on e.id = rd.idequipo"
                + "                        inner join magnitudes m on m.id = e.idmagnitud"
                + "                                inner join modelos mo on mo.id = rd.idmodelo " + busqueda + " "
                + "group by to_char(oc.fecha,'yyyy/MM') "
                + "order by 1";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->GraficaFechaOC");
    }

    public String GraficaFechaOCMag(HttpServletRequest request) {
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        int proveedor = Globales.Validarintnonull(request.getParameter("proveedor"));
        String fechad = (request.getParameter("fechad"));
        String fechah = (request.getParameter("fechah"));
        int magnitud = Globales.Validarintnonull(request.getParameter("magnitud"));
        int equipo = Globales.Validarintnonull(request.getParameter("equipo"));
        int marca = Globales.Validarintnonull(request.getParameter("marca"));
        int modelo = Globales.Validarintnonull(request.getParameter("modelo"));
        int intervalo = Globales.Validarintnonull(request.getParameter("intervalo"));
        String serie = (request.getParameter("serie"));
        int usuarios = Globales.Validarintnonull(request.getParameter("usuario"));

        String busqueda = "WHERE rd.ingreso > 0 and oc.estado <> 'Anulado' AND to_char(oc.fecha,'yyyy-MM-dd') >= '" + fechad + "' and to_char(oc.fecha,'yyyy-MM-dd') <= '" + fechah + "'";

        if (cliente > 0) {
            busqueda += " AND oc.idcliente = " + cliente;
        }
        if (proveedor > 0) {
            busqueda += " AND oc.idproveedor = " + proveedor;
        }

        if (magnitud > 0) {
            busqueda += " AND e.idmagnitud = " + magnitud;
        }
        if (equipo > 0) {
            busqueda += " AND rd.idequipo = " + equipo;
        }
        if (marca > 0) {
            busqueda += " AND mo.idmarca = " + marca;
        }
        if (modelo > 0) {
            busqueda += " AND rd.idmodelo = " + modelo;
        }
        if (intervalo > 0) {
            busqueda += " AND rd.idintervalo = " + intervalo;
        }
        if (!serie.trim().equals("")) {
            busqueda += " AND rd.serie ilike '%" + serie.trim() + "%'";
        }
        if (usuarios > 0) {
            busqueda += " AND oc.idusuario = " + usuarios;
        }
        sql = "select count(distinct rd.ingreso) as cantidad, m.descripcion as magnitud, to_char(oc.fecha,'yyyy/MM') as fecha"
                + "      from ordencompra_tercero oc inner join ordencompter_detalle ocd on oc.id = ocd.idorden"
                + "	                                    inner join remision_detalle rd on rd.ingreso = ocd.ingreso"
                + "                                       inner join remision r on r.id = rd.idremision"
                + "                                      inner join seguridad.rbac_usuario u on r.idusuario = u.idusu"
                + "	                                    inner join equipo e on e.id = rd.idequipo"
                + "                                inner join magnitudes m on m.id = e.idmagnitud"
                + "                                        inner join modelos mo on mo.id = rd.idmodelo " + busqueda + " "
                + "     group by m.descripcion, to_char(oc.fecha,'yyyy/MM')";

        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->GraficaFechaOCMag");
    }

    public String GraficaOCMag(HttpServletRequest request) {
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        int proveedor = Globales.Validarintnonull(request.getParameter("proveedor"));
        String fechad = (request.getParameter("fechad"));
        String fechah = (request.getParameter("fechah"));
        int magnitud = Globales.Validarintnonull(request.getParameter("magnitud"));
        int equipo = Globales.Validarintnonull(request.getParameter("equipo"));
        int marca = Globales.Validarintnonull(request.getParameter("marca"));
        int modelo = Globales.Validarintnonull(request.getParameter("modelo"));
        int intervalo = Globales.Validarintnonull(request.getParameter("intervalo"));
        String serie = (request.getParameter("serie"));
        int usuarios = Globales.Validarintnonull(request.getParameter("usuario"));

        String busqueda = "WHERE rd.ingreso > 0 and oc.estado <> 'Anulado' AND to_char(oc.fecha,'yyyy-MM-dd') >= '" + fechad + "' and to_char(oc.fecha,'yyyy-MM-dd') <= '" + fechah + "'";

        if (cliente > 0) {
            busqueda += " AND oc.idcliente = " + cliente;
        }
        if (proveedor > 0) {
            busqueda += " AND oc.idproveedor = " + proveedor;
        }

        if (magnitud > 0) {
            busqueda += " AND e.idmagnitud = " + magnitud;
        }
        if (equipo > 0) {
            busqueda += " AND rd.idequipo = " + equipo;
        }
        if (marca > 0) {
            busqueda += " AND mo.idmarca = " + marca;
        }
        if (modelo > 0) {
            busqueda += " AND rd.idmodelo = " + modelo;
        }
        if (intervalo > 0) {
            busqueda += " AND rd.idintervalo = " + intervalo;
        }
        if (!serie.trim().equals("")) {
            busqueda += " AND rd.serie ilike '%" + serie.trim() + "%'";
        }
        if (usuarios > 0) {
            busqueda += " AND oc.idusuario = " + usuarios;
        }
        sql = "select count(distinct rd.ingreso) as cantidad, m.descripcion as magnitud"
                + "  from ordencompra_tercero oc inner join ordencompter_detalle ocd on oc.id = ocd.idorden"
                + "                                   inner join remision_detalle rd on rd.ingreso = ocd.ingreso"
                + "                               inner join remision r on r.id = rd.idremision"
                + "                               inner join seguridad.rbac_usuario u on r.idusuario = u.idusu"
                + "                                   inner join equipo e on e.id = rd.idequipo"
                + "                       inner join magnitudes m on m.id = e.idmagnitud"
                + "                               inner join modelos mo on mo.id = rd.idmodelo " + busqueda + " "
                + " group by m.descripcion ORDER BY 1 desc;";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->GraficaOCMag");
    }

    public String GraficaOCEquipo(HttpServletRequest request) {
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        int proveedor = Globales.Validarintnonull(request.getParameter("proveedor"));
        String fechad = (request.getParameter("fechad"));
        String fechah = (request.getParameter("fechah"));
        int magnitud = Globales.Validarintnonull(request.getParameter("magnitud"));
        int equipo = Globales.Validarintnonull(request.getParameter("equipo"));
        int marca = Globales.Validarintnonull(request.getParameter("marca"));
        int modelo = Globales.Validarintnonull(request.getParameter("modelo"));
        int intervalo = Globales.Validarintnonull(request.getParameter("intervalo"));
        String serie = (request.getParameter("serie"));
        int usuarios = Globales.Validarintnonull(request.getParameter("usuario"));
        String busqueda = "WHERE rd.ingreso > 0 and oc.estado <> 'Anulado' AND to_char(oc.fecha,'yyyy-MM-dd') >= '" + fechad + "' and to_char(oc.fecha,'yyyy-MM-dd') <= '" + fechah + "'";

        if (cliente > 0) {
            busqueda += " AND oc.idcliente = " + cliente;
        }
        if (proveedor > 0) {
            busqueda += " AND oc.idproveedor = " + proveedor;
        }

        if (magnitud > 0) {
            busqueda += " AND e.idmagnitud = " + magnitud;
        }
        if (equipo > 0) {
            busqueda += " AND rd.idequipo = " + equipo;
        }
        if (marca > 0) {
            busqueda += " AND mo.idmarca = " + marca;
        }
        if (modelo > 0) {
            busqueda += " AND rd.idmodelo = " + modelo;
        }
        if (intervalo > 0) {
            busqueda += " AND rd.idintervalo = " + intervalo;
        }
        if (!serie.trim().equals("")) {
            busqueda += " AND rd.serie ilike '%" + serie.trim() + "%'";
        }
        if (usuarios > 0) {
            busqueda += " AND oc.idusuario = " + usuarios;
        }
        sql = "select count(distinct rd.ingreso) as cantidad, e.descripcion as equipo"
                + "  from ordencompra_tercero oc inner join ordencompter_detalle ocd on oc.id = ocd.idorden"
                + "                                    inner join remision_detalle rd on rd.ingreso = ocd.ingreso"
                + "                                inner join remision r on r.id = rd.idremision"
                + "                                inner join seguridad.rbac_usuario u on r.idusuario = u.idusu"
                + "                                    inner join equipo e on e.id = rd.idequipo"
                + "                        inner join magnitudes m on m.id = e.idmagnitud"
                + "                                inner join modelos mo on mo.id = rd.idmodelo " + busqueda + " "
                + " group by e.descripcion ORDER BY 1 desc;";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->GraficaOCEquipo");
    }

    public String GuardarRadicacion(HttpServletRequest request) {
        String facturas = (request.getParameter("facturas"));
        String fecha = (request.getParameter("fecha"));
        int mensajero = Globales.Validarintnonull(request.getParameter("mensajero"));
        String observacion = (request.getParameter("observacion"));

        int contador = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT radicacion FROM contadores")) + 1;
        int encontrado = 0;
        String mensaje = "";
        String[] a_factura = facturas.split(",");
        try {
            sql = "";

            if (Globales.PermisosSistemas("FACTURA GUARDAR RADICACION", idusuario) == 0) {
                return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
            }

            for (int x = 0; x < a_factura.length; x++) {
                encontrado = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT factura FROM factura_radicacion where factura = " + a_factura[x]));
                if (encontrado > 0) {
                    mensaje += "La factura número " + a_factura[x] + " ya fue radicada <br>";
                }
                sql += "INSERT INTO factura_radicacion(factura, radicacion, fecharad, idusuario, idmensajero, observacion)"
                        + "  VALUES (" + a_factura[x] + "," + contador + ",'" + fecha + "'," + idusuario + "," + mensajero + ",'" + observacion + "');";
                sql += "UPDATE factura set radicado = 1 where factura = " + a_factura[x] + ";";
            }

            if (!mensaje.equals("")) {
                return "1|" + mensaje;
            }

            sql += "UPDATE contadores SET radicacion=" + contador + ";";
            if (Globales.DatosAuditoria(sql, "FACTURACION", "RADICAR", idusuario, iplocal, this.getClass() + "-->GuardarRadicacion")) {
                return "0|Facturas enviada a radicar con el número |" + contador;
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }

        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String GuardarRecibirRadi(HttpServletRequest request) {
        String ids = (request.getParameter("ids"));
        String fechas = (request.getParameter("fechas"));
        String facturas = (request.getParameter("facturas"));

        int encontrado = 0;
        String mensaje = "";
        String[] a_id = ids.split(",");
        String[] a_fecha = fechas.split(",");
        String[] a_factura = facturas.split(",");
        try {

            if (Globales.PermisosSistemas("FACTURA RECIBIR RADICADO", idusuario) == 0) {
                return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
            }

            sql = "";
            for (int x = 0; x < a_fecha.length; x++) {
                encontrado = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT factura FROM factura_radicacion where id = " + a_id[x] + " and idusuariorec > 0"));
                if (encontrado > 0) {
                    mensaje += "La factura número " + encontrado + ", su radicado ya fue recibido <br>";
                }
                sql += "UPDATE factura_radicacion set fecharad='" + a_fecha[x] + "',  idusuariorec=" + idusuario + ", fecharec=now()"
                        + " WHERE id = " + a_id[x] + ";";
                sql += "UPDATE factura set estado='Radicado', fecharad = '" + a_fecha[x] + "' where factura = " + a_factura[x] + ";";

            }

            if (!mensaje.equals("")) {
                return "1|" + mensaje;
            }

            if (Globales.DatosAuditoria(sql, "FACTURACION", "RECIBIR RADICAR", idusuario, iplocal, this.getClass() + "-->GuardarRecibirRadi")) {
                return "0|Radicado recibido con éxito";
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }

        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String DetRemisionNoFacturado(HttpServletRequest request) {

        int remision = Globales.Validarintnonull(request.getParameter("remision"));

        sql = "select r.id, rd.observacion || '<br>'  || coalesce(rd.accesorio) as observacioning,   '<b>' || rd.sitio || '</b><br>' || rd.garantia || '<br><b>' || case when convenio = 1 then 'SI' ELSE 'NO' END || '</b>'  as garantia,"
                + "  '<a title=''Imprimir Remisión'' type=''button'' href=' || chr(34) || 'javascript:ImprimirRemision(' || r.remision || ',''' || r.estado || ''',0)' ||chr(34) || '>' || r.remision || '</a><br>' ||"
                + "   u.nombrecompleto || '<br><b>' || to_char(fechaing, 'yyyy/MM/dd HH24:MI') || '</b>' || case when r.pdf > 0 then '<br><a class=''text-success text-XX'' title=''Ver Remisión del Cliente'' href=''javascript:VerRemision(' || r.remision || ')''>PDF</a>' ELSE '' END as remision, '<b>' || rd.ingreso || '</b>' as ingreso, "
                + "  case when rd.entregado = 0 then '<b><font color=''red''>NO</font></b><br>' else ' ' end || tiempo(rd.fechaing, case when rd.entregado = 0  then now() else (select max(fechaentrega) FROM devolucion_detalle dd inner join devolucion d on d.id = dd.iddevolucion where dd.ingreso = rd.ingreso) end, 3) as tiempolab, "
                + "  case when rd.entregado = 0 then '<b><font color=''red''>NO</font></b><br>' else ' ' end || tiempo_aprobacion(rd.ingreso, 0, rd.fechaaproajus, rd.fechaaprocoti, rd.fechaing, case when rd.entregado = 0  then now() else (select max(fechaentrega) FROM devolucion_detalle dd inner join devolucion d on d.id = dd.iddevolucion where dd.ingreso = rd.ingreso) end, 3) as tiempoapro, "
                + "  c.documento || '<br><b>' || c.nombrecompleto || '</b><br> <span class=''text-primary''>(' || ua.nomusu || ')</span>' || case when habitual = 'SI' THEN '<br><b>Habitual</b>' else case when aprobar_cotizacion = 'NO' THEN '<br>no.re.ap.co' ELSE '<br><font color=''red''><b>No Habitual</b></font>' END END as cliente,"
                + "  '<b>' || m.descripcion || '</b><br>' || e.descripcion || '<br><b>' || ma.descripcion || '</b><br>' || mo.descripcion as equipo,"
                + "  '<b>' || case when rd.idintervalo = 0 then 'VER ESPECIFICACIONES' ELSE '(' || desde || ' a ' || hasta || ') ' || medida end || '</b><br>' || rd.serie as rango,"
                + "  case when rd.certificado = 1 then '<a href=''javascript:VerDetalle(6,' || rd.ingreso || ')'' title=''Ver Certificado''>' || (select min(numero) from certificados ce where ce.ingreso = rd.ingreso) || '<br>Ver Detalle</a>' else case when calibracion = 2 then 'NO Apto <br> Para Calibración' ELSE CASE WHEN rd.reportado = 0 then 'NO REPORTADO' ELSE tiempo_aprobacion(rd.ingreso, 0, rd.fechaaproajus, rd.fechaaprocoti,(select min(fecha) from reporte_ingreso ri where ri.ingreso = rd.ingreso), now(),2) END END END  as certificado,"
                + "  case when rd.facturado> 0 then '<a href=''javascript:VerDetalle(9,' || rd.ingreso || ')'' title=''Ver Factura''>' || rd.facturado || '<br>Ver Detalle</a>' else 'NO' END  as facturado,"
                + "  case when rd.entregado = 1 and rd.salida = 1 then '<a href=''javascript:VerDetalle(11,' || rd.ingreso || ')'' title=''Ver Ingrega''>SI<br>Ver Detalle</a>' else 'NO' END  as entregado,"
                + "  case when rd.fotos > 0 then '<img src=''imagenes/ingresos/' || rd.ingreso || '/1.jpg'' onclick=''LlamarFotoDet(' || rd.ingreso || ',' || rd.fotos || ')'' width=''80px''/><br>' else '' end || '<b>' || case when salida = 0 then coalesce((SELECT y || x FROM ingreso_ubicacion u WHERE u.ingreso = rd.ingreso),'') else '' end || '</b>' as imagen,"
                + "  case when rd.informetecnico > 0 then '<a href=''javascript:VerDetalle(12,' || rd.ingreso || ')'' title=''Ver Informe Técnico Laboratorio''>' || rd.informetecnico || '<br>Ver Detalle</a>' else CASE WHEN rd.recibidolab = 0 THEN 'NO<BR>RECIBIDO' ELSE case when rd.reportado = 1 and rd.calibracion = 0 then  tiempo_aprobacion(rd.ingreso, 0, rd.fechaaproajus, rd.fechaaprocoti,(select fecha from ingreso_recibidolab ir where ir.ingreso = rd.ingreso), now(),2) ELSE 'NO <BR> REQUERIDO' END  END END  as informe,"
                + "  case when rd.informetercerizado <> '0' then '<a href=''javascript:VerDetalle(17,' || rd.ingreso || ')'' title=''Ver Informe Técnico Tercerizado''>' || rd.informetercerizado || '<br>Ver Detalle</a>' else 'NO<BR>REQUERIDO' END  as informetercerizado,"
                + "  case when rd.noautorizado > 0 then coalesce((SELECT to_char(ri.fecha,'yyyy-MM-dd') || '<br>' || uri.nombrecompleto from reporte_ingreso ri inner join seguridad.rbac_usuario uri on uri.idusu = ri.idusuario and ri.ingreso = rd.ingreso and ri.idconcepto = 6),'') else '' end  as noautorizado"
                + "  from remision r inner join seguridad.rbac_usuario u on r.idusuario = u.idusu"
                + "	                    inner join clientes c on c.id = r.idcliente"
                + "	                    inner join clientes_sede cs on cs.id = r.idsede"
                + "	                    inner join clientes_contac cc on cc.id = r.idcontacto"
                + "	                    inner join ciudad ci on ci.id = cs.idciudad"
                + "	                    inner join departamento d on d.id = ci.iddepartamento"
                + "	                    inner join remision_detalle rd on rd.idremision = r.id "
                + "           inner join equipo e on e.id = rd.idequipo"
                + "	                    inner join modelos mo on mo.id = rd.idmodelo "
                + "           inner join marcas ma on ma.id = mo.idmarca"
                + "          inner join magnitudes m on m.id = e.idmagnitud"
                + "          inner join seguridad.rbac_usuario ua on c.asesor = ua.idusu"
                + "         inner join magnitud_intervalos i on i.id = rd.idintervalo"
                + " WHERE r.remision = " + remision + " ORDER BY rd.ingreso";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->DetRemisionNoFacturado");
    }

    public String ActualizarRetencion(HttpServletRequest request) {
        int tipo = Globales.Validarintnonull(request.getParameter("tipo"));
        int idretencion = Globales.Validarintnonull(request.getParameter("idretencion"));
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));

        if (Globales.PermisosSistemas("CLIENTE ACTUALIZAR RETENCION", idusuario) == 0) {
            return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
        }

        switch (tipo) {
            case 1: //FUENTE
                sql = "UPDATE clientes set retefuente = " + idretencion + " where id = " + cliente;
                break;
            case 2: //IVA
                sql = "UPDATE clientes set reteiva = " + idretencion + " where id = " + cliente;
                break;
            case 3: //ICA
                sql = "UPDATE clientes set reteica = " + idretencion + " where id = " + cliente;
                break;
        }

        if (Globales.DatosAuditoria(sql, "CLIENTE", "ACTUALIZAR RETENCION", idusuario, iplocal, this.getClass() + "-->ActualizarRetencion")) {
            return "0|Reteción actualizada con éxtio";
        } else {
            return "1|Error inesperado... Revise el log de errores";
        }
    }

    public String FacturaOrdenes(HttpServletRequest request) {
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        String habitual = (request.getParameter("habitual"));

        sql = "SELECT DISTINCT numero, numero"
                + "  FROM orden_compra oc inner join remision_detalle rd on rd.ingreso = oc.ingreso"
                + "		                     inner join cotizacion_detalle cd on cd.ingreso = rd.ingreso"
                + "		                     inner join cotizacion c on c.id = cd.idcotizacion and c.estado in (" + (habitual.equals("SI") ? "'Cerrado'," : "") + "'Aprobado')"
                + "                 left join clientes_dependencia cdp on cdp.idcliente = oc.idcliente"
                + "  where (oc.idcliente = " + cliente + " or cdp.idclientedep = " + cliente + ") and rd.facturado <= 0  ORDER BY 1";
        return Globales.ObtenerCombo(sql, 6, 0, 0);
    }

    public String OperacionImportarFactura(HttpServletRequest request) {

        int operacion = Globales.Validarintnonull(request.getParameter("operacion"));
        int IId = Globales.Validarintnonull(request.getParameter("IId"));
        int IFactura = Globales.Validarintnonull(request.getParameter("IFactura"));
        int IIngresos = Globales.Validarintnonull(request.getParameter("IIngresos"));
        String IFecha = request.getParameter("IFecha");
        double ISubTotal = Globales.ValidardoublenoNull(request.getParameter("ISubtotal"));
        double IDescuento = Globales.ValidardoublenoNull(request.getParameter("Idescuento"));
        double IAjuste = Globales.ValidardoublenoNull(request.getParameter("IAjuste"));
        double ISuministro = Globales.ValidardoublenoNull(request.getParameter("Isuministro"));

        try {

            if (operacion == 1) {
                if (IId == 0) {
                    if (Globales.PermisosSistemas("FACTURACION IMPORTAR AGREGAR", idusuario) == 0) {
                        return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                    }
                    sql += "INSERT INTO importado_factura(ingreso, factura, idusuario, fecha, subtotal, descuento, ajuste, suministro)"
                            + "  VALUES (" + IIngresos + "," + IFactura + "," + idusuario + ",'" + IFecha + "'," + ISubTotal + "," + IDescuento + "," + IAjuste + "," + ISuministro + ");";
                    sql += "UPDATE remision_detalle set facturado = " + IFactura + " WHERE ingreso = " + IIngresos + " AND facturado = 0;";
                    if (Globales.DatosAuditoria(sql, "FACTURACION", "IMPORTAR AGREGAR", idusuario, iplocal, this.getClass() + "-->OperacionImportarFactura")) {
                        return "0|Ingresos facturados con éxito";
                    } else {
                        return "1|Error inesperado... Revise el log de errores";
                    }
                } else {
                    if (Globales.PermisosSistemas("FACTURACION IMPORTAR ACTUALIZAR", idusuario) == 0) {
                        return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                    }
                    sql = "UPDATE importado_factura SET ingreso = " + IIngresos + ", factura = " + IFactura + ", fecha = '" + IFecha
                            + "', subtotal=" + ISubTotal + ",descuento=" + IDescuento + ",ajuste=" + IAjuste + ", suministro=" + ISuministro + " where id = " + IId + ";";
                    sql += "UPDATE remision_detalle set facturado = " + IFactura + " WHERE ingreso = " + IIngresos;
                    if (Globales.DatosAuditoria(sql, "FACTURACION", "IMPORTAR EDITAR", idusuario, iplocal, this.getClass() + "-->OperacionImportarFactura")) {
                        return "0|Facturas de ingresos actualizado con éxito";
                    } else {
                        return "1|Error inesperado... Revise el log de errores";
                    }
                }
            } else {
                if (Globales.PermisosSistemas("FACTURACION IMPORTAR ELIMINAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "DELETE FROM importado_factura where id = " + IId + ";";
                sql += "UPDATE remision_detalle set facturado = 0 WHERE ingreso = " + IIngresos;
                if (Globales.DatosAuditoria(sql, "FACTURACION", "IMPORTAR ELIMINAR", idusuario, iplocal, this.getClass() + "-->OperacionImportarFactura")) {
                    return "0|Factura de ingreso eliminado con éxito";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            }
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String TablaFacturacion(HttpServletRequest request) {

        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        String orden = request.getParameter("orden");
        int id = Globales.Validarintnonull(request.getParameter("id"));
        String habitual = request.getParameter("habitual");

        String busqueda = " and oc.numero = '" + orden + "'";

        sql = "select DISTINCT cd.cantidad, cd.precio, cd.descuento,  (cd.cantidad*cd.precio) - (cd.cantidad*cd.precio*cd.descuento/100) as subtotal, "
                + "           quitarcaracter(s.nombre || ' DE ' || e.descripcion || ', <b>MARCA:</b> ' || ma.descripcion || ', <b>MODELO:</b> ' || mo.descripcion || ', <b>SERIE:</b> ' || coalesce(cd.serie,'NA') "
                + "           || ', <b>INTERVALO:</b> ' || case when cd.idintervalo = 0 then 'VER ESPECIFICACIONES' ELSE medida || ' ' || desde || ' - ' || hasta end) as servicio, c.id, cd.ingreso, "
                + "   obtener_certificados(rd.ingreso) as certificado, c.cotizacion, oc.numero as orden, idserviciodep, r.remision, cd.iva, 1  as tipo, idcentro, obtener_certificadospdf(rd.ingreso) as certificadospdf, s.idcuecontable, s.id as idservicio, 0 as idarticulo, 0 as idbodega "
                + "   from cotizacion c INNER JOIN cotizacion_detalle cd on c.id = cd.idcotizacion and c.estado in (" + (habitual == "SI" ? "'Cerrado'," : "") + "'Aprobado') "
                + "                     inner join clientes cli on cli.id = c.idcliente "
                + "                     inner join remision_detalle rd on rd.ingreso = cd.ingreso  "
                + "                     inner join remision r on r.id = rd.idremision "
                + "	                              inner join equipo e on cd.idequipo = e.id "
                + "			                      inner join magnitudes m on m.id = e.idmagnitud "
                + "			                      inner join seguridad.rbac_usuario u on u.idusu = cd.idusuario "
                + "			                      inner join servicio s on s.id = cd.idservicio "
                + "			                      inner join modelos mo on mo.id = cd.idmodelo "
                + "			                      inner join marcas ma on ma.id = mo.idmarca "
                + "                     inner join magnitud_intervalos mi on mi.id = cd.idintervalo "
                + "                     left join orden_compra oc on oc.ingreso = cd.ingreso and oc.ncotizacion = c.cotizacion "
                + "			                      left join proveedores p on p.id = cd.idproveedor "
                + "                     left join clientes_dependencia cdp on cdp.idcliente = cli.id "
                + "    where (c.idcliente = " + cliente + " or cdp.idclientedep = " + cliente + ") and cd.ingreso > 0 and  idserviciodep = 0 and rd.facturado = 0 " + busqueda
                + "   union "
                + "   select cd.cantidad, cd.precio, cd.descuento, (cd.cantidad*cd.precio) - (cd.cantidad*cd.precio*cd.descuento/100) as subtotal,  "
                + "           quitarcaracter(s.descripcion || ' DE ' || e.descripcion || ', <b>MARCA:</b> ' || ma.descripcion || ', <b>MODELO:</b> ' || mo.descripcion || ', <b>SERIE:</b> ' || coalesce(cd.serie,'NA') "
                + "           || ', <b>INTERVALO:</b> ' || case when cd.idintervalo = 0 then 'VER ESPECIFICACIONES' ELSE medida || ' ' || desde || ' - ' || hasta end || "
                + "       case when cd.observacion is null then '' else ' <b>DETALLE:</b> ' || coalesce(quitarenter(cd.observacion),'NA') end) as servicio, "
                + "       c.id, cd.ingreso, obtener_certificados(rd.ingreso) as certificado, c.cotizacion, oc.numero as orden, idserviciodep, r.remision, cd.iva, 1  as tipo, idcentro, obtener_certificadospdf(rd.ingreso) as certificadospdf, s.idcuecontable, s.id as idservicio, 0 as idarticulo, 0 as idbodega "
                + "   from cotizacion c INNER JOIN cotizacion_detalle cd on c.id = cd.idcotizacion and  c.estado in (" + (habitual.equals("SI") ? "'Cerrado'," : "") + "'Aprobado') "
                + "	                              inner join equipo e on cd.idequipo = e.id "
                + "                     inner join remision_detalle rd on rd.ingreso = cd.ingreso "
                + "                     inner join remision r on r.id = rd.idremision "
                + "			                      inner join magnitudes m on m.id = e.idmagnitud "
                + "			                      inner join seguridad.rbac_usuario u on u.idusu = cd.idusuario "
                + "			                      inner join otros_servicios s on s.id = cd.idservicio "
                + "			                      inner join modelos mo on mo.id = cd.idmodelo  "
                + "			                      inner join marcas ma on ma.id = mo.idmarca "
                + "                     inner join magnitud_intervalos mi on mi.id = cd.idintervalo "
                + "                     left join orden_compra oc on oc.ingreso = cd.ingreso "
                + "                     left join proveedores p on p.id = cd.idproveedor "
                + "                     left join clientes_dependencia cdp on cdp.idcliente = c.idcliente "
                + "   where (c.idcliente = " + cliente + " or cdp.idclientedep = " + cliente + ") and  idserviciodep > 0  and idserviciodep <> 99999 and rd.facturado = 0 " + busqueda
                + "    UNION "
                + "    select DISTINCT cd.cantidad, cd.precio, cd.descuento, (cd.cantidad*cd.precio) - (cd.cantidad*cd.precio*cd.descuento/100) as subtotal,  "
                + "           quitarcaracter(s.descripcion || case when cd.observacion is null then '' else ' <b>DETALLE:</b> ' || coalesce(quitarenter(cd.observacion),'NA') end) as servicio, "
                + "       c.id, cd.ingreso, '' as certificado, c.cotizacion, '" + orden + "' as orden, idserviciodep, 0 AS remision, cd.iva, 1, 0 as idcentro, '' as certificadospdf, s.idcuecontable, s.id as idservicio, 0 as idarticulo, 0 as idbodega "
                + "   from cotizacion c INNER JOIN cotizacion_detalle cd on c.id = cd.idcotizacion and c.estado in (" + (habitual.equals("SI") ? "'Cerrado'," : "") + "'Aprobado') "
                + "	                                inner join otros_servicios s on s.id = cd.idservicio "
                + "                       left join clientes_dependencia cdp on cdp.idcliente = c.idcliente "
                + "       where (c.idcliente = " + cliente + " or cdp.idclientedep = " + cliente + ") and  idserviciodep = 99999 and '" + orden + "' <> 'AUTORIZACIÓN' AND cd.facturado = 0 and c.cotizacion in "
                + "        (SELECT cotizacion "
                + "					FROM cotizacion c1 INNER JOIN cotizacion_detalle cd1 on c1.id = cd1.idcotizacion and c1.estado in (" + (habitual.equals("SI") ? "'Cerrado'," : "") + "'Aprobado') "
                + "					 				    inner join remision_detalle rd on rd.ingreso = cd1.ingreso "
                + "									    inner join orden_compra oc on oc.ingreso = cd1.ingreso "
                + "                           left join clientes_dependencia cdp on cdp.idcliente = c1.idcliente "
                + "					where (c1.idcliente = " + cliente + " or cdp.idclientedep = " + cliente + ") and idserviciodep = 0 and rd.facturado = 0) "
                + "   UNION "
                + "   select cantidad, f.precio, f.descuento, f.subtotal, case when idservicio > 0 then '<b>' || s.descripcion || ':</b> ' else '' end  || f.descripcion as descripcion, f.id, ingreso, certificado, cotizacion, orden, 0, 0, iva, 2 as tipo, idcentro, '' as certificadospdf, f.idcuecontable, idservicio, idarticulo, idbodega "
                + "   from factura_temporal f inner join otros_servicios s on s.id = f.idservicio "
                + "   where idcliente = " + cliente + " and orden = '" + orden + "' and idfactura = " + id
                + "   ORDER BY 6, tipo, ingreso desc, idserviciodep";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->TablaFacturacion");

    }

    public String GuardarFactura(HttpServletRequest request) {
        int id = Globales.Validarintnonull(request.getParameter("id"));
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        String orden = request.getParameter("orden");
        double total = Globales.ValidardoublenoNull(request.getParameter("total"));
        double subtotal = Globales.ValidardoublenoNull(request.getParameter("subtotal"));
        double descuento = Globales.ValidardoublenoNull(request.getParameter("descuento"));
        double gravable = Globales.ValidardoublenoNull(request.getParameter("gravable"));
        double exento = Globales.ValidardoublenoNull(request.getParameter("exento"));
        double iva = Globales.ValidardoublenoNull(request.getParameter("iva"));
        double reteiva = Globales.ValidardoublenoNull(request.getParameter("reteiva"));
        double retefuente = Globales.ValidardoublenoNull(request.getParameter("retefuente"));
        double reteica = Globales.ValidardoublenoNull(request.getParameter("reteica"));
        String observacion = request.getParameter("observacion");
        String formapago = request.getParameter("formapago");
        String fechafac = request.getParameter("fechafac");
        String a_ingreso = request.getParameter("a_ingreso");
        String a_orden = request.getParameter("a_orden");
        String a_certificado = request.getParameter("a_certificado");
        String a_cotizacion = request.getParameter("a_cotizacion");
        String a_precio = request.getParameter("a_precio");
        String a_cantidad = request.getParameter("a_cantidad");
        String a_descuento = request.getParameter("a_descuento");
        String a_iva = request.getParameter("a_iva");
        String a_subtotal = request.getParameter("a_subtotal");
        String a_descripcion = request.getParameter("a_descripcion");
        String a_centro = request.getParameter("a_centro");
        String a_cuenta = request.getParameter("a_cuenta");
        String a_servicio = request.getParameter("a_servicio");
        String a_articulos = request.getParameter("a_articulos");
        String a_bodegas = request.getParameter("a_bodegas");
        double porreteiva = Globales.ValidardoublenoNull(request.getParameter("porreteiva"));
        double porretefuente = Globales.ValidardoublenoNull(request.getParameter("porretefuente"));
        double porreteica = Globales.ValidardoublenoNull(request.getParameter("porreteica"));
        String a_ccuenta = request.getParameter("a_ccuenta");
        String a_cdebito = request.getParameter("a_cdebito");
        String a_ccredito = request.getParameter("a_ccredito");
        String a_cconcepto = request.getParameter("a_cconcepto");
        String a_ccentro = request.getParameter("a_ccentro");
        double tdebito = Globales.ValidardoublenoNull(request.getParameter("tdebito"));
        double tcredito = Globales.ValidardoublenoNull(request.getParameter("tcredito"));
        try {
            sql = " SELECT * FROM guardar_factura(" + id + ",'" + fechafac + "'," + cliente + ",'" + orden + "'," + total + "," + subtotal + "," + descuento + "," + gravable
                    + "," + exento + "," + iva + "," + reteiva + "," + retefuente + "," + reteica + "," + porreteiva + "," + porretefuente + "," + porreteica + ",'" + observacion
                    + "'," + a_ingreso + "," + a_descripcion + "," + a_orden + "," + a_certificado + "," + a_cotizacion + "," + a_precio + "," + a_cantidad + "," + a_descuento
                    + "," + a_iva + "," + a_subtotal + "," + a_centro + "," + a_cuenta + "," + a_servicio + "," + a_articulos + "," + a_bodegas + "," + idusuario + ","
                    + a_ccuenta + "," + a_cdebito + "," + a_ccredito + "," + a_cconcepto + "," + a_ccentro + "," + tdebito + "," + tcredito + ")";

            datos = Globales.Obtenerdatos(sql, this.getClass() + "-->GuardarFactura", 1);
            datos.next();
            if (datos.getString("_factura").equals("0")) {
                return "1|" + datos.getString("_mensaje");
            } else {
                return "0|Factura guardada y contabilizada con el número|" + datos.getString("_factura") + "|" + datos.getString("_idfactura");
            }
        } catch (Exception ex) {
            return "9|" + ex.getMessage();
        }
    }

    public String EnvioFactura(HttpServletRequest request) {
        try {
            String principal = request.getParameter("principal");
            int factura = Globales.Validarintnonull(request.getParameter("factura"));
            String e_email = request.getParameter("e_email");
            String archivo = request.getParameter("archivo");
            String observacion = request.getParameter("observacion");
            int error = 0;

            sql = "SELECT clave, c.nombrecompleto as cliente, u.nombrecompleto as usuario, u.cargo "
                    + "  from clientes c inner join factura fa on fa.idcliente = c.id "
                    + "                  INNER JOIN seguridad.rbac_usuario u on u.idusu = fa.idusuario "
                    + "  where factura = " + factura;
            datos = Globales.Obtenerdatos(sql, this.getClass() + "-->EnvioFactura", 1);
            datos.next();
            String ClaveAcceso = datos.getString("clave");
            String cliente = datos.getString("cliente");
            String nomusuario = datos.getString("usuario");
            String cargo = datos.getString("cargo");

            e_email += ";" + correoenvio;
            String[] email = e_email.split(";");

            String correoemp = correoenvio;

            DateFormat formato = new SimpleDateFormat("HH");

            int hora = Globales.Validarintnonull(formato.format(new Date()));
            String saludo = "";
            if (hora <= 11) {
                saludo = "Buenos días";
            } else if (hora <= 18) {
                saludo = "Buenas tardes";
            } else {
                saludo = "Buenas noches";
            }

            String nombrearchivo = "FCS-" + factura + ".pdf";

            if ((correoemp.trim().equals("")) || (clavecorreo.trim().equals(""))) {
                return "1|Debe configurar los parámetros: Correo envio, Clave correo envio";
            }

            String mensaje = saludo + "<br><br><b>SEÑORES</b><br>" + cliente + "<br><br>"
                    + "Reciba un cordial saludo por parte de nuestro equipo de trabajo.<br><br>"
                    + "Se adjunta factura número <b>" + factura + "</b> para su pago oportuno. "
                    + "<br><br>Cordialmente.<br><br>"
                    + "<img src='http://190.144.188.100:82/imagenes/logo.png' width='30%'><br><b>" + nomusuario + "</b><br>Calibration Service SAS<br>" + cargo + "<br><font color='blue'>+57 (1) 2047699 - 7285146 <br>3138141058</font><br><br>";

            if (!observacion.equals("")) {
                mensaje += "<b>OBSERVACIÓN DE ENVÍO:</b><br>" + observacion;
            }

            /*mensaje += "<br><br>Debe de ingresar a su cuenta de <b>CALIBRATION SERVICE SAS</b> para aprobar o no la cotización a través del link <br><br" +
            "<a href='http://190.144.188.100:8080/usuarios/index.php'>http://190.144.188.100:8080/usuarios/index.php</a><br><br>" +
            "Sus datos para ingresar son los siguientes: <br><b>usuario</b> " + principal +
            "<br><b>Clave de acceso</b> " + ClaveAcceso;*/
            String adjunto = Globales.ruta_archivo + "DocumPDF/" + archivo;

            String correo = Globales.EnviarEmail(principal, Globales.Nombre_Empresa, email, "Envío de Factura Número " + factura, mensaje, correoemp, adjunto, correoemp, clavecorreo, nombrearchivo);

            if (!correo.trim().equals("")) {
                return "1|" + correo;
            }

            sql = " UPDATE factura set estado = 'Enviado', fechaenvio = now(),  idusuarioenvia = " + idusuario + ", correoenvia = '" + principal + " " + e_email + "' WHERE factura = " + factura + " and estado = 'Facturado';";

            Globales.DatosAuditoria(sql, "FACTURA", "ENVIAR CORREO", idusuario, iplocal, this.getClass() + "-->EnvioFactura");
            return error + "|Correo enviado a|" + e_email;
        } catch (SQLException ex) {
            return "1|" + ex.getMessage();
        }

    }

    public String ReporteGenFactura(HttpServletRequest request) {
        int opcion = Globales.Validarintnonull(request.getParameter("opciones"));
        int dias = Globales.Validarintnonull(request.getParameter("dias"));
        String fechad = request.getParameter("fechad");
        String fechah = request.getParameter("fechah");
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        int asesor = Globales.Validarintnonull(request.getParameter("asesor"));
        int departamento = Globales.Validarintnonull(request.getParameter("departamento"));
        int ciudad = Globales.Validarintnonull(request.getParameter("ciudad"));
        String estado = request.getParameter("estado");
        int usuarios = Globales.Validarintnonull(request.getParameter("usuario"));
        int factura = Globales.Validarintnonull(request.getParameter("factura"));
        int ingreso = Globales.Validarintnonull(request.getParameter("ingreso"));
        int cotizacion = Globales.Validarintnonull(request.getParameter("cotizacion"));
        String orden = request.getParameter("orden");
        int tipo = Globales.Validarintnonull(request.getParameter("tipo"));
        String Busqueda = " where 1 = 1 ";
        String Busqueda2 = "";

        if (dias > 0) {
            Date fechaf = new Date();
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(fechaf);
            calendar.add(Calendar.DAY_OF_YEAR, (dias * -1));
            Date fechai = calendar.getTime();
            fechad = dateFormat.format(fechai);
            fechah = dateFormat.format(fechaf);
        }

        String titulo = "Fecha Inicial " + fechad + " Fecha Final " + fechah;

        if (factura != 0) {
            Busqueda += " AND f.factura = " + factura;
            titulo += ", Facura: " + factura;
        } else if (!orden.equals("")) {
            Busqueda += " AND f.orden = '" + orden + "'";
            titulo += ", Orden: " + orden;
        } else if (cotizacion != 0) {
            Busqueda += " AND fd.cotizacion = " + cotizacion;
            titulo += ", Cotización: " + cotizacion;
        } else if (ingreso != 0) {
            Busqueda += " AND fd.ingreso = " + ingreso;
            titulo += ", Ingreso: " + ingreso;
        } else {
            if (cliente > 0) {
                Busqueda += " and f.idcliente = " + cliente;
                Busqueda2 += " and idcliente = " + cliente;
            }

            if (!estado.equals("T")) {
                if (!estado.equals("Vencida")) {
                    Busqueda += " and extract(days from (now()-vencimiento)) > 0";
                } else {
                    Busqueda += " and f.estado = '" + estado + "'";
                }

                titulo += ", estado: " + estado;
            }

            if (tipo != 0) {
                Busqueda += " and tc.id = " + tipo;
            }

            if (!fechad.trim().equals("")) {
                Busqueda += " and to_char(f.fecha,'yyyy-MM-dd') >= '" + fechad + "'";
            }
            if (!fechah.trim().equals("")) {
                Busqueda += " and to_char(f.fecha,'yyyy-MM-dd') <= '" + fechah + "'";
            }
            if (asesor > 0) {
                Busqueda += " AND cli.asesor = " + asesor;
            }

            if (ciudad > 0) {
                Busqueda += " AND ci.id = " + ciudad;
            }

            if (departamento > 0) {
                Busqueda += " AND d.id = " + departamento;
            }

            if (usuarios > 0) {
                Busqueda += " AND cf.idusuario = " + usuarios;
            }
        }

        if (opcion == 1) {
            sql = "SELECT cli.nombrecompleto || ' (' || cli.documento || ')' AS cliente, factura, f.subtotal, f.descuento, f.iva, f.total, "
                    + "      f.retefuente,  f.reteica,  f.reteiva, "
                    + "      f.total + f.retefuente +  f.reteica + f.reteiva  as totalretecion, "
                    + "      ci.descripcion  as ciudad, d.descripcion as departamento, "
                    + "      f.orden, cli.Telefono || ' ' || cli.celular as telefono, cli.direccion, cli.email, "
                    + "      to_char(f.fecha,'yyyy/MM/dd HH24:MI') as fecha, "
                    + "      case when f.saldo > 0 then tiempo(f.vencimiento, now(), 5) else '' end as tiempo, "
                    + "      to_char(f.vencimiento,'yyyy/MM/dd') as vencimiento, "
                    + "      to_char(f.fecharad,'yyyy/MM/dd') as fecharad, "
                    + "      to_char(f.fecharad,'yyyy/MM/dd') as fecharad, f.observacion,obseranula, "
                    + "      to_char(f.fechaenvio,'yyyy/MM/dd HH24:MI')  as fechaenvio,  ue.nombrecompleto as usuarioenvio, "
                    + "      to_char(f.fechaanula,'yyyy/MM/dd HH24:MI')  as fechaanula, ua.nombrecompleto as usuarioanula, "
                    + "      (select to_char(fr.fechareg,'yyyy/MM/dd HH24:MI')  || '<br>' || urr.nombrecompleto "
                    + "      from factura_radicacion fr inner join seguridad.rbac_usuario urr on urr.idusu = fr.idusuario and fr.factura = f.factura) as registrorad, "
                    + "  (select to_char(fr.fecharec,'yyyy/MM/dd HH24:MI')  || '<br>' || urc.nombrecompleto "
                    + "      from factura_radicacion fr inner join seguridad.rbac_usuario urc on urc.idusu = fr.idusuariorec and fr.factura = f.factura) as recibidorad, "
                    + "      u.nombrecompleto AS usuario, tc.descripcion as tipo, diaspago, saldo, f.total - f.saldo as abonado, f.estado,correoenvia, "
                    + "      case when max(fd.id) is null then '' else "
                    + "      '<button class=''btn btn-glow btn-success imprimir'' title=''Imprimir Factura'' type=''button'' onclick=' || chr(34) || 'ImprimirFactura(' || f.factura || ')' ||chr(34) || '><span data-icon=''&#xe0f7;''></span></button>' end as imprimir "
                    + "  FROM factura f inner JOIN clientes cli on cli.id = f.idcliente "
                    + "		                   inner JOIN ciudad ci ON ci.id = cli.idciudad "
                    + "		                   inner JOIN departamento d ON d.id = ci.iddepartamento "
                    + "		                   inner JOIN seguridad.rbac_usuario u on u.idusu = f.idusuario "
                    + "		                   inner JOIN seguridad.rbac_usuario ua on ua.idusu = f.idusuarioanula "
                    + "		                   inner JOIN seguridad.rbac_usuario ue on ue.idusu = f.idusuarioenvia "
                    + "		                   inner join tipocliente tc on tc.id = f.tipopago::integer "
                    + "		                   left join factura_datelle fd on f.id = fd.idfactura " + Busqueda
                    + "  group by cli.nombrecompleto, cli.documento, "
                    + "          cli.telefono, ci.descripcion,f.factura, f.subtotal, f.descuento, f.iva, f.total, f.orden, "
                    + "      f.retefuente,  f.reteica,  f.reteiva,  "
                    + "          cli.direccion, cli.Telefono, cli.email, f.fecha, f.vencimiento, f.fechaenvio, f.fechaanula,obseranula,correoenvia, "
                    + "          ue.nombrecompleto, ua.nombrecompleto, u.nombrecompleto, f.fecharad, tc.descripcion, diaspago, saldo, cli.celular, "
                    + "          f.estado, d.descripcion, diaspago, tc.descripcion, f.observacion ORDER BY f.factura";

            return "0||" + Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ReporteGenFactura");
        } else {
            return "1||xx";
        }

    }

    protected void Servidor(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        String ruta_origen = request.getHeader("Origin");

        HttpSession misession = request.getSession();
        idusuario = misession.getAttribute("idusuario").toString();
        iplocal = misession.getAttribute("IPLocal").toString();
        usuario = misession.getAttribute("codusu").toString();
        correoenvio = misession.getAttribute("correoenvio").toString();
        clavecorreo = misession.getAttribute("clavecorreo").toString();
        if (misession.getAttribute("ArchivosAdjuntos") != null) {
            archivoadjunto = misession.getAttribute("ArchivosAdjuntos").toString();
        }
        response.setHeader("Access-Control-Allow-Origin", request.getHeader("Origin"));
        response.setHeader("Access-Control-Allow-Credentials", "true");

        try (PrintWriter out = response.getWriter()) {
            if (Globales.ValidarRuta(ruta_origen) == false) {
                out.print("[{\"error\":\"505\",\"mensaje\":\"Usted no tiene permiso para acceder a esta ruta\"}]");
            } else if (misession.getAttribute("codusu") == null) {
                out.println("cerrada");
            } else if (!Globales.IpUsuario(misession.getAttribute("idusuario").toString()).equals(misession.getAttribute("IPLocal").toString())) {
                out.println("cambio-ip");
            } else {
                String opcion = request.getParameter("opcion");
                switch (opcion) {
                    case "TablaFacturaImportada":
                        out.println(TablaFacturaImportada(request));
                        break;
                    case "ConsultarFacRadicada":
                        System.out.println(request);
                        out.println(ConsultarFacRadicada(request));
                        break;
                    case "ConsultarFactura":
                        System.out.println(request);
                        out.println(ConsultarFactura(request));
                        break;
                    case "ConsultarFacturaPen":
                        out.println(ConsultarFacturaPen(request));
                        break;
                    case "ConsultarFacPorRadicar":
                        out.println(ConsultarFacPorRadicar(request));
                        break;
                    case "ConsultarIngresoFac":
                        out.println(ConsultarIngresoFac(request));
                        break;
                    case "ConsultarRadicada":
                        out.println(ConsultarRadicada(request));
                        break;
                    case "GuardaTempFactura":
                        out.println(GuardaTempFactura(request));
                        break;
                    case "FacturasAnuladas":
                        out.println(FacturasAnuladas(request));
                        break;
                    case "RemisionNoFacturado":
                        out.println(RemisionNoFacturado(request));
                        break;
                    case "ClientesIngresos":
                        out.println(ClientesIngresos(request));
                        break;
                    case "BuscarFactura":
                        out.println(BuscarFactura(request));
                        break;
                    case "ResumenGraficaFactura":
                        out.println(ResumenGraficaFactura(request));
                        break;
                    case "EliminarTempFactura":
                        out.println(EliminarTempFactura(request));
                        break;
                    case "TablaFacturaSinRelacionar":
                        out.println(TablaFacturaSinRelacionar(request));
                        break;
                    case "Contabilizar_Factura":
                        out.println(Contabilizar_Factura(request));
                        break;
                    case "AnularContabilizacion":
                        out.println(AnularContabilizacion(request));
                        break;
                    case "BuscarProveedor":
                        out.println(BuscarProveedor(request));
                        break;
                    case "AnularOrden":
                        out.println(AnularOrden(request));
                        break;
                    case "ReemplazarOrden":
                        out.println(ReemplazarOrden(request));
                        break;
                    /*    
                            case "EnvioSoliCert":
                                out.println(EnvioSoliCert(request));
                                break;
                            case "EnvioFactura":
                                out.println(EnvioFactura(request));
                                break;    
                            case " EnvioOrdenCompra":
                                out.println( EnvioOrdenCompra(request));
                                break;
                     */
                    case "BuscarOIngreso":
                        out.println(BuscarOIngreso(request));
                        break;
                    case "EliminarOIngreso":
                        out.println(EliminarOIngreso(request));
                        break;
                    case "BuscarOrdenTercero":
                        out.println(BuscarOrdenTercero(request));
                        break;
                    case "DescripcionDetalleOrden":
                        out.println(DescripcionDetalleOrden(request));
                        break;
                    case "TablaOrdenTercer":
                        out.println(TablaOrdenTercer(request));
                        break;
                    case "GuardarOtroRegistro":
                        out.println(GuardarOtroRegistro(request));
                        break;
                    case "ConsultarOrdenTercero":
                        out.println(ConsultarOrdenTercero(request));
                        break;
                    case "ConsultarIngresoOC":
                        out.println(ConsultarIngresoOC(request));
                        break;
                    case "GuardarDetFactura":
                        out.println(GuardarDetFactura(request));
                        break;
                    case "GraficaFechaOC":
                        out.println(GraficaFechaOC(request));
                        break;
                    case "GraficaFechaOCMag":
                        out.println(GraficaFechaOCMag(request));
                        break;
                    case "GraficaOCMag":
                        out.println(GraficaOCMag(request));
                        break;
                    case "GraficaOCEquipo":
                        out.println(GraficaOCEquipo(request));
                        break;
                    case "GuardarRadicacion":
                        out.println(GuardarRadicacion(request));
                        break;
                    case "GuardarRecibirRadi":
                        out.println(GuardarRecibirRadi(request));
                        break;
                    case "DetRemisionNoFacturado":
                        out.println(DetRemisionNoFacturado(request));
                        break;
                    case "ActualizarRetencion":
                        out.println(ActualizarRetencion(request));
                        break;
                    case "FacturaOrdenes":
                        out.println(FacturaOrdenes(request));
                        break;
                    case "OperacionImportarFactura":
                        out.println(OperacionImportarFactura(request));
                        break;
                    case "TablaFacturacion":
                        out.println(TablaFacturacion(request));
                        break;
                    case "GuardarFactura":
                        out.println(GuardarFactura(request));
                        break;
                    case "EnvioFactura":
                        out.println(EnvioFactura(request));
                        break;
                    case "ReporteGenFactura":
                        out.println(ReporteGenFactura(request)); 
                        break;
                    default:
                        throw new AssertionError();
                }
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Servidor(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Servidor(request, response);
    }

}
