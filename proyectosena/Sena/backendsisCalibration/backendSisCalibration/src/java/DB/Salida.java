package DB;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.text.DateFormat;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author leudi
 */
@WebServlet(urlPatterns = {"/Salida"})
public class Salida extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    String sql;
    String idusuario = "";
    String usuario = "";
    ResultSet datos = null;
    String iplocal = "";

    //WILMER
    public String TablaSalidaDetalle(HttpServletRequest request) {
        int Cliente = Globales.Validarintnonull(request.getParameter("Cliente"));
        int Proveedor = Globales.Validarintnonull(request.getParameter("proveedor"));
        int Orden = Globales.Validarintnonull(request.getParameter("Orden"));

        if (Orden == 0) {
            sql = "select r.ingreso,  to_char(fechaing, 'yyyy/MM/dd HH24:MI') as fechaing, u.nombrecompleto as usuarioing, r.accesorio,"
                    + "  '<b>' || m.descripcion || '</b><br>' || e.descripcion  as equipo,  ma.descripcion as marca, mo.descripcion as modelo,"
                    + "  case when r.idintervalo = 0 then 'VER ESPECIFICACIONES' ELSE '(' || desde || ' a ' || hasta || ') ' || medida end as intervalo, r.serie,"
                    + "      '<b>' || o.orden || '</b><br>' ||  to_char(o.fecha, 'yyyy/MM/dd HH24:MI') as orden"
                    + " from ordencompter_detalle od inner join remision_detalle r on r.ingreso = od.ingreso"
                    + "			    inner join ordencompra_tercero o on o.id = od.idorden"
                    + "			    inner join equipo e on r.idequipo = e.id"
                    + "			    inner join magnitudes m on m.id = e.idmagnitud"
                    + "			    inner join modelos mo on mo.id = r.idmodelo "
                    + "			    inner join marcas ma on ma.id = mo.idmarca"
                    + "    inner join seguridad.rbac_usuario u on r.idusuario = u.idusu "
                    + "			    inner join magnitud_intervalos i on i.id = r.idintervalo"
                    + "  where r.ingreso <> 0 and salidaterce = 0 and o.idcliente = " + Cliente + " and o.idproveedor = " + Proveedor + " "
                    + "  UNION"
                    + "  select r.ingreso,  to_char(fechaing, 'yyyy/MM/dd HH24:MI') as fechaing, u.nombrecompleto as usuarioing, r.accesorio,"
                    + "     '<b>' || m.descripcion || '</b><br>' || e.descripcion  as equipo,  ma.descripcion as marca, mo.descripcion as modelo,"
                    + "     case when r.idintervalo = 0 then 'VER ESPECIFICACIONES' ELSE '(' || desde || ' a ' || hasta || ') ' || medida end as intervalo, r.serie,"
                    + "         '<b>0</b><br>' ||  ' ' as orden"
                    + "   from  salida_detalle sd inner join remision_detalle r on r.ingreso = sd.ingreso "
                    + "			                    inner join equipo e on r.idequipo = e.id"
                    + "			                    inner join magnitudes m on m.id = e.idmagnitud"
                    + "			                    inner join modelos mo on mo.id = r.idmodelo "
                    + "			                    inner join marcas ma on ma.id = mo.idmarca"
                    + "			                    inner join seguridad.rbac_usuario u on r.idusuario = u.idusu "
                    + "			                    inner join magnitud_intervalos i on i.id = r.idintervalo"
                    + "where sd.idproveedor_det=" + Proveedor + " and sd.idcliente_det = " + Cliente + " and idsalida = 0"
                    + " ORDER BY 9, 1";
        } else {
            sql = "select r.ingreso, p.documento as proveedor, o.idcliente, to_char(fechaing, 'yyyy/MM/dd HH24:MI') as fechaing, u.nombrecompleto as usuarioing,"
                    + "  '<b>' || m.descripcion || '</b><br>' || e.descripcion  as equipo,  ma.descripcion as marca, mo.descripcion as modelo, r.accesorio,"
                    + "  case when r.idintervalo = 0 then 'VER ESPECIFICACIONES' ELSE '(' || desde || ' a ' || hasta || ') ' || medida end as intervalo, r.serie,"
                    + "      '<b>' || o.orden || '</b><br>' ||  to_char(o.fecha, 'yyyy/MM/dd HH24:MI') as orden"
                    + " from ordencompter_detalle od inner join remision_detalle r on r.ingreso = od.ingreso"
                    + "		    inner join ordencompra_tercero o on o.id = od.idorden"
                    + "		    inner join equipo e on r.idequipo = e.id"
                    + "		    inner join magnitudes m on m.id = e.idmagnitud"
                    + "		    inner join modelos mo on mo.id = r.idmodelo  "
                    + "		    inner join marcas ma on ma.id = mo.idmarca"
                    + "		    inner join magnitud_intervalos i on i.id = r.idintervalo"
                    + "   inner join proveedores p on p.id = o.idproveedor "
                    + "   inner join seguridad.rbac_usuario u on r.idusuario = u.idusu "
                    + " where r.ingreso <> 0 and salidaterce = 0 and o.orden=" + Orden + " ORDER BY o.orden, r.ingreso";
        }
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->TablaSalidaDetalle");

    }

    public String BuscarSalida(HttpServletRequest request) {
        int Salida = Globales.Validarintnonull(request.getParameter("Salida"));
        sql = "select r.ingreso, p.documento as proveedor, s.idcliente, to_char(fechaing, 'yyyy/MM/dd HH24:MI') as fechaing, u.nombrecompleto as usuarioing,"
                + "          '<b>'|| m.descripcion || '</b><br>'"
                + " || e.descripcion as equipo"
                + " ,  ma.descripcion as marca"
                + " , mo.descripcion as modelo,"
                + "   case when r.idintervalo = 0 then 'VER ESPECIFICACIONES' ELSE '(' || desde || ' a ' || hasta || ') ' || medida end as intervalo, r.serie,"
                + "       desorden as orden, sd.accesorio,s.nombrecer, s.direccioncer,"
                + "   us.nombrecompleto as usuariosal, ur.nombrecompleto as usuarioret, s.estado,"
                + "   to_char(fechasal, 'yyyy-MM-dd') as fechasal, to_char(fecharet, 'yyyy/MM/dd HH24:MI') as fecharet, equiporet, s.observacion, "
                + "   ua.nombrecompleto as usuarioanula, to_char(fechaanula, 'yyyy/MM/dd HH24:MI') as fechaanula, observacionanula,"
                + "   to_char(fecharetorno, 'yyyy/MM/dd HH24:MI') || '<br>' || ure.nombrecompleto as retorno, observacionretorno, idmotivo"
                + "  from salida s inner join salida_detalle sd on s.id = sd.idsalida"
                + "                inner join remision_detalle r on r.ingreso = sd.ingreso"
                + "		          inner join equipo e on r.idequipo = e.id"
                + "		          inner join magnitudes m on m.id = e.idmagnitud"
                + "		          inner join modelos mo on mo.id = r.idmodelo "
                + "		          inner join marcas ma on ma.id = mo.idmarca"
                + "		          inner join magnitud_intervalos i on i.id = r.idintervalo"
                + "          inner join proveedores p on p.id = s.idproveedor "
                + "          inner join seguridad.rbac_usuario u on r.idusuario = u.idusu "
                + "          inner join seguridad.rbac_usuario us on s.idusuariosal = us.idusu"
                + "          inner join seguridad.rbac_usuario ur on s.idusuarioret = ur.idusu "
                + "          inner join seguridad.rbac_usuario ua on s.idusuarioanula = ua.idusu"
                + "          inner join seguridad.rbac_usuario ure on sd.idusuarioretorno = ure.idusu "
                + " where s.salida=" + Salida + " ORDER BY r.ingreso";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "--> BuscarSalida");
    }

    public String BuscarOrdenSalida(HttpServletRequest request) {
        int Orden = Globales.Validarintnonull(request.getParameter("Orden"));

        sql = "select idcliente, p.documento "
                + "          FROM ordencompra_tercero o inner join proveedores p on p.id = o.idproveedor"
                + " WHERE orden = " + Orden;
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "--> BuscarOrdenSalida");
    }

    public String TablaRetornar(HttpServletRequest request) {
        int Salida = Globales.Validarintnonull(request.getParameter("Salida"));

        sql = "select r.ingreso, "
                + "         '<b>'|| m.descripcion || '</b><br>'"
                + " || e.descripcion as equipo,  ma.descripcion as marca, mo.descripcion as modelo,"
                + " case when r.idintervalo = 0 then 'VER ESPECIFICACIONES' ELSE '(' || desde || ' a ' || hasta || ') ' || medida end as intervalo, r.serie,"
                + "     sd.accesorio"
                + " from salida s inner join salida_detalle sd on s.id = sd.idsalida"
                + "             inner join remision_detalle r on r.ingreso = sd.ingreso"
                + "			          inner join equipo e on r.idequipo = e.id"
                + "			          inner join magnitudes m on m.id = e.idmagnitud"
                + "			          inner join modelos mo on mo.id = r.idmodelo "
                + "			          inner join marcas ma on ma.id = mo.idmarca"
                + "			          inner join magnitud_intervalos i on i.id = r.idintervalo"
                + " where s.salida=" + Salida + " and sd.idusuarioretorno = 0 ORDER BY r.ingreso";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "--> TablaRetornar");
    }

    public String GuardarSalida(HttpServletRequest request) {
        int proveedor = Globales.Validarintnonull(request.getParameter("proveedor"));
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        int motivo = Globales.Validarintnonull(request.getParameter("motivo"));
        String observacion = request.getParameter("observacion");
        String a_ingreso = request.getParameter("a_ingreso");
        String a_accesorio = request.getParameter("a_accesorio");
        String a_orden = request.getParameter("a_orden");
        String direccioncer = request.getParameter("direccioncer");
        String nombrecer = request.getParameter("nombrecer");
        String fechasal = request.getParameter("fechasal");
        try {
            sql = " SELECT * FROM guardar_salida(" + idusuario + "," + proveedor + "," + cliente + "," + motivo + ",'" + observacion + "','" + fechasal
                    + "','" + nombrecer + "','" + direccioncer + "'," + a_ingreso + "," + a_orden + "," + a_accesorio + ")";
            datos = Globales.Obtenerdatos(sql, this.getClass() + "-->GuardarSalida", 1);
            if (datos.getString("_salida").equals("0")) {
                return "1|" + datos.getString("_mensaje");
            } else {
                return "0|Salida de equipos guardada con el número|" + datos.getString("_salida") + "|" + datos.getString("_idsalida");
            }
        } catch (Exception ex) {
            return "9|" + ex.getMessage();
        }
    }

    public String GuardarOtroIngreso(HttpServletRequest request) {
        int ingreso = Globales.Validarintnonull(request.getParameter("ingreso"));
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        int proveedor = Globales.Validarintnonull(request.getParameter("proveedor"));
        try {

            sql = "SELECT count(id) "
                    + "FROM salida_detalle "
                    + "where idproveedor_det = " + proveedor + " and idcliente_det = " + cliente + " and  idsalida= 0";
            int cantidad = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
            if (cantidad > 0) {
                return "1|No se puede agregar este ingreso porque ya esta en esta salida";
            }
            sql
                    = "INSERT INTO salida_detalle(idsalida, ingreso, accesorio, desorden, idproveedor_det, idcliente_det)"
                    + "       VALUES(0, " + ingreso + ", ' ', '0', " + proveedor + ", " + cliente + ");";

            if (Globales.DatosAuditoria(sql, "SALIDA", "AGREGAR DETALLE", idusuario, iplocal, this.getClass() + "--> GuardarOtroIngreso")) {
                return "0|Detalle agregado con éxito";
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }

        } catch (Exception ex) {

            return "1|" + ex.getMessage();
        }

    }

    public String RetornarEquipo(HttpServletRequest request) {
        int salida = Globales.Validarintnonull(request.getParameter("salida"));
        String a_ingreso = request.getParameter("a_ingreso");
        String a_observacion = request.getParameter("a_observacion");
                
        try {
            sql = "SELECT estado, equipossal, equiporet, id, to_char(fechasal,'yyyy-MM-dd HH24:MI') as fechasal FROM salida where salida =" + salida;
            datos = Globales.Obtenerdatos(sql, this.getClass() + "-->RetornarEquipo", 1);
            String estado = datos.getString("estado");
            int equipos = Globales.Validarintnonull(datos.getString("equipossal"));
            int idsalida = Globales.Validarintnonull(datos.getString("id"));
            String fechasal = datos.getString("fechasal");
            int equiposret = Globales.Validarintnonull(datos.getString("equiporet"));

            if (!estado.equals("Salida")) {
                return "1|No se puede recibir una salida con estado " + estado;
            }
            String mensaje = "";
            int encontrado = 0;
            int recibidos = 0;

            String[] ingresos = a_ingreso.split("|");
            String[] observaciones = a_observacion.split("|");

            for (int x = 0; x < ingresos.length; x++) {

                encontrado = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT recibidoterc FROM remision_detalle where recibidoterc = 1 and ingreso = " + ingresos[x]));
                if (encontrado > 0) {
                    mensaje += "Ingreso número " + ingresos[x] + " ya fue recibido en el laboratorio <br>";
                }
            }

            if (!mensaje.equals("")) {
                return "1|" + mensaje;
            }
            String textosql = "";
            for (int x = 0; x < ingresos.length; x++) {

                textosql += "UPDATE salida_detalle SET fecharetorno=now(), idusuarioretorno=" + idusuario + ", observacionretorno=" + observaciones[x]
                        + " WHERE idsalida = " + idsalida + " and ingreso = " + ingresos[x] + ";";
                textosql += "UPDATE remision_detalle SET recibidoterc = 1 WHERE ingreso = " + ingresos[x] + ";";
                textosql += "INSERT INTO public.ingreso_estados(ingreso, idusuario, descripcion, tiempo)"
                        + " VALUES(" + ingresos[x] + "," + idusuario + ",'INGRESO RECIBIDO DE TERCERIZACION CON LA SALIDA NÚMERO " + salida + "',tiempo('" + fechasal + "',now(),1));";
                recibidos++;
            }

            if (equipos == (recibidos + equiposret)) {
                textosql += "UPDATE salida SET estado = 'Recibido' WHERE salida = " + salida + ";";
            }
            textosql += "UPDATE salida SET fecharet = now(), idusuarioret = " + idusuario + ", equiporet = " + (recibidos + equiposret) + " WHERE salida = " + salida + ";";

            if (Globales.DatosAuditoria(sql, "SALIDA", "RECIBIR EQUIPOS", idusuario, iplocal, this.getClass() + "-->RetornarEquipo")) {
                return "0|Ingreso(s) recibido(s) con éxito";
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }
        } catch (Exception ex) {
            return "9|" + ex.getMessage();
        }
    }

    public String AnularSalida(HttpServletRequest request) {
        int salida = Globales.Validarintnonull(request.getParameter("salida"));
        String observaciones = request.getParameter("observaciones");
        try {
            sql = "SELECT * FROM anular_salida(" + idusuario + "," + salida + ",'" + observaciones + "')";
            datos = Globales.Obtenerdatos(sql, this.getClass() + "-->AnularSalida", 1);
            if (!datos.getString("_numerror").equals("0")) {
                return "1|" + datos.getString("_mensaje");
            } else {
                return "0|Salida anulada con éxito";
            }
        } catch (Exception ex) {
            return "9|" + ex.getMessage();
        }
    }

    public String ConsultarSalida(HttpServletRequest request) {
        int salida = Globales.Validarintnonull(request.getParameter("salida"));
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        int proveedor = Globales.Validarintnonull(request.getParameter("proveedor"));
        int ingreso = Globales.Validarintnonull(request.getParameter("ingreso"));
        String estado = request.getParameter("estado");
        String fechad = request.getParameter("fechad");
        String fechah = request.getParameter("fechah");
        int magnitud = Globales.Validarintnonull(request.getParameter("magnitud"));
        String equipo = request.getParameter("equipo");
        int marca = Globales.Validarintnonull(request.getParameter("marca"));
        String modelo = request.getParameter("modelo");
        String intervalo = request.getParameter("intervalo");
        String serie = request.getParameter("serie");
        int usuarios = Globales.Validarintnonull(request.getParameter("usuario"));
        String retornado = request.getParameter("retornado");

        String busqueda = "WHERE 1=1 ";
        if (salida > 0) {
            busqueda += " AND s.salida = " + salida;
        } else {
            if (ingreso > 0) {
                busqueda += " AND rd.ingreso = " + ingreso;
            } else {
                busqueda += " AND to_char(s.fechasal,'yyyy-MM-dd') >= '" + fechad + "' and to_char(s.fechasal,'yyyy-MM-dd') <= '" + fechah + "'";
                if (cliente > 0) {
                    busqueda += " AND s.idcliente = " + cliente;
                }
                if (proveedor > 0) {
                    busqueda += " AND s.idproveedor = " + proveedor;
                }
                if (ingreso > 0) {
                    busqueda += " AND rd.ingreso = " + ingreso;
                }
                if (!estado.equals("T")) {
                    busqueda += " AND s.estado = '" + estado + "'";
                }
                if (magnitud > 0) {
                    busqueda += " AND e.idmagnitud = " + magnitud;
                }
                if (!equipo.equals("")) {
                    busqueda += " AND e.descripcion = '" + equipo + "'";
                }
                if (marca > 0) {
                    busqueda += " AND mo.idmarca = " + marca;
                }
                if (!modelo.equals("")) {
                    busqueda += " AND mo.descripcion = '" + modelo + "'";
                }
                if (!intervalo.equals("")) {
                    busqueda += " AND '(' || desde || ' a ' || hasta || ') ' || medida = '" + intervalo + "'";
                }
                if (!serie.trim().equals("")) {
                    busqueda += " AND rd.serie ilike '%" + serie.trim() + "%'";
                }
                if (!retornado.equals("T")) {
                    if (retornado.equals("SI")) {
                        busqueda += " AND equiporet > 0 ";
                    } else {
                        busqueda += " AND equiporet = 0 ";
                    }
                }

            }
        }
        sql = "select s.salida, s.estado, s.observacion, to_char(fechasal, 'yyyy/MM/dd') as fecha, u.nombrecompleto as usuario, "
                + "          c.nombrecompleto || ' (' || c.documento || ')' as cliente, nombrecer, direccioncer,"
                + "         p.nombrecompleto || ' (' || p.documento || ')' as proveedor, ms.descripcion as motivo,"
                + "         c.telefono || ' / ' || coalesce(c.celular,'') || '<br>' || c.email as telefono, equipossal, equiporet, to_char(fecharet, 'yyyy/MM/dd HH24:MI') as fecharet,"
                + "         to_char(fechaanula, 'yyyy/MM/dd HH24:MI') as anulado,"
                + "         '<button class=''btn btn-glow btn-success imprimir'' title=''Imprimir Salida'' type=''button'' onclick=' || chr(34) || 'ImprimirSalida(' || s.salida || ')' ||chr(34) || '><span data-icon=''&#xe0f7;''></span></button>' as imprimir"
                + "     from salida s inner join salida_detalle sd on s.id = sd.idsalida"
                + "                  inner join seguridad.rbac_usuario u on s.idusuariosal = u.idusu"
                + "                  inner join clientes c on c.id = s.idcliente"
                + "                  inner join proveedores p on p.id = s.idproveedor"
                + "                    inner join remision_detalle rd on rd.ingreso = sd.ingreso "
                + "                    inner join equipo e on e.id = rd.idequipo"
                + "                    inner join modelos mo on mo.id = rd.idmodelo"
                + "            inner join magnitud_intervalos i on i.id = rd.idintervalo  "
                + "                    inner join seguridad.rbac_usuario ua on s.idusuarioanula = ua.idusu"
                + "                    inner join motivo_salida ms on ms.id = idmotivo " + busqueda + " "
                + "    group by s.salida, s.estado, s.observacion, s.fechasal, u.nombrecompleto,"
                + "        c.documento, c.nombrecompleto, nombrecer, direccioncer, c.telefono, c.celular, c.email,"
                + "        equipossal, equiporet, fecharet, fechaanula, p.nombrecompleto, p.documento, ms.descripcion"
                + "    ORDER BY s.salida";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "--> ConsultarSalida");
    }

public String ConsultarIngresosSalida(HttpServletRequest request) {
        int remision = Globales.Validarintnonull(request.getParameter("remision"));
        int cotizacion = Globales.Validarintnonull(request.getParameter("cotizacion"));
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        int ingreso = Globales.Validarintnonull(request.getParameter("ingreso"));
        String estado = request.getParameter("estado");
        String fechad = request.getParameter("fechad");
        String fechah= request.getParameter("fechah");
        int magnitud = Globales.Validarintnonull(request.getParameter("magnitud"));
        int equipo = Globales.Validarintnonull(request.getParameter("equipo"));
        int marca  = Globales.Validarintnonull(request.getParameter("marca"));
        int modelo = Globales.Validarintnonull(request.getParameter("modelo"));
        int intervalo = Globales.Validarintnonull(request.getParameter("intervalo"));
        String serie = request.getParameter("serie");
        int usuarios = Globales.Validarintnonull(request.getParameter("usuario"));
        String recepcion= request.getParameter("recepcion");
        
                
        String busqueda = "WHERE 1=1 ";
        if (remision > 0) {
            busqueda += " AND r.remision = " + remision;
        } else {
            if (cotizacion > 0) {
                busqueda += " AND r.cotizacion = " + cotizacion;
            } else {
                if (ingreso > 0) {
                    busqueda += " AND rd.ingreso = " + ingreso;
                } else {
                    busqueda += " AND to_char(r.fecha,'yyyy-MM-dd') >= '" + fechad + "' and to_char(r.fecha,'yyyy-MM-dd') <= '" + fechah + "'";
                    if (cliente > 0) {
                        busqueda += " AND r.idcliente = " + cliente;
                    }
                    if (!estado.equals("0")) {
                        busqueda += " AND r.estado = '" + estado + "'";
                    }
                    if (magnitud > 0) {
                        busqueda += " AND e.idmagnitud = " + magnitud;
                    }
                    if (equipo > 0) {
                        busqueda += " AND rd.idequipo = " + equipo;
                    }
                    if (marca > 0) {
                        busqueda += " AND mo.idmarca = " + marca;
                    }
                    if (modelo > 0) {
                        busqueda += " AND rd.idmodelo = " + modelo;
                    }
                    if (intervalo > 0) {
                        busqueda += " AND rd.idintervalo = " + intervalo;
                    }
                    if (!serie.trim().equals("")) {
                        busqueda += " AND rd.serie ilike '%" + serie.trim() + "%'";
                    }
                    if (usuarios > 0) {
                        busqueda += " AND r.idusuario = " + usuarios;
                    }
                    if (!recepcion.equals("")) {
                        busqueda += " AND r.recepcion = '" + recepcion + "'";
                    }
                }
            }
        }
       sql = "select r.id, r.remision, r.cotizacion, r.estado, r.observacion, to_char(fecha, 'yyyy/MM/dd HH24:MI') as fecha, u.nombrecompleto as usuario, r.recepcion,"+
              "      c.documento, c.nombrecompleto as cliente, cs.nombre as sede, cc.nombres as contacto, ci.descripcion as ciudad, d.descripcion as departamento,"+
              "      cc.telefonos || ' / ' || coalesce(cc.fax,'') as telefonos, cc.email, r.totales,"+
              "      from remision r inner join seguridad.rbac_usuario u on r.idusuario = u.idusu"+
		"		                    inner join clientes c on c.id = r.idcliente"+
		"		                    inner join clientes_sede cs on cs.id = r.idsede"+
		"		                    inner join clientes_contac cc on cc.id = r.idcontacto"+
		"		                    inner join ciudad ci on ci.id = cs.idciudad"+
		"		                    inner join departamento d on d.id = ci.iddepartamento"+
		"		                    inner join remision_detalle rd on rd.idremision = r.id"+ 
                 "                   inner join equipo e on e.id = rd.idequipo"+
		"		                    inner join modelos mo on mo.id = rd.idmodelo " + busqueda + " "+
                "    group by r.id, r.remision, r.cotizacion, r.estado, r.observacion, fecha, u.nombrecompleto,r.recepcion, "+
                "    c.documento, c.nombrecompleto, cs.nombre, cc.nombres, ci.descripcion, d.descripcion, cc.telefonos, cc.fax, cc.email, r.totales"+
                "    ORDER BY r.id desc";
            return Globales.ObtenerDatosJSon(sql, this.getClass() + "--> ConsultarIngresosSalida");
}

    /*public string RpSalida(int salida)
        {
            if (Inicializar() != "0") return validarinicio;
            string reporte = "ImpSalida";


            string sql = @"select s.salida, r.ingreso, p.documento, p.nombrecompleto as proveedor, p.direccion, p.email, coalesce(p.telefono) || ' / ' || coalesce(p.celular) as telefonos, 
                            e.descripcion  as equipo,  ma.descripcion as marca, mo.descripcion as modelo, p.contacto,
                            case when r.idintervalo = 0 then 'VER ESPECIFICACIONES' ELSE '(' || desde || ' a ' || hasta || ') ' || medida end as intervalo, r.serie,
	                       sd.accesorio, ms.descripcion as motivo, s.nombrecer, s.direccioncer,
                            us.nombrecompleto as usuariosal, ur.nombrecompleto as usuarioret, us.documento as documentosal, ur.documento as documentoret,
                            to_char(s.fecha, 'yyyy-MM-dd'::text) AS fecha,
                            to_char(s.fechasal, 'yyyy/MM/dd'::text) AS fechasal,
                            to_char(fecharet, 'yyyy-MM-dd HH24:MI') as fecharet, equiporet, s.observacion
			            from salida s inner join salida_detalle sd on s.id = sd.idsalida
                                              inner join remision_detalle r on r.ingreso = sd.ingreso
						                      inner join equipo e on r.idequipo = e.id
						                      inner join magnitudes m on m.id = e.idmagnitud
						                      inner join modelos mo on mo.id = r.idmodelo  
						                      inner join marcas ma on ma.id = mo.idmarca
						                      inner join magnitud_intervalos i on i.id = r.idintervalo
						                      inner join motivo_salida ms on ms.id=idmotivo
                                              inner join proveedores p on p.id = s.idproveedor 
                                              inner join seguridad.rbac_usuario u on r.idusuario = u.idusu 
                                              inner join seguridad.rbac_usuario us on s.idusuariosal = us.idusu 
                                              inner join seguridad.rbac_usuario ur on s.idusuarioret = ur.idusu 
                                              inner join seguridad.rbac_usuario ua on s.idusuarioanula = ua.idusu 
                            where s.salida = " + salida + @" 
                            ORDER BY sd.ingreso";
            datos = Globales.Obtenerdatos(sql, conexion);

            if (datos.Rows.Count == 0)
                return "1|No hay nada que reportar";

            DateTime fechasal = DateTime.Parse(datos.Rows[0]["fechasal"].ToString());
            string proveedor = datos.Rows[0]["proveedor"].ToString();

            string DirectorioReportesRelativo = "~/Reportes/";
            string directorio = "~/DocumPDF/";
            string unico = "Salida-" + salida + "_" + DateTime.Now.ToString("yyyyMMddHHmmss");
            string urlArchivo = string.Format("{0}.{1}", reporte, "rdlc");

            string urlArchivoGuardar;
            urlArchivoGuardar = string.Format("{0}.{1}", unico, "pdf");

            string FullPathReport = string.Format("{0}{1}",
                                    this.HttpContext.Server.MapPath(DirectorioReportesRelativo),
                                        urlArchivo);
            string FullGuardar = string.Format("{0}{1}",
                                    this.HttpContext.Server.MapPath(directorio),
                                        urlArchivoGuardar);
            Microsoft.Reporting.WebForms.ReportViewer Reporte = new ReportViewer();
            Reporte.Reset();
            Reporte.LocalReport.ReportPath = FullPathReport;
            Reporte.LocalReport.EnableExternalImages = true;

            ReportDataSource DataSource = new ReportDataSource("DataSet1", datos);
            Reporte.LocalReport.DataSources.Add(DataSource);
            Reporte.LocalReport.Refresh();
            byte[] ArchivoPDF = Reporte.LocalReport.Render("PDF");
            FileStream fs = new FileStream(FullGuardar, FileMode.Create);
            fs.Write(ArchivoPDF, 0, ArchivoPDF.Length);
            fs.Close();
            string rutasalida = HostingEnvironment.MapPath("~/DocumPDF/" + urlArchivoGuardar);
            string puerto = Request.Url.Port.ToString();

            if ((puerto == "80" || puerto == "90") && rutasalida != "")
            {
                string rutagestion = System.Configuration.ConfigurationManager.AppSettings["sistemagestion"];
                rutagestion += fechasal.Year + "\\7. ÁREA DE LOGÍSTICA\\1. INTERNA\\DID-107\\DID-107-" + salida + " " + proveedor + ".pdf";
                
                System.IO.File.Copy(rutasalida, rutagestion, true);
            }

            return "0|" + urlArchivoGuardar;

        }*/
    //FIN DEL CODIGO
    protected void Servidor(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        String ruta_origen = request.getHeader("Origin");

        HttpSession misession = request.getSession();
        idusuario = misession.getAttribute("idusuario").toString();
        iplocal = misession.getAttribute("IPLocal").toString();
        usuario = misession.getAttribute("codusu").toString();
        response.setHeader("Access-Control-Allow-Origin", request.getHeader("Origin"));
        response.setHeader("Access-Control-Allow-Credentials", "true");

        try (PrintWriter out = response.getWriter()) {
            if (!ruta_origen.equals("http://localhost:8090") && !ruta_origen.equals("http://192.168.0.55:82") && !ruta_origen.equals("https://192.168.0.55:90")) {
                out.print("[{\"error\":\"505\",\"mensaje\":\"Usted no tiene permiso para acceder a esta ruta\"}]");
            } else {
                if (misession.getAttribute("codusu") == null) {
                    out.println("cerrada");
                } else {
                    if (!Globales.IpUsuario(misession.getAttribute("idusuario").toString()).equals(misession.getAttribute("IPLocal").toString())) {
                        out.println("cambio-ip");
                    } else {
                        String opcion = request.getParameter("opcion");
                        switch (opcion) {

                            //WILMER
                            case "TablaSalidaDetalle":
                                out.println(TablaSalidaDetalle(request));
                                break;
                            case "BuscarSalida":
                                out.println(BuscarSalida(request));
                                break;
                            case "BuscarOrdenSalida":
                                out.println(BuscarOrdenSalida(request));
                                break;
                            case "TablaRetornar":
                                out.println(TablaRetornar(request));
                                break;
                            case "GuardarSalida":
                                out.println(GuardarSalida(request));
                                break;
                            case "GuardarOtroIngreso":
                                out.println(GuardarOtroIngreso(request));
                                break;
                            case "RetornarEquipo":
                                out.println(RetornarEquipo(request));
                                break;
                            case "AnularSalida":
                                out.println(AnularSalida(request));
                                break;
                            case "ConsultarSalida":
                                out.println(ConsultarSalida(request));
                                break;
                            case "ConsultarIngresosSalida":
                                out.println(ConsultarIngresosSalida(request));
                                break;  
                            //FIN
                            default:
                                throw new AssertionError();
                        }
                    }
                }
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Servidor(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Servidor(request, response);
    }

}
