package DB;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author leudi
 */
@WebServlet(urlPatterns = {"/image"})
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 10, // 10 MB 
        maxFileSize = 1024 * 1024 * 50, // 50 MB
        maxRequestSize = 1024 * 1024 * 100)      // 100 MB
public class image extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /*Datos recibidos de formulario con los codigo en base64*/
            System.out.println("-Guardando archivo-");
            String[] datosB64 = request.getParameterValues("datosB64");
            String datosTipo = request.getParameter("datosTipo");
            String inconformidades = "";
            for (int i = 0; i < datosB64.length; i++) {
                String tipo = datosB64[i].split(",")[0].trim();
                String valorB64 = datosB64[i].split(",")[1].trim().replaceAll(" ", "+");
                String extencion = extencionesArc(tipo);
                if (extencion.equals("-")) {
                    inconformidades += "|El archivo " + "NOMBRE DEL ARCHIVO" + " no esta en alguno de los formatos admitidos.";
                } else if(rutasArc(datosTipo).equals("-")) {
                    inconformidades += "|El archivo " + "NOMBRE DEL ARCHIVO" + ", no es de algun tipo de archivos admido.";
                }else{
                    Path destinationFile = Paths.get(rutasArc(datosTipo), "Nombre" + i + "." + extencion);
                    byte[] decodedImg = Base64.getMimeDecoder().decode(valorB64.trim().getBytes(StandardCharsets.UTF_8));
                    
                    Files.write(destinationFile, decodedImg);
                }
            }
            if (inconformidades.equals("")) {
                out.print("Los archivos se han subido correctamente");
            } else {
                out.print(inconformidades);
            }
        }
    }

    private String extencionesArc(String tipo) {
        switch (tipo) {
            case "data:image/jpeg;base64":
                return "jpg";
            case "data:image/png;base64":
                return "png";
            case "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64":
                return "xlsx";
            case "data:application/msword;base64":
                return "doc";
            case "data:application/pdf;base64":
                return "pdf";
            default:
                return "-";
        }
    }

    private String rutasArc(String tipo) {
        switch (tipo) {
            case "1":
                return "C:\\Users\\leudi\\OneDrive\\Documents\\EjecReportes";
            case "2":
                return "png";
            case "3":
                return "xlsx";
            case "4":
                return "doc";
            case "5":
                return "pdf";
            default:
                return "-";
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

/*
if (request.getParameter("base") != null) { // Verifico que el parametro [base] que es el que me trae los archivos no esten null
                System.out.println("-Entrado a crear imagen-");
                System.out.println("-URL de imagen: " + request.getRealPath("/") + "myImage.jpg");

                String encodedImg = request.getParameter("base").split(",")[1].trim().replaceAll(" ", "+");
                System.out.println("IMAGEN: " + encodedImg.trim());
                byte[] decodedImg = Base64.getMimeDecoder().decode(encodedImg.trim().getBytes(StandardCharsets.UTF_8));
                Path destinationFile = Paths.get(request.getRealPath("/"), "myImage.jpg");
                Files.write(destinationFile, decodedImg);

            }
 */
