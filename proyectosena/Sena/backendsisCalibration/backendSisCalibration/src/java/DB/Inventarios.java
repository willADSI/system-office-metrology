package DB;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author leudi
 */
@WebServlet(urlPatterns = {"/Inventarios"})
public class Inventarios extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    String sql;
    String idusuario = "";
    String usuario = "";
    ResultSet datos = null;
    String iplocal = "";

    //WILMER
    public String GuardarOpcion(HttpServletRequest request) {
        String descripcion = request.getParameter("descripcion");
        int tipo = Globales.Validarintnonull(request.getParameter("tipo"));
        int grupo = Globales.Validarintnonull(request.getParameter("grupo"));
        int marca = Globales.Validarintnonull(request.getParameter("marca"));
        int id = 0;

        Configuracion config = new Configuracion();

        try {
            String resultado = "";
            switch (tipo) {
                case 1:
                    if (Globales.PermisosSistemas("GRUPO-INVENTARIO GUARDAR", idusuario) == 0) {
                        return "XX|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                    }
                    descripcion = descripcion.trim().toUpperCase();
                    id = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT id FROM inventarios.grupos WHERE descripcion = '" + descripcion + "'"));
                    if (id > 0) {
                        return "XX|Descripcíon ya existe como grupo";
                    }
                    sql = "INSERT INTO inventarios.grupos (descripcion) VALUES ('" + descripcion + "')";
                    if (Globales.DatosAuditoria(sql, "GRUPO-INVENTARIO", "GUARDAR", idusuario, iplocal, this.getClass() + "->GuardarOpcion")) {
                        id = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT id FROM inventarios.grupos WHERE descripcion = '" + descripcion + "'"));
                        resultado = id + "|" + config.CargarCombos(null, 47, 1, "", 0);
                    }
                    break;
                case 2:
                    if (Globales.PermisosSistemas("EQUIPOS-INVENTARIO GUARDAR", idusuario) == 0) {
                        return "XX|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                    }

                    descripcion = descripcion.trim().toUpperCase();
                    id = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT id FROM inventarios.equipos WHERE descripcion = '" + descripcion + "' and idgrupo = " + grupo));
                    if (id > 0) {
                        return "XX|Descripcíon ya existe como equipo";
                    }
                    sql = "INSERT INTO inventarios.equipos (descripcion, idgrupo) VALUES ('" + descripcion + "'," + grupo + ")";
                    if (Globales.DatosAuditoria(sql, "EQUIPOS-INVENTARIO", "GUARDAR", idusuario, iplocal, this.getClass() + "->GuardarOpcion")) {
                        id = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT id FROM inventarios.equipos WHERE descripcion = '" + descripcion + "' and idgrupo = " + grupo));
                        resultado = id + "|" + config.CargarCombos(null, 48, 1, String.valueOf(grupo), 0);
                    }
                    break;
                case 3:
                    if (Globales.PermisosSistemas("MARCA-INVENTARIO GUARDAR", idusuario) == 0) {
                        return "XX|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                    }
                    descripcion = descripcion.trim().toUpperCase();
                    id = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT id FROM inventarios.marcas WHERE descripcion = '" + descripcion + "'"));
                    if (id > 0) {
                        return "XX|Descripcíon ya existe como marca";
                    }
                    sql = "INSERT INTO inventarios.marcas (descripcion) VALUES ('" + descripcion + "')";
                    if (Globales.DatosAuditoria(sql, "MARCA", "GUARDAR", idusuario, iplocal, this.getClass() + "->GuardarOpcion")) {
                        id = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT id FROM inventarios.marcas WHERE descripcion = '" + descripcion + "'"));
                        resultado = id + "|" + config.CargarCombos(null, 49, 1, "", 0);
                    }
                    break;
                case 4:
                    if (Globales.PermisosSistemas("MODELO-INVENTARIO GUARDAR", idusuario) == 0) {
                        return "XX|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                    }
                    descripcion = descripcion.trim().toUpperCase();
                    id = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT id FROM inventarios.modelos WHERE descripcion = '" + descripcion + "' and idmarca = " + marca));
                    if (id > 0) {
                        return "XX|Descripcíon ya existe modelo";
                    }
                    sql = "INSERT INTO inventarios.modelos (descripcion, idmarca) VALUES ('" + descripcion + "'," + marca + ")";
                    if (Globales.DatosAuditoria(sql, "MODELO", "GUARDAR", idusuario, iplocal, this.getClass() + "->GuardarOpcion")) {
                        id = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT id FROM inventarios.modelos WHERE descripcion = '" + descripcion + "' and idmarca = " + marca));
                        resultado = id + "|" + config.CargarCombos(null, 50, 1, String.valueOf(marca), 0);
                    }
                    break;
            }
            return resultado;
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }

    }

    public String BuscarArticulos(HttpServletRequest request) {
        int id = Globales.Validarintnonull(request.getParameter("id"));
        sql = "select a.*, idmarca, abreviatura || '-' || trim(to_char(codigointerno,'0000000')) as codigo, to_char(fechafactura,'yyyy-MM-dd') as fechafac,"
                + "      P.id AS idpprecio,  precio1, precio2, precio3, precio4, precio5, idgrupo,"
                + "      precio6, precio7, precio8, precio9, precio10"
                + "  from inventarios.articulos a INNER JOIN inventarios.modelos mo on mo.id = a.idmodelo"
                + "                          inner join inventarios.equipos e on a.idequipo = e.id "
                + "                          inner join inventarios.grupos g on g.id = e.idgrupo"
                + "                          left JOIN  inventarios.articulos_precios p on p.idarticulo = a.id "
                + " where a.id = " + id;
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->BuscarArticulos");
    }

    public String CuentaContable_Grupo(HttpServletRequest request) {
        int grupo = Globales.Validarintnonull(request.getParameter("grupo"));

        sql = "SELECT idcuecontable"
                + "  FROM inventarios.grupos"
                + "WHERE id = " + grupo;
        return Globales.ObtenerUnValor(sql);

    }

    public String ConsultarArticulos(HttpServletRequest request) {
        String presentacion = request.getParameter("presentacion");
        String tipo = request.getParameter("tipo");
        int grupo = Globales.Validarintnonull(request.getParameter("grupo"));
        String estado = request.getParameter("estado");
        int marca = Globales.Validarintnonull(request.getParameter("marca"));
        int modelo = Globales.Validarintnonull(request.getParameter("modelo"));
        int proveedor = Globales.Validarintnonull(request.getParameter("proveedor"));
        int equipo = Globales.Validarintnonull(request.getParameter("equipo"));

        String busqueda = "WHERE presentacion ilike '%" + presentacion + "%'";
        if (!estado.equals("Todos")) {
            busqueda += " AND a.tipo  = '" + tipo + "'";
        }
        if (grupo > 0) {
            busqueda += " AND e.idgrupo = " + grupo;
        }
        if (equipo > 0) {
            busqueda += " AND a.idequipo = " + equipo;
        }
        if (marca > 0) {
            busqueda += " AND mo.idmarca = " + marca;
        }
        if (modelo > 0) {
            busqueda += " AND a.idmodelo = " + modelo;
        }
        if (proveedor > 0) {
            busqueda += " AND a.idproveedor = " + proveedor;
        }

        sql = "SELECT a.id, abreviatura || '-' || trim(to_char(codigointerno,'0000000')) as codigointerno, codigobarras, a.tipo, g.descripcion as grupo, e.descripcion as equipo, te.descripcion as estado, ma.descripcion as marca, mo.descripcion as modelo,presentacion,existencia,p.nombrecompleto as proveedor,ultimocosto,"
                + "      ultimafactura,i.descripcion as iva, a.estado, catalogo,referencia "
                + "  FROM inventarios.articulos a inner join inventarios.equipos e on a.idequipo = e.id  "
                + "                        inner join inventarios.grupos g on g.id = e.idgrupo"
                + "                        inner join inventarios.modelos mo on mo.id = a.idmodelo "
                + "                        inner join inventarios.marcas ma on mo.idmarca = ma.id"
                + "                        inner join iva i on a.idiva = i.id"
                + "                        inner join proveedores p on a.idproveedor = p.id "
                + "                        inner join tipo_estado te on a.estado = te.id " + busqueda + " ORDER BY a.id";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ConsultarArticulos");
    }

    public int ValorIva(HttpServletRequest request) {
        int Iva = Globales.Validarintnonull(request.getParameter("Iva"));

        sql = "select valor from iva where id = " + Iva;
        return Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
    }

    public String CodigoInterno(HttpServletRequest request) {
        int Grupo = Globales.Validarintnonull(request.getParameter("Grupo"));
        try {
            sql = "SELECT to_char(coalesce(max(a.codigointerno),0)+1,'0000000') as codigo, abreviatura"
                    + "  from inventarios.grupos g left join inventarios.equipos e on e.idgrupo = g.id "
                    + "	                                  left join inventarios.articulos a on e.id = a.idequipo"
                    + "   WHERE g.id=" + Grupo + "group by abreviatura";
            datos = Globales.Obtenerdatos(sql, this.getClass() + "-->CodigoInterno", 1);
            datos.next();

            return datos.getString("abreviatura") + "-" + datos.getString("codigo").trim();
        } catch (Exception e) {
            Globales.GuardarLogger(e.getMessage(), this.getClass() + "-->CodigoInterno");
            return "";
        }
    }

    public String GuardarArticulos(HttpServletRequest request) {

        int IdArticulo = Globales.Validarintnonull(request.getParameter("IdArticulo"));
        String Referencia = request.getParameter("Referencia");
        String TipoInventario = request.getParameter("TipoInventario");
        int Grupo = Globales.Validarintnonull(request.getParameter("Grupo"));
        int Equipo = Globales.Validarintnonull(request.getParameter("Equipo"));
        String Descripcion = request.getParameter("Descripcion");
        int Modelo = Globales.Validarintnonull(request.getParameter("Modelo"));
        int Iva = Globales.Validarintnonull(request.getParameter("Iva"));
        int Estado = Globales.Validarintnonull(request.getParameter("Estado"));
        String Catalogo = request.getParameter("Catalogo");
        String Presentacion = request.getParameter("Presentacion");
        String CodigoBarras = request.getParameter("CodigoBarras");
        double Precio1 = Globales.ValidardoublenoNull(request.getParameter("Precio1"));
        double Precio2 = Globales.ValidardoublenoNull(request.getParameter("Precio2"));
        double Precio3 = Globales.ValidardoublenoNull(request.getParameter("Precio3"));
        double Precio4 = Globales.ValidardoublenoNull(request.getParameter("Precio4"));
        double Precio5 = Globales.ValidardoublenoNull(request.getParameter("Precio5"));
        double Precio6 = Globales.ValidardoublenoNull(request.getParameter("Precio6"));
        double Precio7 = Globales.ValidardoublenoNull(request.getParameter("Precio7"));
        double Precio8 = Globales.ValidardoublenoNull(request.getParameter("Precio8"));
        double Precio9 = Globales.ValidardoublenoNull(request.getParameter("Precio9"));
        double Precio10 = Globales.ValidardoublenoNull(request.getParameter("Precio10"));
        double UltimoCosto = Globales.ValidardoublenoNull(request.getParameter("UltimoCosto"));
        int CuentaContable = Globales.Validarintnonull(request.getParameter("CuentaContable"));
        int Proveedor = Globales.Validarintnonull(request.getParameter("Proveedor"));

        String Mensaje = "";
        int IdPrecio = 0;
        int CodigoInterno = 0;
        String Codigo;
        int encontrado = 0;
        String Abreviatura = "";
        int ValorIva = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT valor from iva where id = " + Iva));
        CodigoBarras = CodigoBarras.trim();
        try {
            if (!CodigoBarras.equals("")) {
                sql = "SELECT id from inventarios.articulos WHERE codigobarras = '" + CodigoBarras + "' and id <> " + IdArticulo;
                encontrado = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
                if (encontrado > 0) {
                    return "1|El código de barras " + CodigoBarras + " ya se encuentra asignado a otro artículo en el inventario";
                }
            }

            sql = "SELECT id from inventarios.articulos WHERE referencia = '" + Referencia + "' and id <> " + IdArticulo + " and idmodelo=" + Modelo + " and idequipo=" + Equipo;
            encontrado = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
            if (encontrado > 0) {
                return "1|Este equipo con estas características ya se encuentra registro";
            }

            sql = "SELECT to_char(coalesce(max(a.codigointerno),0)+1,'0000000') as codigo, abreviatura"
                    + "  from inventarios.grupos g left join inventarios.equipos e on e.idgrupo = g.id "
                    + "	                                  left join inventarios.articulos a on e.id = a.idequipo"
                    + "   WHERE g.id=" + Grupo + "group by abreviatura";
            datos = Globales.Obtenerdatos(sql, this.getClass() + "-->GuardarArticulos", 1);
            datos.next();
            Abreviatura = datos.getString("abreviatura");
            CodigoInterno = Globales.Validarintnonull(datos.getString("codigo"));
            Codigo = datos.getString("codigo").trim();

            if (IdArticulo == 0) {
                if (Globales.PermisosSistemas("INVENTARIO AGREGAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "INSERT INTO inventarios.articulos(referencia, tipo, idequipo, descripcion, idmodelo,idiva, idproveedor, estado, catalogo, ultimocosto, presentacion, codigointerno, codigobarras, idcuecontable)"
                        + "   VALUES('" + Referencia + "'  ,'" + TipoInventario + "'  ," + Equipo + "  ,'" + Descripcion.trim().toUpperCase() + "'," + Modelo + "," + Iva + "," + Proveedor + "," + Estado + ",'" + Catalogo + ",'" + UltimoCosto + ",'" + Presentacion.trim() + "'," + CodigoInterno + ",'" + CodigoBarras + "'," + CuentaContable + ");";
                if (Globales.DatosAuditoria(sql, "INVENTARIO", "AGREGAR", idusuario, iplocal, this.getClass() + "-->GuardarArticulos")) {
                    IdArticulo = Globales.Validarintnonull(Globales.ObtenerUnValor(" SELECT MAX(id) FROM inventarios.articulos;"));
                    Mensaje = "Artículo agregado con éxito";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            } else {

                if (Globales.PermisosSistemas("INVENTARIO EDITAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql
                        = "UPDATE inventarios.articulos"
                        + "SET tipo ='" + TipoInventario + "'  , idequipo=" + Equipo + "  ,descripcion='" + Descripcion.trim().toUpperCase() + "'  " + ",idmodelo=" + Modelo + ",idiva=" + Iva + ",idproveedor=" + Proveedor + ",estado=" + Estado
                        + ",catalogo='" + Catalogo + "',ultimocosto=" + UltimoCosto + ", referencia='" + Referencia + "' "
                        + ", presentacion='" + Presentacion.trim() + "', codigoBarras='" + CodigoBarras + "', idcuecontable = " + CuentaContable + " WHERE id= " + IdArticulo;

                if (Globales.DatosAuditoria(sql, "INVENTARIO", "ACTUALIZAR", idusuario, iplocal, this.getClass() + "-->GuardarArticulos")) {
                    Mensaje = "Artículo actualizado con éxito";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            }

            IdPrecio = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT id FROM inventarios.articulos_precios WHERE idarticulo=" + IdArticulo));
            if (IdPrecio == 0) {
                sql = "INSERT INTO inventarios.articulos_precios(idarticulo, precio1, precio2, precio3, precio4, precio5, "
                        + "precio6, precio7, precio8, precio9, precio10)"
                        + "VALUES(" + IdArticulo + ", " + Precio1 + ", " + Precio2 + ", " + Precio3 + ", " + Precio4 + "," + Precio5 + "," + Precio6 + "," + Precio7 + "," + Precio8 + "," + Precio9 + "," + Precio10 + ")";
            } else {
                sql
                        = "UPDATE inventarios.articulos_precios"
                        + "          SET precio1 = " + Precio1 + ", precio2 = " + Precio2 + ", precio3 = " + Precio3 + ", precio4 =" + Precio4
                        + ",precio5=" + Precio5 + ", precio6=" + Precio6 + ", precio7=" + Precio7 + ", precio8=" + Precio8 + ", precio9=" + Precio9 + ", precio10=" + Precio1 + " WHERE id=" + IdPrecio;
            }
            if (Globales.DatosAuditoria(sql, "INVENTARIO", "ACTUALIZAR PRECIOS", idusuario, iplocal, this.getClass() + "-->GuardarArticulos")) {
                Mensaje = "precio actualizado con éxito";
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }

            return "0|" + Mensaje + "|" + IdArticulo + "|" + Abreviatura + "-" + Codigo + "|" + ValorIva;

        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String ModalArticulos(HttpServletRequest request) {
        String codigo = request.getParameter("codigo");
        String descripcion = request.getParameter("descripcion");
        int grupo = Globales.Validarintnonull(request.getParameter("grupo"));
        int ubicacion = Globales.Validarintnonull(request.getParameter("ubicacion"));

        String busqueda = " WHERE a.estado = 1 ";
        if (grupo > 0) {
            busqueda += " and g.id=" + grupo;
        }
        if (!codigo.equals("")) {
            busqueda += " and (abreviatura || '-' || trim(to_char(codigointerno,'0000000')) ilike '%" + codigo + "%' or codigobarras ilike '%" + codigo + "%')";
        }
        if (!descripcion.equals("")) {
            busqueda += " and (e.descripcion ilike '%" + descripcion + "%' or ma.descripcion ilike '%" + descripcion + "%' or mo.descripcion ilike '%" + descripcion + "%')";
        }
        if (ubicacion > 0) {
            busqueda += " and u.idbodega = " + ubicacion;
        }

        sql = "select a.id, '' as foto, abreviatura || '-' || trim(to_char(codigointerno,'0000000')) as codigointerno, a.codigobarras, a.ultimocosto, "
                + "  e.descripcion as equipo, mo.descripcion as modelo, ma.descripcion as marca, g.descripcion as grupo, presentacion, a.tipo,"
                + "  existencia, i.valor as iva, p.nombrecompleto as proveedor, c.cuenta"
                + " FROM inventarios.articulos a inner join inventarios.equipos e on a.idequipo = e.id  "
                + "              " + (ubicacion > 0 ? " inner join inventarios.articulo_ubicacion u on u.idarticulo = e.id " : "") + " "
                + "                        inner join inventarios.grupos g on g.id = e.idgrupo"
                + "                        inner join inventarios.modelos mo on mo.id = a.idmodelo  "
                + "                        inner join inventarios.marcas ma on mo.idmarca = ma.id"
                + "                        inner join iva i on a.idiva = i.id"
                + "            inner join contabilidad.cuenta c on c.id = a.idcuecontable"
                + "                        inner join proveedores p on a.idproveedor = p.id "
                + "                        inner join tipo_estado te on a.estado = te.id " + busqueda + " ORDER BY a.codigointerno";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ModalArticulos");
    }

    public String DescripcionPrecio() {

        sql = "SELECT id, descripcion ||  ' (' || porcentaje || '%)' as precio, porcentaje, descripcion "
                + "  from tablaprecios order by id";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->DescripcionPrecio");
    }

    public String CodigoTomaFisica(HttpServletRequest request) {
        int opcion = Globales.Validarintnonull(request.getParameter("opcion"));
        int grupo = Globales.Validarintnonull(request.getParameter("grupo"));
        int equipo = Globales.Validarintnonull(request.getParameter("equipo"));
        String presentacion = request.getParameter("presentacion");
        int marca = Globales.Validarintnonull(request.getParameter("marca"));
        int modelo = Globales.Validarintnonull(request.getParameter("modelo"));

        String combo = "";
        int idgrupo = 0;
        switch (opcion) {
            case 1:
                sql = "SELECT distinct e.id, e.descripcion "
                        + "  FROM inventarios.articulos a INNER JOIN inventarios.equipos e on a.idequipo = e.id "
                        + " WHERE e.idgrupo = " + grupo + " and a.estado = 1 ORDER BY 2";
                combo = Globales.ObtenerCombo(sql, 0, 0, 0);
                if (combo.equals("")) {
                    combo = "XX";
                }
                return combo;
            case 2:
                sql = "SELECT distinct presentacion, presentacion "
                        + "  FROM inventarios.articulos a INNER JOIN inventarios.equipos e on a.idequipo = e.id "
                        + " WHERE e.id = " + equipo + " and a.estado = 1 ORDER BY 2";
                combo = Globales.ObtenerCombo(sql, 0, 0, 0);
                combo = "<option value=''>NINGUNA</option>" + combo;
                sql = "SELECT idgrupo"
                        + "  FROM inventarios.equipos "
                        + " WHERE id = " + equipo;
                idgrupo = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
                return combo + "||" + idgrupo;
            case 3:
                sql = "SELECT distinct ma.id, ma.descripcion FROM inventarios"
                        + ".articulos a INNER JOIN inventarios.modelos mo on a.idmodelo = mo.id "
                        + "INNER JOIN inventarios.marcas ma on mo.idmarca = ma.id "
                        + "WHERE a.idequipo = " + equipo + " and presentacion = '" + presentacion + "'  and a.estado = 1 ORDER BY 2";

                combo = Globales.ObtenerCombo(sql, 0, 0, 0);
                if (combo.equals("")) {
                    combo = "XX";
                }
                return combo;
            case 4:
                sql
                        = "SELECT distinct mo.id, mo.descripcion  FROM inventarios"
                        + ".articulos a INNER JOIN inventarios.modelos mo on a"
                        + ".idmodelo = mo.id WHERE mo.idmarca = " + marca + " and a"
                        + ".estado = 1 and a.idequipo = " + equipo + " and a"
                        + ".presentacion = '" + presentacion + "'  ORDER BY 2";

                combo = Globales.ObtenerCombo(sql, 0, 0, 0);
                if (combo.equals("")) {
                    combo = "XX";
                }
                return combo;
            case 5:
                sql
                        = "select abreviatura || '-' || trim(to_char(codigointerno,'0000000')), abreviatura || '-' || trim(to_char(codigointerno,'0000000'))"
                        + "FROM inventarios.articulos a inner join inventarios.equipos e on a"
                        + ".idequipo = e.id  inner join inventarios.grupos g on g.id = e.idgrupo"
                        + "WHERE a.idmodelo = " + modelo + " and a"
                        + ".estado = 1 and a.idequipo = " + equipo + " and a"
                        + ".presentacion = '" + presentacion + "'  ORDER BY 2";

                combo = Globales.ObtenerCombo(sql, 0, 0, 0);
                if (combo.equals("")) {
                    combo = "XX";
                }
                return combo;
        }

        return "";

    }

    public String BuscarArticulosToma(HttpServletRequest request) {
        String codigo = request.getParameter("codigo");

        sql = "select a.id, abreviatura || '-' || trim(to_char(codigointerno,'0000000')) as codigointerno, codigobarras, presentacion, a.estado,  "
                + "      mo.descripcion as modelo, ma.descripcion as marca, g.descripcion as grupo, a.tipo, e.descripcion as equipo, a.existencia, a.ultimocosto,"
                + "     a.idcuecontable  "
                + "from inventarios.articulos a INNER JOIN inventarios.modelos mo on mo.id = a.idmodelo"
                + "                              inner join inventarios.marcas ma on ma.id = mo.idmarca"
                + "                             inner join inventarios.equipos e on a.idequipo = e.id  "
                + "                             inner join inventarios.grupos g on g.id = e.idgrupo"
                + "where abreviatura || '-' || trim(to_char(codigointerno,'0000000'))  = '" + codigo + "' or"
                + "    codigobarras = '" + codigo + "'";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->BuscarArticulosToma");
    }

    public String GuardarToma(HttpServletRequest request) {
        int articulo = Globales.Validarintnonull(request.getParameter("articulo"));
        int cantidad = Globales.Validarintnonull(request.getParameter("cantidad"));
        String descripcion = request.getParameter("descripcion");
        int toma = Globales.Validarintnonull(request.getParameter("toma"));
        int ubicacion = Globales.Validarintnonull(request.getParameter("ubicacion"));
        int responsable = Globales.Validarintnonull(request.getParameter("responsable"));

        try {
            if (Globales.PermisosSistemas("TOMA FISICA GUARDAR", idusuario) == 0) {
                return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
            }

            int idubicacion = Globales.Validarintnonull(Globales.ObtenerUnValor("select id from inventarios.articulo_ubicacion where idarticulo = " + articulo + " and idbodega=" + ubicacion + " and idresponsable=" + responsable));
            if (idubicacion == 0) {
                sql = "INSERT INTO inventarios.articulo_ubicacion(idarticulo, idbodega, cantidad, idresponsable, "
                        + " tomafisica" + toma + ", fechatoma" + toma + ", idusuariotoma" + toma + ")"
                        + " VALUES(" + articulo + ", " + ubicacion + ", 0, " + responsable + ", " + cantidad + ", now(), " + idubicacion + ")";
            } else {
                sql = "UPDATE inventarios.articulo_ubicacion"
                        + " SET tomafisica" + toma + "=coalesce(tomafisica" + toma + ",0) + " + cantidad + ",fechatoma" + toma + "=now(), idusuariotoma" + toma + "=" + idusuario + " "
                        + " WHERE id = " + idubicacion;
            }
            if (Globales.DatosAuditoria(sql, "TOMA FISICA", "GUARDAR", idusuario, iplocal, this.getClass() + "-->BuscarArticulosToma")) {
                return "0|Toma física agregada con éxito";
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }

        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }

    }

    public String TablaTomaFisica(HttpServletRequest request) {
        int ubicacion = Globales.Validarintnonull(request.getParameter("ubicacion"));
        int responsable = Globales.Validarintnonull(request.getParameter("responsable"));
        sql = "select row_number() OVER(order by a.id) as fila, abreviatura || '-' || trim(to_char(codigointerno,'0000000')) as codigointerno, codigobarras, "
                + "      '<b>Tipo:</b> ' || tipo || ', <b>Grupo:</b> ' || g.descripcion || ', <b>Presentación:</b> ' || presentacion || "
                + "      '<b>, Marca:</b> ' || ma.descripcion ||', <b>Modelo:</b> ' || mo.descripcion as descripcion,"
                + "      tomafisica1, tomafisica2,  to_char(fechatoma1, 'yyyy/MM/dd HH24:MI') || '<br><b>' || u1.nomusu as fecha1,"
                + "      to_char(fechatoma2, 'yyyy/MM/dd HH24:MI') || '<br><b>' || u2.nomusu as fecha2,"
                + "      '<button class=''btn btn-glow btn-danger'' title=''Eliminar Toma Física'' type=''button'' onclick=' || chr(34) || 'EliminarTomaFisica(' || a.id || ',''' ||  abreviatura || '-' || trim(to_char(codigointerno,'0000000')) || ''')' ||chr(34) || '><span data-icon=''&#xe0d8;''></span></button>' as eliminar"
                + " from inventarios.articulos a INNER JOIN inventarios.modelos mo on mo.id = a.idmodelo"
                + "                         inner join inventarios.marcas ma on ma.id = mo.idmarca"
                + "                         inner join inventarios.equipos e on a.idequipo = e.id  "
                + "                         inner join inventarios.grupos g on g.id = e.idgrupo"
                + "                         inner join inventarios.articulo_ubicacion au on au.idarticulo = a.id and idbodega = " + ubicacion + " and idresponsable = " + responsable + " "
                + "                         inner join seguridad.rbac_usuario u1 on u1.idusu = idusuariotoma1"
                + "                         inner join seguridad.rbac_usuario u2 on u2.idusu = idusuariotoma2"
                + " where tomafisica1 is not null or tomafisica2 is not null ORDER BY fechatoma1 desc, fechatoma2 desc";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->TablaTomaFisica");
    }

    public String InicializarToma(HttpServletRequest request) {
        int ubicacion = Globales.Validarintnonull(request.getParameter("ubicacion"));
        int responsable = Globales.Validarintnonull(request.getParameter("responsable"));

        if (Globales.PermisosSistemas("TOMA FISICA INICIALIZAR", idusuario) == 0) {
            return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
        }

        sql = "UPDATE inventarios.articulo_ubicacion"
                + "      SET tomafisica1 = null, fechatoma1 = null, idusuariotoma1=0, tomafisica2 = null, fechatoma2 = null, idusuariotoma2=0"
                + "  where tomafisica1 is not null or tomafisica2 is not null and idubicacion=" + ubicacion + " and idresponsable=" + responsable;
        if (Globales.DatosAuditoria(sql, "TOMA FISICA", "INICIALIZAR", idusuario, iplocal, this.getClass() + "-->BuscarArticulosToma")) {
            return "0|Toma física inicializada con éxito";
        } else {
            return "1|Error inesperado... Revise el log de errores";
        }

    }

    public String EliminarToma(HttpServletRequest request) {
        int articulo = Globales.Validarintnonull(request.getParameter("articulo"));
        int ubicacion = Globales.Validarintnonull(request.getParameter("ubicacion"));
        int responsable = Globales.Validarintnonull(request.getParameter(" responsable"));

        if (Globales.PermisosSistemas("TOMA FISICA ELIMINAR", idusuario) == 0) {
            return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
        }

        sql = "UPDATE inventarios.articulos"
                + "      SET tomafisica1 = null, fechatoma1 = null, idusuariotoma1=0, tomafisica2 = null, fechatoma2 = null, idusuariotoma2=0"
                + "  where id=" + articulo + "and idubicacion = " + ubicacion + " and idresponsable = " + responsable;

        if (Globales.DatosAuditoria(sql, "TOMA FISICA", "ELIMINAR", idusuario, iplocal, this.getClass() + "-->EliminarToma")) {
            return "0|Toma física eliminada con éxito";
        } else {
            return "1|Error inesperado... Revise el log de errores";
        }

    }

    public String ConsultaTomaExcel(HttpServletRequest request) {
        String tipo = request.getParameter("tipo");
        int grupo = Globales.Validarintnonull(request.getParameter("grupo"));
        int equipo = Globales.Validarintnonull(request.getParameter("equipo"));
        String presentacion = request.getParameter("presentacion");
        int marca = Globales.Validarintnonull(request.getParameter("marca"));
        int modelo = Globales.Validarintnonull(request.getParameter("modelo"));
        int nulo = Globales.Validarintnonull(request.getParameter("nulo"));
        int ubicacion = Globales.Validarintnonull(request.getParameter("ubicacion"));
        int responsable = Globales.Validarintnonull(request.getParameter("responsable"));

        String busqueda = "where a.estado = 1";

        if (nulo == 1) {
            busqueda += " and tomafisica1 is not null";
        } else {
            if (ubicacion != -1) {
                busqueda += " and coalesce(au.idbodega,0) = " + ubicacion;
            }
            if (responsable != -1) {
                busqueda += " and coalesce(au.idresponsable,0) = " + ubicacion;
            }
            if (nulo == 2) {
                busqueda += " and tomafisica1 is not null";
            }
            if (!tipo.equals("")) {
                busqueda += " and a.tipo = '" + tipo + "'";
            }
            if (!presentacion.equals("")) {
                busqueda += " and a.presentacion = '" + presentacion + "'";
            }
            if (grupo > 0) {
                busqueda += " and g.id = " + grupo;
            }
            if (equipo > 0) {
                busqueda += " and e.id = " + equipo;
            }
            if (marca > 0) {
                busqueda += " and ma.id = " + marca;
            }
            if (modelo > 0) {
                busqueda += " and mo.id = " + modelo;
            }
        }

        sql = "select b.descripcion as ubicacion, u.nombrecompleto as responsable,  row_number() OVER(order by a.id) as fila, a.id, au.id as idubicacion, abreviatura || '-' || trim(to_char(codigointerno,'0000000')) as codigointerno, presentacion, au.cantidad as existencia,"
                + "      mo.descripcion as modelo, ma.descripcion as marca, g.descripcion as grupo, a.tipo, e.descripcion as equipo, ultimocosto, tomafisica1, au.cantidad - tomafisica1 as diferencia,"
                + "     (au.cantidad - tomafisica1)*ultimocosto as valor"
                + " from inventarios.articulos a INNER JOIN inventarios.modelos mo on mo.id = a.idmodelo"
                + "                          inner join inventarios.marcas ma on ma.id = mo.idmarca"
                + "                          inner join inventarios.equipos e on a.idequipo = e.id "
                + "                          inner join inventarios.grupos g on g.id = e.idgrupo"
                + "                          left join inventarios.articulo_ubicacion au on au.idarticulo = a.id"
                + "			 left join inventarios.bodega b on b.id = au.idbodega"
                + "			 left join seguridad.rbac_usuario u on u.idusu = au.idresponsable " + busqueda + " order by 2";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ConsultaTomaExcel");
    }

    /*
      public String ImportarArchivo()
        {
           
            if (Globales.PermisosSistemas("TOMA FISICA IMPORTAR EXCEL",idusuario) == 0)
                return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
            
            String resultado = "";
            int id = 0;
            int toma = 0;

            
            String ruta = HostingEnvironment.MapPath("~/uploads/" + Session["ArchivoExcel"].ToString());
            
            ExcelWorksheet workSheet;

            try
            {
                sql = "";
                var package1 = new ExcelPackage(new FileInfo(ruta));
                workSheet = package1.Workbook.Worksheets[1];

                for (int i = 3; i <= workSheet.Dimension.End.Row; i++)
                {
                    if (workSheet.Cells[i, 2].Value != null)
                    {
                        id = Globales.Validarintnonull(workSheet.Cells[i, 2].Value.ToString());
                        toma = 0;
                        if (workSheet.Cells[i, 13].Value != null)
                            toma = Globales.Validarintnonull(workSheet.Cells[i, 13].Value.ToString());
                        if (id > 0)
                        {
                            sql += "UPDATE inventarios.articulos"+
                                   "     SET tomafisica1 = " + toma + ", fechatoma1 = now(), idusuariotoma1=" + idusuario + " "+
                                    "where id=" + id + ";";

                        }
                    }
                }

                if (!sql.equals(""))
                {
                    Globales.DatosAuditoria(sql, "TOMA FISICA","IMPORTAR EXCEL", idusuario, iplocal);
                    Session["ArchivoExcel"] = null;
                    resultado = "0|Toma física importada con éxito";
                }
                else
                {
                    resultado = "1|No se encontrarón movimientos en el archivo";
                }

            }
            catch (Exception ex)
            {
                return "1|" + ex.getMessage();
            }

            return resultado;
        }
     */
    public String CerrarTomaFisica(HttpServletRequest request) {
        String tipo = request.getParameter("tipo");
        int grupo = Globales.Validarintnonull(request.getParameter("grupo"));
        int equipo = Globales.Validarintnonull(request.getParameter("equipo"));
        String presentacion = request.getParameter(" presentacion");
        int marca = Globales.Validarintnonull(request.getParameter("marca"));
        int modelo = Globales.Validarintnonull(request.getParameter("modelo"));

        String busqueda = "where a.estado = 1 and a.tomafisica1 is not null";
        if (!tipo.equals("")) {
            busqueda += " and a.tipo = '" + tipo + "'";
        }
        if (!presentacion.equals("")) {
            busqueda += " and a.presentacion = '" + presentacion + "'";
        }
        if (grupo > 0) {
            busqueda += " and g.id = " + grupo;
        }
        if (equipo > 0) {
            busqueda += " and e.id = " + equipo;
        }
        if (marca > 0) {
            busqueda += " and ma.id = " + marca;
        }
        if (modelo > 0) {
            busqueda += " and mo.id = " + modelo;
        }

        if (Globales.PermisosSistemas("TOMA FISICA CERRAR", idusuario) == 0) {
            return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
        }

        sql = "SELECT count(a.id) "
                + " from inventarios.articulos a INNER JOIN inventarios.modelos mo on mo.id = a.idmodelo"
                + "                             inner join inventarios.marcas ma on ma.id = mo.idmarca"
                + "                             inner join inventarios.equipos e on a.idequipo = e.id"
                + "                             inner join inventarios.grupos g on g.id = e.idgrupo " + busqueda + ";";
        int registros = Globales.Validarintnonull(Globales.ObtenerUnValor(sql)) + 1;

        if (registros == 0) {
            return "0|No hay registros de toma física para realizar el cierre";
        }

        int contador = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT tomafisica from contadores")) + 1;

        sql = "INSERT INTO inventarios.kardex(idarticulo, documento, cantidad, costo, tipo, idusuario, es)"
                + "  SELECT a.id, " + contador + ",tomafisica1 - existencia, ultimocosto, 'Toma Física'," + idusuario + ",1"
                + "  from inventarios.articulos a INNER JOIN inventarios.modelos mo on mo.id = a.idmodelo"
                + "                          inner join inventarios.marcas ma on ma.id = mo.idmarca"
                + "                          inner join inventarios.equipos e on a.idequipo = e.id "
                + "                          inner join inventarios.grupos g on g.id = e.idgrupo " + busqueda + ";";

        sql += "INSERT INTO inventarios.toma_fisica(idarticulo, control, idusuario, tomafisica, existencia, diferencia, valor)"
                + " SELECT a.id, " + contador + "," + idusuario + ",tomafisica1, existencia, existencia-tomafisica1, ultimocosto"
                + " from inventarios.articulos a INNER JOIN inventarios.modelos mo on mo.id = a.idmodelo"
                + "                         inner join inventarios.marcas ma on ma.id = mo.idmarca"
                + "                         inner join inventarios.equipos e on a.idequipo = e.id "
                + "                         inner join inventarios.grupos g on g.id = e.idgrupo " + busqueda + ";";

        sql += "UPDATE inventarios.articulos"
                + "     SET existencia = a.tomafisica1, tomafisica1 = null, fechatoma1 = null, idusuariotoma1=0, tomafisica2 = null, fechatoma2 = null, idusuariotoma2=0"
                + " from inventarios.articulos a INNER JOIN inventarios.modelos mo on mo.id = a.idmodelo"
                + "                            inner join inventarios.marcas ma on ma.id = mo.idmarca"
                + "                           inner join inventarios.equipos e on a.idequipo = e.id "
                + "                          inner join inventarios.grupos g on g.id = e.idgrupo " + busqueda + " AND inventarios.articulos.id = a.id;";
        sql += "UPDATE contadores set tomafisica = " + contador + ";";

        if (Globales.DatosAuditoria(sql, "TOMA FISICA", "CERRAR", idusuario, iplocal, this.getClass() + "-->CerrarTomaFisica")) {
            return "0|Toma física cerrada con el número|" + contador;
        } else {
            return "1|Error inesperado... Revise el log de errores";
        }

    }

    public String ConsultaAuditoriaToma(HttpServletRequest request) {
        int codigo = Globales.Validarintnonull(request.getParameter("codigo"));
        String fechad = request.getParameter("fechad");
        String fechah = request.getParameter("fechah");
        String tipo = request.getParameter("tipo");
        int grupo = Globales.Validarintnonull(request.getParameter("grupo"));
        int equipo = Globales.Validarintnonull(request.getParameter("equipo"));
        String presentacion = request.getParameter("presentacion");
        int marca = Globales.Validarintnonull(request.getParameter("marca"));
        int modelo = Globales.Validarintnonull(request.getParameter("modelo"));
        String busqueda = "where to_char(t.fecha,'yyyy-MM-dd') >= '" + fechad + "' and to_char(t.fecha,'yyyy-MM-dd') <= '" + fechah + "'";
        if (codigo > 0) {
            busqueda += " and t.control = " + codigo;
        } else {
            if (!tipo.equals("")) {
                busqueda += " and a.tipo = '" + tipo + "'";
            }
            if (!presentacion.equals("")) {
                busqueda += " and a.presentacion = '" + presentacion + "'";
            }
            if (grupo > 0) {
                busqueda += " and g.id = " + grupo;
            }
            if (equipo > 0) {
                busqueda += " and e.id = " + equipo;
            }
            if (marca > 0) {
                busqueda += " and ma.id = " + marca;
            }
            if (modelo > 0) {
                busqueda += " and mo.id = " + modelo;
            }
        }

        sql = "select row_number() OVER(order by a.id) as fila, to_char(t.fecha,'yyyy/MM/dd HH24:MI') as fecha, t.control, abreviatura || '-' || trim(to_char(codigointerno,'0000000')) as codigointerno, presentacion, "
                + "      mo.descripcion as modelo, ma.descripcion as marca, g.descripcion as grupo, a.tipo, e.descripcion as equipo, "
                + "      t.existencia, t.tomafisica, t.diferencia, t.diferencia*t.valor as valor, u.nombrecompleto as usuario"
                + "  from inventarios.toma_fisica t inner join inventarios.articulos a on a.id = t.idarticulo"
                + "                           INNER JOIN inventarios.modelos mo on mo.id = a.idmodelo"
                + "                           inner join inventarios.marcas ma on ma.id = mo.idmarca"
                + "                           inner join inventarios.equipos e on a.idequipo = e.id  "
                + "                           inner join inventarios.grupos g on g.id = e.idgrupo "
                + "                           inner join seguridad.rbac_usuario u on u.idusu = t.idusuario " + busqueda + " order by t.control, 3";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ConsultaAuditoriaToma");
    }

    public String TablaTraslado(HttpServletRequest request) {
        int ubicacion = Globales.Validarintnonull(request.getParameter("ubicacion"));
        int responsable = Globales.Validarintnonull(request.getParameter("responsable"));
        int traslado = Globales.Validarintnonull(request.getParameter("traslado"));

        String busqueda = "where td.idoficina = " + ubicacion + " and td.idresponsable = " + responsable + " and td.idusuario=" + idusuario + " and idtraslado = " + traslado;

        sql = "select row_number() OVER(order by a.id) as fila, abreviatura || '-' || trim(to_char(codigointerno,'0000000')) as codigointerno, codigobarras, "
                + "      '<b>Tipo:</b> ' || tipo || ', <b>Grupo:</b> ' || g.descripcion || ', <b>Presentación:</b> ' || presentacion || "
                + "      '<b>, Marca:</b> ' || ma.descripcion ||', <b>Modelo:</b> ' || mo.descripcion as descripcion,"
                + "      td.cantidad, td.cantidad * td.costo as valor, to_char(td.fecha, 'yyyy/MM/dd HH24:MI') as fecha,"
                + "      '<button class=''btn btn-glow btn-danger'' title=''Eliminar Traslado Detalle'' type=''button'' onclick=' || chr(34) || 'EliminarTraslado(' || td.id || ',''' ||  abreviatura || '-' || trim(to_char(codigointerno,'0000000')) || ''')' ||chr(34) || '><span data-icon=''&#xe0d8;''></span></button>' as eliminar"
                + "  from inventarios.traslado_detalle td inner join  inventarios.articulos a on td.idarticulo = a.id"
                + "                              INNER JOIN inventarios.modelos mo on mo.id = a.idmodelo"
                + "                              inner join inventarios.marcas ma on ma.id = mo.idmarca"
                + "                              inner join inventarios.equipos e on a.idequipo = e.id "
                + "                              inner join inventarios.grupos g on g.id = e.idgrupo " + busqueda + " ORDER BY td.id desc";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->TablaTraslado");
    }

    public String GuardarTrasladoDetalle(HttpServletRequest request) {
        int ubicacion = Globales.Validarintnonull(request.getParameter("ubicacion"));
        int responsable = Globales.Validarintnonull(request.getParameter("responsable"));
        int articulo = Globales.Validarintnonull(request.getParameter("articulo"));
        int cantidad = Globales.Validarintnonull(request.getParameter("cantidad"));

        try {
            if (Globales.PermisosSistemas("TRASLADO ARTICULO AGREGAR DETALLE", idusuario) == 0) {
                return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
            }

            sql = "SELECT existencia, ultimocosto from inventarios.articulos where id=" + articulo;
            datos = Globales.Obtenerdatos(sql, this.getClass() + "-->GuardarTrasladoDetalle", 1);

            int existencia = Globales.Validarintnonull(datos.getString("existencia"));
            double costo = Globales.ValidardoublenoNull(datos.getString("ultimocosto"));

            if (existencia <= 0) {
                return "1|El artículo no posee existencias disponibles";
            }
            if (cantidad > existencia) {
                return "1|La cantidad a trasladar [" + cantidad + "] es mayor a la existencia actual del artículo [" + existencia + "]";
            }
            String busqueda = "where td.idoficina = " + ubicacion + " and td.idresponsable = " + responsable + " and td.idusuario=" + idusuario + " and idtraslado = 0";

            sql = "UPDATE inventarios.articulos set existencia = existencia - " + cantidad + " where id = " + articulo + ";";
            sql += "INSERT INTO inventarios.traslado_detalle(idarticulo, idusuario, cantidad, costo, idoficina, idresponsable)"
                    + "      VALUES (" + articulo + "," + idusuario + "," + cantidad + "," + costo + "," + ubicacion + "," + responsable + "); select max(id) from inventarios.traslado_detalle;";
            if (Globales.DatosAuditoria(sql, "TRASLADO ARTICULO", "AGREGAR DETALLE", idusuario, iplocal, this.getClass() + "-->GuardarTrasladoDetalle")) {
                return "0|Artículo agregado con éxito|";
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }
        } catch (Exception e) {
            return "1|" + e.getMessage();
        }
    }

    public String EliminarTraslado(HttpServletRequest request) {
        int detalle = Globales.Validarintnonull(request.getParameter("detalle"));

        if (Globales.PermisosSistemas("TRASLADO ELIMINAR", idusuario) == 0) {
            return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
        }
        sql = "UPDATE inventarios.articulos SET existencia = existencia  + cantidad "
                + " from inventarios.traslado_detalle td "
                + " where inventarios.articulos.id = td.idarticulo and td.id =" + detalle + ";";
        sql += "DELETE FROM inventarios.traslado_detalle WHERE id = " + detalle;

        if (Globales.DatosAuditoria(sql, "TRASLADO ARTICULO", "ELIMINAR", idusuario, iplocal, this.getClass() + "--> EliminarTraslado")) {
            return "0|Items del traslado eliminado con éxito";
        } else {
            return "1|Error inesperado... Revise el log de errores";
        }

    }

    public String InicializarTraslado(HttpServletRequest request) {
        try {
            int ubicacion = Globales.Validarintnonull(request.getParameter("ubicacion"));
            int responsable = Globales.Validarintnonull(request.getParameter("responsable"));

            if (Globales.PermisosSistemas("TRASLADO INICIALIZAR", idusuario) == 0) {
                return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
            }

            sql = "SELECT id, idarticulo, cantidad FROM inventarios.traslado_detalle WHERE idtraslado = 0 and idoficina = " + ubicacion + " and idresponsable = " + responsable + " and idusuario=" + idusuario;
            datos = Globales.Obtenerdatos(sql, this.getClass() + "-->InicializarTraslado", 1);
            sql = "";

            while (datos.next()) {
                sql += "UPDATE inventarios.articulos SET existencia = existencia  + " + datos.getString("cantidad") + "WHERE id = " + datos.getString("idarticulo") + ";";
                sql += "DELETE FROM inventarios.traslado_detalle WHERE idtraslado = 0 and idoficina = " + ubicacion + " and idresponsable = " + responsable + " and idusuario=" + idusuario;

            }
            if (!sql.equals("")) {
                if (Globales.DatosAuditoria(sql, "TRASLADO", "INICIALIZAR", idusuario, iplocal, this.getClass() + "-->InicializarTraslado")) {
                    return "0|Traslado inicializado con éxito";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            } else {
                return "1|Este traslado no contiene items para eliminar";
            }

        } catch (SQLException ex) {
            Logger.getLogger(Inventarios.class.getName()).log(Level.SEVERE, null, ex);
            return "1|" + ex.getMessage();
        }
    }

    public String GuardarTraslado(HttpServletRequest request) {
        int ubicacion = Globales.Validarintnonull(request.getParameter("ubicacion"));
        int responsable = Globales.Validarintnonull(request.getParameter("responsable"));
        int idtraslado;
        try {

            if (Globales.PermisosSistemas("TRASLADO GUARDAR", idusuario) == 0) {
                return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
            }

            sql = "SELECT count(idarticulo), sum(cantidad) as cantidad, sum(cantidad*costo) as costo FROM inventarios.traslado_detalle WHERE idtraslado = 0 and idoficina = " + ubicacion + " and idresponsable = " + responsable + " and idusuario=" + idusuario;
            datos = Globales.Obtenerdatos(sql, this.getClass() + "-->GuardarTraslado", 1);
            if (!datos.next()) {
                return "1|Este traslado no contiene items para trasladar";
            }
            int cantidades = Globales.Validarintnonull(datos.getString("cantidad"));
            double costos = Globales.ValidardoublenoNull(datos.getString("costo"));
            sql = "";
            int contador = Globales.Validarintnonull(Globales.ObtenerUnValor("select traslado from contadores")) + 1;
            sql += "INSERT INTO inventarios.traslado(traslado, idresponsable, idoficina, idusuario, cantidad, costo)"
                    + "  VALUES (" + contador + "," + responsable + "," + ubicacion + "," + idusuario + "," + cantidades + "," + costos + ");"
                    + " ";
            if (Globales.DatosAuditoria(sql, "INVENTARIO", "AGREGAR", idusuario, iplocal, this.getClass() + "-->GuardarTraslado")) {
                idtraslado = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT MAX(id) from inventarios.traslado;"));
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }

            sql = "INSERT INTO inventarios.kardex(idarticulo, cantidad, costo, tipo, idusuario, es, documento)"
                    + "   SELECT idarticulo, cantidad, costo, 'TRASLADO INTERNO',2," + contador + ""
                    + "   FROM inventarios.traslado_detalle "
                    + "   WHERE idtraslado = 0 and idoficina = " + ubicacion + " and idresponsable = " + responsable + " and idusuario=" + idusuario + ";"
                    + "  UPDATE contadores SET traslado=" + contador + ";";

            sql += "UPDATE  inventarios.traslado_detalle  SET idtraslado=" + contador + ""
                    + "  WHERE idtraslado = 0 and idoficina = " + ubicacion + " and idresponsable = " + responsable + " and idusuario=" + idusuario + ";";

            if (Globales.DatosAuditoria(sql, "TRASLADO", "AGREGAR DETALLES", idusuario, iplocal, this.getClass() + "-->GuardarTraslado")) {
                return "0|Traslado guardado con éxito con el número|" + contador + "|" + idtraslado;
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }

        } catch (SQLException ex) {
            Globales.GuardarLogger(ex.getMessage(), this.getClass() + "-->GuardarTraslado");
            return "1|" + ex.getMessage();
        }
    }

    public String AnularTraslado(HttpServletRequest request) {
        try {
            int traslado = Globales.Validarintnonull(request.getParameter("traslado"));
            String observacion = request.getParameter("observacion");

            if (Globales.PermisosSistemas("TRASLADO ANULAR", idusuario) == 0) {
                return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
            }

            sql = "SELECT traslado, estado FROM inventarios.traslado WHERE id = " + traslado;
            datos = Globales.Obtenerdatos(sql, this.getClass() + "-->AnularTraslado", 1);
            datos.next();
            String estado = datos.getString("estado").trim();
            String contador = datos.getString("traslado").trim();

            if (estado.equals("Anulado")) {
                return "1|Este traslado ya fue anulado";
            }

            sql = "update inventarios.traslado set estado='Anulado', idusuarioanula = " + idusuario + ", observacionanula ='" + observacion + "', fechaanula = now()"
                    + "where id = " + traslado + ";";

            sql += "INSERT INTO inventarios.kardex(idarticulo, cantidad, costo, tipo, idusuario, es, documento)"
                    + "  SELECT idarticulo, cantidad, costo, 'TRASLADO INTERNO ANULADO',1," + contador + ""
                    + "  FROM inventarios.traslado_detalle"
                    + "  WHERE idtraslado = " + traslado + ";";

            sql += "UPDATE inventarios.articulos  SET existencia = existencia + "
                    + "  (select sum(cantidad) from inventarios.traslado_detalle td where  inventarios.articulos.id = td.idarticulo and td.idtraslado = " + traslado + ") "
                    + " from inventarios.traslado_detalle "
                    + " where inventarios.articulos.id = td.idarticulo and td.idtraslado = " + traslado;
            if (Globales.DatosAuditoria(sql, "TRASLADO", "ANULAR", idusuario, iplocal, this.getClass() + "-->AnularTraslado")) {
                return "0|Traslado anulado con éxito";
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }
        } catch (SQLException ex) {
            Globales.GuardarLogger(ex.getMessage(), this.getClass() + "-->AnularTraslado");
            return "1|" + ex.getMessage();
        }
    }

    public String TablaConfiguracion(HttpServletRequest request) {
        int tipo = Globales.Validarintnonull(request.getParameter("tipo"));
        int id = Globales.Validarintnonull(request.getParameter("id"));
        int grupo = Globales.Validarintnonull(request.getParameter("grupo"));
        int estado = Globales.Validarintnonull(request.getParameter("estado"));
        switch (tipo) {
            case 1:
                sql = "SELECT g.*, te.descripcion as desestado, cu.nombre as cuenta, "
                        + "      '<button class=''btn btn-glow btn-danger'' title=''Eliminar Grupo'' type=''button'' onclick=' || chr(34) || 'EliminarGrupo(' || g.id || ',''' || g.descripcion  || ''')' ||chr(34) || '><span data-icon=''&#xe0d8;''></span></button>' as eliminar"
                        + "  FROM inventarios.grupos g INNER JOIN tipo_estado te on te.id = g.estado "
                        + "                            INNER JOIN contabilidad.cuenta cu on cu.id = g.idcuecontable " + (id > 0 ? " WHERE g.id=" + id : "") + ""
                        + "  ORDER BY descripcion";
                break;
            case 2:
                sql = "SELECT e.id, e.descripcion, e.estado, idgrupo, te.descripcion as desestado, g.descripcion as grupo,"
                        + "        '<button class=''btn btn-glow btn-danger'' title=''Eliminar Equipo'' type=''button'' onclick=' || chr(34) || 'EliminarEquipo(' || e.id || ',''' || e.descripcion  || ' (' || g.descripcion || ')''' || ')' ||chr(34) || '><span data-icon=''&#xe0d8;''></span></button>' as eliminar"
                        + "    FROM inventarios.equipos e INNER JOIN tipo_estado te on te.id = e.estado"
                        + "			       INNER JOIN inventarios.grupos g on g.id = e.idgrupo " + (id > 0 ? " WHERE e.id=" + id : grupo != 0 ? " WHERE idgrupo = " + grupo : "")
                        + "   ORDER BY e.descripcion";
                break;
            case 3:
                sql = "SELECT m.id, m.descripcion, m.estado, te.descripcion as desestado,"
                        + "      '<button class=''btn btn-glow btn-danger'' title=''Eliminar Marca'' type=''button'' onclick=' || chr(34) || 'EliminarMarca(' || m.id || ',''' || m.descripcion || ''')' || chr(34) || '><span data-icon=''&#xe0d8;''></span></button>' as eliminar"
                        + "  FROM inventarios.marcas m INNER JOIN tipo_estado te on te.id = m.estado " + (id > 0 ? " WHERE m.id = " + id : "");
                break;

            case 4:
                sql = "SELECT mo.id, mo.idmarca, mo.descripcion, mo.estado, te.descripcion as desestado, m.descripcion as marca,"
                        + "          '<button class=''btn btn-glow btn-danger'' title=''Eliminar Modelo'' type=''button'' onclick=' || chr(34) || 'EliminarModelo(' || mo.id || ',''' || mo.descripcion || '(' || m.descripcion || ')'',' || mo.idmarca || ')' ||  chr(34) || '><span data-icon=''&#xe0d8;''></span></button>' as eliminar"
                        + "    FROM inventarios.modelos mo inner join inventarios.marcas m on m.id = mo.idmarca "
                        + "                    INNER JOIN tipo_estado te on te.id = mo.estado " + (id > 0 ? " WHERE mo.id = " + id : "");
                break;
        }
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->AnularTraslado");
    }

    public String GuardarGrupos(HttpServletRequest request) {
        int Gru_Id = Globales.Validarintnonull(request.getParameter("Gru_Id"));
        String Gru_Descripcion = request.getParameter("Gru_Descripcion");
        int Gru_Estado = Globales.Validarintnonull(request.getParameter("Gru_estado"));
        int Gru_CuentaContable = Globales.Validarintnonull(request.getParameter("Gru_CuentaContable"));
        String Gru_Inicial = request.getParameter("Gru_Inicial");

        String Mensaje = "";
        try {
            if (Gru_Id == 0) {
                if (Globales.PermisosSistemas("GRUPO-INVENTARIO GUARDAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "INSERT INTO inventarios.grupos(descripcion, estado, abreviatura, idcuecontable)"
                        + "VALUES ('" + Gru_Descripcion.trim() + "'," + Gru_Estado + ",'" + Gru_Inicial + "'," + Gru_CuentaContable + ")";

                if (Globales.DatosAuditoria(sql, "GRUPO-INVENTARIO", "GUARDAR", idusuario, iplocal, this.getClass() + "-->GuardarGrupos")) {
                    Mensaje = "0|Grupo agregado con éxito";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }

            } else {
                if (Globales.PermisosSistemas("GRUPO-INVENTARIO EDITAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "UPDATE inventarios.grupos"
                        + "SET descripcion='" + Gru_Descripcion.trim() + "', estado=" + Gru_Estado + ",idcuecontable=" + Gru_CuentaContable
                        + ", abreviatura='" + Gru_Inicial + "' WHERE id=" + Gru_Id;

                if (Globales.DatosAuditoria(sql, "GRUPO-INVENTARIO", "EDITAR", idusuario, iplocal, this.getClass() + "-->GuardarGrupos")) {
                    Mensaje = "0|Grupo actualizado con éxito";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }

            }

            return Mensaje;
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String GuardarEquipos(HttpServletRequest request) {
        int Equ_Grupo = Globales.Validarintnonull(request.getParameter("Equ_Grupo"));
        int Equ_Id = Globales.Validarintnonull(request.getParameter("Equ_Id"));
        String Equ_Descripcion = request.getParameter("Equ_Descripcion");
        int Equ_Estado = Globales.Validarintnonull(request.getParameter("Equ_Estado"));
        String Mensaje = "";
        try {
            if (Equ_Id == 0) {
                if (Globales.PermisosSistemas("EQUIPOS-INVENTARIO GUARDAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "INSERT INTO inventarios.equipos(descripcion, estado, idgrupo)"
                        + "VALUES ('" + Equ_Descripcion.trim() + "'," + Equ_Estado + "," + Equ_Grupo + ")";

                if (Globales.DatosAuditoria(sql, "EQUIPOS-INVENTARIO", "GUARDAR", idusuario, iplocal, this.getClass() + "-->GuardarEquipos")) {
                    Mensaje = "0|Equipo agregado con éxito";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }

            } else {
                if (Globales.PermisosSistemas("EQUIPOS-INVENTARIO EDITAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "UPDATE inventarios.equipos"
                        + "SET descripcion='" + Equ_Descripcion.trim() + "', estado=" + Equ_Estado + ",idgrupo=" + Equ_Grupo
                        + "WHERE id=" + Equ_Id;

                if (Globales.DatosAuditoria(sql, "EQUIPOS-INVENTARIO", "EDITAR", idusuario, iplocal, this.getClass() + "-->GuardarEquipos")) {
                    Mensaje = "0|Equipo actualizado con éxito";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }

            }

            return Mensaje;
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String EliminarEquipos(HttpServletRequest request) {
        int Id = Globales.Validarintnonull(request.getParameter("Id"));
        int IdReemplazo = Globales.Validarintnonull(request.getParameter("IdReemplazo"));
        String Mensaje = "";
        try {
            if (Globales.PermisosSistemas("EQUIPOS-INVENTARIO ELIMINAR", idusuario) == 0) {
                return "9|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
            }
            sql = "DELETE FROM inventarios.equipos WHERE id = " + Id;

            if (Globales.DatosAuditoria(sql, "EQUIPOS-INVENTARIO", "ELIMINAR", idusuario, iplocal, this.getClass() + "-->GuardarEquipos")) {
                Mensaje = "0|Equipo eliminado con éxito";
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }

            return Mensaje;
        } catch (Exception ex) {
            if (IdReemplazo > 0) {
                try {
                    sql = "UPDATE remision_detalle SET idequipo = " + IdReemplazo + " WHERE idequipo = " + Id + ";";
                    sql += "UPDATE cotizacion_detalle SET idequipo = " + IdReemplazo + " WHERE idequipo = " + Id + ";";
                    sql += "UPDATE web.solicitud_detalle SET idequipo = " + IdReemplazo + " WHERE idequipo = " + Id + ";";
                    sql += "UPDATE servicio_precios SET idequipo = " + IdReemplazo + " WHERE idequipo = " + Id + ";";
                    sql += "UPDATE instrumento_presion SET idequipo = " + IdReemplazo + " WHERE idequipo = " + Id + ";";
                    sql += "UPDATE instrumento SET idequipo = " + IdReemplazo + " WHERE idequipo = " + Id + ";";
                    sql += "UPDATE metodo_equipo SET idequipo = " + IdReemplazo + " WHERE idequipo = " + Id + ";";
                    sql += "UPDATE ingreso_plantilla SET idequipo = " + IdReemplazo + " WHERE idequipo = " + Id + ";";
                    sql += "DELETE FROM equipo WHERE id = " + Id + ";";

                    if (Globales.DatosAuditoria(sql, "EQUIPOS", "REEMPLAZAR Y ELIMINAR", idusuario, iplocal, this.getClass() + "-->EliminarEquipos")) {
                        Mensaje = "0|Equipo reemplazado y eliminado con éxito";
                    } else {
                        return "1|Error inesperado... Revise el log de errores";
                    }

                    return Mensaje;
                } catch (Exception ex2) {
                    return "1|" + ex2.getMessage();
                }

            }
            return "1|" + ex.getMessage();
        }
    }

    public String GuardarMarcas(HttpServletRequest request) {
        int Mar_Id = Globales.Validarintnonull(request.getParameter("Mar_Id"));
        String Mar_Descripcion = request.getParameter("Mar_Descripcion");
        int Mar_Estado = Globales.Validarintnonull(request.getParameter("Mar_Estado"));

        String Mensaje = "";
        try {
            if (Mar_Id == 0) {
                if (Globales.PermisosSistemas("MARCA-INVENTARIO GUARDAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "INSERT INTO inventarios.marcas(descripcion, estado)"
                        + "VALUES ('" + Mar_Descripcion + "'," + Mar_Estado + ")";

                if (Globales.DatosAuditoria(sql, "MARCA-INVENTARIO", "GUARDAR", idusuario, iplocal, this.getClass() + "-->GuardarMarcas")) {
                    Mensaje = "0|Marca agregado con éxito";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }

            } else {
                if (Globales.PermisosSistemas("MARCA-INVENTARIO EDITAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "UPDATE inventarios.marcas"
                        + "SET descripcion='" + Mar_Descripcion + "', estado=" + Mar_Estado
                        + " WHERE id=" + Mar_Id;

                if (Globales.DatosAuditoria(sql, "MARCA-INVENTARIO", "EDITAR", idusuario, iplocal, this.getClass() + "-->GuardarMarcas")) {
                    Mensaje = "0|Marca actualizada con éxito";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }

            }

            return Mensaje;
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String EliminarMarcas(HttpServletRequest request) {
        int Id = Globales.Validarintnonull(request.getParameter("Id"));
        int IdReemplazo = Globales.Validarintnonull(request.getParameter("IdReemplazo"));

        String Mensaje = "";
        try {
            if (Globales.PermisosSistemas("MARCA-INVENTARIO ELIMINAR", idusuario) == 0) {
                return "9|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
            }
            sql = "DELETE FROM inventarios.marcas WHERE id = " + Id;

            if (Globales.DatosAuditoria(sql, "MARCA-INVENTARIO", "ELIMINAR", idusuario, iplocal, this.getClass() + "-->EliminarMarcas")) {
                Mensaje = "0|Intervalo eliminado con éxito";
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }

            return Mensaje;
        } catch (Exception ex) {
            if (IdReemplazo > 0) {
                try {
                    sql = "UPDATE modelos SET idmarca = " + IdReemplazo + " WHERE idmarca = " + Id + ";";
                    sql += "UPDATE web.solicitud_detalle SET idmarca = " + IdReemplazo + " WHERE idmarca = " + Id + ";";
                    sql += "UPDATE servicio_precios SET idmarca = " + IdReemplazo + " WHERE idmarca = " + Id + ";";
                    sql += "DELETE FROM inventarios.marcas WHERE id = " + Id + ";";

                    if (Globales.DatosAuditoria(sql, "MARCA", "REEMPLAZAR Y ELIMINAR", idusuario, iplocal, this.getClass() + "-->EliminarMarcas")) {
                        Mensaje = "0|Marca reemplazada y eliminada con éxito";
                    } else {
                        return "1|Error inesperado... Revise el log de errores";
                    }

                    return Mensaje;
                } catch (Exception ex2) {
                    return "1|" + ex2.getMessage();
                }

            }
            return "1|" + ex.getMessage();
        }
    }

    public String EliminarModelos(HttpServletRequest request) {
        int Id = Globales.Validarintnonull(request.getParameter("Id"));
        int IdReemplazo = Globales.Validarintnonull(request.getParameter("IdReemplazo"));
        String Mensaje = "";
        try {
            if (Globales.PermisosSistemas("MODELO-INVENTARIO ELIMINAR", idusuario) == 0) {
                return "9|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
            }
            sql = "DELETE FROM inventarios.modelos WHERE id = " + Id;
            if (Globales.DatosAuditoria(sql, "MODELO-INVENTARIO", "ELIMINAR", idusuario, iplocal, this.getClass() + "-->EliminarModelos")) {
                Mensaje = "0|Intervalo eliminado con éxito";
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }

            return Mensaje;
        } catch (Exception ex) {
            if (IdReemplazo > 0) {
                try {
                    sql = "UPDATE remision_detalle SET idmodelo = " + IdReemplazo + " WHERE idmodelo = " + Id + ";";
                    sql += "UPDATE cotizacion_detalle SET idmodelo = " + IdReemplazo + " WHERE idmodelo = " + Id + ";";
                    sql += "UPDATE web.solicitud_detalle SET idmodelo = " + IdReemplazo + " WHERE idmodelo = " + Id + ";";
                    sql += "UPDATE servicio_precios SET idmodelo = " + IdReemplazo + " WHERE idmodelo = " + Id + ";";
                    sql += "DELETE FROM inventarios.modelos WHERE id = " + Id + ";";
                    if (Globales.DatosAuditoria(sql, "MODELO", "REEMPLAZAR Y EDITAR", idusuario, iplocal, this.getClass() + "-->EliminarModelos")) {
                        Mensaje = "0|Modelo reemplazado y eliminado con éxito";
                    } else {
                        return "1|Error inesperado... Revise el log de errores";
                    }
                    return Mensaje;
                } catch (Exception ex2) {
                    return "1|" + ex2.getMessage();
                }

            }
            return "1|" + ex.getMessage();
        }
    }

    public String GuardarModelos(HttpServletRequest request) {
        int Mod_Id = Globales.Validarintnonull(request.getParameter("Mod_Idn"));
        String Mod_Descripcion = request.getParameter("Mod_Descripcion");
        int Mod_Marca = Globales.Validarintnonull(request.getParameter("Mod_Marcan"));
        int Mod_Estado = Globales.Validarintnonull(request.getParameter("Mod_Estadon"));
        String Mensaje = "";
        try {
            if (Mod_Id == 0) {
                if (Globales.PermisosSistemas("MODELO-INVENTARIO GUARDAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "INSERT INTO inventarios.modelos(idmarca, descripcion, estado)"
                        + "VALUES (" + Mod_Marca + ",'" + Mod_Descripcion + "'," + Mod_Estado + ")";
                if (Globales.DatosAuditoria(sql, "MODELO-INVENTARIO", "GUARDAR", idusuario, iplocal, this.getClass() + "-->GuardarModelos")) {
                    Mensaje = "0|Modelo agregado con éxito";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            } else {
                if (Globales.PermisosSistemas("MODELO-INVENTARIO EDITAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "UPDATE inventarios.modelos"
                        + "SET idmarca=" + Mod_Marca + ",descripcion='" + Mod_Descripcion + "', estado=" + Mod_Estado
                        + " WHERE id=" + Mod_Id;

                if (Globales.DatosAuditoria(sql, "MODELO-INVENTARIO", "EDITAR", idusuario, iplocal, this.getClass() + "-->GuardarModelos")) {
                    Mensaje = "0|Modelo actualizado con éxito";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }

            }
            return Mensaje;
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    //FIN DEL CODIGO
    protected void Servidor(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        String ruta_origen = request.getHeader("Origin");

        HttpSession misession = request.getSession();
        idusuario = misession.getAttribute("idusuario").toString();
        iplocal = misession.getAttribute("IPLocal").toString();
        usuario = misession.getAttribute("codusu").toString();
        response.setHeader("Access-Control-Allow-Origin", request.getHeader("Origin"));
        response.setHeader("Access-Control-Allow-Credentials", "true");

        try (PrintWriter out = response.getWriter()) {
            if (!ruta_origen.equals("http://localhost:8090") && !ruta_origen.equals("http://192.168.0.55:82") && !ruta_origen.equals("https://192.168.0.55:90")) {
                out.print("[{\"error\":\"505\",\"mensaje\":\"Usted no tiene permiso para acceder a esta ruta\"}]");
            } else if (misession.getAttribute("codusu") == null) {
                out.println("cerrada");
            } else if (!Globales.IpUsuario(misession.getAttribute("idusuario").toString()).equals(misession.getAttribute("IPLocal").toString())) {
                out.println("cambio-ip");
            } else {
                String opcion = request.getParameter("opcion");
                switch (opcion) {

                    //WILMER
                    case "GuardarOpcion":
                        out.println(GuardarOpcion(request));
                        break;
                    case "CuentaContable_Grupo":
                        out.println(CuentaContable_Grupo(request));
                        break;
                    case "BuscarArticulos":
                        out.println(BuscarArticulos(request));
                        break;
                    case "ConsultarArticulos":
                        out.println(ConsultarArticulos(request));
                        break;
                    case "ValorIva":
                        out.println(ValorIva(request));
                        break;
                    case "CodigoInterno":
                        out.println(CodigoInterno(request));
                        break;
                    case "GuardarArticulos":
                        out.println(GuardarArticulos(request));
                        break;
                    case "ModalArticulos":
                        out.println(ModalArticulos(request));
                        break;
                    case "CodigoTomaFisica":
                        out.println(CodigoTomaFisica(request));
                        break;
                    case "BuscarArticulosToma":
                        out.println(BuscarArticulosToma(request));
                        break;
                    case "GuardarToma":
                        out.println(GuardarToma(request));
                        break;
                    case "TablaTomaFisica":
                        out.println(TablaTomaFisica(request));
                        break;
                    case "InicializarToma":
                        out.println(InicializarToma(request));
                        break;
                    case "EliminarToma":
                        out.println(EliminarToma(request));
                        break;
                    case "ConsultaTomaExcel":
                        out.println(ConsultaTomaExcel(request));
                        break;
                    /*
                            case "ImportarArchivo":
                                out.println(ImportarArchivo(request));
                                break;
                     */
                    case "CerrarTomaFisica":
                        out.println(CerrarTomaFisica(request));
                        break;
                    case "ConsultaAuditoriaToma":
                        out.println(ConsultaAuditoriaToma(request));
                        break;
                    case "TablaTraslado":
                        out.println(TablaTraslado(request));
                        break;
                    case "GuardarTrasladoDetalle":
                        out.println(GuardarTrasladoDetalle(request));
                        break;
                    case "EliminarTraslado":
                        out.println(EliminarTraslado(request));
                        break;
                    case "InicializarTraslado":
                        out.println(InicializarTraslado(request));
                        break;
                    case "GuardarTraslado":
                        out.println(GuardarTraslado(request));
                        break;
                    case "AnularTraslado":
                        out.println(AnularTraslado(request));
                        break;
                    case "TablaConfiguracion":
                        out.println(TablaConfiguracion(request));
                        break;
                    case "GuardarGrupos":
                        out.println(GuardarGrupos(request));
                        break;
                    case "GuardarEquipos":
                        out.println(GuardarEquipos(request));
                        break;
                    case "EliminarEquipos":
                        out.println(EliminarEquipos(request));
                        break;
                    case "GuardarMarcas":
                        out.println(GuardarMarcas(request));
                        break;
                    case "EliminarMarcas":
                        out.println(EliminarMarcas(request));
                        break;
                    case "EliminarModelos":
                        out.println(EliminarModelos(request));
                        break;
                    case "GuardarModelos":
                        out.println(GuardarModelos(request));
                        break;
                    //FIN
                    default:
                        throw new AssertionError();
                }
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Servidor(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Servidor(request, response);
    }

}
