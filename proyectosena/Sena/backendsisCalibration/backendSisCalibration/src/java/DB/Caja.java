package DB;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author leudi
 */
@WebServlet(urlPatterns = {"/Caja"})
public class Caja extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    String sql;
    String idusuario = "";
    String usuario = "";
    ResultSet datos = null;
    String iplocal = "";

    //ANDRES
    public String SaldoTotal(HttpServletRequest request) {
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        int tipo = Globales.Validarintnonull(request.getParameter("tipo"));

        if (tipo == 1) {
            sql = "select sum(case when extract(days from(now() - vencimiento)) >= diasbloqueo then saldo else 0 end) as vencido "
                    + "FROM factura f inner join clientes c on c.id = f.idcliente where f.estado  <> 'Anulado' and saldo > 2000 and f.idcliente = " + cliente;
            double saldo = Globales.ValidardoublenoNull(Globales.ObtenerUnValor(sql));
            sql = "select sum(saldo) as vencido "
                    + "FROM anticipos where estado  <> 'Anulado' and saldo > 2000 and idcliente = " + cliente;
            double anticipo = Globales.ValidardoublenoNull(Globales.ObtenerUnValor(sql));
            return Double.toString(saldo - anticipo);
        } else {
            sql = "select sum(case when extract(days from(now() - vencimiento)) >= diasbloqueo then saldo else 0 end) as vencido, sum(saldo) as saldo "
                    + "FROM factura f inner join clientes c on c.id = f.idcliente where f.estado  <> 'Anulado' and saldo > 2000 and idcliente = " + cliente;
            String sql2 = "select sum(saldo) as saldo from anticipos where estado  <> 'Anulado' and saldo > 2000 and idcliente = " + cliente;
            return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->SaldoTotal") + "|" + Globales.ObtenerDatosJSon(sql2, this.getClass() + "-->SaldoTotal");
        }

    }

    public String TablaMovimientos(HttpServletRequest request) {
        int cuenta = Globales.Validarintnonull(request.getParameter("cuenta"));
        int anio = Globales.Validarintnonull(request.getParameter("anio"));
        int mes = Globales.Validarintnonull(request.getParameter("mes"));
        int tipo = Globales.Validarintnonull(request.getParameter("tipo"));

        String busqueda = "";
        if (tipo == 1) {
            busqueda = " AND monto > 0 AND (movimiento IS NULL or coalesce(operacion,'') = 'PAGOS POR CONFIRMAR') ";
        }
        if (tipo == 2) {
            busqueda = " AND monto < 0 AND (movimiento IS NULL or coalesce(operacion,'') = 'PAGOS POR CONFIRMAR') ";
        }

        sql = "SELECT row_number() OVER(order by m.id) as fila, m.id, anio, mes, idcuenta, idusuario, movimiento, operacion, to_char(fecha,'yyyy-MM-dd') as fecha, \n"
                + "referencia, m.descripcion, oficina, adicional, case when monto > 0 then monto else 0 end as entrada, \n"
                + "case when monto <= 0 then abs(monto) else 0 end as salida, fecharegistro, monto,\n"
                + "b.descripcion || ' - ' || tc.descripcion || ' ' || subString(c.cuenta,length(cuenta)-4,6) as cuenta\n"
                + "FROM movimiento_banco m inner join  bancos_cuenta c on c.id = m.idcuenta\n"
                + "inner join bancos b on c.idbanco = b.id\n"
                + "inner join tipo_cuenta tc on tc.id = c.idtipocuenta\n"
                + "WHERE idcuenta = " + cuenta + " and mes = " + mes + " and anio = " + anio + busqueda + " ORDER BY fecha, m.id";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->TablaMovimientos");
    }

    public String AnularRecibo(HttpServletRequest request) {
        int recibo = Globales.Validarintnonull(request.getParameter("recibo"));
        String observaciones = request.getParameter("observaciones");

        try {
            if (Globales.PermisosSistemas("RECIBO DE CAJA ANULAR", idusuario) == 0) {
                return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
            }
            sql = "SELECT * FROM anular_recibocaja(" + recibo + "," + idusuario + ",'" + observaciones + "')";
            datos = Globales.Obtenerdatos(sql, this.getClass() + "-->AnularRecibo", 1);
            //datos = Globales.DatosAuditoria(sql, "RECIBO DE CAJA", "ANULAR", idusuario, iplocal, this.getClass() + "-->AnularRecibo");
            datos.next();
            if (!datos.getString("_errorcaja").equals("0")) {
                return "1|" + datos.getString("_mensaje");
            } else {
                return "0|Recibo de caja anulado con éxito";
            }
        } catch (Exception ex) {
            return "9|" + ex.getMessage();

        }
    }

    public String GuardarCheque(HttpServletRequest request) {
        int id_cheque = Globales.Validarintnonull(request.getParameter("id_cheque"));
        String Guia = request.getParameter("Guia");
        String ChNumero = request.getParameter("ChNumero");
        double Valor = Globales.ValidardoublenoNull(request.getParameter("Valor"));
        String chFecha = request.getParameter("chFecha");
        String ChCuenta = request.getParameter("ChCuenta");
        String ChNombre = request.getParameter("ChNombre");
        String chCedula = request.getParameter("ChCedula");
        String ChTelefono = request.getParameter("ChTelefono");
        String ChAutorizacion = request.getParameter("ChAutorizacion");
        String chqEstado = request.getParameter("ChqEstado");
        String ChAutorizado = request.getParameter("ChAutorizado");
        String ChDireccion = request.getParameter("ChDireccion");
        int ChBanco = Globales.Validarintnonull(request.getParameter("ChBanco"));
        int ChPlaza = Globales.Validarintnonull(request.getParameter("ChPlaza"));
        int ChTipoC = Globales.Validarintnonull(request.getParameter("ChTipoC"));
        int ChCantidad = Globales.Validarintnonull(request.getParameter("ChCantidad"));

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");

        Date fecha = new Date();

        String estado = "";
        if (ChTipoC == 0) {
            estado = "F";
        } else if (ChTipoC == 1) {
            estado = "P";
        } else {
            estado = "V";
        }

        try {
            if (id_cheque == 0) {
                sql = "";
                for (int x = 1; x <= ChCantidad; x++) {

                    if (x == 1) {
                        fecha = dateFormat.parse(chFecha);
                    } else {
                        calendar.add(Calendar.MONTH, 1);
                    }
                    sql += " INSERT INTO TemporalRecibosCaja (CV, chqNumero,chqValorconsig,chqFechaCheque, chqCtaGirador,chqGirador,chqCedula,chqTelefono,chqAutoriza,chqEstado,chqAutorizadoPor,Direccion, chqCodBanco,chqCodPlaza, Guia)  "
                            + " VALUES ('" + ChNumero + "','" + Valor + "','" + Globales.FechaActual(1) + "','" + ChCuenta + "','" + ChNombre + "','" + chCedula + "','" + ChTelefono + "','" + ChAutorizacion + "','" + estado + "','" + ChAutorizado + "','" + ChDireccion + "','" + ChBanco + "','" + ChPlaza + "','" + Guia + "')";
                    Globales.Obtenerdatos(sql, this.getClass() + "-->GuardarCheque", 2);
                }
                return "0|Agregado con éxito";
            } else {
                sql = "UPDATE TemporalRecibosCaja \n"
                        + "SET chqNumero = '" + ChNumero + "' +\n"
                        + ",chqValorconsig = " + Valor + "\n"
                        + ",chqFechaCheque = '" + chFecha + "' +\n"
                        + ",chqCtaGirador ='" + ChCuenta + "' +\n"
                        + ",chqGirador ='" + ChNombre + "' +\n"
                        + ",chqCedula='" + chCedula + "' +\n"
                        + ",chqEstado = '" + estado + "' +\n"
                        + ",chqTelefono='" + ChTelefono + "' +\n"
                        + ",chqAutoriza='" + ChAutorizacion + "' +\n"
                        + ",chqAutorizadoPor ='" + ChAutorizado + "' +\n"
                        + ",Direccion='" + ChDireccion + "' +\n"
                        + ",chqCodBanco=" + ChBanco + "\n"
                        + ",chqCodPlaza=" + ChPlaza + "\n"
                        + " WHERE id = " + id_cheque;
                Globales.Obtenerdatos(sql, this.getClass() + "-->GuardarCheque", 2);
                return "0|Actualizado con éxito";
            }

        } catch (Exception ex) {
            return "1|" + ex.getMessage();

        }
    }

    public String EliminarCheque(HttpServletRequest request) {
        int id = Globales.Validarintnonull(request.getParameter("id"));
        String guia = request.getParameter("guia");

        try {
            if (id > 0) {
                sql = "DELETE FROM TemporalRecibosCaja WHERE id = " + id;
            } else {
                sql = "DELETE FROM TemporalRecibosCaja WHERE guia = '" + guia + "'";
            }
            Globales.Obtenerdatos(sql, this.getClass() + "-->EliminarCheque", 1);
            return "0|Cheque Eliminado con éxito";

        } catch (Exception ex) {
            return "1|" + ex;

        }

    }

    public String TablaCheque(HttpServletRequest request) {

        String guia = request.getParameter("guia");
        int id = Globales.Validarintnonull(request.getParameter("id"));
        String busqueda = " WHERE Guia = '" + guia + "'";

        //busqueda = " WHERE 1 = 1 ";
        if (id != 0) {
            busqueda = " WHERE id = " + id;
        }

        sql = "SELECT id, NomBanco  as Banco, PlaPlaza as Plaza, chqNumero as Numero,chqCtaGirador as Cuenta, chqValorconsig as Valor,Novedades.dbo.FormatoCortoFecha(chqFechaCheque,0) as Fecha, \n"
                + "chqGirador as Girador,chqCedula as Cedula,chqTelefono as Telefono,Direccion,chqAutoriza as Autoriza,\n"
                + "chqEstado,chqAutorizadoPor as AutorizadoPor,chqCodBanco,chqCodPlaza,chqFechaCheque,\n"
                + "'<button class=''btn btn-glow-primary'' type=''button'' title=''Editar'' onclick=''EditarTemporalCheque(' + cast(id as nvarchar(10)) + ')''><i class=''glyphicon glyphicon-edit''></i></button>' as Edi,\n"
                + "'<button class=''btn btn-glow-danger'' type=''button'' title=''Eliminar'' onclick=''EliminarTemporalCheque(' + cast(id as nvarchar(10)) + ',' + cast(chqNumero as nvarchar(10)) + ')''><i class=''glyphicon glyphicon-remove''></i></button>' as Eli\n"
                + "FROM TemporalRecibosCaja t INNER JOIN general.dbo.Bancos b on t.ChqCodBanco = b.CliCodBanco \n"
                + "INNER JOIN general.dbo.Plazas p on t.ChqCodPlaza = p.PlaCodigo " + busqueda + " ORDER BY id";

        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->TablaCheque");

    }

    public String BuscarComprobante(HttpServletRequest request) {
        int cpago = Globales.Validarintnonull(request.getParameter("cpago"));

        sql = "select SUBSTRING (c.tipo,1,3) as tipo, c.compra,  c.factura, c.subtotal, cd.abonado,  cp.recaudado, c.saldo + c.reteica + c.retefuente + c.reteiva as saldo, total + c.reteica + c.retefuente + c.reteiva as total, to_char(c.fecha,'dd/MM/yyyy') as fecha, to_char(vencimiento,'dd/MM/yyyy') as vencimiento, c.descuento, \n"
                + "c.reteica as reteicafac, c.reteiva as reteivafac, c.retefuente as retefuentefac,\n"
                + "cp.reteica, cp.retecree, cp.reteiva, cp.retefuente, cp.descuento, cp.oingreso, cp.oegreso, cp.efectivo, cp.tarjeta, cp.cheque, cp.consignacion, to_char(cp.fechaanul,'dd/MM/yyyy') as fechaanul, observaanula,\n"
                + "cp.concepto, c.iva, ua.nombrecompleto as usuarioanula, c.descuento as descuentofac, \n"
                + "'<b>Fecha:</b> ' ||  to_char(m.fecha,'dd/MM/yyyy') || ', <b>Referencia:</b> ' || referencia || ', <b>Descripción:</b> ' || m.descripcion || ', <b>Monto:</b> ' || abs(m.monto) as movimiento\n"
                + "FROM comprobante_pago cp inner join comprobante_pago_detalle cd on cp.id = cd.idcpago\n"
                + "inner join compra c on c.id = cd.idcompra\n"
                + "inner join seguridad.rbac_usuario ua on ua.idusu = cp.idusuarioanu\n"
                + "inner join movimiento_banco m on m.id = cp.movimiento\n"
                + "WHERE cp.cpago = " + cpago + " ORDER BY c.id";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->BuscarComprobante");

    }

    public String FacturasPendientesCP(HttpServletRequest request) {
        int proveedor = Globales.Validarintnonull(request.getParameter("proveedor"));
        int tipo = Globales.Validarintnonull(request.getParameter("tipo"));

        sql = "Select c.id, compra, SUBSTRING (tipo,1,3) as tipo, factura, saldo + c.reteica + c.retefuente + c.reteiva as saldo, total + c.reteica + c.retefuente + c.reteiva as total, iva, c.reteica, c.retefuente, c.reteiva, \n"
                + "c.subtotal, to_char(fecha,'dd/MM/yyyy') as fecha, \n"
                + "to_char(vencimiento,'dd/MM/yyyy') as vencimiento, c.descuento,\n"
                + "extract(days from(now() - vencimiento)) as dias\n"
                + "FROM compra c \n"
                + "WHERE saldo > 0 and c.idproveedor = " + proveedor + (tipo == 1 ? " and c.estado  <> 'Anulado'" : "") + " order by c.id";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->FacturasPendientesCP");
    }

    public String FacturasPendientes(HttpServletRequest request) {
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        int tipo = Globales.Validarintnonull(request.getParameter("tipo"));

        sql = "Select factura, saldo + f.reteica + f.retefuente + f.reteiva as saldo, total + f.reteica + f.retefuente + f.reteiva as total, iva, f.reteica, f.retefuente, f.reteiva, \n"
                + "f.subtotal, to_char(fecha,'dd/MM/yyyy') as fecha, \n"
                + "to_char(vencimiento,'dd/MM/yyyy') as vencimiento, f.descuento, u.nombrecompleto as asesor,\n"
                + "extract(days from(now() - vencimiento)) as dias\n"
                + "FROM factura f inner join clientes c on c.id = f.idcliente\n"
                + "inner join seguridad.rbac_usuario u on u.idusu = c.asesor\n"
                + "WHERE saldo > 0 and f.idcliente = " + cliente + (tipo == 1 ? " and f.estado  <> 'Anulado'" : "") + " order by factura";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->FacturasPendientes");
    }

    public String FormaPagoReciboCP(HttpServletRequest request) {
        try {
            int cpago = Globales.Validarintnonull(request.getParameter("cpago"));
            String tipo = request.getParameter("tipo");
            String resultado = "";
            switch (tipo) {
                case "C":
                    sql = "select ChqNumero as Numero, NomBanco as Banco, PlaPlaza as Plaza, Novedades.dbo.FormatoCortoFecha(chqFechaCheque,0) as Fecha, ChqValorConsig as Valor, chqGirador as Girador, chqTelefono as Telefono, Direccion as Direccion, ChqAutoriza as Autoriza, chqAutorizadoPor as Autorizado\n"
                            + "from ChequesRec ch INNER JOIN General.dbo.Bancos b on ChqCodbanco = CliCodBanco\n"
                            + "INNER JOIN General.dbo.Plazas ON PlaCodigo = ChqCodPlaza\n"
                            + "where ChqReciboCaja = " + cpago;
                    datos = Globales.Obtenerdatos(sql, this.getClass() + "-->FormaPagoReciboCP", 1);
                    while (datos.next()) {
                        resultado += "<b>NUMERO:</b> " + datos.getString("Numero") + "<br>"
                                + "<b>BANCO:</b> " + datos.getString("Banco") + "<br>"
                                + "<b>PLAZA:</b> " + datos.getString("Plaza") + "<br>"
                                + "<b>FECHA:</b> " + datos.getString("Fecha") + "<br>"
                                + //float.Parse(datos.getString("Valor")).ToString("C")
                                "<b>VALOR:</b> " + String.valueOf(datos.getString("Valor")) + "<br>"
                                + "<b>GIRADOR:</b> " + datos.getString("Girador") + "<br>"
                                + "<b>TELEFONO:</b> " + datos.getString("Telefono") + "<br>"
                                + "<b>DIRECCION:</b> " + datos.getString("Direccion") + "<br>"
                                + "<b>AUTORIZACION:</b> " + datos.getString("Autoriza") + "<br>"
                                + "<b>AUTORIZADO POR POR:</b> " + datos.getString("Autorizado") + "<br>";
                    }
                    break;
                case "T":
                    sql = "select ReCNumeroTarjeta as Numero, ReCVencimientoTarjeta as Vencimiento, ReCCodSecreto as Secreto, Descripcion as Entidad, ReCAutoriza as Autorizacion,  ReCValorTarjeta as Valor\n"
                            + "from ReciboCajaDetallePagos inner join General.dbo.TCredito on ReCCodTarjeta = Codigo\n"
                            + "where ReCNumero = " + cpago + " AND isnull(ReCValorTarjeta,0) > 0";
                    datos = Globales.Obtenerdatos(sql, this.getClass() + "-->FormaPagoReciboCP", 1);
                    while (datos.next()) {
                        resultado += "<b>NUMERO:</b> " + datos.getString("Numero") + "<br>"
                                + "<b>ENTIDAD:</b> " + datos.getString("Entidad") + "<br>"
                                + //float.Parse(datos.getString("Valor")).ToString("C")
                                "<b>VALOR:</b> " + String.valueOf(datos.getString("Valor")) + "<br>"
                                + "<b>FECHA VENCIMIENTO:</b> " + datos.getString("Vencimiento") + "<br>"
                                + "<b>CÓDIGO SECRETO:</b> " + datos.getString("Secreto") + "<br>"
                                + "<b>AUTORIZACION:</b> " + datos.getString("Autorizacion") + "<br>";
                    }
                    break;
                case "CO":
                    sql = "SELECT  TO_CHAR(p.fecha,'dd/MM/yyyy') as fecha, p.monto, numerocontrol, \n"
                            + "case when tipomov = 'M' THEN 'TRANSFERENCIA' WHEN tipomov = 'E' THEN 'EFECTIVO' ELSE 'CHEQUE' END AS movimiento, \n"
                            + "b.descripcion || ' - ' || tc.descripcion || ' ' || subString(c.cuenta,length(cuenta)-4,6) as cuenta\n"
                            + "FROM comprobante_pago_pagos p INNER JOIN comprobante_pago cp on cp.id = p.idcpago\n"
                            + "INNER JOIN bancos_cuenta c on c.id = p.idcuenta\n"
                            + "INNER JOIN bancos b on b.id = c.idbanco\n"
                            + "INNER JOIN tipo_cuenta tc on tc.id = c.idtipocuenta\n"
                            + "WHERE cpago = " + cpago + " and tipopago = 'CONSIGNACION'";
                    datos = Globales.Obtenerdatos(sql, this.getClass() + "-->FormaPagoReciboCP", 1);
                    while (datos.next()) {
                        resultado += "<b>NUMERO CONTROL:</b> " + datos.getString("numerocontrol") + "<br>"
                                + "<b>CUENTA:</b> " + datos.getString("cuenta") + "<br>"
                                + //float.Parse(datos.getString("Valor")).ToString("C")
                                "<b>VALOR:</b> " + String.valueOf(datos.getString("Valor")) + "<br>"
                                + "<b>FECHA DE CONSIGNACION:</b> " + datos.getString("fecha") + "<br>"
                                + "<b>TIPO DE MOVIMIENTO:</b> " + datos.getString("movimiento") + "<br>";
                    }
                    break;

            }

            return resultado;
        } catch (Exception ex) {
            return "9|" + ex.getMessage();
        }

    }

    public String GuardarRecibo(HttpServletRequest request) {
        int Cliente = Globales.Validarintnonull(request.getParameter("Cliente"));
        String Facturas = request.getParameter("Facturas");
        String Concepto = request.getParameter("Concepto");
        double Recaudado = Globales.ValidardoublenoNull(request.getParameter("Recaudado"));
        double Efectivo = Globales.ValidardoublenoNull(request.getParameter("Efectivo"));

        double RetIVA = Globales.ValidardoublenoNull(request.getParameter("RetIVA"));
        double RetCRE = Globales.ValidardoublenoNull(request.getParameter("RetCRE"));
        double RetICA = Globales.ValidardoublenoNull(request.getParameter("RetICA"));
        double RetFuente = Globales.ValidardoublenoNull(request.getParameter("RetFuente"));
        double Descuento = Globales.ValidardoublenoNull(request.getParameter("Descuento"));
        double OIngresos = Globales.ValidardoublenoNull(request.getParameter("OIngresos"));
        double OEgresos = Globales.ValidardoublenoNull(request.getParameter("OEgresos"));

        int numtarj = Globales.Validarintnonull(request.getParameter("numtarj"));
        double valtarj = Globales.ValidardoublenoNull(request.getParameter("valtarj"));
        int tiptarj = Globales.Validarintnonull(request.getParameter("tiptarj"));
        String auttarj = request.getParameter("auttarj");
        String Guia = request.getParameter("Guia");
        double DiferenciaMenor = Globales.ValidardoublenoNull(request.getParameter("DiferenciaMenor"));

        String confecha = request.getParameter("confecha");
        String contipo = request.getParameter("contipo");
        int concuenta = Globales.Validarintnonull(request.getParameter("concuenta"));
        String connumero = request.getParameter("connumero");
        double Consignacion = Globales.ValidardoublenoNull(request.getParameter("Consignacion"));

        String NombreCliente = request.getParameter("NombreCliente");
        int Movimiento = Globales.Validarintnonull(request.getParameter("Movimiento"));
        String Observacion = request.getParameter("Observacion");

        double Anticipo = Globales.ValidardoublenoNull(request.getParameter("Anticipo"));
        String NumAnticipo = request.getParameter("NumAnticipo");
        String MontoAnticipo = request.getParameter("MontoAnticipo");
        String SaldoAnticipo = request.getParameter("SaldoAnticipo");

        String a_cuenta = request.getParameter("a_cuenta");
        String a_debito = request.getParameter("a_debito");
        String a_credito = request.getParameter("a_credito");
        String a_concepto = request.getParameter("a_concepto");
        double tdebito = Globales.ValidardoublenoNull(request.getParameter("tdebito"));
        double tcredito = Globales.ValidardoublenoNull(request.getParameter("tcredito"));

        Facturas = "array[" + Facturas + "]";

        try {
            if (Globales.PermisosSistemas("RECIBO DE CAJA GUARDAR", idusuario) == 0) {
                return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
            }

            sql = "SELECT * FROM guardarrecibocaja(" + idusuario + "," + Cliente + "," + Facturas + ",'" + Concepto + "',"
                    + Recaudado + "," + Efectivo + "," + DiferenciaMenor + "," + RetIVA + "," + RetICA + "," + RetFuente + ","
                    + RetCRE + "," + Descuento + "," + OIngresos + "," + OEgresos + ","
                    + numtarj + "," + valtarj + "," + tiptarj + ",'" + auttarj + "','" + Guia + "',"
                    + (concuenta == 0 ? 0 : concuenta) + "," + connumero + ",'" + confecha + "','" + contipo + "'," + Consignacion
                    + ",'" + NombreCliente + "'," + Movimiento + ",'" + Observacion + "'," + Anticipo
                    + "," + NumAnticipo + "," + SaldoAnticipo + "," + MontoAnticipo + ","
                    + a_cuenta + "," + a_debito + "," + a_credito + "," + a_concepto + "," + tdebito + "," + tcredito + ")";

            datos = Globales.Obtenerdatos(sql, this.getClass() + "-->GuardarRecibo", 1);
            datos.next();
            if (datos.getString("_caja").equals("0")) {
                return "1|" + datos.getString("_mensaje");
            } else {
                return "0|Recibo de Caja guardado con el número|" + datos.getString("_caja") + "|" + datos.getString("_idcaja") + "|" + datos.getString("_nanticipo") + "|" + datos.getString("asesores");
            }
        } catch (Exception ex) {
            return "9|" + ex.getMessage();
        }

    }

    public String GuardarComprobante(HttpServletRequest request) {
        int Proveedor = Globales.Validarintnonull(request.getParameter("Proveedor"));
        String Compras = request.getParameter("Compras");
        String Concepto = request.getParameter("Concepto");
        double Recaudado = Globales.Validarintnonull(request.getParameter("Recaudado"));
        double Efectivo = Globales.Validarintnonull(request.getParameter("Efectivo"));

        double RetIVA = Globales.Validarintnonull(request.getParameter("RetIVA"));
        double RetCRE = Globales.ValidardoublenoNull(request.getParameter("RetCRE"));
        double RetICA = Globales.ValidardoublenoNull(request.getParameter("RetICA"));
        double RetFuente = Globales.ValidardoublenoNull(request.getParameter("RetFuente"));
        double Descuento = Globales.ValidardoublenoNull(request.getParameter("Descuento"));
        double OIngresos = Globales.ValidardoublenoNull(request.getParameter("OIngresos"));
        double OEgresos = Globales.ValidardoublenoNull(request.getParameter("OEgresos"));

        int numtarj = Globales.Validarintnonull(request.getParameter("numtarj"));
        double valtarj = Globales.ValidardoublenoNull(request.getParameter("valtarj"));
        int tiptarj = Globales.Validarintnonull(request.getParameter("tiptarj"));
        String auttarj = request.getParameter("auttarj");
        String Guia = request.getParameter("Guia");

        String confecha = request.getParameter("confecha");
        String contipo = request.getParameter("contipo");
        int concuenta = Globales.Validarintnonull(request.getParameter("concuenta"));
        String connumero = request.getParameter("connumero");
        double Consignacion = Globales.Validarintnonull(request.getParameter("Consignacion"));

        String NombreProveedor = request.getParameter("NombreProveedor");
        int Movimiento = Globales.Validarintnonull(request.getParameter("Movimiento"));
        String Observacion = request.getParameter("Observacion");

        Compras = "array[" + Compras + "]";

        try {
            if (Globales.PermisosSistemas("COMPROBANTE DE PAGO GUARDAR", idusuario) == 0) {
                return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
            }

            sql = "SELECT * FROM guardarcomprobantepago(" + idusuario + "," + Proveedor + "," + Compras + ",'" + Concepto + "',"
                    + Recaudado + "," + Efectivo + "," + RetIVA + "," + RetICA + "," + RetFuente + ", \n"
                    + RetCRE + "," + Descuento + "," + OIngresos + "," + OEgresos + ", +\n"
                    + numtarj + "," + valtarj + "," + tiptarj + ",'" + auttarj + "','" + Guia + "', \n"
                    + (concuenta == 0 ? 0 : concuenta) + "," + connumero + ",'" + confecha + "','" + contipo + "'," + Consignacion + " \n"
                    + ",'" + NombreProveedor + "'," + Movimiento + ",'" + Observacion + "')";

            Globales.DatosAuditoria(sql, "COMPROBANTE DE PAGO", "GUARDAR", idusuario, iplocal, this.getClass() + "-->GuardarComprobante");
            datos = Globales.Obtenerdatos(sql, this.getClass() + "-->GuardarComprobante", 1);
            //datos = Globales.DatosAuditoria(sql, "COMPROBANTE DE PAGO", "GUARDAR", idusuario, iplocal, this.getClass() + "-->GuardarComprobante");
            if (datos.getString("_cpago").equals("0")) {
                return "1|" + datos.getString("_mensaje");
            } else {
                return "0|Comprobante de pago guardado con el número|" + datos.getString("_cpago") + "|" + datos.getString("_idcpago");
            }
        } catch (Exception ex) {
            return "9|" + ex.getMessage();
        }

    }

    public String ModalComprobantes(HttpServletRequest request) {
        int cpago = Globales.Validarintnonull(request.getParameter("cpago"));
        int compra = Globales.Validarintnonull(request.getParameter("compra"));
        String documento = request.getParameter("documento");
        String proveedor = request.getParameter("proveedor");
        String estado = request.getParameter("estado");
        String fechad = request.getParameter("fechad");
        String fechah = request.getParameter("fechah");
        int dias = Globales.Validarintnonull(request.getParameter("dias"));

        String Busqueda = " where 1 = 1 ";

        if (dias > 0) {

            Date fechaf = new Date();
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(fechaf);
            calendar.add(Calendar.DAY_OF_YEAR, (dias * -1));
            Date fechai = calendar.getTime();
            fechad = dateFormat.format(fechai);
            fechah = dateFormat.format(fechaf);
        }
        if (cpago != 0) {
            Busqueda += " AND cp.cpago = " + cpago;
        } else if (compra > 0) {
            Busqueda += " and c.compra = " + compra;
        } else {
            if (!proveedor.equals("")) {
                Busqueda += " and p.nombrecompleto like '%" + proveedor + "%'";
            }
            if (!documento.equals("")) {
                Busqueda += " and p.documento like '%" + documento + "%'";
            }
            if (!estado.equals("T")) {
                Busqueda += " and cp.estado = '" + estado + "'";
            }
            if (!fechad.trim().equals("")) {
                Busqueda += " and to_char(cp.fecha, 'yyyy-MM-dd') >= '" + fechad + "'";
            }
            if (!fechah.trim().equals("")) {
                Busqueda += " and to_char(cp.fecha, 'yyyy-MM-dd') <= '" + fechah + "'";
            }
        }
        sql = "SELECT  cpago, to_char(cp.fecha,'yyyy/MM/dd HH24:MI') as fecha, recaudado, cp.estado,  p.nombrecompleto as proveedor, \n"
                + "Count(*) AS total, to_char(fechaanul,'yyyy/MM/dd HH24:MI') as anulado, to_char(fechaenvio,'yyyy/MM/dd HH24:MI') as fechaenvio,\n"
                + "u.nombrecompleto as usuario, p.documento\n"
                + "FROM comprobante_pago cp inner join proveedores p on p.id = cp.idproveedor\n"
                + "inner join seguridad.rbac_usuario u on u.idusu = cp.idusuario\n"
                + "inner join comprobante_pago_detalle cd on cd.idcpago = cp.id \n"
                + "inner join compra c on c.id = cd.idcompra " + Busqueda + " \n"
                + "GROUP BY cpago, cp.fecha, recaudado, cp.estado,  p.nombrecompleto, fechaanul,fechaenvio,u.nombrecompleto, p.documento\n"
                + "ORDER BY cpago";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ModalComprobantes");
    }

    public String ModalAnticipos(HttpServletRequest request) {
        int anticipo = Globales.Validarintnonull(request.getParameter("anticipo"));
        String documento = request.getParameter("documento");
        String cliente = request.getParameter("cliente");
        String estado = request.getParameter("estado");
        String fechad = request.getParameter("fechad");
        String fechah = request.getParameter("fechah");
        int dias = Globales.Validarintnonull(request.getParameter("cpago"));

        String Busqueda = " where 1 = 1 ";
        if (dias > 0) {

            Date fechaf = new Date();
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(fechaf);
            calendar.add(Calendar.DAY_OF_YEAR, (dias * -1));
            Date fechai = calendar.getTime();
            fechad = dateFormat.format(fechai);
            fechah = dateFormat.format(fechaf);
        }
        if (anticipo != 0) {
            Busqueda += " AND a.anticipo = " + anticipo;
        } else {

            if (!cliente.equals("")) {
                Busqueda += " and c.nombrecompleto like '%" + cliente + "%'";
            }
            if (!documento.equals("")) {
                Busqueda += " and c.documento like '%" + documento + "%'";
            }
            if (!estado.equals("T")) {
                Busqueda += " and a.estado = '" + estado + "'";
            }
            if (!fechad.trim().equals("")) {
                Busqueda += " and to_char(a.fecha, 'yyyy-MM-dd') >= '" + fechad + "'";
            }
            if (!fechah.trim().equals("")) {
                Busqueda += " and to_char(a.fecha, 'yyyy-MM-dd') <= '" + fechah + "'";
            }
        }

        sql = "SELECT  anticipo, to_char(a.fecha,'yyyy/MM/dd HH24:MI') as fecha, a.monto, a.saldo, a.estado,  c.nombrecompleto as cliente, \n"
                + "to_char(fechaanul,'yyyy/MM/dd HH24:MI') as anulado, to_char(fechaenvio,'yyyy/MM/dd HH24:MI') as fechaenvio,\n"
                + "u.nombrecompleto as usuario, c.documento\n"
                + "FROM anticipos a inner join clientes c on c.id = a.idcliente\n"
                + "inner join seguridad.rbac_usuario u on u.idusu = a.idusuario " + Busqueda + " \n"
                + "ORDER BY anticipo";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ModalAnticipos");
    }

    public String AnularAnticipo(HttpServletRequest request) {
        int anticipo = Globales.Validarintnonull(request.getParameter("anticipo"));
        String observaciones = request.getParameter("observaciones");

        if (Globales.PermisosSistemas("ANTICIPO ANULAR", idusuario) == 0) {
            return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
        }
        try {
            sql = "SELECT * FROM anular_anticipo(" + anticipo + "," + idusuario + ",'" + observaciones + "')";
            Globales.DatosAuditoria(sql, "ANTICIPO", "ANULAR", idusuario, iplocal, this.getClass() + "-->AnularAnticipo");
            datos = Globales.Obtenerdatos(sql, this.getClass() + "-->AnularAnticipo", 1);
            //datos = Globales.DatosAuditoria(sql, "RECIBO DE CAJA", "ANULAR", idusuario, iplocal, this.getClass() + "-->AnularRecibo");
            datos.next();
            if (!datos.getString("_erroranticipo").equals("0")) {
                return "1|" + datos.getString("_mensaje");
            } else {
                return "0|Recibo de caja anulado con éxito";
            }
        } catch (Exception ex) {
            return "9|" + ex.getMessage();
        }
    }

    public String FormaPagoAnticipo(HttpServletRequest request) {
        try {
            int anticipo = Globales.Validarintnonull(request.getParameter("anticipo"));
            String tipo = request.getParameter("tipo");

            String resultado = "";
            switch (tipo) {
                case "C":
                    sql = "select ChqNumero as Numero, NomBanco as Banco, PlaPlaza as Plaza, Novedades.dbo.FormatoCortoFecha(chqFechaCheque,0) as Fecha, ChqValorConsig as Valor, chqGirador as Girador, chqTelefono as Telefono, Direccion as Direccion, ChqAutoriza as Autoriza, chqAutorizadoPor as Autorizado\n"
                            + "from ChequesRec ch INNER JOIN General.dbo.Bancos b on ChqCodbanco = CliCodBanco\n"
                            + "INNER JOIN General.dbo.Plazas ON PlaCodigo = ChqCodPlaza\n"
                            + "where ChqReciboCaja = " + anticipo;
                    datos = Globales.Obtenerdatos(sql, this.getClass() + "-->FormaPagoAnticipo", 1);
                    while (datos.next()) {
                        resultado += "<b>NUMERO:</b> " + datos.getString("Numero") + "<br>"
                                + "<b>BANCO:</b> " + datos.getString("Banco") + "<br>"
                                + "<b>PLAZA:</b> " + datos.getString("Plaza") + "<br>"
                                + "<b>FECHA:</b> " + datos.getString("Fecha") + "<br>"
                                + //float.Parse(datos.getString("Valor")).ToString("C")
                                "<b>VALOR:</b> " + String.valueOf(datos.getString("Valor")) + "<br>"
                                + "<b>GIRADOR:</b> " + datos.getString("Girador") + "<br>"
                                + "<b>TELEFONO:</b> " + datos.getString("Telefono") + "<br>"
                                + "<b>DIRECCION:</b> " + datos.getString("Direccion") + "<br>"
                                + "<b>AUTORIZACION:</b> " + datos.getString("Autoriza") + "<br>"
                                + "<b>AUTORIZADO POR POR:</b> " + datos.getString("Autorizado") + "<br>";
                    }
                    break;
                case "T":
                    sql = "select ReCNumeroTarjeta as Numero, ReCVencimientoTarjeta as Vencimiento, ReCCodSecreto as Secreto, Descripcion as Entidad, ReCAutoriza as Autorizacion,  ReCValorTarjeta as Valor\n"
                            + "from ReciboCajaDetallePagos inner join General.dbo.TCredito on ReCCodTarjeta = Codigo\n"
                            + "where ReCNumero = \" + anticipo + \" AND isnull(ReCValorTarjeta,0) > 0";
                    datos = Globales.Obtenerdatos(sql, this.getClass() + "-->FormaPagoAnticipo", 1);
                    while (datos.next()) {
                        resultado += "<b>NUMERO:</b> " + datos.getString("Numero") + "<br>"
                                + "<b>ENTIDAD:</b> " + datos.getString("Entidad") + "<br>"
                                + //float.Parse(datos.getString("Valor")).ToString("C")
                                "<b>VALOR:</b> " + String.valueOf(datos.getString("Valor")) + "<br>"
                                + "<b>FECHA VENCIMIENTO:</b> " + datos.getString("Vencimiento") + "<br>"
                                + "<b>CÓDIGO SECRETO:</b> " + datos.getString("Secreto") + "<br>"
                                + "<b>AUTORIZACION:</b> " + datos.getString("Autorizacion") + "<br>";
                    }
                    break;
                case "CO":
                    sql = "SELECT  TO_CHAR(p.fecha,'dd/MM/yyyy') as fecha, p.monto, numerocontrol, \n"
                            + "case when tipomov = 'M' THEN 'TRANSFERENCIA' WHEN tipomov = 'E' THEN 'EFECTIVO' ELSE 'CHEQUE' END AS movimiento, \n"
                            + "b.descripcion || ' - ' || tc.descripcion || ' ' || subString(c.cuenta,length(cuenta)-4,6) as cuenta\n"
                            + "FROM anticipos_pagos p INNER JOIN anticipos r on r.id = p.idanticipo\n"
                            + "INNER JOIN bancos_cuenta c on c.id = p.idcuenta\n"
                            + "INNER JOIN bancos b on b.id = c.idbanco\n"
                            + "INNER JOIN tipo_cuenta tc on tc.id = c.idtipocuenta\n"
                            + "WHERE anticipo = " + anticipo + " and tipopago = 'CONSIGNACION'";
                    datos = Globales.Obtenerdatos(sql, this.getClass() + "-->FormaPagoAnticipo", 1);
                    while (datos.next()) {
                        resultado += "<b>NUMERO CONTROL:</b> " + datos.getString("numerocontrol") + "<br>"
                                + "<b>CUENTA:</b> " + datos.getString("cuenta") + "<br>"
                                + //float.Parse(datos.getString("Valor")).ToString("C")
                                "<b>VALOR:</b> " + String.valueOf(datos.getString("Valor")) + "<br>"
                                + "<b>FECHA DE CONSIGNACION:</b> " + datos.getString("fecha") + "<br>"
                                + "<b>TIPO DE MOVIMIENTO:</b> " + datos.getString("movimiento") + "<br>";
                    }
                    break;
            }
            return resultado;
        } catch (Exception ex) {
            return "9|" + ex.getMessage();
        }

    }

    public String GuardarAnticipo(HttpServletRequest request) {
        int Cliente = Globales.Validarintnonull(request.getParameter("Cliente"));
        String Concepto = request.getParameter("Concepto");
        double Anticipo = Globales.ValidardoublenoNull(request.getParameter("Anticipo"));
        double Efectivo = Globales.ValidardoublenoNull(request.getParameter("Efectivo"));

        double Convenio = Globales.ValidardoublenoNull(request.getParameter("Convenio"));
        int numtarj = Globales.Validarintnonull(request.getParameter("numtarj"));
        double valtarj = Globales.ValidardoublenoNull(request.getParameter("valtarj"));
        int tiptarj = Globales.Validarintnonull(request.getParameter("tiptarj"));
        String auttarj = request.getParameter("auttarj");
        String Guia = request.getParameter("Guia");

        String confecha = request.getParameter("confecha");
        String contipo = request.getParameter("contipo");
        int concuenta = Globales.Validarintnonull(request.getParameter("concuenta"));
        String connumero = request.getParameter("connumero");
        double Consignacion = Globales.ValidardoublenoNull(request.getParameter("Consignacion"));

        int Movimiento = Globales.Validarintnonull(request.getParameter("Movimiento"));
        String NombreCliente = request.getParameter("NombreCliente");

        try {

            if (Globales.PermisosSistemas("ANTICIPO GUARDAR", idusuario) == 0) {
                return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
            }

            sql = "SELECT * FROM guardar_anticipo(" + idusuario + "," + Cliente + ",'" + Concepto + "',"
                    + Anticipo + "," + Efectivo + "," + Convenio + ","
                    + numtarj + "," + valtarj + "," + tiptarj + ",'" + auttarj + "','" + Guia + "',"
                    + (concuenta == 0 ? 0 : concuenta) + "," + connumero + ",'" + confecha + "','" + contipo + "'," + Consignacion
                    + "," + Movimiento + ",'" + NombreCliente + "')";

            datos = Globales.Obtenerdatos(sql, this.getClass() + "-->GuardarAnticipo", 1);
            Globales.DatosAuditoria(sql, "ANTICIPO", "GUARDAR", idusuario, iplocal, this.getClass() + "-->GuardarAnticipo");
            if (datos.getString("_anticipo").equals("0")) {
                return "1|" + datos.getString("_mensaje");
            } else {
                return "0|Anticipo guardado con el número|" + datos.getString("_anticipo") + "|" + datos.getString("_idanticipo");
            }
        } catch (Exception ex) {
            return "9|" + ex.getMessage();
        }

    }

    public String BuscarAnticipo(HttpServletRequest request) {
        int anticipo = Globales.Validarintnonull(request.getParameter("anticipo"));

        sql = "Select a.efectivo, a.tarjeta, a.anticipo, a.convenio, a.cheque, a.consignacion, a.tarjeta, a.concepto, a.monto, recibocaja, a.saldo, "
                + "to_char(a.fechaanul, 'dd/MM/yyyy') as fechaanula, ua.nombrecompleto as usuarioanula, a.observaanula, "
                + "'<b>Fecha:</b> ' ||  to_char(m.fecha,'dd/MM/yyyy') || ', <b>Referencia:</b> ' || referencia || ', <b>Descripción:</b> ' || m.descripcion || ', <b>Monto:</b> ' || m.monto as movimiento "
                + "FROM anticipos a inner join movimiento_banco m on m.id = a.movimiento "
                + "inner join seguridad.rbac_usuario ua on ua.idusu = a.idusuarioanu "
                + "WHERE a.anticipo = " + anticipo;
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->BuscarAnticipo");
    }

    public String ModalAnticiposCaja(HttpServletRequest request) {
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));

        String Busqueda = " where idcliente = " + cliente + " and saldo > 1 estado <> 'Anulado'";

        sql = "SELECT  anticipo, to_char(fecha,'yyyy/MM/dd HH24:MI') as fecha, monto, saldo, concepto "
                + "FROM anticipos "
                + "where idcliente = " + cliente + " ORDER BY anticipo";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ModalAnticiposCaja");
    }

    public String BuscarRecibo(HttpServletRequest request) {
        int recibo = Globales.Validarintnonull(request.getParameter("recibo"));

        sql = "Select cd.factura, f.subtotal, cd.abonado,  r.recaudado, f.saldo + f.reteica + f.retefuente + f.reteiva as saldo, total + f.reteica + f.retefuente + f.reteiva as total, to_char(f.fecha,'dd/MM/yyyy') as fecha, to_char(vencimiento,'dd/MM/yyyy') as vencimiento, f.descuento, u.nombrecompleto as asesor, "
                + "f.reteica as reteicafac, f.reteiva as reteivafac, f.retefuente as retefuentefac, "
                + "r.reteica, r.retecree, r.reteiva, r.retefuente, r.descuento, r.oingreso, r.oegreso, r.efectivo, r.tarjeta, r.cheque, r.consignacion, to_char(r.fechaanul,'dd/MM/yyyy') as fechaanul, observaanula, "
                + "r.concepto, f.iva, ua.nombrecompleto as usuarioanula, f.descuento as descuentofac, r.anticipo, r.diferenciamenor, "
                + "'<b>Fecha:</b> ' ||  to_char(m.fecha,'dd/MM/yyyy') || ', <b>Referencia:</b> ' || referencia || ', <b>Descripción:</b> ' || m.descripcion || ', <b>Monto:</b> ' || m.monto as movimiento "
                + "FROM recibo_caja r inner join recibo_caja_detalle cd on r.id = cd.idcaja "
                + "inner join factura f on f.factura = cd.factura "
                + "inner join clientes c on c.id = f.idcliente "
                + "inner join seguridad.rbac_usuario u on u.idusu = c.asesor "
                + "inner join seguridad.rbac_usuario ua on ua.idusu = r.idusuarioanu "
                + "inner join movimiento_banco m on m.id = r.movimiento "
                + "WHERE r.caja = " + recibo + " ORDER BY cd.factura";
        String sql2 = "SELECT tipopago, idcuenta FROM recibo_caja_pagos cp inner join recibo_caja rc on cp.idcaja = rc.id "
                + "where caja = " + recibo + " and tipopago <> 'EFECTIVO'"
                + "group by tipopago, idcuenta";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->BuscarRecibo") + "||" + Globales.ObtenerDatosJSon(sql2, this.getClass() + "-->BuscarRecibo");
    }

    public String ModalRecibos(HttpServletRequest request) {
        int recibo = Globales.Validarintnonull(request.getParameter("recibo"));
        int factura = Globales.Validarintnonull(request.getParameter("factura"));
        String documento = request.getParameter("documento");
        String cliente = request.getParameter("cliente");
        String estado = request.getParameter("estado");
        String fechad = request.getParameter("fechad");
        String fechah = request.getParameter("fechah");
        int dias = Globales.Validarintnonull(request.getParameter("dias"));

        String Busqueda = " where 1 = 1 ";
        if (dias > 0) {

            Date fechaf = new Date();
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(fechaf);
            calendar.add(Calendar.DAY_OF_YEAR, (dias * -1));
            Date fechai = calendar.getTime();
            fechad = dateFormat.format(fechai);
            fechah = dateFormat.format(fechaf);
        }
        if (recibo != 0) {
            Busqueda += " AND r.caja = " + recibo;
        } else if (factura > 0) {
            Busqueda += " and rd.factura = " + factura;
        } else {
            if (!cliente.equals("")) {
                Busqueda += " and c.nombrecompleto like '%" + cliente + "%'";
            }
            if (!documento.equals("")) {
                Busqueda += " and c.documento like '%" + documento + "%'";
            }
            if (!estado.equals("T")) {
                Busqueda += " and r.estado = '" + estado + "'";
            }
            if (!fechad.trim().equals("")) {
                Busqueda += " and to_char(r.fecha, 'yyyy-MM-dd') >= '" + fechad + "'";
            }
            if (!fechah.trim().equals("")) {
                Busqueda += " and to_char(r.fecha, 'yyyy-MM-dd') <= '" + fechah + "'";
            }
        }
        sql = "SELECT  caja, to_char(fecha,'yyyy/MM/dd HH24:MI') as fecha, recaudado + anticipo as recaudado, r.estado,  c.nombrecompleto as cliente, \n"
                + "Count(*) AS total, to_char(fechaanul,'yyyy/MM/dd HH24:MI') as anulado, to_char(fechaenvio,'yyyy/MM/dd HH24:MI') as fechaenvio,\n"
                + "u.nombrecompleto as usuario, c.documento\n"
                + "FROM recibo_caja r inner join clientes c on c.id = r.idcliente\n"
                + "inner join seguridad.rbac_usuario u on u.idusu = r.idusuario\n"
                + "inner join recibo_caja_detalle rd on rd.idcaja = r.id " + Busqueda + " \n"
                + "GROUP BY caja, fecha, recaudado, r.estado,  c.nombrecompleto, fechaanul,fechaenvio,u.nombrecompleto, c.documento, anticipo\n"
                + "ORDER BY caja";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ModalRecibos");
    }

    public String Contabilizar_ReciboCaja(HttpServletRequest request) {

        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        double recaudado = Globales.ValidardoublenoNull(request.getParameter("recaudado"));
        double reteiva = Globales.ValidardoublenoNull(request.getParameter("reteiva"));
        double retefuente = Globales.ValidardoublenoNull(request.getParameter("retefuente"));
        double reteica = Globales.ValidardoublenoNull(request.getParameter("reteica"));

        double deposito = Globales.ValidardoublenoNull(request.getParameter("deposito"));
        int banco_dep = Globales.Validarintnonull(request.getParameter("banco_dep"));
        double efectivo = Globales.ValidardoublenoNull(request.getParameter("efectivo"));
        double anticipo = Globales.ValidardoublenoNull(request.getParameter("anticipo"));
        double descuento = Globales.ValidardoublenoNull(request.getParameter("descuento"));
        double otroegreso = Globales.ValidardoublenoNull(request.getParameter("otroegreso"));
        double otroingreso = Globales.ValidardoublenoNull(request.getParameter("otroingreso"));
        double diferencia = Globales.ValidardoublenoNull(request.getParameter("diferencia"));
        int buscar = Globales.Validarintnonull(request.getParameter("buscar"));

        int idreteiva = Globales.Validarintnonull(request.getParameter("idreteiva"));
        int idretefuente = Globales.Validarintnonull(request.getParameter("idretefuente"));
        int idreteica = Globales.Validarintnonull(request.getParameter("idreteica"));
        String facturas = request.getParameter("facturas");
        try {
            sql = "SELECT _idcuenta, _cuenta, _concepto, sum(_debito) as _debito, sum(_credito) as _credito, _error, _mensaje  "
                    + "  FROM contabilidad.contabilizacion_recibocaja(" + cliente + "," + recaudado + "," + reteiva + "," + retefuente + "," + reteica + ","
                    + idreteiva + "," + idretefuente + "," + idreteica + ","
                    + deposito + "," + banco_dep + "," + efectivo + "," + anticipo + "," + descuento + "," + otroegreso + "," + otroingreso + "," + diferencia + "," + idusuario + "," + buscar + ",'" + facturas + "')"
                    + " group by _idcuenta, _cuenta, _orden, _error, _mensaje, _concepto order by _orden";

            return "0||" + Globales.ObtenerDatosJSon(sql, this.getClass() + "-->Contabilizar_ReciboCaja");
        } catch (Exception ex) {
            return "1||" + ex.getMessage();
        }
    }

    public String FacturasNotaCredito(HttpServletRequest request) {
        int Factura = Globales.Validarintnonull(request.getParameter("Factura"));
        sql = "SELECT nota FROM nota_credito where factura = " + Factura + " and estado <> 'Anulado'";
        int encontrado = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
        if (encontrado > 0) {
            return "1|Esta factura ya se le realizó una nota de crédito con número " + encontrado;
        }
        sql = "SELECT fd.id, fd.descripcion, precio, fd.descuento, cantidad, fd.subtotal, fd.iva, c.direccion, c.email, f.saldo, c.id as idcliente, plazopago, tp.descripcion as tipocliente, \n"
                + "f.reteiva * 100 /(f.iva) as clireteiva, f.reteica* 100 /(f.subtotal-f.descuento) as clireteica, f.retefuente* 100 /(f.subtotal-f.descuento) as cliretefuente, c.documento, c.nombrecompleto as cliente, ci.descripcion || ' ' || d.descripcion as ciudad, coalesce(telefono,'') || ' / ' || coalesce(celular,'') as telefono, p.descripcion as tbprecio\n"
                + "FROM factura_datelle fd inner join factura f on f.id = fd.idfactura\n"
                + "inner join clientes c on c.id = idcliente\n"
                + "inner join ciudad ci on ci.id = c.idciudad\n"
                + "inner join departamento d on d.id = iddepartamento\n"
                + "inner join tipocliente tp on tp.id = idtipcli\n"
                + "INNER JOIN tablaprecios p ON p.id = c.tablaprecio\n"
                + "where factura = " + Factura + " order by fd.id";
        return "0|" + Globales.ObtenerDatosJSon(sql, this.getClass() + "-->FacturasNotaCredito");
    }

    public String BuscarNotaCredito(HttpServletRequest request) {
        int Nota = Globales.Validarintnonull(request.getParameter("Nota"));

        sql = "SELECT n.factura, ncd.descripcion, precio, ncd.descuento, cantidad, ncd.subtotal, ncd.iva, c.direccion, c.email, f.saldo, c.id as idcliente, plazopago, tp.descripcion as tipocliente, \n"
                + "c.documento, c.nombrecompleto as cliente, ci.descripcion || ' ' || d.descripcion as ciudad, coalesce(c.telefono,'') || ' / ' || coalesce(c.celular,'') as telefono, p.descripcion as tbprecio,\n"
                + "case when n.estado = 'Anulado' then 'Anulado por ' || ua.nombrecompleto || ' <b>fecha:</b> ' || to_char(n.fechaanula,'dd/MM/yyyy') || ' <b>observación:</b> ' || n.obseranula else '' end as anulado,\n"
                + "u.nombrecompleto as usuario, n.estado, to_char(n.fechareg,'dd/MM/yyyy HH24:MI') as fechareg, to_char(n.fecha,'yyyy-MM-dd') as fecha, n.observacion\n"
                + "FROM nota_credito n inner join nota_credito_datelle ncd on n.id = ncd.idnota\n"
                + "inner join factura f on f.factura = n.factura\n"
                + "inner join clientes c on c.id = f.idcliente\n"
                + "inner join ciudad ci on ci.id = c.idciudad\n"
                + "inner join departamento d on d.id = iddepartamento\n"
                + "inner join tipocliente tp on tp.id = idtipcli\n"
                + "INNER JOIN tablaprecios p ON p.id = c.tablaprecio\n"
                + "inner join seguridad.rbac_usuario u on u.idusu = n.idusuario\n"
                + "inner join seguridad.rbac_usuario ua on ua.idusu = n.idusuarioanula\n"
                + "where nota = " + Nota + " order by ncd.id";
        return "0|" + Globales.ObtenerDatosJSon(sql, this.getClass() + "-->BuscarNotaCredito");
    }

    public String AnularNotaCredito(HttpServletRequest request) {
        int Nota = Globales.Validarintnonull(request.getParameter("Nota"));
        String Observaciones = request.getParameter("Observaciones");

        if (Globales.PermisosSistemas("NOTA CREDITO ANULAR", idusuario) == 0) {
            return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
        }
        try {
            sql = "SELECT estado, factura, total FROM nota_credito where nota = " + Nota;
            datos = Globales.Obtenerdatos(sql, this.getClass() + "-->AnularNotaCredito", 1);
            datos.next();
            String estado = datos.getString("estado");
            String factura = datos.getString("factura");
            String total = datos.getString("total");

            if (estado.equals("Anulado")) {
                return "1|Esta nota de crédito ya fue anulada";
            }

            sql = "UPDATE nota_credito\n"
                    + "SET estado='Anulado', fechaanula=now(), idusuarioanula=\" + idusuario + \",obseranula='\" + Observaciones +\n"
                    + "'WHERE nota = " + Nota + ";";
            sql += "UPDATE factura set saldo = saldo  + " + total + " WHERE factura = " + factura;
            Globales.DatosAuditoria(sql, "NOTA CREDITO", "ANULAR", idusuario, iplocal, this.getClass() + "-->AnularNotaCredito");
            return "0|Nota de crédito anulada con éxito";
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String ConsultarNotaCre(HttpServletRequest request) {
        int nota = Globales.Validarintnonull(request.getParameter("nota"));
        int factura = Globales.Validarintnonull(request.getParameter("factura"));
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        String fechad = request.getParameter("fechad");
        String fechah = request.getParameter("fechah");
        int usuarios = Globales.Validarintnonull(request.getParameter("codusu"));
        String estado = request.getParameter("estado");

        String busqueda = "WHERE 1=1 ";
        if (nota > 0) {
            busqueda += " AND n.nota = " + nota;
        } else if (factura != 0) {
            busqueda += " AND n.factura = " + factura;
        } else {
            busqueda += " AND to_char(n.fecha,'yyyy-MM-dd') >= '" + fechad + "' and to_char(n.fecha,'yyyy-MM-dd') <= '" + fechah + "'";
            if (cliente > 0) {
                busqueda += " AND f.idcliente = " + cliente;
            }
            if (usuarios > 0) {
                busqueda += " AND n.idusuario = " + usuario;
            }
            if (!estado.equals("Todos")) {
                busqueda += " AND n.estado = '" + estado + "'";
            }
        }

        String sql2 = "SELECT n.nota, n.factura, n.subtotal, n.iva, n.total, n.descuento,  c.direccion, f.saldo, c.id as idcliente, n.reteiva, n.reteica, n.retefuente,\n"
                + "c.nombrecompleto || ' (' || c.documento || ')'  as cliente, ci.descripcion || '<br>' || d.descripcion as ciudad, coalesce(c.telefono,'') || ' / ' || coalesce(c.celular,'') || '<br>' || c.direccion as telefono, c.email,\n"
                + "to_char(n.fecha,'yyyy/MM/dd') as fecha, to_char(n.fechareg,'yyyy/MM/dd HH24:MI') as fechareg,\n"
                + "to_char(n.fechaanula,'yyyy-MM-dd') || '<br>' || ua.nombrecompleto as anulado, n.estado,\n"
                + "to_char(n.fechaenvio,'yyyy-MM-dd') || '<br>' || ue.nombrecompleto as enviado, u.nombrecompleto as usuario,\n"
                + "'<button class=''btn btn-glow btn-success imprimir'' title=''Imprimir Nota de Crédito'' type=''button'' onclick=' || chr(34) || 'ImprimirNotaCre(' || n.nota || ')' ||chr(34) || '><span data-icon=''&#xe0f7;''></span></button>' as imprimir\n"
                + "FROM nota_credito n inner join factura f on f.factura = n.factura\n"
                + "inner join clientes c on c.id = f.idcliente\n"
                + "inner join ciudad ci on ci.id = c.idciudad\n"
                + "inner join departamento d on d.id = iddepartamento\n"
                + "INNER JOIN tablaprecios p ON p.id = c.tablaprecio\n"
                + "inner join seguridad.rbac_usuario u on u.idusu = n.idusuario\n"
                + "inner join seguridad.rbac_usuario ua on ua.idusu = n.idusuarioanula \n"
                + "inner join seguridad.rbac_usuario ue on ue.idusu = n.idusuarioenvia " + busqueda + " order by n.nota";
        return Globales.ObtenerDatosJSon(sql2, this.getClass() + "-->ConsultarNotaCre ");
    }

    public String ModalFacturas(HttpServletRequest request) {
        int Factura = Globales.Validarintnonull(request.getParameter("Factura"));
        int Cliente = Globales.Validarintnonull(request.getParameter("Cliente"));
        String FechaDesde = request.getParameter("FechaDesde");
        String FechaHasta = request.getParameter("FechaHasta");

        String Busqueda = " WHERE saldo > 0";

        if (Factura != 0) {
            Busqueda += " AND f.factura = " + Factura;
        } else {
            if (Cliente != 0) {
                Busqueda += " and f.idcliente = " + Cliente;
            }
            if (!FechaDesde.trim().equals("")) {
                Busqueda += " and to_char(f.fecha, 'yyyy-MM-dd') >= '" + FechaDesde + "'";
            }
            if (!FechaHasta.trim().equals("")) {
                Busqueda += " and to_char(f.fecha, 'yyyy-MM-dd') <= '" + FechaHasta + "'";
            }
        }

        sql = "SELECT cli.nombrecompleto || ' (' || cli.documento || ')' AS cliente, factura, f.total, "
                + "to_char(f.fechaenvio,'yyyy/MM/dd HH24:MI')  || '<br>' || ue.nombrecompleto as envio, "
                + "to_char(f.fechaanula,'yyyy/MM/dd HH24:MI')  || '<br>' || ua.nombrecompleto as anula, "
                + "saldo, f.estado, f.total-saldo as abonado, to_char(f.fecha,'yyyy/MM/dd') as fecha "
                + "FROM factura f inner JOIN clientes cli on cli.id = f.idcliente "
                + "inner JOIN seguridad.rbac_usuario ua on ua.idusu = f.idusuarioanula "
                + "inner JOIN seguridad.rbac_usuario ue on ue.idusu = f.idusuarioenvia " + Busqueda + " ORDER BY factura";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ModalFacturas ");
    }

    public String TipoArchivoBanco(HttpServletRequest request) {
        int cuenta = Globales.Validarintnonull(request.getParameter("cuenta"));
        sql = "SELECT tipoarchivo FROM bancos b inner join bancos_cuenta c on c.idbanco = b.id where c.id = " + cuenta;
        String resultado = Globales.ObtenerUnValor(sql);
        if (resultado.equals("")) {
            resultado = "XX";
        }
        return resultado;
    }

    public String TablaMovimientosResumen(HttpServletRequest request) {
        int cuenta = Globales.Validarintnonull(request.getParameter("cuenta"));
        int banco = Globales.Validarintnonull(request.getParameter("banco"));
        String fechad = request.getParameter("fechad");
        String fechah = request.getParameter("fechah");
        String tipo = request.getParameter("tipo");

        String busqueda = "WHERE to_char(m.fecha,'yyyy-MM-dd') >= '" + fechad + "' and to_char(m.fecha,'yyyy-MM-dd') <= '" + fechah + "'";
        if (banco > 0) {
            busqueda = " AND b.id = " + banco;
        }
        if (cuenta > 0) {
            busqueda = " AND c.id = " + cuenta;
        }

        if (tipo.equals("Detallado")) {
            sql = "SELECT c.id, anio || '/' || mes as fecha, sum(case when monto > 0 then monto else 0 end) as entrada, \n"
                    + "sum(case when monto <= 0 then monto else 0 end) as salida,\n"
                    + "b.descripcion || ' - ' || tc.descripcion || ' ' || subString(c.cuenta,length(cuenta)-4,6) as cuenta\n"
                    + "FROM movimiento_banco m inner join  bancos_cuenta c on c.id = m.idcuenta\n"
                    + "inner join bancos b on c.idbanco = b.id  \n"
                    + "INNER JOIN tipo_cuenta tc on tc.id = c.idtipocuenta " + busqueda + " \n"
                    + "group by anio, mes, b.descripcion, tc.descripcion, c.cuenta, c.id\n"
                    + " ORDER BY c.id, anio, mes";
        } else {
            sql = "SELECT 1 as id, 'TODOS LOS BANCOS' AS cuenta, anio || '/' || mes as fecha, sum(case when monto > 0 then monto else 0 end) as entrada, \n"
                    + "sum(case when monto <= 0 then monto else 0 end) as salida\n"
                    + "FROM movimiento_banco m inner join  bancos_cuenta c on c.id = m.idcuenta\n"
                    + "inner join bancos b on c.idbanco = b.id  \n"
                    + "INNER JOIN tipo_cuenta tc on tc.id = c.idtipocuenta \" + busqueda + @\" \n"
                    + "group by anio, mes\n"
                    + "ORDER BY anio, mes";
        }

        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->TablaMovConsolidado ");
    }

    public String TablaMovConsolidado(HttpServletRequest request) {
        int cuenta = Globales.Validarintnonull(request.getParameter("cuenta"));
        int banco = Globales.Validarintnonull(request.getParameter("banco"));
        String fechad = request.getParameter("fechad");
        String fechah = request.getParameter("fechah");

        String busqueda = "WHERE to_char(m.fecha,'yyyy-MM-dd') >= '" + fechad + "' and to_char(m.fecha,'yyyy-MM-dd') <= '" + fechah + "'";
        if (banco > 0) {
            busqueda = " AND b.id = " + banco;
        }
        if (cuenta > 0) {
            busqueda = " AND c.id = " + cuenta;
        }

        sql = "SELECT row_number() OVER(order by m.id) as fila, m.id, anio, mes, idcuenta, idusuario, movimiento, operacion, to_char(fecha,'yyyy-MM-dd') as fecha, \n"
                + "referencia, m.descripcion, oficina, adicional, case when monto > 0 then monto else 0 end as entrada, \n"
                + "case when monto <= 0 then monto else 0 end as salida, fecharegistro, monto,\n"
                + "b.descripcion || ' - ' || tc.descripcion || ' ' || subString(c.cuenta,length(cuenta)-4,6) as cuenta\n"
                + "FROM movimiento_banco m inner join  bancos_cuenta c on c.id = m.idcuenta\n"
                + "inner join bancos b on c.idbanco = b.id \n"
                + "INNER JOIN tipo_cuenta tc on tc.id = c.idtipocuenta " + busqueda + " ORDER BY fecha, m.id";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->TablaMovConsolidado ");
    }

    public String DetalleMovimientos(HttpServletRequest request) {
        int id = Globales.Validarintnonull(request.getParameter("id"));

        sql = "SELECT movimiento, operacion, to_char(fecha,'yyyy-MM-dd') as fecha, \n"
                + "referencia, descripcion, case when monto > 0 then monto else 0 end as entrada, \n"
                + "case when monto <= 0 then monto else 0 end as salida\n"
                + "FROM movimiento_banco \n"
                + "WHERE id = " + id;
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->DetalleMovimientos ");
    }

    public String EditarMovimientos(HttpServletRequest request) {
        int id = Globales.Validarintnonull(request.getParameter("id"));
        String movimiento = request.getParameter("movimiento");
        String operacion = request.getParameter("operacion");
        if (movimiento == null) {
            movimiento = "";
        }
        if (operacion == null) {
            operacion = "";
        }

        if (Globales.PermisosSistemas("MOVIMIENTO BANCARIO EDITAR", idusuario) == 0) {
            return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
        }
        sql = "UPDATE movimiento_banco SET movimiento='" + movimiento + "', operacion='" + operacion
                + "' WHERE id = " + id;
        Globales.DatosAuditoria(sql, "MOVIMIENTO BANCARIO", "EDITAR", idusuario, iplocal, this.getClass() + "-->EditarMovimientos");
        return "0|Movimiento actualizado";

    }

    public String GuardarNotaCredito(HttpServletRequest request) {
        int factura = Globales.Validarintnonull(request.getParameter("factura"));
        double total = Globales.ValidardoublenoNull(request.getParameter("total"));
        double subtotal = Globales.ValidardoublenoNull(request.getParameter("subtotal"));
        double descuento = Globales.ValidardoublenoNull(request.getParameter("descuento"));
        double gravable = Globales.ValidardoublenoNull(request.getParameter("gravable"));
        double exento = Globales.ValidardoublenoNull(request.getParameter("exento"));

        double iva = Globales.ValidardoublenoNull(request.getParameter("iva"));
        double reteiva = Globales.ValidardoublenoNull(request.getParameter("reteiva"));
        double retefuente = Globales.ValidardoublenoNull(request.getParameter("retefuente"));
        double reteica = Globales.ValidardoublenoNull(request.getParameter("reteica"));
        String observacion = request.getParameter("observacion");
        String fechanot = request.getParameter("fechanot");
        String a_ingreso = request.getParameter("a_ingreso");
        String a_orden = request.getParameter("a_orden");
        String a_certificado = request.getParameter("a_certificado");
        String a_cotizacion = request.getParameter("a_cotizacion");
        String a_precio = request.getParameter("a_precio");
        String a_cantidad = request.getParameter("a_cantidad");
        String a_descuento = request.getParameter("a_descuento");
        String a_iva = request.getParameter("a_iva");
        String a_subtotal = request.getParameter("a_subtotal");
        String a_descripcion = request.getParameter("a_descripcion");

        if (Globales.PermisosSistemas("NOTA CREDITO GUARDAR", idusuario) == 0) {
            return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
        }
        try {
            sql = " SELECT * FROM guardar_nota_credito(" + factura + ",'" + fechanot + "'," + total + "," + subtotal + "," + descuento + "," + gravable
                    + "," + exento + "," + iva + "," + reteiva + "," + retefuente + "," + reteica + ",'" + observacion
                    + "'," + a_descripcion + "," + a_precio + "," + a_cantidad + "," + a_descuento + "," + a_iva + "," + a_subtotal + "," + idusuario + ")";

            Globales.DatosAuditoria(sql, "NOTA CREDITO", "GUARDAR", idusuario, iplocal, this.getClass() + "-->GuardarNotaCredito");
            datos = Globales.Obtenerdatos(sql, this.getClass() + "-->GuardarNotaCredito", 1);
            datos.next();

            if (datos.getString("_nota").equals("0")) {
                return "1|" + datos.getString("_mensaje");
            } else {
                return "0|Nota de crédito guardada con el número|" + datos.getString("_nota");
            }
        } catch (Exception ex) {
            return "9|" + ex.getMessage();
        }
    }

    public String ActualizarCorCartera(HttpServletRequest request) {
        int id = Globales.Validarintnonull(request.getParameter("id"));
        String correo = request.getParameter("correo");

        if (Globales.PermisosSistemas("CARTERA ACTUALIZAR CORREO", idusuario) == 0) {
            return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
        }
        sql = "UPDATE clientes SET emailcartera='" + correo + "' WHERE id = " + id;
        Globales.DatosAuditoria(sql, "CARTERA", "ACTUALIZAR CORREO CARTERA", idusuario, iplocal, this.getClass() + "-->ActualizarCorCartera");
        return "0|Correo electrónico actualizado con éxito";

    }

    public String VerGestionCartera(HttpServletRequest request) {
        int Cliente = Globales.Validarintnonull(request.getParameter("Cliente"));
        String Fecha = request.getParameter("Fecha");

        sql = "SELECT seguimiento || '|' || devolucion as seguimiento FROM seguimiento_cartera where to_char(fecha,'yyyy-MM-dd') = '" + Fecha + "' and idcliente = " + Cliente;
        String resultado = Globales.ObtenerUnValor(sql);
        if (resultado.equals("")) {
            resultado = "XX|";
        }
        sql = "select devolucion, devolucion FROM devolucion where entregado = 0 and idusuarioanula = 0 and idcliente= " + Cliente + " order by 1";
        resultado = resultado + "|" + Globales.ObtenerCombo(sql, 0, 0, 0);
        return resultado;

    }

    public String GestionCartera(HttpServletRequest request) {
        int Cliente = Globales.Validarintnonull(request.getParameter("Cliente"));
        String Fecha = request.getParameter("Cliente");
        String Observacion = request.getParameter("Cliente");
        String Devolucion = request.getParameter("Cliente");

        sql = "SELECT id FROM seguimiento_cartera where to_char(fecha,'yyyy-MM-dd') = '" + Fecha + "' and idcliente = " + Cliente;
        int id = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));

        if (id == 0) {
            if (Globales.PermisosSistemas("CARTERA GESTION GUARDAR", idusuario) == 0) {
                return "1|Usted no posee permisos para esta operación.... Consulte con el administ "
                        + "VALUES (" + Cliente + "," + idusuario + ",'" + Observacion + "','" + Devolucion + "')";
            }
            Globales.DatosAuditoria(sql, "CARTERA GESTION", "GUARDAR", idusuario, iplocal, this.getClass() + "-->GuardarNotaCredito");
            return "0|Gestión de cartera guardada con éxito";
        } else {
            if (Globales.PermisosSistemas("CARTERA GESTION EDITAR", idusuario) == 0) {
                return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
            }
            sql = "UPDATE seguimiento_cartera SET fecha = now(), seguimiento = '" + Observacion + "', devolucion='" + Devolucion + "' WHERE id = " + id;
            Globales.DatosAuditoria(sql, "CARTERA GESTION", "EDITAR", idusuario, iplocal, this.getClass() + "-->GuardarNotaCredito");
            return "0|Gestión de cartera actualizada con éxito";
        }

    }

    public String ListadoEquipos(HttpServletRequest request) {
        int idcliente = Globales.Validarintnonull(request.getParameter("idcliente"));

        sql = "select '<a title=''Imprimir Remisión'' type=''button'' href=' || chr(34) || 'javascript:ImprimirRemision(' || r.remision || ',''' || r.estado || ''',0)' ||chr(34) || '>' || r.remision || '</a><br>' ||\n"
                + "u.nombrecompleto || '<br><b>' || to_char(fechaing, 'yyyy/MM/dd HH24:MI') || '</b>' || case when r.pdf > 0 then '<br><a class=''text-success text-XX'' title=''Ver Remisión del Cliente'' href=''javascript:VerRemision(' || r.remision || ')''>PDF</a>' ELSE '' END as remision, '<b>' || rd.ingreso || '</b>' as ingreso, \n"
                + "case when rd.entregado = 0 then '<b><font color=''red''>NO</font></b><br>' else ' ' end || tiempo(rd.fechaing, case when rd.entregado = 0  then now() else (select max(fechaentrega) FROM devolucion_detalle dd inner join devolucion d on d.id = dd.iddevolucion where dd.ingreso = rd.ingreso) end, 3) as tiempo, \n"
                + "'<b>' || m.descripcion || '</b><br>' || e.descripcion || '<br><b>' || ma.descripcion || '</b><br>' || mo.descripcion || '<b>' || case when rd.idintervalo = 0 then 'VER ESPECIFICACIONES' ELSE '(' || desde || ' a ' || hasta || ') ' || medida end || '</b><br>' || rd.serie as equipo,\n"
                + "case when r.importado = 1 and ncotizacion = 0 then coalesce((SELECT max(ip.descripcion) FROM importado_cotizacion ip where ip.ingreso = rd.ingreso),'NO') else case when rd.cotizado = 1 then '<a href=''javascript:VerDetalle(1,' || rd.ingreso || ')'' title=''Ver Cotizaciones''>' || rd.ncotizacion || '<br>Ver Detalle</a>' else  tiempo(rd.fechaing, now(),2) END END as cotizado,\n"
                + "case when rd.certificado = 1 then '<a href=''javascript:VerDetalle(6,' || rd.ingreso || ')'' title=''Ver Certificado''>' || (select min(numero) from certificados ce where ce.ingreso = rd.ingreso) || '<br>Ver Detalle</a>' else case when calibracion = 2 then 'NO Apto <br> Para Calibración' ELSE CASE WHEN rd.reportado = 0 then 'NO REPORTADO' ELSE tiempo((select min(fecha) from reporte_ingreso ri where ri.ingreso = rd.ingreso), now(),2) END END END  as certificado,\n"
                + "case when rd.orden = 1 then '<a href=''javascript:VerDetalle(8,' || rd.ingreso || ')'' title=''Ver Orden de Compra''>' || (select orco.numero || '<br>' || to_char(fechaorden,'yyyy/MM/dd HH24:MI') from orden_compra orco WHERE orco.ingreso = rd.ingreso order by orco.id desc limit 1) ||'<br>Ver Detalle</a>' else case when cotizado = 1 then tiempo((select MIN(fechareg) from cotizacion_detalle cd where cd.ingreso = rd.ingreso), now(),2) ELSE 'NO <br>COTIZADO' END END  as orden,\n"
                + "case when rd.facturado> 0 then '<a href=''javascript:VerDetalle(9,' || rd.ingreso || ')'' title=''Ver Factura''>' || rd.facturado || '<br>Ver Detalle</a>' else 'NO' END  as facturado,\n"
                + "case when rd.salida = 0 then 'NO' else case when salida > 1 THEN (SELECT max(id.descripcion) FROM importado_devolucion id where id.ingreso = rd.ingreso) else '<a href=''javascript:VerDetalle(10,' || rd.ingreso || ')'' title=''Ver Devolución''>' || (select max(devolucion) from devolucion de inner join devolucion_detalle dd on de.id = dd.iddevolucion and dd.ingreso = rd.ingreso) || '<br>Ver Detalle</a>' END END  as devolucion,\n"
                + "case when rd.fotos > 0 then '<img src=''imagenes/ingresos/' || rd.ingreso || '/1.jpg'' onclick=''LlamarFotoDet(' || rd.ingreso || ',' || rd.fotos || ')'' width=''80px''/><br>' else '' end || '<b>' || case when salida = 0 then coalesce((SELECT y || x FROM ingreso_ubicacion u WHERE u.ingreso = rd.ingreso),'') else '' end || '</b>' as imagen,\n"
                + "case when rd.informetecnico > 0 then '<a href=''javascript:VerDetalle(12,' || rd.ingreso || ')'' title=''Ver Reporte''>' || rd.informetecnico || '<br>Ver Detalle</a>' else CASE WHEN rd.recibidolab = 0 THEN 'NO<BR>RECIBIDO' ELSE case when rd.reportado = 1 and rd.calibracion = 0 then  tiempo((select fecha from ingreso_recibidolab ir where ir.ingreso = rd.ingreso), now(),2) ELSE 'NO <BR> REQUERIDO' END  END END  as informe\n"
                + "from remision r inner join seguridad.rbac_usuario u on r.idusuario = u.idusu\n"
                + "inner join clientes c on c.id = r.idcliente\n"
                + "inner join remision_detalle rd on rd.idremision = r.id \n"
                + "inner join equipo e on e.id = rd.idequipo\n"
                + "inner join modelos mo on mo.id = rd.idmodelo \n"
                + "inner join marcas ma on ma.id = mo.idmarca\n"
                + "inner join magnitudes m on m.id = e.idmagnitud\n"
                + "inner join seguridad.rbac_usuario ua on c.asesor = ua.idusu\n"
                + "inner join magnitud_intervalos i on i.id = rd.idintervalo where rd.facturado > 0 and rd.salida <> 2 and rd.entregado = 0 and c.id = \" + idcliente + @\" \n"
                + "ORDER BY rd.ingreso";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ListadoEquipos");
    }

    public String ListadoGestionCartera(HttpServletRequest request) {

        int Cliente = Globales.Validarintnonull(request.getParameter("Cliente"));

        sql = "SELECT to_char(fecha,'yyyy/MM/dd HH24:MI') as fecha, seguimiento, replace(devolucion,'$',',') as devolucion, nombrecompleto as usuario  "
                + "FROM seguimiento_cartera c inner join seguridad.rbac_usuario u on u.idusu = c.idusuario  "
                + "where idcliente = " + Cliente + " ORDER BY 1";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ListadoGestionCartera");
    }

    public String TablaGestionCartera(HttpServletRequest request) {
        int Cliente = Globales.Validarintnonull(request.getParameter("Cliente"));
        int Tipo = Globales.Validarintnonull(request.getParameter("Tipo"));
        int Dias = Globales.Validarintnonull(request.getParameter("Dias"));
        int Estado = Globales.Validarintnonull(request.getParameter("Estado"));
        int Asesor = Globales.Validarintnonull(request.getParameter("Asesor"));
        String Fecha = request.getParameter("Fecha");
        int Orden = Globales.Validarintnonull(request.getParameter("Orden"));
        int TipoOrden = Globales.Validarintnonull(request.getParameter("TipoOrden"));
        int Gestionado = Globales.Validarintnonull(request.getParameter("Gestionado"));

        String Busqueda = "where saldo > 1 and f.estado <> 'Anulado'";

        if (Cliente > 0) {
            Busqueda += " and c.id =" + Cliente;
        }
        if (Asesor > 0) {
            Busqueda += " and c.id =" + Asesor;
        }
        if (Tipo > 0) {
            Busqueda += " and c.idtipcli =" + Tipo;
        }
        if (Estado != 0) {
            switch (Estado) {
                case 1:
                    Busqueda += " and extract(days from (now()-vencimiento)) > 0 ";
                    break;
                case 2:
                    Busqueda += " and extract(days from (now()-vencimiento)) > diasprebloqueo ";
                    break;
                case 3:
                    Busqueda += " and extract(days from (now()-vencimiento)) > diasbloqueo ";
                    break;
            }
        }
        if (Gestionado != 0) {
            if (Gestionado == 1) {
                Busqueda += " and coalesce((SELECT seguimiento FROM seguimiento_cartera sc WHERE sc.idcliente  = c.id and to_char(sc.fecha,'yyyy-MM-dd') ='" + Fecha + "'),'') <> '' ";
            } else {
                Busqueda += " and coalesce((SELECT seguimiento FROM seguimiento_cartera sc WHERE sc.idcliente  = c.id and to_char(sc.fecha,'yyyy-MM-dd') ='" + Fecha + "'),'') = '' ";
            }
        }

        sql = "select c.nombrecompleto || ' (' || c.documento || ')' as cliente, u.nomusu as asesor, sum(total - saldo) as abonado, sum(saldo) as saldo, sum(total) as total, \n"
                + "sum(subtotal) as subtotal, sum(f.descuento) as descuento, sum(iva) as iva, tc.descripcion as tipocliente,plazopago,\n"
                + "(select sum(case when extract(days from (now()-fa.vencimiento)) > 0 then fa.saldo else 0 end) from factura fa where fa.idcliente = c.id and fa.estado <> 'Anulado') as vencido,\n"
                + "c.direccion, coalesce(c.telefono,'') || ' ' || coalesce(c.celular,'') as telefono, coalesce(c.emailcartera,'') as email, ci.descripcion as ciudad,  c.id as idcliente, \n"
                + "(SELECT seguimiento || '<br><b>Devolución: </b>' || replace(devolucion,'$',',')  FROM seguimiento_cartera sc WHERE sc.idcliente  = c.id and to_char(sc.fecha,'yyyy-MM-dd') ='\" + Fecha + @\"') as seguimiento,\n"
                + "(select count(distinct ingreso) from remision_detalle rd WHERE rd.idcliente = c.id and rd.entregado = 0 and rd.facturado > 0 and rd.salida <> 2) as equipos\n"
                + "from factura f inner join clientes c on c.id = f.idcliente\n"
                + "inner join seguridad.rbac_usuario u on u.idusu = c.asesor\n"
                + "inner join ciudad ci on ci.id = c.idciudad\n"
                + "inner join tipocliente tc on tc.id = c.idtipcli\n"
                + "inner join departamento d on d.id = ci.iddepartamento " + Busqueda + " \n"
                + "group by c.nombrecompleto, c.documento, u.nomusu, c.direccion, c.telefono, c.celular, c.emailcartera, ci.descripcion, c.id, tc.descripcion, plazopago\n"
                + "order by " + Orden + " " + (TipoOrden == 2 ? "desc" : "");
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->TablaGestionCartera");
    }

    public String ReporteCartera(HttpServletRequest request) {
        try {
            int opcion = Globales.Validarintnonull(request.getParameter("opcion"));
            String fechaf = request.getParameter("fechaf");
            String fechai = request.getParameter("fechai");
            int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
            String factura = request.getParameter("factura");
            int asesor = Globales.Validarintnonull(request.getParameter("asesor"));
            String estado = request.getParameter("estado");
            int departamento = Globales.Validarintnonull(request.getParameter("departamento"));
            int ciudad = Globales.Validarintnonull(request.getParameter("ciudad"));
            int opcionreporte = Globales.Validarintnonull(request.getParameter("opcionreporte"));
            int estadocar = Globales.Validarintnonull(request.getParameter("estadocar"));
            int orden = Globales.Validarintnonull(request.getParameter("orden"));

            if (Globales.PermisosSistemas("CARTERA REPORTE", idusuario) == 0) {
                return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
            }

            String titulo = "";
            String tiporeport = "Semanal";
            String busqueda = "";
            String busqueda2 = "";
            String sfecha = fechaf.replace("-", "");
            String NombreReporte = "";
            String Ordenar = orden == 1 ? " order by 2, 1" : " order by 6";
            busqueda = " WHERE f.estado  != 'Anulado' and saldo <> 0 ";
            busqueda2 = " WHERE a.estado  != 'Anulado' and saldo <> 0 ";
            Date fecha = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");

            String Guia = dateFormat.format(fecha);

            if (cliente > 0) {
                busqueda += " AND c.id = " + cliente;
                busqueda2 += " AND c.id = " + cliente;
            }

            if (asesor > 0) {
                busqueda += " AND u.idusu = " + asesor;
                busqueda2 += " AND u.idusu = " + asesor;
            }

            if (departamento > 0) {
                busqueda += " AND d.id = " + departamento;
                busqueda2 += " AND d.id = " + departamento;
            }

            if (ciudad > 0) {
                busqueda += " AND ci.id = " + ciudad;
                busqueda2 += " AND ci.id = " + ciudad;
            }

            if (!factura.equals("")) {
                busqueda += " AND f.factura in(" + factura + ")";
            }
            if (estadocar == 1) {
                busqueda += " AND extract(days from (now()-vencimiento)) > 0 ";
            } else if (estadocar == 2) {
                busqueda += " AND extract(days from (now()-vencimiento)) <= 0 ";
            }

            if (!fechai.equals("")) {
                busqueda += " and to_char(f.fecha,'yyyy-MM-dd') >= '" + fechai + "'";
                busqueda2 += " and to_char(a.fecha,'yyyy-MM-dd') >= '" + fechai + "'";
            }

            if (!fechaf.equals("")) {
                busqueda += " and to_char(f.fecha,'yyyy-MM-dd') <= '" + fechaf + "'";
                busqueda2 += " and to_char(a.fecha,'yyyy-MM-dd') <= '" + fechaf + "'";
            }

            if (opcionreporte == 1) {
                sql = "select factura, c.nombrecompleto || ' (' || c.documento || ')' as cliente, u.nombrecompleto as asesor, CASE WHEN c.estado = 1  THEN 'ACTIVO' ELSE 'INACTIVO' END as estado, to_char(f.fecha,'yyyy/MM/dd') as fecha,\n"
                        + "to_char(f.vencimiento,'yyyy/MM/dd') as vencimiento, total - saldo as abonado, \n"
                        + "c.direccion, coalesce(c.telefono,'') || ' ' || coalesce(c.celular,'') as telefono, emailcartera as email, f.orden, ci.descripcion as ciudad, d.descripcion as departamento,\n"
                        + "total, saldo, case when extract(days from (now()-vencimiento)) <= 0 then saldo else 0 end as ValorActual,\n"
                        + "case when extract(days from (now()-vencimiento)) BETWEEN  1 and 7 then saldo else 0 end as Opcion1,'1-7 Días' as DesOpc1,\n"
                        + "case when extract(days from (now()-vencimiento)) BETWEEN  8 and 14 then saldo else 0 end as Opcion2,'8-14 Días' as DesOpc2,\n"
                        + "case when extract(days from (now()-vencimiento)) BETWEEN  15 and 21 then saldo else 0 end as Opcion3,'15-21 Días' as DesOpc3,\n"
                        + "case when extract(days from (now()-vencimiento)) BETWEEN  22 and 28 then saldo else 0 end as Opcion4,'22-28 Días' as DesOpc4,\n"
                        + "case when extract(days from (now()-vencimiento)) BETWEEN  29 and 35 then saldo else 0 end as Opcion5,'29-35 Días' as DesOpc5,\n"
                        + "case when extract(days from (now()-vencimiento)) >  35 then saldo else 0 end as Opcion6,'>35 Días' as DesOpc6, extract(days from (now()-vencimiento)) as dias\n"
                        + "from factura f inner join clientes c on c.id = f.idcliente\n"
                        + "inner join seguridad.rbac_usuario u on u.idusu = c.asesor\n"
                        + "inner join ciudad ci on ci.id = c.idciudad\n"
                        + "inner join departamento d on d.id = ci.iddepartamento " + busqueda + "\n"
                        + "union\n"
                        + "select anticipo, c.nombrecompleto || ' (' || c.documento || ')' as cliente, u.nombrecompleto as asesor, CASE WHEN c.estado = 1  THEN 'ACTIVO' ELSE 'INACTIVO' END as estado, to_char(a.fecha,'yyyy/MM/dd') as fecha,\n"
                        + "to_char(a.fecha,'yyyy/MM/dd') as vencimiento, 0 as abonado, \n"
                        + "c.direccion, coalesce(c.telefono,'') || ' ' || coalesce(c.celular,'') as telefono, c.email, ' ' as orden, ci.descripcion as ciudad, d.descripcion as departamento,\n"
                        + "monto*-1 as monto, saldo*-1 as saldo, saldo*-1 as saldo,\n"
                        + "0 as Opcion1,'1-7 Días' as DesOpc1,\n"
                        + "0 as Opcion2,'8-14 Días' as DesOpc2,\n"
                        + "0 as Opcion3,'15-21 Días' as DesOpc3,\n"
                        + "0 as Opcion4,'22-28 Días' as DesOpc4,\n"
                        + "0 as Opcion5,'29-35 Días' as DesOpc5,\n"
                        + "0 as Opcion6,'>35 Días' as DesOpc6, 0 as dias\n"
                        + "from anticipos a inner join clientes c on c.id = a.idcliente\n"
                        + "inner join seguridad.rbac_usuario u on u.idusu = c.asesor\n"
                        + "inner join ciudad ci on ci.id = c.idciudad\n"
                        + "inner join departamento d on d.id = ci.iddepartamento " + busqueda2 + Ordenar;
                NombreReporte = "RpCartera";
                tiporeport = "Semanas";
            }

            if (opcionreporte == 2) {
                sql = "select factura, c.nombrecompleto || ' (' || c.documento || ')' as cliente, u.nombrecompleto as asesor, CASE WHEN c.estado = 1  THEN 'ACTIVO' ELSE 'INACTIVO' END as estado, to_char(f.fecha,'yyyy/MM/dd') as fecha,\n"
                        + "to_char(f.vencimiento,'yyyy/MM/dd') as vencimiento,  total - saldo as abonado, \n"
                        + "c.direccion, coalesce(c.telefono,'') || ' ' || coalesce(c.celular,'') as telefono, emailcartera as email, f.orden, ci.descripcion as ciudad, d.descripcion as departamento,\n"
                        + "total, saldo, case when extract(days from (now()-vencimiento)) <= 0 then saldo else 0 end as ValorActual,\n"
                        + "case when extract(days from (now()-vencimiento)) BETWEEN  1 and 15 then saldo else 0 end as Opcion1,'1-15 Días' as DesOpc1,\n"
                        + "case when extract(days from (now()-vencimiento)) BETWEEN  16 and 30 then saldo else 0 end as Opcion2,'16-30 Días' as DesOpc2,\n"
                        + "case when extract(days from (now()-vencimiento)) BETWEEN  31 and 45 then saldo else 0 end as Opcion3,'31-45 Días' as DesOpc3,\n"
                        + "case when extract(days from (now()-vencimiento)) BETWEEN  46 and 60 then saldo else 0 end as Opcion4,'46-60 Días' as DesOpc4,\n"
                        + "case when extract(days from (now()-vencimiento)) BETWEEN  61 and 75 then saldo else 0 end as Opcion5,'61-75 Días' as DesOpc5,\n"
                        + "case when extract(days from (now()-vencimiento)) >  75 then saldo else 0 end as Opcion6,'>75 Días' as DesOpc6, extract(days from (now()-vencimiento)) as dias\n"
                        + "from factura f inner join clientes c on c.id = f.idcliente\n"
                        + "inner join seguridad.rbac_usuario u on u.idusu = c.asesor\n"
                        + "inner join ciudad ci on ci.id = c.idciudad\n"
                        + "inner join departamento d on d.id = ci.iddepartamento " + busqueda + "\n"
                        + "union\n"
                        + "select anticipo, c.nombrecompleto || ' (' || c.documento || ')' as cliente, u.nombrecompleto as asesor, CASE WHEN c.estado = 1  THEN 'ACTIVO' ELSE 'INACTIVO' END as estado, to_char(a.fecha,'yyyy/MM/dd') as fecha,\n"
                        + "to_char(a.fecha,'yyyy/MM/dd') as vencimiento, 0 as abonado, \n"
                        + "c.direccion, coalesce(c.telefono,'') || ' ' || coalesce(c.celular,'') as telefono, c.email, ' ' as orden, ci.descripcion as ciudad, d.descripcion as departamento,\n"
                        + "monto*-1 as monto, saldo*-1 as saldo, saldo*-1 as saldo,\n"
                        + "0 as Opcion1,'1-15 Días' as DesOpc1,\n"
                        + "0 as Opcion2,'16-30 Días' as DesOpc2,\n"
                        + "0 as Opcion3,'31-45 Días' as DesOpc3,\n"
                        + "0 as Opcion4,'46-60 Días' as DesOpc4,\n"
                        + "0 as Opcion5,'61-75 Días' as DesOpc5,\n"
                        + "0 as Opcion6,'>75 Días' as DesOpc6, 0 as dias\n"
                        + "from anticipos a inner join clientes c on c.id = a.idcliente\n"
                        + "inner join seguridad.rbac_usuario u on u.idusu = c.asesor\n"
                        + "inner join ciudad ci on ci.id = c.idciudad\n"
                        + "inner join departamento d on d.id = ci.iddepartamento " + busqueda2 + Ordenar;;
                NombreReporte = "RpCartera";
                tiporeport = "Quincenas";
            }

            if (opcionreporte == 3) {
                sql = "select factura, c.nombrecompleto || ' (' || c.documento || ')' as cliente, u.nombrecompleto as asesor, CASE WHEN c.estado = 1  THEN 'ACTIVO' ELSE 'INACTIVO' END as estado, to_char(f.fecha,'yyyy/MM/dd') as fecha,\n"
                        + "to_char(f.vencimiento,'yyyy/MM/dd') as vencimiento, total - saldo as abonado, \n"
                        + "c.direccion, coalesce(c.telefono,'') || ' ' || coalesce(c.celular,'') as telefono, emailcartera as email, f.orden, ci.descripcion as ciudad, d.descripcion as departamento,\n"
                        + "total, saldo, case when extract(days from (now()-vencimiento)) <= 0 then saldo else 0 end as ValorActual,\n"
                        + "case when extract(days from (now()-vencimiento)) BETWEEN  1 and 30 then saldo else 0 end as Opcion1,'1-30 Días' as DesOpc1,\n"
                        + "case when extract(days from (now()-vencimiento)) BETWEEN  31 and 60 then saldo else 0 end as Opcion2,'31-60 Días' as DesOpc2,\n"
                        + "case when extract(days from (now()-vencimiento)) BETWEEN  61 and 90 then saldo else 0 end as Opcion3,'61-90 Días' as DesOpc3,\n"
                        + "case when extract(days from (now()-vencimiento)) BETWEEN  91 and 120 then saldo else 0 end as Opcion4,'91-120 Días' as DesOpc4,\n"
                        + "case when extract(days from (now()-vencimiento)) BETWEEN  121 and 150 then saldo else 0 end as Opcion5,'121-150 Días' as DesOpc5,\n"
                        + "case when extract(days from (now()-vencimiento)) >  150 then saldo else 0 end as Opcion6,'>150 Días' as DesOpc6, extract(days from (now()-vencimiento)) as dias\n"
                        + "from factura f inner join clientes c on c.id = f.idcliente\n"
                        + "inner join seguridad.rbac_usuario u on u.idusu = c.asesor\n"
                        + "inner join ciudad ci on ci.id = c.idciudad\n"
                        + "inner join departamento d on d.id = ci.iddepartamento " + busqueda + "\n"
                        + "union\n"
                        + "select anticipo, c.nombrecompleto || ' (' || c.documento || ')' as cliente, u.nombrecompleto as asesor, CASE WHEN c.estado = 1  THEN 'ACTIVO' ELSE 'INACTIVO' END as estado, to_char(a.fecha,'yyyy/MM/dd') as fecha,\n"
                        + "to_char(a.fecha,'yyyy/MM/dd') as vencimiento, 0 as abonado, \n"
                        + "c.direccion, coalesce(c.telefono,'') || ' ' || coalesce(c.celular,'') as telefono, emailcartera as email, ' ' as orden, ci.descripcion as ciudad, d.descripcion as departamento,\n"
                        + "monto*-1 as monto, saldo*-1 as saldo, saldo*-1 as saldo,\n"
                        + "0 as Opcion1,'1-30 Días' as DesOpc1,\n"
                        + "0 as Opcion2,'31-60 Días' as DesOpc2,\n"
                        + "0 as Opcion3,'61-90 Días' as DesOpc3,\n"
                        + "0 as Opcion4,'91-120 Días' as DesOpc4,\n"
                        + "0 as Opcion5,'121-150 Días' as DesOpc5,\n"
                        + "0 as Opcion6,'>150 Días' as DesOpc6, 0 as dias\n"
                        + "from anticipos a inner join clientes c on c.id = a.idcliente\n"
                        + "inner join seguridad.rbac_usuario u on u.idusu = c.asesor\n"
                        + "inner join ciudad ci on ci.id = c.idciudad\n"
                        + "inner join departamento d on d.id = ci.iddepartamento " + busqueda2 + Ordenar;;
                NombreReporte = "RpCartera";
                tiporeport = "Meses";
            }
            if (opcionreporte == 4) {
                sql = "INSERT INTO TmpCarteraResumida (DesAlmacen,Almacen,Distrito,Grupo,CodCli,Cliente,Pagador,Ciudad,Telefono,Factura,FvnFechaFactura,FVnVencimiento\n"
                        + ",Valor,Opcion1,DesOpc1,Opcion2,DesOpc2,Opcion3,DesOpc3,Opcion4,DesOpc4,Opcion5,DesOpc5,Opcion6,DesOpc6,Asesor,Estado,ZonaVentas,ZonaUbicacion\n"
                        + ",FvnDebito,FvnCreFletes,CliMail,CliNit,CliDesDir,CliDesTel,CliDesFax, Guia, Abonado)\n"
                        + "select 'Almacén: ' + Almacen + ' - Distrito: ' + NomDes + ' - Grupo: ' + Nomgru as DesAlmacen, Almacen, NomDes as Distrito, NomGru as Grupo, CodCli, NomRazSoc as Cliente, ISNULL(CliPagador,'')  as Pagador, cu.Descripcion as Ciudad,  c.CliTelPag as Telefono,  FvnNumero as Factura, Novedades.dbo.FormatoCortoFecha(FvnFechaFactura, 0) as FvnFechaFactura, Novedades.dbo.FormatoCortoFecha(FVnVencimiento, 0)  as FVnVencimiento,      \n"
                        + "iif(DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') <= 0, FvnValor,0) Valor,\n"
                        + "iif(DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') > 0 and DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') <=7, FvnValor,0) as Opcion1,'1-7 Días' as DesOpc1,\n"
                        + "iif(DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') > 7 and DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') <=15, FvnValor,0) as Opcion2,'8-15 Días' as DesOpc2, \n"
                        + "iif(DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') > 15 and DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') <=21, FvnValor,0) as Opcion3,'16-21 Días' as DesOpc3, \n"
                        + "iif(DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') > 21 and DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') <=30, FvnValor,0) as Opcion4,'22-30 Días' as DesOpc4, \n"
                        + "iif(DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') > 30 and DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') <=37, FvnValor,0) as Opcion5,'31-37 Días' as DesOpc5, \n"
                        + "iif(DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') > 37, FvnValor,0) as Opcion6,'Más 37 Días' as DesOpc6,\n"
                        + "ISNULL((select NomVenCal from General.dbo.VendedorCalle vc where vc.CliCodVenCal = c.CliCodVenCal),'SIN ASESOR') as Asesor,\n"
                        + "CASE CliEstado WHEN 0 THEN 'Bloqueado' when 1 THEN 'Pre-Bloqueado' when 2 then 'Abierto' end as Estado, ZonaVentas, ZonaUbicacion, FvnDebito, FvnCreFletes, CliMail, CliNit, CliDesDir, CliDesTel, CliDesFax, '\" + Guia + @\"' as Guia,\n"
                        + "ISNULL((select sum(HRcVrAbonado) from Novedades.dbo.historicoRCaja h where h.HRcCantFact = FvnCantFact and HRcNumero = FVnNumero and FvnAlmacen = h.CV),0) +\n"
                        + "ISNULL((select sum(HRcVrAbonado) from Novedades.dbo.HistNotasCredito h where h.HRcCantFact = FvnCantFact and HRcNumero = FVnNumero and FvnAlmacen = h.CV),0) +\n"
                        + "ISNULL((select sum(VrPago) from Novedades.dbo.SalidasPagos s WHERE s.NroOrden = FVnNumero and s.CV = FvnAlmacen),0) as Abonado\n"
                        + "FROM FacVenCon  f inner join General.Clientes c ON c.CodCli = f.FVnCodCliente \n"
                        + "LEFT JOIN General.dbo.Ciudades cu ON cu.idCiudad = c.CliCodDesCiu\n"
                        + "LEFT JOIN General.dbo.Almacen a ON a.CliCodAlm = f.FvnAlmacen\n"
                        + "LEFT JOIN General.dbo.Grupos g ON g.CliCodGru = c.CliCodGru\n"
                        + "LEFT JOIN General.dbo.Distritos d on d.CliCodDesc = c.CliCodDesc\n"
                        + "LEFT JOIN General.dbo.ZnVentas zv on zv.CliZonVen = c.CliZonVen\n"
                        + " LEFT JOIN General.dbo.ZnUbicacion zu on zu.CliZonUbi = c.CliZonUbi\n"
                        + busqueda + " AND FvnValor > 0 and fvnEstado <> 'A'  \n"
                        + "order by Almacen, nomdes, NomGru, NomRazSoc;";
                NombreReporte = "RpCarteraResumen";
                tiporeport = "Semanas";

                sql += " select  DesAlmacen,Almacen,Distrito,Grupo,CodCli,Cliente,Pagador,Ciudad,Telefono,min(Factura) as Factura,min(FvnFechaFactura) as FvnFechaFactura, min(FVnVencimiento) as FVnVencimiento,\n"
                        + "sum(ISNULL(Valor,0)) as ValorActual, sum(Opcion1) as Opcion1,DesOpc1,sum(Opcion2) as Opcion2,DesOpc2,sum(Opcion3) as Opcion3,DesOpc3,sum(Opcion4) as Opcion4,DesOpc4,sum(Opcion5) as Opcion5,DesOpc5,sum(Opcion6) as Opcion6,DesOpc6,Estado,ZonaVentas,ZonaUbicacion, Asesor\n"
                        + ",sum(FvnDebito) as FvnDebito,sum(FvnCreFletes) as FvnCreFletes,CliMail,CliNit,CliDesDir,CliDesTel,CliDesFax, sum(Abonado) as Abonado\n"
                        + "from TmpCarteraResumida\n"
                        + "where Guia = '" + Guia + "'  \n"
                        + "group by DesAlmacen, Almacen, Distrito, Grupo, CodCli, Cliente, Pagador, Ciudad, Telefono,\n"
                        + "DesOpc1, DesOpc2, DesOpc3, DesOpc4, DesOpc5, DesOpc6, Estado, ZonaVentas, ZonaUbicacion, CliMail, CliNit, CliDesDir, CliDesTel, CliDesFax, Asesor ";
            }
            if (opcionreporte == 5) {
                sql = "INSERT INTO TmpCarteraResumida (DesAlmacen,Almacen,Distrito,Grupo,CodCli,Cliente,Pagador,Ciudad,Telefono,Factura,FvnFechaFactura,FVnVencimiento\n"
                        + ",Valor,Opcion1,DesOpc1,Opcion2,DesOpc2,Opcion3,DesOpc3,Opcion4,DesOpc4,Opcion5,DesOpc5,Opcion6,DesOpc6,Asesor,Estado,ZonaVentas,ZonaUbicacion\n"
                        + ",FvnDebito,FvnCreFletes,CliMail,CliNit,CliDesDir,CliDesTel,CliDesFax, Guia, Abonado)     \n"
                        + "select 'Almacén: ' + Almacen + ' - Distrito: ' + NomDes + ' - Grupo: ' + Nomgru as DesAlmacen, Almacen, NomDes as Distrito, NomGru as Grupo, CodCli, NomRazSoc as Cliente, ISNULL(CliPagador,'')  as Pagador, cu.Descripcion as Ciudad,  c.CliTelPag as Telefono,  FvnNumero as Factura, Novedades.dbo.FormatoCortoFecha(FvnFechaFactura, 0) as FvnFechaFactura, Novedades.dbo.FormatoCortoFecha(FVnVencimiento, 0)  as FVnVencimiento,      \n"
                        + "iif(DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') <= 0, FvnValor,0) as ValorActual,\n"
                        + "iif(DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') > 0 and DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') <=15, FvnValor,0) as Opcion1,'1-15 Días' as DesOpc1,\n"
                        + "iif(DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') > 15 and DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') <=30, FvnValor,0) as Opcion2,'16-30 Días' as DesOpc2, \n"
                        + "iif(DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') > 30 and DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') <=45, FvnValor,0) as Opcion3,'31-45 Días' as DesOpc3, \n"
                        + "iif(DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') > 45 and DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') <=60, FvnValor,0) as Opcion4,'46-60 Días' as DesOpc4, \n"
                        + "iif(DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') > 60 and DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') <=75, FvnValor,0) as Opcion5,'61-75 Días' as DesOpc5, \n"
                        + "iif(DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') > 75, FvnValor,0)) as Opcion6,'Más 75 Días' as DesOpc6,\n"
                        + "ISNULL((select NomVenCal from General.dbo.VendedorCalle vc where vc.CliCodVenCal = c.CliCodVenCal),'SIN ASESOR') as Asesor,\n"
                        + "CASE CliEstado WHEN 0 THEN 'Bloqueado' when 1 THEN 'Pre-Bloqueado' when 2 then 'Abierto' end as Estado, ZonaVentas, ZonaUbicacion, FvnDebito, FvnCreFletes, CliMail, CliNit, CliDesDir, CliDesTel, CliDesFax, '\" + Guia + @\"', \n"
                        + "ISNULL((select sum(HRcVrAbonado) from Novedades.dbo.historicoRCaja h where h.HRcCantFact = FvnCantFact and HRcNumero = FVnNumero and FvnAlmacen = h.CV),0) +\n"
                        + "ISNULL((select sum(HRcVrAbonado) from Novedades.dbo.HistNotasCredito h where h.HRcCantFact = FvnCantFact and HRcNumero = FVnNumero and FvnAlmacen = h.CV),0) +\n"
                        + "ISNULL((select sum(VrPago) from Novedades.dbo.SalidasPagos s WHERE s.NroOrden = FVnNumero and s.CV = FvnAlmacen),0) as Abonado\n"
                        + "FROM FacVenCon  f inner join General.Clientes c ON c.CodCli = f.FVnCodCliente \n"
                        + "LEFT JOIN General.dbo.Ciudades cu ON cu.idCiudad = c.CliCodDesCiu\n"
                        + "LEFT JOIN General.dbo.Almacen a ON a.CliCodAlm = f.FvnAlmacen\n"
                        + "LEFT JOIN General.dbo.Grupos g ON g.CliCodGru = c.CliCodGru\n"
                        + "LEFT JOIN General.dbo.Distritos d on d.CliCodDesc = c.CliCodDesc\n"
                        + "LEFT JOIN General.dbo.ZnVentas zv on zv.CliZonVen = c.CliZonVen\n"
                        + "LEFT JOIN General.dbo.ZnUbicacion zu on zu.CliZonUbi = c.CliZonUbi\n"
                        + "WHERE FvnValor > 0 and fvnEstado <> 'A' " + busqueda + " \n"
                        + "order by Almacen, nomdes, NomGru, NomRazSoc;";
                NombreReporte = "RpCarteraResumen";
                tiporeport = "Quincenas";
                sql += " select  DesAlmacen,Almacen,Distrito,Grupo,CodCli,Cliente,Pagador,Ciudad,Telefono,min(Factura) as Factura,min(FvnFechaFactura) as FvnFechaFactura, min(FVnVencimiento) as FVnVencimiento,\n"
                        + "sum(ISNULL(Valor,0)) as Valor,sum(Opcion1) as Opcion1,DesOpc1,sum(Opcion2) as Opcion2,DesOpc2,sum(Opcion3) as Opcion3,DesOpc3,sum(Opcion4) as Opcion4,DesOpc4,sum(Opcion5) as Opcion5,DesOpc5,sum(Opcion6) as Opcion6,DesOpc6,Estado,ZonaVentas,ZonaUbicacion, Asesor\n"
                        + ",sum(FvnDebito) as FvnDebito,sum(FvnCreFletes) as FvnCreFletes,CliMail,CliNit,CliDesDir,CliDesTel,CliDesFax, sum(Abonado) as Abonado\n"
                        + "from TmpCarteraResumida\n"
                        + "where Guia = '" + Guia + "'  \n"
                        + "group by DesAlmacen, Almacen, Distrito, Grupo, CodCli, Cliente, Pagador, Ciudad, Telefono, Asesor,\n"
                        + "DesOpc1, DesOpc2, DesOpc3, DesOpc4, DesOpc5, DesOpc6, Estado, ZonaVentas, ZonaUbicacion, CliMail, CliNit, CliDesDir, CliDesTel, CliDesFax ";
            }
            if (opcionreporte == 6) {
                sql = "INSERT INTO TmpCarteraResumida (DesAlmacen,Almacen,Distrito,Grupo,CodCli,Cliente,Pagador,Ciudad,Telefono,Factura,FvnFechaFactura,FVnVencimiento\n"
                        + ",Valor,Opcion1,DesOpc1,Opcion2,DesOpc2,Opcion3,DesOpc3,Opcion4,DesOpc4,Opcion5,DesOpc5,Opcion6,DesOpc6,Asesor,Estado,ZonaVentas,ZonaUbicacion\n"
                        + ",FvnDebito,FvnCreFletes,CliMail,CliNit,CliDesDir,CliDesTel,CliDesFax, Guia, Abonado)     \n"
                        + "select 'Almacén: ' + Almacen + ' - Distrito: ' + NomDes + ' - Grupo: ' + Nomgru as DesAlmacen, Almacen, NomDes as Distrito, NomGru as Grupo, CodCli, NomRazSoc as Cliente, ISNULL(CliPagador,'')  as Pagador, cu.Descripcion as Ciudad,  c.CliTelPag as Telefono,  FvnNumero as Factura, Novedades.dbo.FormatoCortoFecha(FvnFechaFactura, 0) as FvnFechaFactura, Novedades.dbo.FormatoCortoFecha(FVnVencimiento, 0)  as FVnVencimiento,      \n"
                        + "iif(DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') <= 0, FvnValor,0) as Valor,\n"
                        + "iif(DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') > 0 and DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') <=30, FvnValor,0) as Opcion1,'1-30 Días' as DesOpc1,\n"
                        + "iif(DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') > 30 and DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') <=60, FvnValor,0) as Opcion2,'31-60 Días' as DesOpc2, \n"
                        + "iif(DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') > 60 and DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') <=90, FvnValor,0) as Opcion3,'61-90 Días' as DesOpc3, \n"
                        + "iif(DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') > 90 and DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') <=120, FvnValor,0) as Opcion4,'91-120 Días' as DesOpc4, \n"
                        + "iif(DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') > 120 and DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') <=150, FvnValor,0) as Opcion5,'121-150 Días' as DesOpc5, \n"
                        + "iif(DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') > 150, FvnValor,0) as Opcion6,'Más 151 Días' as DesOpc6,\n"
                        + "ISNULL((select NomVenCal from General.dbo.VendedorCalle vc where vc.CliCodVenCal = c.CliCodVenCal),'SIN ASESOR') as Asesor,\n"
                        + "CASE CliEstado WHEN 0 THEN 'Bloqueado' when 1 THEN 'Pre-Bloqueado' when 2 then 'Abierto' end as Estado, ZonaVentas, ZonaUbicacion, FvnDebito, FvnCreFletes, CliMail, CliNit, CliDesDir, CliDesTel, CliDesFax, '\" + Guia + @\"',\n"
                        + "ISNULL((select sum(HRcVrAbonado) from Novedades.dbo.historicoRCaja h where h.HRcCantFact = FvnCantFact and HRcNumero = FVnNumero and FvnAlmacen = h.CV),0) +\n"
                        + "ISNULL((select sum(HRcVrAbonado) from Novedades.dbo.HistNotasCredito h where h.HRcCantFact = FvnCantFact and HRcNumero = FVnNumero and FvnAlmacen = h.CV),0) +\n"
                        + "ISNULL((select sum(VrPago) from Novedades.dbo.SalidasPagos s WHERE s.NroOrden = FVnNumero and s.CV = FvnAlmacen),0) as Abonado\n"
                        + "FROM FacVenCon  f inner join General.Clientes c ON c.CodCli = f.FVnCodCliente \n"
                        + "LEFT JOIN General.dbo.Ciudades cu ON cu.idCiudad = c.CliCodDesCiu\n"
                        + "LEFT JOIN General.dbo.Almacen a ON a.CliCodAlm = f.FvnAlmacen\n"
                        + "LEFT JOIN General.dbo.Grupos g ON g.CliCodGru = c.CliCodGru\n"
                        + "LEFT JOIN General.dbo.Distritos d on d.CliCodDesc = c.CliCodDesc\n"
                        + "LEFT JOIN General.dbo.ZnVentas zv on zv.CliZonVen = c.CliZonVen\n"
                        + "LEFT JOIN General.dbo.ZnUbicacion zu on zu.CliZonUbi = c.CliZonUbi\n"
                        + "WHERE FvnValor > 0 and fvnEstado <> 'A' " + busqueda + " \n"
                        + "order by Almacen, nomdes, NomGru, NomRazSoc;";
                NombreReporte = "RpCarteraResumen";
                tiporeport = "Meses";
                sql += "select  DesAlmacen,Almacen,Distrito,Grupo,CodCli,Cliente,Pagador,Ciudad,Telefono,min(Factura) as Factura,min(FvnFechaFactura) as FvnFechaFactura, min(FVnVencimiento) as FVnVencimiento,\n"
                        + "sum(ISNULL(Valor,0)) as ValorActual, sum(Opcion1) as Opcion1,DesOpc1,sum(Opcion2) as Opcion2,DesOpc2,sum(Opcion3) as Opcion3,DesOpc3,sum(Opcion4) as Opcion4,DesOpc4,sum(Opcion5) as Opcion5,DesOpc5,sum(Opcion6) as Opcion6,DesOpc6,Estado,ZonaVentas,ZonaUbicacion, Asesor\n"
                        + ",sum(FvnDebito) as FvnDebito,sum(FvnCreFletes) as FvnCreFletes,CliMail,CliNit,CliDesDir,CliDesTel,CliDesFax, Sum(Abonado) as Abonado\n"
                        + "from TmpCarteraResumida\n"
                        + "where Guia = '" + Guia + "'  \n"
                        + "group by DesAlmacen, Almacen, Distrito, Grupo, CodCli, Cliente, Pagador, Ciudad, Telefono,\n"
                        + "DesOpc1, DesOpc2, DesOpc3, DesOpc4, DesOpc5, DesOpc6, Estado, ZonaVentas, ZonaUbicacion, CliMail, CliNit, CliDesDir, CliDesTel, CliDesFax, Asesor  ";
            }

            if (opcionreporte == 7) {
                sql = "select 'Almacén: ' + Almacen + ' - Distrito: ' + NomDes + ' - Grupo: ' + Nomgru as DesAlmacen, Almacen, NomDes as Distrito, NomGru as Grupo, CodCli, NomRazSoc as Cliente, ISNULL(CliPagador,'')  as Pagador, cu.Descripcion as Ciudad,  c.CliTelPag as Telefono,  FvnNumero as Factura, Novedades.dbo.FormatoCortoFecha(FvnFechaFactura, 0) as FvnFechaFactura, Novedades.dbo.FormatoCortoFecha(FVnVencimiento, 0)  as FVnVencimiento,  \n"
                        + "DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') as Dias, FvnValor,\n"
                        + "isnull((SELECT Descripcion FROM General..Usuarios u, Pediresumen p WHERE p.Numero = f.FvnNoPedido and p.CV = f.FvnAlmacen and p.usuario = u.codigo),'SIN ASESOR') as Asesor,\n"
                        + "CASE CliEstado WHEN 0 THEN 'Bloqueado' when 1 THEN 'Pre-Bloqueado' when 2 then 'Abierto' end as Estado, ZonaVentas, ZonaUbicacion, FvnDebito, FvnCreFletes, CliMail, CliNit, CliDesDir, CliDesTel, CliDesFax\n"
                        + "FROM FacVenCon  f inner join General.Clientes c ON c.CodCli = f.FVnCodCliente \n"
                        + "LEFT JOIN General.dbo.Ciudades cu ON cu.idCiudad = c.CliCodDesCiu\n"
                        + "LEFT JOIN General.dbo.Almacen a ON a.CliCodAlm = f.FvnAlmacen\n"
                        + "LEFT JOIN General.dbo.Grupos g ON g.CliCodGru = c.CliCodGru\n"
                        + "LEFT JOIN General.dbo.Distritos d on d.CliCodDesc = c.CliCodDesc\n"
                        + "LEFT JOIN General.dbo.ZnVentas zv on zv.CliZonVen = c.CliZonVen\n"
                        + "LEFT JOIN General.dbo.ZnUbicacion zu on zu.CliZonUbi = c.CliZonUbi\n"
                        + "where FvnValor > 0 and fvnEstado <> 'A' and DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') > 0 " + busqueda + " \n"
                        + "order by Almacen, nomdes, NomGru, NomRazSoc, FvnNumero";
                NombreReporte = "RpCarteraVencidas";
                tiporeport = "";
            }

            if (opcionreporte == 8) {
                sql = "select 'Almacén: ' + Almacen + ' - Distrito: ' + NomDes + ' - Grupo: ' + Nomgru as DesAlmacen, Almacen, NomDes as Distrito, NomGru as Grupo, CodCli, NomRazSoc as Cliente, ISNULL(CliPagador,'')  as Pagador, cu.Descripcion as Ciudad,  c.CliTelPag as Telefono,  FvnNumero as Factura, Novedades.dbo.FormatoCortoFecha(FvnFechaFactura, 0) as FvnFechaFactura, Novedades.dbo.FormatoCortoFecha(FVnVencimiento, 0)  as FVnVencimiento,  \n"
                        + "iif(DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') <= 0, FvnValor,0) as Valor,\n"
                        + "iif(DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') > 0 and DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') <=30, FvnValor,0) as Opcion1,'1-30 Días' as DesOpc1,\n"
                        + "iif(DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') > 31 and DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') <=60, FvnValor,0) as Opcion2,'31-60 Días' as DesOpc2, \n"
                        + "iif(DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') > 61 and DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') <=90, FvnValor,0) as Opcion3,'61-90 Días' as DesOpc3, \n"
                        + "iif(DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') > 91 and DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') <=120, FvnValor,0) as Opcion4,'91-120 Días' as DesOpc4, \n"
                        + "iif(DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') > 121 and DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') <=150, FvnValor,0) as Opcion5,'121-150 Días' as DesOpc5, \n"
                        + "iif(DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') > 150, FvnValor,0) as Opcion6,'Más 150 Días' as DesOpc6,\n"
                        + "ISNULL((select NomVenCal from General.dbo.VendedorCalle vc where vc.CliCodVenCal = c.CliCodVenCal),'SIN ASESOR') as Asesor,\n"
                        + "CASE CliEstado WHEN 0 THEN 'Bloqueado' when 1 THEN 'Pre-Bloqueado' when 2 then 'Abierto' end as Estado\n"
                        + "FROM FacVenCon  f inner join General.Clientes c ON c.CodCli = f.FVnCodCliente \n"
                        + "LEFT JOIN General.dbo.Ciudades cu ON cu.idCiudad = c.CliCodDesCiu\n"
                        + "LEFT JOIN General.dbo.Almacen a ON a.CliCodAlm = f.FvnAlmacen\n"
                        + "LEFT JOIN General.dbo.Grupos g ON g.CliCodGru = c.CliCodGru\n"
                        + "LEFT JOIN General.dbo.Distritos d on d.CliCodDesc = c.CliCodDesc\n"
                        + "LEFT JOIN General.dbo.ZnVentas zv on zv.CliZonVen = c.CliZonVen\n"
                        + "LEFT JOIN General.dbo.ZnUbicacion zu on zu.CliZonUbi = c.CliZonUbi\n"
                        + "where FvnValor > 0 and fvnEstado <> 'A' and DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') > 0 " + busqueda + " \n"
                        + "order by Almacen, nomdes, NomGru, NomRazSoc, FvnNumero";
                NombreReporte = "RpCartera";
                tiporeport = "Meses (Vencidas)";
            }

            datos = Globales.Obtenerdatos(sql, this.getClass() + "-->ReporteCartera", 1);

            if (!datos.next()) {
                return "1|No hay nada que reportar";
            }

            if (opcion == 1) {
                return "0|" + Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ReporteCartera");
            }
            /*
            String DirectorioReportesRelativo = "~/Reportes/";
            String directorio = "~/DocumPDF/";
            String unico = DateTime.Now.ToString("yyyyMMddHHmmss");
            String urlArchivo = String.Format("{0}.{1}", NombreReporte, "rdlc");
            String tipo = "PDF";
            if (opcion == 3)
                tipo = "EXCEL";


            String urlArchivoGuardar;
            urlArchivoGuardar = (opcion == 2) ? String.Format("{0}.{1}", NombreReporte + unico, "pdf") : String.Format("{0}.{1}", NombreReporte + unico, "xls");

            String FullPathReport = String.Format("{0}{1}",
                                    this.HttpContext.Server.MapPath(DirectorioReportesRelativo),
                                        urlArchivo);
            String FullGuardar = String.Format("{0}{1}",
                                    this.HttpContext.Server.MapPath(directorio),
                                        urlArchivoGuardar);
            Microsoft.Reporting.WebForms.ReportViewer Reporte = new ReportViewer();
            Reporte.Reset();
            Reporte.LocalReport.ReportPath = FullPathReport;

            ReportParameter[] parameters = new ReportParameter[2];
            parameters[0] = new ReportParameter("Titulo", titulo);
            parameters[1] = new ReportParameter("tipo", tiporeport);
            Reporte.LocalReport.SetParameters(parameters);

            ReportDataSource DataSource = new ReportDataSource("DataSet1", datos);
            Reporte.LocalReport.DataSources.Add(DataSource);
            Reporte.LocalReport.Refresh();
            byte[] ArchivoPDF = Reporte.LocalReport.Render(tipo);
            FileStream fs = new FileStream(FullGuardar, FileMode.Create);
            fs.Write(ArchivoPDF, 0, ArchivoPDF.Length);
            fs.Close();

            //Globales.Obtenerdatos("DELETE from TmpCarteraResumida where Guia = '" + Guia + @"'", conexion);

            Globales.GuardarAuditoria("REPORTE GENERADO CON EL NOMBRE " + urlArchivoGuardar, "CARTERA", "REPORTE", idusuario, iplocal);

            return "0|" + urlArchivoGuardar;
            
             */
            return "0|" + Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ReporteCartera");
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String ReporteCPagar(HttpServletRequest request) {
        try {

            int opcion = Globales.Validarintnonull(request.getParameter("opcion"));
            String fechaf = request.getParameter("fechaf");
            String fechai = request.getParameter("fechai");
            int proveedor = Globales.Validarintnonull(request.getParameter("proveedor"));
            String factura = request.getParameter("factura");
            int asesor = Globales.Validarintnonull(request.getParameter("asesor"));
            int compra = Globales.Validarintnonull(request.getParameter("compra"));

            String tipocompra = request.getParameter("tipocompra");
            String estado = request.getParameter("estado");
            int departamento = Globales.Validarintnonull(request.getParameter("departamento"));
            int ciudad = Globales.Validarintnonull(request.getParameter("ciudad"));
            int opcionreporte = Globales.Validarintnonull(request.getParameter("opcionreporte"));
            int estadocar = Globales.Validarintnonull(request.getParameter("estadocar"));
            int orden = Globales.Validarintnonull(request.getParameter("orden"));

            if (Globales.PermisosSistemas("CARTERA REPORTE", idusuario) == 0) {
                return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
            }

            String titulo = "";
            String tiporeport = "Semanal";
            String busqueda = "";
            String sfecha = fechaf.replace("-", "");
            String NombreReporte = "";
            String Ordenar = orden == 1 ? " order by 2, 1" : " order by 6";
            busqueda = " WHERE c.estado  != 'Anulado' and c.saldo <> 0 ";

            Date fecha = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");

            String Guia = dateFormat.format(fecha);

            if (proveedor > 0) {
                busqueda += " AND p.id = " + proveedor;
            }

            if (!tipocompra.equals("T")) {
                busqueda += " AND c.tipo = '" + tipocompra + "'";
            }

            if (compra > 0) {
                busqueda += " AND c.compra = " + tipocompra;
            }

            if (departamento > 0) {
                busqueda += " AND d.id = " + departamento;
            }

            if (ciudad > 0) {
                busqueda += " AND ci.id = " + ciudad;
            }

            if (!factura.equals("")) {
                busqueda += " AND c.factura = '" + factura + "'";
            }
            if (estadocar == 1) {
                busqueda += " AND extract(days from (now()-vencimiento)) > 0 ";
            } else if (estadocar == 2) {
                busqueda += " AND extract(days from (now()-vencimiento)) <= 0 ";
            }

            if (!fechai.equals("")) {
                busqueda += " and to_char(c.fecha,'yyyy-MM-dd') >= '" + fechai + "'";
            }

            if (!fechaf.equals("")) {
                busqueda += " and to_char(c.fecha,'yyyy-MM-dd') <= '" + fechaf + "'";
            }

            if (opcionreporte == 1) {
                sql = "select c.compra, c.factura, p.nombrecompleto || ' (' || p.documento || ')' as proveedor, c.tipo,  to_char(c.fecha,'yyyy/MM/dd') as fecha,\n"
                        + "to_char(c.vencimiento,'yyyy/MM/dd') as vencimiento, total - saldo as abonado, \n"
                        + "p.direccion, coalesce(p.telefono,'') || ' ' || coalesce(p.celular,'') as telefono, p.email, c.orden, ci.descripcion as ciudad, d.descripcion as departamento,\n"
                        + "total, saldo, case when extract(days from (now()-vencimiento)) <= 0 then saldo else 0 end as ValorActual,    \n"
                        + "case when extract(days from (now()-vencimiento)) BETWEEN  1 and 7 then saldo else 0 end as Opcion1,'1-7 Días' as DesOpc1,\n"
                        + "case when extract(days from (now()-vencimiento)) BETWEEN  8 and 14 then saldo else 0 end as Opcion2,'8-14 Días' as DesOpc2,\n"
                        + "case when extract(days from (now()-vencimiento)) BETWEEN  15 and 21 then saldo else 0 end as Opcion3,'15-21 Días' as DesOpc3,\n"
                        + "case when extract(days from (now()-vencimiento)) BETWEEN  22 and 28 then saldo else 0 end as Opcion4,'22-28 Días' as DesOpc4,\n"
                        + "case when extract(days from (now()-vencimiento)) BETWEEN  29 and 35 then saldo else 0 end as Opcion5,'29-35 Días' as DesOpc5,\n"
                        + "case when extract(days from (now()-vencimiento)) >  35 then saldo else 0 end as Opcion6,'>35 Días' as DesOpc6, extract(days from (now()-vencimiento)) as dias\n"
                        + "from compra c inner join proveedores p on p.id = c.idproveedor\n"
                        + "inner join ciudad ci on ci.id = p.idciudad\n"
                        + "inner join departamento d on d.id = ci.iddepartamento " + busqueda + Ordenar;
                NombreReporte = "RpCPagar";
                tiporeport = "Semanas";
            }

            if (opcionreporte == 2) {
                sql = "select c.compra, c.factura, p.nombrecompleto || ' (' || p.documento || ')' as proveedor, c.tipo,  to_char(c.fecha,'yyyy/MM/dd') as fecha,\n"
                        + "to_char(c.vencimiento,'yyyy/MM/dd') as vencimiento, total - saldo as abonado, \n"
                        + "p.direccion, coalesce(p.telefono,'') || ' ' || coalesce(p.celular,'') as telefono, p.email, c.orden, ci.descripcion as ciudad, d.descripcion as departamento,\n"
                        + "total, saldo, case when extract(days from (now()-vencimiento)) <= 0 then saldo else 0 end as ValorActual,\n"
                        + "case when extract(days from (now()-vencimiento)) BETWEEN  1 and 15 then saldo else 0 end as Opcion1,'1-15 Días' as DesOpc1,\n"
                        + "case when extract(days from (now()-vencimiento)) BETWEEN  16 and 30 then saldo else 0 end as Opcion2,'16-30 Días' as DesOpc2,\n"
                        + "case when extract(days from (now()-vencimiento)) BETWEEN  31 and 45 then saldo else 0 end as Opcion3,'31-45 Días' as DesOpc3,\n"
                        + "case when extract(days from (now()-vencimiento)) BETWEEN  46 and 60 then saldo else 0 end as Opcion4,'46-60 Días' as DesOpc4,\n"
                        + "case when extract(days from (now()-vencimiento)) BETWEEN  61 and 75 then saldo else 0 end as Opcion5,'61-75 Días' as DesOpc5,\n"
                        + "case when extract(days from (now()-vencimiento)) >  75 then saldo else 0 end as Opcion6,'>75 Días' as DesOpc6, extract(days from (now()-vencimiento)) as dias\n"
                        + "from compra c inner join proveedores p on p.id = c.idproveedor\n"
                        + "inner join ciudad ci on ci.id = p.idciudad\n"
                        + "inner join departamento d on d.id = ci.iddepartamento " + busqueda + Ordenar;
                NombreReporte = "RpCPagar";
                tiporeport = "Quincenas";
            }

            if (opcionreporte == 3) {
                sql = "select c.compra, c.factura, p.nombrecompleto || ' (' || p.documento || ')' as proveedor, c.tipo,  to_char(c.fecha,'yyyy/MM/dd') as fecha,\n"
                        + "to_char(c.vencimiento,'yyyy/MM/dd') as vencimiento, total - saldo as abonado, \n"
                        + "p.direccion, coalesce(p.telefono,'') || ' ' || coalesce(p.celular,'') as telefono, p.email, c.orden, ci.descripcion as ciudad, d.descripcion as departamento,\n"
                        + "total, saldo, case when extract(days from (now()-vencimiento)) <= 0 then saldo else 0 end as ValorActual,\n"
                        + "case when extract(days from (now()-vencimiento)) BETWEEN  1 and 30 then saldo else 0 end as Opcion1,'1-30 Días' as DesOpc1,\n"
                        + "case when extract(days from (now()-vencimiento)) BETWEEN  31 and 60 then saldo else 0 end as Opcion2,'31-60 Días' as DesOpc2,\n"
                        + "case when extract(days from (now()-vencimiento)) BETWEEN  61 and 90 then saldo else 0 end as Opcion3,'61-90 Días' as DesOpc3,\n"
                        + "case when extract(days from (now()-vencimiento)) BETWEEN  91 and 120 then saldo else 0 end as Opcion4,'91-120 Días' as DesOpc4,\n"
                        + "case when extract(days from (now()-vencimiento)) BETWEEN  121 and 150 then saldo else 0 end as Opcion5,'121-150 Días' as DesOpc5,\n"
                        + "case when extract(days from (now()-vencimiento)) >  150 then saldo else 0 end as Opcion6,'>150 Días' as DesOpc6, extract(days from (now()-vencimiento)) as dias\n"
                        + "from compra c inner join proveedores p on p.id = c.idproveedor\n"
                        + "inner join ciudad ci on ci.id = p.idciudad\n"
                        + "inner join departamento d on d.id = ci.iddepartamento " + busqueda + Ordenar;
                NombreReporte = "RpCPagar";
                tiporeport = "Meses";
            }
            if (opcionreporte == 4) {
                sql = "INSERT INTO TmpCarteraResumida (DesAlmacen,Almacen,Distrito,Grupo,CodCli,Cliente,Pagador,Ciudad,Telefono,Factura,FvnFechaFactura,FVnVencimiento\n"
                        + ",Valor,Opcion1,DesOpc1,Opcion2,DesOpc2,Opcion3,DesOpc3,Opcion4,DesOpc4,Opcion5,DesOpc5,Opcion6,DesOpc6,Asesor,Estado,ZonaVentas,ZonaUbicacion\n"
                        + ",FvnDebito,FvnCreFletes,CliMail,CliNit,CliDesDir,CliDesTel,CliDesFax, Guia, Abonado)\n"
                        + "select 'Almacén: ' + Almacen + ' - Distrito: ' + NomDes + ' - Grupo: ' + Nomgru as DesAlmacen, Almacen, NomDes as Distrito, NomGru as Grupo, CodCli, NomRazSoc as Cliente, ISNULL(CliPagador,'')  as Pagador, cu.Descripcion as Ciudad,  c.CliTelPag as Telefono,  FvnNumero as Factura, Novedades.dbo.FormatoCortoFecha(FvnFechaFactura, 0) as FvnFechaFactura, Novedades.dbo.FormatoCortoFecha(FVnVencimiento, 0)  as FVnVencimiento,      \n"
                        + "iif(DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') <= 0, FvnValor,0) Valor,\n"
                        + "iif(DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') > 0 and DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') <=7, FvnValor,0) as Opcion1,'1-7 Días' as DesOpc1,\n"
                        + "iif(DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') > 7 and DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') <=15, FvnValor,0) as Opcion2,'8-15 Días' as DesOpc2, \n"
                        + "iif(DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') > 15 and DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') <=21, FvnValor,0) as Opcion3,'16-21 Días' as DesOpc3, \n"
                        + "iif(DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') > 21 and DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') <=30, FvnValor,0) as Opcion4,'22-30 Días' as DesOpc4, \n"
                        + "iif(DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') > 30 and DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') <=37, FvnValor,0) as Opcion5,'31-37 Días' as DesOpc5, \n"
                        + "iif(DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') > 37, FvnValor,0) as Opcion6,'Más 37 Días' as DesOpc6,\n"
                        + "ISNULL((select NomVenCal from General.dbo.VendedorCalle vc where vc.CliCodVenCal = c.CliCodVenCal),'SIN ASESOR') as Asesor,\n"
                        + "CASE CliEstado WHEN 0 THEN 'Bloqueado' when 1 THEN 'Pre-Bloqueado' when 2 then 'Abierto' end as Estado, ZonaVentas, ZonaUbicacion, FvnDebito, FvnCreFletes, CliMail, CliNit, CliDesDir, CliDesTel, CliDesFax, '\" + Guia + @\"' as Guia,\n"
                        + "ISNULL((select sum(HRcVrAbonado) from Novedades.dbo.historicoRCaja h where h.HRcCantFact = FvnCantFact and HRcNumero = FVnNumero and FvnAlmacen = h.CV),0) +\n"
                        + "ISNULL((select sum(HRcVrAbonado) from Novedades.dbo.HistNotasCredito h where h.HRcCantFact = FvnCantFact and HRcNumero = FVnNumero and FvnAlmacen = h.CV),0) +\n"
                        + "ISNULL((select sum(VrPago) from Novedades.dbo.SalidasPagos s WHERE s.NroOrden = FVnNumero and s.CV = FvnAlmacen),0) as Abonado\n"
                        + "FROM FacVenCon  f inner join General..Clientes c ON c.CodCli = f.FVnCodCliente \n"
                        + "LEFT JOIN General.dbo.Ciudades cu ON cu.idCiudad = c.CliCodDesCiu\n"
                        + "LEFT JOIN General.dbo.Almacen a ON a.CliCodAlm = f.FvnAlmacen\n"
                        + "LEFT JOIN General.dbo.Grupos g ON g.CliCodGru = c.CliCodGru\n"
                        + "LEFT JOIN General.dbo.Distritos d on d.CliCodDesc = c.CliCodDesc\n"
                        + "LEFT JOIN General.dbo.ZnVentas zv on zv.CliZonVen = c.CliZonVen\n"
                        + "LEFT JOIN General.dbo.ZnUbicacion zu on zu.CliZonUbi = c.CliZonUbi\n"
                        + "WHERE FvnValor > 0 and fvnEstado <> 'A' " + busqueda + " \n"
                        + "order by Almacen, nomdes, NomGru, NomRazSoc";
                NombreReporte = "RpCarteraResumen";
                tiporeport = "Semanas";

                sql += " select  DesAlmacen,Almacen,Distrito,Grupo,CodCli,Cliente,Pagador,Ciudad,Telefono,min(Factura) as Factura,min(FvnFechaFactura) as FvnFechaFactura, min(FVnVencimiento) as FVnVencimiento,\n"
                        + "sum(ISNULL(Valor,0)) as ValorActual, sum(Opcion1) as Opcion1,DesOpc1,sum(Opcion2) as Opcion2,DesOpc2,sum(Opcion3) as Opcion3,DesOpc3,sum(Opcion4) as Opcion4,DesOpc4,sum(Opcion5) as Opcion5,DesOpc5,sum(Opcion6) as Opcion6,DesOpc6,Estado,ZonaVentas,ZonaUbicacion, Asesor\n"
                        + ",sum(FvnDebito) as FvnDebito,sum(FvnCreFletes) as FvnCreFletes,CliMail,CliNit,CliDesDir,CliDesTel,CliDesFax, sum(Abonado) as Abonado\n"
                        + "from TmpCarteraResumida\n"
                        + "where Guia = '" + Guia + "'  \n"
                        + "group by DesAlmacen, Almacen, Distrito, Grupo, CodCli, Cliente, Pagador, Ciudad, Telefono,\n"
                        + "DesOpc1, DesOpc2, DesOpc3, DesOpc4, DesOpc5, DesOpc6, Estado, ZonaVentas, ZonaUbicacion, CliMail, CliNit, CliDesDir, CliDesTel, CliDesFax, Asesor ";
            }
            if (opcionreporte == 5) {
                sql = "INSERT INTO TmpCarteraResumida (DesAlmacen,Almacen,Distrito,Grupo,CodCli,Cliente,Pagador,Ciudad,Telefono,Factura,FvnFechaFactura,FVnVencimiento\n"
                        + ",Valor,Opcion1,DesOpc1,Opcion2,DesOpc2,Opcion3,DesOpc3,Opcion4,DesOpc4,Opcion5,DesOpc5,Opcion6,DesOpc6,Asesor,Estado,ZonaVentas,ZonaUbicacion\n"
                        + ",FvnDebito,FvnCreFletes,CliMail,CliNit,CliDesDir,CliDesTel,CliDesFax, Guia, Abonado)     \n"
                        + "select 'Almacén: ' + Almacen + ' - Distrito: ' + NomDes + ' - Grupo: ' + Nomgru as DesAlmacen, Almacen, NomDes as Distrito, NomGru as Grupo, CodCli, NomRazSoc as Cliente, ISNULL(CliPagador,'')  as Pagador, cu.Descripcion as Ciudad,  c.CliTelPag as Telefono,  FvnNumero as Factura, Novedades.dbo.FormatoCortoFecha(FvnFechaFactura, 0) as FvnFechaFactura, Novedades.dbo.FormatoCortoFecha(FVnVencimiento, 0)  as FVnVencimiento,      \n"
                        + "iif(DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') <= 0, FvnValor,0) as ValorActual,\n"
                        + "iif(DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') > 0 and DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') <=15, FvnValor,0) as Opcion1,'1-15 Días' as DesOpc1,\n"
                        + "iif(DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') > 15 and DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') <=30, FvnValor,0) as Opcion2,'16-30 Días' as DesOpc2, \n"
                        + "iif(DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') > 30 and DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') <=45, FvnValor,0) as Opcion3,'31-45 Días' as DesOpc3, \n"
                        + "iif(DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') > 45 and DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') <=60, FvnValor,0) as Opcion4,'46-60 Días' as DesOpc4, \n"
                        + "iif(DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') > 60 and DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') <=75, FvnValor,0) as Opcion5,'61-75 Días' as DesOpc5, \n"
                        + "iif(DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') > 75, FvnValor,0)) as Opcion6,'Más 75 Días' as DesOpc6,\n"
                        + "ISNULL((select NomVenCal from General.dbo.VendedorCalle vc where vc.CliCodVenCal = c.CliCodVenCal),'SIN ASESOR') as Asesor,\n"
                        + "CASE CliEstado WHEN 0 THEN 'Bloqueado' when 1 THEN 'Pre-Bloqueado' when 2 then 'Abierto' end as Estado, ZonaVentas, ZonaUbicacion, FvnDebito, FvnCreFletes, CliMail, CliNit, CliDesDir, CliDesTel, CliDesFax, '\" + Guia + @\"', \n"
                        + "ISNULL((select sum(HRcVrAbonado) from Novedades.dbo.historicoRCaja h where h.HRcCantFact = FvnCantFact and HRcNumero = FVnNumero and FvnAlmacen = h.CV),0) +\n"
                        + "ISNULL((select sum(HRcVrAbonado) from Novedades.dbo.HistNotasCredito h where h.HRcCantFact = FvnCantFact and HRcNumero = FVnNumero and FvnAlmacen = h.CV),0) +\n"
                        + "ISNULL((select sum(VrPago) from Novedades.dbo.SalidasPagos s WHERE s.NroOrden = FVnNumero and s.CV = FvnAlmacen),0) as Abonado\n"
                        + "FROM FacVenCon  f inner join General..Clientes c ON c.CodCli = f.FVnCodCliente \n"
                        + "LEFT JOIN General.dbo.Ciudades cu ON cu.idCiudad = c.CliCodDesCiu\n"
                        + "LEFT JOIN General.dbo.Almacen a ON a.CliCodAlm = f.FvnAlmacen\n"
                        + "LEFT JOIN General.dbo.Grupos g ON g.CliCodGru = c.CliCodGru\n"
                        + "LEFT JOIN General.dbo.Distritos d on d.CliCodDesc = c.CliCodDesc\n"
                        + "LEFT JOIN General.dbo.ZnVentas zv on zv.CliZonVen = c.CliZonVen\n"
                        + "LEFT JOIN General.dbo.ZnUbicacion zu on zu.CliZonUbi = c.CliZonUbi\n"
                        + "WHERE FvnValor > 0 and fvnEstado <> 'A' " + busqueda + " \n"
                        + "order by Almacen, nomdes, NomGru, NomRazSoc";
                NombreReporte = "RpCarteraResumen";
                tiporeport = "Quincenas";
                sql += "select  DesAlmacen,Almacen,Distrito,Grupo,CodCli,Cliente,Pagador,Ciudad,Telefono,min(Factura) as Factura,min(FvnFechaFactura) as FvnFechaFactura, min(FVnVencimiento) as FVnVencimiento,\n"
                        + "sum(ISNULL(Valor,0)) as Valor,sum(Opcion1) as Opcion1,DesOpc1,sum(Opcion2) as Opcion2,DesOpc2,sum(Opcion3) as Opcion3,DesOpc3,sum(Opcion4) as Opcion4,DesOpc4,sum(Opcion5) as Opcion5,DesOpc5,sum(Opcion6) as Opcion6,DesOpc6,Estado,ZonaVentas,ZonaUbicacion, Asesor\n"
                        + ",sum(FvnDebito) as FvnDebito,sum(FvnCreFletes) as FvnCreFletes,CliMail,CliNit,CliDesDir,CliDesTel,CliDesFax, sum(Abonado) as Abonado\n"
                        + "from TmpCarteraResumida\n"
                        + "where Guia = '" + Guia + "'  \n"
                        + "group by DesAlmacen, Almacen, Distrito, Grupo, CodCli, Cliente, Pagador, Ciudad, Telefono, Asesor,\n"
                        + "DesOpc1, DesOpc2, DesOpc3, DesOpc4, DesOpc5, DesOpc6, Estado, ZonaVentas, ZonaUbicacion, CliMail, CliNit, CliDesDir, CliDesTel, CliDesFax ";
            }
            if (opcionreporte == 6) {
                sql = "INSERT INTO TmpCarteraResumida (DesAlmacen,Almacen,Distrito,Grupo,CodCli,Cliente,Pagador,Ciudad,Telefono,Factura,FvnFechaFactura,FVnVencimiento\n"
                        + ",Valor,Opcion1,DesOpc1,Opcion2,DesOpc2,Opcion3,DesOpc3,Opcion4,DesOpc4,Opcion5,DesOpc5,Opcion6,DesOpc6,Asesor,Estado,ZonaVentas,ZonaUbicacion\n"
                        + ",FvnDebito,FvnCreFletes,CliMail,CliNit,CliDesDir,CliDesTel,CliDesFax, Guia, Abonado)     \n"
                        + "select 'Almacén: ' + Almacen + ' - Distrito: ' + NomDes + ' - Grupo: ' + Nomgru as DesAlmacen, Almacen, NomDes as Distrito, NomGru as Grupo, CodCli, NomRazSoc as Cliente, ISNULL(CliPagador,'')  as Pagador, cu.Descripcion as Ciudad,  c.CliTelPag as Telefono,  FvnNumero as Factura, Novedades.dbo.FormatoCortoFecha(FvnFechaFactura, 0) as FvnFechaFactura, Novedades.dbo.FormatoCortoFecha(FVnVencimiento, 0)  as FVnVencimiento,      \n"
                        + "iif(DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') <= 0, FvnValor,0) as Valor,\n"
                        + "iif(DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') > 0 and DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') <=30, FvnValor,0) as Opcion1,'1-30 Días' as DesOpc1,\n"
                        + "iif(DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') > 30 and DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') <=60, FvnValor,0) as Opcion2,'31-60 Días' as DesOpc2, \n"
                        + "iif(DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') > 60 and DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') <=90, FvnValor,0) as Opcion3,'61-90 Días' as DesOpc3, \n"
                        + "iif(DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') > 90 and DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') <=120, FvnValor,0) as Opcion4,'91-120 Días' as DesOpc4, \n"
                        + "iif(DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') > 120 and DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') <=150, FvnValor,0) as Opcion5,'121-150 Días' as DesOpc5, \n"
                        + "iif(DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') > 150, FvnValor,0) as Opcion6,'Más 151 Días' as DesOpc6,\n"
                        + "ISNULL((select NomVenCal from General.dbo.VendedorCalle vc where vc.CliCodVenCal = c.CliCodVenCal),'SIN ASESOR') as Asesor,\n"
                        + "CASE CliEstado WHEN 0 THEN 'Bloqueado' when 1 THEN 'Pre-Bloqueado' when 2 then 'Abierto' end as Estado, ZonaVentas, ZonaUbicacion, FvnDebito, FvnCreFletes, CliMail, CliNit, CliDesDir, CliDesTel, CliDesFax, '\" + Guia + @\"',\n"
                        + "ISNULL((select sum(HRcVrAbonado) from Novedades.dbo.historicoRCaja h where h.HRcCantFact = FvnCantFact and HRcNumero = FVnNumero and FvnAlmacen = h.CV),0) +\n"
                        + "ISNULL((select sum(HRcVrAbonado) from Novedades.dbo.HistNotasCredito h where h.HRcCantFact = FvnCantFact and HRcNumero = FVnNumero and FvnAlmacen = h.CV),0) +\n"
                        + "ISNULL((select sum(VrPago) from Novedades.dbo.SalidasPagos s WHERE s.NroOrden = FVnNumero and s.CV = FvnAlmacen),0) as Abonado\n"
                        + "FROM FacVenCon  f inner join General..Clientes c ON c.CodCli = f.FVnCodCliente \n"
                        + "LEFT JOIN General.dbo.Ciudades cu ON cu.idCiudad = c.CliCodDesCiu\n"
                        + "LEFT JOIN General.dbo.Almacen a ON a.CliCodAlm = f.FvnAlmacen\n"
                        + "LEFT JOIN General.dbo.Grupos g ON g.CliCodGru = c.CliCodGru\n"
                        + "LEFT JOIN General.dbo.Distritos d on d.CliCodDesc = c.CliCodDesc\n"
                        + "LEFT JOIN General.dbo.ZnVentas zv on zv.CliZonVen = c.CliZonVen\n"
                        + "LEFT JOIN General.dbo.ZnUbicacion zu on zu.CliZonUbi = c.CliZonUbi\n"
                        + "WHERE FvnValor > 0 and fvnEstado <> 'A' " + busqueda + " \n"
                        + "order by Almacen, nomdes, NomGru, NomRazSoc";
                NombreReporte = "RpCarteraResumen";
                tiporeport = "Meses";
                sql += "select  DesAlmacen,Almacen,Distrito,Grupo,CodCli,Cliente,Pagador,Ciudad,Telefono,min(Factura) as Factura,min(FvnFechaFactura) as FvnFechaFactura, min(FVnVencimiento) as FVnVencimiento,\n"
                        + "sum(ISNULL(Valor,0)) as ValorActual, sum(Opcion1) as Opcion1,DesOpc1,sum(Opcion2) as Opcion2,DesOpc2,sum(Opcion3) as Opcion3,DesOpc3,sum(Opcion4) as Opcion4,DesOpc4,sum(Opcion5) as Opcion5,DesOpc5,sum(Opcion6) as Opcion6,DesOpc6,Estado,ZonaVentas,ZonaUbicacion, Asesor\n"
                        + ",sum(FvnDebito) as FvnDebito,sum(FvnCreFletes) as FvnCreFletes,CliMail,CliNit,CliDesDir,CliDesTel,CliDesFax, Sum(Abonado) as Abonado\n"
                        + "from TmpCarteraResumida\n"
                        + "where Guia = '" + Guia + "'  \n"
                        + "group by DesAlmacen, Almacen, Distrito, Grupo, CodCli, Cliente, Pagador, Ciudad, Telefono,\n"
                        + "DesOpc1, DesOpc2, DesOpc3, DesOpc4, DesOpc5, DesOpc6, Estado, ZonaVentas, ZonaUbicacion, CliMail, CliNit, CliDesDir, CliDesTel, CliDesFax, Asesor  ";
            }

            if (opcionreporte == 7) {
                sql = "select 'Almacén: ' + Almacen + ' - Distrito: ' + NomDes + ' - Grupo: ' + Nomgru as DesAlmacen, Almacen, NomDes as Distrito, NomGru as Grupo, CodCli, NomRazSoc as Cliente, ISNULL(CliPagador,'')  as Pagador, cu.Descripcion as Ciudad,  c.CliTelPag as Telefono,  FvnNumero as Factura, Novedades.dbo.FormatoCortoFecha(FvnFechaFactura, 0) as FvnFechaFactura, Novedades.dbo.FormatoCortoFecha(FVnVencimiento, 0)  as FVnVencimiento,  \n"
                        + "DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') as Dias, FvnValor,\n"
                        + "isnull((SELECT Descripcion FROM General..Usuarios u, Pediresumen p WHERE p.Numero = f.FvnNoPedido and p.CV = f.FvnAlmacen and p.usuario = u.codigo),'SIN ASESOR') as Asesor,\n"
                        + "CASE CliEstado WHEN 0 THEN 'Bloqueado' when 1 THEN 'Pre-Bloqueado' when 2 then 'Abierto' end as Estado, ZonaVentas, ZonaUbicacion, FvnDebito, FvnCreFletes, CliMail, CliNit, CliDesDir, CliDesTel, CliDesFax\n"
                        + "FROM FacVenCon  f inner join General..Clientes c ON c.CodCli = f.FVnCodCliente \n"
                        + "LEFT JOIN General.dbo.Ciudades cu ON cu.idCiudad = c.CliCodDesCiu\n"
                        + "LEFT JOIN General.dbo.Almacen a ON a.CliCodAlm = f.FvnAlmacen\n"
                        + "LEFT JOIN General.dbo.Grupos g ON g.CliCodGru = c.CliCodGru\n"
                        + "LEFT JOIN General.dbo.Distritos d on d.CliCodDesc = c.CliCodDesc\n"
                        + "LEFT JOIN General.dbo.ZnVentas zv on zv.CliZonVen = c.CliZonVen\n"
                        + "LEFT JOIN General.dbo.ZnUbicacion zu on zu.CliZonUbi = c.CliZonUbi\n"
                        + "where FvnValor > 0 and fvnEstado <> 'A' and DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') > 0 " + busqueda + " \n"
                        + "order by Almacen, nomdes, NomGru, NomRazSoc, FvnNumero";
                NombreReporte = "RpCarteraVencidas";
                tiporeport = "";
            }

            if (opcionreporte == 8) {
                sql = "select 'Almacén: ' + Almacen + ' - Distrito: ' + NomDes + ' - Grupo: ' + Nomgru as DesAlmacen, Almacen, NomDes as Distrito, NomGru as Grupo, CodCli, NomRazSoc as Cliente, ISNULL(CliPagador,'')  as Pagador, cu.Descripcion as Ciudad,  c.CliTelPag as Telefono,  FvnNumero as Factura, Novedades.dbo.FormatoCortoFecha(FvnFechaFactura, 0) as FvnFechaFactura, Novedades.dbo.FormatoCortoFecha(FVnVencimiento, 0)  as FVnVencimiento,  \n"
                        + "iif(DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') <= 0, FvnValor,0) as Valor,\n"
                        + "iif(DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') > 0 and DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') <=30, FvnValor,0) as Opcion1,'1-30 Días' as DesOpc1,\n"
                        + "iif(DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') > 31 and DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') <=60, FvnValor,0) as Opcion2,'31-60 Días' as DesOpc2, \n"
                        + "iif(DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') > 61 and DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') <=90, FvnValor,0) as Opcion3,'61-90 Días' as DesOpc3, \n"
                        + "iif(DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') > 91 and DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') <=120, FvnValor,0) as Opcion4,'91-120 Días' as DesOpc4, \n"
                        + "iif(DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') > 121 and DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') <=150, FvnValor,0) as Opcion5,'121-150 Días' as DesOpc5, \n"
                        + "iif(DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') > 150, FvnValor,0) as Opcion6,'Más 150 Días' as DesOpc6,\n"
                        + "ISNULL((select NomVenCal from General.dbo.VendedorCalle vc where vc.CliCodVenCal = c.CliCodVenCal),'SIN ASESOR') as Asesor,\n"
                        + "CASE CliEstado WHEN 0 THEN 'Bloqueado' when 1 THEN 'Pre-Bloqueado' when 2 then 'Abierto' end as Estado\n"
                        + "FROM FacVenCon  f inner join General..Clientes c ON c.CodCli = f.FVnCodCliente \n"
                        + "LEFT JOIN General.dbo.Ciudades cu ON cu.idCiudad = c.CliCodDesCiu\n"
                        + "LEFT JOIN General.dbo.Almacen a ON a.CliCodAlm = f.FvnAlmacen\n"
                        + "LEFT JOIN General.dbo.Grupos g ON g.CliCodGru = c.CliCodGru\n"
                        + "JOIN General.dbo.Distritos d on d.CliCodDesc = c.CliCodDesc\n"
                        + "LEFT JOIN General.dbo.ZnVentas zv on zv.CliZonVen = c.CliZonVen\n"
                        + "LEFT JOIN General.dbo.ZnUbicacion zu on zu.CliZonUbi = c.CliZonUbi\n"
                        + "where FvnValor > 0 and fvnEstado <> 'A' and DATEDIFF(DAY, FVnVencimiento, '" + sfecha + "') > 0 " + busqueda + " \n"
                        + "order by Almacen, nomdes, NomGru, NomRazSoc, FvnNumero";
                NombreReporte = "RpCartera";
                tiporeport = "Meses (Vencidas)";
            }

            datos = Globales.Obtenerdatos(sql, this.getClass() + "-->ReporteCPagar", 1);

            if (!datos.next()) {
                return "1|No hay nada que reportar";
            }

            if (opcion == 1) {
                return "0|" + Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ReporteCPagar");
            }
            /*

        String DirectorioReportesRelativo = "~/Reportes/";
        String directorio = "~/DocumPDF/";
        String unico = DateTime.Now.ToString("yyyyMMddHHmmss");
        String urlArchivo = String.Format("{0}.{1}", NombreReporte, "rdlc");
        String tipo = "PDF";
        if (opcion == 3) {
            tipo = "EXCEL";
        }

        String urlArchivoGuardar;
        urlArchivoGuardar = (opcion == 2) ? String.Format("{0}.{1}", NombreReporte + unico, "pdf") : String.Format("{0}.{1}", NombreReporte + unico, "xls");

        String FullPathReport = String.Format("{0}{1}",
                this.HttpContext.Server.MapPath(DirectorioReportesRelativo),
                urlArchivo);
        String FullGuardar = String.Format("{0}{1}",
                this.HttpContext.Server.MapPath(directorio),
                urlArchivoGuardar);
        Microsoft.Reporting.WebForms.ReportViewer Reporte = new ReportViewer();
        Reporte.Reset();
        Reporte.LocalReport.ReportPath = FullPathReport;

        ReportParameter[] parameters = new ReportParameter[2];
        parameters[0] = new ReportParameter("Titulo", titulo);
        parameters[1] = new ReportParameter("tipo", tiporeport);
        Reporte.LocalReport.SetParameters(parameters);

        ReportDataSource DataSource = new ReportDataSource("DataSet1", datos);
        Reporte.LocalReport.DataSources.Add(DataSource);
        Reporte.LocalReport.Refresh();
        byte[] ArchivoPDF = Reporte.LocalReport.Render(tipo);
        FileStream fs = new FileStream(FullGuardar, FileMode.Create);
        fs.Write(ArchivoPDF, 0, ArchivoPDF.Length);
        fs.Close();

        //Globales.Obtenerdatos("DELETE from TmpCarteraResumida where Guia = '" + Guia + @"'", conexion);
        Globales.GuardarAuditoria("REPORTE GENERADO CON EL NOMBRE " + urlArchivoGuardar, "CARTERA", "REPORTE", idusuario, iplocal);

        return "0|" + urlArchivoGuardar;
             */
            return "0|" + Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ReporteCPagar");
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String SeguimientoCartera(HttpServletRequest request) {
        int devolucion = Globales.Validarintnonull(request.getParameter("cotizacion"));
        sql = "SELECT s.id, '<b>Gestión de Cartera:</b> ' || seguimiento || ' <b>(' || to_char(fecha,'yyyy/MM/dd HH24:MI') || ')' as seguimiento "
                + " FROM public.seguimiento_cartera s inner join seguridad.rbac_usuario u on s.idusuario = u.idusu "
                + "  where devolucion like '%$" + devolucion + "$%'";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->SeguimientoCartera");
    }
    //FIN DEL CODIGO

    protected void Servidor(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        String ruta_origen = request.getHeader("Origin");

        HttpSession misession = request.getSession();
        idusuario = misession.getAttribute("idusuario").toString();
        iplocal = misession.getAttribute("IPLocal").toString();
        usuario = misession.getAttribute("codusu").toString();
        response.setHeader("Access-Control-Allow-Origin", request.getHeader("Origin"));
        response.setHeader("Access-Control-Allow-Credentials", "true");

        try (PrintWriter out = response.getWriter()) {
            if (!ruta_origen.equals("http://localhost:8090") && !ruta_origen.equals("http://192.168.0.55:82") && !ruta_origen.equals("https://192.168.0.55:90")) {
                out.print("[{\"error\":\"505\",\"mensaje\":\"Usted no tiene permiso para acceder a esta ruta\"}]");
            } else if (misession.getAttribute("codusu") == null) {
                out.println("cerrada");
            } else if (!Globales.IpUsuario(misession.getAttribute("idusuario").toString()).equals(misession.getAttribute("IPLocal").toString())) {
                out.println("cambio-ip");
            } else {
                String opcion = request.getParameter("opcion");
                switch (opcion) {

                    //ANDRES
                    case "SaldoTotal":
                        out.println(SaldoTotal(request));
                        break;
                    case "TablaMovimientos":
                        out.println(TablaMovimientos(request));
                        break;
                    case "AnularRecibo":
                        out.println(AnularRecibo(request));
                        break;
                    case "EliminarCheque":
                        out.println(EliminarCheque(request));
                        break;
                    case "TablaCheque":
                        out.println(TablaCheque(request));
                        break;
                    case "BuscarComprobante":
                        out.println(BuscarComprobante(request));
                        break;
                    case "FacturasPendientesCP":
                        out.println(FacturasPendientesCP(request));
                        break;
                    case "FormaPagoReciboCP":
                        out.println(FormaPagoReciboCP(request));
                        break;
                    case "GuardarComprobante":
                        out.println(GuardarComprobante(request));
                        break;
                    case "ModalComprobantes":
                        out.println(ModalComprobantes(request));
                        break;
                    case "FormaPagoAnticipo":
                        out.println(FormaPagoAnticipo(request));
                        break;
                    case "GuardarAnticipo":
                        out.println(GuardarAnticipo(request));
                        break;
                    case "ModalAnticipos":
                        out.println(ModalAnticipos(request));
                        break;
                    case "BuscarAnticipo":
                        out.println(BuscarAnticipo(request));
                        break;
                    case "ModalAnticiposCaja":
                        out.println(ModalAnticiposCaja(request));
                        break;
                    case "BuscarRecibo":
                        out.println(BuscarRecibo(request));
                        break;
                    case "ModalRecibos":
                        out.println(ModalRecibos(request));
                        break;
                    case "Contabilizar_ReciboCaja":
                        out.println(Contabilizar_ReciboCaja(request));
                        break;
                    case "FacturasNotaCredito":
                        out.println(FacturasNotaCredito(request));
                        break;
                    case "BuscarNotaCredito":
                        out.println(BuscarNotaCredito(request));
                        break;
                    case "AnularNotaCredito":
                        out.println(AnularNotaCredito(request));
                        break;
                    case "ConsultarNotaCre":
                        out.println(ConsultarNotaCre(request));
                        break;
                    case "ModalFacturas":
                        out.println(ModalFacturas(request));
                        break;
                    case "TipoArchivoBanco":
                        out.println(TipoArchivoBanco(request));
                        break;
                    case "TablaMovConsolidado":
                        out.println(TablaMovConsolidado(request));
                        break;
                    case "TablaMovimientosResumen":
                        out.println(TablaMovimientosResumen(request));
                        break;
                    case "DetalleMovimientos":
                        out.println(DetalleMovimientos(request));
                        break;
                    case "EditarMovimientos":
                        out.println(EditarMovimientos(request));
                        break;
                    case "GuardarRecibo":
                        out.println(GuardarRecibo(request));
                        break;
                    case "FacturasPendientes":
                        out.println(FacturasPendientes(request));
                        break;
                    case "GuardarNotaCredito":
                        out.println(GuardarNotaCredito(request));
                        break;
                    case "ActualizarCorCartera":
                        out.println(ActualizarCorCartera(request));
                        break;
                    case "VerGestionCartera":
                        out.println(VerGestionCartera(request));
                        break;
                    case "GestionCartera":
                        out.println(GestionCartera(request));
                        break;
                    case "ListadoEquipos":
                        out.println(ListadoEquipos(request));
                        break;
                    case "TablaGestionCartera":
                        out.println(TablaGestionCartera(request));
                        break;
                    case "AnularAnticipo":
                        out.println(AnularAnticipo(request));
                        break;
                    case "ReporteCartera":
                        out.println(ReporteCartera(request));
                        break;
                    case "ReporteCPagar":
                        out.println(ReporteCPagar(request));
                        break;
                    case "SeguimientoCartera":
                        out.println(SeguimientoCartera(request));
                        break;
                    /*case "ImprimirReporteRecibos":
                        out.println(ImprimirReporteRecibos(request));
                        break;*/

                    //FIN
                    default:
                        throw new AssertionError();
                }
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Servidor(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Servidor(request, response);
    }

}
