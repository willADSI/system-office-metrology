package DB;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author leudi
 */
@WebServlet(urlPatterns = {"/Compras"})
public class Compras extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    String sql;
    String idusuario = "";
    ResultSet datos = null;
    String iplocal = "";

    public String AutoCompletoOServ() {

        sql = "SELECT distinct descripcion "
                + "  FROM public.otro_registro where ingreso = 0 and descripcion not ilike '%CALIBRA%' ORDER BY 1";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->TablaFacturaImportada");
    }

    //Wilmer
    public String ConsultarCompra(HttpServletRequest request) {
        String tipo = (request.getParameter("tipo"));
        int compra = Globales.Validarintnonull(request.getParameter("compra"));
        int orden = Globales.Validarintnonull(request.getParameter("orden"));
        int proveedor = Globales.Validarintnonull(request.getParameter("proveedor"));
        String estado = (request.getParameter("estado"));
        String fechad = (request.getParameter("fechad"));
        String fechah = (request.getParameter("fechah"));
        int usuario = Globales.Validarintnonull(request.getParameter("usuario"));
        String vencida = (request.getParameter("vencida"));

        String busqueda = "WHERE c.id > 0 ";
        if (!tipo.equals("TODOS")) {
            busqueda += "c.tipo='" + tipo + "'";
        }
        if (compra > 0) {
            busqueda += " AND c.compra = " + compra;
        } else {
            if (orden > 0) {
                busqueda += " AND c.orden = " + orden;
            } else {
                busqueda += " AND to_char(c.fecha,'yyyy-MM-dd') >= '" + fechad + "' and to_char(c.fecha,'yyyy-MM-dd') <= '" + fechah + "'";
                if (proveedor > 0) {
                    busqueda += " AND c.idproveedor = " + proveedor;
                }
                if (usuario > 0) {
                    busqueda += " AND c.idusuario = " + usuario;
                }
                if (!estado.equals("Todos")) {
                    busqueda += " AND c.estado = '" + estado + "'";
                }
                if (!vencida.equals("vencida")) {
                    if (vencida.equals("SI")) {
                        busqueda += " AND c.vencimiento >= now() and c.saldo > 1";
                    } else {
                        busqueda += " AND c.vencimiento < now() and c.saldo > 1";
                    }
                }
            }
        }

        sql = "SELECT row_number() OVER(order by c.id) as fila, "
                + "       p.nombrecompleto || ' (' || p.documento || ')' AS proveedor, compra, c.subtotal, c.descuento, c.iva, c.total,"
                + "       c.retefuente, c.reteiva, c.reteica,"
                + "       ci.descripcion  as ciudad,"
                + "       c.orden, p.Telefono || '<br>' || p.direccion as direccion, p.email, "
                + "       to_char(c.fecha,'yyyy/MM/dd') as fecha,"
                + "       tiempo(c.vencimiento, now(), 5) as tiempo,"
                + "       to_char(c.vencimiento,'yyyy/MM/dd') as vencimiento,"
                + "       to_char(c.fechaanula,'yyyy/MM/dd HH24:MI')  || '<br>' || ua.nombrecompleto as anula,"
                + "       to_char(c.fechareg,'yyyy/MM/dd HH24:MI')  || '<br>' || u.nombrecompleto as registro,"
                + "       c.tipo, c.plazopago, c.saldo, c.estado,"
                + "       '<button class=''btn btn-glow btn-success imprimir'' title=''Imprimir Compra'' type=''button'' onclick=' || chr(34) || 'ImprimirCompra(' || c.compra || ',''' || c.tipo || ''')' ||chr(34) || '><span data-icon=''&#xe0f7;''></span></button>' as imprimir"
                + "  FROM compra c inner JOIN proveedores p on p.id = c.idproveedor"
                + " 				         inner JOIN ciudad ci ON ci.id = p.idciudad"
                + "                       inner JOIN seguridad.rbac_usuario u on u.idusu = c.idusuario "
                + "                      inner JOIN seguridad.rbac_usuario ua on ua.idusu = c.idusuarioanula " + busqueda + " ORDER BY c.id";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ConsultarCompra");
    }

    public String EliminarTempCompra(HttpServletRequest request) {
        int id = Globales.Validarintnonull(request.getParameter("id"));
        int ingreso = Globales.Validarintnonull(request.getParameter("ingreso"));

        try {
            sql = "DELETE FROM Compra_temporal WHERE id = " + id + ";";
            sql += "UPDATE remision_detalle SET Comprado = 0 where ingreso = " + ingreso + ";";
            Globales.ObtenerDatosJSon(sql, this.getClass() + "-->EliminarTempCompra");
            return "0|Ingreso eliminado con éxito";
        } catch (Exception ex) {

            return "1|" + ex.getMessage();
        }

    }

    public String BuscarTempCompra(HttpServletRequest request) {

        int id = Globales.Validarintnonull(request.getParameter("id"));

        sql = "select * FROM compra_detalle WHERE id = " + id + ";";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->C BuscarTempCompra");
    }

    public String ModalRecibos(HttpServletRequest request) {
        int Recibo = Globales.Validarintnonull(request.getParameter("Recibo"));
        String Documento = (request.getParameter("Documento"));
        String Tercero = (request.getParameter("Tercero"));
        String busqueda = " and  t.nombre ilike '%" + Tercero + "%' and t.documento ilike '%" + Documento + "%'";
        if (Recibo > 0) {
            busqueda += " and recibo = " + Recibo;
        }
        sql = "SELECT cm.id, to_char(fechareg,'yyyy-MM-dd HH24:MI') as fecha, concepto || '<br>' || observacion as concepto, "
                + "  nombre || ' (' || nit ||  case when documento = 'NIT' THEN '-' || verificador when documento = 'RUT' THEN '-'  || verificador else '' end || ')' as tercero, recibo, valor,"
                + "  recaudado, valor-recaudado as pendiente"
                + " FROM caja_menor cm inner join terceros t on t.id = cm.idtercero"
                + "               inner join bancos_cuenta c on c.id = cm.idcuenta"
                + "               inner join bancos b on c.idbanco = b.id"
                + " WHERE concepto = 'ANTICIPO' AND cm.estado <> 'Anulado' and valor-recaudado > 0 " + busqueda + " ORDER BY recibo";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ModalRecibos");
    }

    public String TablaCompra(HttpServletRequest request) {
        int proveedor = Globales.Validarintnonull(request.getParameter("proveedor"));
        int compra = Globales.Validarintnonull(request.getParameter("compra"));

        String busqueda = "";
        if (compra == 0) {
            busqueda = "WHERE cd.idproveedor = " + proveedor + " and cd.idcompra = 0 and idusuario = " + idusuario;
        } else {
            busqueda = "WHERE cd.idcompra = " + compra;
        }

        sql = "select cd.id, cd.descripcion, cd.cantidad, cd.precio, cd.iva, cd.descuento, cd.subtotal, cd.orden, cd.idarticulo, cd.idcentro, idservicio, idcuecontable, ingreso,"
                + "     (select max(remision) FROM remision r INNER JOIN remision_detalle rd on rd.idremision = r.id where rd.ingreso = cd.ingreso) as remision "
                + "    from compra_detalle cd " + busqueda + " order by cd.id";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->TablaCompra");
    }

    public String BuscarCompra(HttpServletRequest request) {

        int Compra = Globales.Validarintnonull(request.getParameter("Compra"));
        String Tipo = (request.getParameter("Tipo"));

        sql = "SELECT c.id, compra, c.orden, c.observacion, to_char(c.fecha,'yyyy-MM-dd') as fecha, c.estado, c.concepto,  "
                + "  saldo, to_char(c.vencimiento,'yyyy-MM-dd') as vencimiento, to_char(c.fechareg,'yyyy-MM-dd HH24:MI') as fechareg, c.plazopago, factura, c.recibo, valor-recaudado as saldorecibo, valor, "
                + "  u.nombrecompleto as usuario, p.documento, c.retefuente, c.reteiva, c.reteica, "
                + "  t.nombre || ' (' || nit ||  case when t.documento = 'NIT' THEN '-' || t.verificador when t.documento = 'RUT' THEN '-'  || t.verificador else '' end || ')' as tercero,"
                + "  case when c.estado = 'Anulado' then 'Anulado por ' || ua.nombrecompleto || ' <b>fecha:</b> ' || to_char(c.fechaanula,'dd/MM/yyyy') || ' <b>observación:</b> ' || c.obseranula else '' end as anulado"
                + "  FROM compra c inner join seguridad.rbac_usuario u on u.idusu = c.idusuario"
                + "          inner join seguridad.rbac_usuario ua on ua.idusu = c.idusuarioanula"
                + "          inner join caja_menor ca on ca.recibo = c.recibo"
                + "          inner join terceros t on t.id = ca.idtercero"
                + "          inner join proveedores p on p.id = c.idproveedor where c.compra = " + Compra + " and c.tipo = '" + Tipo + "'";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->BuscarCompra");
    }

    public String AnularCompra(HttpServletRequest request) {
        int id = Globales.Validarintnonull(request.getParameter("id"));
        String observaciones = (request.getParameter("observaciones"));
        try {
            sql = " SELECT * FROM anular_compra(" + id + ",'" + observaciones + "'," + idusuario + ")";
            datos = Globales.Obtenerdatos(sql, this.getClass() + "-->AnularCompra", 1);
            if (datos.getString("_numero").equals("0")) {
                return "0|Compra anulada con éxito";
            } else {
                return "1|" + datos.getString("_mensaje");
            }
        } catch (Exception ex) {
            return "9|" + ex.getMessage();
        }

    }

    public String ReemplazarCompra(HttpServletRequest request) {

        int id = Globales.Validarintnonull(request.getParameter("id"));
        String observaciones = (request.getParameter("observaciones"));

        try {
            sql = " SELECT * FROM reemplzar_compra(" + id + ",'" + observaciones + "'," + idusuario + ")";
            datos = Globales.Obtenerdatos(sql, this.getClass() + "-->ReemplazarCompra", 1);
            if (datos.getString("_numero").equals("0")) {
                return "0|Compra anulada con éxito";
            } else {
                return "1|" + datos.getString("_mensaje");
            }
        } catch (Exception ex) {
            return "9|" + ex.getMessage();
        }

    }

    public String GuardarOrdenCompra(HttpServletRequest request) {
        int proveedor = Globales.Validarintnonull(request.getParameter("proveedor"));
        int orden = Globales.Validarintnonull(request.getParameter("orden"));

        int encontrado = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT count(cd.id) from compra_detalle cd left join compra c on c.id=cd.idcompra where coalesce(c.estado,'Registrado') not in ('Anulado','Reemplazado') and cd.orden = " + orden));

        if (encontrado > 0) {
            return "1|Esta órden de compra ya fue asignada a una factura de compra";
        }

        sql = "INSERT INTO compra_detalle(idproveedor, idcompra, descripcion, precio, descuento, cantidad, subtotal,iva, orden, idarticulo, idusuario)"
                + "    select " + proveedor + ",0, descripcion, precio, descuento, cantidad, subtotal, iva, " + orden + ", idarticulo," + idusuario + " "
                + "    from otro_registro  "
                + "    where idproveedor = " + proveedor + " and ingreso = 0 and  tipo = 2 and numorden = " + orden + " order by id";
        if (Globales.DatosAuditoria(sql, "COMPRAS", "ASIGNAR ORDEN", idusuario, iplocal, this.getClass() + "-->GuardarOrdenCompra")) {
            return "0|Ingreso agregado con éxito";
        } else {
            return "1|Error inesperado... Revise el log de errores";
        }

    }

    public String GuardarCompra(HttpServletRequest request) {
        int proveedor = Globales.Validarintnonull(request.getParameter("proveedor"));
        String orden = (request.getParameter("orden"));
        double total = Globales.Validarintnonull(request.getParameter("total"));
        double subtotal = Globales.Validarintnonull(request.getParameter("subtotal"));
        double descuento = Globales.Validarintnonull(request.getParameter("descuento"));
        double gravable = Globales.Validarintnonull(request.getParameter("gravable"));
        double exento = Globales.Validarintnonull(request.getParameter("exento"));
        double iva = Globales.Validarintnonull(request.getParameter("iva"));
        double reteiva = Globales.Validarintnonull(request.getParameter("reteiva"));
        double retefuente = Globales.Validarintnonull(request.getParameter("retefuente"));
        double reteica = Globales.Validarintnonull(request.getParameter("reteica"));
        String observacion = (request.getParameter("observacion"));
        String fechacom = (request.getParameter("fechacom"));
        String a_id = (request.getParameter("a_id"));
        String a_articulo = (request.getParameter("a_articulo"));
        String a_precio = (request.getParameter("a_precio"));
        String a_centro = (request.getParameter("a_centro"));
        String a_cantidad = (request.getParameter("a_cantidad"));
        String a_descuento = (request.getParameter("a_descuento"));
        String a_iva = (request.getParameter("a_iva"));
        String a_subtotal = (request.getParameter("a_subtotal"));
        String vencimiento = (request.getParameter("vencimiento"));
        String factura = (request.getParameter("factura"));
        String tipo = (request.getParameter("tipo"));
        int recibo = Globales.Validarintnonull(request.getParameter("recibo"));
        int plazopago = Globales.Validarintnonull(request.getParameter("plazopago"));
        String concepto = (request.getParameter("concepto"));
        int idreteiva = Globales.Validarintnonull(request.getParameter("idreteiva"));
        int idretefuente = Globales.Validarintnonull(request.getParameter("idretefuente"));
        int idreteica = Globales.Validarintnonull(request.getParameter("idreteica"));
        try {
            sql = " SELECT * FROM guardar_compra('" + concepto + "','" + fechacom + "'," + proveedor + ",'" + orden + "'," + total + "," + subtotal + "," + descuento + "," + gravable
                    + "," + exento + "," + iva + "," + reteiva + "," + retefuente + "," + reteica + ","
                    + idreteiva + "," + idretefuente + "," + idreteica + ",'" + observacion
                    + "'," + a_id + "," + a_precio + "," + a_cantidad + "," + a_descuento
                    + "," + a_iva + "," + a_subtotal + "," + a_articulo + "," + a_centro + ",'" + vencimiento + "','" + factura + "','" + tipo + "'," + recibo + "," + plazopago + "," + idusuario + ")";

            datos = Globales.Obtenerdatos(sql, this.getClass() + "-->GuardarCompra", 1);
            if (datos.getString("_compra").equals("0")) {
                return "1|" + datos.getString("_mensaje");
            } else {
                return "0|Compra guardada con el número|" + datos.getString("_Compra") + "|" + datos.getString("_idCompra");
            }
        } catch (Exception ex) {
            return "9|" + ex.getMessage();
        }
    }

    public String DescripcionDetalleOrden(HttpServletRequest request) {
        int id = Globales.Validarintnonull(request.getParameter("id"));

        sql = "SELECT descripcion FROM otro_registro WHERE id = " + id;
        return Globales.ObtenerUnValor(sql);
    }

    public String PorcentajeRetencion(HttpServletRequest request) {
        int retencion = Globales.Validarintnonull(request.getParameter("retencion"));
        sql = "select porcentaje from contabilidad.retenciones where id =" + retencion;
        return Globales.ObtenerUnValor(sql);
    }

    public String GuardarOtroRegistro(HttpServletRequest request) {
        int ingreso = Globales.Validarintnonull(request.getParameter("ingreso"));
        int proveedor = Globales.Validarintnonull(request.getParameter("proveedor"));
        int id = Globales.Validarintnonull(request.getParameter("id"));
        String descripcion = (request.getParameter("descripcion"));
        int cantidad = Globales.Validarintnonull(request.getParameter("cantidad"));
        int descuento = Globales.Validarintnonull(request.getParameter("descuento"));
        double precio = Globales.ValidardoublenoNull(request.getParameter("precio"));
        double subtotal = Globales.ValidardoublenoNull(request.getParameter("subtotal"));
        int tipo = Globales.Validarintnonull(request.getParameter("tipo"));
        int compra = Globales.Validarintnonull(request.getParameter("compra"));
        String tipocompra = (request.getParameter("tipocompra"));

        sql = "SELECT estado FROM compra where compra = " + compra + " tipo = '" + tipocompra + "'";
        String estado = Globales.ObtenerUnValor(sql);
        if (!estado.equals("")) {
            return "1|No se puede editar una compra en estado " + estado;
        }

        try {
            if (id == 0) {
                sql = "INSERT INTO otro_registro(compra, tipocompra descripcion, precio, cantidad, descuento, subtotal, idproveedor, idcliente, tipo)"
                        + "VALUES (" + compra + ",'" + tipocompra + "','" + descripcion + "'," + precio + "," + cantidad + "," + descuento + "," + subtotal + "," + proveedor + ",0," + tipo + ");";
                if (Globales.DatosAuditoria(sql, "COMPRA", "AGREGAR OTRO INGRESO", idusuario, iplocal, this.getClass() + "-->GuardaTempFactura")) {
                    id = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT MAX(id) FROM otro_registro"));
                    return "0|Ingreso agregado con éxito|" + id;
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }

            } else {
                sql = "UPDATE otro_registro"
                        + "SET descripcion='" + descripcion + "', precio=" + precio + ", cantidad=" + cantidad + ", descuento=" + descuento + ",subtotal=" + subtotal + ", ingreso = " + ingreso
                        + " WHERE id = " + id;
                if (Globales.DatosAuditoria(sql, "COMPRA", "EDITAR OTRO INGRESO", idusuario, iplocal, this.getClass() + "-->GuardarOtroRegistro")) {
                    return "0|Ingreso actualizado con éxito|" + id;
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }

            }

        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }

    }

    public String GuardarDetCompra(HttpServletRequest request) {
        int proveedor = Globales.Validarintnonull(request.getParameter("proveedor"));
        int id = Globales.Validarintnonull(request.getParameter("id"));
        String descripcion = (request.getParameter("descripcion"));
        int cantidad = Globales.Validarintnonull(request.getParameter("cantidad"));
        int descuento = Globales.Validarintnonull(request.getParameter("descuento"));
        int iva = Globales.Validarintnonull(request.getParameter("iva"));
        double precio = Globales.ValidardoublenoNull(request.getParameter("precio"));
        double subtotal = Globales.ValidardoublenoNull(request.getParameter("subtotal"));
        int idCompra = Globales.Validarintnonull(request.getParameter("idCompra"));
        int idarticulo = Globales.Validarintnonull(request.getParameter("idarticulo"));
        int cuenta = Globales.Validarintnonull(request.getParameter("cuenta"));

        String mensaje = "";
        try {
            sql = "SELECT estado FROM compra WHERE id = " + idCompra;

            String Estado = Globales.ObtenerUnValor(sql).trim();
            if (!Estado.equals("Registrado") && !Estado.equals("")) {
                return "1|No se puede editar una compra en estado " + Estado;
            }

            sql = "SELECT id FROM contabilidad.cuenta WHERE cuenta = " + cuenta;
            int idcuenta = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
            if (idcuenta == 0) {
                return "1|El artículo no posee una cuenta contable asignada";
            }

            if (id == 0) {
                sql = "INSERT INTO compra_detalle(idcompra, descripcion, precio, cantidad, descuento, subtotal,iva, idarticulo, idproveedor, idusuario, idcuecontable)"
                        + "  VALUES (" + idCompra + ",'" + descripcion + "'," + precio + "," + cantidad + "," + descuento + "," + subtotal + "," + iva + "," + idarticulo + "," + proveedor + "," + idusuario + "," + idcuenta + ")";
                if (Globales.DatosAuditoria(sql, "COMPRAS", "AGREGAR ITEMS", idusuario, iplocal, this.getClass() + "-->GuardarDetCompra")) {
                    mensaje = "0|Detalle agregado con éxito";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            } else {
                sql = "UPDATE compra_detalle"
                        + "SET descripcion='" + descripcion + "', precio=" + precio + ", cantidad=" + cantidad + ", descuento=" + descuento
                        + ",subtotal=" + subtotal + ",iva=" + iva + ", idcuecontable=" + idcuenta
                        + " WHERE id = " + id + ";";
                if (Globales.DatosAuditoria(sql, "COMPRAS", "ACTUALIZAR ITEMS", idusuario, iplocal, this.getClass() + "-->GuardarDetCompra")) {
                    mensaje = "0|Detalle agregado con éxito";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            }

            return mensaje;
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }

    }

    public String TablaDocumentCompra(HttpServletRequest request) {
        int IdCompra = Globales.Validarintnonull(request.getParameter("IdCompra"));
        sql = "SELECT id, archivo, foto FROM compra_anexo WHERE idcompra = " + IdCompra;
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->TablaDocumentCompra");
    }

    public String EliminarDocumentCompra(HttpServletRequest request) {
        int id = Globales.Validarintnonull(request.getParameter("id"));
        int Compra = Globales.Validarintnonull(request.getParameter("Compra"));
        int foto = Globales.Validarintnonull(request.getParameter("foto"));

        try {
            File archivo = new File(Globales.ruta_archivo + Compra + "/" + foto + ".pdf");
            Files.deleteIfExists(archivo.toPath());
            sql = "DELETE FROM compra_anexo WHERE id = " + id;
            Globales.Obtenerdatos(sql, this.getClass() + "-->EliminarDocumentCompra", 2);
            return "0|Archivo eliminado con éxito";
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    /*
    public String AdjuntarArchivo(HttpServletRequest request) {
        HttpPostedFileBase documento
        String respuesta = "9|Error al Subir el Archivo ";
        if (documento != null) {
            String adjunto = documento.FileName;
            Session["ArchivoExcel"] = adjunto;
            documento.SaveAs(HostingEnvironment.MapPath("~/uploads/" + adjunto));
            respuesta = "0|" + HostingEnvironment.MapPath("~/uploads/" + adjunto);
        }

        return respuesta;
    }
     */
    /*
    public String GuardarDocumentCompra(HttpServletRequest request) {
        int IdCompra = Globales.Validarintnonull(request.getParameter("IdCompra"));
        int Compra = Globales.Validarintnonull(request.getParameter("Compra"));

        if (Session["ArchivoExcel"] == null) {
            return "1|Error al adjuntar el archivo pdf de la compra... Intentelo de nuevo";
        }

        try {
            int foto = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT max(foto) from compra_anexo where idcompra = " + IdCompra)) + 1;
            String fileName = HostingEnvironment.MapPath("~/uploads/" + Session["ArchivoExcel"].ToString());
            String NombreArchivo = Session["ArchivoExcel"].ToString();
            String path = HostingEnvironment.MapPath("~/imagenes/ArchivoCompra/" + Compra);
            if (!Directory.Exists(path)) {
                DirectoryInfo di = Directory.CreateDirectory(path);
            }
            String filedestino = path + "\\" + foto + ".pdf";
            System.IO.File.Copy(fileName, filedestino, true);

            sql = "INSERT INTO compra_anexo(idcompra, archivo, foto)"
                    + " VALUES (" + IdCompra + ",'" + NombreArchivo + "'," + foto + ")";
            Globales.Obtenerdatos(sql, this.getClass() + "-->GuardarDocumentCompra", 2);

            Session["ArchivoExcel"] = null;
            return "0|Archivo agregado con éxito";
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }
*/
    public String EliminarRegistrosSel(HttpServletRequest request) {

        String seleccion = (request.getParameter("seleccion"));
        int idCompra = Globales.Validarintnonull(request.getParameter("idCompra"));

        String mensaje = "";
        try {
            sql = "SELECT estado FROM compra WHERE id = " + idCompra;
            String Estado = Globales.ObtenerUnValor(sql).trim();
            if (!Estado.equals("")) {
                return "1|No se puede eliminar los registros de una compra en estado " + Estado;
            }

            sql = "DELETE FROM compra_detalle WHERE id in (" + seleccion + ")";
            if (Globales.DatosAuditoria(sql, "FACTURA", "GUARDAR TEMPORAL", idusuario, iplocal, this.getClass() + "--> EliminarRegistrosSel")) {
                return "0|Ingreso eliminado con éxito";
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }

        } catch (Exception ex) {

            return "1|" + ex.getMessage();
        }

    }

    public String ActualizarRetencion(HttpServletRequest request) {

        int tipo = Globales.Validarintnonull(request.getParameter("tipo"));
        int idretencion = Globales.Validarintnonull(request.getParameter("idretencion"));
        int proveedor = Globales.Validarintnonull(request.getParameter("proveedor"));

        if (Globales.PermisosSistemas("PROVEEDOR ACTUALIZAR RETENCION", idusuario) == 0) {
            return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
        }

        switch (tipo) {
            case 1: //FUENTE
                sql = "UPDATE proveedores set retefuente = " + idretencion + " where id = " + proveedor;
                break;
            case 2: //IVA
                sql = "UPDATE proveedores set reteiva = " + idretencion + " where id = " + proveedor;
                break;
            case 3: //ICA
                sql = "UPDATE proveedores set reteica = " + idretencion + " where id = " + proveedor;
                break;
        }

        if (Globales.DatosAuditoria(sql, "PROVEEDOR", "ACTUALIZAR RETENCION", idusuario, iplocal, this.getClass() + "-->ActualizarRetencion")) {
            return "0|Retención actualizada con éxito";
        } else {
            return "1|Error inesperado... Revise el log de errores";
        }
    }

    public String Contabilizar_Compra(HttpServletRequest request) {
        int factura = Globales.Validarintnonull(request.getParameter("factura"));
        int proveedor = Globales.Validarintnonull(request.getParameter("proveedor"));
        double subtotal = Globales.ValidardoublenoNull(request.getParameter("subtotal"));
        double total = Globales.ValidardoublenoNull(request.getParameter("total"));
        double iva = Globales.ValidardoublenoNull(request.getParameter("iva"));
        double reteiva = Globales.ValidardoublenoNull(request.getParameter("reteiva"));
        double retefuente = Globales.ValidardoublenoNull(request.getParameter("retefuente"));
        double reteica = Globales.ValidardoublenoNull(request.getParameter("retefuente"));
        String a_cuenta = request.getParameter("a_cuenta");
        String a_monto = request.getParameter("a_monto");
        String a_centro = request.getParameter("a_centro");
        double tdebito = Globales.ValidardoublenoNull(request.getParameter("tdebito"));
        double tcredito = Globales.ValidardoublenoNull(request.getParameter("tcredito"));
        int buscar = Globales.Validarintnonull(request.getParameter("buscar"));
        int idreteiva = Globales.Validarintnonull(request.getParameter("idreteiva"));
        int idretefuente = Globales.Validarintnonull(request.getParameter("idretefuente"));
        int idreteica = Globales.Validarintnonull(request.getParameter("idreteica"));

        try {
            if (factura > 0) {
                if (Globales.PermisosSistemas("COMPRA CONTABILIZAR", idusuario) == 0) {
                    return "1||Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
            }

            sql = "SELECT _idcuenta, _cuenta, sum(_debito) as _debito, sum(_credito) as _credito, _error, _mensaje  "
                    + "  FROM contabilidad.contabilizacion_compra(" + factura + "," + proveedor + "," + subtotal + "," + total + "," + iva + "," + reteiva + ","
                    + retefuente + "," + reteica + "," + idreteiva + "," + idretefuente + "," + idreteica + "," + a_cuenta + "," + a_monto + "," + a_centro + "," + tdebito + "," + tcredito + "," + idusuario + "," + buscar + ")"
                    + "group by _idcuenta, _cuenta, _orden, _error, _mensaje order by _orden";

            return "0||" + Globales.ObtenerDatosJSon(sql, this.getClass() + "-->Contabilizar_Compra");
        } catch (Exception ex) {
            return "1||" + ex.getMessage();
        }
    }

    public String AnularContabilizacion(HttpServletRequest request) {
        int compra = Globales.Validarintnonull(request.getParameter("compra"));
        try {
            sql = "SELECT estado, idcomprobante, to_char(fecha,'yyyy-MM') as fecha FROM factura where factura = " + compra;
            datos = Globales.Obtenerdatos(sql, this.getClass() + "-->AnularContabilizacion", 1);
            String estado = datos.getString("estado").trim();
            String fecha = datos.getString("fecha").trim();
            int comprobante = Globales.Validarintnonull(datos.getString("idcomprobante").trim());
            Date fechaactual = new Date();
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM");
            if (!estado.equals("Contabilizado")) {
                return "1|No se anular un comprobante contable de una factura en estado " + estado;
            }

            if (Globales.PermisosSistemas("FACTURA CONTABILIZACION ANULAR", idusuario) == 0) {
                return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
            }

            if (!fecha.equals(dateFormat.format(fechaactual))) {
                return "1|No se puede anular un comprobante que no pertenezca al mes actual";
            }

            sql = "update compra set estado = 'Facturado',idusuarioconta=0, fechaconta=null, idcomprobante=0 where compra = " + compra + ";";
            sql += "update contabilidad.comprobante set estado = 'Anulado', idusuarioanula=" + idusuario + ", fechaanula=now()  where id = " + comprobante;
            if (Globales.DatosAuditoria(sql, "FACTURA", "CONTABILIZACION ELIMINAR", idusuario, iplocal, this.getClass() + "-->AnularContabilizacion")) {
                return "0|Contabilización de factura anulado con éxito";
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }

        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    //FIN DEL CÓDIGO
    protected void Servidor(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        String ruta_origen = request.getHeader("Origin");

        HttpSession misession = request.getSession();
        idusuario = misession.getAttribute("idusuario").toString();
        response.setHeader("Access-Control-Allow-Origin", request.getHeader("Origin"));
        response.setHeader("Access-Control-Allow-Credentials", "true");

        try (PrintWriter out = response.getWriter()) {
            if (!ruta_origen.equals("http://localhost:8090") && !ruta_origen.equals("http://192.168.0.55:82") && !ruta_origen.equals("https://192.168.0.55:90")) {
                out.print("[{\"error\":\"505\",\"mensaje\":\"Usted no tiene permiso para acceder a esta ruta\"}]");
            } else {
                if (misession.getAttribute("codusu") == null) {
                    out.println("cerrada");
                } else {
                    if (!Globales.IpUsuario(misession.getAttribute("idusuario").toString()).equals(misession.getAttribute("IPLocal").toString())) {
                        out.println("cambio-ip");
                    } else {
                        String opcion = request.getParameter("opcion");
                        switch (opcion) {
                            case "AutoCompletoOServ":
                                out.println(AutoCompletoOServ());
                                break;
                            //Wilmer    
                            case "ConsultarCompra":
                                out.println(ConsultarCompra(request));
                                break;
                            case "EliminarTempCompra":
                                out.println(EliminarTempCompra(request));
                                break;
                            case "BuscarTempCompra":
                                out.println(BuscarTempCompra(request));
                                break;
                            case "ModalRecibos":
                                out.println(BuscarTempCompra(request));
                                break;
                            case "TablaCompra":
                                out.println(TablaCompra(request));
                                break;
                            case "BuscarCompra":
                                out.println(BuscarCompra(request));
                                break;
                            case "AnularCompra":
                                out.println(AnularCompra(request));
                                break;
                            case "ReemplazarCompra":
                                out.println(ReemplazarCompra(request));
                                break;
                            case "GuardarOrdenCompra":
                                out.println(GuardarOrdenCompra(request));
                                break;
                            case "DescripcionDetalleOrden":
                                out.println(DescripcionDetalleOrden(request));
                                break;
                            case "PorcentajeRetencion":
                                out.println(PorcentajeRetencion(request));
                                break;
                            case "GuardarOtroRegistro":
                                out.println(GuardarOtroRegistro(request));
                                break;
                            case "GuardarDetCompra":
                                out.println(GuardarDetCompra(request));
                                break;
                            case "TablaDocumentCompra":
                                out.println(TablaDocumentCompra(request));
                                break;
                            case "EliminarDocumentCompra":
                                out.println(EliminarDocumentCompra(request));
                                break;
                            /*    
                            case "AdjuntarArchivo":
                                out.println(AdjuntarArchivo(request));
                                break;
                             
                            case "GuardarDocumentCompra":
                                out.println(GuardarDocumentCompra(request));
                                break;
                             */   
                            case "EliminarRegistrosSel":
                                out.println(EliminarRegistrosSel(request));
                                break;
                            case "ActualizarRetencion":
                                out.println(ActualizarRetencion(request));
                                break;
                            case "Contabilizar_Compra":
                                out.println(Contabilizar_Compra(request));
                                break;
                            case "AnularContabilizacion":
                                out.println(AnularContabilizacion(request));
                                break;
                            //FIN DEL CÓDIGO    
                            default:
                                throw new AssertionError();
                        }
                    }
                }
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Servidor(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Servidor(request, response);
    }

}
