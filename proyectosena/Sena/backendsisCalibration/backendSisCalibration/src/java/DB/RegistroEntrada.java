package DB;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author eurid
 */
@WebServlet(urlPatterns = {"/RegistroEntrada"})
public class RegistroEntrada extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    String sql;
    String idusuario = "";
    String usuario = "";
    ResultSet datos = null;
    String iplocal = "";
    String url_archivo = Globales.url_archivo;
    String archivoadjunto = null;

    public String GuardarPersonal(HttpServletRequest request) {

        int IdPersonal = Globales.Validarintnonull(request.getParameter("IdPersonal"));
        String Documento = request.getParameter("Documento");
        String Nombres = request.getParameter("Nombres");
        String Cargo = request.getParameter("Cargo");
        String Telefonos = request.getParameter("Telefonos");
        String Celular = request.getParameter("Celular");
        String CorreoPersonal = request.getParameter("CorreoPersonal");

        int Ciudad = Globales.Validarintnonull(request.getParameter("Ciudad"));
        String DocumentoEntr = request.getParameter("DocumentoEntr");
        String Motivo = request.getParameter("Motivo");
        String Empresa = request.getParameter("Empresa");

        int EPS = Globales.Validarintnonull(request.getParameter("EPS"));
        int ARL = Globales.Validarintnonull(request.getParameter("ARL"));
        String Contacto = request.getParameter("Contacto");
        String TelefonosEmer = request.getParameter("TelefonosEmer");
        String Area = request.getParameter("Area");
        int Responsable = Globales.Validarintnonull(request.getParameter("Responsable"));
        String EPP = request.getParameter("EPP");

        String resultado = "";
        int IdVisita = 0;
        String FechaSalida = "";
        String FechaEntrada = "";
        int ValorEPP = 1;
        if (EPP == null) {
            ValorEPP = 0;
        }
        try {
            if (IdPersonal == 0) {

                if (Globales.PermisosSistemas("REGISTRO DEL PERSONAL AGREGAR PERSONAL EXTERNO", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }

                sql = "INSERT INTO personal_externo(nombrecompleto, documento, telefono, email, celular, cargo) "
                        + "VALUES('" + Nombres + "','" + Documento + "','" + Telefonos + "','" + CorreoPersonal + "','" + Celular + "','" + Cargo + "');";
                if (Globales.DatosAuditoria(sql, "REGISTRO DEL PERSONAL", "AGREGAR PERSONAL EXTERNO",idusuario,iplocal,  this.getClass() + "-->GuardarPersonal")){
                    String sql2 = "SELECT MAX(id) FROM personal_externo;";
                    IdPersonal = Globales.Validarintnonull(Globales.ObtenerUnValor(sql2));
                    resultado = "0|Personal y visita agregado con éxito |" + IdPersonal;
                }else{
                    return "1|Error inesperado... Revise el log de errores";
                }
                
            } else {
                if (Globales.PermisosSistemas("RREGISTRO DEL PERSONAL EDITAR PERSONAL EXTERNO", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "UPDATE personal_externo "
                        + "SET nombrecompleto='" + Nombres + "',  "
                        + "documento ='" + Documento + "', telefono='" + Telefonos + "', email='" + CorreoPersonal + "', celular='" + Celular + "', "
                        + "cargo='" + Cargo + "' "
                        + "WHERE id= " + IdPersonal;
                if (Globales.DatosAuditoria(sql, "REGISTRO DEL PERSONAL", "EDITAR PERSONAL EXTERNO",idusuario,iplocal,  this.getClass() + "-->GuardarPersonal")){
                    resultado = "0|Personal y visita actualizado con éxito|" + IdPersonal;
                }else{
                    return "1|Error inesperado... Revise el log de errores";
                }
            }

            sql = "SELECT id, to_char(fechasalida, 'yyyy-MM-dd HH24:MI') as fechasalida FROM visitas WHERE idpersonal=" + IdPersonal + " and to_char(fechaentrada,'yyyy-MM-dd') = '" + Globales.FechaActual(1) + "' order by id desc limit 1";
            datos = Globales.Obtenerdatos(sql, this.getClass() + "-->GuardarPersonal", 1);
            if (datos.next()) {
                IdVisita = Globales.Validarintnonull(datos.getString("id"));
                FechaSalida = datos.getString("fechasalida");
            }

            if (IdVisita == 0 || FechaSalida != null) {
                sql = "INSERT INTO visitas(idpersonal, empresa, motivo, documentoentre, idusuario, idciudad, ideps, idarl, idresponsable, area, contacto, "
                        + "telefonoemer, epp) "
                        + "VALUES (" + IdPersonal + ",'" + Empresa.trim().toUpperCase() + "','" + Motivo + "','" + DocumentoEntr + "'," + idusuario + "," + Ciudad
                        + "," + EPS + "," + ARL + "," + Responsable + ",'" + Area + "','" + Contacto + "','" + TelefonosEmer + "'," + ValorEPP + ");";
                Globales.DatosAuditoria(sql, "REGISTRO DEL PERSONAL","ASIGNAR VISITA", idusuario, iplocal, this.getClass() + "-->GuardarPersonal");
                String sql3 = "SELECT MAX(id) FROM visitas";
                IdVisita = Globales.Validarintnonull(Globales.ObtenerUnValor(sql3));
            } else {
                sql = "UPDATE visitas SET fechasalida = now() WHERE id=" + IdVisita + ";";
                Globales.DatosAuditoria(sql, "REGISTRO DEL PERSONAL","ACTUALIZAR VISITA", idusuario, iplocal, this.getClass() + "-->GuardarPersonal");
            }

            sql = "SELECT to_char(fechasalida, 'yyyy-MM-dd HH24:MI') as fechasalida, to_char(fechaentrada, 'yyyy-MM-dd HH24:MI') as fechaentrada "
                    + "FROM visitas "
                    + "WHERE id=" + IdVisita;
            datos = Globales.Obtenerdatos(sql, this.getClass() + "-->GuardarPersonal", 1);
            if (datos.next()) {
                FechaSalida = datos.getString("fechasalida");
                FechaEntrada = datos.getString("fechaentrada");
            }

            resultado += "|" + IdVisita + "|" + FechaEntrada + "|" + FechaSalida;

        } catch (SQLException ex) {
            return "1|" + ex.getMessage();
        }

        return resultado;

    }

    public String BuscarPersonal(HttpServletRequest request) {
        String documento = request.getParameter("documento");

        sql = "SELECT p.*, to_char(V.fechaentrada,'yyyy-MM-dd HH24:MI') AS fechaentrada, to_char(v.fechaentrada,'yyyy-MM-dd') AS fecha, coalesce(v.id,0) as idvisita,\n"
                + "to_char(v.fechasalida,'yyyy-MM-dd HH24:MI') AS fechasalida, coalesce(idciudad,0) as ciudad, coalesce(iddepartamento,0) as departamento, coalesce(idpais,0) as pais, \n"
                + "coalesce(v.motivo,'') as motivo, coalesce(v.empresa,'') as empresa, coalesce(documentoentre,'Cedula') as documentoentre, \n"
                + "coalesce(idresponsable,0) as responsable, coalesce(ideps,0) as eps, coalesce(idarl,0) as arl, coalesce(epp,0) as epp,\n"
                + "coalesce(area,'') as area, coalesce(contacto,'') as contacto, coalesce(telefonoemer,'') as telefonoemer  \n"
                + "FROM personal_externo p left join visitas v on v.idpersonal = p.id\n"
                + "left JOIN ciudad c on c.id = v.idciudad\n"
                + "left join departamento d on d.id = c.iddepartamento\n"
                + "WHERE documento = '" + documento + "' ORDER BY v.id DESC LIMIT 1";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->GuardarPersonal");
    }

    public String TablaVisitas(HttpServletRequest request) {
        int personal = Globales.Validarintnonull(request.getParameter("personal"));

        sql = "SELECT to_char(v.fechaentrada,'yyyy-MM-dd HH24:MI') AS fechaentrada, to_char(v.fechasalida,'yyyy-MM-dd HH24:MI') AS fechasalida, "
                + "v.motivo, v.empresa "
                + "FROM visitas v "
                + "WHERE idpersonal = " + personal;
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->TablaVisitas");
    }

    public String DetalleVisita(HttpServletRequest request) {
        int visita = Globales.Validarintnonull(request.getParameter("visita"));

        sql = "SELECT p.*, to_char(V.fechaentrada,'yyyy-MM-dd HH24:MI') AS fechaentrada, to_char(v.fechaentrada,'yyyy-MM-dd') AS fecha, coalesce(v.id,0) as idvisita, "
                + "to_char(v.fechasalida,'yyyy-MM-dd HH24:MI') AS fechasalida, c.descripcion as ciudad, d.descripcion as departamento, pa.descripcion as pais, "
                + "v.motivo, v.empresa, documentoentre,  u.nombrecompleto as responsable, "
                + "eps.descripcion as eps, arl.descripcion as arl, epp, "
                + "area, contacto, telefonoemer "
                + "FROM personal_externo p inner join visitas v on v.idpersonal = p.id "
                + "inner JOIN ciudad c on c.id = v.idciudad "
                + "inner join departamento d on d.id = c.iddepartamento "
                + "inner join pais pa on pa.id = d.idpais "
                + "inner join eps on eps.id = v.ideps "
                + "inner join arl on arl.id = v.idarl "
                + "inner join seguridad.rbac_usuario u on u.idusu = v.idresponsable "
                + "WHERE v.id = " + visita;
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->DetalleVisita");
    }

    public String ConsultaVisitas(HttpServletRequest request) {
        String empresa = request.getParameter("empresa");
        String visitante = request.getParameter("visitante");
        String motivo = request.getParameter("motivo");
        String desde = request.getParameter("desde");
        String hasta = request.getParameter("hasta");

        String busqueda = " where to_char(v.fechaentrada,'yyyy-MM-dd') >= '" + desde + "' and to_char(v.fechaentrada,'yyyy-MM-dd') <= '" + hasta + "' ";
        if (!empresa.equals("")) {
            busqueda += " and empresa ilike '%" + empresa + "%' ";
        }
        if (!visitante.equals("")) {
            busqueda += " and nombrecompleto ilike '%" + visitante + "%' ";
        }
        if (!motivo.equals("")) {
            busqueda += " and motivo ilike '%" + motivo + "%' ";
        }

        sql = "SELECT p.nombrecompleto || ' (' || p.documento || ')' as visitante, p.telefono || '<br>' ||p.celular as telefono, p.email, p.cargo,  \n"
                + "to_char(V.fechaentrada,'yyyy-MM-dd HH24:MI') AS fechaentrada, to_char(v.fechasalida,'yyyy-MM-dd HH24:MI') AS fechasalida, \n"
                + "c.descripcion as ciudad, d.descripcion as departamento, pa.descripcion as pais, u.nombrecompleto as usuario,\n"
                + "v.motivo, v.empresa, v.id as visita,\n"
                + "'<img src=''" + url_archivo + "imagenes/FotoVisita/' || p.documento || '.jpg'' width=''100%'' onclick=''LlamarFoto(' || p.documento || ')''/>' AS foto\n"
                + "FROM personal_externo p INNER join visitas v on v.idpersonal = p.id\n"
                + "INNER JOIN seguridad.rbac_usuario u on u.idusu = v.idusuario\n"
                + "INNER JOIN ciudad c on c.id = v.idciudad\n"
                + "INNER join departamento d on d.id = c.iddepartamento\n"
                + "INNER JOIN pais pa on pa.id = d.idpais " + busqueda + " ORDER BY v.empresa";

        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ConsultaVisitas");
    }

    public String Autocompletar() {
        sql = "SELECT DISTINCT empresa FROM visitas ORDER BY 1";
        String sql2 = "SELECT DISTINCT cargo from personal_externo ORDER BY 1";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->Autocompletar") + "|" + Globales.ObtenerDatosJSon(sql2, this.getClass() + "-->Autocompletar");
    }

    protected void Servidor(HttpServletRequest request, HttpServletResponse response) {

        try {
            response.setContentType("text/html;charset=UTF-8");
            String ruta_origen = request.getHeader("Origin");

            HttpSession misession = request.getSession();
            idusuario = misession.getAttribute("idusuario").toString();
            iplocal = misession.getAttribute("IPLocal").toString();
            usuario = misession.getAttribute("codusu").toString();
            if (misession.getAttribute("ArchivosAdjuntos") != null) {
                archivoadjunto = misession.getAttribute("ArchivosAdjuntos").toString();
            }
            response.setHeader("Access-Control-Allow-Origin", request.getHeader("Origin"));
            response.setHeader("Access-Control-Allow-Credentials", "true");

            PrintWriter out = response.getWriter();
            if (Globales.ValidarRuta(ruta_origen) == false) {
                out.print("[{\"error\":\"505\",\"mensaje\":\"Usted no tiene permiso para acceder a esta ruta\"}]");
            } else if (misession.getAttribute("codusu") == null) {
                out.println("cerrada");
            } else if (!Globales.IpUsuario(misession.getAttribute("idusuario").toString()).equals(misession.getAttribute("IPLocal").toString())) {
                out.println("cambio-ip");
            } else {
                String opcion = request.getParameter("opcion");
                switch (opcion) {
                    //ANDRES
                    case "GuardarPersonal":
                        out.println(GuardarPersonal(request));
                        break;
                    case "BuscarPersonal":
                        out.println(BuscarPersonal(request));
                        break;
                    case "TablaVisitas":
                        out.println(TablaVisitas(request));
                        break;
                    case "DetalleVisita":
                        out.println(DetalleVisita(request));
                        break;
                    case "ConsultaVisitas":
                        out.println(ConsultaVisitas(request));
                        break;
                    case "Autocompletar":
                        out.println(Autocompletar());
                        break;

                    //FIN
                    default:
                        throw new AssertionError();
                }
            }
        } catch (Exception ex) {
            try {
                PrintWriter out = response.getWriter();
                out.println(ex.getMessage());

            } catch (IOException exl) {
                Globales.GuardarLogger(exl.getMessage(), Laboratorio.class.getName());
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        Servidor(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * param request servlet request param response servlet response throws
     * ServletException if a servlet-specific error occurs throws IOException if
     * an I/O error occurs
     *
     * @param request
     * @param response
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        Servidor(request, response);
    }

}
