package DB;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.text.DateFormat;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author leudi
 */
@WebServlet(urlPatterns = {"/Estadisticas"})
public class Estadisticas extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    String sql;
    String idusuario = "";
    String usuario = "";
    ResultSet datos = null;
    String iplocal = "";
    String url_archivo = Globales.url_archivo;
    String archivoadjunto = null;

    //WILMER
    public String TablaInformesGerencia(HttpServletRequest request) {
        int magnitud = Globales.Validarintnonull(request.getParameter("magnitud"));
        String fechaini = request.getParameter("fechaini");
        String fechafin = request.getParameter("fechafin");

        sql = "select count( distinct r.ingreso) as cantidad, e.descripcion  as descripcion"
                + "  from remision_detalle r inner join equipo e on r.idequipo = e.id"
                + "                    inner join modelos mo on mo.id = r.idmodelo "
                + "                    inner join marcas ma on ma.id = mo.idmarca "
                + "                    inner join magnitud_intervalos i on i.id = r.idintervalo"
                + "                    inner join clientes c on c.id = r.idcliente"
                + "    WHERE e.idmagnitud = " + magnitud + " and to_char(fechaing,'yyyy-MM-dd') >= '" + fechaini + "' and to_char(fechaing,'yyyy-MM-dd') <= '" + fechafin + "'"
                + "    group by e.descripcion "
                + "    order by 1 desc";

        String sql2 = "select count( distinct r.ingreso) as cantidad, ma.descripcion  as descripcion"
                + " from remision_detalle r inner join equipo e on r.idequipo = e.id"
                + "                    inner join modelos mo on mo.id = r.idmodelo  "
                + "                    inner join marcas ma on ma.id = mo.idmarca "
                + "                    inner join magnitud_intervalos i on i.id = r.idintervalo"
                + "                    inner join clientes c on c.id = r.idcliente"
                + "  WHERE e.idmagnitud = " + magnitud + " and to_char(fechaing,'yyyy-MM-dd') >= '" + fechaini + "' and to_char(fechaing,'yyyy-MM-dd') <= '" + fechafin + "'"
                + " group by ma.descripcion "
                + " order by 1 desc";

        String sql3 = "select count( distinct r.ingreso) as cantidad, mo.descripcion  as descripcion"
                + " from remision_detalle r inner join equipo e on r.idequipo = e.id"
                + "	                    inner join modelos mo on mo.id = r.idmodelo "
                + "	                    inner join marcas ma on ma.id = mo.idmarca "
                + "	                    inner join magnitud_intervalos i on i.id = r.idintervalo"
                + "	                    inner join clientes c on c.id = r.idcliente"
                + "    WHERE e.idmagnitud = " + magnitud + " and to_char(fechaing,'yyyy-MM-dd') >= '" + fechaini + "' and to_char(fechaing,'yyyy-MM-dd') <= '" + fechafin + "'"
                + "    group by mo.descripcion "
                + "    order by 1 desc";

        String sql4 = "select count( distinct r.ingreso) as cantidad, case when r.idintervalo = 0 then 'VER ESPECIFICACIONES' ELSE '(' || desde || ' a ' || hasta || ') ' || medida end AS descripcion"
                + " from remision_detalle r inner join equipo e on r.idequipo = e.id"
                + "                    inner join modelos mo on mo.id = r.idmodelo  "
                + "                    inner join marcas ma on ma.id = mo.idmarca "
                + "                    inner join magnitud_intervalos i on i.id = r.idintervalo"
                + "                    inner join clientes c on c.id = r.idcliente"
                + "  WHERE e.idmagnitud = " + magnitud + " and to_char(fechaing,'yyyy-MM-dd') >= '" + fechaini + "' and to_char(fechaing,'yyyy-MM-dd') <= '" + fechafin + "'"
                + " group by r.idintervalo, desde, hasta, medida "
                + " order by 1 desc";

        String sql5 = "select count( distinct r.ingreso) as cantidad, c.nombrecompleto  as descripcion"
                + "from remision_detalle r inner join equipo e on r.idequipo = e.id"
                + "                    inner join modelos mo on mo.id = r.idmodelo  "
                + "                    inner join marcas ma on ma.id = mo.idmarca"
                + "                    inner join magnitud_intervalos i on i.id = r.idintervalo"
                + "                    inner join clientes c on c.id = r.idcliente"
                + " WHERE e.idmagnitud = " + magnitud + " and to_char(fechaing,'yyyy-MM-dd') >= '" + fechaini + "' and to_char(fechaing,'yyyy-MM-dd') <= '" + fechafin + "'"
                + " group by c.nombrecompleto "
                + " order by 1 desc";

        String sql6 = "select count( distinct r.ingreso) as cantidad, u.nombrecompleto  as descripcion"
                + "from remision_detalle r inner join equipo e on r.idequipo = e.id"
                + "                    inner join modelos mo on mo.id = r.idmodelo  "
                + "                    inner join marcas ma on ma.id = mo.idmarca "
                + "                    inner join magnitud_intervalos i on i.id = r.idintervalo"
                + "                    inner join certificados c on c.ingreso = r.ingreso"
                + "        inner join seguridad.rbac_usuario u on u.idusu = c.idusuario"
                + "  WHERE e.idmagnitud = " + magnitud + " and to_char(c.fecha,'yyyy-MM-dd') >= '" + fechaini + "' and to_char(c.fecha,'yyyy-MM-dd') <= '" + fechafin + "'"
                + " group by u.nombrecompleto "
                + " order by 1 desc";

        String sql7 = "select count( distinct r.ingreso) as cantidad, e.descripcion  as descripcion"
                + "from remision_detalle r inner join equipo e on r.idequipo = e.id"
                + "                    inner join modelos mo on mo.id = r.idmodelo "
                + "                    inner join marcas ma on ma.id = mo.idmarca "
                + "                    inner join magnitud_intervalos i on i.id = r.idintervalo"
                + "                    inner join certificados c on c.ingreso = r.ingreso"
                + "        inner join seguridad.rbac_usuario u on u.idusu = c.idusuario"
                + "     WHERE e.idmagnitud = " + magnitud + " and to_char(c.fecha,'yyyy-MM-dd') >= '" + fechaini + "' and to_char(c.fecha,'yyyy-MM-dd') <= '" + fechafin + "'"
                + "     group by e.descripcion "
                + "     order by 1 desc";
        String sql8 = "select r.ingreso,  'DID-130-' || trim(to_char(inf.informe,'0000')) as informe, "
                + "   '<b>' || m.descripcion || '</b><br>' || e.descripcion || '<br><b>' || ma.descripcion || '</b><br>' || mo.descripcion || '<br> ' || case when r.idintervalo = 0 then 'VER ESPECIFICACIONES' ELSE '(' || desde || ' a ' || hasta || ') ' || medida end || '<br>' || r.serie as equipo,"
                + "        c.nombrecompleto as cliente, inf.observacion,"
                + "    '<button class=''btn btn-glow btn-success imprimir'' title=''Imprimir Informe Técnico'' type=''button'' onclick=' || chr(34) || 'ImprimirInformeTec(' || inf.informe || ')' ||chr(34) || '><span data-icon=''&#xe0f7;''></span></button>' as imprimir"
                + "from remision_detalle r inner join equipo e on r.idequipo = e.id"
                + "		                    inner join magnitudes m on m.id = e.idmagnitud"
                + "		                    inner join modelos mo on mo.id = r.idmodelo "
                + "		                    inner join marcas ma on ma.id = mo.idmarca"
                + "            inner join informetecnico_dev inf on inf.ingreso = r.ingreso"
                + "            inner join magnitud_intervalos i on i.id = r.idintervalo "
                + "            inner join clientes c on c.id = r.idcliente"
                + " WHERE e.idmagnitud = " + magnitud + " and to_char(inf.fecha,'yyyy-MM-dd') >= '" + fechaini + "' and to_char(inf.fecha,'yyyy-MM-dd') <= '" + fechafin + "'"
                + " order by 1 desc";
        String sql9 = "SELECT * from quejasinforme(" + magnitud + ",'" + fechaini + "','" + fechafin + "')";
        String sql10 = "SELECT _descripcion as descripcion, count(_cantidad) as cantidad "
                + "  from entregainforme(" + magnitud + ",'" + fechaini + "','" + fechafin + "')"
                + " GROUP BY _descripcion"
                + " ORDER BY 1";
        String sql11 = "SELECT cast(avg(respuesta) as decimal(12,2)) as cantidad, count(respuesta) as contador, trim(pregunta) as descripcion, substring(trim(pregunta),1,2)"
                + "  FROM web.encuesta_satisfaccion en inner join remision_detalle rd on rd.ingreso = en.ingreso"
                + "	                                inner join equipo e on e.id = rd.idequipo"
                + "    WHERE e.idmagnitud = " + magnitud + " and to_char(fechaenc,'yyyy-MM-dd') >= '" + fechaini + "' and to_char(fechaenc,'yyyy-MM-dd') <= '" + fechafin + "'"
                + "   GROUP BY pregunta"
                + "  ORDER BY cast(substring(trim(pregunta),1,2) as double precision)";
        String sql12 = "SELECT 'INF-0001-' || revision || '-PT' as revision,"
                + " fecha, idusuario, trabajonoconforme, "
                + " accionespreventivas, auditoriainterna, auditoriaexterna, oportunidadmejora, "
                + " sugerencias, m.descripcion as magnitud, u.nombrecompleto as usuario, u.idusu, u.cargo"
                + " FROM magnitudes m left join revisiones r on m.id = r.idmagnitud"
                + "                             left join seguridad.rbac_usuario u on u.idusu = r.idusuario"
                + "   WHERE m.id = " + magnitud + " and to_char(fecha,'yyyy-MM-dd') = '" + fechaini + "'";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->TablaInformesGerencia") + "|" + Globales.ObtenerDatosJSon(sql2, this.getClass() + "-->TablaInformesGerencia") + "|" + Globales.ObtenerDatosJSon(sql3, this.getClass() + "-->TablaInformesGerencia") + "|"
                + Globales.ObtenerDatosJSon(sql4, this.getClass() + "-->TablaInformesGerencia") + "|" + Globales.ObtenerDatosJSon(sql5, this.getClass() + "-->TablaInformesGerencia") + "|" + Globales.ObtenerDatosJSon(sql6, this.getClass() + "-->TablaInformesGerencia") + "|" + Globales.ObtenerDatosJSon(sql7, this.getClass() + "-->TablaInformesGerencia") + "|"
                + Globales.ObtenerDatosJSon(sql8, this.getClass() + "-->TablaInformesGerencia") + "|" + Globales.ObtenerDatosJSon(sql9, this.getClass() + "-->TablaInformesGerencia") + "|" + Globales.ObtenerDatosJSon(sql10, this.getClass() + "-->TablaInformesGerencia") + "|" + Globales.ObtenerDatosJSon(sql11, this.getClass() + "-->TablaInformesGerencia") + "|" + Globales.ObtenerDatosJSon(sql12, this.getClass() + "-->TablaInformesGerencia");
    }

    public String TablaInformesProduccion(HttpServletRequest request) {

        String fechaini = request.getParameter("fechaini");
        String fechafin = request.getParameter("fechafin");

        sql = "select count( distinct r.ingreso) as cantidad, m.descripcion, "
                + "          (select count(distinct rd.ingreso) "
                + "           from remision_detalle rd inner join equipo e1 on rd.idequipo = e1.id"
                + "	                      inner join magnitudes m1 on m1.id = e1.idmagnitud"
                + "     WHERE m1.id = m.id and to_char(rd.fechaing,'yyyy-MM-dd') >= '" + fechaini + "' and to_char(rd.fechaing,'yyyy-MM-dd') <= '" + fechafin + "') as ingreso"
                + "from remision_detalle r inner join equipo e on r.idequipo = e.id"
                + "                   inner join magnitudes m on m.id = e.idmagnitud"
                + "           WHERE m.id <> 0 and r.cotizado > 0 and to_char(fechaing,'yyyy-MM-dd') >= '" + fechaini + "' and to_char(fechaing,'yyyy-MM-dd') <= '" + fechafin + "'"
                + "          group by m.descripcion, m.id  "
                + "         order by 1 DESC";

        String sql2 = "select count( distinct r.ingreso) as cantidad, m.descripcion, "
                + "  (select count(distinct rd.ingreso) "
                + "   from remision_detalle rd inner join equipo e1 on rd.idequipo = e1.id"
                + "	                      inner join magnitudes m1 on m1.id = e1.idmagnitud"
                + "     WHERE m1.id = m.id and to_char(rd.fechaing,'yyyy-MM-dd') >= '" + fechaini + "' and to_char(rd.fechaing,'yyyy-MM-dd') <= '" + fechafin + "') as ingreso"
                + " from remision_detalle r inner join equipo e on r.idequipo = e.id"
                + "                   inner join magnitudes m on m.id = e.idmagnitud"
                + "          inner join ingreso_plantilla ip on ip.ingreso = r.ingreso"
                + "     WHERE m.id <> 0 and reportado > 0 and to_char(fechaing,'yyyy-MM-dd') >= '" + fechaini + "' and to_char(fechaing,'yyyy-MM-dd') <= '" + fechafin + "'"
                + "    group by m.descripcion, m.id"
                + "   order by 1 DESC";

        String sql3 = "select count( distinct r.ingreso) as cantidad, m.descripcion, "
                + " (select count(distinct rd.ingreso) "
                + "  from remision_detalle rd inner join equipo e1 on rd.idequipo = e1.id"
                + "	                      inner join magnitudes m1 on m1.id = e1.idmagnitud"
                + "     WHERE m1.id = m.id and to_char(rd.fechaing,'yyyy-MM-dd') >= '" + fechaini + "' and to_char(rd.fechaing,'yyyy-MM-dd') <= '" + fechafin + "') as ingreso"
                + " from remision_detalle r inner join equipo e on r.idequipo = e.id"
                + "                    inner join magnitudes m on m.id = e.idmagnitud"
                + "           inner join ingreso_plantilla ip on ip.ingreso = r.ingreso"
                + "  WHERE m.id <> 0 and certificado > 0 and to_char(fechaing,'yyyy-MM-dd') >= '" + fechaini + "' and to_char(fechaing,'yyyy-MM-dd') <= '" + fechafin + "'"
                + "  group by m.descripcion, m.id  "
                + " order by 1 DESC";

        String sql4 = "select count( distinct r.ingreso) as cantidad, m.descripcion,"
                + "  (select count(distinct rd.ingreso) "
                + "  from remision_detalle rd inner join equipo e1 on rd.idequipo = e1.id"
                + "	                      inner join magnitudes m1 on m1.id = e1.idmagnitud"
                + "      WHERE m1.id = m.id and to_char(rd.fechaing,'yyyy-MM-dd') >= '" + fechaini + "' and to_char(rd.fechaing,'yyyy-MM-dd') <= '" + fechafin + "') as ingreso"
                + "from remision_detalle r inner join equipo e on r.idequipo = e.id"
                + "                   inner join magnitudes m on m.id = e.idmagnitud"
                + "          inner join ingreso_plantilla ip on ip.ingreso = r.ingreso"
                + "    WHERE m.id <> 0 and informetecnico > 0 and to_char(fechaing,'yyyy-MM-dd') >= '" + fechaini + "' and to_char(fechaing,'yyyy-MM-dd') <= '" + fechafin + "'"
                + "   group by m.descripcion, m.id  "
                + "  order by 1 DESC";

        String sql5 = "select count( distinct r.ingreso) as cantidad, m.descripcion, "
                + "  (select count(distinct rd.ingreso) "
                + "   from remision_detalle rd inner join equipo e1 on rd.idequipo = e1.id"
                + "                     inner join magnitudes m1 on m1.id = e1.idmagnitud"
                + "  WHERE m1.id = m.id and to_char(rd.fechaing,'yyyy-MM-dd') >= '" + fechaini + "' and to_char(rd.fechaing,'yyyy-MM-dd') <= '" + fechafin + "') as ingreso"
                + "from remision_detalle r inner join equipo e on r.idequipo = e.id"
                + "                   inner join magnitudes m on m.id = e.idmagnitud"
                + " WHERE m.id <> 0 and r.orden > 0 and to_char(fechaing,'yyyy-MM-dd') >= '" + fechaini + "' and to_char(fechaing,'yyyy-MM-dd') <= '" + fechafin + "'"
                + " group by m.descripcion, m.id  "
                + " order by 1 DESC";

        String sql6 = "select count( distinct r.ingreso) as cantidad, m.descripcion, "
                + " (select count(distinct rd.ingreso) "
                + "  from remision_detalle rd inner join equipo e1 on rd.idequipo = e1.id"
                + "	                      inner join magnitudes m1 on m1.id = e1.idmagnitud"
                + "     WHERE m1.id = m.id and to_char(rd.fechaing,'yyyy-MM-dd') >= '" + fechaini + "' and to_char(rd.fechaing,'yyyy-MM-dd') <= '" + fechafin + "') as ingreso"
                + " from remision_detalle r inner join equipo e on r.idequipo = e.id"
                + "                    inner join magnitudes m on m.id = e.idmagnitud"
                + "    WHERE m.id <> 0 and r.facturado > 0 and to_char(fechaing,'yyyy-MM-dd') >= '" + fechaini + "' and to_char(fechaing,'yyyy-MM-dd') <= '" + fechafin + "'"
                + "    group by m.descripcion, m.id  "
                + "    order by 1 DESC";

        String sql7 = "select count( distinct r.ingreso) as cantidad, m.descripcion, "
                + "  (select count(distinct rd.ingreso) "
                + "   from remision_detalle rd inner join equipo e1 on rd.idequipo = e1.id"
                + "	                      inner join magnitudes m1 on m1.id = e1.idmagnitud"
                + "     WHERE m1.id = m.id and to_char(rd.fechaing,'yyyy-MM-dd') >= '" + fechaini + "' and to_char(rd.fechaing,'yyyy-MM-dd') <= '" + fechafin + "') as ingreso"
                + " from remision_detalle r inner join equipo e on r.idequipo = e.id"
                + "                   inner join magnitudes m on m.id = e.idmagnitud"
                + "    WHERE m.id <> 0 and r.recibidoing > 0 and to_char(fechaing,'yyyy-MM-dd') >= '" + fechaini + "' and to_char(fechaing,'yyyy-MM-dd') <= '" + fechafin + "'"
                + "   group by m.descripcion, m.id  "
                + "  order by 1 DESC";

        String sql8 = "select count( distinct r.ingreso) as cantidad, m.descripcion, "
                + "     (select count(distinct rd.ingreso) "
                + "     from remision_detalle rd inner join equipo e1 on rd.idequipo = e1.id"
                + "	                      inner join magnitudes m1 on m1.id = e1.idmagnitud"
                + "     WHERE m1.id = m.id and to_char(rd.fechaing,'yyyy-MM-dd') >= '" + fechaini + "' and to_char(rd.fechaing,'yyyy-MM-dd') <= '" + fechafin + "') as ingreso"
                + " from remision_detalle r inner join equipo e on r.idequipo = e.id"
                + "                    inner join magnitudes m on m.id = e.idmagnitud"
                + "     WHERE m.id <> 0 and r.salida > 0 and to_char(fechaing,'yyyy-MM-dd') >= '" + fechaini + "' and to_char(fechaing,'yyyy-MM-dd') <= '" + fechafin + "'"
                + "     group by m.descripcion, m.id  "
                + "     order by 1 DESC";
        String sql9 = "select count( distinct r.ingreso) as cantidad, m.descripcion, "
                + "  (select count(distinct rd.ingreso) "
                + "   from remision_detalle rd inner join equipo e1 on rd.idequipo = e1.id"
                + "                      inner join magnitudes m1 on m1.id = e1.idmagnitud"
                + " WHERE m1.id = m.id and to_char(rd.fechaing,'yyyy-MM-dd') >= '" + fechaini + "' and to_char(rd.fechaing,'yyyy-MM-dd') <= '" + fechafin + "') as ingreso"
                + " from remision_detalle r inner join equipo e on r.idequipo = e.id"
                + "                    inner join magnitudes m on m.id = e.idmagnitud"
                + "    WHERE m.id <> 0 and r.entregado > 0 and to_char(fechaing,'yyyy-MM-dd') >= '" + fechaini + "' and to_char(fechaing,'yyyy-MM-dd') <= '" + fechafin + "'"
                + "   group by m.descripcion, m.id"
                + "   order by 1 DESC";
        String sql10 = "select count( distinct r.ingreso) as cantidad, m.descripcion, "
                + " (select count(distinct rd.ingreso) "
                + " from remision_detalle rd inner join equipo e1 on rd.idequipo = e1.id"
                + "                      inner join magnitudes m1 on m1.id = e1.idmagnitud"
                + "  WHERE m1.id = m.id and to_char(rd.fechaing,'yyyy-MM-dd') >= '" + fechaini + "' and to_char(rd.fechaing,'yyyy-MM-dd') <= '" + fechafin + "') as ingreso"
                + " from remision_detalle r inner join equipo e on r.idequipo = e.id"
                + "                   inner join magnitudes m on m.id = e.idmagnitud"
                + "    WHERE m.id <> 0 and r.recibidocli > 0 and to_char(fechaing,'yyyy-MM-dd') >= '" + fechaini + "' and to_char(fechaing,'yyyy-MM-dd') <= '" + fechafin + "'"
                + "    group by m.descripcion, m.id "
                + "   order by 1 DESC";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->TablaInformesProduccion") + "|" + Globales.ObtenerDatosJSon(sql2, this.getClass() + "-->TablaInformesProduccion") + "|" + Globales.ObtenerDatosJSon(sql3, this.getClass() + "-->TablaInformesProduccion") + "|"
                + Globales.ObtenerDatosJSon(sql4, this.getClass() + "-->TablaInformesProduccion") + "|" + Globales.ObtenerDatosJSon(sql5, this.getClass() + "-->TablaInformesProduccion") + "|" + Globales.ObtenerDatosJSon(sql6, this.getClass() + "-->TablaInformesProduccion") + "|" + Globales.ObtenerDatosJSon(sql7, this.getClass() + "-->TablaInformesGerencia") + "|"
                + Globales.ObtenerDatosJSon(sql8, this.getClass() + "-->TablaInformesProduccion") + "|" + Globales.ObtenerDatosJSon(sql9, this.getClass() + "-->TablaInformesProduccion") + "|" + Globales.ObtenerDatosJSon(sql10, this.getClass() + "-->TablaInformesProduccion");
    }

    public String RelacionIngreso(HttpServletRequest request) {

        int proveedor = Globales.Validarintnonull(request.getParameter("proveedor"));
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        String fechad = request.getParameter("fechad");
        String fechah = request.getParameter("fechah");
        String sitio = request.getParameter("sitio");
        String garantia = request.getParameter("garantia");
        String convenio = request.getParameter("convenio");
        int magnitud = Globales.Validarintnonull(request.getParameter("magnitud"));
        String equipo = request.getParameter("equipo");
        int marca = Globales.Validarintnonull(request.getParameter("marca"));
        String modelo = request.getParameter("modelo");
        String intervalo = request.getParameter("intervalo");
        int usuariolog = Globales.Validarintnonull(request.getParameter("usuariolog"));
        int usuariocom = Globales.Validarintnonull(request.getParameter("usuariocom"));
        int usuariofac = Globales.Validarintnonull(request.getParameter("usuariofac"));

        String busqueda = "WHERE 1=1 ";

        String fechaing = " AND to_char(rd.fechaing,'yyyy-MM-dd') >= '" + fechad + "' and to_char(rd.fechaing,'yyyy-MM-dd') <= '" + fechah + "' and remision > 0";
        String fechafac = " AND to_char(f.fecha,'yyyy-MM-dd') >= '" + fechad + "' and to_char(f.fecha,'yyyy-MM-dd') <= '" + fechah + "' and f.factura > 0 and (case when f.estado <> 'Anulado' then 1 else case when to_char(f.fechaanula,'yyyy-MM') > to_char(f.fecha,'yyyy-MM') then 1 else 0 end end) = 1 ";
        String fechacot = " AND to_char(c.fecha,'yyyy-MM-dd') >= '" + fechad + "' and to_char(c.fecha,'yyyy-MM-dd') <= '" + fechah + "' and c.cotizacion > 0";
        String fechaord = " AND to_char(c.fecha,'yyyy-MM-dd') >= '" + fechad + "' and to_char(c.fecha,'yyyy-MM-dd') <= '" + fechah + "' and c.orden > 0";
        if (cliente > 0) {
            fechacot += " AND rd.idcliente = " + cliente;
            fechaing += " AND rd.idcliente = " + cliente;
            fechafac += " AND f.idcliente = " + cliente;
            fechaord += " AND rd.idcliente = " + cliente;
        }

        if (magnitud > 0) {
            busqueda += " AND e.idmagnitud = " + magnitud;
        }
        if (!equipo.equals("")) {
            busqueda += " AND e.descripcion = '" + equipo + "'";
        }
        if (marca > 0) {
            busqueda += " AND mo.idmarca = " + marca;
        }
        if (!modelo.equals("")) {
            busqueda += " AND mo.descripcion = '" + modelo + "'";
        }
        if (!intervalo.equals("")) {
            busqueda += " AND '(' || i.desde || ' a ' || i.hasta || ') ' || i.medida = '" + intervalo + "'";
        }
        if (!sitio.equals("")) {
            busqueda += " AND rd.sitio = '" + sitio + "'";
        }
        if (!garantia.equals("")) {
            busqueda += " AND rd.garantia = '" + garantia + "'";
        }
        if (!convenio.equals("")) {
            busqueda += " AND rd.convenio = " + convenio;
        }
        if (usuariolog > 0) {
            fechaing += " AND r.idusuario = " + usuariolog;
        }
        if (usuariofac > 0) {
            fechaing += " AND f.idusuario = " + usuario;
            fechaord += " and c.idusuario = " + usuariofac;
        }

        if (usuariocom > 0) {
            fechacot += " AND c.idusuario = " + usuariocom;
        }
        if (proveedor > 0) {
            fechaord += " AND c.idproveedor = " + proveedor;
        }

        String[] sqls = new String[15];

        sqls[0] = "select to_char(c.fecha,'yyyy/MM') as fecha, sum((rd.precio*rd.cantidad) - (rd.precio*rd.cantidad*rd.descuento/100))::double precision as valor, m.abreviatura as magnitud"
                + "  from cotizacion_detalle rd inner join equipo e on e.id = rd.idequipo"
                + "                        inner join cotizacion c on c.id = rd.idcotizacion"
                + "                        inner join magnitudes m on m.id = e.idmagnitud " + busqueda + fechacot + " and c.estado not in('Anulado','Rechazado','Reemplazado') "
                + " group by to_char(c.fecha,'yyyy/MM'), m.abreviatura "
                + " order by 1";
        sqls[1] = "select to_char(c.fecha,'yyyy/MM') as fecha, sum(rd.cantidad) as valor, m.abreviatura as magnitud"
                + " from cotizacion_detalle rd inner join equipo e on e.id = rd.idequipo"
                + "                       inner join cotizacion c on c.id = rd.idcotizacion"
                + "                      inner join magnitudes m on m.id = e.idmagnitud " + busqueda + fechacot + " and c.estado not in('Anulado','Rechazado','Reemplazado') "
                + " group by to_char(c.fecha,'yyyy/MM'), m.abreviatura "
                + " order by 1";
        sqls[2] = "select to_char(rd.fechaing,'yyyy/MM') as fecha, m.abreviatura as magnitud,"
                + "                sum(coalesce(case when tipoprecio = 1 and rd.idintervalo > 0 then (select max(precio1) from servicio_precios sp where sp.idintervalo = rd.idintervalo) else "
                + "		            (select max(precio1) from servicio_precios sp where sp.idmarca = mo.idmarca and sp.idmodelo = rd.idmodelo and rd.idequipo = sp.idequipo and idproveedor = 0) end,140000)) as valor"
                + "       from remision_detalle rd inner join equipo e on e.id = rd.idequipo"
                + "                       inner join remision r on r.id = rd.idremision"
                + "                               inner join magnitudes m on m.id = e.idmagnitud"
                + "                               inner join modelos mo on mo.id = rd.idmodelo " + busqueda + fechaing + " "
                + "      group by to_char(rd.fechaing,'yyyy/MM'), m.abreviatura "
                + "     order by 1";

        sqls[3] = "select to_char(rd.fechaing,'yyyy/MM') as fecha, count(distinct rd.ingreso) as valor, m.abreviatura as magnitud"
                + " from remision_detalle rd inner join equipo e on e.id = rd.idequipo"
                + "                 inner join remision r on r.id = rd.idremision"
                + "                        inner join magnitudes m on m.id = e.idmagnitud"
                + "                        inner join modelos mo on mo.id = rd.idmodelo " + busqueda + fechaing + " "
                + " group by to_char(rd.fechaing,'yyyy/MM'), m.abreviatura "
                + " order by 1";
        sqls[4] = "select to_char(f.fecha,'yyyy/MM') as fecha, sum(fd.subtotal) as valor, m.abreviatura as magnitud"
                + "   from factura_datelle fd inner join remision_detalle rd on rd.ingreso = fd.ingreso"
                + "                        inner join equipo e on e.id = rd.idequipo"
                + "                        inner join factura f on f.id = fd.idfactura"
                + "                        inner join magnitudes m on m.id = e.idmagnitud " + busqueda + fechafac + " and fd.descripcion not like 'AJUSTE%' AND  fd.descripcion not like 'SUMINISTRO%' AND  fd.descripcion not like 'MANTENIMIENTO%'"
                + "group by to_char(f.fecha,'yyyy/MM'), m.abreviatura "
                + "union"
                + "select to_char(f.fecha,'yyyy/MM') as fecha, sum(imf.subtotal-imf.descuento) as valor, m.abreviatura as magnitud"
                + "from importado_factura imf inner join remision_detalle rd on rd.ingreso = imf.ingreso"
                + "           inner join factura f on f.factura = imf.factura"
                + "                      inner join equipo e on e.id = rd.idequipo"
                + "                      inner join magnitudes m on m.id = e.idmagnitud " + busqueda + fechafac + " and imf.subtotal > 0 "
                + " group by to_char(f.fecha,'yyyy/MM'), m.abreviatura  "
                + " order by 1";
        sqls[5] = "select to_char(f.fecha,'yyyy/MM') as fecha, sum(fd.cantidad) as valor, m.abreviatura as magnitud"
                + "  from factura_datelle fd inner join remision_detalle rd on rd.ingreso = fd.ingreso"
                + "                        inner join equipo e on e.id = rd.idequipo"
                + "                        inner join factura f on f.id = fd.idfactura"
                + "                        inner join magnitudes m on m.id = e.idmagnitud " + busqueda + fechafac + "  and  fd.descripcion not like 'AJUSTE%' AND  fd.descripcion not like 'SUMINISTRO%' AND  fd.descripcion not like 'MANTENIMIENTO%'"
                + " group by to_char(f.fecha,'yyyy/MM'), m.abreviatura "
                + " union"
                + " select to_char(f.fecha,'yyyy/MM') as fecha, count(distinct rd.ingreso) as valor, m.abreviatura as magnitud"
                + " from importado_factura imf inner join remision_detalle rd on rd.ingreso = imf.ingreso"
                + "           inner join factura f on f.factura = imf.factura"
                + "                     inner join equipo e on e.id = rd.idequipo"
                + "                    inner join magnitudes m on m.id = e.idmagnitud " + busqueda + fechafac + " "
                + " group by to_char(f.fecha,'yyyy/MM'), m.abreviatura "
                + " order by 1";

        sqls[6] = "select to_char(f.fecha,'yyyy/MM') as fecha, sum(fd.subtotal) as valor, m.abreviatura as magnitud"
                + "  from factura_datelle fd inner join remision_detalle rd on rd.ingreso = fd.ingreso"
                + "                        inner join equipo e on e.id = rd.idequipo"
                + "                        inner join factura f on f.id = fd.idfactura"
                + "                        inner join magnitudes m on m.id = e.idmagnitud " + busqueda + fechafac + " and  (fd.descripcion like 'AJUSTE%' OR fd.descripcion like 'MANTENIMIENTO%') "
                + "group by to_char(f.fecha,'yyyy/MM'), m.abreviatura "
                + "union"
                + "select to_char(f.fecha,'yyyy/MM') as fecha, sum(imf.ajuste) as valor, m.abreviatura as magnitud"
                + "from importado_factura imf inner join remision_detalle rd on rd.ingreso = imf.ingreso"
                + "            inner join factura f on f.factura = imf.factura"
                + "                       inner join equipo e on e.id = rd.idequipo"
                + "                        inner join magnitudes m on m.id = e.idmagnitud " + busqueda + fechafac + " and imf.ajuste > 0 "
                + "group by to_char(f.fecha,'yyyy/MM'), m.abreviatura "
                + "order by 1";
        sqls[7] = "select to_char(f.fecha,'yyyy/MM') as fecha, sum(fd.cantidad) as valor, m.abreviatura as magnitud"
                + " from factura_datelle fd inner join remision_detalle rd on rd.ingreso = fd.ingreso"
                + "                        inner join equipo e on e.id = rd.idequipo"
                + "                        inner join factura f on f.id = fd.idfactura"
                + "                        inner join magnitudes m on m.id = e.idmagnitud " + busqueda + fechafac + " AND (fd.descripcion like 'AJUSTE%' OR fd.descripcion like 'MANTENIMIENTO%')"
                + "group by to_char(f.fecha,'yyyy/MM'), m.abreviatura "
                + "union"
                + "select to_char(f.fecha,'yyyy/MM') as fecha, count(distinct rd.ingreso) as valor, m.abreviatura as magnitud"
                + "from importado_factura imf inner join remision_detalle rd on rd.ingreso = imf.ingreso"
                + "           inner join factura f on f.factura = imf.factura"
                + "                      inner join equipo e on e.id = rd.idequipo "
                + "                      inner join magnitudes m on m.id = e.idmagnitud " + busqueda + fechafac + " and imf.ajuste > 0 "
                + "group by to_char(f.fecha,'yyyy/MM'), m.abreviatura  "
                + "order by 1";
        sqls[8] = "select to_char(f.fecha,'yyyy/MM') as fecha, sum(fd.subtotal) as valor, m.abreviatura as magnitud"
                + "  from factura_datelle fd inner join remision_detalle rd on rd.ingreso = fd.ingreso"
                + "                        inner join equipo e on e.id = rd.idequipo"
                + "                        inner join factura f on f.id = fd.idfactura"
                + "                        inner join magnitudes m on m.id = e.idmagnitud " + busqueda + fechafac + " and  fd.descripcion like 'SUMINISTRO%'"
                + "group by to_char(f.fecha,'yyyy/MM'), m.abreviatura "
                + "union"
                + "select to_char(f.fecha,'yyyy/MM') as fecha, sum(imf.suministro) as valor, m.abreviatura as magnitud"
                + "from importado_factura imf inner join remision_detalle rd on rd.ingreso = imf.ingreso"
                + "            inner join factura f on f.factura = imf.factura"
                + "                        inner join equipo e on e.id = rd.idequipo"
                + "                        inner join magnitudes m on m.id = e.idmagnitud " + busqueda + fechafac + " and imf.suministro > 0 "
                + "group by to_char(f.fecha,'yyyy/MM'), m.abreviatura  "
                + "order by 1";
        sqls[9] = "select to_char(f.fecha,'yyyy/MM') as fecha, sum(fd.cantidad) as valor, m.abreviatura as magnitud"
                + "  from factura_datelle fd inner join remision_detalle rd on rd.ingreso = fd.ingreso"
                + "                        inner join equipo e on e.id = rd.idequipo"
                + "                        inner join factura f on f.id = fd.idfactura"
                + "                        inner join magnitudes m on m.id = e.idmagnitud " + busqueda + fechafac + " and  fd.descripcion like 'SUMINISTRO%'"
                + "group by to_char(f.fecha,'yyyy/MM'), m.abreviatura "
                + "union"
                + "select to_char(f.fecha,'yyyy/MM') as fecha, count(distinct rd.ingreso) as valor, m.abreviatura as magnitud"
                + "from importado_factura imf inner join remision_detalle rd on rd.ingreso = imf.ingreso"
                + "            inner join factura f on f.factura = imf.factura"
                + "                        inner join equipo e on e.id = rd.idequipo"
                + "                        inner join magnitudes m on m.id = e.idmagnitud " + busqueda + fechafac + " and imf.suministro > 0 "
                + "group by to_char(f.fecha,'yyyy/MM'), m.abreviatura "
                + "order by 1";
        sqls[10] = "select to_char(c.fecha,'yyyy/MM') as fecha, sum(cd.subtotal) as valor, m.abreviatura as magnitud"
                + "from ordencompter_detalle cd inner join remision_detalle rd on rd.ingreso = cd.ingreso"
                + "                       inner join equipo e on e.id = rd.idequipo"
                + "                       inner join ordencompra_tercero c on c.id = cd.idorden"
                + "                       inner join magnitudes m on m.id = e.idmagnitud " + busqueda + fechaord + " and c.estado <> 'Anulado'"
                + "group by to_char(c.fecha,'yyyy/MM'), m.abreviatura "
                + "order by 1";

        sqls[11] = "select to_char(c.fecha,'yyyy/MM') as fecha, count(distinct rd.ingreso) as valor, m.abreviatura as magnitud"
                + " from ordencompter_detalle cd inner join remision_detalle rd on rd.ingreso = cd.ingreso"
                + "                        inner join equipo e on e.id = rd.idequipo"
                + "                        inner join ordencompra_tercero c on c.id = cd.idorden"
                + "                        inner join magnitudes m on m.id = e.idmagnitud " + busqueda + fechaord + " and c.estado <> 'Anulado' "
                + "group by to_char(c.fecha,'yyyy/MM'), m.abreviatura "
                + "order by 1";
        sqls[12] = "select to_char(rd.fechaing,'yyyy/MM') as fecha"
                + " from remision_detalle rd inner join equipo e on e.id = rd.idequipo"
                + "                inner join remision r on r.id = rd.idremision"
                + "                        inner join magnitudes m on m.id = e.idmagnitud"
                + "                        inner join modelos mo on mo.id = rd.idmodelo " + busqueda + fechaing + " "
                + "group by to_char(rd.fechaing,'yyyy/MM')"
                + "order by 1";
        sqls[13] = "select to_char(f.fecha,'yyyy/MM') as fecha, sum(imf.subtotal-imf.descuento) as valor "
                + " from importado_factura imf inner join remision_detalle rd on rd.ingreso = imf.ingreso"
                + "             inner join factura f on f.factura = imf.factura"
                + "                        inner join equipo e on e.id = rd.idequipo "
                + "                        inner join magnitudes m on m.id = e.idmagnitud " + busqueda + fechafac + " and imf.subtotal > 0"
                + "group by to_char(f.fecha,'yyyy/MM') "
                + "order by 1";
        sqls[14] = "select to_char(f.fecha,'yyyy/MM') as fecha, count(distinct rd.ingreso) as valor"
                + " from importado_factura imf inner join remision_detalle rd on rd.ingreso = imf.ingreso"
                + "            inner join factura f on f.factura = imf.factura"
                + "                        inner join equipo e on e.id = rd.idequipo"
                + "                        inner join magnitudes m on m.id = e.idmagnitud " + busqueda + fechafac + " and imf.subtotal > 0 "
                + " group by to_char(f.fecha,'yyyy/MM')"
                + " order by 1";
        
        String resultado = "{";
        String Tabla = "";
        for (int x = 0; x < sqls.length; x++) {
            if (!resultado.equals("{")) {
                resultado += ",";
            }
            if (x == 0) {
                Tabla = "Table";
            } else {
                Tabla = "Table" + x;
            }
            resultado += "\"" + Tabla + "\":" + Globales.ObtenerDatosJSon(sqls[x], this.getClass() + "-->AlertaLaboratorio");
        }
        resultado += "}";
        return resultado;
    }

    public String DetRelacionIngreso(HttpServletRequest request) {
        int proveedor = Globales.Validarintnonull(request.getParameter("proveedor"));
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        String fecha = request.getParameter("fecha");
        String sitio = request.getParameter("sitio");
        String garantia = request.getParameter("garantia");
        String convenio = request.getParameter("convenio");
        String magnitudes = request.getParameter("magnitudes");
        String equipo = request.getParameter("equipo");
        int marca = Globales.Validarintnonull(request.getParameter("marca"));
        String modelo = request.getParameter("modelo");
        String intervalo = request.getParameter("intervalo");
        int usuariolog = Globales.Validarintnonull(request.getParameter("usuariolog"));
        int usuariocom = Globales.Validarintnonull(request.getParameter("usuariocom"));
        int usuariofac = Globales.Validarintnonull(request.getParameter("usuariofac"));
        String opcion = request.getParameter("opcion");

        String busqueda = "WHERE m.abreviatura = '" + magnitudes + "'";

        String fechaing = " AND to_char(rd.fechaing,'yyyy/MM') = '" + fecha + "' and remision > 0";
        String fechafac = " AND to_char(f.fecha,'yyyy/MM') = '" + fecha + "' and f.factura > 0 ";
        String fechacot = " AND to_char(c.fecha,'yyyy/MM') = '" + fecha + "' and cotizacion > 0";
        String fechaord = " AND to_char(c.fecha,'yyyy/MM') = '" + fecha + "' and orden > 0";
        if (cliente > 0) {
            busqueda += " AND r.idcliente = " + cliente;
        }
        if (!equipo.equals("")) {
            busqueda += " AND e.descripcion = '" + equipo + "'";
        }
        if (marca > 0) {
            busqueda += " AND mo.idmarca = " + marca;
        }
        if (!modelo.equals("")) {
            busqueda += " AND mo.descripcion = '" + modelo + "'";
        }
        if (!intervalo.equals("")) {
            busqueda += " AND '(' || i.desde || ' a ' || i.hasta || ') ' || i.medida = '" + intervalo + "'";
        }
        if (!sitio.equals("")) {
            busqueda += " AND rd.sitio = '" + sitio + "'";
        }
        if (!garantia.equals("")) {
            busqueda += " AND rd.garantia = '" + garantia + "'";
        }
        if (!convenio.equals("")) {
            busqueda += " AND rd.convenio = " + convenio;
        }
        if (usuariolog > 0) {
            fechaing += " AND r.idusuario = " + usuariolog;
        }
        if (usuariofac > 0) {
            fechaing += " AND f.idusuario = " + usuario;
            fechaord += " and c.idusuario = " + usuariofac;
        }

        if (usuariocom > 0) {
            fechacot += " AND c.idusuario = " + usuariocom;
        }
        if (proveedor > 0) {
            fechaord += " AND c.idproveedor = " + proveedor;
        }

        switch (opcion) {
            case "Cotizados":
                sql = "select '<a href=''javascript:ImprimirCotizacion(' || cotizacion || ')''>' || cotizacion || '</a>' as control,"
                        + "to_char(c.fecha,'yyyy/MM/dd HH24:MI') as fecha, sum((rd.precio*rd.cantidad) - (rd.precio*rd.cantidad*rd.descuento/100))::double precision as valor, "
                        + "sum(rd.cantidad) as cantidad, rd.ingreso, m.abreviatura as magnitud, cli.nombrecompleto as cliente, e.descripcion as equipo"
                        + "from cotizacion_detalle rd inner join equipo e on e.id = rd.idequipo"
                        + "                       inner join cotizacion c on c.id = rd.idcotizacion"
                        + "           inner join clientes cli on cli.id = c.idcliente"
                        + "                       inner join magnitudes m on m.id = e.idmagnitud " + busqueda + fechacot + " and c.estado not in('Anulado','Rechazado','Reemplazado') "
                        + "group by c.fecha, m.abreviatura, rd.ingreso, c.cotizacion, cli.nombrecompleto, e.descripcion "
                        + "order by 1";
                break;
            case "Ingresados":
                sql = "select  '<a href=''javascript:ImprimirRemision(' || remision || ')''>' || remision || '</a>' as control, "
                        + "  to_char(rd.fechaing,'yyyy/MM/dd HH24:MI') as fecha, m.abreviatura as magnitud,"
                        + "  1 as cantidad, rd.ingreso,"
                        + "             coalesce(case when tipoprecio = 1 and rd.idintervalo > 0 then (select max(precio1) from servicio_precios sp where sp.idintervalo = rd.idintervalo) else"
                        + "	            (select max(precio1) from servicio_precios sp where sp.idmarca = mo.idmarca and sp.idmodelo = rd.idmodelo and rd.idequipo = sp.idequipo and idproveedor = 0) end,140000) as valor,"
                        + "   cli.nombrecompleto as cliente, e.descripcion as equipo"
                        + " from remision_detalle rd inner join equipo e on e.id = rd.idequipo"
                        + "                 inner join remision r on r.id = rd.idremision"
                        + "                 inner join clientes cli on cli.id = r.idcliente"
                        + "                         inner join magnitudes m on m.id = e.idmagnitud"
                        + "                         inner join modelos mo on mo.id = rd.idmodelo " + busqueda + fechaing + " "
                        + "group by rd.fechaing, m.abreviatura, rd.ingreso, remision, cli.nombrecompleto, tipoprecio, rd.idintervalo, mo.idmarca, rd.idequipo, rd.idmodelo, e.descripcion "
                        + "order by 1";
                break;
            case "Facturados":
                sql = "select '<a href=''javascript:ImprimirFactura(' || f.factura || ')''>' || f.factura || '</a>' as control, "
                        + "      to_char(f.fecha,'yyyy/MM/dd') as fecha, sum(fd.subtotal) as valor, m.abreviatura as magnitud,"
                        + "      sum(fd.cantidad) as cantidad, fd.ingreso, cli.nombrecompleto as cliente, e.descripcion as equipo"
                        + "from factura_datelle fd inner join remision_detalle rd on rd.ingreso = fd.ingreso"
                        + "                      inner join equipo e on e.id = rd.idequipo"
                        + "                      inner join factura f on f.id = fd.idfactura"
                        + "         inner join clientes cli on cli.id = f.idcliente"
                        + "                    inner join magnitudes m on m.id = e.idmagnitud " + busqueda + fechafac + " and f.estado <> 'Anulado' and fd.descripcion not like 'AJUSTE%' AND  fd.descripcion not like 'SUMINISTRO%' AND  fd.descripcion not like 'MANTENIMIENTO%'"
                        + "group by f.fecha, m.abreviatura, fd.ingreso, cli.nombrecompleto, f.factura, e.descripcion "
                        + "union"
                        + "select '<a href=''javascript:ImprimirFactura(' || f.factura || ')''>' || f.factura || '</a>' as control, "
                        + "to_char(f.fecha,'yyyy/MM/dd HH24:MI') as fecha, sum(imf.subtotal) as valor, m.abreviatura as magnitud,"
                        + "    1 as cantidad, imf.ingreso, cli.nombrecompleto as cliente, e.descripcion as equipo"
                        + "from importado_factura imf inner join remision_detalle rd on rd.ingreso = imf.ingreso"
                        + "           inner join factura f on f.factura = imf.factura"
                        + "           inner join clientes cli on cli.id = f.idcliente"
                        + "                      inner join equipo e on e.id = rd.idequipo"
                        + "                      inner join magnitudes m on m.id = e.idmagnitud " + busqueda + fechafac + " and imf.subtotal > 0 and f.estado <> 'Anulado' "
                        + "group by f.fecha, m.abreviatura, cli.nombrecompleto, f.factura, imf.ingreso, e.descripcion   "
                        + "order by 1";
                break;
        }
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->DetRelacionIngreso");
    }

    public String RegistroRevision(HttpServletRequest request) {
        int magnitud = Globales.Validarintnonull(request.getParameter("proveedor"));
        String fechaini = request.getParameter("fechaini");
        String fechafin = request.getParameter("fechafin");
        String TrabajoNoConforme = request.getParameter("TrabajoNoConforme");
        String AccionesPreventivas = request.getParameter("AccionesPreventivas");
        String AuditoriasInternas = request.getParameter("AuditoriasInternas");
        String AuditoriasExternas = request.getParameter("AuditoriasExternas");
        String OportunidadesMejora = request.getParameter("OportunidadesMejora");
        String Sugerencias = request.getParameter("Sugerencias");
        int ultimodia = Globales.Validarintnonull(request.getParameter("ultimodia"));

        sql = "SELECT id"
                + "          FROM revisiones "
                + "     WHERE idmagnitud = " + magnitud + " and to_char(fecha,'yyyy-MM-dd') = '" + fechaini + "'";
        int id = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
        if (id == 0) {

            String[] _fec1 = fechaini.split("-");
            String[] _fec2 = fechafin.split("-");
            if (!_fec1[1].equals(_fec2[1]) || !_fec1[0].equals(_fec2[0])) {
                return "1|Recuerde que debe seleccionar las fechas del mismo mes";
            }
            
            if (fechaini.compareTo(Globales.FechaActual(1)) > 0) {
                return "1|Todabía no puede generar el informe de las revisiones por las magnitudes";
            }

            if (Globales.Validarintnonull(_fec1[2]) != 1) {
                return "1|Recuerde que el días de la fecha inicial debe ser 1";
            }

            if (ultimodia != Globales.Validarintnonull(_fec2[2])) {
                return "1|Recuerde que el días de la fecha final debe ser " + ultimodia;
            }

            int contador = Globales.Validarintnonull(Globales.ObtenerUnValor("select revision from contadores")) + 1;
            sql = "INSERT INTO revisiones(revision, idmagnitud, fecha, idusuario, trabajonoconforme, "
                    + "  accionespreventivas, auditoriainterna, auditoriaexterna, oportunidadmejora, sugerencias)"
                    + "  VALUES (" + contador + "," + magnitud + ",'" + fechaini + "'," + idusuario + ",'" + TrabajoNoConforme + "','" + AccionesPreventivas + "','" + AuditoriasInternas + "','" + AuditoriasExternas + "','" + OportunidadesMejora + "','" + Sugerencias + "');"
                    + "  update contadores set revision=" + contador;

            if (Globales.DatosAuditoria(sql, "REVISION POR LAS MAGNITUDES", "AGREGAR", idusuario, iplocal, this.getClass() + "-->RegistroRevision")) {

                return "0|Revisión por las magnitudes registrada con éxito";
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }

        } else {
            sql = "UPDATE public.revisiones"
                    + "  SET fechareg=now(), idusuario=" + idusuario
                    + ",trabajonoconforme='" + TrabajoNoConforme + "', accionespreventivas='" + AccionesPreventivas + "', auditoriainterna='" + AuditoriasInternas
                    + "',auditoriaexterna='" + AuditoriasExternas + "', oportunidadmejora='" + OportunidadesMejora + "', sugerencias='" + Sugerencias + "'"
                    + " WHERE id = " + id;

            if (Globales.DatosAuditoria(sql, "REVISION POR LAS MAGNITUDES", "EDITAR", idusuario, iplocal, this.getClass() + "-->RegistroRevision")) {

                return "0|Revisión por las magnitudes actualizada con éxito";
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }

        }
    }

    /*
        public String RpInformeDireccion(HttpServletRequest request)
        {
           int magnitud= Globales.Validarintnonull(request.getParameter("magnitud"));
           String fechaini=request.getParameter("fechaini");
           String fechafin=request.getParameter("fechafin");
            String reporte = "RevisionDireccion";

            String[] sqls = new String[13];

            sqls[0] = "SELECT 'INF-0001-' || revision || '-PT' as revision,"+
                      "      fecha, idusuario, trabajonoconforme, "+
                       "     accionespreventivas, auditoriainterna, auditoriaexterna, oportunidadmejora,"+ 
                       "     sugerencias, m.descripcion as magnitud, u.nombrecompleto as usuario, u.idusu, u.cargo"+
                       " FROM magnitudes m left join revisiones r on m.id = r.idmagnitud"+
			 "                                    left join seguridad.rbac_usuario u on u.idusu = r.idusuario"+
                   " WHERE m.id = " + magnitud + " and to_char(fecha,'yyyy-MM-dd') = '" + fechaini + "'";

            sqls[1] = "select count( distinct r.ingreso) as cantidad, e.descripcion  as descripcion"+
	               "         ,(select count(distinct r2.ingreso) "+
	               "           from remision_detalle r2 inner join equipo e2 on r2.idequipo = e2.id "+
	               "           where e2.idmagnitud = e.idmagnitud and to_char(r2.fechaing,'yyyy-MM-dd') >= '" + fechaini + "' and to_char(r2.fechaing,'yyyy-MM-dd') <= '" + fechafin + "' "+
	               "           group by e2.descripcion "+
	               "           order by 1 desc limit 1) as maxcantidad"+
	               "          ,(select count(distinct r2.ingreso)"+ 
	               "           from remision_detalle r2 inner join equipo e2 on r2.idequipo = e2.id "+
	               "           where e2.idmagnitud = e.idmagnitud and to_char(r2.fechaing,'yyyy-MM-dd') >= '" + fechaini + "' and to_char(r2.fechaing,'yyyy-MM-dd') <= '" + fechafin + "' "+
	               "           group by e2.descripcion "+
	               "           order by 1 limit 1) as mincantidad"+
	               "           ,(select e2.descripcion"+
	               "           from remision_detalle r2 inner join equipo e2 on r2.idequipo = e2.id "+
	               "           where e2.idmagnitud = e.idmagnitud and to_char(r2.fechaing,'yyyy-MM-dd') >= '" + fechaini + "' and to_char(r2.fechaing,'yyyy-MM-dd') <= '" + fechafin + "' "+
	               "           group by e2.descripcion "+
	               "           order by count(distinct r2.ingreso) desc limit 1) as maxdescripcion"+
	               "          ,(select e2.descripcion"+
	               "           from remision_detalle r2 inner join equipo e2 on r2.idequipo = e2.id "+
	               "           where e2.idmagnitud = e.idmagnitud and to_char(r2.fechaing,'yyyy-MM-dd') >= '" + fechaini + "' and to_char(r2.fechaing,'yyyy-MM-dd') <= '" + fechafin + "'"+ 
	               "           group by e2.descripcion "+
	               "           order by count(distinct r2.ingreso) limit 1) as mindescripcion"+
                    "from remision_detalle r inner join equipo e on r.idequipo = e.id"+
			"                    inner join modelos mo on mo.id = r.idmodelo"+  
			"                    inner join marcas ma on ma.id = mo.idmarca "+
			"                    inner join magnitud_intervalos i on i.id = r.idintervalo"+
			"                    inner join clientes c on c.id = r.idcliente"+
                 "   WHERE e.idmagnitud = " + magnitud + " and to_char(fechaing,'yyyy-MM-dd') >= '" + fechaini + "' and to_char(fechaing,'yyyy-MM-dd') <= '" + fechafin + "'"+
                 "   group by e.descripcion, e.idmagnitud "+ 
                 "   order by 1 desc limit 12";

            sqls[2] = "select count( distinct r.ingreso) as cantidad, ma.descripcion  as descripcion"+
	              "          ,(select count(distinct r2.ingreso) "+
	              "            from remision_detalle r2 inner join equipo e2 on r2.idequipo = e2.id "+
			"				   inner join modelos mo2 on mo2.id = r2.idmodelo "+ 
			"				   inner join marcas ma2 on ma2.id = mo2.idmarca"+ 
	                "          where e2.idmagnitud = e.idmagnitud and to_char(r2.fechaing,'yyyy-MM-dd') >= '" + fechaini + "' and to_char(r2.fechaing,'yyyy-MM-dd') <= '" + fechafin + "' "+
	                "          group by ma2.descripcion "+
	                "          order by 1 desc limit 1) as maxcantidad"+
	                "         ,(select count(distinct r2.ingreso)"+ 
	                 "         from remision_detalle r2 inner join equipo e2 on r2.idequipo = e2.id "+
			"				   inner join modelos mo2 on mo2.id = r2.idmodelo  "+
			"				   inner join marcas ma2 on ma2.id = mo2.idmarca"+ 
	                "          where e2.idmagnitud = e.idmagnitud and to_char(r2.fechaing,'yyyy-MM-dd') >= '" + fechaini + "' and to_char(r2.fechaing,'yyyy-MM-dd') <= '" + fechafin + "' "+
	                "          group by ma2.descripcion "+
	                "          order by 1 limit 1) as mincantidad"+
	                "          ,(select ma2.descripcion"+
	                "          from remision_detalle r2 inner join equipo e2 on r2.idequipo = e2.id "+
			"				   inner join modelos mo2 on mo2.id = r2.idmodelo  "+
			"				   inner join marcas ma2 on ma2.id = mo2.idmarca"+ 
	                "          where e2.idmagnitud = e.idmagnitud and to_char(r2.fechaing,'yyyy-MM-dd') >= '" + fechaini + "' and to_char(r2.fechaing,'yyyy-MM-dd') <= '" + fechafin + "' "+
	                "          group by ma2.descripcion "+
	                "          order by count(distinct r2.ingreso) desc limit 1) as maxdescripcion"+
	                "         ,(select ma2.descripcion"+
	                "          from remision_detalle r2 inner join equipo e2 on r2.idequipo = e2.id"+ 
			"				   inner join modelos mo2 on mo2.id = r2.idmodelo "+ 
			"				   inner join marcas ma2 on ma2.id = mo2.idmarca "+
	                 "         where e2.idmagnitud = e.idmagnitud and to_char(r2.fechaing,'yyyy-MM-dd') >= '" + fechaini + "' and to_char(r2.fechaing,'yyyy-MM-dd') <= '" + fechafin + "' "+
	                 "         group by ma2.descripcion"+ 
	                 "         order by count(distinct r2.ingreso) limit 1) as mindescripcion"+
                    "from remision_detalle r inner join equipo e on r.idequipo = e.id"+
			"                    inner join modelos mo on mo.id = r.idmodelo "+ 
			"                    inner join marcas ma on ma.id = mo.idmarca "+
			"                    inner join magnitud_intervalos i on i.id = r.idintervalo"+
			"                    inner join clientes c on c.id = r.idcliente"+
                    "WHERE e.idmagnitud = " + magnitud + " and to_char(fechaing,'yyyy-MM-dd') >= '" + fechaini + "' and to_char(fechaing,'yyyy-MM-dd') <= '" + fechafin + "'"+
                    "group by ma.descripcion, e.idmagnitud "+
                    "order by 1 desc limit 12";

            sqls[3] = "select count( distinct r.ingreso) as cantidad, mo.descripcion  as descripcion"+
	                "        ,(select count(distinct r2.ingreso) "+
	                "          from remision_detalle r2 inner join equipo e2 on r2.idequipo = e2.id "+
			"				   inner join modelos mo2 on mo2.id = r2.idmodelo  "+
			"				   inner join marcas ma2 on ma2.id = mo2.idmarca "+
	                "          where e2.idmagnitud = e.idmagnitud and to_char(r2.fechaing,'yyyy-MM-dd') >= '" + fechaini + "' and to_char(r2.fechaing,'yyyy-MM-dd') <= '" + fechafin + "' "+
	                "          group by mo2.descripcion "+
	                "          order by 1 desc limit 1) as maxcantidad"+
	                "         ,(select count(distinct r2.ingreso) "+
	                "          from remision_detalle r2 inner join equipo e2 on r2.idequipo = e2.id "+
			"				   inner join modelos mo2 on mo2.id = r2.idmodelo  "+
			"				   inner join marcas ma2 on ma2.id = mo2.idmarca"+ 
	                "          where e2.idmagnitud = e.idmagnitud and to_char(r2.fechaing,'yyyy-MM-dd') >= '" + fechaini + "' and to_char(r2.fechaing,'yyyy-MM-dd') <= '" + fechafin + "'"+ 
	                "          group by mo2.descripcion "+
	                "          order by 1 limit 1) as mincantidad"+
	                "          ,(select mo2.descripcion"+
	                "          from remision_detalle r2 inner join equipo e2 on r2.idequipo = e2.id "+
			"				   inner join modelos mo2 on mo2.id = r2.idmodelo  "+
			"				   inner join marcas ma2 on ma2.id = mo2.idmarca "+
	                "          where e2.idmagnitud = e.idmagnitud and to_char(r2.fechaing,'yyyy-MM-dd') >= '" + fechaini + "' and to_char(r2.fechaing,'yyyy-MM-dd') <= '" + fechafin + "' "+
	                "          group by mo2.descripcion "+
	                "          order by count(distinct r2.ingreso) desc limit 1) as maxdescripcion"+
	                "         ,(select mo2.descripcion"+
	                 "         from remision_detalle r2 inner join equipo e2 on r2.idequipo = e2.id "+
			"				   inner join modelos mo2 on mo2.id = r2.idmodelo "+ 
			"				   inner join marcas ma2 on ma2.id = mo2.idmarca"+ 
	                      "    where e2.idmagnitud = e.idmagnitud and to_char(r2.fechaing,'yyyy-MM-dd') >= '" + fechaini + "' and to_char(r2.fechaing,'yyyy-MM-dd') <= '" + fechafin + "' "+
	                      "    group by mo2.descripcion "+
	                      "    order by count(distinct r2.ingreso) limit 1) as mindescripcion"+
                    "from remision_detalle r inner join equipo e on r.idequipo = e.id"+
			"                    inner join modelos mo on mo.id = r.idmodelo  "+
			"                    inner join marcas ma on ma.id = mo.idmarca"+ 
			"                    inner join magnitud_intervalos i on i.id = r.idintervalo"+
			"                    inner join clientes c on c.id = r.idcliente"+
                    "WHERE e.idmagnitud = " + magnitud + " and to_char(fechaing,'yyyy-MM-dd') >= '" + fechaini + "' and to_char(fechaing,'yyyy-MM-dd') <= '" + fechafin + "'"+
                    "group by mo.descripcion, e.idmagnitud "+
                    "order by 1 desc limit 12";

            sqls[4] = "select count( distinct r.ingreso) as cantidad, case when r.idintervalo = 0 then 'VER ESPECIFICACIONES' ELSE '(' || desde || ' a ' || hasta || ') ' || medida end AS descripcion"+
                      "   ,(select count(distinct r2.ingreso) "+
	               "           from remision_detalle r2 inner join equipo e2 on r2.idequipo = e2.id "+
	               "           where e2.idmagnitud = e.idmagnitud and to_char(r2.fechaing,'yyyy-MM-dd') >= '" + fechaini + "' and to_char(r2.fechaing,'yyyy-MM-dd') <= '" + fechafin + "' "+
	               "           group by r2.idintervalo "+
	               "           order by 1 desc limit 1) as maxcantidad"+
	               "          ,(select count(distinct r2.ingreso) "+
	               "           from remision_detalle r2 inner join equipo e2 on r2.idequipo = e2.id "+
	               "           where e2.idmagnitud = e.idmagnitud and to_char(r2.fechaing,'yyyy-MM-dd') >= '" + fechaini + "' and to_char(r2.fechaing,'yyyy-MM-dd') <= '" + fechafin + "' "+
	               "           group by r2.idintervalo "+
	               "           order by 1 limit 1) as mincantidad"+
	               "           ,(select case when r2.idintervalo = 0 then 'VER ESPECIFICACIONES' ELSE '(' || i2.desde || ' a ' || i2.hasta || ') ' || i2.medida end"+
	               "           from remision_detalle r2 inner join equipo e2 on r2.idequipo = e2.id "+
			"				   inner join magnitud_intervalos i2 on i2.id = r2.idintervalo"+
	                "          where e2.idmagnitud = e.idmagnitud and to_char(r2.fechaing,'yyyy-MM-dd') >= '" + fechaini + "' and to_char(r2.fechaing,'yyyy-MM-dd') <= '" + fechafin + "'"+ 
	                "          group by r2.idintervalo, i2.desde, i2.hasta, i2.medida  "+
	                "          order by count(distinct r2.ingreso) desc limit 1) as maxdescripcion"+
	                "         ,(select case when r2.idintervalo = 0 then 'VER ESPECIFICACIONES' ELSE '(' || i2.desde || ' a ' || i2.hasta || ') ' || i2.medida end"+
	                "          from remision_detalle r2 inner join equipo e2 on r2.idequipo = e2.id "+
                        "                               inner join magnitud_intervalos i2 on i2.id = r2.idintervalo"+
	                "          where e2.idmagnitud = e.idmagnitud and to_char(r2.fechaing,'yyyy-MM-dd') >= '" + fechaini + "' and to_char(r2.fechaing,'yyyy-MM-dd') <= '" + fechafin + "' "+
	                "          group by r2.idintervalo, i2.desde, i2.hasta, i2.medida "+ 
                        "      order by count(distinct r2.ingreso) limit 1) as mindescripcion"+
                    "from remision_detalle r inner join equipo e on r.idequipo = e.id"+
			"                    inner join modelos mo on mo.id = r.idmodelo"+  
			"                    inner join marcas ma on ma.id = mo.idmarca "+
			"                    inner join magnitud_intervalos i on i.id = r.idintervalo"+
			 "                   inner join clientes c on c.id = r.idcliente"+
                    "WHERE e.idmagnitud = " + magnitud + " and to_char(fechaing,'yyyy-MM-dd') >= '" + fechaini + "' and to_char(fechaing,'yyyy-MM-dd') <= '" + fechafin + "'"+
                    "group by r.idintervalo, desde, hasta, medida, e.idmagnitud "+
                    "order by 1 desc limit 12";

            sqls[5] = "select count( distinct r.ingreso) as cantidad, c.nombrecompleto  as descripcion"+
                      "   ,(select count(distinct r2.ingreso) "+
	               "           from remision_detalle r2 inner join equipo e2 on r2.idequipo = e2.id "+
			"				                           inner join clientes c2 on c2.id = r2.idcliente"+
	                "          where e2.idmagnitud = e.idmagnitud and to_char(r2.fechaing,'yyyy-MM-dd') >= '" + fechaini + "' and to_char(r2.fechaing,'yyyy-MM-dd') <= '" + fechafin + "' "+
	                "          group by c2.nombrecompleto"+
	                "          order by 1 desc limit 1) as maxcantidad"+
	                "         ,(select count(distinct r2.ingreso) "+
	                "          from remision_detalle r2 inner join equipo e2 on r2.idequipo = e2.id "+
			"				                           inner join clientes c2 on c2.id = r2.idcliente"+
	                "          where e2.idmagnitud = e.idmagnitud and to_char(r2.fechaing,'yyyy-MM-dd') >= '" + fechaini + "' and to_char(r2.fechaing,'yyyy-MM-dd') <= '" + fechafin + "' "+
	                "          group by c2.nombrecompleto"+
	                "          order by 1 limit 1) as mincantidad"+
	                "          ,(select c2.nombrecompleto"+
	                "          from remision_detalle r2 inner join equipo e2 on r2.idequipo = e2.id"+ 
			"				                           inner join clientes c2 on c2.id = r2.idcliente"+
	                "          where e2.idmagnitud = e.idmagnitud and to_char(r2.fechaing,'yyyy-MM-dd') >= '" + fechaini + "' and to_char(r2.fechaing,'yyyy-MM-dd') <= '" + fechafin + "'"+ 
	                "          group by c2.nombrecompleto"+
	                "          order by count(distinct r2.ingreso) desc limit 1) as maxdescripcion"+
	                "         ,(select c2.nombrecompleto"+
	                "          from remision_detalle r2 inner join equipo e2 on r2.idequipo = e2.id "+
			"				                           inner join clientes c2 on c2.id = r2.idcliente"+
	                "          where e2.idmagnitud = e.idmagnitud and to_char(r2.fechaing,'yyyy-MM-dd') >= '" + fechaini + "' and to_char(r2.fechaing,'yyyy-MM-dd') <= '" + fechafin + "' "+
	                "          group by c2.nombrecompleto"+
	                "          order by count(distinct r2.ingreso) limit 1) as mindescripcion"+
                    "from remision_detalle r inner join equipo e on r.idequipo = e.id"+
			"                    inner join modelos mo on mo.id = r.idmodelo  "+
			"                    inner join marcas ma on ma.id = mo.idmarca "+
			"                    inner join magnitud_intervalos i on i.id = r.idintervalo"+
			"                    inner join clientes c on c.id = r.idcliente"+
                    "WHERE e.idmagnitud = " + magnitud + " and to_char(fechaing,'yyyy-MM-dd') >= '" + fechaini + "' and to_char(fechaing,'yyyy-MM-dd') <= '" + fechafin + "'"+
                    "group by c.nombrecompleto, e.idmagnitud "+
                    "order by 1 desc limit 12";

            sqls[6] = "select count(distinct c.numero) as cantidad, u.nombrecompleto  as descripcion"+
                      "   ,(select count(distinct c2.numero) "+
	               "           from remision_detalle r2 inner join equipo e2 on r2.idequipo = e2.id "+
		"					                           inner join certificados c2 on c2.ingreso = r2.ingreso"+
                  "                                     inner join seguridad.rbac_usuario u2 on u2.idusu = c2.idusuario"+
	           "               where e2.idmagnitud = e.idmagnitud and c2.estado <> 'Anulado' and to_char(r2.fechaing,'yyyy-MM-dd') >= '" + fechaini + "' and to_char(r2.fechaing,'yyyy-MM-dd') <= '" + fechafin + "' "+
	            "              group by u2.nombrecompleto"+
	             "             order by 1 desc limit 1) as maxcantidad"+
	              "           ,(select count(distinct c2.numero)"+ 
	               "           from remision_detalle r2 inner join equipo e2 on r2.idequipo = e2.id "+
			"				                           inner join certificados c2 on c2.ingreso = r2.ingreso"+
                        "                               inner join seguridad.rbac_usuario u2 on u2.idusu = c2.idusuario"+
	                "          where e2.idmagnitud = e.idmagnitud and c2.estado <> 'Anulado' and to_char(r2.fechaing,'yyyy-MM-dd') >= '" + fechaini + "' and to_char(r2.fechaing,'yyyy-MM-dd') <= '" + fechafin + "' "+
	                "          group by u2.nombrecompleto"+
	                "          order by 1 limit 1) as mincantidad"+
	                "          ,(select u2.nombrecompleto"+
	                "          from remision_detalle r2 inner join equipo e2 on r2.idequipo = e2.id "+
			"				                           inner join certificados c2 on c2.ingreso = r2.ingreso"+
                         "                              inner join seguridad.rbac_usuario u2 on u2.idusu = c2.idusuario"+
	                  "        where e2.idmagnitud = e.idmagnitud and c2.estado <> 'Anulado' and to_char(r2.fechaing,'yyyy-MM-dd') >= '" + fechaini + "' and to_char(r2.fechaing,'yyyy-MM-dd') <= '" + fechafin + "' "+
	                  "        group by u2.nombrecompleto"+
	                  "        order by count(distinct c2.numero) desc limit 1) as maxdescripcion"+
	                  "       ,(select u2.nombrecompleto"+
	                   "       from remision_detalle r2 inner join equipo e2 on r2.idequipo = e2.id "+
			"				                           inner join certificados c2 on c2.ingreso = r2.ingreso"+
                         "                              inner join seguridad.rbac_usuario u2 on u2.idusu = c2.idusuario"+
	                  "        where e2.idmagnitud = e.idmagnitud and c2.estado <> 'Anulado' and to_char(r2.fechaing,'yyyy-MM-dd') >= '" + fechaini + "' and to_char(r2.fechaing,'yyyy-MM-dd') <= '" + fechafin + "' "+
	                  "        group by u2.nombrecompleto"+
	                  "        order by count(distinct c2.numero) limit 1) as mindescripcion"+   
                    "from remision_detalle r inner join equipo e on r.idequipo = e.id"+
			"                    inner join modelos mo on mo.id = r.idmodelo "+ 
			 "                   inner join marcas ma on ma.id = mo.idmarca"+ 
			 "                   inner join magnitud_intervalos i on i.id = r.idintervalo"+
			 "                   inner join certificados c on c.ingreso = r.ingreso"+
                         "       inner join seguridad.rbac_usuario u on u.idusu = c.idusuario"+
                   " WHERE e.idmagnitud = " + magnitud + " and c.estado <> 'Anulado' and to_char(fechaing,'yyyy-MM-dd') >= '" + fechaini + "' and to_char(fechaing,'yyyy-MM-dd') <= '" + fechafin + "'"+
                   " group by u.nombrecompleto, e.idmagnitud "+
                   " order by 1 desc";

            sqls[7] = "select count( distinct c.numero) as cantidad, e.descripcion  as descripcion"+
                    ",(select count(distinct c2.numero) "+
	             "             from remision_detalle r2 inner join equipo e2 on r2.idequipo = e2.id "+
                      "                                 inner join certificados c2 on c2.ingreso = r2.ingreso"+
	              "            where e2.idmagnitud = e.idmagnitud and c2.estado <> 'Anulado' and  to_char(r2.fechaing,'yyyy-MM-dd') >= '" + fechaini + "' and to_char(r2.fechaing,'yyyy-MM-dd') <= '" + fechafin + "' "+
	              "            group by c2.numero  "+
	              "            order by 1 desc limit 1) as maxcantidad"+
	              "            ,(select e2.descripcion"+
	              "            from remision_detalle r2 inner join equipo e2 on r2.idequipo = e2.id "+
			"				   inner join certificados c2 on c2.ingreso = r2.ingreso"+
	                 "         where e2.idmagnitud = e.idmagnitud and to_char(r2.fechaing,'yyyy-MM-dd') >= '" + fechaini + "' and to_char(r2.fechaing,'yyyy-MM-dd') <= '" + fechafin + "' "+
	                 "         group by e2.descripcion  "+
	                 "         order by count(distinct c2.numero) desc limit 1) as maxdescripcion"+
	                         
                   " from remision_detalle r inner join equipo e on r.idequipo = e.id"+
			"                    inner join modelos mo on mo.id = r.idmodelo"+  
			"                    inner join marcas ma on ma.id = mo.idmarca "+
			"                    inner join magnitud_intervalos i on i.id = r.idintervalo"+
			"                    inner join certificados c on c.ingreso = r.ingreso"+
                        "        inner join seguridad.rbac_usuario u on u.idusu = c.idusuario"+
                   " WHERE e.idmagnitud = " + magnitud + " and c.estado <> 'Anulado' and to_char(fechaing,'yyyy-MM-dd') >= '" + fechaini + "' and to_char(fechaing,'yyyy-MM-dd') <= '" + fechafin + "'"+
                   " group by e.descripcion, e.idmagnitud "+
                   " order by 1 desc";
            sqls[8] = "SELECT cast(avg(respuesta) as decimal(12,2)) as cantidad, count(respuesta) as contador, trim(pregunta) as descripcion, substring(trim(pregunta),1,2) as numero, 0.5 as errores"+
                     "        FROM web.encuesta_satisfaccion en inner join remision_detalle rd on rd.ingreso = en.ingreso"+
			"	                                inner join equipo e on e.id = rd.idequipo"+
                         "    WHERE e.idmagnitud = " + magnitud + " and to_char(fechaenc,'yyyy-MM-dd') >= '" + fechaini + "' and to_char(fechaenc,'yyyy-MM-dd') <= '" + fechafin + "'"+
                          "   GROUP BY pregunta"+
                          "   ORDER BY cast(substring(trim(pregunta),1,2) as double precision)";
            sqls[9] = "SELECT _descripcion as descripcion, count(_cantidad) as cantidad "+
                      "       from entregainforme(" + magnitud + ",'" + fechaini + "','" + fechafin + "')"+
                       "      GROUP BY _descripcion"+
                       "      ORDER BY 1";
            sqls[10] = "select r.ingreso,  'DID-130-' || trim(to_char(inf.informe,'0000')) as informe, "+
	               "             '<b>' || m.descripcion || '</b><br>' || e.descripcion || '<br><b>' || ma.descripcion || '</b><br>' || mo.descripcion || '<br> ' || case when r.idintervalo = 0 then 'VER ESPECIFICACIONES' ELSE '(' || desde || ' a ' || hasta || ') ' || medida end || '<br>' || r.serie as equipo,"+
	                "            c.nombrecompleto as cliente, inf.observacion,"+
                        "        '<button class=''btn btn-glow btn-success imprimir'' title=''Imprimir Informe Técnico'' type=''button'' onclick=' || chr(34) || 'ImprimirInformeTec(' || inf.informe || ')' ||chr(34) || '><span data-icon=''&#xe0f7;''></span></button>' as imprimir"+
                        "    from remision_detalle r inner join equipo e on r.idequipo = e.id"+
			"			                    inner join magnitudes m on m.id = e.idmagnitud"+
			"			                    inner join modelos mo on mo.id = r.idmodelo "+ 
			"			                    inner join marcas ma on ma.id = mo.idmarca"+
                         "                   inner join informetecnico_dev inf on inf.ingreso = r.ingreso"+
                          "                  inner join magnitud_intervalos i on i.id = r.idintervalo "+
                           "                 inner join clientes c on c.id = r.idcliente"+
                   " WHERE e.idmagnitud = " + magnitud + " and to_char(inf.fecha,'yyyy-MM-dd') >= '" + fechaini + "' and to_char(inf.fecha,'yyyy-MM-dd') <= '" + fechafin + "'"+
                   " order by 1 desc";
            sqls[11] = "SELECT * from quejasinforme(" + magnitud + ",'" + fechaini + "','" + fechafin + "')";

            sqls[12] = "SELECT to_char(fecha,'HH24:MI') as hora, to_char(fecha,'yyyy-MM-dd') as fecha, max(punto::numeric) as punto, max(temperatura::numeric) as temperatura, max(humedad::numeric) as humedad, 28.0 as maxtemp, 17.0 as mintemp,"+
			"            70.0 as maxhum, 30.0 as minhume"+
                   " FROM condiciones_ambientales ca inner join intrumento_condamb_sensores s on ca.idsensor = s.id"+
                   " WHERE idmagnitud = " + magnitud + " and to_char(fecha,'yyyy-MM-dd') >= '" + fechaini + "' and to_char(fecha,'yyyy-MM-dd') <= '" + fechafin + "'"+
                   " group by to_char(fecha,'yyyy-MM-dd'), to_char(fecha,'HH24:MI')"+
                   " ORDER BY punto";




            DataSet ds = Globales.ObtenerDataSet(sqls, conexion);



            /*if (ds.Tables[0].Rows.Count == 0)
                return "1 |No hay nada que reportar";*/
 /*    String DirectorioReportesRelativo = "~/Reportes/";
            String directorio = "~/DocumPDF/";
            String unico = "INFORME_MAGNITUD_" + DateTime.Now.ToString("yyyyMMddHHmmss");
            String urlArchivo = String.Format("{0}.{1}", reporte, "rdlc");
                        
            String urlArchivoGuardar;
            urlArchivoGuardar = String.Format("{0}.{1}", unico, "pdf");

            String FullPathReport = string.Format("{0}{1}",
                                    this.HttpContext.Server.MapPath(DirectorioReportesRelativo),
                                        urlArchivo);
            String FullGuardar = String.Format("{0}{1}",
                                    this.HttpContext.Server.MapPath(directorio),
                                        urlArchivoGuardar);
            ReportViewer Reporte = new ReportViewer();
            Reporte.Reset();
            Reporte.LocalReport.ReportPath = FullPathReport;
            Reporte.LocalReport.EnableExternalImages = true;
            Reporte.LocalReport.EnableHyperlinks = true;

            
            /*ReportDataSource DataSource = new ReportDataSource("DataSet1", ds.Tables[0]);*/
 /*  ReportDataSource DataSource1 = new ReportDataSource("Revision", ds.Tables[0]);
            ReportDataSource DataSource2 = new ReportDataSource("Equipos", ds.Tables[1]);
            ReportDataSource DataSource3 = new ReportDataSource("Marcas", ds.Tables[2]);
            ReportDataSource DataSource4 = new ReportDataSource("Modelos", ds.Tables[3]);
            ReportDataSource DataSource5 = new ReportDataSource("Rangos", ds.Tables[4]);
            ReportDataSource DataSource6 = new ReportDataSource("Empresas", ds.Tables[5]);
            ReportDataSource DataSource7 = new ReportDataSource("Tecnicos", ds.Tables[6]);
            ReportDataSource DataSource8 = new ReportDataSource("Certificados", ds.Tables[7]);
            ReportDataSource DataSource9 = new ReportDataSource("Encuesta", ds.Tables[8]);
            ReportDataSource DataSource10 = new ReportDataSource("Entrega", ds.Tables[9]);
            ReportDataSource DataSource11 = new ReportDataSource("Informes", ds.Tables[10]);
            ReportDataSource DataSource12 = new ReportDataSource("Quejas", ds.Tables[11]);
            ReportDataSource DataSource13 = new ReportDataSource("Temperaturas", ds.Tables[12]);

            /*Reporte.LocalReport.DataSources.Add(DataSource);*/
 /*   Reporte.LocalReport.DataSources.Add(DataSource1);
            Reporte.LocalReport.DataSources.Add(DataSource2);
            Reporte.LocalReport.DataSources.Add(DataSource3);
            Reporte.LocalReport.DataSources.Add(DataSource4);
            Reporte.LocalReport.DataSources.Add(DataSource5);
            Reporte.LocalReport.DataSources.Add(DataSource6);
            Reporte.LocalReport.DataSources.Add(DataSource7);
            Reporte.LocalReport.DataSources.Add(DataSource8);
            Reporte.LocalReport.DataSources.Add(DataSource9);
            Reporte.LocalReport.DataSources.Add(DataSource10);
            Reporte.LocalReport.DataSources.Add(DataSource11);
            Reporte.LocalReport.DataSources.Add(DataSource12);
            Reporte.LocalReport.DataSources.Add(DataSource13);

            Reporte.LocalReport.Refresh();
            byte[] ArchivoPDF = Reporte.LocalReport.Render("PDF");
            FileStream fs = new FileStream(FullGuardar, FileMode.Create);
            fs.Write(ArchivoPDF, 0, ArchivoPDF.length);
            fs.Close();
            return "0|" + urlArchivoGuardar;
        }
    
    
     */
    //FIN DEL CÓDIGO
    protected void Servidor(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        String ruta_origen = request.getHeader("Origin");

        HttpSession misession = request.getSession();
        idusuario = misession.getAttribute("idusuario").toString();
        iplocal = misession.getAttribute("IPLocal").toString();
        usuario = misession.getAttribute("codusu").toString();
        if (misession.getAttribute("ArchivosAdjuntos") != null) {
            archivoadjunto = misession.getAttribute("ArchivosAdjuntos").toString();
        }
        response.setHeader("Access-Control-Allow-Origin", request.getHeader("Origin"));
        response.setHeader("Access-Control-Allow-Credentials", "true");

        try (PrintWriter out = response.getWriter()) {
            if (Globales.ValidarRuta(ruta_origen) == false) {
                out.print("[{\"error\":\"505\",\"mensaje\":\"Usted no tiene permiso para acceder a esta ruta\"}]");
            } else {
                if (misession.getAttribute("codusu") == null) {
                    out.println("cerrada");
                } else {
                    if (!Globales.IpUsuario(misession.getAttribute("idusuario").toString()).equals(misession.getAttribute("IPLocal").toString())) {
                        out.println("cambio-ip");
                    } else {
                        String opcion = request.getParameter("opcion");
                        switch (opcion) {
                            case "TablaInformesGerencia":
                                out.println(TablaInformesGerencia(request));
                                break;
                            case "TablaInformesProduccion":
                                System.out.println(request);
                                out.println(TablaInformesProduccion(request));
                                break;
                            case "RelacionIngreso":
                                System.out.println(request);
                                out.println(RelacionIngreso(request));
                                break;
                            case "DetRelacionIngreso":
                                out.println(DetRelacionIngreso(request));
                                break;
                            case "RegistroRevision":
                                out.println(RegistroRevision(request));
                                break;
                            /*    
                            case "RpInformeDireccion":
                                out.println(RpInformeDireccion(request));
                                break;
                                
                             */

                            default:
                                throw new AssertionError();
                        }
                    }
                }
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Servidor(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Servidor(request, response);
    }

}
