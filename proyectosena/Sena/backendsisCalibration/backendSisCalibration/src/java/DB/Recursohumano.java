package DB;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author leudi
 */
@WebServlet(urlPatterns = {"/Recursohumano"})
public class Recursohumano extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    String sql;
    String idusuario = "";
    String usuario = "";
    ResultSet datos = null;
    String iplocal = "";
    String archivoadjunto = null;
    String empleado;
    String nombreusuario;

    public String BuscarEmpleados(HttpServletRequest request) {

        String documento = request.getParameter("documento");
        int empleado = Globales.Validarintnonull(request.getParameter("empleado"));

        sql = "SELECT e.id, tipodocumento, documento, nombrecompleto, telefono, email, c.descripcion as cargo, ideps, idafp, foto, idcesantia, "
                + "      celular, direccion, idcargo, idbanco, cuenta, tipocuenta,  sueldo, e.estado, huella, idhorario, b.descripcion as banco, "
                + "      to_char(fechainicio,'yyyy-MM-dd') as fechainicio, to_char(fechafin,'yyyy-MM-dd') as fechafin, idpais, idciudad, iddepartamento, "
                + "      estadocivil, to_char(fechanacimiento,'yyyy-MM-dd') as fechanacimiento, idarea, genero, camisa, pantalon, tiposangre, barrio, "
                + "      idclaseriesgo, idcentrocosto, periodopago "
                + "  from rrhh.empleados e inner join rrhh.cargo c on c.id = e.idcargo  "
                + "                        inner join bancos b on b.id = e.idbanco "
                + "                        left join ciudad ci on ci.id = e.idciudad "
                + "                        left join departamento d on d.id = ci.iddepartamento                                             "
                + "  WHERE " + (empleado == 0 ? " documento = '" + documento + "'" : "e.id=" + empleado);
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->BuscarEmpleados");
    }

    public String GuardarDocumentosEmpleados(HttpServletRequest request) {
        int Empleado = Globales.Validarintnonull(request.getParameter("Empleado"));
        int Documento = Globales.Validarintnonull(request.getParameter("Documento"));
        int Numero = Globales.Validarintnonull(request.getParameter("Numero"));

        try {
            sql = "SELECT id "
                    + " FROM rrhh.empleados_documentos "
                    + " where idempleado = " + Empleado + " and iddocumento = " + Documento + " and nrodocumento =" + Numero;
            int id = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));

            if (archivoadjunto == null) {
                return "1|Error al subir el archivo";
            }

            File path = new File(Globales.ruta_archivo + "DocumentosEmpleados/" + Empleado);
            if (!path.exists()) {
                path.mkdirs();
            }

            File fileName = new File(Globales.ruta_archivo + "uploads/" + archivoadjunto);
            File destFile = new File(Globales.ruta_archivo + "DocumentosEmpleados/" + Empleado + "/" + Documento + "-" + Numero + ".pdf");
            Files.copy(fileName.toPath(), destFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
            if (id == 0) {
                sql = "INSERT INTO rrhh.empleados_documentos(idempleado, iddocumento, idusuario, nrodocumento)"
                        + "  VALUES (" + Empleado + "," + Documento + "," + idusuario + ", " + Numero + ");";
                if (Globales.DatosAuditoria(sql, "EMPLEADO", "AGREGAR DOCUMENTO", idusuario, iplocal, this.getClass() + "-->GuardarRecibirRadi")) {
                    return "0|Documento agregado con éxito";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            } else {
                sql = "UPDATE rrhh.empleados_documentos"
                        + "      SET idusuario=" + idusuario + ",fecha=now() "
                        + " WHERE id=" + id;
                if (Globales.DatosAuditoria(sql, "EMPLEADO", "ACTUALIZAR DOCUMENTO", idusuario, iplocal, this.getClass() + "-->GuardarRecibirRadi")) {
                    return "0|Documento actualizado con éxito";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            }
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String DocumentosEmpleado(HttpServletRequest request) {
        int Empleado = Globales.Validarintnonull(request.getParameter("Empleado"));
        int Documento = Globales.Validarintnonull(request.getParameter("Documento"));
        int Numero = Globales.Validarintnonull(request.getParameter("Numero"));

        sql = "SELECT to_char(fecha,'yyyy/MM/dd HH24:MI') as fecha, u.nombrecompleto as usuario"
                + "            FROM rrhh.empleados_documentos cd INNER JOIN seguridad.rbac_usuario u on cd.idusuario = u.idusu"
                + "           where cd.idempleado = " + Empleado + " and iddocumento = " + Documento + "and nrodocumento = " + Numero;
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->DocumentosEmpleado");
    }

    public String TablaDocumentosEmpleado(HttpServletRequest request) {
        int Empleado = Globales.Validarintnonull(request.getParameter("Empleado"));

        sql = "SELECT Z.*"
                + "FROM(SELECT id, 0 as nrodocumento,descripcion,obligatorio FROM rrhh.documentos d"
                + "UNION SELECT iddocumento, nrodocumento,idempleado::text,0 FROM rrhh.empleados_documentos WHERE idempleado = " + Empleado + ") AS ZORDER BY 1,2";

        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->TablaDocumentosEmpleado");
    }

    public String ConsultarEmpleados(HttpServletRequest request) {
        int documento = Globales.Validarintnonull(request.getParameter("documento"));
        String nombre = request.getParameter("nombre");
        String tipodocumento = request.getParameter("tipodocumento");
        int cargo = Globales.Validarintnonull(request.getParameter("cargo"));
        String estado = request.getParameter("estado");
        String area = request.getParameter("area");
        String afp = request.getParameter("afp");
        String eps = request.getParameter("eps");
        String cesantia = request.getParameter("cesantia");
        String Busqueda = "WHERE 1 = 1";

        if (documento > 0) {
            Busqueda += " AND documento like '%" + documento + "%' ";
        }
        if (!tipodocumento.equals("T")) {
            Busqueda += " AND tipodocumento = '" + tipodocumento + "' ";
        }
        if (!nombre.trim().equals("")) {
            Busqueda += " AND nombrecompleto ilike '%" + nombre + "%' ";
        }
        if (cargo > 0) {
            Busqueda += " AND idcargo = " + cargo;
        }
        if (!estado.trim().equals("TODOS")) {
            Busqueda += " AND e.estado = '" + estado + "' ";
        }
        if (!cesantia.equals("")) {
            Busqueda += " AND idcesantia = " + cesantia;
        }
        if (!area.equals("")) {
            Busqueda += " AND idarea = " + area;
        }
        if (!eps.equals("")) {
            Busqueda += " AND ideps = " + eps;
        }
        if (!afp.equals("")) {
            Busqueda += " AND idafp = " + afp;
        }
        String url_archivo = Globales.url_archivo;
        sql = "select e.id, "
                + "      case when tipodocumento = 'CC' then 'Cédula Ciudadanía' when tipodocumento = 'CE' then 'Cédula Extrangería' when tipodocumento = 'PA' then 'Pasaporte' when tipodocumento = 'PP' THEN 'Permiso de Permanencia' end as tipodocumento, "
                + "      documento, nombrecompleto, c.descripcion as cargo, tipocuenta, cuenta, b.descripcion as banco, sueldo, to_char(fechainicio,'yyyy-MM-dd') as fechainicio, to_char(fechafin,'yyyy-MM-dd') as fechafin, "
                + "      direccion, telefono, celular, email, e.estado, huella, h.descripcion as horario, eps.descripcion as eps, afp.descripcion as afp, ce.descripcion as cesantia,"
                + "      a.descripcion as area, barrio, genero, tiposangre, camisa, pantalon, to_char(fechanacimiento,'yyyy-MM-dd') as fechanacimiento, "
                + "      coalesce(p.descripcion || '/' || d.descripcion || '/' || ci.descripcion,'') as lugarnacimiento,"
                + "      0 as obligatorio,"
                + "      case when coalesce(foto,'') <> '' then '<a href=' || chr(34) || 'javascript:MaximizarFoto(''' || foto || ''')' || chr(34) || '><img src=' || chr(34) || '" + url_archivo + "imagenes/FotoEmpleados/' || e.id || '.jpg' || chr(34) || ' width=''80px''/></a>' end as foto"
                + "     from rrhh.empleados e inner join rrhh.cargo c on c.id = e.idcargo "
                + "                       inner join bancos b on b.id = e.idbanco"
                + "                       inner join rrhh.horario h on h.id = e.idhorario"
                + "                       inner join rrhh.area a on a.id = e.idarea"
                + "                       inner join eps on eps.id = e.ideps"
                + "                       inner join afp on afp.id = e.idafp "
                + "                       inner join cesantia ce on ce.id = e.idcesantia "
                + "                       inner join ciudad ci on ci.id = e.idciudad"
                + "                               inner join departamento d on d.id = ci.iddepartamento"
                + "                               inner join pais p on p.id = d.idpais " + Busqueda + " order by 2";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ConsultarEmpleados");
    }

    public String CambioDocumento(HttpServletRequest request) {
        String documento = request.getParameter("documento");
        int id = Globales.Validarintnonull(request.getParameter("id"));
        String tipo = request.getParameter("tipo");

        try {
            sql = "UPDATE rrhh.empleados set documento ='" + documento.trim() + "', tipodocumento = '" + tipo + "' WHERE id = " + id;
            if (Globales.DatosAuditoria(sql, "RECURSOS HUMANOS", "REEMPLAZAR ", idusuario, iplocal, this.getClass() + "-->CambioDocumento")) {
                return "0|Cambio de Documento realizado con éxito";
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String GuardarEmpleados(HttpServletRequest request) {
        int IdEmpleado = Globales.Validarintnonull(request.getParameter("IdEmpleado"));
        String TipoDocumento = request.getParameter("TipoDocumento");
        String Documento = request.getParameter("Documento");
        String Nombres = request.getParameter("Nombres");
        String Telefonos = request.getParameter("Telefonos");
        String Celular = request.getParameter("Celular");
        String Email = request.getParameter("Email");
        int Banco = Globales.Validarintnonull(request.getParameter("Banco"));
        String Cargo = request.getParameter("Cargo");
        String FechaInicio = request.getParameter("FechaInicio");
        String FechaFinal = request.getParameter("FechaFinal");
        String Direccion = request.getParameter("Direccion");
        double Sueldo = Globales.Validarintnonull(request.getParameter("Sueldo"));
        String Cuenta = request.getParameter("Cuenta");
        String Estado = request.getParameter("Estado");
        int Horario = Globales.Validarintnonull(request.getParameter("Horario"));
        int Huella = Globales.Validarintnonull(request.getParameter("Huella"));
        String TipoCuenta = request.getParameter("TipoCuenta");
        String Ciudad = request.getParameter("Ciudad");
        String EstadoCivil = request.getParameter("EstadoCivil");
        String FechaNacimiento = request.getParameter("FechaNacimiento");
        int ClaseRiesgo = Globales.Validarintnonull(request.getParameter("ClaseRiesgo"));
        String Pantalon = request.getParameter("Pantalon");
        String Camisa = request.getParameter("Camisa");
        String TipoSangre = request.getParameter("TipoSangre");
        String Genero = request.getParameter("Genero");
        int Area = Globales.Validarintnonull(request.getParameter("Area"));
        String Barrio = request.getParameter("Barrio");
        int EPS = Globales.Validarintnonull(request.getParameter("EPS"));
        int AFP = Globales.Validarintnonull(request.getParameter("AFP"));
        int CESANTIA = Globales.Validarintnonull(request.getParameter("CESANTIA"));
        int CentroCosto = Globales.Validarintnonull(request.getParameter("CentroCosto"));
        String PeriodoPago = request.getParameter("PeriodoPago");

        if (FechaFinal.equals("")) {
            FechaFinal = "null";
        } else {
            FechaFinal = "'" + FechaFinal + "'";
        }
        if (FechaInicio.equals("")) {
            FechaInicio = "null";
        } else {
            FechaInicio = "'" + FechaInicio + "'";
        }

        if (TipoCuenta == null) {
            TipoCuenta = "";
        }
        try {
            if (IdEmpleado == 0) {
                if (Globales.PermisosSistemas("EMPLEADOS GUARDAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "INSERT INTO rrhh.empleados(tipodocumento, documento, nombrecompleto, telefono, email, "
                        + "      celular, direccion, idcargo, idbanco, cuenta, tipocuenta, idciudad, estadocivil, fechanacimiento, sueldo, estado, fechainicio, fechafin, huella, idhorario, ideps, idafp, idcesantia,"
                        + "      idarea, genero, camisa, pantalon, tiposangre, barrio, idclaseriesgo, idcentrocosto, periodopago)"
                        + "      VALUES('" + TipoDocumento + "'  ,'" + Documento.trim() + "'  ,'" + Nombres.trim().toUpperCase() + "'  ,'" + Telefonos + "'  ,'" + Email + "','" + Celular + "','" + Direccion + "'," + Cargo + "," + Banco + ",'" + Cuenta + "','" + TipoCuenta + "'," + Ciudad + ",'" + EstadoCivil + "','" + FechaNacimiento + "'," + Sueldo + ",'" + Estado + "'," + FechaInicio + "," + FechaFinal + "," + Huella + "," + Horario + "," + EPS + "," + AFP + "," + CESANTIA + ","
                        + Area + ",'" + Genero + "','" + Camisa + "','" + Pantalon + "','" + TipoSangre + "','" + Barrio + "'," + ClaseRiesgo + "," + CentroCosto + ",'" + PeriodoPago + "');";

                if (Globales.DatosAuditoria(sql, "EMPLEADOS", "GUARDAR", idusuario, iplocal, this.getClass() + "-->GuardarEmpleados")) {
                    IdEmpleado = Globales.Validarintnonull(Globales.ObtenerUnValor(" SELECT MAX(id) FROM rrhh.empleados;"));
                    return "0|Empleado guardado con éxito|" + IdEmpleado;
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            } else {
                if (Globales.PermisosSistemas("EMPLEADOS EDITAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "UPDATE rrhh.empleados SET tipodocumento ='" + TipoDocumento + "'  , nombrecompleto='" + Nombres.toUpperCase() + "', telefono='" + Telefonos + "', celular='" + Celular + "', email='" + Email + "',"
                        + "direccion='" + Direccion + "',idcargo=" + Cargo + ",idbanco=" + Banco
                        + ",cuenta='" + Cuenta + "', tipocuenta='" + TipoCuenta + "',sueldo=" + Sueldo + ",estado='" + Estado
                        + "',fechainicio=" + FechaInicio + ", fechafin = " + FechaFinal
                        + ",huella=" + Huella + ",idhorario=" + Horario + ",ideps=" + EPS + ",idafp=" + AFP + ", idcesantia=" + CESANTIA + ",idciudad=" + Ciudad + ",estadocivil='" + EstadoCivil + "',fechanacimiento= '" + FechaNacimiento
                        + "', idarea=" + Area + ", genero='" + Genero + "', camisa='" + Camisa + "', pantalon='" + Pantalon + "', "
                        + "tiposangre='" + TipoSangre + "', barrio='" + Barrio + "', idclaseriesgo=" + ClaseRiesgo
                        + ", idcentrocosto=" + CentroCosto + ",periodopago='" + PeriodoPago + "' WHERE id=" + IdEmpleado;
                if (Globales.DatosAuditoria(sql, "EMPLEADOS", "EDITAR", idusuario, iplocal, this.getClass() + "-->GuardarEmpleados")) {
                    return "0|Empleado actualizado con éxito|" + IdEmpleado;
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            }
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String RepararConceptos(HttpServletRequest request) {
        int nomina = Globales.Validarintnonull(request.getParameter("nomina"));

        sql = "select * from rrhh.repararconceptos(" + nomina + ");";
        return Globales.ObtenerUnValor(sql);
    }

    public String BuscarNominas(HttpServletRequest request) {
        int tipo = Globales.Validarintnonull(request.getParameter("tipo"));
        int codigo = Globales.Validarintnonull(request.getParameter("codigo"));
        int tipoconcepto = Globales.Validarintnonull(request.getParameter("tipoconcepto"));
        int nomina = Globales.Validarintnonull(request.getParameter("nomina"));

        switch (tipo) {
            case 1:
                sql = "SELECT n.id, codigo, n.tipo, titulo, descripcion, to_char(n.fechainicio,'yyyy-MM-dd') as fechainicio, estado,"
                        + "  numero || ' - Inicio: ' || to_char(p.fechainicio,'dd/MM/yyyy') || ' Fin: ' || to_char(p.fechafinal,'dd/MM/yyyy') || "
                        + "  case when calculado = 'SI' THEN ' Calculada' else '' end || case when cerrado = 'SI' THEN ' Cerrada' else '' end as periodo,"
                        + "  calculado, cerrado, pagado, contabilizado,"
                        + "  (select count(ne.idempleado) from rrhh.nominas_empleados ne where ne.idperiodo = p.id) as cantidad"
                        + "  FROM rrhh.nominas n inner join  rrhh.nominas_periodos p on p.numero = n.periodo and n.id = p.idnomina " + ""
                        + (codigo > 0 ? "where codigo = " + codigo : "") + " ORDER BY codigo";
                break;
            case 2:
                sql = "SELECT c.*, tc.descripcion as tipo, "
                        + "              (select count (id) from rrhh.conceptos_empleados ne where ne.idconcepto = c.id) as cantempleados"
                        + "      FROM rrhh.conceptos c inner join  rrhh.nominas_periodos np on c.idperiodo = np.id"
                        + "			                          inner join rrhh.tipo_concepto tc on tc.id = c.idtipo"
                        + "    WHERE idperiodo = " + nomina + ""
                        + (codigo > 0 ? " and codigo = " + codigo : "") + (tipoconcepto > 0 ? " and c.idtipo= " + tipoconcepto : "") + " ORDER BY codigo";
                break;
            case 3:
                sql = "SELECT c.*,"
                        + "             (select count (id) from rrhh.constantes_empleados ne where ne.idconstante = c.id) as cantempleados"
                        + "    FROM rrhh.constantes c inner join  rrhh.nominas_periodos np on c.idperiodo = np.id"
                        + "    WHERE idperiodo = " + nomina
                        + (codigo > 0 ? " and codigo = " + codigo : "") + " ORDER BY codigo";
                break;

        }
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->RepararConceptos");
    }

    private String NominaCalculada(int idperiodo, int tipo) {
        String campo = "";
        if (tipo == 1) {
            campo = "calculado";
        } else if (tipo == 2) {
            campo = "cerrado";
        } else if (tipo == 3) {
            campo = "pagado";
        } else {
            campo = "contabilizado";
        }
        sql = "SELECT " + campo + ""
                + "      FROM rrhh.nominas_periodos "
                + "     where id = " + idperiodo;
        return Globales.ObtenerUnValor(sql);
    }

    public String Periodos_Nomina(HttpServletRequest request) {
        int nomina = Globales.Validarintnonull(request.getParameter("nomina"));
        sql = "SELECT numero, to_char(fechainicio,'dd/MM/yyyy') as fechainicio, to_char(fechafinal,'dd/MM/yyyy') as fechafinal, calculado, cerrado, pagado, contabilizado"
                + "  FROM rrhh.nominas_periodos where idnomina = " + nomina + " ORDER BY numero";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->Periodos_Nomina");
    }

    public String BuscarHojaVida(HttpServletRequest request) {
        int Empleado = Globales.Validarintnonull(request.getParameter("Empleado"));
        sql = "select documento, nombrecompleto, telefono, email, celular, c.descripcion as cargo, direccion, coalesce(estadocivil,'') as estadocivil, coalesce(to_char(fechanacimiento,'yyyy-MM-dd'),'') as fechanacimiento,"
                + "          coalesce(p.descripcion || '/' || d.descripcion || '/' || ci.descripcion,'') as lugarnacimiento,"
                + "          coalesce(perfil_profesional,'') as perfil_profesional, coalesce(estudios_secundarios,'') as estudios_secundarios, coalesce(pregrado,'') as pregrado, coalesce(postgrado,'') as postgrado, "
                + "          coalesce(otros_estudios,'') as otros_estudios, coalesce(ultimo_cargo,'') as ultimo_cargo, coalesce(logros,'') as logros, coalesce(jefe_inmediato,'') as jefe_inmediato, "
                + "         coalesce(telefono_contacto,'') as telefono_contacto, coalesce(tiempo_laborado,'') as tiempo_laborado, coalesce(foto,'') as foto"
                + "  from rrhh.empleados e inner join rrhh.cargo c on c.id = e.idcargo"
                + "                      LEFT join ciudad ci on ci.id = e.idciudad"
                + "                      LEFT join departamento d on d.id = ci.iddepartamento"
                + "                      LEFT join pais p on p.id = d.idpais"
                + "                      LEFT JOIN rrhh.hoja_vida hv on hv.idempleado =  e.id"
                + " where e.id = " + Empleado;
        String sql2 = "SELECT hvc.id, archivo, foto"
                + "  FROM rrhh.hoja_vida_certificacion hvc inner join rrhh.hoja_vida hv on hv.id = hvc.idhoja"
                + " WHERE idempleado = " + Empleado + " order by foto";
        String sql3 = "SELECT posicion, nombre, relacion, cargo, telefono, tipo"
                + " FROM rrhh.hoja_vida_referencia hvr inner join rrhh.hoja_vida hv on hv.id = hvr.idhoja"
                + " WHERE idempleado = " + Empleado + " order by hvr.id";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->BuscarHojaVida") + "|" + Globales.ObtenerDatosJSon(sql2, this.getClass() + "-->BuscarHojaVida") + "|" + Globales.ObtenerDatosJSon(sql3, this.getClass() + "-->BuscarHojaVida");
    }

    /* 
   
 public String BuscarPermisolaboral(HttpServletRequest request){
     int Empleado= Globales.Validarintnonull(request.getParameter("Empleado")); 
     
            
            sql = "select documento, nombrecompleto, telefono, email, celular, c.descripcion as cargo, coalesce(to_char(fecha,'yyyy-MM-dd'),'') as fecha"+
	          "      from rrhh.empleados e inner join rrhh.cargo c on c.id = e.idcargo"+
		   "                       LEFT JOIN rrhh.permiso_laboral pl on pl.idempleado =  e.id"+
                   " where e.id = " + Empleado;
            return Globales.ObtenerDatosJSon(sql,  this.getClass() + "-->BuscarPermisolaboral");
        }

        
        private byte[] StreamFile(String filename)
        {
            FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read);

            // Create a byte array of file stream length
            byte[] ImageData = new byte[fs.Length];

            //Read block of bytes from stream into the byte array
            fs.Read(ImageData, 0, System.Convert.ToInt32(fs.Length));

            //Close the File Stream
            fs.Close();
            return ImageData;
        }
        
        
        
        
        
        
         public string AdjuntarFoto(HttpPostedFileBase documento)
        {
            string respuesta = "9|Error al Subir el Archivo ";
            if (documento != null)
            {
                string adjunto = "hojavida" + DateTime.Now.ToString("yyyyMMddHHmmss") + Path.GetExtension(documento.FileName);
                //adjunto = "MonturasPrecios" +  Path.GetExtension(documento.FileName);
                Session["FotoHojaVida"] = adjunto;
                documento.SaveAs(HostingEnvironment.MapPath("~/uploads/" + adjunto));
                respuesta = "0|" + HostingEnvironment.MapPath("~/uploads/" + adjunto);
            }

            return respuesta;
        }
        
        
        
   
   
     public string GuardarAnexoHoja(int Empleado, string NombreArchivo)
        {
            if (Inicializar() != "0") return validarinicio;
            if (Session["FotoHojaVida"] == null)
                return "1|Error al subir el archivo";
                        
            try
            {

                if (Empleado.ToString().Trim() != Session["empleado"].ToString().Trim())
                {
                    if (PermisosSistemas("HOJA DE VIDA GUARDAR ANEXO") == 0)
                        return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }

                int IdHoja = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT id from rrhh.hoja_vida where idempleado = " + Empleado, conexion, 0));
                if (IdHoja == 0)
                {
                    sql = @"INSERT INTO rrhh.hoja_vida(idempleado, idusuario)
                            VALUES (" + Empleado + "," + idusuario + ");SELECT MAX(id) from rrhh.hoja_vida;";
                    IdHoja = Globales.Validarintnonull(Globales.ObtenerUnValor(sql, conexion,0));
                }

                sql = @"SELECT hvc.id
                            FROM rrhh.hoja_vida_certificacion hvc inner join rrhh.hoja_vida hv on hv.id = hvc.idhoja
                            WHERE idempleado = " + Empleado + " AND archivo = '" + NombreArchivo + "'";
                int Id = Globales.Validarintnonull(Globales.ObtenerUnValor(sql, conexion, 0));
                if (Id > 0)
                    return "1|Este archivo ya existe como anexo a la hoja de vida";

                sql = @"SELECT max(foto)
                            FROM rrhh.hoja_vida_certificacion hvc inner join rrhh.hoja_vida hv on hv.id = hvc.idhoja
                            WHERE idempleado = " + Empleado;
                int NumFoto = Globales.Validarintnonull(Globales.ObtenerUnValor(sql, conexion, 0))+1;

                string foto = HostingEnvironment.MapPath("~/uploads/" + Session["FotoHojaVida"].ToString());

                Image fotonew;

                int width = 680;
                var height = 0; //succeeds at 65499, 65500

                byte[] bytes = StreamFile(foto);
                                
                using (var ms = new MemoryStream(bytes, 0, bytes.Length))
                {
                    Image Imagen = Image.FromStream(ms, true);
                    fotonew = Globales.CambiarTamanoImagen(Imagen, width, height);
                }

                if (fotonew == null)
                    return "1|Error al guardar la foto";

                string path = HostingEnvironment.MapPath("~/imagenes/AnexoHojaVida/" + Empleado);
                if (!Directory.Exists(path))
                {
                    DirectoryInfo di = Directory.CreateDirectory(path);
                }

                string ruta = HostingEnvironment.MapPath("~/imagenes/AnexoHojaVida/" + Empleado + "/" + NumFoto + ".jpg");
                fotonew.Save(ruta);
                System.IO.File.Delete(foto);

                sql = @"INSERT INTO rrhh.hoja_vida_certificacion(idhoja, archivo, foto)
                         VALUES(" + IdHoja + ",'" + NombreArchivo + "'," + NumFoto + ");select max(id) from rrhh.hoja_vida_certificacion;";

                string id =Globales.DatosAuditoriaValor(sql, "HOJA DE VIDA", "GUARDAR ANEXO", idusuario, iplocal);

                return "0|Anexo subido con éxito|" + NumFoto + "|" + id;

            }
            catch (Exception ex)
            {

                return "1|" + ex.Message;
            }
        }

        private static ImageCodecInfo GetEncoderInfo(String mimeType)
        {
            int j;
            ImageCodecInfo[] encoders;
            encoders = ImageCodecInfo.GetImageEncoders();
            for (j = 0; j < encoders.Length; ++j)
            {
                if (encoders[j].MimeType == mimeType)
                    return encoders[j];
            }
            return null;
        }

      
        
          public string GuardarFotoEmpleado(string documento, int Empleado, int x1, int x2, int y1, int y2, int w, int h)
        {
            if (Inicializar() != "0") return validarinicio;

            if (Empleado.ToString().Trim() != Session["empleado"].ToString().Trim())
            {
                if (PermisosSistemas("HOJA DE VIDA SUBIR FOTO") == 0)
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
            }


            string base64 = documento.Substring(documento.IndexOf(',') + 1);
            base64 = base64.Trim('\0');
            /*byte[] chartData = Convert.FromBase64String(base64);*/
 /*   Image foto;
            byte[] imageBytes = Convert.FromBase64String(base64);
            // Convert byte[] to Image
            using (var ms = new MemoryStream(imageBytes, 0, imageBytes.Length))
            {
                Image image = Image.FromStream(ms, true);
                foto = image;
            }

            string respuesta = "1|Error al guardar la foto ";

            if (foto == null)
                return respuesta;

            try
            {
               
                ImageCodecInfo myImageCodecInfo;
                Encoder myEncoder;
                EncoderParameter myEncoderParameter;
                EncoderParameters myEncoderParameters;
                string nomfoto = Empleado + "_" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".jpg";
                myImageCodecInfo = GetEncoderInfo("image/jpeg");
                myEncoder = Encoder.Quality;
                

                //foto = Globales.RecortarImagen(foto, x1, x2, y1, y2, w, h);
                string ruta = HostingEnvironment.MapPath("~/imagenes/FotoEmpleados/" + nomfoto);
                string rutaeliminar = HostingEnvironment.MapPath("~/imagenes/FotoEmpleados/");

                var ficheros = Directory.GetFiles(rutaeliminar, Empleado + "_*.jpg");
                foreach (string archivo in ficheros)
                {
                    System.IO.File.Delete(archivo);
                }
                                
                myEncoderParameters = new EncoderParameters(1);
                myEncoderParameter = new EncoderParameter(myEncoder, 50L);
                myEncoderParameters.Param[0] = myEncoderParameter;
                foto.Save(ruta, myImageCodecInfo, myEncoderParameters);
                sql = @"UPDATE rrhh.empleados set foto = '" + nomfoto + "' WHERE id = " + Empleado;
                Globales.DatosAuditoriaValor(sql, "EMPLEADOS", "ACTUALIZAR FOTO", idusuario, iplocal);
                if (Empleado.ToString().Trim() != Session["empleado"].ToString().Trim())
                    Session["foto"] = nomfoto;
                return "0|Foto actualizada|" + nomfoto;

            }
            catch (Exception e)
            {
                return "1|" + e.Message;
            }
        }
     
    public String EliminarFotoEmpleado(HttpServletRequest request) {

        int Empleado = Globales.Validarintnonull(request.getParameter("Empleado"));
        String Foto = request.getParameter("Foto");

        try {

            if (Empleado.ToString().Trim() != Session["empleado"].ToString().Trim()) {
                if (Globales.PermisosSistemas("HOJA DE VIDA ELIMINAR FOTO", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
            }

            String ruta = HostingEnvironment.MapPath("~/imagenes/FotoEmpleados/" + Foto);
            System.IO.File.Delete(ruta);
            sql = "UPDATE rrhh.empleados set foto = '' WHERE id = " + Empleado;
            Globales.DatosAuditoria(sql, "EMPLEADOS", "ELIMINAR FOTO EMPLEADO", idusuario, iplocal, this.getClass() + "-->EliminarFotoEmpleado");
            if (Empleado.ToString().trim() != Session["empleado"].ToString().trim()) {
                Session["foto"] = "";
            }
            return "0|Foto eliminada";

        } catch (Exception ex) {

            return "1|" + ex.getMessage();
        }
    }
     */
    public String EliminarAnexoHoja(HttpServletRequest request) {

        int FotoId = Globales.Validarintnonull(request.getParameter("FotoId"));
        String Empleado = request.getParameter("Empleado");
        int Foto = Globales.Validarintnonull(request.getParameter("Foto"));
        try {
            if (!Empleado.trim().equals(empleado.trim())) {
                if (Globales.PermisosSistemas("HOJA DE VIDA ELIMINAR ANEXO", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
            }

            File ruta = new File(Globales.ruta_archivo + "imagenes/AnexoHojaVida/" + Empleado + "/" + Foto + ".jpg");
            Files.deleteIfExists(ruta.toPath());

            sql = "DELETE FROM rrhh.hoja_vida_certificacion WHERE id = " + FotoId;
            Globales.DatosAuditoria(sql, "HOJA DE VIDA", "ELIMINAR FOTO", idusuario, iplocal, this.getClass() + "-->EliminarAnexoHoja");
            return "0|Anexo eliminado con éxito";
        } catch (Exception ex) {

            return "1|" + ex.getMessage();
        }
    }

    public String GuardarHojaVida(HttpServletRequest request) {
        String IdEmpleado = request.getParameter("IdEmpleado");
        String Perfil = request.getParameter("Perfil");
        String Estudios = request.getParameter("Estudios");
        String Pregrado = request.getParameter("Pregrado");
        String Postgrado = request.getParameter("Postgrado");
        String OtrosE = request.getParameter("OtrosE");
        String UltimoC = request.getParameter("UltimoC");
        String Logros = request.getParameter("Logros");
        String Jefe = request.getParameter("Jefe");
        String[] TelefonoC = request.getParameterValues("TelefonoC");
        String[] TiempoL = request.getParameterValues("TiempoL");
        String[] NombreRF = request.getParameterValues("NombreRF");
        String[] RelacionRF = request.getParameterValues("RelacionRF");
        String[] TelefonoRF = request.getParameterValues("TelefonoRF");
        String[] NombreRP = request.getParameterValues("NombreRP");
        String[] RelacionRP = request.getParameterValues("RelacionRP");
        String[] TelefonoRP = request.getParameterValues("TelefonoRP");
        String[] NombreRL = request.getParameterValues("NombreRL");
        String[] CargoRL = request.getParameterValues("CargoRL");
        String[] TelefonoRL = request.getParameterValues("TelefonoRL");

        String Mensaje = "";

        try {
            int IdHoja = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT id from rrhh.hoja_vida where idempleado = " + IdEmpleado));

            if (!IdEmpleado.trim().equals(empleado)) {
                if (IdHoja == 0) {
                    if (Globales.PermisosSistemas("HOJA DE VIDA AGREGAR", idusuario) == 0) {
                        return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                    }
                } else if (Globales.PermisosSistemas("HOJA DE VIDA EDITAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
            }

            if (IdHoja == 0) {
                sql = "INSERT INTO rrhh.hoja_vida"
                        + "(idempleado, idusuario, perfil_profesional, estudios_secundarios, pregrado, postgrado, otros_estudios, ultimo_cargo, logros, jefe_inmediato, telefono_contacto, tiempo_laborado) "
                        + "VALUES (" + IdEmpleado + ", " + idusuario + ",'" + Perfil + "','" + Estudios + "','" + Pregrado + "','" + Postgrado + "','" + OtrosE + "','" + UltimoC + "','" + Logros + "','" + Jefe + "','" + TelefonoC + "','" + TiempoL + "');";
                if (Globales.DatosAuditoria(sql, "HOJA DE VIDA", "AGREGAR", idusuario, iplocal, this.getClass() + "-->GuardarHojaVida")) {
                    IdHoja = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT MAX(id) from rrhh.hoja_vida;"));
                    Mensaje = "0|Hoja de vida agregada con éxito";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            } else {
                sql = "UPDATE rrhh.hoja_vida SET idusuario=" + idusuario + ",fecha=now(), perfil_profesional='" + Perfil
                        + "',estudios_secundarios='" + Estudios + "',pregrado='" + Pregrado + "', postgrado='" + Postgrado
                        + "', otros_estudios='" + OtrosE + "',ultimo_cargo='" + UltimoC + "',logros='" + Logros
                        + "', jefe_inmediato='" + Jefe + "', telefono_contacto='" + TelefonoC + "',tiempo_laborado='" + TiempoL
                        + "' WHERE id=" + IdHoja;

                if (Globales.DatosAuditoria(sql, "HOJA DE VIDA", "EDITAR", idusuario, iplocal, this.getClass() + "-->GuardarHojaVida")) {
                    Mensaje = "0|Hoja de vida actualizada con éxito";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            }

            sql = "DELETE FROM rrhh.hoja_vida_referencia WHERE idhoja=" + IdHoja + ";";
            for (int x = 0; x < NombreRF.length; x++) {
                sql += "INSERT INTO rrhh.hoja_vida_referencia(idhoja, posicion, nombre, relacion, telefono, tipo) "
                        + "  VALUES(" + IdHoja + "," + x + ",'" + NombreRF[x] + "','" + RelacionRF[x] + "','" + TelefonoRF[x] + "','RF');";
            }

            for (int x = 0; x < NombreRP.length; x++) {
                sql += "INSERT INTO rrhh.hoja_vida_referencia(idhoja, posicion,nombre, relacion, telefono, tipo) "
                        + " VALUES(" + IdHoja + "," + x + ",'" + NombreRP[x] + "','" + RelacionRP[x] + "','" + TelefonoRP[x] + "','RP');";
            }

            for (int x = 0; x < NombreRL.length; x++) {
                sql += "INSERT INTO rrhh.hoja_vida_referencia(idhoja, posicion,nombre, cargo, telefono, tipo)"
                        + "  VALUES(" + IdHoja + "," + x + ",'" + NombreRL[x] + "','" + CargoRL[x] + "','" + TelefonoRL[x] + "','RL');";
            }
            Globales.DatosAuditoria(sql, "HOJA DE VIDA", "AGREGAR REFERENCIA", idusuario, iplocal, this.getClass() + "-->GuardarHojaVida");

            return Mensaje;

        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String Empleados_Nomina(HttpServletRequest request) {
        int id = Globales.Validarintnonull(request.getParameter("id"));
        int tipo = Globales.Validarintnonull(request.getParameter("tipo"));
        switch (tipo) {
            case 1:
                sql = "select e.id, tipodocumento || ' ' || documento as documento, nombrecompleto, c.descripcion as cargo, sueldo,"
                        + "          coalesce((select max(ne.id) from rrhh.nominas_empleados ne inner join rrhh.nominas_periodos np on ne.idperiodo = np.id "
                        + "							       inner join rrhh.nominas n on n.id = np.idnomina and n.periodo = np.numero"
                        + "		    WHERE ne.idempleado = e.id and np.idnomina = " + id + "),0) as activo, estado"
                        + " from rrhh.empleados e inner join rrhh.cargo c on c.id = e.idcargo order by nombrecompleto";
                break;
            case 2:
                sql = "select e.id, tipodocumento || ' ' || documento as documento, nombrecompleto, c.descripcion as cargo, sueldo,"
                        + "          coalesce((select max(ce.id) from rrhh.conceptos_empleados ce inner join rrhh.conceptos c on c.id = ce.idconcepto inner join  rrhh.nominas_periodos np on c.idperiodo = np.id WHERE ce.idempleado = e.id and ce.idconcepto = " + id + "),0) as activo, estado"
                        + " from rrhh.empleados e inner join rrhh.cargo c on c.id = e.idcargo order by nombrecompleto";
                break;
            case 3:
                sql = "select e.id, tipodocumento || ' ' || documento as documento, nombrecompleto, c.descripcion as cargo, sueldo,"
                        + "      coalesce((select max(ce.monto) from rrhh.constantes_empleados ce inner join rrhh.constantes c on c.id = ce.idconstante inner join  rrhh.nominas_periodos np on c.idperiodo = np.id WHERE ce.idempleado = e.id and ce.idconstante = " + id + "),0) as monto, estado"
                        + " from rrhh.empleados e inner join rrhh.cargo c on c.id = e.idcargo "
                        + " order by nombrecompleto";
                break;
        }

        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->Empleados_Nomina");
    }

    public String CambioCodigo(HttpServletRequest request) {
        int tipo = Globales.Validarintnonull(request.getParameter("tipo"));
        int codigo = Globales.Validarintnonull(request.getParameter("codigo"));
        int id = Globales.Validarintnonull(request.getParameter("id"));

        try {
            switch (tipo) {
                case 1:
                    sql = "select e.id, "
                            + "case when tipodocumento = 'CC' then 'Cédula Ciudadanía' when tipodocumento = 'CE' then 'Cédula Extrangería' when tipodocumento = 'PA' then 'Pasaporte' when tipodocumento = 'PP' THEN 'Permiso de Permanencia' end as tipodocumento, "
                            + "documento, nombrecompleto, c.descripcion as cargo, cuenta, b.descripcion as banco, sueldo, to_char(fechainicio,'yyyy-MM-dd') as fechainicio, to_char(fechafin,'yyyy-MM-dd') as fechafin, "
                            + "direccion, telefono, celular, email, e.estado"
                            + "from rrhh.empleados e inner join rrhh.cargo c on c.id = e.idcargo "
                            + "                    inner join bancos b on b.id = e.idbanco order by 2";

                    break;
            }
            return "";
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public int MaxCodigo(HttpServletRequest request, int _tipo, int _nomina, int _tipoconcepto) {
        int tipo;
        int nomina;
        int tipoconcepto;
        if (request == null) {
            tipo = _tipo;
            nomina = _nomina;
            tipoconcepto = _tipoconcepto;
        } else {
            tipo = Globales.Validarintnonull(request.getParameter("tipo"));
            nomina = Globales.Validarintnonull(request.getParameter("nomina"));
            tipoconcepto = Globales.Validarintnonull(request.getParameter("tipoconcepto"));
        }

        int codigo = 0;
        switch (tipo) {
            case 1:
                sql = "select max(codigo) from rrhh.nominas";
                codigo = Globales.Validarintnonull(Globales.ObtenerUnValor(sql)) + 1;
                return codigo;
            case 2:
                int codigoinicial = Globales.Validarintnonull(Globales.ObtenerUnValor("select codigoinicial from rrhh.tipo_concepto where id = " + tipoconcepto));
                sql
                        = "select max(c.codigo) + 1 "
                        + "            from rrhh.conceptos c "
                        + " where idperiodo = " + nomina + " and idtipo = " + tipoconcepto;
                codigo = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
                if (codigo == 0) {
                    codigo = codigoinicial;
                }
                return codigo;
            case 3:
                sql = "select max(codigo+1) from rrhh.constantes where idperiodo=" + nomina;
                codigo = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
                if (codigo == 0) {
                    codigo = 10000;
                }
                return codigo;

        }
        return 0;
    }

    public String GuardarNomina(HttpServletRequest request) {
        int IdNomina = Globales.Validarintnonull(request.getParameter("IdNomina"));
        int CodNomina = Globales.Validarintnonull(request.getParameter("CodNomina"));
        String TipoNomina = request.getParameter("TipoNomina");
        String TituloNom = request.getParameter("TituloNom");
        String DescripcionNom = request.getParameter("DescripcionNom");
        String FechaIniNom = request.getParameter("DescripcionNom");
        String EstadoNom = request.getParameter("EstadoNom");
        String[] seleccion_nom = request.getParameterValues("seleccion_nom");
        String fechainicio = "";
        String fechafinal = "";
        DateFormat FormatoDia = new SimpleDateFormat("dd");
        DateFormat Formato = new SimpleDateFormat("yyyy-MM-dd");

        Date startDate = Globales.ValidarFecha(Globales.FechaActual(5));
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(startDate);
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        calendar.add(Calendar.DAY_OF_YEAR, -1);
        Date endDate = calendar.getTime();
        String mensaje = "";
        String TipoPeriodo = "PRIMERO";
        int IdPeriodo = 0;
        try {
            if (IdNomina == 0) {
                sql = "INSERT INTO rrhh.nominas(codigo, tipo, titulo, descripcion, fechainicio, estado)"
                        + " VALUES ('" + CodNomina + "','" + TipoNomina + "','" + TituloNom.trim().toUpperCase() + "','" + DescripcionNom + "','" + FechaIniNom + "','" + EstadoNom + "');SELECT MAX(id) FROM rrhh.nominas;";

                IdNomina = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
                switch (TipoNomina) {
                    case "Quincenal":
                        fechainicio = FechaIniNom;
                        if (Globales.Validarintnonull(FechaIniNom.split("-")[2]) < 15) {
                            fechafinal = FechaIniNom.split("-")[0] + "-" + FechaIniNom.split("-")[1] + "-15";
                            TipoPeriodo = "PRIMERO";
                        } else {
                            fechafinal = Formato.format(endDate);
                            TipoPeriodo = "SEGUNDO";
                        }
                        break;
                    case "Mensual":
                        fechainicio = FechaIniNom;
                        fechafinal = Formato.format(endDate);
                        break;
                    case "Especial":
                        fechainicio = FechaIniNom;
                        fechafinal = fechainicio;
                        break;

                }

                sql = "INSERT INTO rrhh.nominas_periodos(idnomina, numero, fechainicio, fechafinal, tipo)"
                        + "  VALUES (" + IdNomina + ",1,'" + fechainicio + "','" + fechafinal + "','" + TipoPeriodo + "');select max(id) from rrhh.nominas_periodos;";
                IdPeriodo = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
                mensaje = "0|Nómina guardado con el código número |" + IdNomina + "|" + CodNomina;
            } else {
                IdPeriodo = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT np.id FROM rrhh.nominas n inner join rrhh.nominas_periodos np on np.numero = n.periodo WHERE n.id = " + IdNomina));
                if (NominaCalculada(IdPeriodo, 1).equals("SI")) {
                    return "1|No se puede editar una nómina calculada";
                }

                sql = "UPDATE rrhh.nominas SET tipo='" + TipoNomina + "', titulo='" + TituloNom.trim().toUpperCase() + "', descripcion='" + DescripcionNom + "', " + "estado='" + EstadoNom + "' WHERE id=" + IdNomina + ";";
                sql += "select * from rrhh.repararconceptos(" + IdNomina + ");";

                if (Globales.DatosAuditoria(sql, "NOMINA", "GUARDAR", idusuario, iplocal, this.getClass() + "-->GuardarNomina")) {
                    mensaje = "0|Nómina actualizada con el código número |" + IdNomina + "|" + CodNomina;
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }

            }

            sql = "DELETE FROM rrhh.nominas_empleados where idperiodo = " + IdPeriodo + ";";
            if (seleccion_nom != null) {
                for (int x = 0; x < seleccion_nom.length; x++) {
                    sql = "INSERT INTO rrhh.nominas_empleados(idperiodo, idempleado) VALUES(" + IdPeriodo + "," + seleccion_nom[x] + ");";
                }
            }
            if (Globales.DatosAuditoria(sql, "NOMINA", "GUARDAR ", idusuario, iplocal, this.getClass() + "-->GuardarNomina")) {
                return "0|Nomina guardada con éxito";
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String GuardarConcepto(HttpServletRequest request) {
        int IdConcepto = Globales.Validarintnonull(request.getParameter("IdConcepto"));
        int NominaConcep = Globales.Validarintnonull(request.getParameter("NominaConcep"));
        int CodConcepto = Globales.Validarintnonull(request.getParameter("CodConcepto"));
        int TipoConcepto = Globales.Validarintnonull(request.getParameter("TipoConcepto"));
        String TituloConcep = request.getParameter("TituloConcep");
        String DescripcionConcep = request.getParameter("DescripcionConcep");
        String Formula = request.getParameter("Formula");
        String FormulaAporte = request.getParameter("FormulaAporte");
        String PeriodoConcepto = request.getParameter("PeriodoConcepto");
        String EstadoConcep = request.getParameter("EstadoConcep");
        int Orden = Globales.Validarintnonull(request.getParameter("Orden"));
        String Global = request.getParameter("Global");
        String Vacaciones = request.getParameter("Vacaciones");
        String Cesantia = request.getParameter("Cesantia");
        String Prima = request.getParameter("Prima");
        String Visible = request.getParameter("Visible");
        String[] seleccionado_concep = request.getParameterValues("seleccionado_concep");

        if (NominaCalculada(NominaConcep, 1).equals("SI")) {
            return "1|No se puede editar conceptos de una nómina calculada";
        }

        Global = (Global == null ? "NO" : "SI");
        Vacaciones = (Vacaciones == null ? "NO" : "SI");
        Cesantia = (Cesantia == null ? "NO" : "SI");
        Prima = (Prima == null ? "NO" : "SI");
        FormulaAporte = (FormulaAporte == null ? "" : FormulaAporte);
        Visible = (Visible == null ? "NO" : "SI");

        String mensaje = "";

        try {
            if (IdConcepto == 0) {

                if (Globales.PermisosSistemas("CONCEPTO NOMINA GUARDAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }

                CodConcepto = MaxCodigo(null, 2, NominaConcep, TipoConcepto);
                sql = "INSERT INTO rrhh.conceptos(idperiodo, codigo, idtipo, titulo, descripcion, formula, formulaaporte, periodo, estado, globales, vacaciones, cesantia, prima, orden, visible)"
                        + " VALUES (" + NominaConcep + "," + CodConcepto + ",'" + TipoConcepto + "','" + TituloConcep.trim().toUpperCase() + "','" + DescripcionConcep + "','" + Formula.toLowerCase() + "','" + FormulaAporte.toLowerCase() + "','" + PeriodoConcepto + "','" + EstadoConcep + "','" + Global + "','" + Vacaciones + "','" + Cesantia + "','" + Prima + "'," + Orden + ",'" + Visible + "');";
                if (Globales.DatosAuditoria(sql, "CONCEPTO NOMINA", "GUARDAR", idusuario, iplocal, this.getClass() + "-->GuardarConcepto")) {
                    IdConcepto = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT MAX (id) FROM rrhh.conceptos;"));
                    mensaje = "0|Concepto agregado con el código número |" + IdConcepto + "|" + CodConcepto;
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            } else {

                if (Globales.PermisosSistemas("CONCEPTO NOMINA EDITAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }

                int idtipo = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT idtipo from rrhh.conceptos where id = " + IdConcepto));
                if (idtipo != TipoConcepto) {
                    CodConcepto = MaxCodigo(null, 2, NominaConcep, TipoConcepto);
                }

                sql = "UPDATE rrhh.conceptos"
                        + "SET codigo=" + CodConcepto + ", idtipo='" + TipoConcepto + "', titulo='" + TituloConcep + "', descripcion='" + DescripcionConcep + "'," + "formula ='" + Formula.toLowerCase() + "', formulaaporte='" + FormulaAporte.toLowerCase() + "', periodo='" + PeriodoConcepto + "'," + "globales='" + Global + "', vacaciones='" + Vacaciones + "',cesantia ='" + Cesantia + "', prima='" + Prima + "'," + "estado= '" + EstadoConcep + "', orden=" + Orden + ",visible='" + Visible + "' WHERE id = " + IdConcepto;
                if (Globales.DatosAuditoria(sql, "CONCEPTO NOMINA", "EDITAR", idusuario, iplocal, this.getClass() + "-->GuardarOtroRegistro")) {
                    mensaje = "0|Concepto actualizada con el código número |" + IdConcepto + "|" + CodConcepto;
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            }
            sql = "DELETE FROM rrhh.conceptos_empleados where idconcepto = " + IdConcepto + ";";
            if (seleccionado_concep != null && Global.equals("NO")) {
                for (int x = 0; x < seleccionado_concep.length; x++) {
                    sql += "INSERT INTO rrhh.conceptos_empleados(idconcepto, idempleado) VALUES(" + IdConcepto + "," + seleccionado_concep[x] + ");";
                }
            }
            Globales.DatosAuditoria(sql, "CONCEPTO", "ELIMINAR", idusuario, iplocal, this.getClass() + "-->GuardarConcepto");

            return mensaje;
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String GuardarConstante(HttpServletRequest request) {
        int IdConstante = Globales.Validarintnonull(request.getParameter("IdConstante"));
        int NominaConsta = Globales.Validarintnonull(request.getParameter("NominaConsta"));
        int CodConstante = Globales.Validarintnonull(request.getParameter("CodConstante"));
        String TitConstante = request.getParameter("TitConstante");
        String DescConstante = request.getParameter("DescConstante");
        String Reiniciar = request.getParameter("Reiniciar");
        String EstConstante = request.getParameter("EstConstante");
        String[] seleccionado_constante = request.getParameterValues(" seleccionado_constante");
        String[] seleccionado_monto = request.getParameterValues("seleccionado_monto");
        double MontoConstante = Globales.ValidardoublenoNull(request.getParameter("MontoConstante"));

        Reiniciar = (Reiniciar == null ? "NO" : "SI");

        if (NominaCalculada(NominaConsta, 1).equals("SI")) {
            return "1|No se puede editar una constante de una nómina calculada";
        }

        String mensaje = "";

        try {
            if (IdConstante == 0) {
                if (Globales.PermisosSistemas("CONSTANTE NOMINA GUARDAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                CodConstante = MaxCodigo(null, 3, NominaConsta, 0);
                sql = "INSERT INTO rrhh.constantes(idperiodo, codigo, titulo, descripcion, monto, estado)"
                        + "VALUES (" + NominaConsta + "," + CodConstante + ",'" + TitConstante.trim() + "','" + DescConstante + "'," + MontoConstante + ",'" + EstConstante + "');";
                if (Globales.DatosAuditoria(sql, "CONSTANTE NOMINA", "GUARDAR", idusuario, iplocal, this.getClass() + "-->GuardarConcepto")) {
                    IdConstante = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT MAX(id) FROM rrhh.constantes;"));
                    mensaje = "0|Constante agregado con el código número |" + IdConstante + "|" + CodConstante;
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            } else {
                if (Globales.PermisosSistemas("CONSTANTE NOMINA EDITAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "UPDATE rrhh.constantes"
                        + "SET codigo=" + CodConstante + ", titulo='" + TitConstante + "', descripcion='" + DescConstante + "'," + "monto =" + MontoConstante + ", estado='" + EstConstante + "' WHERE id = " + IdConstante;
                if (Globales.DatosAuditoria(sql, "CONSTANTE", "ACTUALIZAR", idusuario, iplocal, this.getClass() + "-->GuardarConstante")) {
                    mensaje = "0|Constante actualizada con el código número |" + IdConstante + "|" + CodConstante;
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            }
            sql = "DELETE FROM rrhh.constantes_empleados where idconstante = " + IdConstante + ";";
            if (seleccionado_constante != null) {
                for (int x = 0; x < seleccionado_constante.length; x++) {
                    sql += "INSERT INTO rrhh.constantes_empleados(idconstante, idempleado, monto) VALUES(" + IdConstante + "," + seleccionado_constante[x] + "," + seleccionado_monto[x] + ");";
                }
            }
            Globales.DatosAuditoria(sql, "CONSTANTE", "GUARDAR", idusuario, iplocal, this.getClass() + "-->GuardarConstante");
            return mensaje;
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public int PeriodoActual(HttpServletRequest request) {
        int nomina = Globales.Validarintnonull(request.getParameter("nomina"));

        sql = "select p.id"
                + "  from rrhh.nominas n inner join rrhh.nominas_periodos p on p.idnomina = n.id and p.numero = n.periodo  where n.id = " + nomina;
        return Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
    }

    public String CalNominaEmpleado(HttpServletRequest request) {
        int periodo = Globales.Validarintnonull(request.getParameter("periodo"));
        int empleado = Globales.Validarintnonull(request.getParameter("empleado"));

        if (NominaCalculada(periodo, 1).equals("NO")) {
            sql = "select * from rrhh.calcularnomina(" + periodo + "," + empleado + ") WHERE _reporte = 0;";
        } else {
            sql = "SELECT orden as _numero, asignacion as _asignaciones, deduccion as _deducciones, aporteemp as _aporteemp, aportepat as _aportepat, "
                    + "         formula as _formula, formulaaporte as _formulaapo, codigo as _codigo, concepto as _concepto"
                    + "         FROM rrhh.salida "
                    + "     WHERE idperiodo = " + periodo + " and idempleado = " + empleado + " "
                    + "     ORDER BY orden";
        }
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->CalNominaEmpleado");
    }

    public String CalcularNomina(HttpServletRequest request) {
        int periodo = Globales.Validarintnonull(request.getParameter("periodo"));
        if (NominaCalculada(periodo, 1).equals("SI")) {
            return "1|Esta nómina ya se encuentra calculada";
        }
        try {
            sql = "select * from rrhh.calcularnomina(" + periodo + ",0) WHERE _reporte = 0;";
            if (Globales.DatosAuditoria(sql, "NOMINA", "CALCULAR", idusuario, iplocal, this.getClass() + "-->CalcularNomina")) {
                return "0|Nómina calculada con éxito";
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String CerrarPeriodo(HttpServletRequest request) {
        int periodo = Globales.Validarintnonull(request.getParameter("periodo"));
        try {
            sql = "SELECT rrhh.cerrar_periodo(" + periodo + ")";
            if (Globales.DatosAuditoria(sql, "NOMINA", "CERRAR PERIODO", idusuario, iplocal, this.getClass() + "-->CerrarPeriodo")) {
                return "0|Periodo de nómina cerrado con éxito|";
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String AbrirPeriodo(HttpServletRequest request) {

        int periodo = Globales.Validarintnonull(request.getParameter("periodo"));
        int nomina = Globales.Validarintnonull(request.getParameter("nomina"));

        if (NominaCalculada(periodo, 1).equals("NO")) {
            return "1|No se puede abrir este periodo, ya que el periodo no se encuentra calculado";
        }
        if (NominaCalculada(periodo, 2).equals("NO")) {
            return "1|Esta nómina no se encuentra cerrada";
        }
        try {

            int NumPeriodo = 0;
            sql = "SELECT numero"
                    + "  FROM rrhh.nominas_periodos "
                    + " where id = " + periodo;
            datos = Globales.Obtenerdatos(sql, this.getClass() + "-->AbrirPeriodo", 1);
            NumPeriodo = Globales.Validarintnonull(datos.getString(+1));

            sql = "SELECT calculado from rrhh.nominas_periodos WHERE numero = " + NumPeriodo;
            String calculado = Globales.ObtenerUnValor(sql);
            if (calculado.equals("SI")) {
                return "1|No se puede abrir este periodo, ya que el periodo siguiente se encuentra calculado";
            }

            sql = "UPDATE rrhh.nominas_periodos SET cerrado = 'NO' WHERE id = " + periodo + ";";
            sql += "UPDATE rrhh.nominas SET periodo = " + (NumPeriodo - 1) + " WHERE id = " + nomina;
            if (Globales.DatosAuditoria(sql, "NOMINA", "ACTUALIZAR", idusuario, iplocal, this.getClass() + "-->AbrirPeriodo")) {
                return "0|Periodo de nómina abierto con éxito|";
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }

        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String ReversarNomina(HttpServletRequest request) {
        int periodo = Globales.Validarintnonull(request.getParameter("periodo"));
        if (NominaCalculada(periodo, 1).equals("NO")) {
            return "1|Esta nómina no se encuentra calculada";
        }
        if (NominaCalculada(periodo, 2).equals("SI")) {
            return "1|No se puede reversar una nómina cerrada";
        }
        try {
            sql = "DELETE FROM rrhh.salida where idperiodo = " + periodo + ";";
            sql += "DELETE FROM rrhh.nomina_historico where idperiodo = " + periodo + ";";
            sql += "UPDATE rrhh.nominas_periodos SET calculado = 'NO' WHERE id = " + periodo + ";";
            sql += "UPDATE rrhh.prestamo_cuota set idperiodo = 0  where idperiodo = " + periodo + ";";

            if (Globales.DatosAuditoria(sql, "NOMINA", "ELIMINAR", idusuario, iplocal, this.getClass() + "-->ReversarNomina")) {
                return "0|Nómina reversada con éxito|";
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String ResumenNomina(HttpServletRequest request) {
        int periodo = Globales.Validarintnonull(request.getParameter("periodo"));

        if (NominaCalculada(periodo, 1).equals("NO")) {
            return "[]|[]|[]";
        }

        sql = "SELECT orden, codigo, concepto, formula, formulaaporte, "
                + "      SUM(asignacion) AS asignacion, SUM(deduccion) AS deduccion, SUM(aporteemp) AS aporteemp, SUM(aportepat) AS aportepat"
                + "  FROM rrhh.salida"
                + "  WHERE idperiodo = " + periodo + " "
                + "  GROUP BY orden, codigo, concepto, formula, formulaaporte"
                + "  ORDER BY orden";

        String sql2 = "SELECT row_number() OVER(order by documento) as fila, tipodocumento || ' ' || documento as documento, nombrecompleto as empleado, sueldo, c.descripcion as cargo, "
                + "      SUM(asignacion) AS asignacion, SUM(deduccion) AS deduccion, SUM(aporteemp) AS aporteemp, SUM(aportepat) AS aportepat,"
                + "      SUM(asignacion - deduccion - aporteemp)  as totalpagar"
                + "      FROM rrhh.salida s inner join rrhh.empleados e on e.id = idempleado"
                + "                          inner join rrhh.cargo c on c.id = idcargo"
                + "  WHERE idperiodo = " + periodo + " "
                + "  GROUP BY tipodocumento, documento, nombrecompleto, sueldo, c.descripcion"
                + "  ORDER BY nombrecompleto";
        String sql3 = "SELECT calculado, cerrado FROM rrhh.nominas_periodos WHERE id = " + periodo;

        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ResumenNomina") + "|" + Globales.ObtenerDatosJSon(sql2, this.getClass() + "-->ResumenNomina") + "|" + Globales.ObtenerDatosJSon(sql3, this.getClass() + "-->ResumenNomina");
    }

    /*
    public String ImportarAsistencia(HttpServletRequest request) {

        int periodo = Globales.Validarintnonull(request.getParameter("periodo"));
        if (NominaCalculada(periodo, 1).equals("SI")) {
            return "1|No se puede editar una nómina calculada";
        }

        DateTime fecha;
        DateTime fechainicio = DateTime.Parse("1900-01-01");
        DateTime fechafin = DateTime.Parse("1900-01-01");

        sql = "SELECT to_char(fechahuellafin,'yyyy-MM-dd') as fechahuellafin FROM rrhh.nominas_periodos WHERE id < " + periodo + " ORDER BY id desc";
        datos = Globales.Obtenerdatos(sql, this.getClass() + "-->ImportarAsistencia", 1);
        if (datos.Rows.Count > 0){
            if (datos.Rows[0][0] != null) {
                fechainicio = DateTime.Parse(datos.Rows[0][0].ToString());
            }
        }

        sql = "SELECT to_char(fechainicio,'yyyy-MM-dd') as fechainicio,  to_char(fechafinal,'yyyy-MM-dd') as fechafin FROM rrhh.nominas_periodos WHERE id = " + periodo + " ORDER BY id desc";
            datos = Globales.Obtenerdatos(sql, this.getClass() + "-->ImportarAsistencia", 1);
        if (datos.Rows.Count > 0) {
            if (fechainicio.ToString("yyyy-MM-dd") == "1900-01-01") {
                fechainicio = DateTime.Parse(datos.getString("fechainicio"));
            }
            fechafin = DateTime.Parse(datos.getString("fechafin"));
        }

        String ruta = HostingEnvironment.MapPath("~/uploads/" + Session["ArchivoExcel"].ToString());
        String[] lineas;
        String empleado;
        String huella;
        String fechah;
        String horaini;
        String horafin;
        String horas;

        try {

            lineas = System.IO.File.ReadAllLines(ruta);
            sql = "";
            using(StreamWriter writer = new StreamWriter(ruta)){
             foreach(string line in lineas){
                        if (line.trim() != "") {
                        empleado = line.Substring(0, 36).Trim();
                        huella = line.Substring(36, 6).Trim();
                        fechah = line.Substring(57, 12).Trim();
                        horaini = Globales.QuitarCaracter(line.Substring(69, 9), ".");
                        if (line.Length >= 100) {
                            horafin = Globales.QuitarCaracter(line.Substring(93, 9), ".");
                            horas = line.Substring(117, 9).Trim().Replace(',', '.');
                        } else {
                            horafin = "";
                            horas = "0";
                        }

                        if (Globales.Validarintnonull(huella) > 0) {
                            if (DateTime.TryParse(Globales.ConvertirFecha(fechah,2),out fecha)) {

                                    if (fecha >= fechainicio && fecha <= fechafin) {
                                    sql
                                            += "INSERT INTO rrhh.asistencia(idperiodo, huella, empleado, horaini, horafin, horas, fecha)"
                                            + "      VALUES(" + periodo + ", " + huella + ",'" + empleado + "'  ,'" + horaini + "'  ," + (horafin != "    " ? "  '" + horafin + "'  " : "  null") + "  ," + horas + "  ,'" + fecha.ToString("yyyy-MM-dd") + "'  );";

                                }
                            }
                        }
                    }
                }
            }

            if (!sql.equals("")) {
                sql = "DELETE FROM rrhh.asistencia WHERE idperiodo = " + periodo + ";" + sql;
                Globales.Obtenerdatos(sql, this.getClass() + "-->ImportarAsistencia", 1);
                return "0|Registro de asistencia cargada con éxito";
            } else {
                return "1|No hay registro de asistencia para este periodo";
            }
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }

    }

    /*
    public String ReporteNomina(HttpServletRequest request) {
        int periodo = Globales.Validarintnonull(request.getParameter("periodo"));
        int tipo = Globales.Validarintnonull(request.getParameter("tipo"));
        int concepto = Globales.Validarintnonull(request.getParameter("concepto"));
        int empleado = Globales.Validarintnonull(request.getParameter("empleado"));
        String reporte = "";
        String sql2 = "";
        switch (tipo) {
            case 1:
                sql = "SELECT orden, codigo, concepto, formula, formulaaporte, "
                        + "      SUM(asignacion) AS asignacion, SUM(deduccion) AS deduccion, SUM(aporteemp) AS aporteemp, SUM(aportepat) AS aportepat"
                        + "  FROM rrhh.salida"
                        + "  WHERE idperiodo = " + periodo + ""
                        + "  GROUP BY orden, codigo, concepto, formula, formulaaporte"
                        + " 2  ORDER BY orden";
                reporte = "RpResumenNomina";
                break;
            case 2:
                sql = "SELECT row_number() OVER(order by documento) as fila, tipodocumento || ' ' || documento as documento, nombrecompleto as empleado, sueldo, c.descripcion as cargo, "
                        + "     SUM(asignacion) AS asignacion, SUM(deduccion) AS deduccion, SUM(aporteemp) AS aporteemp, SUM(aportepat) AS aportepat, tipocuenta || ' ' || cuenta as cuenta, b.descripcion as banco,"
                        + "     SUM(asignacion - deduccion - aporteemp)  as totalpagar"
                        + "     FROM rrhh.salida s inner join rrhh.empleados e on e.id = idempleado"
                        + "                         inner join rrhh.cargo c on c.id = idcargo"
                        + "                 inner join bancos b on b.id = idbanco"
                        + " WHERE idperiodo = " + periodo + " "
                        + " GROUP BY tipodocumento, documento, nombrecompleto, sueldo, c.descripcion, b.descripcion, cuenta, tipocuenta"
                        + " ORDER BY nombrecompleto";
                reporte = "RpResumenEmpleado";
                break;
            case 3:
                sql
                        = "SELECT row_number() OVER(order by documento) as fila, tipodocumento || ' ' || documento as documento, nombrecompleto as empleado, sueldo, c.descripcion as cargo, "
                        + "                SUM(aporteemp) AS aporteemp,"
                        + " SUM"
                        + " (aportepat"
                        + " ) AS aportepat,"
                        + "  tipocuenta || ' ' || cuenta as cuenta"
                        + " , b.descripcion as banco,"
                        + "                  SUM(asignacion - deduccion - aporteemp)  as totalpagar,s.codigo"
                        + "  , s.concepto FROM rrhh.salida s inner join rrhh.empleados e on e"
                        + "  .id = idempleado"
                        + "  inner join rrhh.cargo c on c"
                        + "  .id = idcargo"
                        + " inner join bancos b on b"
                        + " .id = idbanco"
                        + " where(aportepat > 0 OR aporteemp > 0) AND idperiodo = " + periodo + " "
                        + " GROUP BY tipodocumento"
                        + " , documento, nombrecompleto, sueldo, c.descripcion, b.descripcion, cuenta, s.codigo, s.concepto, tipocuenta ORDER BY nombrecompleto";
                reporte = "RpListadoConceptoAporte";
                break;
            case 4:
                sql
                        = "SELECT row_number() OVER(order by documento) as fila, tipodocumento || ' ' || documento as documento, nombrecompleto as empleado, sueldo, c.descripcion as cargo, "
                        + "asignacion, deduccion, aporteemp, aportepat, tipocuenta || ' ' || cuenta as cuenta,b.descripcion as banco,asignacion - deduccion - aporteemp as totalpagar,"
                        + "s.codigo, s.concepto FROM rrhh.salida s inner join rrhh.empleados e on e.id = idempleadoinner join rrhh.cargo c on c.id = idcargo"
                        + "inner join bancos b on b.id = idbanco WHERE idperiodo = " + periodo + " ORDER BY nombrecompleto";
                reporte = "RpPagoNominaEmpresa";
                break;
            case 5:
                sql = "SELECT * FROM rrhh.pagonomina(" + periodo + "," + empleado + ")";
                reporte = "RpPagoNominaEmpleado";
                break;
        }

        //int numperiodo = Globales.Validarintnonull(Globales.ObtenerUnValor("select numero from rrhh.nominas_periodos where id = " + periodo, conexion, 0));
        sql2
                = "SELECT codigo || ' ' || titulo || ' (' || n.tipo || ') ' || p.numero || ' - Inicio: ' || to_char(p.fechainicio,'dd/MM/yyyy') || ' Fin: ' || to_char(p.fechafinal,'dd/MM/yyyy') as descripcion"
                + "ROM rrhh.nominas n inner join  rrhh.nominas_periodos p on p.idnomina = n.id WHERE p.id = " + periodo;
        String titulo = Globales.ObtenerUnValor(sql2);

        datos = Globales.Obtenerdatos(sql);

        if (datos.Rows.Count == 0) {
            return "1|No hay nada que reportar";
        }

        String DirectorioReportesRelativo = "~/Reportes/";
        String directorio = "~/DocumPDF/";
        String unico = reporte + "_" + DateTime.Now.ToString("yyyyMMddHHmmss");
        String urlArchivo = String.Format("{0}.{1}", reporte, "rdlc");

        String urlArchivoGuardar;
        urlArchivoGuardar = String.Format("{0}.{1}", unico, "pdf");

        String FullPathReport = String.Format("{0}{1}",
                this.HttpContext.Server.MapPath(DirectorioReportesRelativo),
                urlArchivo);
        String FullGuardar = String.Format("{0}{1}",
                this.HttpContext.Server.MapPath(directorio),
                urlArchivoGuardar);
        Microsoft.Reporting.WebForms.ReportViewer Reporte = new ReportViewer();
        Reporte.Reset();
        Reporte.LocalReport.ReportPath = FullPathReport;
        Reporte.LocalReport.EnableExternalImages = true;

        ReportParameter[] parameters = new ReportParameter[1];
        parameters[0] = new ReportParameter("titulo", titulo);
        Reporte.LocalReport.SetParameters(parameters);

        ReportDataSource DataSource = new ReportDataSource("DataSet1", datos);
        Reporte.LocalReport.DataSources.Add(DataSource);

        Reporte.LocalReport.Refresh();
        byte[] ArchivoPDF = Reporte.LocalReport.Render("PDF");
        FileStream fs = new FileStream(FullGuardar, FileMode.Create);
        fs.Write(ArchivoPDF, 0, ArchivoPDF.Length);
        fs.Close();

        return "0|" + urlArchivoGuardar;

    }
     */
    public String GuardarAutorizacion(HttpServletRequest request) {
        int IdAutorizacion = Globales.Validarintnonull(request.getParameter("IdAutorizacion"));
        int Empleado = Globales.Validarintnonull(request.getParameter("Empleado"));
        int Equipo = Globales.Validarintnonull(request.getParameter("Equipo"));
        int Intervalo = Globales.Validarintnonull(request.getParameter("Intervalo"));
        int Procedimiento = Globales.Validarintnonull(request.getParameter("Procedimiento"));
        int AreaCalibracion = Globales.Validarintnonull(request.getParameter("AreaCalibracion"));
        int AreaMantenimiento = Globales.Validarintnonull(request.getParameter("AreaMantenimiento"));
        int Ciudad = Globales.Validarintnonull(request.getParameter("Ciudad"));
        //ActulizarImagenes();

        try {
            String Cargo = Globales.ObtenerUnValor("SELECT idcargo FROM rrhh.empleados where id = " + Empleado);
            if (IdAutorizacion == 0) {
                if (Globales.PermisosSistemas("AUTORIZACION AL PERSONAL AGREGAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "INSERT INTO rrhh.autorizacion_personal(idempleado, idcargo, idequipo, idintervalo, idprocedimiento, "
                        + "idarecalibracion, idaremantenimiento, idusuario, idciudad)"
                        + " VALUES(" + Empleado + ", " + Cargo + ", " + Equipo + ", " + Intervalo + "," + Procedimiento + "," + AreaCalibracion + "," + AreaMantenimiento + "," + idusuario + "," + Ciudad + "); ";
                if (Globales.DatosAuditoria(sql, "AUTORIZACION AL PERSONAL", "AGREGAR", idusuario, iplocal, this.getClass() + "-->GuardarAutorizacion")) {
                    IdAutorizacion = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT MAX(id) FROM rrhh.autorizacion_personal;"));
                    return "0|Hoja de vida agregada con éxito";
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }

            } else {
                if (Globales.PermisosSistemas("AUTORIZACION AL PERSONAL EDITAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                String Estado = Globales.ObtenerUnValor("SELECT estado FROM rrhh.autorizacion_personal where id = " + IdAutorizacion);
                if (Estado.equals("Suspendido")) {
                    return "1|No se puede editar una autorización con estado " + Estado;
                }
                sql = "UPDATE rrhh.autorizacion_personal SET idempleado=" + Empleado + ",idcargo=" + Cargo + ",idequipo=" + Equipo + ",idintervalo=" + Intervalo
                        + ",idprocedimiento=" + Procedimiento + ", idarecalibracion=" + AreaCalibracion + ",idaremantenimiento=" + AreaMantenimiento
                        + ", fehaedicion=now(), idusuarioedito=" + idusuario + ", idciudad=" + Ciudad + " WHERE id = " + IdAutorizacion;
                Globales.DatosAuditoria(sql, "AUTORIZACION AL PERSONAL", "ACTUALIZAR", idusuario, iplocal, this.getClass() + "-->GuardarAutorizacion");
            }
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }

        return "0|" + String.valueOf(IdAutorizacion) + "|" + nombreusuario + "|" + Globales.FechaActual(2);

    }

    public String ConsultarAutorizacion(HttpServletRequest request) {
        int empleado = Globales.Validarintnonull(request.getParameter("empleado"));
        int cargo = Globales.Validarintnonull(request.getParameter("cargo"));
        int procedimiento = Globales.Validarintnonull(request.getParameter("procedimiento"));
        String estado = request.getParameter("estado");
        int magnitud = Globales.Validarintnonull(request.getParameter("magnitud"));
        int equipo = Globales.Validarintnonull(request.getParameter("equipo"));
        int intervalo = Globales.Validarintnonull(request.getParameter("intervalo"));
        int areamant = Globales.Validarintnonull(request.getParameter("areamant"));
        int areacalib = Globales.Validarintnonull(request.getParameter("areacalib"));
        String fechad = request.getParameter("fechad");
        String fechah = request.getParameter("fechah");

        String Busqueda = "WHERE to_char(ap.fecha,'yyyy-MM-dd') >= '" + fechad + "' and to_char(ap.fecha,'yyyy-MM-dd') <= '" + fechah + "'";

        if (empleado > 0) {
            Busqueda += " AND e.id = " + empleado;
        }
        if (cargo > 0) {
            Busqueda += " AND c.id = " + cargo;
        }
        if (procedimiento > 0) {
            Busqueda += " AND p.id = " + procedimiento;
        }
        if (estado.trim() != "0") {
            Busqueda += " AND ap.estado = '" + estado + "' ";
        }
        if (magnitud > 0) {
            Busqueda += " AND m.id = " + magnitud;
        }
        if (equipo > 0) {
            Busqueda += " AND eq.id = " + equipo;
        }
        if (intervalo > 0) {
            Busqueda += " AND i.id = " + intervalo;
        }
        if (areamant > 0) {
            Busqueda += " AND mam.id = " + areamant;
        }
        if (areacalib > 0) {
            Busqueda += " AND mac.id = " + areacalib;
        }

        sql = "SELECT ap.id, to_char(ap.fecha,'yyyy/MM/dd HH24:MI') as fecha, ap.estado, e.nombrecompleto || ' (' || tipodocumento || ' ' || e.documento  as empleado,"
                + "     idusuariosus, fechasus, observacionsus, to_char(fehaedicion,'yyyy/MM/dd HH24:MI') as fehaedicion, idusuarioedito, c.descripcion as cargo,"
                + "     eq.descripcion as equipo, m.descripcion as magnitud, p.descripcion as procedimiento, ci.descripcion as ciudad, mac.descripcion as areacalibracion,"
                + "     mam.descripcion as areamantenimiento, u.nombrecompleto as usuario,"
                + "     to_char(ap.fechasus,'yyyy/MM/dd HH24:MI') || '<br>' || us.nomusu as suspendido,"
                + "     to_char(ap.fehaedicion,'yyyy/MM/dd HH24:MI') || '<br>' || ua.nomusu as actualizado,"
                + "     case when ap.idintervalo = 0 then 'VER ESPECIFICACIONES' ELSE '(' || desde || ' a ' || hasta || ') ' || medida end as intervalo,"
                + "     ' ' as imprimir"
                + "FROM rrhh.autorizacion_personal ap INNER JOIN rrhh.empleados e on e.id = ap.idempleado"
                + "	                     INNER JOIN rrhh.cargo c on c.id = ap.idcargo"
                + "	                     INNER JOIN equipo eq on eq.id = ap.idequipo"
                + "	                     INNER JOIN magnitudes m on m.id = eq.idmagnitud"
                + "	                     INNER JOIN procedimiento p on p.id = ap.idprocedimiento"
                + "	                     INNER JOIN magnitudes mac on mac.id = ap.idarecalibracion"
                + "	                     INNER JOIN magnitudes mam on mam.id = ap.idaremantenimiento"
                + "	                     INNER JOIN seguridad.rbac_usuario u on u.idusu = ap.idusuario"
                + "	                     INNER JOIN seguridad.rbac_usuario ua on ua.idusu = ap.idusuarioedito"
                + "	                     INNER JOIN seguridad.rbac_usuario us on us.idusu = ap.idusuariosus"
                + "            INNER JOIN ciudad ci on ci.id = ap.idciudad"
                + "	                     inner join magnitud_intervalos i on i.id = ap.idintervalo " + Busqueda + " order by ap.id";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ConsultarAutorizacion");
    }

    public String SuspenderAutorizacion(HttpServletRequest request) {
        int id = Globales.Validarintnonull(request.getParameter("id"));
        String observacion = request.getParameter("observacion");

        try {
            String fecha = Globales.FechaActual(2);

            if (Globales.PermisosSistemas("AUTORIZACION AL PERSONAL SUSPENDER", idusuario) == 0) {
                return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
            }

            String Estado = Globales.ObtenerUnValor("SELECT estado FROM rrhh.autorizacion_personal where id = " + id);
            if (Estado.equals("Suspendido")) {
                return "1|Esta autorización ya fue suspendida";
            }

            sql = "UPDATE rrhh.autorizacion_personal"
                    + "  SET estado='Suspendido', idusuariosus=" + idusuario + ",fechasus=now(), observacionsus='" + observacion
                    + "' WHERE id = " + id;
            if (Globales.DatosAuditoria(sql, "AUTORIZACION AL PERSONAL", "SUSPENDER", idusuario, iplocal, this.getClass() + "-->SuspenderAutorizacion")) {
                return "0|" + fecha;
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }

    }

    public String BuscarAutorizacion(HttpServletRequest request) {
        int id = Globales.Validarintnonull(request.getParameter("id"));

        sql = "SELECT to_char(ap.fecha,'yyyy/MM/dd HH24:MI') as fecha, ap.estado, "
                + "     ap.idequipo,  ap.idprocedimiento,  c.descripcion as cargo, ap.idempleado, ap.idintervalo,"
                + "    u.nombrecompleto as usuario, idpais, iddepartamento, idciudad, "
                + "    to_char(ap.fechasus,'yyyy/MM/dd HH24:MI') as fechasus, idarecalibracion, idaremantenimiento"
                + " FROM rrhh.autorizacion_personal ap INNER JOIN seguridad.rbac_usuario u on u.idusu = ap.idusuario"
                + "	                     INNER JOIN ciudad ci on ci.id = ap.idciudad"
                + "	                     INNER JOIN departamento d on d.id = ci.iddepartamento"
                + "			     INNER JOIN rrhh.cargo c on c.id = ap.idcargo"
                + " WHERE ap.id = " + id;
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->BuscarAutorizacion");
    }

    /*
     public string RpHojaVida(int Empleado)
        {
            if (Inicializar() != "0") return validarinicio;
            string reporte = "ImpHojaVida";

            
            DataTable datos2;
            sql = @"SELECT archivo, '" + urlarchivo + @"imagenes/AnexoHojaVida/' || idempleado || '/' || foto || '.jpg' as foto
                            FROM rrhh.hoja_vida_certificacion hvc inner join rrhh.hoja_vida hv on hv.id = hvc.idhoja
                            WHERE idempleado = " + Empleado + " order by foto";
            datos2 = Globales.Obtenerdatos(sql, conexion);

            sql = @"SELECT tipodocumento || '-' || documento AS documento, nombrecompleto, telefono, email, hv.fecha,
                       celular, direccion, estadocivil, coalesce(p.descripcion || '/' || d.descripcion || '/' || ci.descripcion,'') as lugarnacimiento,
                       coalesce(to_char(fechanacimiento,'yyyy-MM-dd'),'') as fechanacimiento, c.descripcion as cargo, coalesce(perfil_profesional,'') as perfil_profesional, coalesce(estudios_secundarios,'') as estudios_secundarios, coalesce(pregrado,'') as pregrado, coalesce(postgrado,'') as postgrado, 
	                                    coalesce(otros_estudios,'') as otros_estudios, coalesce(ultimo_cargo,'') as ultimo_cargo, coalesce(logros,'') as logros, coalesce(jefe_inmediato,'') as jefe_inmediato, 
                                           coalesce(telefono_contacto,'') as telefono_contacto, coalesce(tiempo_laborado,'') as tiempo_laborado, 
                    '" + urlarchivo + @"imagenes/FotoEmpleados/' || foto as foto,
	                (select nombre from rrhh.hoja_vida_referencia hvr where hvr.idhoja = hv.id and posicion = 0 and tipo = 'RF') as nombrerf1,
	                (select relacion from rrhh.hoja_vida_referencia hvr where hvr.idhoja = hv.id and posicion = 0 and tipo = 'RF') as relacionrf1,
	                (select telefono from rrhh.hoja_vida_referencia hvr where hvr.idhoja = hv.id and posicion = 0 and tipo = 'RF') as telefonorf1,
	                (select nombre from rrhh.hoja_vida_referencia hvr where hvr.idhoja = hv.id and posicion = 1 and tipo = 'RF') as nombrerf2,
	                (select relacion from rrhh.hoja_vida_referencia hvr where hvr.idhoja = hv.id and posicion = 1 and tipo = 'RF') as relacionrf2,
	                (select telefono from rrhh.hoja_vida_referencia hvr where hvr.idhoja = hv.id and posicion = 1 and tipo = 'RF') as telefonorf2,
	                (select nombre from rrhh.hoja_vida_referencia hvr where hvr.idhoja = hv.id and posicion = 0 and tipo = 'RP') as nombrerp1,
	                (select relacion from rrhh.hoja_vida_referencia hvr where hvr.idhoja = hv.id and posicion = 0 and tipo = 'RP') as relacionrp1,
	                (select telefono from rrhh.hoja_vida_referencia hvr where hvr.idhoja = hv.id and posicion = 0 and tipo = 'RP') as telefonorp1,
	                (select nombre from rrhh.hoja_vida_referencia hvr where hvr.idhoja = hv.id and posicion = 1 and tipo = 'RP') as nombrerp2,
	                (select relacion from rrhh.hoja_vida_referencia hvr where hvr.idhoja = hv.id and posicion = 1 and tipo = 'RP') as relacionrp2,
	                (select telefono from rrhh.hoja_vida_referencia hvr where hvr.idhoja = hv.id and posicion = 1 and tipo = 'RP') as telefonorp2,
	                (select nombre from rrhh.hoja_vida_referencia hvr where hvr.idhoja = hv.id and posicion = 0 and tipo = 'RL') as nombrerl1,
	                (select cargo from rrhh.hoja_vida_referencia hvr where hvr.idhoja = hv.id and posicion = 0 and tipo = 'RL') as cargorl1,
	                (select telefono from rrhh.hoja_vida_referencia hvr where hvr.idhoja = hv.id and posicion = 0 and tipo = 'RL') as telefonorl1,
	                (select nombre from rrhh.hoja_vida_referencia hvr where hvr.idhoja = hv.id and posicion = 1 and tipo = 'RL') as nombrerl2,
	                (select cargo from rrhh.hoja_vida_referencia hvr where hvr.idhoja = hv.id and posicion = 1 and tipo = 'RL') as cargorl2,
	                (select telefono from rrhh.hoja_vida_referencia hvr where hvr.idhoja = hv.id and posicion = 1 and tipo = 'RL') as telefonorl2
                  FROM rrhh.empleados e inner join rrhh.cargo c on c.id = e.idcargo
		                                          LEFT join ciudad ci on ci.id = e.idciudad
		                                          LEFT join departamento d on d.id = ci.iddepartamento
		                                          LEFT join pais p on p.id = d.idpais
		                                          LEFT JOIN rrhh.hoja_vida hv on hv.idempleado =  e.id
                WHERE e.id = " + Empleado;

            datos = Globales.Obtenerdatos(sql, conexion);
            if (datos.Rows.Count == 0)
                return "1|No hay nada que reportar";
            string DirectorioReportesRelativo = "~/Reportes/";
            string directorio = "~/DocumPDF/";
            string unico = "HojaVida-" + Empleado + "_" + DateTime.Now.ToString("yyyyMMddHHmmss");
            string urlArchivo = string.Format("{0}.{1}", reporte, "rdlc");

            string urlArchivoGuardar;
            urlArchivoGuardar = string.Format("{0}.{1}", unico, "pdf");

            string FullPathReport = string.Format("{0}{1}",
                                    this.HttpContext.Server.MapPath(DirectorioReportesRelativo),
                                        urlArchivo);
            string FullGuardar = string.Format("{0}{1}",
                                    this.HttpContext.Server.MapPath(directorio),
                                        urlArchivoGuardar);
            Microsoft.Reporting.WebForms.ReportViewer Reporte = new ReportViewer();
            Reporte.Reset();
            Reporte.LocalReport.ReportPath = FullPathReport;
            Reporte.LocalReport.EnableExternalImages = true;
                        
            ReportDataSource DataSource = new ReportDataSource("DataSet1", datos);
            ReportDataSource DataSource2 = new ReportDataSource("DataSet2", datos2);
            Reporte.LocalReport.DataSources.Add(DataSource);
            Reporte.LocalReport.DataSources.Add(DataSource2);

            Reporte.LocalReport.Refresh();
            byte[] ArchivoPDF = Reporte.LocalReport.Render("PDF");
            FileStream fs = new FileStream(FullGuardar, FileMode.Create);
            fs.Write(ArchivoPDF, 0, ArchivoPDF.Length);
            fs.Close();

            string puerto = Request.Url.Port.ToString();

            /*if (puerto == "80" || puerto == "90")
            {
               rutagestion += fechaco.Year + "\\6. ÁREA COMERCIAL\\1. VENTAS\\DID-79\\DID-79-" + cotizacion + " " + cliente + ".pdf";
                System.IO.File.Copy(rutacotizacion, rutagestion, true);
            }*/
 /*   return "0|" + urlArchivoGuardar;

        }
    
     */
    public String GuardarPrestamo(HttpServletRequest request) {
        int IdPrestamo = Globales.Validarintnonull(request.getParameter("IdPrestamo"));
        int IdEmpleado = Globales.Validarintnonull(request.getParameter("IdEmpleado"));
        String TipoPrestamo = request.getParameter("TipoPrestamo");
        String Descripcion = request.getParameter("Descripcion");
        double Monto = Globales.ValidardoublenoNull(request.getParameter("Monto"));
        String FormaPago = request.getParameter("FormaPago");
        int NCuota = Globales.Validarintnonull(request.getParameter("NCuota"));
        double ValorCuota = Globales.ValidardoublenoNull(request.getParameter("ValorCuota"));
        double Prima = Globales.ValidardoublenoNull(request.getParameter("Prima"));
        String FechaIniCuota = request.getParameter("FechaIniCuota");
        String Desembolso = request.getParameter("Desembolso");

        String mensaje = "";

        try {
            if (IdPrestamo == 0) {

                if (Globales.PermisosSistemas("PRESTAMO GUARDAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "INSERT INTO rrhh.prestamo(tipo, descripcion, idempleado, idusuario,  fechaini, desembolso, formapago, cuotas, sueldo, monto, prima)"
                        + "VALUES('" + TipoPrestamo + "'  ,'" + Descripcion + "'  ," + IdEmpleado + "  ," + idusuario + "  ,'" + FechaIniCuota + "'  ,'" + Desembolso + "','" + FormaPago + "'," + NCuota + "," + (ValorCuota * NCuota) + "," + Monto + "," + Prima + ")";
                if (Globales.DatosAuditoria(sql, "PRESTAMO", "GUARDAR", idusuario, iplocal, this.getClass() + "-->GuardarPrestamo")) {
                    IdPrestamo = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT MAX(id) FROM rrhh.prestamo;"));
                    mensaje = "0|Prestamo agregado con éxito|" + IdPrestamo;
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            } else {

                if (Globales.PermisosSistemas("PRESTAMO EDITAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }

                sql = "SELECT count(pc.id)"
                        + "  FROM rrhh.prestamo_cuota pc inner join rrhh.prestamo p on p.id = pc.idprestamo"
                        + " WHERE idempleado = " + IdEmpleado + " and p.id = " + IdPrestamo + " and idperiodo > 0";
                int pagado = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
                if (pagado > 0) {
                    return "1|No se puede editar este préstamo, porque hay cuotas pagadas";
                }

                sql = "UPDATE rrhh.prestamo SET tipo ='" + TipoPrestamo + "'  , descripcion='" + Descripcion + "'  ,idusuario=" + idusuario
                        + ",fechareg=now(), fechaini='" + FechaIniCuota + "',desembolso='" + Desembolso + "',formapago='" + FormaPago
                        + "', cuotas=" + NCuota + ",sueldo=" + (ValorCuota * NCuota) + ",monto=" + Monto + ",prima=" + Prima
                        + " where id = " + IdPrestamo;
                Globales.DatosAuditoria(sql, "PRESTAMO", "EDITAR", idusuario, iplocal, this.getClass() + "-->GuardarPrestamo");
                mensaje = "0|Prestamo actualizado con éxito|" + IdPrestamo;
            }

            sql = "DELETE FROM rrhh.prestamo_cuota where idprestamo = " + IdPrestamo + ";";
            if (Prima > 0) {
                sql += "INSERT INTO rrhh.prestamo_cuota(idprestamo, ncuota, monto, tipo_cuota, idperiodo)"
                        + "  VALUES (" + IdPrestamo + ",1," + Prima + ",'PRIMA');";
            }
            if (ValorCuota > 0) {
                for (int X = 1; X <= NCuota; X++) {
                    sql += "INSERT INTO rrhh.prestamo_cuota(idprestamo, ncuota, monto, tipo_cuota)"
                            + "VALUES (" + IdPrestamo + "," + X + "," + ValorCuota + ",'SUELDO');";
                }
            }
            Globales.DatosAuditoria(sql, "PRESTAMO", "AGREGAR", idusuario, iplocal, this.getClass() + "-->GuardarHojaVida");
            return mensaje;
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String ConsultarPrestamos(HttpServletRequest request) {
        int empleado = Globales.Validarintnonull(request.getParameter("empleado"));
        int cargo = Globales.Validarintnonull(request.getParameter("cargo"));
        String tipo = request.getParameter("tipo");
        String estado = request.getParameter("estado");
        String Busqueda = "WHERE 1 = 1";
        if (empleado > 0) {
            Busqueda += " AND e.id = " + empleado;
        }
        if (cargo > 0) {
            Busqueda += " AND e.idcargo = " + cargo;
        }
        if (!tipo.equals("")) {
            Busqueda += " AND p.tipo = '" + tipo + "'";
        }

        sql = "SELECT p.id, tipo, p.descripcion, fechareg, to_char(p.fechaini,'yyyy/MM/dd') as fechaini, e.nombrecompleto || ' (' || e.documento || ')' as empleado, c.descripcion as cargo, e.sueldo,"
                + "     desembolso, formapago, cuotas, p.sueldo/cuotas as valorcuota, monto, prima,"
                + "    coalesce((select sum(monto) from rrhh.prestamo_cuota pc where pc.idprestamo = p.id and idperiodo = 0),0) as pendiente,"
                + "    coalesce((select sum(monto) from rrhh.prestamo_cuota pc where pc.idprestamo = p.id and idperiodo > 0),0) as pagado,"
                + "    to_char(p.fechareg,'yyyy/MM/dd HH24:MI') || '<br>' || u.nombrecompleto as registro,"
                + "    '<button class=''btn btn-glow btn-success'' title=''Ver Cuotas'' type=''button'' onclick=' || chr(34) || 'VerCuotas(' || p.id || ')' ||chr(34) || '><span data-icon=''&#xe147;''></span></button>' as estado_cuenta"
                + " FROM rrhh.prestamo p inner join rrhh.empleados e on e.id = p.idempleado"
                + "                     inner join seguridad.rbac_usuario u on u.idusu = p.idusuario"
                + "                    inner join rrhh.cargo c on c.id = e.idcargo  " + Busqueda + " "
                + "   order by p.id";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ConsultarPrestamos");
    }

    protected void Servidor(HttpServletRequest request, HttpServletResponse response) {
        try {

            response.setContentType("text/html;charset=UTF-8");
            String ruta_origen = request.getHeader("Origin");
            if (ruta_origen == null) {
                ruta_origen = "http://localhost:8090";
            }

            HttpSession misession = request.getSession();
            idusuario = misession.getAttribute("idusuario").toString();
            iplocal = misession.getAttribute("IPLocal").toString();
            usuario = misession.getAttribute("codusu").toString();
            nombreusuario = misession.getAttribute("usuario").toString();
            empleado = misession.getAttribute("empleado").toString();
            if (misession.getAttribute("ArchivosAdjuntos") != null) {
                archivoadjunto = misession.getAttribute("ArchivosAdjuntos").toString();
            }

            response.setHeader("Access-Control-Allow-Origin", request.getHeader("Origin"));
            response.setHeader("Access-Control-Allow-Credentials", "true");

            PrintWriter out = response.getWriter();
            if (Globales.ValidarRuta(ruta_origen) == false) {
                out.print("[{\"error\":\"505\",\"mensaje\":\"Usted no tiene permiso para acceder a esta ruta\"}]");
            } else if (misession.getAttribute("codusu") == null) {
                out.println("cerrada");
            } else if (!Globales.IpUsuario(misession.getAttribute("idusuario").toString()).equals(misession.getAttribute("IPLocal").toString())) {
                out.println("cambio-ip");
            } else {
                String opcion = request.getParameter("opcion");
                switch (opcion) {
                    case "BuscarEmpleados":
                        out.println(BuscarEmpleados(request));
                        break;
                    case "GuardarDocumentosEmpleados":
                        out.println(GuardarDocumentosEmpleados(request));
                        break;
                    case "DocumentosEmpleado":
                        out.println(DocumentosEmpleado(request));
                        break;
                    case "ConsultarEmpleados":
                        out.println(ConsultarEmpleados(request));
                        break;
                    case "CambioDocumento":
                        out.println(CambioDocumento(request));
                        break;
                    case "RepararConceptos":
                        out.println(RepararConceptos(request));
                        break;
                    case "BuscarNominas":
                        out.println(BuscarNominas(request));
                        break;
                    case "Periodos_Nomina":
                        out.println(Periodos_Nomina(request));
                        break;
                    case "BuscarHojaVida":
                        out.println(BuscarHojaVida(request));
                        break;
                    case "EliminarAnexoHoja":
                        out.println(EliminarAnexoHoja(request));
                        break;
                    case "GuardarHojaVida":
                        out.println(GuardarHojaVida(request));
                        break;
                    case "Empleados_Nomina":
                        out.println(Empleados_Nomina(request));
                        break;
                    case "CambioCodigo":
                        out.println(CambioCodigo(request));
                        break;
                    case "GuardarNomina":
                        out.println(GuardarNomina(request));
                        break;
                    case "GuardarConcepto":
                        out.println(GuardarConcepto(request));
                        break;
                    case "GuardarConstante":
                        out.println(GuardarConstante(request));
                        break;
                    case "PeriodoActual":
                        out.println(PeriodoActual(request));
                        break;
                    case "CalcularNomina":
                        out.println(CalcularNomina(request));
                        break;
                    case "CerrarPeriodo":
                        out.println(CerrarPeriodo(request));
                        break;
                    case "AbrirPeriodo":
                        out.println(AbrirPeriodo(request));
                        break;
                    case "ReversarNomina":
                        out.println(ReversarNomina(request));
                        break;
                    case "ResumenNomina":
                        out.println(ResumenNomina(request));
                        break;

                    case "GuardarAutorizacion":
                        out.println(GuardarAutorizacion(request));
                        break;
                    case "ConsultarAutorizacion":
                        out.println(ConsultarAutorizacion(request));
                        break;
                    case "SuspenderAutorizacion":
                        out.println(SuspenderAutorizacion(request));
                        break;
                    case "BuscarAutorizacion":
                        out.println(BuscarAutorizacion(request));
                        break;
                    case "TablaDocumentosEmpleado":
                        out.println(TablaDocumentosEmpleado(request));
                        break;
                    case "GuardarPrestamo":
                        out.println(GuardarPrestamo(request));
                        break;
                    case "ConsultarPrestamos":
                        out.println(ConsultarPrestamos(request));
                        break;
                    case "GuardarEmpleados":
                        out.println(GuardarEmpleados(request));
                        break;
                    default:
                        throw new AssertionError();
                }
            }
        } catch (Exception ex) {
            try {
                PrintWriter out = response.getWriter();
                out.println(ex.getMessage());
            } catch (IOException exl) {
                Globales.GuardarLogger(exl.getMessage(), Laboratorio.class.getName());
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        Servidor(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        Servidor(request, response);
    }

}
