package DB;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author leudi
 */
@WebServlet(urlPatterns = {"/Laboratorio"})
public class Laboratorio extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    String sql;
    String idusuario = "";
    String usuario = "";
    ResultSet datos = null;
    String iplocal = "";
    String url_archivo = Globales.url_archivo;
    String usumagnitud = "";
    String correoenvio = "";
    String clavecorreo = "";

    public String NotifiReportes() {

        sql = "SELECT ri.ingreso, concepto, ajuste, suministro, to_char(ri.fecha, 'dd/MM/yyyy HH24:MI') as fecha, ri.observacion, nroreporte, nombrecompleto as asesor, "
                + "         tiempo(rL.fecha, ri.fecha, 3) as tiempo, p.descripcion as plantilla, "
                + "          '<b>' || m.descripcion || '</b><br>' || e.descripcion || '<br><b>' || ma.descripcion || '</b><br>' || mo.descripcion || '<br> ' || case when rd.idintervalo = 0 then 'VER ESPECIFICACIONES' ELSE '(' || desde || ' a ' || hasta || ') ' || medida end || '<br>' || rd.serie as equipo "
                + "  FROM reporte_ingreso ri inner join seguridad.rbac_usuario u on u.idusu = idusuario "
                + "	                    inner join ingreso_recibidolab rl on rl.ingreso = ri.ingreso "
                + "	                    inner join remision_detalle rd on rd.ingreso  = rl.ingreso "
                + "              inner join ingreso_plantilla ip on ip.ingreso = rd.ingreso and ri.plantilla = ip.idplantilla "
                + "              inner join plantillas p on p.id = ri.plantilla  "
                + "	                    inner join equipo e on rd.idequipo = e.id "
                + "	                    inner join magnitudes m on m.id = e.idmagnitud "
                + "	                    inner join modelos mo on mo.id = rd.idmodelo   "
                + "	                    inner join marcas ma on ma.id = mo.idmarca  "
                + "	                    inner join magnitud_intervalos i on i.id = rd.idintervalo "
                + "  WHERE to_char(ri.fecha,'yyyy-MM-dd') =  to_char(now(),'yyyy-MM-dd') ORDER BY ri.id DESC";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->NotifiReportes");
    }

    public String NotifiReportesApro() {
        String magnitudes = Globales.ObtenerUnValor("SELECT magnitud FROM seguridad.rbac_usuario WHERE idusu = " + idusuario);
        sql = "SELECT ri.ingreso, concepto, ajuste, suministro, to_char(ri.fecaprobacion, 'dd/MM/yyyy HH24:MI') as fecha, ri.observacionapro, ri.observacion, tipoaprobacion, nroreporte, nombrecompleto as asesor, "
                + "          tiempo(ri.fecha, ri.fecaprobacion, 3) as tiempo, p.descripcion as plantilla, "
                + "          '<b>' || m.descripcion || '</b><br>' || e.descripcion || '<br><b>' || ma.descripcion || '</b><br>' || mo.descripcion || '<br> ' || case when rd.idintervalo = 0 then 'VER ESPECIFICACIONES' ELSE '(' || desde || ' a ' || hasta || ') ' || medida end || '<br>' || rd.serie as equipo "
                + "  FROM reporte_ingreso ri inner join seguridad.rbac_usuario u on u.idusu = idusuario "
                + "	                    inner join ingreso_recibidolab rl on rl.ingreso = ri.ingreso "
                + "	                    inner join remision_detalle rd on rd.ingreso  = rl.ingreso "
                + "              inner join ingreso_plantilla ip on ip.ingreso = rd.ingreso and ri.plantilla = ip.idplantilla "
                + "              inner join plantillas p on p.id = ri.plantilla  "
                + "	                    inner join equipo e on rd.idequipo = e.id "
                + "	                    inner join magnitudes m on m.id = e.idmagnitud "
                + "	                    inner join modelos mo on mo.id = rd.idmodelo  "
                + "	                    inner join marcas ma on ma.id = mo.idmarca "
                + "	                    inner join magnitud_intervalos i on i.id = rd.idintervalo "
                + "  WHERE fecaprobacion is not null and ip.certificado = 0 and ip.informetecnico = 0 and ip.noautorizado = 0 and e.idmagnitud in (" + magnitudes + ") ORDER BY ri.id DESC";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->NotifiReportesApro");
    }

    public String ConsultarIngresos(HttpServletRequest request) {

        int remision = Globales.Validarintnonull(request.getParameter("remision"));
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        String ingresos = request.getParameter("ingresos");
        String estado = request.getParameter("estado");
        String fechad = request.getParameter("fechad");
        String fechah = request.getParameter("fechah");

        int magnitud = Globales.Validarintnonull(request.getParameter("magnitud"));
        String equipo = request.getParameter("equipo");
        int marca = Globales.Validarintnonull(request.getParameter("marca"));
        String modelo = request.getParameter("modelo");
        String intervalo = request.getParameter("intervalo");
        String serie = request.getParameter("serie");
        int usuarios = Globales.Validarintnonull(request.getParameter("usuario"));
        String recepcion = request.getParameter("recepcion");
        int vcotizado = Globales.Validarintnonull(request.getParameter("vcotizado"));
        int vrecibidolab = Globales.Validarintnonull(request.getParameter("vrecibidolab"));
        int vreportado = Globales.Validarintnonull(request.getParameter("vreportado"));
        int venvtercero = Globales.Validarintnonull(request.getParameter("venvtercero"));
        int vrectercero = Globales.Validarintnonull(request.getParameter("vrectercero"));
        int vcertificado = Globales.Validarintnonull(request.getParameter("vcertificado"));
        int vordencompra = Globales.Validarintnonull(request.getParameter("vordencompra"));
        int vrecibidoing = Globales.Validarintnonull(request.getParameter("vrecibidoing"));
        int vfacturado = Globales.Validarintnonull(request.getParameter("vfacturado"));
        int vdevolucion = Globales.Validarintnonull(request.getParameter("vdevolucion"));
        int ventregado = Globales.Validarintnonull(request.getParameter("ventregado"));
        int vncotizado = Globales.Validarintnonull(request.getParameter("vncotizado"));
        int vnrecibidolab = Globales.Validarintnonull(request.getParameter("vnrecibidolab"));
        int vnreportado = Globales.Validarintnonull(request.getParameter("vnreportado"));
        int vnenvtercero = Globales.Validarintnonull(request.getParameter("vnenvtercero"));
        int vnrectercero = Globales.Validarintnonull(request.getParameter("vnrectercero"));
        int vncertificado = Globales.Validarintnonull(request.getParameter("vncertificado"));
        int vnordencompra = Globales.Validarintnonull(request.getParameter("vnordencompra"));
        int vnrecibidoing = Globales.Validarintnonull(request.getParameter("vnrecibidoing"));
        int vnfacturado = Globales.Validarintnonull(request.getParameter("vnfacturado"));
        int vndevolucion = Globales.Validarintnonull(request.getParameter("vndevolucion"));
        int vnentregado = Globales.Validarintnonull(request.getParameter("vnentregado"));
        int vnencuesta = Globales.Validarintnonull(request.getParameter("vnencuesta"));
        int asecome = Globales.Validarintnonull(request.getParameter("asecome"));
        int tecrep = Globales.Validarintnonull(request.getParameter("tecrep"));
        int teccer = Globales.Validarintnonull(request.getParameter("teccer"));
        int vencuesta = Globales.Validarintnonull(request.getParameter("vencuesta"));
        String fechaentrega = request.getParameter("fechaentrega");
        String idequipo = request.getParameter("idequipo");

        int ingreso = Globales.Validarintnonull(ingresos);
        String[] a_ingreso = ingresos.split("-");

        String busqueda = "WHERE rd.ingreso <> 0 ";
        if (remision > 0) {
            busqueda += " AND r.remision = " + remision;
        } else if (ingreso > 0) {
            busqueda += " AND rd.ingreso = " + ingreso;
        } else if (Globales.Validarintnonull(a_ingreso[0]) > 0 && Globales.Validarintnonull(a_ingreso[1]) > 0) {
            busqueda += " AND rd.ingreso >= " + a_ingreso[0] + " and rd.ingreso <= " + a_ingreso[1];
        } else {
            busqueda += " AND to_char(rd.fechaing,'yyyy-MM-dd') >= '" + fechad + "' and to_char(rd.fechaing,'yyyy-MM-dd') <= '" + fechah + "'";
            if (cliente > 0) {
                busqueda += " AND r.idcliente = " + cliente;
            }
            if (ingreso > 0) {
                busqueda += " AND rd.ingreso = " + ingreso;
            }
            if (!estado.equals("0")) {
                busqueda += " AND r.estado = '" + estado + "'";
            }
            if (magnitud > 0) {
                busqueda += " AND e.idmagnitud = " + magnitud;
            }
            if (!equipo.equals("")) {
                busqueda += " AND e.descripcion = '" + equipo + "'";
            }
            if (marca > 0) {
                busqueda += " AND mo.idmarca = " + marca;
            }
            if (!modelo.equals("")) {
                busqueda += " AND mo.descripcion = '" + modelo + "'";
            }
            if (!intervalo.equals("")) {
                busqueda += " AND '(' || desde || ' a ' || hasta || ') ' || medida = '" + intervalo + "'";
            }
            if (!serie.trim().equals("")) {
                busqueda += " AND rd.serie ilike '%" + serie.trim() + "%'";
            }
            if (!idequipo.trim().equals("")) {
                busqueda += " AND rd.ident_equipo ilike '%" + idequipo.trim() + "%'";
            }
            if (usuarios > 0) {
                busqueda += " AND r.idusuario = " + usuarios;
            }
            if (recepcion != null && !recepcion.isEmpty()) {
                busqueda += " AND r.recepcion = '" + recepcion + "'";
            }
            if (asecome > 0) {
                busqueda += " AND (c.asesor = " + asecome + " or r.asesor = " + asecome + ")";
            }
            if (tecrep > 0) {
                busqueda += " AND coalesce((SELECT max(ingreso) FROM reporte_ingreso ring where ring.ingreso = rd.ingreso and ring.idusuario = " + tecrep + "),0) = rd.ingreso";
            }
            if (teccer > 0) {
                busqueda += " AND coalesce((SELECT max(ingreso) FROM certificados certi where certi.ingreso = rd.ingreso and certi.idusuario = " + teccer + "),0) = rd.ingreso";
            }
            if (!fechaentrega.equals("")) {
                busqueda += " and fecha_entrega(rd.ingreso,2) <= '" + fechaentrega + "'";
            }

            if (vcotizado == 1) {
                busqueda += " AND rd.cotizado = " + vcotizado;
            }
            if (vrecibidolab == 1) {
                busqueda += " AND rd.recibidolab = " + vrecibidolab;
            }
            if (vreportado == 1) {
                busqueda += " AND ip.reportado = " + vreportado;
            }
            if (venvtercero == 1) {
                busqueda += " AND rd.envtercero = " + venvtercero;
            }
            if (vrectercero == 1) {
                busqueda += " AND rd.rectercero = " + vrectercero;
            }
            if (vcertificado == 1) {
                busqueda += " AND (ip.certificado = " + vcertificado + " or ip.informetecnico > 0 or ip.noautorizado > 0 or informetercerizado <> '0')";
            }
            if (vordencompra == 1) {
                busqueda += " AND rd.orden = " + vordencompra;
            }
            if (vrecibidoing == 1) {
                busqueda += " AND rd.recibidoing = " + vrecibidoing;
            }
            if (vfacturado == 1) {
                busqueda += " AND rd.facturado > 0 ";
            }
            if (vdevolucion == 1) {
                busqueda += " AND rd.salida >= 1";
            }
            if (ventregado == 1) {
                busqueda += " AND rd.entregado = " + ventregado;
            }
            if (vencuesta == 1) {
                busqueda += " AND rd.recibidocli = 1 ";
            }

            if (vncotizado == 1) {
                busqueda += " AND rd.cotizado = 0 ";
            }
            if (vnrecibidolab == 1) {
                busqueda += " AND rd.recibidolab = 0 ";
            }
            if (vnreportado == 1) {
                busqueda += " AND ip.reportado = 0 ";
            }
            if (vnenvtercero == 1) {
                busqueda += " AND rd.envtercero = 0 ";
            }
            if (vnrectercero == 1) {
                busqueda += " AND rd.rectercero = 0 ";
            }
            if (vncertificado == 1) {
                busqueda += " AND (ip.certificado = 0 and ip.informetecnico = 0 and ip.noautorizado = 0 and informetercerizado = '0')";
            }
            if (vnordencompra == 1) {
                busqueda += " AND rd.orden = 0 ";
            }
            if (vnrecibidoing == 1) {
                busqueda += " AND rd.recibidoing = 0 ";
            }
            if (vnfacturado == 1) {
                busqueda += " AND rd.facturado = 0 ";
            }
            if (vndevolucion == 1) {
                busqueda += " AND rd.salida = 0 ";
            }
            if (vnentregado == 1) {
                busqueda += " AND rd.entregado = 0 ";
            }
            if (vnencuesta == 1) {
                busqueda += " AND rd.recibidocli = 0";
            }
        }
        sql = "select r.id, rd.observacion as observacioning, coalesce(rd.accesorio) as accesorio, '<b>' || rd.sitio || '</b><br>' || rd.garantia || '<br><b>' || case when convenio = 1 then 'SI' ELSE 'NO' END || '</b>'  as garantia, "
                + "      case when rd.solicitud <= 0 then tiempo_aprobacion(rd.ingreso, 0, ip.fechaaproajus, rd.fechaaprocoti, rd.fechaing, now(),2) else  '<a href=''javascript:VerDetalle(16,' || rd.ingreso || ')'' title=''Ver Solicitud''>' || rd.solicitud || '<br>Ver Detalle</a>' END as solicitud, "
                + "      '<a title=''Imprimir Remisión'' type=''button'' href=' || chr(34) || 'javascript:ImprimirRemision(' || r.remision || ',''' || r.estado || ''',0)' ||chr(34) || '>' || r.remision || '</a><br>' || "
                + "       u.nombrecompleto || '<br><b>' || to_char(fechaing, 'yyyy/MM/dd HH24:MI') || '</b>' || case when r.pdf > 0 then '<br><a class=''text-success text-XX'' title=''Ver Remisión del Cliente'' href=''javascript:VerRemision(' || r.remision || ')''>PDF</a>' ELSE '' END as remision, rd.ingreso, r.estado,  "
                + "      to_char(fechaaprocoti, 'yyyy/MM/dd HH24:MI') as fechaaprocoti, "
                + "      to_char(fechaaproajus, 'yyyy/MM/dd HH24:MI') as fechaaproajus, "
                + "      p.descripcion || case when p.excel > 0 then '<br><a href=''javascript:DescargarPlantilla(' || p.id || ')'' title=''Descargar Plantilla''>Descargar</a>' else '' end as plantilla, "
                + "      fecha_entrega(rd.ingreso, 1, coalesce(idplantilla,0)) as fechaentrega, "
                + "      case when rd.entregado = 0 then '<b><font color=''red''>NO</font></b><br>' else ' ' end || tiempo(rd.fechaing, case when rd.entregado = 0  then now() else (select max(fechaentrega) FROM devolucion_detalle dd inner join devolucion d on d.id = dd.iddevolucion where dd.ingreso = rd.ingreso) end, 3) as tiempolab,  "
                + "      case when rd.entregado = 0 then '<b><font color=''red''>NO</font></b><br>' else ' ' end || tiempo_aprobacion(rd.ingreso, 0, ip.fechaaproajus, rd.fechaaprocoti, rd.fechaing, case when rd.entregado = 0  then now() else (select max(fechaentrega) FROM devolucion_detalle dd inner join devolucion d on d.id = dd.iddevolucion where dd.ingreso = rd.ingreso) end, 3) as tiempoapro, "
                + "      tiempo_aprobacion(rd.ingreso, 0, ip.fechaaproajus, rd.fechaaprocoti, rd.fechaing, case when rd.entregado = 0  then now() else (select max(fechaentrega) FROM devolucion_detalle dd inner join devolucion d on d.id = dd.iddevolucion where dd.ingreso = rd.ingreso) end, 8) as diaapro, "
                + "      c.documento || '<br><b>' || c.nombrecompleto || '</b><br> <span class=''text-primary''>(' || ua.nomusu || ')</span>' || case when habitual = 'SI' THEN '<br><b>Habitual</b>' else case when aprobar_cotizacion = 'NO' THEN '<br>no.re.ap.co' ELSE '<br><font color=''red''><b>No Habitual</b></font>' END END as cliente, "
                + "      '<b>' || m.descripcion || '</b><br>' || e.descripcion || '<br><b>' || ma.descripcion || '</b><br>' || mo.descripcion as equipo, "
                + "      '<b>' || case when rd.idintervalo = 0 then 'VER ESPECIFICACIONES' ELSE '(' || desde || ' a ' || hasta || ') ' || medida end || '</b><br>' || rd.serie as rango, "
                + "      case when r.importado = 1 and ncotizacion = 0 then coalesce((SELECT max(ip.descripcion) FROM importado_cotizacion ip where ip.ingreso = rd.ingreso),'NO') else case when rd.cotizado = 1 then '<a href=''javascript:VerDetalle(1,' || rd.ingreso || ')'' title=''Ver Cotizaciones''>' || rd.ncotizacion || (select '<br>' || co.estado from cotizacion co where cotizacion = rd.ncotizacion) || '<br>Ver Detalle</a>' else  tiempo_aprobacion(rd.ingreso, 0, ip.fechaaproajus, rd.fechaaprocoti, rd.fechaing, now(),2) END END as cotizado, "
                + "      case when rd.recibidolab = 1 then '<a href=''javascript:VerDetalle(2,' || rd.ingreso || ')'' title=''Ver Recepción de Laboratorio''>' || (select subString(observacion,1,30) from ingreso_recibidolab rb where rb.ingreso = rd.ingreso) || '...<br>Ver Detalle</a>' else  tiempo_aprobacion(rd.ingreso, 0, ip.fechaaproajus, rd.fechaaprocoti,rd.fechaing, now(),2) END as recibidolab, "
                + "      case when ip.reportado = 1 then '<a href=''javascript:VerDetalle(3,' || rd.ingreso || ')'' title=''Ver Reporte''>' || (select concepto || case when fecaprobacion is null then '' else '<br>[Autorizado]' end from reporte_ingreso ri where ri.ingreso = rd.ingreso and ip.idplantilla = ri.plantilla order by id desc limit 1) || '<br>Ver Detalle</a>' else CASE WHEN rd.recibidolab = 0 THEN 'NO<BR>RECIBIDO' ELSE tiempo_aprobacion(rd.ingreso, 0, ip.fechaaproajus, rd.fechaaprocoti, (select fecha from ingreso_recibidolab ir where ir.ingreso = rd.ingreso), now(),2)  END END  as reportado, "
                + "      case when operacion_previa is null then 'SIN OPERACION PREVIA' ELSE  '<a href=''javascript:VerDetalle(18,' || rd.ingreso || ')'' title=''Ver Operación Previa''>' || to_char(operacion_previa,'yyyy/MM/dd HH24:MI') end as operaciones, "
                + "      case when rd.envtercero = 1 then '<a href=''javascript:VerDetalle(4,' || rd.ingreso || ')'' title=''Ver Envío de Tercero''>' || (select oct.orden from ordencompra_tercero oct inner join ordencompter_detalle octdet on octdet.idorden = oct.id WHERE octdet.ingreso = rd.ingreso order by oct.id DESC LIMIT 1) || '<br>Ver Detalle</a>' else  "
                + "                  CASE WHEN rd.cotizado = 0 then 'NO <br> COTIZADO' ELSE CASE WHEN coalesce((select max(idproveedor) FROM cotizacion_detalle cd where cd.estado in ('Cerrado','Aprobado','Cotizado','Facturado') and cd.ingreso = rd.ingreso),0) > 1 THEN  'NO' ELSE 'NO<br> REQUERIDO' END END END as envtercero, "
                + "      case when rd.rectercero = 1 then '<a href=''javascript:VerDetalle(5,' || rd.ingreso || ')'' title=''Ver Recibir Tercero''>SI<br>Ver Detalle</a>' else CASE WHEN envtercero <= 0 THEN 'NO<BR>REQUERIDO' ELSE tiempo_aprobacion(rd.ingreso, 0, ip.fechaaproajus, rd.fechaaprocoti,(select fechasal FROM salida sal inner join salida_detalle saldet on sal.id = saldet.idsalida where saldet.ingreso = rd.ingreso ORDER BY sal.id DESC LIMIT 1),now(),2) END END  as rectercero, "
                + "      case when ip.certificado = 1 or certificados_externos = 1 then '<a href=''javascript:VerDetalle(6,' || rd.ingreso || ')'' title=''Ver Certificado''>' || (select min(numero) from certificados ce where ce.ingreso = rd.ingreso and ce.item = ip.idplantilla) || '<br>Ver Detalle</a>' else case when ip.calibracion = 2 then 'NO Apto <br> Para Calibración' ELSE CASE WHEN ip.reportado = 0 then 'NO REPORTADO' ELSE tiempo_aprobacion(rd.ingreso, 0, ip.fechaaproajus, rd.fechaaprocoti,(select min(fecha) from reporte_ingreso ri where ri.ingreso = rd.ingreso), now(),2) END END END  as certificado, "
                + "      case when rd.recibidoing = 1 then '<a href=''javascript:VerDetalle(7,' || rd.ingreso || ')'' title=''Ver Recepción de Ingreso''>' || (select subString(observacion,1,30) from ingreso_recibidoing rb where rb.ingreso = rd.ingreso) || '...<br>Ver Detalle</a>' else 'NO' END  as recingreso, "
                + "      case when rd.orden = 1 then '<a href=''javascript:VerDetalle(8,' || rd.ingreso || ')'' title=''Ver Orden de Compra''>' || (select orco.numero || '<br>' || to_char(fechaorden,'yyyy/MM/dd HH24:MI') from orden_compra orco WHERE orco.ingreso = rd.ingreso order by orco.id desc limit 1) ||'<br>Ver Detalle</a>' else case when cotizado = 1 then tiempo_aprobacion(rd.ingreso, 0, ip.fechaaproajus, rd.fechaaprocoti,(select MIN(fechareg) from cotizacion_detalle cd where cd.ingreso = rd.ingreso), now(),2) ELSE 'NO <br>COTIZADO' END END  as orden, "
                + "      case when rd.facturado> 0 then '<a href=''javascript:VerDetalle(9,' || rd.ingreso || ')'' title=''Ver Factura''>' || rd.facturado || '<br>Ver Detalle</a>' else 'NO' END  as facturado, "
                + "      '' as pagado, "
                + "      case when salidaterce > 0 then '<a href=''javascript:VerDetalle(15,' || rd.ingreso || ')'' title=''Ver Salida''>' || rd.salidaterce || '<br>Ver Detalle</a>' else  "
                + "          case when rd.recibidocome = 0 then 'NO<br>Requerido' else tiempo_aprobacion(rd.ingreso, 0, ip.fechaaproajus, rd.fechaaprocoti,(select max(rc.fecha) from ingreso_recibidocome rc where rc.ingreso = rd.ingreso),now(), 2) END END as salida, "
                + "      CASE WHEN recibidocome = 0 THEN 'NO<br>RECIBIDO' ELSE '<a href=''javascript:VerDetalle(14,' || rd.ingreso || ')'' title=''Ver Recepción de Comercial''>SI Ver<br>Detalle</a>' end as recibidocom, "
                + "      case when rd.salida = 0 then 'NO' else case when salida > 1 THEN (SELECT max(id.descripcion) FROM importado_devolucion id where id.ingreso = rd.ingreso) else '<a href=''javascript:VerDetalle(10,' || rd.ingreso || ')'' title=''Ver Devolución''>' || (select max(devolucion) from devolucion de inner join devolucion_detalle dd on de.id = dd.iddevolucion and dd.ingreso = rd.ingreso) || '<br>Ver Detalle</a>' END END  as devolucion, "
                + "      case when rd.entregado = 1 and rd.salida = 1 then '<a href=''javascript:VerDetalle(11,' || rd.ingreso || ')'' title=''Ver Ingrega''>SI<br>Ver Detalle</a>' else 'NO' END  as entregado, "
                + "      case when rd.fotos > 0 then '<img src=''" + url_archivo + "Adjunto/imagenes/ingresos/' || rd.ingreso || '/1.jpg'' onclick=''LlamarFotoDet(' || rd.ingreso || ',' || rd.fotos || ')'' width=''80px''/><br>' else '' end || '<b>' || case when salida = 0 then coalesce((SELECT y || x FROM ingreso_ubicacion u WHERE u.ingreso = rd.ingreso),'') else '' end || '</b>' as imagen, "
                + "      case when ip.informetecnico > 0 then '<a href=''javascript:VerDetalle(12,' || rd.ingreso || ')'' title=''Ver Informe Técnico Laboratorio''>' || ip.informetecnico || '<br>Ver Detalle</a>' else CASE WHEN rd.recibidolab = 0 THEN 'NO<BR>RECIBIDO' ELSE case when ip.reportado = 1 and ip.calibracion = 0 then  tiempo_aprobacion(rd.ingreso, 0, ip.fechaaproajus, rd.fechaaprocoti,(select fecha from ingreso_recibidolab ir where ir.ingreso = rd.ingreso), now(),2) ELSE 'NO <BR> REQUERIDO' END  END END  as informe, "
                + "      case when rd.informetercerizado <> '0' then '<a href=''javascript:VerDetalle(17,' || rd.ingreso || ')'' title=''Ver Informe Técnico Tercerizado''>' || rd.informetercerizado || '<br>Ver Detalle</a>' else 'NO<BR>REQUERIDO' END  as informetercerizado, "
                + "      case when ip.noautorizado > 0 then coalesce((SELECT to_char(ri.fecha,'yyyy-MM-dd') || '<br>' || uri.nombrecompleto from reporte_ingreso ri inner join seguridad.rbac_usuario uri on uri.idusu = ri.idusuario and ri.ingreso = rd.ingreso and ri.plantilla = ip.idplantilla and ri.idconcepto = 6),'') else '' end  as noautorizado, "
                + "      case when rd.recibidocli = 1 THEN '<a href=''javascript:VerDetalle(13,' || rd.ingreso || ')'' title=''Ver Encuesta''>SI<br>Ver Detalle</a>'  "
                + "          else case when rd.entregado = 0 or rd.salida = 0  then 'NO<BR>ENTREGADO' ELSE tiempo_aprobacion(rd.ingreso, 0, ip.fechaaproajus, rd.fechaaprocoti,(select max(fechaentrega) FROM devolucion_detalle dd inner join devolucion d on d.id = dd.iddevolucion where dd.ingreso = rd.ingreso), now(),2) end end as encuesta, "
                + "      (select count(ingreso)  "
                + "              from ingreso_programacion p2 inner join ingreso_plantilla ip2 on ip2.id = p2.idip "
                + "              where tecnico = " + idusuario + " and reportado = 0) as cantidad, "
                + "      (select '<b>' || to_char(ip2.fechapro,'yyyy/MM/dd HH24:MI') || '</b><br>' || up.nombrecompleto "
                + "          from ingreso_programacion ip2 inner join seguridad.rbac_usuario up on up.idusu = ip2.idusuario where ip2.idip = ip.id) as programacion "
                + "from remision r inner join seguridad.rbac_usuario u on r.idusuario = u.idusu "
                + "		                    inner join clientes c on c.id = r.idcliente "
                + "			                    inner join clientes_sede cs on cs.id = r.idsede "
                + "			                    inner join clientes_contac cc on cc.id = r.idcontacto "
                + "			                    inner join ciudad ci on ci.id = cs.idciudad "
                + "			                    inner join departamento d on d.id = ci.iddepartamento "
                + "			                    inner join remision_detalle rd on rd.idremision = r.id "
                + "                            inner join equipo e on e.id = rd.idequipo "
                + "			                    inner join modelos mo on mo.id = rd.idmodelo  "
                + "                            inner join marcas ma on ma.id = mo.idmarca "
                + "                            inner join magnitudes m on m.id = e.idmagnitud "
                + "                            inner join seguridad.rbac_usuario ua on c.asesor = ua.idusu "
                + "                            inner join magnitud_intervalos i on i.id = rd.idintervalo "
                + "                            left join ingreso_plantilla ip on ip.ingreso = rd.ingreso "
                + "                            left join plantillas p on p.id = ip.idplantilla " + busqueda
                + "                ORDER BY rd.ingreso desc";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ConsultarIngresos");

    }

    public String DetalleIngreso(HttpServletRequest request) {

        int opcion = Globales.Validarintnonull(request.getParameter("opciones"));
        int ingreso = Globales.Validarintnonull(request.getParameter("ingreso"));

        String resultado = "";

        try {
            switch (opcion) {
                case 1:
                    resultado = "<table width='100%' class='table table-bordered hover'>";
                    resultado += "<tr>"
                            + "<th width='5%'>&nbsp;</td>"
                            + "<th>Cotización</th>"
                            + "<th>Estado</th>"
                            + "<th>Fecha</th>"
                            + "<th>Asesor</th>"
                            + "<th>Tiempo</th>"
                            + "<th>Enviado</th>"
                            + "<th>Aprobado<br>Rechazado</th>"
                            + "<th>Observacion<br>Apro/Rechazo</th></tr>";

                    sql = "SELECT DISTINCT c.cotizacion, c.estado, to_char(c.fecha, 'dd/MM/yyyy HH24:MI') as fecha, u.nombrecompleto as usuario, "
                            + "  tiempo(rd.fechaing, c.fecha, 3) as tiempo, "
                            + "  coalesce('<b>' || ca.estado || '</b><br>' || ca.usuario || '<br>' || '<br><b>' || to_char(ca.fecha,'dd/MM/yyyy HH24:MI') || '</b><br>' || coalesce(ca.tiempo,''),'') as aprobacion, "
                            + "  coalesce(ca.observacion,'') as observacion, coalesce(ca.idusuario,0) as usuarioapro, "
                            + "  coalesce('<b>' || to_char(c.fecenviada,'dd/MM/yyyy HH24:MI') || '</b><br>' || c.correoenviado,'') as envio "
                            + "  FROM cotizacion c INNER JOIN cotizacion_detalle cd on cd.idcotizacion = c.id "
                            + "                    INNER JOIN remision_detalle rd on rd.ingreso = cd.ingreso "
                            + "                    INNER JOIN seguridad.rbac_usuario u on u.idusu = c.idusuario "
                            + "                    LEFT JOIN cotizacion_aprobacion ca on ca.cotizacion = c.cotizacion "
                            + " WHERE cd.ingreso = " + ingreso + " ORDER BY cotizacion DESC";
                    datos = Globales.Obtenerdatos(sql, this.getClass() + "-->DetalleIngreso", 1);
                    while (datos.next()) {
                        resultado += "<tr>"
                                + "<td width='5%'><button class='btn btn-success' title='Imprimir Cotizacion' type='button' onclick='ImprimirCotizacion(" + datos.getString("cotizacion") + ")'><span data-icon='&#xe0f7;'></span></button></td>"
                                + "<td>" + datos.getString("cotizacion") + "</td>"
                                + "<td>" + datos.getString("estado") + "</td>"
                                + "<td>" + datos.getString("fecha") + "</td>"
                                + "<td>" + datos.getString("usuario") + "</td>"
                                + "<td>" + datos.getString("tiempo") + "</td>"
                                + "<td>" + datos.getString("envio") + "</td>"
                                + "<td>" + datos.getString("aprobacion") + (!datos.getString("usuarioapro").equals("0") ? "<br><a href='javascript:VerAprobacion(" + datos.getString("cotizacion") + ")'>Ver Aprobación</a>" : "") + "</td>"
                                + "<td>" + datos.getString("observacion") + "</td></tr>";
                    }
                    resultado += "</table>";
                    break;
                case 2:
                    resultado = "<table width='100%' class='table table-bordered hover'>";
                    resultado += "<tr>"
                            + "<th>Asesor Entrega</th>"
                            + "<th>Fecha</th>"
                            + "<th>Observación del Especialista</th>"
                            + "<th>Asesor Recibe</th>"
                            + "<th>Tiempo</th></tr>";

                    sql = "SELECT ir.observacion, to_char(ir.fecha, 'dd/MM/yyyy HH24:MI') as fecha, ur.nombrecompleto as usuariorec, ue.nombrecompleto as usuarioent, "
                            + "          tiempo(rd.fechaing, ir.fecha, 3) as tiempo "
                            + "  FROM ingreso_recibidolab ir inner join seguridad.rbac_usuario ue on ue.idusu = idusuarioent "
                            + "	                                inner join seguridad.rbac_usuario ur on ur.idusu = idusuario "
                            + "	                                INNER JOIN remision_detalle rd on ir.ingreso = rd.ingreso "
                            + "  WHERE ir.ingreso = " + ingreso;
                    datos = Globales.Obtenerdatos(sql, this.getClass() + "-->DetalleIngreso", 1);
                    while (datos.next()) {
                        resultado += "<tr>"
                                + "<td>" + datos.getString("usuarioent") + "</td>"
                                + "<td>" + datos.getString("fecha") + "</td>"
                                + "<td>" + datos.getString("observacion") + "</td>"
                                + "<td>" + datos.getString("usuariorec") + "</td>"
                                + "<td>" + datos.getString("tiempo") + "</td></tr>";
                    }
                    resultado += "</table>";
                    break;

                case 3:
                    resultado = "<table width='100%' class='table table-bordered hover'>";
                    resultado += "<tr>"
                            + "<th width='5%'>Nro Reporte</th>"
                            + "<th>Concepto</th>"
                            + "<th width='25%'>Ajuste</th>"
                            + "<th width='25%'>Suministro</th>"
                            + "<th width='25%'>Observacion</th>"
                            + "<th>Cotización</th>"
                            + "<th>Estado</th>"
                            + "<th>Especialista</th>"
                            + "<th>Fecha</th>"
                            + "<th>Envío</th>"
                            + "<th>Observacion<br>Autorización</th>"
                            + "<th>Tiempo</th></tr>";

                    sql = "SELECT ri.ingreso, concepto, ajuste, suministro, to_char(ri.fecha, 'dd/MM/yyyy HH24:MI') as fecha, ri.observacion, cotizado, nroreporte, nombrecompleto as asesor, "
                            + "case when ri.cotizado = 0 THEN 'SIN CONTIZAR' ELSE case when ri.cedula is not null then '<b><font color=''green''>AUTORIZADO</font></b>' ELSE '<b>EN ESPERA DEL CLIENTE</b>' end END as estado, "
                            + "(select max(cotizacion) FROM cotizacion c inner join cotizacion_detalle cd on cd.idcotizacion = c.id WHERE cd.id = ri.cotizado) as cotizacion,  "
                            + "tiempo(rL.fecha, ri.fecha, 3) as tiempo, coalesce(to_char(ri.fechaenvio, 'dd/MM/yyyy HH24:MI') || '<br>' || ri.correoenvio,'') as envio, "
                            + "coalesce(to_char(fecaprobacion, 'yyyy/MM/dd HH24:MI') || '<br>' || coalesce(usuarioapro,'') || '<br>' || coalesce(cedula,'') || '<br>' || coalesce(ri.cargo,''),'') as fechaapro, "
                            + "case when fecaprobacion is null then 0 else case when formaaprobacion = 'Cliente' then 0 else ri.id end end as aprobacion "
                            + "FROM reporte_ingreso ri inner join seguridad.rbac_usuario u on u.idusu = idusuario "
                            + "                        inner join ingreso_recibidolab rl on rl.ingreso = ri.ingreso "
                            + " WHERE ri.ingreso = " + ingreso + " ORDER BY ri.id DESC";
                    datos = Globales.Obtenerdatos(sql, this.getClass() + "-->DetalleIngreso", 1);
                    while (datos.next()) {
                        resultado += "<tr>"
                                + "<td>" + datos.getString("nroreporte") + "</td>"
                                + "<td>" + datos.getString("concepto") + "</td>"
                                + "<td>" + datos.getString("ajuste") + "</td>"
                                + "<td>" + datos.getString("suministro") + "</td>"
                                + "<td>" + datos.getString("observacion") + "</td>"
                                + "<td>" + datos.getString("cotizacion") + "</td>"
                                + "<td>" + datos.getString("estado") + "</td>"
                                + "<td>" + datos.getString("asesor") + "</td>"
                                + "<td>" + datos.getString("fecha") + "</td>"
                                + "<td>" + datos.getString("envio") + "</td>"
                                + "<td>" + datos.getString("fechaapro") + (!datos.getString("aprobacion").equals("0") ? "<br><a href='javascript:VerAprobacionReporte(" + datos.getString("aprobacion") + ")'>Ver Aprobación</a>" : "") + "</td>"
                                + "<td>" + datos.getString("tiempo") + "</td></tr>";

                    }
                    resultado += "</table>";
                    break;

                case 4:
                    resultado = "<table width='100%' class='table table-bordered hover'>";
                    resultado += "<tr>"
                            + "<th width='5%'>&nbsp;</td>"
                            + "<th>Nro Órden</th>"
                            + "<th>Proveedor</th>"
                            + "<th>Estado</th>"
                            + "<th>Registro</th>"
                            + "<th>DiasEntrega</th>"
                            + "<th>Tiempo Trans</th>"
                            + "<th>Enviado</th>"
                            + "<th>Correo</th>"
                            + "<th>Anulado</th>"
                            + "<th>Obser.Anula</th></tr>";

                    sql = "SELECT orden,to_char(fecha,'yyyy/MM/dd HH24:MI') || '<br>' || u.nombrecompleto as registro, "
                            + "         to_char(fechaanula,'yyyy/MM/dd HH24:MI') || '<br>' || ua.nombrecompleto as anulado,  "
                            + "         to_char(fechaenvio,'yyyy/MM/dd HH24:MI') || '<br>' || ue.nombrecompleto as enviado, ot.correoenvio, "
                            + "         observacionanula, ot.estado, diasentrega, tiempo(fecha, coalesce((SELECT fecharet from salida s inner join salida_detalle sd on s.id = sd.idsalida WHERE sd.ingreso = otd.ingreso AND s.estado <> 'Anulado'),now()),1) as tiempo, "
                            + "     p.nombrecompleto as proveedor "
                            + "FROM ordencompra_tercero ot INNER JOIN ordencompter_detalle otd on ot.id = otd.idorden "
                            + "		        INNER JOIN seguridad.rbac_usuario u on u.idusu = ot.idusuario "
                            + "                     INNER JOIN seguridad.rbac_usuario ua on ua.idusu = ot.idusuarioanula "
                            + "		        INNER JOIN seguridad.rbac_usuario ue on ue.idusu = ot.idusuaenvio "
                            + "  INNER JOIN proveedores p on p.id = ot.idproveedor "
                            + "WHERE otd.ingreso = " + ingreso + " ORDER BY orden DESC";
                    datos = Globales.Obtenerdatos(sql, this.getClass() + "-->DetalleIngreso", 1);
                    while (datos.next()) {
                        resultado += "<tr>"
                                + "<td width='5%'><button class='btn btn-success' title='Imprimir Órden de Compra Tercerizado' type='button' onclick='ImprimirOrdenTercero(" + datos.getString("orden") + ")'><span data-icon='&#xe0f7;'></span></button></td>"
                                + "<td>" + datos.getString("orden") + "</td>"
                                + "<td>" + datos.getString("proveedor") + "</td>"
                                + "<td>" + datos.getString("estado") + "</td>"
                                + "<td>" + datos.getString("registro") + "</td>"
                                + "<td>" + datos.getString("diasentrega") + "</td>"
                                + "<td>" + datos.getString("tiempo") + "</td>"
                                + "<td>" + datos.getString("enviado") + "</td>"
                                + "<td>" + datos.getString("correoenvio") + "</td>"
                                + "<td>" + datos.getString("anulado") + "</td>"
                                + "<td>" + datos.getString("observacionanula") + "</td></tr>";

                    }
                    resultado += "</table>";
                    break;
                case 6:
                    resultado = "<table width='100%' class='table table-bordered hover'>";
                    resultado += "<tr>"
                            + "<th>ITEM</th>"
                            + "<th>Nro Certificado</th>"
                            + "<th>Fecha</th>"
                            + "<th>Especialista</th>"
                            + "<th>Observación</th>"
                            + "<th>Tiempo</th>"
                            + "<th>Anulado</th>"
                            + "<td width='5%'>&nbsp;</td></tr>";

                    sql = "SELECT coalesce(c.observacion,'') as observacion, to_char(c.fecha, 'dd/MM/yyyy HH24:MI') as fecha, u.nombrecompleto as usuario, c.numero, to_char(c.ingreso,'0000000') as ingreso, "
                            + "     tiempo((select ri.fecha from reporte_ingreso ri where ri.ingreso = c.ingreso order by ri.id limit 1), c.fecha, 3) as tiempo, c.item, coalesce(to_char(fechaanula,'dd/MM/yyyy HH24:MI') || '<br>' || ua.nombrecompleto,'')  as anulado, c.impreso "
                            + " FROM certificados c inner join seguridad.rbac_usuario u on u.idusu = idusuario "
                            + "                     inner join seguridad.rbac_usuario ua on ua.idusu = idusuarioanula "
                            + " WHERE c.ingreso = " + ingreso
                            + " ORDER BY item";
                    datos = Globales.Obtenerdatos(sql, this.getClass() + "-->DetalleIngreso", 1);
                    while (datos.next()) {
                        resultado += "<tr " + (!datos.getString("anulado").trim().equals("") ? "class='bg-danger '" : "") + ">"
                                + "<td>" + datos.getString("item") + "</td>"
                                + "<td>" + datos.getString("numero") + "</td>"
                                + "<td>" + datos.getString("fecha") + "</td>"
                                + "<td>" + datos.getString("usuario") + "</td>"
                                + "<td>" + (datos.getString("observacion") != null ? datos.getString("observacion") : "") + "</td>"
                                + "<td>" + datos.getString("tiempo") + "</td>"
                                + "<td>" + datos.getString("anulado") + "</td>"
                                + "<td width='5%'>" + (datos.getString("anulado").equals("") ? "<button class='btn btn-success' title='Imprimir Certificado' type='button' onclick=\"ImprimirCertificado('" + (datos.getString("impreso").equals("1") ? datos.getString("numero") + " " + datos.getString("ingreso") : datos.getString("item") + "-" + datos.getString("numero")) + "')\"><span data-icon='&#xe0f7;'></span></button>" : "") + "</td></tr>";

                    }
                    resultado += "</table>";
                    break;
                case 7:
                    resultado = "<table width='100%' class='table table-bordered hover'>";
                    resultado += "<tr>"
                            + "<th>Especialista Entrega</th>"
                            + "<th>Fecha</th>"
                            + "<th>Observación del Asesor</th>"
                            + "<th>Asesor Recibe</th></tr>";

                    sql = "SELECT observacion, to_char(fecha, 'dd/MM/yyyy HH24:MI') as fecha, ur.nombrecompleto as usuariorec, ue.nombrecompleto as usuarioent "
                            + "  FROM ingreso_recibidoing ir inner join seguridad.rbac_usuario ue on ue.idusu = idusuarioent "
                            + "                                          inner join seguridad.rbac_usuario ur on ur.idusu = idusuario "
                            + "  WHERE ingreso = " + ingreso;
                    datos = Globales.Obtenerdatos(sql, this.getClass() + "-->DetalleIngreso", 1);
                    while (datos.next()) {
                        resultado += "<tr>"
                                + "<td>" + datos.getString("usuarioent") + "</td>"
                                + "<td>" + datos.getString("fecha") + "</td>"
                                + "<td>" + datos.getString("observacion") + "</td>"
                                + "<td>" + datos.getString("usuariorec") + "</td></tr>";

                    }
                    resultado += "</table>";
                    break;
                case 8:
                    resultado = "<table width='100%' class='table table-bordered hover'>";
                    resultado += "<tr>"
                            + "<th>Nro Orden de Compra</th>"
                            + "<th>Tipo</th>"
                            + "<th>Fecha</th>"
                            + "<th>Asesor</th>"
                            + "<td width='5%'>&nbsp;</td></tr>";

                    sql = "SELECT numero, to_char(fechaorden, 'dd/MM/yyyy HH24:MI') as fecha, u.nombrecompleto as usuario, tipo "
                            + "  FROM orden_compra oc inner join seguridad.rbac_usuario u on u.idusu = idusuario "
                            + "  WHERE ingreso = " + ingreso;
                    datos = Globales.Obtenerdatos(sql, this.getClass() + "-->DetalleIngreso", 1);
                    while (datos.next()) {
                        resultado += "<tr>"
                                + "<td>" + datos.getString("numero") + "</td>"
                                + "<td>" + datos.getString("tipo") + "</td>"
                                + "<td>" + datos.getString("fecha") + "</td>"
                                + "<td>" + datos.getString("usuario") + "</td>"
                                + "<td width='5%'><button class='btn btn-success' title='Imprimir Órden de Compra' type='button' onclick=\"ImprimirOrden('" + datos.getString("numero") + "')\"><span data-icon='&#xe0f7;'></span></button></td></tr>";

                    }
                    resultado += "</table>";
                    break;
                case 9:
                    resultado = "<table width='100%' class='table table-bordered hover'>";
                    resultado += "<tr>"
                            + "<th width='5%'>&nbsp;</th>"
                            + "<th>Nro Factura</th>"
                            + "<th>Estado</th>"
                            + "<th>Registro</th>"
                            + "<th>Enviada</th>"
                            + "<th>correoenvio</th>"
                            + "<th>Anulada</th></tr>";

                    sql = "select DISTINCT f.factura, to_char(f.fecha,'yyyy/MM/dd HH:24') || '<br>' || u.nombrecompleto as registro, f.estado, "
                            + "      to_char(f.fechaenvio,'yyyy/MM/dd HH:24') || '<br>' || ue.nombrecompleto as enviado, "
                            + "      to_char(f.fechaanula,'yyyy/MM/dd HH:24') || '<br>' || ua.nombrecompleto as anulado, f.correoenvia "
                            + "      from factura f inner join factura_datelle fd on f.id = fd.idfactura "
                            + "                     inner join seguridad.rbac_usuario u on u.idusu = f.idusuario "
                            + "                     inner join seguridad.rbac_usuario ua on ua.idusu = f.idusuarioanula "
                            + "                     inner join seguridad.rbac_usuario ue on ue.idusu = f.idusuarioenvia "
                            + "      WHERE ingreso = " + ingreso + " ORDER BY f.factura DESC";
                    datos = Globales.Obtenerdatos(sql, this.getClass() + "-->DetalleIngreso", 1);
                    while (datos.next()) {
                        resultado += "<tr>"
                                + "<td width='5%'><button class='btn btn-success' title='Imprimir Factura' type='button' onclick=\"ImprimirFactura('" + datos.getString("factura") + "')\"><span data-icon='&#xe0f7;'></span></button></td>"
                                + "<td>" + datos.getString("factura") + "</td>"
                                + "<td>" + datos.getString("estado") + "</td>"
                                + "<td>" + datos.getString("registro") + "</td>"
                                + "<td>" + datos.getString("enviado") + "</td>"
                                + "<td>" + datos.getString("anulado") + "</td>"
                                + "<td>" + datos.getString("correoenvia") + "</td></tr>";

                    }
                    resultado += "</table>";
                    break;
                case 10:
                    resultado = "<table width='100%' class='table table-bordered hover'>";
                    resultado += "<tr>"
                            + "<th>Nro Devolucion</th>"
                            + "<th>Asesor Logistico</th>"
                            + "<th>Fecha</th>"
                            + "<th>Observacion Inicial</th>"
                            + "<th>Observación Final</th>"
                            + "<th>Certificado</th>"
                            + "<th>Enviado</th>"
                            + "<th>Entregado</th>"
                            + "<th>Tiempo</th>"
                            + "<th>&nbsp;</th></tr>";

                    sql = "SELECT ir.observacion as observafin, rd.observacion as observaini,  to_char(de.fecha, 'dd/MM/yyyy HH24:MI') as fecha, u.nombrecompleto as asesor, "
                            + "      de.devolucion, tiempo(ir.fecha, de.fecha,3) as tiempo, dd.certificado, "
                            + "      '<b>' || nombreentrega || '</b><br>' || observacionentrega || '<br><b>' || to_char(fechaentrega,'dd/MM/yyyy HH24:MI') || '</b>' as entregado, "
                            + "      coalesce('<b>' || to_char(de.fechaenvio,'dd/MM/yyyy HH24:MI') || '</b><br>' || de.correoenvio,'') as envio "
                            + "  FROM devolucion de INNER JOIN devolucion_detalle dd on de.id = dd.iddevolucion  "
                            + "                     INNER JOIN ingreso_recibidoing ir ON ir.ingreso = dd.ingreso "
                            + "                     inner join seguridad.rbac_usuario u on u.idusu = de.idusuario "
                            + "                                 inner join remision_detalle rd on rd.ingreso = dd.ingreso "
                            + "  WHERE rd.ingreso = " + ingreso;
                    datos = Globales.Obtenerdatos(sql, this.getClass() + "-->DetalleIngreso", 1);
                    while (datos.next()) {
                        resultado += "<tr>"
                                + "<td>" + datos.getString("devolucion") + "</td>"
                                + "<td>" + datos.getString("asesor") + "</td>"
                                + "<td>" + datos.getString("fecha") + "</td>"
                                + "<td>" + datos.getString("observaini") + "</td>"
                                + "<td>" + datos.getString("observafin") + "</td>"
                                + "<td>" + datos.getString("certificado") + "</td>"
                                + "<td>" + datos.getString("envio") + "</td>"
                                + "<td>" + datos.getString("entregado") + "</td>"
                                + "<td>" + datos.getString("tiempo") + "</td>"
                                + "<td width='5%'><button class='btn btn-success' title='Imprimir Devolución' type='button' onclick=\"ImprimirDevolucion('" + datos.getString("devolucion") + "')\"><span data-icon='&#xe0f7;'></span></button></td></tr>";

                    }
                    resultado += "</table>";
                    break;
                case 11:
                    resultado = "<table width='100%' class='table table-bordered hover'>";
                    resultado += "<tr>"
                            + "<th>Nro Devolucion</th>"
                            + "<th>Entregado a</th>"
                            + "<th>Fecha</th>"
                            + "<th>Observación Entrega</th>"
                            + "<th>Devolución</th>"
                            + "<th>Guía de Envío</th>"
                            + "<th>Fotos</th></tr>";
                    sql = "SELECT de.devolucion, nombreentrega, observacionentrega, to_char(fechaentrega,'dd/MM/yyyy HH24:MI') as fechaentrega, de.empresa_envia, guia, paginaweb, en.id as idempresa, "
                            + "  fotos, fotoentrega "
                            + "  FROM devolucion de INNER JOIN devolucion_detalle dd on de.id = dd.iddevolucion  "
                            + "                     INNER JOIN empresa_envio en on en.id = de.empresa_envia "
                            + "  WHERE ingreso = " + ingreso;
                    datos = Globales.Obtenerdatos(sql, this.getClass() + "-->DetalleIngreso", 1);
                    while (datos.next()) {
                        resultado += "<tr>"
                                + "<td>" + datos.getString("devolucion") + "</td>"
                                + "<td>" + datos.getString("nombreentrega") + "</td>"
                                + "<td>" + datos.getString("fechaentrega") + "</td>"
                                + "<td>" + datos.getString("observacionentrega") + "</td>"
                                + "<td width='5%'>" + (datos.getString("fotoentrega").equals("0") ? "<button class='btn btn-success' title='Imprimir Devolución' type='button' onclick=\"ImprimirDevolucionFirma('" + datos.getString("devolucion") + "')\"><span data-icon='&#xe0f7;'></span></button>" : "") + "</td>"
                                + "<td width='5%'>" + (!datos.getString("idempresa").equals("0") ? "<button class='btn btn-primary' title='Ver Guía' type='button' onclick=\"VerGuia('" + datos.getString("guia") + "','" + datos.getString("paginaweb") + "')\"><span data-icon='&#xe0f7;'></span></button>" : "&nbsp;") + "</td>"
                                + "<td width='5%'>" + (!datos.getString("fotos").equals("0") ? "<button class='btn btn-success' title='Fotos Devolución' type='button' onclick=\"VerFotosDevolucion('" + datos.getString("devolucion") + "'," + datos.getString("fotos") + ")\"><span data-icon='&#xe2c7;'></span></button>" : "")
                                + (!datos.getString("fotoentrega").equals("0") ? "<button class='btn btn-info' title='Fotos de Entrega' type='button' onclick=\"VerFotosEntrega('" + datos.getString("devolucion") + "'," + datos.getString("fotoentrega") + ")\"><span data-icon='&#xe2c7;'></span></button>" : "") + "</td></tr>";

                    }
                    resultado += "</table>";
                    break;
                case 12:
                    resultado = "<table width='100%' class='table table-bordered hover'>";
                    resultado += "<tr>"
                            + "<th width='5%'>&nbsp;</th>"
                            + "<th>Nro Informe</th>"
                            + "<th>Tipo</th>"
                            + "<th>Fecha</th>"
                            + "<th>Observación</th>"
                            + "<th>Conclusion</th>"
                            + "<th>Asesor</th>"
                            + "<th>Anulado</th></tr>";

                    sql = "SELECT informe, observacion, conclusion, to_char(fecha,'yyyy/MM/dd HH24:MI') as fecha, "
                            + "         CASE WHEN tipo =  1 THEN 'Devolución' else 'Mantenimiento' end as destipo, tipo, ingreso, "
                            + "         u.nombrecompleto as asesor, ua.nombrecompleto || '<br>' || to_char(fechaanulacion,'yyyy/MM/dd HH24:MI') as anulado "
                            + "    FROM informetecnico_dev i inner join seguridad.rbac_usuario u on u.idusu = idusuario "
                            + "                                          inner join seguridad.rbac_usuario ua on ua.idusu = idusuarioanula "
                            + "    WHERE ingreso = " + ingreso + " ORDER BY i.id DESC";
                    datos = Globales.Obtenerdatos(sql, this.getClass() + "-->DetalleIngreso", 1);
                    while (datos.next()) {
                        resultado += "<tr>"
                                + "<td width='5%'><button class='btn btn-success' title='Imprimir Informe Técnico' type='button' onclick=\"ImprimirInforme('" + datos.getString("informe") + "'," + datos.getString("tipo") + "," + datos.getString("ingreso") + ")\"><span data-icon='&#xe0f7;'></span></button></td>"
                                + "<td>" + datos.getString("informe") + "</td>"
                                + "<td>" + datos.getString("destipo") + "</td>"
                                + "<td>" + datos.getString("fecha") + "</td>"
                                + "<td>" + datos.getString("observacion") + "</td>"
                                + "<td>" + datos.getString("conclusion") + "</td>"
                                + "<td>" + datos.getString("asesor") + "</td>"
                                + "<td>" + datos.getString("anulado") + "</td></tr>";

                    }
                    resultado += "</table>";
                    break;
                case 14:
                    resultado = "<table width='100%' class='table table-bordered hover'>";
                    resultado += "<tr>"
                            + "<th>Especialista Entrega</th>"
                            + "<th>Fecha</th>"
                            + "<th>Observación del Asesor</th>"
                            + "<th>Asesor Recibe</th></tr>";

                    sql = "SELECT observacion, to_char(fecha, 'dd/MM/yyyy HH24:MI') as fecha, ur.nombrecompleto as usuariorec, ue.nombrecompleto as usuarioent "
                            + "  FROM ingreso_recibidocome ir inner join seguridad.rbac_usuario ue on ue.idusu = idusuarioent "
                            + "                                          inner join seguridad.rbac_usuario ur on ur.idusu = idusuario "
                            + "  WHERE ingreso = " + ingreso;
                    datos = Globales.Obtenerdatos(sql, this.getClass() + "-->DetalleIngreso", 1);
                    while (datos.next()) {
                        resultado += "<tr>"
                                + "<td>" + datos.getString("usuarioent") + "</td>"
                                + "<td>" + datos.getString("fecha") + "</td>"
                                + "<td>" + datos.getString("observacion") + "</td>"
                                + "<td>" + datos.getString("usuariorec") + "</td></tr>";

                    }
                    resultado += "</table>";
                    break;
                case 15:
                    resultado = "<table width='100%' class='table table-bordered hover'>";
                    resultado += "<tr>"
                            + "<th width='5%'>&nbsp;</td>"
                            + "<th>Nro Salida</th>"
                            + "<th>Proveedor</th>"
                            + "<th>Estado</th>"
                            + "<th>Registro</th>"
                            + "<th>Enviado</th>"
                            + "<th>Correo</th>"
                            + "<th>Anulado</th>"
                            + "<th>Obser.Anula</th></tr>";

                    sql = "SELECT salida, to_char(fecha,'yyyy/MM/dd HH24:MI') || '<br>' || u.nombrecompleto as registro, "
                            + "         to_char(fechaenvio,'yyyy/MM/dd HH24:MI') || '<br>' || ua.nombrecompleto as anulado,  "
                            + "         to_char(fechaenvio,'yyyy/MM/dd HH24:MI') || '<br>' || ue.nombrecompleto as enviado,  "
                            + "         observacionanula, s.estado, s.correoenvio, p.nombrecompleto as proveedor  "
                            + "  FROM salida s INNER JOIN salida_detalle sd on s.id = sd.idsalida "
                            + "		                        INNER JOIN seguridad.rbac_usuario u on u.idusu = s.idusuariosal "
                            + "		                        INNER JOIN seguridad.rbac_usuario ua on ua.idusu = s.idusuarioanula "
                            + "		                        INNER JOIN seguridad.rbac_usuario ue on ue.idusu = s.idusuarioenvia "
                            + "                    INNER JOIN proveedores p on p.id = s.idproveedor "
                            + "WHERE sd.ingreso = " + ingreso + " ORDER BY salida DESC";
                    datos = Globales.Obtenerdatos(sql, this.getClass() + "-->DetalleIngreso", 1);
                    while (datos.next()) {
                        resultado += "<tr>"
                                + "<td width='5%'><button class='btn btn-success' title='Imprimir Salida para Tercerizar' type='button' onclick='ImprimirSalida(" + datos.getString("salida") + ")'><span data-icon='&#xe0f7;'></span></button></td>"
                                + "<td>" + datos.getString("salida") + "</td>"
                                + "<td>" + datos.getString("proveedor") + "</td>"
                                + "<td>" + datos.getString("estado") + "</td>"
                                + "<td>" + datos.getString("registro") + "</td>"
                                + "<td>" + datos.getString("enviado") + "</td>"
                                + "<td>" + datos.getString("correoenvio") + "</td>"
                                + "<td>" + datos.getString("anulado") + "</td>"
                                + "<td>" + datos.getString("observacionanula") + "</td></tr>";

                    }
                    resultado += "</table>";
                    break;
                case 16:
                    resultado = "<table width='100%' class='table table-bordered hover'>";
                    resultado += "<tr>"
                            + "<th width='5%'>&nbsp;</td>"
                            + "<th>Solicitud</th>"
                            + "<th>Estado</th>"
                            + "<th>Registro</th>"
                            + "<th>Anulado</th>"
                            + "<th>Obser.Anula</th>"
                            + "<th>Asesor</th>"
                            + "<th>Cotización</th></tr>";

                    sql = "SELECT DISTINCT solicitud, to_char(fecha,'yyyy/MM/dd HH24:MI') || '<br>' as registro, to_char(fecha,'yyyy/MM/dd HH24:MI') || '<br>' as registro, "
                            + "      coalesce(to_char(fechaanulado,'yyyy/MM/dd HH24:MI'),'') as anulado, coalesce(observacionanula,'') as observacionanula,  coalesce('<b>' || cotizacion  || '</b><br>' || to_char(fechacotizado,'yyyy/MM/dd HH24:MI'),'') as cotizado, "
                            + "      coalesce(u.nombrecompleto,'') as asesor, s.estado "
                            + "  FROM web.solicitud s INNER JOIN web.solicitud_detalle sd using (solicitud) "
                            + "                       INNER JOIN seguridad.rbac_usuario u on u.idusu = s.asesor "
                            + "  WHERE ingreso = " + ingreso + " ORDER BY solicitud DESC";
                    datos = Globales.Obtenerdatos(sql, this.getClass() + "-->DetalleIngreso", 1);
                    while (datos.next()) {
                        resultado += "<tr>"
                                + "<td width='5%'><button class='btn btn-success' title='Imprimir Solicitud' type='button' onclick='ImprimirSolicitud(" + datos.getString("solicitud") + ")'><span data-icon='&#xe0f7;'></span></button></td>"
                                + "<td>" + datos.getString("solicitud") + "</td>"
                                + "<td>" + datos.getString("estado") + "</td>"
                                + "<td>" + datos.getString("registro") + "</td>"
                                + "<td>" + datos.getString("anulado") + "</td>"
                                + "<td>" + datos.getString("observacionanula") + "</td>"
                                + "<td>" + datos.getString("asesor") + "</td>"
                                + "<td>" + datos.getString("cotizado") + "</td></tr>";

                    }
                    resultado += "</table>";
                    break;
                case 17:
                    resultado = "<table width='100%' class='table table-bordered hover'>";
                    resultado += "<tr>"
                            + "<th width='5%'>&nbsp;</th>"
                            + "<th>Items</th>"
                            + "<th>Informe</th>"
                            + "<th>Fecha</th>"
                            + "<th>Observacion</th>"
                            + "<th>Asesor</th></tr>";

                    sql = "SELECT informe, numero, observacion, to_char(fecha,'yyyy/MM/dd HH24:MI') as fecha, "
                            + "         ingreso, "
                            + "         u.nombrecompleto as asesor "
                            + "    FROM informetecnico_tercerizado i inner join seguridad.rbac_usuario u on u.idusu = idusuario "
                            + "    WHERE ingreso = " + ingreso + " ORDER BY i.id DESC";
                    datos = Globales.Obtenerdatos(sql, this.getClass() + "-->DetalleIngreso", 1);
                    while (datos.next()) {
                        resultado += "<tr>"
                                + "<td width='5%'><button class='btn btn-success' title='Imprimir Informe Técnico Tercerizado' type='button' onclick=\"ImprimirInformeTerce('" + datos.getString("numero") + "-" + datos.getString("informe") + "')\"><span data-icon='&#xe0f7;'></span></button></td>"
                                + "<td>" + datos.getString("numero") + "</td>"
                                + "<td>" + datos.getString("informe") + "</td>"
                                + "<td>" + datos.getString("fecha") + "</td>"
                                + "<td>" + datos.getString("observacion") + "</td>"
                                + "<td>" + datos.getString("asesor") + "</td></tr>";

                    }
                    resultado += "</table>";
                    break;
            }
        } catch (SQLException e) {
            return e.getMessage();
        }

        return resultado;
    }

    public String BuscarIngresoOperacion(HttpServletRequest request) {
        int ingreso = Globales.Validarintnonull(request.getParameter("ingreso"));
        int magnitud = Globales.Validarintnonull(request.getParameter("magnitud"));
        String medida = request.getParameter("medida");
        int revision = Globales.Validarintnonull(request.getParameter("revision"));
        int version = Globales.Validarintnonull(request.getParameter("version"));
        String sql3 = "";
        String sql5 = "";
        String sql6 = "";
        switch (magnitud) {
            case 6:
                sql = "select e.descripcion as equipo, ma.descripcion as marca, mo.descripcion as modelo, re.remision, "
                        + "      r.observacion, r.estado, r.serie, trim(to_char(r.ingreso,'0000000')) as ingreso, r.fotos, "
                        + "       desde, hasta, medida, m.id as idmagnitud,  to_char(rl.fecha, 'yyyy-MM-dd HH24:MI') as fecharec, ui.nombrecompleto as usuarioing, "
                        + "      case when coalesce(cd.direccion,'SEDE') = 'SEDE' OR trim(cd.direccion) = '' THEN cs.direccion || ', ' || ci.descripcion || ' (' || d.descripcion || ')' else cd.direccion end as direccion, "
                        + "      re.remision, ui.nombrecompleto as usuarioing, tiempo(rl.fecha, now(),1) as tiempo, "
                        + "      case when cd.id is null then cc.email else (SELECT c2.email FROM clientes_contac c2 where c2.id= cd.idcontacto) end as email, "
                        + "       cd.proxima, r.sitio, r.garantia, tolerancia_sch, resolucion_sch, "
                        + "      coalesce((select max(revision) from instrumento ins2 where ins2.ingreso = r.ingreso and ins2.revision = " + revision + " and ins2.idversion = " + version + "),0) as maxrevision, "
                        + "      coalesce((select numero from certificados ce where ce.ingreso = r.ingreso and ce.revision = " + revision + "),'') as certificado, "
                        + "      clase, ins.descripcion, indicacion, resolucion, tolerancia, clasificacion, ins.id as idinstrumento, to_char(re.fecha, 'yyyy-MM-dd') as fechaing, "
                        + "      cli.nombrecompleto as cliente, m.contador+1 as contador, coalesce((select valor from conversiones con where con.medidacon = i.medida and con.medida = '" + medida + "'),0) as medidacon, "
                        + "      ase.idusu as asesor, cli.aprobar_cotizacion, cli.habitual, r.fechaaprocoti "
                        + "  from remision_detalle r inner join equipo e on r.idequipo = e.id "
                        + "                          inner join remision re on re.id = r.idremision "
                        + "                          inner join magnitudes m on m.id = e.idmagnitud "
                        + "                          inner join modelos mo on mo.id = r.idmodelo  "
                        + "		                                inner join marcas ma on ma.id = mo.idmarca "
                        + "                          inner join magnitud_intervalos i on i.id = r.idintervalo "
                        + "                          inner join ingreso_recibidolab rl on rl.ingreso = r.ingreso "
                        + "                          inner join clientes cli on cli.id = re.idcliente "
                        + "                          INNER JOIN clientes_sede cs on re.idsede = cs.id "
                        + "                          INNER JOIN clientes_contac cc on re.idcontacto = cc.id "
                        + "	                                    inner join ciudad ci on ci.id = cs.idciudad "
                        + "	                                    inner join departamento d on d.id = ci.iddepartamento "
                        + "                          inner join seguridad.rbac_usuario ui on ui.idusu = rl.idusuario "
                        + "                          inner join seguridad.rbac_usuario ase on ase.idusu = cli.asesor "
                        + "                          left join instrumento ins on ins.ingreso = r.ingreso and ins.revision = " + revision + " and idversion = " + version
                        + "                          left join cotizacion_detalle cd on cd.ingreso = r.ingreso and cd.estado in ('Aprobado','Cerrado','Cotizado','POR REEMPLAZAR') "
                        + "  where r.ingreso = " + ingreso + " ORDER BY cd.idcotizacion desc limit 1";

                sql3 = "SELECT id, to_char(fecha,'yyyy-MM-dd HH24:MI' ) as fecha, u.nombrecompleto as usuario, puntos "
                        + "  from operprevia_partorsional op inner join seguridad.rbac_usuario u on u.idusu = op.idusuario "
                        + "  where ingreso = " + ingreso + " and revision=" + revision;
                break;
            case 2:
                sql = "select e.descripcion as equipo, ma.descripcion as marca, mo.descripcion as modelo, re.remision, "
                        + "      r.observacion, r.estado, r.serie, trim(to_char(r.ingreso,'0000000')) as ingreso, r.fotos, "
                        + "       desde, hasta, medida, m.id as idmagnitud,  to_char(rl.fecha, 'yyyy-MM-dd HH24:MI') as fecharec, ui.nombrecompleto as usuarioing, "
                        + "      case when coalesce(cd.direccion,'SEDE') = 'SEDE' OR trim(cd.direccion) = '' THEN cs.direccion || ', ' || ci.descripcion || ' (' || d.descripcion || ')' else cd.direccion end as direccion, "
                        + "      re.remision, ui.nombrecompleto as usuarioing, tiempo(rl.fecha, now(),1) as tiempo, coef_senc, "
                        + "      case when cd.id is null then cc.email else (SELECT c2.email FROM clientes_contac c2 where c2.id= cd.idcontacto) end as email, "
                        + "      r.informetecnico, cd.proxima, r.sitio,tipolectura, r.garantia, delrango,"
                        + "      coalesce((select max(revision) from instrumento_presion ins2 where ins2.ingreso = r.ingreso and ins2.revision = " + revision + " and ins2.idversion = " + version + "),0) as maxrevision, "
                        + "      clase, ins.tipo, division_escala, resolucion, glicerina, altura_patron, altura_ibc, tope_cero, fluido_usado, ins.id as idinstrumento, to_char(re.fecha, 'yyyy-MM-dd') as fechaing, "
                        + "      cli.nombrecompleto as cliente, m.contador+1 as contador, coalesce((select valor from conversiones con where con.medidacon = i.medida and con.medida = '" + medida + "'),0) as medidacon, "
                        + "      coalesce((SELECT max(op.revision) FROM operprevia_presion op where op.ingreso =r.ingreso),0) as revision, "
                        + "      ase.idusu as asesor, cli.aprobar_cotizacion, cli.habitual, r.fechaaprocoti "
                        + "  from remision_detalle r inner join equipo e on r.idequipo = e.id "
                        + "                          inner join remision re on re.id = r.idremision "
                        + "                          inner join magnitudes m on m.id = e.idmagnitud "
                        + "                          inner join modelos mo on mo.id = r.idmodelo "
                        + "                          inner join marcas ma on ma.id = mo.idmarca "
                        + "                          inner join magnitud_intervalos i on i.id = r.idintervalo "
                        + "                          inner join ingreso_recibidolab rl on rl.ingreso = r.ingreso "
                        + "                          inner join clientes cli on cli.id = re.idcliente "
                        + "                          INNER JOIN clientes_sede cs on re.idsede = cs.id "
                        + "                          INNER JOIN clientes_contac cc on re.idcontacto = cc.id "
                        + "                          inner join ciudad ci on ci.id = cs.idciudad "
                        + "                          inner join departamento d on d.id = ci.iddepartamento "
                        + "                          inner join seguridad.rbac_usuario ui on ui.idusu = rl.idusuario "
                        + "                          inner join seguridad.rbac_usuario ase on ase.idusu = cli.asesor "
                        + "                          left join instrumento_presion ins on ins.ingreso = r.ingreso and ins.revision = " + revision + " and idversion = " + version
                        + "                          left join cotizacion_detalle cd on cd.ingreso = r.ingreso and cd.estado in ('Aprobado','Cerrado','Cotizado','POR REEMPLAZAR') "
                        + " where r.ingreso = " + ingreso + " ORDER BY cd.idcotizacion desc limit 1";

                sql3 = "SELECT * from operprevia_presion where ingreso = " + ingreso + " and revision=" + revision;
                break;
            case 5:
                sql = "select e.descripcion as equipo, ma.descripcion as marca, mo.descripcion as modelo, re.remision, "
                        + "      r.observacion, r.estado, r.serie, trim(to_char(r.ingreso,'0000000')) as ingreso, r.fotos, "
                        + "       desde, hasta, medida, m.id as idmagnitud,  to_char(rl.fecha, 'yyyy-MM-dd HH24:MI') as fecharec, ui.nombrecompleto as usuarioing, "
                        + "      case when coalesce(cd.direccion,'SEDE') = 'SEDE' OR trim(cd.direccion) = '' THEN cs.direccion || ', ' || ci.descripcion || ' (' || d.descripcion || ')' else cd.direccion end as direccion, "
                        + "      re.remision, ui.nombrecompleto as usuarioing, tiempo(rl.fecha, now(),1) as tiempo,  "
                        + "      case when cd.id is null then cc.email else (SELECT c2.email FROM clientes_contac c2 where c2.id= cd.idcontacto) end as email, "
                        + "      cd.proxima, r.sitio, r.garantia,  r.idequipo, "
                        + "      ins.id as idinstrumento, divisionescala, resolucion, revision, errormaxpos, errormaxneg, to_char(re.fecha, 'yyyy-MM-dd') as fechaing, "
                        + "      cli.nombrecompleto as cliente, m.contador+1 as contador, coalesce((select valor from conversiones con where con.medidacon = i.medida and con.medida = '" + medida + "'),0) as medidacon, "
                        + "      coalesce((SELECT max(op.revision) FROM operprevia_presion op where op.ingreso =r.ingreso),0) as revision, "
                        + "      ase.idusu as asesor, cli.aprobar_cotizacion, cli.habitual, r.fechaaprocoti "
                        + "  from remision_detalle r inner join equipo e on r.idequipo = e.id "
                        + "                          inner join remision re on re.id = r.idremision "
                        + "                          inner join magnitudes m on m.id = e.idmagnitud "
                        + "                          inner join modelos mo on mo.id = r.idmodelo "
                        + "                          inner join marcas ma on ma.id = mo.idmarca "
                        + "                          inner join magnitud_intervalos i on i.id = r.idintervalo "
                        + "                          inner join ingreso_recibidolab rl on rl.ingreso = r.ingreso "
                        + "                          inner join clientes cli on cli.id = re.idcliente "
                        + "                          INNER JOIN clientes_sede cs on re.idsede = cs.id "
                        + "                          INNER JOIN clientes_contac cc on re.idcontacto = cc.id "
                        + "                          inner join ciudad ci on ci.id = cs.idciudad "
                        + "                          inner join departamento d on d.id = ci.iddepartamento "
                        + "                          inner join seguridad.rbac_usuario ui on ui.idusu = rl.idusuario "
                        + "                          inner join seguridad.rbac_usuario ase on ase.idusu = cli.asesor "
                        + "                          left join instrumento_longitud ins on ins.ingreso = r.ingreso  "
                        + "                          left join cotizacion_detalle cd on cd.ingreso = r.ingreso  and cd.estado in ('Aprobado','Cerrado','Cotizado','POR REEMPLAZAR') "
                        + "  where r.ingreso = " + ingreso + " ORDER BY cd.idcotizacion desc limit 1";

                sql3 = "SELECT * from operprevia_longitud where ingreso = " + ingreso + " and revision = " + revision;
                sql5 = "SELECT op.* from operprevia_longitud ol INNER JOIN operprevia_patron op on idoperacion = ol.id where ingreso = " + ingreso + " and revision = " + revision;
                break;
            case 3:
                sql = "select e.descripcion as equipo, ma.descripcion as marca, mo.descripcion as modelo, re.remision, "
                        + "      r.observacion, r.estado, r.serie, trim(to_char(r.ingreso,'0000000')) as ingreso, r.fotos, "
                        + "       i.desde, i.hasta, i.medida, i2.desde as desde2, i2.hasta as hasta2, i2.medida as medida2, m.id as idmagnitud,  to_char(rl.fecha, 'yyyy-MM-dd HH24:MI') as fecharec, ui.nombrecompleto as usuarioing, "
                        + "      case when coalesce(cd.direccion,'SEDE') = 'SEDE' OR trim(cd.direccion) = '' THEN cs.direccion || ', ' || ci.descripcion || ' (' || d.descripcion || ')' else cd.direccion end as direccion, "
                        + "      re.remision, ui.nombrecompleto as usuarioing, tiempo(rl.fecha, now(),1) as tiempo,  "
                        + "      case when cd.id is null then cc.email else (SELECT c2.email FROM clientes_contac c2 where c2.id= cd.idcontacto) end as email, "
                        + "      r.informetecnico, cd.proxima, r.sitio, r.garantia, "
                        + "      divisionescala, resolucion, ins.id as idinstrumento, emisividad, rangobanda, to_char(re.fecha, 'yyyy-MM-dd') as fechaing, "
                        + "      cli.nombrecompleto as cliente, m.contador+1 as contador, coalesce((select valor from conversiones con where con.medidacon = i.medida and con.medida = '" + medida + "'),0) as medidacon, "
                        + "      coalesce((SELECT max(op.revision) FROM operprevia_presion op where op.ingreso =r.ingreso),0) as revision, "
                        + "      ase.idusu as asesor, cli.aprobar_cotizacion, cli.habitual, r.fechaaprocoti "
                        + "  from remision_detalle r inner join equipo e on r.idequipo = e.id "
                        + "                          inner join remision re on re.id = r.idremision "
                        + "                          inner join magnitudes m on m.id = e.idmagnitud "
                        + "                          inner join modelos mo on mo.id = r.idmodelo   "
                        + "                          inner join marcas ma on ma.id = mo.idmarca "
                        + "                          inner join magnitud_intervalos i on i.id = r.idintervalo "
                        + "                          inner join magnitud_intervalos i2 on i2.id = r.idintervalo2 "
                        + "                          inner join ingreso_recibidolab rl on rl.ingreso = r.ingreso "
                        + "                          inner join clientes cli on cli.id = re.idcliente "
                        + "                          INNER JOIN clientes_sede cs on re.idsede = cs.id "
                        + "                          INNER JOIN clientes_contac cc on re.idcontacto = cc.id "
                        + "                          inner join ciudad ci on ci.id = cs.idciudad "
                        + "                          inner join departamento d on d.id = ci.iddepartamento "
                        + "                          inner join seguridad.rbac_usuario ui on ui.idusu = rl.idusuario "
                        + "                          inner join seguridad.rbac_usuario ase on ase.idusu = cli.asesor "
                        + "                          left join instrumento_temperatura ins on ins.ingreso = r.ingreso "
                        + "                          left join cotizacion_detalle cd on cd.ingreso = r.ingreso and cd.estado in ('Aprobado','Cerrado','Cotizado','POR REEMPLAZAR') "
                        + "  where r.ingreso = " + ingreso + " ORDER BY cd.idcotizacion desc limit 1";

                sql3 = "SELECT * from operprevia_temperatura where ingreso = " + ingreso + " and revision=" + revision;
                sql5 = "SELECT op.* from operprevia_temperatura ol INNER JOIN operprevia_patron op on idoperacion = ol.id where ingreso = " + ingreso + " and revision=" + revision;
                sql6 = "SELECT op.* from operprevia_temperatura ol INNER JOIN operprevia_lectura op on idoperacion = ol.id where ingreso = " + ingreso + " and revision=" + revision;
                break;
            //--and cd.estado  in ('Aprobado','Cerrado','Cotizado')
            }

        String sql4 = "select c.cotizacion, c.estado, cd.observacion,me.descripcion as metodo, punto, declaracion, cd.nombreca, "
                + "          case when cd.direccion = 'SEDE' THEN cs.direccion || ', ' || ci.descripcion || ' (' ||d.descripcion ||')' else cd.direccion end as direccion, coalesce(proxima,'NA') as proxima,"
                + "          max(entrega) as entrega, u.nombrecompleto as asesor, s.nombre as servicio "
                + "  from cotizacion c INNER JOIN cotizacion_detalle cd on c.id = cd.idcotizacion "
                + "                     INNER JOIN Clientes cli  on cli.id = c.idcliente "
                + "                     INNER JOIN clientes_sede cs on c.idsede = cs.id "
                + "                     INNER JOIN clientes_contac cc on c.idcontacto = cc.id "
                + "                     inner join metodo me on me.id = cd.metodo "
                + "                     inner join ciudad ci on ci.id = cs.idciudad "
                + "                     inner join departamento d on d.id = ci.iddepartamento "
                + "                     inner join seguridad.rbac_usuario u on u.idusu = c.idusuario "
                + "                     inner join servicio s on s.id = cd.idservicio "
                + "  where idserviciodep = 0  and ingreso = " + ingreso + " and c.estado in ('Aprobado','Cerrado','POR REEMPLAZAR') "
                + "  group by c.cotizacion, c.estado, cd.observacion,me.descripcion, punto, declaracion, cd.nombreca, u.nombrecompleto, "
                + "          cd.direccion, cs.direccion, ci.descripcion, d.descripcion, proxima, s.nombre ORDER BY c.cotizacion DESC";

        String sql7 = "select p.id, p.descripcion "
                + " from ingreso_plantilla ip inner join plantillas p on p.id = ip.idplantilla "
                + " where ingreso = " + ingreso;

        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->BuscarIngresoOperacion") + "|" + Globales.ObtenerDatosJSon(sql3, this.getClass() + "-->BuscarIngresoOperacion") + "|" + Globales.ObtenerDatosJSon(sql4, this.getClass() + "-->BuscarIngresoOperacion")
                + "|" + (!sql5.equals("") ? Globales.ObtenerDatosJSon(sql5, this.getClass() + "-->BuscarIngresoOperacion") + "|" : "") + (!sql6.equals("") ? Globales.ObtenerDatosJSon(sql6, this.getClass() + "-->ConsultarIngresos") + "|" : "") + Globales.ObtenerCombo(sql7, 1, 0, 0);

    }

    public String BuscarIngresoPlaCert(HttpServletRequest request) {
        int ingreso = Globales.Validarintnonull(request.getParameter("ingreso"));
        int magnitud = Globales.Validarintnonull(request.getParameter("magnitud"));
        String medida = request.getParameter("medida");
        int numero = Globales.Validarintnonull(request.getParameter("numero"));
        int version = Globales.Validarintnonull(request.getParameter("version"));
        int revision = 1;
        int anulado = 0;
        String sql2 = "";
        String sql6 = "";
        String sql7 = "";
        String sql4 = "";
        String sql5 = "";
        String sql8 = "";
        String sql9 = "";
        String sql10 = "";
        String sql11 = "";
        String sql12 = "";
        String sql13 = "";
        String sql14 = "";
        String sql15 = "";

        String sql3 = "";

        try {
            String magnitudes = Globales.ObtenerUnValor("SELECT magnitud FROM seguridad.rbac_usuario WHERE idusu = " + idusuario);
            String[] a_magnitud = magnitudes.split(",");
            int existe = 0;

            if (Arrays.asList(a_magnitud).contains(magnitud)) {
                existe = 1;
            }
            if (existe == 0) {
                return "1|Usted no tiene asignada esta magnitud";
            }

            switch (magnitud) {
                case 6:
                    sql = "SELECT revision, idusuarioanula FROM certificados_previos WHERE ingreso = " + ingreso + " and item = " + numero + " ORDER BY id desc";
                    datos = Globales.Obtenerdatos(sql, this.getClass() + "-->ConsultarIngresos", 1);
                    if (datos.next()) {
                        anulado = Globales.Validarintnonull(datos.getString("idusuarioanula"));
                        revision = Globales.Validarintnonull(datos.getString("revision"));
                        if (anulado > 0) {
                            revision++;
                        }
                    }
                    sql = "select cli.id as idcliente, e.descripcion as equipo, ma.descripcion as marca, mo.descripcion as modelo, re.remision, cuadrante, "
                            + "      r.observacion, r.estado, r.serie, trim(to_char(r.ingreso,'0000000')) as ingreso, r.fotos, op.puntos,r.sitio, "
                            + "       r.ingreso, desde, hasta, medida, m.id as idmagnitud,  to_char(op.fecha, 'yyyy-MM-dd HH24:MI') as fecharec, ui.nombrecompleto as usuarioing, "
                            + "      case when coalesce(cd.direccion,'SEDE') = 'SEDE' OR trim(cd.direccion) = '' THEN cs.direccion || ', ' || ci.descripcion || ' (' || d.descripcion || ')' else cd.direccion end as direccion, "
                            + "      re.remision, ui.nombrecompleto as usuarioing, tiempo(op.fecha, now(),1) as tiempo, cc.email, descripcion1, desviacion, "
                            + "      ins.clase, ins.descripcion, indicacion, resolucion, tolerancia, resolucion_sch, tolerancia_sch, clasificacion, ins.lectura, ins.id as idinstrumento, to_char(re.fecha, 'yyyy-MM-dd') as fechaing, "
                            + "      case when coalesce(cd.nombreca,'') <> '' then cd.nombreca else cli.nombrecompleto end as cliente, m.contador+1 as contador, coalesce((select valor from conversiones con where con.medidacon = i.medida and con.medida = '" + medida + "'),0) as medidacon, firmacertificado, "
                            + revision + " as revision "
                            + "  from remision_detalle r inner join equipo e on r.idequipo = e.id "
                            + "                          inner join remision re on re.id = r.idremision "
                            + "                          inner join magnitudes m on m.id = e.idmagnitud "
                            + "                          inner join modelos mo on mo.id = r.idmodelo  "
                            + "                          inner join marcas ma on ma.id = mo.idmarca "
                            + "                          inner join magnitud_intervalos i on i.id = r.idintervalo  "
                            + "                          inner join clientes cli on cli.id = re.idcliente "
                            + "                          INNER JOIN clientes_sede cs on re.idsede = cs.id "
                            + "                          INNER JOIN clientes_contac cc on re.idcontacto = cc.id "
                            + "                          inner join ciudad ci on ci.id = cs.idciudad "
                            + "                          inner join departamento d on d.id = ci.iddepartamento "
                            + "                          inner join operprevia_partorsional op on op.ingreso = r.ingreso "
                            + "                          inner join seguridad.rbac_usuario ui on ui.idusu = op.idusuario "
                            + "                          left join instrumento ins on ins.ingreso = r.ingreso and idversion = " + version
                            + "                          left JOIN clasi_torcometros ct on ct.tipo = ins.clasificacion and ct.clase = ins.clase "
                            + "                          left join cotizacion_detalle cd on cd.ingreso = r.ingreso "
                            + "                          left join cotizacion co on co.id = cd.idcotizacion and co.estado in ('Aprobado','Cerrado','Cotizado') "
                            + "  where r.ingreso = " + ingreso + "   order by ins.id desc, op.id desc limit 1";

                    sql2 = "SELECT * from operprevia_partor where ingreso = " + ingreso + " and revision=" + revision;

                    sql4 = "SELECT cd.*, cp.factor "
                            + "  from certificado_datos_a cd inner join certificados_previos cp on cp.ingreso = cd.ingreso and  cp.revision = cd.revision and cp.item = cd.numero "
                            + "  where cp.ingreso = " + ingreso + " and cp.item = " + numero + " and cp.revision = " + revision;

                    sql11 = "select cr.*  from certificado_repetibilidad cr inner join certificados_previos cp on cp.id = cr.idoperacion "
                            + "where cp.ingreso = " + ingreso + " and cp.item = " + numero + " and cp.revision = " + revision + " ORDER BY cr.id";
                    break;
                case 2:
                    sql = "SELECT revision, idusuarioanula FROM certificados WHERE ingreso = " + ingreso + " and item = " + numero + " ORDER BY id desc";
                    datos = Globales.Obtenerdatos(sql, this.getClass() + "-->ConsultarIngresos", 1);
                    if (datos.next()) {
                        anulado = Globales.Validarintnonull(datos.getString("idusuarioanula"));
                        revision = Globales.Validarintnonull(datos.getString("revision"));
                        if (anulado > 0) {
                            revision++;
                        }
                    }
                    sql = "select e.descripcion || ' (' || tipolectura || ')' as equipo, tipolectura, ma.descripcion as marca, mo.descripcion as modelo, re.remision, "
                            + "      r.observacion, r.estado, r.serie, trim(to_char(r.ingreso,'0000000')) as ingreso, r.fotos, alcance_desde, alcance_hasta, "
                            + "       desde, hasta, medida, m.id as idmagnitud,  to_char(rl.fecha, 'yyyy-MM-dd HH24:MI') as fecharec, ui.nombrecompleto as usuarioing, "
                            + "      case when coalesce(cd.direccion,'SEDE') = 'SEDE' OR trim(cd.direccion) = '' THEN cs.direccion || ', ' || ci.descripcion || ' (' || d.descripcion || ')' else cd.direccion end as direccion, "
                            + "      re.remision, ui.nombrecompleto as usuarioing, tiempo(rl.fecha, now(),1) as tiempo, cc.email, cd.proxima, coef_senc, "
                            + "      clase, ins.tipo, division_escala, resolucion, glicerina, altura_patron, altura_ibc, tope_cero, fluido_usado, ins.id as idinstrumento, to_char(re.fecha, 'yyyy-MM-dd') as fechaing, "
                            + "      tf.densidad, tf.derivada, (SELECT f.densidad || '!' || f.derivada FROM tipo_fluido f where f.fluido = 'AIRE') AS aire, delrango, "
                            + "      case when coalesce(cd.nombreca,'') <> '' then cd.nombreca ELSE cli.nombrecompleto END as cliente, m.contador+1 as contador, coalesce((select valor from conversiones con where con.medidacon = i.medida and con.medida = '" + medida + "'),0) as medidacon, "
                            + "      coalesce((select valor from conversiones con where con.medida = 'psi' and con.medidacon = i.medida),0) as medidacon, "
                            + "      coalesce((select valor from conversiones con where con.medida = 'kPa' and con.medidacon = i.medida),0) as medidaconkpa, r.sitio, "
                            + "      coalesce((select valor from conversiones con where con.medida = 'Pa' and con.medidacon = i.medida),0) as medidaconpa, firmacertificado, "
                            + "      coalesce((select revision from certificados cer where cer.ingreso = r.ingreso and coalesce(cer.horaini,'') <> '' order by revision desc limit 1),0) as revision "
                            + "  from remision_detalle r inner join equipo e on r.idequipo = e.id "
                            + "                          inner join remision re on re.id = r.idremision "
                            + "                          inner join magnitudes m on m.id = e.idmagnitud "
                            + "                          inner join modelos mo on mo.id = r.idmodelo  "
                            + "                          inner join marcas ma on ma.id = mo.idmarca "
                            + "                          inner join magnitud_intervalos i on i.id = " + (numero == 1 ? "r.idintervalo" : "r.idintervalo2")
                            + "                          inner join ingreso_recibidolab rl on rl.ingreso = r.ingreso "
                            + "                          inner join clientes cli on cli.id = re.idcliente "
                            + "                          INNER JOIN clientes_sede cs on re.idsede = cs.id "
                            + "                          INNER JOIN clientes_contac cc on re.idcontacto = cc.id "
                            + "                          inner join ciudad ci on ci.id = cs.idciudad "
                            + "                          inner join departamento d on d.id = ci.iddepartamento "
                            + "                          inner join seguridad.rbac_usuario ui on ui.idusu = rl.idusuario "
                            + "                          left join instrumento_presion ins on ins.ingreso = r.ingreso and ins.revision = " + revision + " and idversion = " + version
                            + "                          left JOIN tipo_fluido tf ON tf.fluido = fluido_usado "
                            + "                          left join cotizacion_detalle cd on cd.ingreso = r.ingreso "
                            + "                          left join cotizacion co on co.id = cd.idcotizacion and co.estado in ('Aprobado','Cerrado','Cotizado') "
                            + " where r.ingreso = " + ingreso;

                    sql2 = "SELECT * from operprevia_presion where ingreso = " + ingreso + " and revision=" + revision;

                    sql4 = "SELECT cp.* from certificado_datos_pre cp inner join certificados_previos op on op.id = cp.idcerprevio  where ingreso = " + ingreso + " and item = " + numero + " and revision = " + revision + " ORDER BY cp.id";

                    sql6 = "SELECT ic.*, (SELECT max(utotal)  FROM instcondamb_transductor1 it where it.idpatron = i.id) as maxutotal "
                            + " FROM instcondamb_coeficiente ic INNER JOIN intrumento_condamb i ON i.id = ic.idpatron "
                            + " where idmagnitud = " + magnitud + " and descripcion like '%REGLA GRADUADA%'";

                    sql7 = "SELECT cp.* from certificado_puntos_presion cp inner join certificados_previos op on op.id = cp.idcerprevio  where ingreso = " + ingreso + " and item = " + numero + " and revision=" + revision + " ORDER BY cp.id";

                    sql8 = "SELECT premax, premin, copremax, copremin, coprevar, rangohasta FROM certificados_previos where ingreso = " + ingreso + " and item = " + numero + " and revision = " + revision + " ORDER BY item DESC limit 1";

                    sql9 = "SELECT tipo, valor1, valor2, valor3, valor4, valor5, valor6, "
                            + " (SELECT MAX(utotal) from instcondamb_transductor1 it where it.idpatron = p.id) as maxutotal "
                            + " FROM instcondamb_medidaaltura a inner join intrumento_condamb p on a.idpatron = p.id "
                            + " where idmagnitud = 2 and descripcion = 'REGLA GRADUADA'";
                    break;
                case 5:
                    sql = "select CASE WHEN (SELECT COUNT(*) FROM remision_detalle WHERE remision_detalle.ingreso = " + ingreso + " )>0 THEN (SELECT public.tiempo(fechaing, now(), 1) FROM remision_detalle WHERE remision_detalle.ingreso = " + ingreso + ") ELSE now()::text END as tiempo, e.descripcion as equipo, ma.descripcion as marca, mo.descripcion as modelo, re.remision, "
                            + "      r.observacion, r.estado, r.serie, trim(to_char(r.ingreso,'0000000')) as ingreso, r.fotos, "
                            + "       desde, hasta, medida, m.id as idmagnitud,  to_char(rl.fecha, 'yyyy-MM-dd HH24:MI') as fecharec, ui.nombrecompleto as usuarioing, "
                            + "      case when coalesce(cd.direccion,'SEDE') = 'SEDE' OR trim(cd.direccion) = '' THEN cs.direccion || ', ' || ci.descripcion || ' (' || d.descripcion || ')' else cd.direccion end as direccion, "
                            + "      re.remision, ui.nombrecompleto as usuarioing, tiempo(rl.fecha, now(),1) as tiempo,  "
                            + "      case when cd.id is null then cc.email else (SELECT c2.email FROM clientes_contac c2 where c2.id= cd.idcontacto) end as email,  "
                            + "      cd.proxima, r.sitio, r.garantia, e.id as idequipo, coalesce(cd.metodo, 0) as metodo_cot, "
                            + "      ins.id as idinstrumento, divisionescala, resolucion, revision, errormaxpos, errormaxneg, to_char(re.fecha, 'yyyy-MM-dd') as fechaing, "
                            + "      cli.nombrecompleto as cliente, m.contador+1 as contador, coalesce((select valor from conversiones con where con.medidacon = i.medida and con.medida = '" + medida + "'),0) as medidacon, "
                            + "      coalesce((SELECT max(op.revision) FROM operprevia_presion op where op.ingreso =r.ingreso),0) as revision, "
                            + "      ase.idusu as asesor, cli.aprobar_cotizacion, cli.habitual, r.fechaaprocoti, ins.tipo "
                            + "  from remision_detalle r inner join equipo e on r.idequipo = e.id "
                            + "                          inner join remision re on re.id = r.idremision "
                            + "                          inner join magnitudes m on m.id = e.idmagnitud "
                            + "                          inner join modelos mo on mo.id = r.idmodelo  "
                            + "                          inner join marcas ma on ma.id = mo.idmarca "
                            + "                          inner join magnitud_intervalos i on i.id = r.idintervalo "
                            + "                          inner join ingreso_recibidolab rl on rl.ingreso = r.ingreso "
                            + "                          inner join clientes cli on cli.id = re.idcliente "
                            + "                          INNER JOIN clientes_sede cs on re.idsede = cs.id "
                            + "                          INNER JOIN clientes_contac cc on re.idcontacto = cc.id "
                            + "                          inner join ciudad ci on ci.id = cs.idciudad "
                            + "                          inner join departamento d on d.id = ci.iddepartamento "
                            + "                          inner join seguridad.rbac_usuario ui on ui.idusu = rl.idusuario "
                            + "                          inner join seguridad.rbac_usuario ase on ase.idusu = cli.asesor "
                            + "                          left join instrumento_longitud ins on ins.ingreso = r.ingreso "
                            + "                          left join cotizacion_detalle cd on cd.ingreso = r.ingreso  and cd.estado in ('Aprobado','Cerrado','Cotizado','POR REEMPLAZAR') "
                            + " where r.ingreso = " + ingreso + " ORDER BY cd.idcotizacion desc limit 1";

                    sql2 = "SELECT * from operprevia_longitud where ingreso = " + ingreso + " and revision = " + revision;

                    sql4 = "SELECT op.* from operprevia_longitud ol INNER JOIN operprevia_patron op on idoperacion = ol.id where ingreso = " + ingreso + " and revision = " + revision;

                    sql12 = "SELECT pt.id, desviacionlongitud, variacionlongitud, valornominal, marca, serie, pt.estado, pt.derivada, pt.denominadorderivada, pt.upatron "
                            + " FROM public.patron_transductor4 pt "
                            + " INNER JOIN operprevia_patron op ON op.idpatron = pt.idpatron "
                            + " INNER JOIN operprevia_longitud ol ON ol.id = op.idoperacion AND ol.id = (SELECT max(id) FROM operprevia_longitud ol2 WHERE ol2.ingreso = ol.ingreso) "
                            + " INNER JOIN patron p ON p.id = op.idpatron "
                            + " WHERE ingreso = " + ingreso + " ORDER BY 2";

                    sql13 = "SELECT * FROM error_longitud "
                            + " WHERE tipo = 'EMP PARALELISMO INTERIORES LOS PIE DE REY' ORDER BY 4 ASC";

                    sql14 = "select * from public.error_longitud where tipo like '%PIE DE REY' order by 1";

                    sql15 = "select * from error_longitud_abbe order by 1";

                    break;
            }

            sql3 = "SELECT cp.id, cp.ingreso, cp.idusuario, to_char(cp.fecha, 'yyyy-MM-dd') as fecha, cp.observacion, item,  "
                    + "            horaini, horafin, tiempotrans, acreditado,  proximacali, clase, rangohasta, "
                    + "            coalesce((select informe from informetecnico_dev id where id.ingreso = cp.ingreso and id.idplantilla = cp.item),0) as informetecnico, "
                    + "            tempmax, tempmin, tempvar, humemax, humemin, cp.revision, co0, co1, co2, co3, co4, co5, umetodo, incermetodo, "
                    + "            ri.idconcepto, ri.ajuste, ri.suministro, ri.conclusion, ri.id as idreporte, "
                    + "             tipo_aprobacion, case when tipoaprobacion is null then case when habitual = 'SI' OR aprobar_ajuste = 'NO' THEN 'No requiere aprobación' else 'EN ESPERA DE LA APROBACION' END else tipoaprobacion end as tipoaprobacion, "
                    + "             to_char(fecaprobacion,'yyyy/MM/dd HH24:MI') as fecaprobacion, coalesce(observacionapro,'') as observacionapro "
                    + " from certificados_previos cp INNER JOIN remision_detalle rd on rd.ingreso = cp.ingreso "
                    + "                                      inner join clientes c on c.id = rd.idcliente "
                    + "                                      left join reporte_ingreso ri on ri.ingreso = cp.ingreso and nroreporte = 0 "
                    + " where cp.ingreso = " + ingreso + " and cp.item = " + numero + " and cp.revision = " + revision + " order by ri.id desc limit 1";

            sql5 = "SELECT numero, nombrecompleto, coalesce(nombrecer,'') as nombrecer, coalesce(direccioncer,'') as direccioncer, to_char(fechaanula,'dd/MM/yyyy HH24:MI') as fecha, to_char(fechaexp,'dd/MM/yyyy HH24:MI') as fechaexp, observacionanula, idusuarioanula, revision, item, "
                    + "             premax, premin, copremax, copremin, coprevar "
                    + "        FROM certificados c INNER JOIN seguridad.rbac_usuario u on u.idusu = idusuarioanula "
                    + "          where ingreso = " + ingreso + " and item = " + numero + " and revision = " + revision + " ORDER BY revision DESC LIMIT 1";

            sql10 = "select cd.observacion,me.descripcion as metodo, punto, declaracion, cd.nombreca, "
                    + "                case when cd.direccion = 'SEDE' THEN cs.direccion || ', ' || ci.descripcion || ' (' ||d.descripcion ||')' else cd.direccion end as direccion, coalesce(proxima,'NA') as proxima,"
                    + "                max(entrega) as entrega, u.nombrecompleto as asesor, s.nombre as servicio "
                    + " from cotizacion c INNER JOIN cotizacion_detalle cd on c.id = cd.idcotizacion "
                    + "                   INNER JOIN Clientes cli  on cli.id = c.idcliente "
                    + "                   INNER JOIN clientes_sede cs on c.idsede = cs.id "
                    + "                   INNER JOIN clientes_contac cc on c.idcontacto = cc.id "
                    + "                   inner join metodo me on me.id = cd.metodo "
                    + "                   inner join ciudad ci on ci.id = cs.idciudad "
                    + "                   inner join departamento d on d.id = ci.iddepartamento "
                    + "                   inner join seguridad.rbac_usuario u on u.idusu = c.idusuaio "
                    + "                   inner join servicio s on s.id = cd.idservicio "
                    + " where idserviciodep = 0  and c.estado in ('Cerrado','Cotizado','POR REEMPLAZAR', 'Aprobado') and ingreso = " + ingreso
                    + " group by cd.observacion,me.descripcion, punto, declaracion, cd.nombreca, u.nombrecompleto, "
                    + " cd.direccion, cs.direccion, ci.descripcion, d.descripcion, proxima, s.nombre";
            return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->BuscarIngresoPlaCert") + "|" + Globales.ObtenerDatosJSon(sql2, this.getClass() + "-->BuscarIngresoPlaCert") + "|" + Globales.ObtenerDatosJSon(sql3, this.getClass() + "-->BuscarIngresoPlaCert") + "|"
                    + Globales.ObtenerDatosJSon(sql4, this.getClass() + "-->BuscarIngresoPlaCert") + "|" + Globales.ObtenerDatosJSon(sql5, this.getClass() + "-->BuscarIngresoPlaCert") + (sql6 != "" ? "|" + Globales.ObtenerDatosJSon(sql6, this.getClass() + "-->BuscarIngresoPlaCert") : "")
                    + (!sql7.equals("") ? "|" + Globales.ObtenerDatosJSon(sql7, this.getClass() + "-->BuscarIngresoPlaCert") : "") + (!sql8.equals("") ? "|" + Globales.ObtenerDatosJSon(sql8, this.getClass() + "-->BuscarIngresoPlaCert") : "") + (!sql9.equals("") ? "|" + Globales.ObtenerDatosJSon(sql9, this.getClass() + "-->BuscarIngresoPlaCert") : "") + "|" + Globales.ObtenerDatosJSon(sql10, this.getClass() + "-->BuscarIngresoPlaCert") + (!sql11.equals("") ? "|" + Globales.ObtenerDatosJSon(sql11, this.getClass() + "-->BuscarIngresoPlaCert") : "")
                    + (!sql12.equals("") ? "|" + Globales.ObtenerDatosJSon(sql12, this.getClass() + "-->BuscarIngresoPlaCert") : "") + (!sql13.equals("") ? "|" + Globales.ObtenerDatosJSon(sql13, this.getClass() + "-->BuscarIngresoPlaCert") : "") + (!sql14.equals("") ? "|" + Globales.ObtenerDatosJSon(sql14, this.getClass() + "-->BuscarIngresoPlaCert") : "")
                    + (!sql15.equals("") ? "|" + Globales.ObtenerDatosJSon(sql15, this.getClass() + "-->BuscarIngresoPlaCert") : "");
        } catch (Exception e) {

            return "1|" + e.getMessage();
        }
    }

    public String BuscarReportesLab(HttpServletRequest request) {
        int ingreso = Globales.Validarintnonull(request.getParameter("ingreso"));
        int plantilla = Globales.Validarintnonull(request.getParameter("plantilla"));

        sql = "SELECT rp.id, rp.idusuario, rp.idconcepto, rp.ajuste, rp.suministro, to_char(rp.fecha, 'dd/MM/yyyy HH24:MI') as fecha, "
                + "  rp.observacion as observaciones, rp.nroreporte, u.nombrecompleto as usuario, conclusion, "
                + "  coalesce(' - <i>Aprobado por: ' || rp.usuarioapro || ', ' || rp.cedula || ', ' || rp.cargo || ', ' || rp.tipoaprobacion || '</i>',' ') as aprobado "
                + "  from reporte_ingreso rp INNER JOIN seguridad.rbac_usuario u on u.idusu = rp.idusuario "
                + "                          inner join remision_detalle rd on rd.ingreso = rp.ingreso "
                + "  where rp.ingreso = " + ingreso + " and nroreporte <> 0";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->BuscarReportesLab");
    }

    public String BuscarIngresoAjuste(HttpServletRequest request) {
        int ingreso = Globales.Validarintnonull(request.getParameter("ingreso"));
        String busqueda = "where r.ingreso = " + ingreso + " and (idconcepto = 2 or idconcepto is null)";

        sql = "select e.descripcion as equipo, ma.descripcion as marca, mo.descripcion as modelo, "
                + "      r.observacion, r.estado, r.serie, r.ingreso, e.idmagnitud, coalesce(rl.id,0) as idrecibido, "
                + "      case when r.idintervalo = 0 then 'VER ESPECIFICACIONES' ELSE '(' || desde || ' a ' || hasta || ') ' || medida end AS intervalo, m.descripcion as magnitud, "
                + "  re.remision, rl.observacion as observaesp, r.fotos, tiempo(rl.fecha, now(),1) as tiempo, "
                + "  re.remision, to_char(rl.fecha, 'yyyy-MM-dd HH24:MI') as fechaing, ui.nombrecompleto as usuarioing, "
                + "  (select informe FROM informetecnico_dev i where i.ingreso = r.ingreso and i.estado <> 'Anulado') as informe, "
                + "  sitio, garantia, accesorio, r.sitio, r.garantia, c.aprobar_cotizacion, c.habitual, c.asesor, r.fechaaprocoti "
                + "  from remision_detalle r inner join equipo e on r.idequipo = e.id "
                + "                           inner join remision re on re.id = r.idremision "
                + "                           inner join clientes c on c.id = r.idcliente "
                + "                           inner join magnitudes m on m.id = e.idmagnitud "
                + "	                      inner join modelos mo on mo.id = r.idmodelo  "
                + "                           inner join marcas ma on ma.id = mo.idmarca "
                + "                           inner join magnitud_intervalos i on i.id = r.idintervalo "
                + "                           left join reporte_ingreso rp on rp.ingreso = r.ingreso "
                + "                           left join ingreso_recibidolab rl on rl.ingreso = r.ingreso "
                + "                           left join seguridad.rbac_usuario ui on ui.idusu = rp.idusuario " + busqueda;

        String sql2 = "SELECT p.id, p.descripcion "
                + " from plantillas p inner join  ingreso_plantilla ip on ip.idplantilla = p.id "
                + "  where ip.ingreso = " + ingreso + " order by 2";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->BuscarIngresoAjuste") + "|" + Globales.ObtenerCombo(sql2, 1, 0, 0);
    }

    public String BuscarIngresoReport(HttpServletRequest request) {
        int ingreso = Globales.Validarintnonull(request.getParameter("ingreso"));
        String busqueda = "where r.ingreso = " + ingreso;

        sql = "select e.descripcion as equipo, ma.descripcion as marca, mo.descripcion as modelo, "
                + "      r.observacion, r.estado, r.serie, r.ingreso, e.idmagnitud, "
                + "      case when r.idintervalo = 0 then 'VER ESPECIFICACIONES' ELSE '(' || desde || ' a ' || hasta || ') ' || medida end AS intervalo, m.descripcion as magnitud, "
                + "  re.remision, rl.observacion as observaesp, r.fotos, tiempo(rl.fecha, now(),1) as tiempo, "
                + "  re.remision, to_char(rl.fecha, 'yyyy-MM-dd HH24:MI') as fechaing, ui.nombrecompleto as usuarioing, "
                + "  (select informe FROM informetecnico_dev i where i.ingreso = r.ingreso and i.estado <> 'Anulado') as informe, "
                + "  sitio, garantia, accesorio, r.garantia, convenio, c.aprobar_cotizacion, c.habitual, c.asesor, r.fechaaprocoti "
                + "  from remision_detalle r inner join equipo e on r.idequipo = e.id "
                + "                           inner join remision re on re.id = r.idremision "
                + "                           inner join clientes c on c.id = r.idcliente "
                + "                           inner join magnitudes m on m.id = e.idmagnitud "
                + "                           inner join modelos mo on mo.id = r.idmodelo "
                + "                           inner join marcas ma on ma.id = mo.idmarca "
                + "                           inner join magnitud_intervalos i on i.id = r.idintervalo "
                + "                           inner join ingreso_recibidolab rl on rl.ingreso = r.ingreso "
                + "                           inner join seguridad.rbac_usuario ui on ui.idusu = rl.idusuario " + busqueda;

        String sql2 = "select cd.observacion,me.descripcion as metodo, punto, declaracion, cd.nombreca, "
                + "          case when cd.direccion = 'SEDE' THEN cs.direccion || ', ' || ci.descripcion || ' (' ||d.descripcion ||')' else cd.direccion end as direccion, coalesce(proxima,'NA') as proxima, "
                + "          max(entrega) as entrega, u.nombrecompleto as asesor, s.nombre as servicio, cotizacion, c.estado "
                + "  from cotizacion c INNER JOIN cotizacion_detalle cd on c.id = cd.idcotizacion "
                + "                     INNER JOIN Clientes cli  on cli.id = c.idcliente "
                + "                     INNER JOIN clientes_sede cs on c.idsede = cs.id "
                + "                     INNER JOIN clientes_contac cc on c.idcontacto = cc.id "
                + "                     inner join metodo me on me.id = cd.metodo "
                + "                     inner join ciudad ci on ci.id = cs.idciudad "
                + "                     inner join departamento d on d.id = ci.iddepartamento "
                + "                     inner join seguridad.rbac_usuario u on u.idusu = c.idusuario "
                + "                     inner join servicio s on s.id = cd.idservicio "
                + "  where idserviciodep = 0  and c.estado in ('Cerrado','POR REEMPLAZAR','Aprobado') and ingreso = " + ingreso
                + "  group by cd.observacion,me.descripcion, punto, declaracion, cd.nombreca, u.nombrecompleto, cotizacion, c.estado, "
                + "          cd.direccion, cs.direccion, ci.descripcion, d.descripcion, proxima, s.nombre order by c.cotizacion DESC";
        String sql3 = "SELECT p.id, p.descripcion "
                + "      from plantillas p inner join  ingreso_plantilla ip on ip.idplantilla = p.id "
                + "  where ip.ingreso = " + ingreso + " order by 2";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->BuscarIngresoReport") + "|" + Globales.ObtenerDatosJSon(sql2, this.getClass() + "-->BuscarIngresoReport") + "|" + Globales.ObtenerCombo(sql3, 1, 0, 0);
    }

    public String BuscarEstadoIngreso(HttpServletRequest request) {
        int ingreso = Globales.Validarintnonull(request.getParameter("ingreso"));
        int plantilla = Globales.Validarintnonull(request.getParameter("plantilla"));

        if (plantilla == 0) {
            sql = "select e.descripcion as equipo, ma.descripcion as marca, mo.descripcion as modelo, "
                    + "  r.observacion, r.estado, r.serie, r.ingreso, r.accesorio, "
                    + "  case when r.idintervalo = 0 then 'VER ESPECIFICACIONES' ELSE '(' || desde || ' a ' || hasta || ') ' || medida end AS intervalo, m.descripcion as magnitud, "
                    + " re.remision, r.fotos, c.nombrecompleto as cliente, '--' as plantilla, "
                    + " re.remision, to_char(fechaing, 'dd/MM/yyyy HH24:MI') as fechaing, ui.nombrecompleto as usuarioing "
                    + " from remision_detalle r inner join equipo e on r.idequipo = e.id "
                    + "                       inner join remision re on re.id = r.idremision "
                    + "				                     inner join magnitudes m on m.id = e.idmagnitud "
                    + "				                     inner join modelos mo on mo.id = r.idmodelo "
                    + "				                     inner join marcas ma on ma.id = mo.idmarca "
                    + "                       inner join clientes c on c.id = r.idcliente "
                    + "                       inner join magnitud_intervalos i on i.id = r.idintervalo "
                    + "                       inner join seguridad.rbac_usuario ui on ui.idusu = re.idusuario "
                    + " where r.ingreso = " + ingreso;
        } else {
            sql = "select e.descripcion as equipo, ma.descripcion as marca, mo.descripcion as modelo, "
                    + "  r.observacion, r.estado, r.serie, r.ingreso, r.accesorio, "
                    + "  case when r.idintervalo = 0 then 'VER ESPECIFICACIONES' ELSE '(' || desde || ' a ' || hasta || ') ' || medida end AS intervalo, m.descripcion as magnitud, "
                    + " re.remision, r.fotos, c.nombrecompleto as cliente, p.descripcion as plantilla, "
                    + " re.remision, to_char(fechaing, 'dd/MM/yyyy HH24:MI') as fechaing, ui.nombrecompleto as usuarioing "
                    + " from remision_detalle r inner join equipo e on r.idequipo = e.id "
                    + "                         inner join remision re on re.id = r.idremision "
                    + "				                     inner join magnitudes m on m.id = e.idmagnitud "
                    + "				                     inner join modelos mo on mo.id = r.idmodelo  "
                    + "				                     inner join marcas ma on ma.id = mo.idmarca "
                    + "                       inner join clientes c on c.id = r.idcliente "
                    + "                       inner join magnitud_intervalos i on i.id = r.idintervalo "
                    + "                       inner join seguridad.rbac_usuario ui on ui.idusu = re.idusuario "
                    + "                       left join ingreso_plantilla ip on ip.ingreso = r.ingreso and ip.idplantilla = " + plantilla
                    + "                       left join plantillas p on p.id = ip.idplantilla "
                    + " where r.ingreso = " + ingreso;
        }

        String sql2 = "SELECT id, ingreso, descripcion, to_char(fecha,'dd/MM/yyyy HH24:MI') as fecha, nombrecompleto as usuario, tiempo "
                + "  FROM public.ingreso_estados e inner join seguridad.rbac_usuario u on e.idusuario = u.idusu "
                + "  WHERE ingreso = " + ingreso + " order by id desc";

        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->BuscarEstadoIngreso") + "|" + Globales.ObtenerDatosJSon(sql2, this.getClass() + "-->BuscarEstadoIngreso");
    }

    public String BuscarIngresoCertificado(HttpServletRequest request) {
        int ingreso = Globales.Validarintnonull(request.getParameter("ingreso"));
        int tipo = Globales.Validarintnonull(request.getParameter("tipo"));

        if (tipo == 1) {
            sql = "select coalesce(recibidolab,-1) "
                    + "  from remision_detalle "
                    + "  where ingreso = " + ingreso;
        } else {
            sql = "select coalesce(recibidocome,-1) "
                    + "  from remision_detalle "
                    + "  where ingreso = " + ingreso;
        }

        int recibido = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));

        if (recibido <= 0) {
            return "1||" + recibido;
        }

        if (tipo == 1) {
            sql = "select e.descripcion as equipo, ma.descripcion as marca, mo.descripcion as modelo, re.remision, "
                    + "  r.observacion, r.estado, r.serie, r.ingreso, r.fotos, "
                    + " case when r.idintervalo = 0 then 'VER ESPECIFICACIONES' ELSE '(' || desde || ' a ' || hasta || ') ' || medida end AS intervalo, m.descripcion as magnitud, "
                    + " r.recibidolab, r.cotizado, "
                    + " cd.observacion as observacot, punto, me.descripcion as metodo, s.nombre as servicio,nombreca, case when cd.direccion = 'SEDE' THEN cs.direccion else cd.direccion end as direccion, "
                    + " re.remision, (select to_char(ri.fecha, 'yyyy-MM-dd HH24:MI') from reporte_ingreso ri where ri.ingreso = r.ingreso order by ri.id limit 1) as fechaing, u.nombrecompleto as usuarioing, "
                    + " tiempo((select ri.fecha from reporte_ingreso ri where ri.ingreso = r.ingreso order by ri.id limit 1), now(),1) as tiempo, "
                    + " r.sitio, r.garantia, r.accesorio, "
                    + " cs2.direccion || ', ' || ci2.descripcion || ' (' ||d2.descripcion ||'), ' || pa2.descripcion as direccion2, "
                    + " cli.nombrecompleto as cliente "
                    + " from remision_detalle r inner join equipo e on r.idequipo = e.id "
                    + "                       inner join remision re on re.id = r.idremision "
                    + "                       left JOIN clientes cli on cli.id = re.idcliente "
                    + "                       inner join magnitudes m on m.id = e.idmagnitud "
                    + "				                     inner join modelos mo on mo.id = r.idmodelo  "
                    + "				                     inner join marcas ma on ma.id = mo.idmarca "
                    + "                       inner join magnitud_intervalos i on i.id = r.idintervalo  "
                    + "                       inner join seguridad.rbac_usuario u on u.idusu = r.idusuario "
                    + "                       inner JOIN clientes_sede cs2 on re.idsede = cs2.id "
                    + "		                             inner join ciudad ci2 on ci2.id = cs2.idciudad "
                    + "		                             inner join departamento d2 on d2.id = ci2.iddepartamento "
                    + "		                             inner join pais pa2 on pa2.id = d2.idpais "
                    + "                       left join cotizacion_detalle cd on cd.ingreso = r.ingreso "
                    + "                       left join cotizacion c on c.id = cd.idcotizacion and c.estado in ('POR REEMPLAZAR','Cerrado','Aprobado') "
                    + "                       left JOIN clientes_sede cs on c.idsede = cs.id "
                    + "		                             left join ciudad ci on ci.id = cs.idciudad "
                    + "		                             left join departamento d on d.id = ci.iddepartamento "
                    + "		                             left join pais pa on pa.id = d.idpais "
                    + "                       left join servicio s on s.id = cd.idservicio "
                    + "                       left join metodo me on me.id = cd.metodo "
                    + " where r.ingreso = " + ingreso + " ORDER BY cd.id desc LIMIT 1";
        } else {
            sql = "select e.descripcion as equipo, ma.descripcion as marca, mo.descripcion as modelo, re.remision, "
                    + " r.observacion, r.estado, r.serie, r.ingreso, rl.observacion as observaesp, "
                    + " case when r.idintervalo = 0 then 'VER ESPECIFICACIONES' ELSE '(' || desde || ' a ' || hasta || ') ' || medida end AS intervalo, m.descripcion as magnitud, "
                    + " r.recibidolab, r.cotizado, "
                    + " cd.observacion as observacot, punto, me.descripcion as metodo, s.nombre as servicio,nombreca, case when cd.direccion = 'SEDE' THEN cs.direccion else cd.direccion end as direccion, "
                    + " re.remision, to_char(r.fechaing, 'yyyy-MM-dd HH24:MI') as fechaing, ui.nombrecompleto as usuarioing, tiempo(r.fechaing, now(),1) as tiempo "
                    + " from remision_detalle r inner join equipo e on r.idequipo = e.id "
                    + "                         inner join remision re on re.id = r.idremision "
                    + "                         left JOIN clientes cli on cli.id = re.idcliente "
                    + "                         inner join magnitudes m on m.id = e.idmagnitud "
                    + " 	                inner join modelos mo on mo.id = r.idmodelo "
                    + "	                        inner join marcas ma on ma.id = mo.idmarca "
                    + "	                        left join seguridad.rbac_usuario ui on ui.idusu = r.idusuario "
                    + "                         inner join magnitud_intervalos i on i.id = r.idintervalo "
                    + "                         inner join ingreso_recibidocome rl on rl.ingreso = r.ingreso "
                    + "                         left join cotizacion_detalle cd on cd.ingreso = r.ingreso "
                    + "                       left join cotizacion c on c.id = cd.idcotizacion "
                    + "                       left JOIN clientes_sede cs on c.idsede = cs.id "
                    + "		                             left join ciudad ci on ci.id = cs.idciudad "
                    + "		                             left join departamento d on d.id = ci.iddepartamento "
                    + "		                             left join pais pa on pa.id = d.idpais "
                    + "                       left join servicio s on s.id = cd.idservicio "
                    + "                       left join metodo me on me.id = cd.metodo "
                    + " where r.ingreso = " + ingreso + " LIMIT 1";
        }

        String sql2 = "SELECT p.id, p.descripcion "
                + "      from plantillas p inner join ingreso_plantilla ip on ip.idplantilla = p.id "
                + " where ip.calibracion = 1 and reportado = 1 and informetecnico = 0 and noautorizado = 0 and ip.ingreso = " + ingreso;

        String sql3 = "SELECT DISTINCT nombrecer from certificados where ingreso = " + ingreso;

        String sql4 = "SELECT DISTINCT direccioncer from certificados where ingreso = " + ingreso;

        return "0||" + Globales.ObtenerDatosJSon(sql, this.getClass() + "-->BuscarIngresoCertificado") + "||" + Globales.ObtenerCombo(sql2, 0, 0, 0) + "||" + Globales.ObtenerDatosJSon(sql3, this.getClass() + "-->BuscarIngresoCertificado") + "||" + Globales.ObtenerDatosJSon(sql4, this.getClass() + "-->BuscarIngresoCertificado");

    }

    public String BuscarIngresoRecbLab(HttpServletRequest request) {
        int ingreso = Globales.Validarintnonull(request.getParameter("ingreso"));

        sql = "select r.idremision, e.descripcion as equipo, ma.descripcion as marca, mo.descripcion as modelo, m.descripcion as magnitud, "
                + "      r.observacion, r.estado, r.serie, r.ingreso, u.nombrecompleto as usuario, re.remision, r.fotos, "
                + "      case when r.idintervalo = 0 then 'VER ESPECIFICACIONES' ELSE '(' || desde || ' a ' || hasta || ') ' || medida end AS intervalo, "
                + "   rl.id, r.ingreso, to_char(rl.fecha, 'dd/MM/yyyy HH24:MI') as fecha, "
                + "  rl.observacion as observaciones, rl.idusuarioent, sitio, garantia, accesorio, "
                + "  re.remision, to_char(fechaing, 'yyyy-MM-dd HH24:MI') as fechaing, ui.nombrecompleto as usuarioing, tiempo(r.fechaing, now(),1) as tiempo "
                + "  from remision_detalle r  inner join equipo e on r.idequipo = e.id "
                + "                           inner join remision re on re.id = r.idremision "
                + "				                     inner join magnitudes m on m.id = e.idmagnitud "
                + "				                     inner join modelos mo on mo.id = r.idmodelo  "
                + "				                     inner join marcas ma on ma.id = mo.idmarca "
                + "                           inner join magnitud_intervalos i on i.id = r.idintervalo "
                + "                           inner join seguridad.rbac_usuario ui on ui.idusu = re.idusuario "
                + "                           left join ingreso_recibidolab rl on rl.ingreso = r.ingreso "
                + "                           left join seguridad.rbac_usuario u on u.idusu = rl.idusuario "
                + "  where r.ingreso = " + ingreso;
        String sql2 = "SELECT p.id, p.descripcion, "
                + "      coalesce((select ip.id from ingreso_plantilla ip where p.id = ip.idplantilla and ip.ingreso = rd.ingreso),0) as seleccionado "
                + "  from plantillas p inner join  plantilla_equipo pe on pe.idplantilla = p.id "
                + "                    inner join remision_detalle rd on rd.idequipo = pe.idequipo "
                + "  where rd.ingreso= " + ingreso + " order by 2";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->BuscarIngresoRecbLab") + "||" + Globales.ObtenerDatosJSon(sql2, this.getClass() + "-->BuscarIngresoRecbLab");
    }

    public String BuscarIngresoRecbCome(HttpServletRequest request) {
        int ingreso = Globales.Validarintnonull(request.getParameter("ingreso"));

        sql = "select e.descripcion as equipo, ma.descripcion as marca, mo.descripcion as modelo, m.descripcion as magnitud, "
                + "      r.observacion, r.estado, r.serie, r.ingreso, u.nombrecompleto as usuario, re.remision, r.fotos, "
                + "      case when r.idintervalo = 0 then 'VER ESPECIFICACIONES' ELSE '(' || desde || ' a ' || hasta || ') ' || medida end AS intervalo, "
                + "   rl.id, rl.ingreso, to_char(rl.fecha, 'dd/MM/yyyy HH24:MI') as fecha, ir.observacion as Observaesp, "
                + " rl.observacion as observaciones, rl.idusuarioent, sitio, garantia, accesorio, "
                + "  re.remision, to_char(fechaing, 'yyyy-MM-dd HH24:MI') as fechaing, ui.nombrecompleto as usuarioing, tiempo(r.fechaing, now(),1) as tiempo "
                + "  from remision_detalle r  inner join equipo e on r.idequipo = e.id "
                + "                           inner join remision re on re.id = r.idremision "
                + "				                     inner join magnitudes m on m.id = e.idmagnitud "
                + "				                     inner join modelos mo on mo.id = r.idmodelo  "
                + "				                     inner join marcas ma on ma.id = mo.idmarca "
                + "                           inner join magnitud_intervalos i on i.id = r.idintervalo "
                + "                           inner join seguridad.rbac_usuario ui on ui.idusu = re.idusuario "
                + "                           left join ingreso_recibidocome rl on rl.ingreso = r.ingreso "
                + "                           left join ingreso_recibidolab ir on ir.ingreso = r.ingreso "
                + "                            left join seguridad.rbac_usuario u on u.idusu = rl.idusuario "
                + "  where r.ingreso = " + ingreso;
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->BuscarIngresoRecbCome");
    }

    public String BuscarIngresoCome(HttpServletRequest request) {
        int ingreso = Globales.Validarintnonull(request.getParameter("ingreso"));
        sql = "select e.descripcion as equipo, ma.descripcion as marca, mo.descripcion as modelo, m.descripcion as magnitud, "
                + "      r.observacion, r.estado, r.serie, r.ingreso, u.nombrecompleto as usuario, re.remision, r.fotos, "
                + "      case when r.idintervalo = 0 then 'VER ESPECIFICACIONES' ELSE '(' || desde || ' a ' || hasta || ') ' || medida end AS intervalo, "
                + "   rl.id, rl.ingreso, to_char(rl.fecha, 'dd/MM/yyyy HH24:MI') as fecha, "
                + "  rl.observacion as observaciones, rl.idusuarioent, "
                + "  re.remision, to_char(fechaing, 'yyyy-MM-dd HH24:MI') as fechaing, ui.nombrecompleto as usuarioing, tiempo(r.fechaing, now(),1) as tiempo "
                + "  from remision_detalle r  inner join equipo e on r.idequipo = e.id "
                + "                           inner join remision re on re.id = r.idremision "
                + "				                     inner join magnitudes m on m.id = e.idmagnitud "
                + "				                     inner join modelos mo on mo.id = r.idmodelo  "
                + "				                     inner join marcas ma on ma.id = mo.idmarca "
                + "                           inner join magnitud_intervalos i on i.id = r.idintervalo "
                + "                           inner join seguridad.rbac_usuario ui on ui.idusu = re.idusuario "
                + "                           left join ingreso_recibidocome rl on rl.ingreso = r.ingreso "
                + "                           left join seguridad.rbac_usuario u on u.idusu = rl.idusuario "
                + "  where r.ingreso = " + ingreso;
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->BuscarIngresoCome");
    }

    public String BuscarIngresoRecbIng(HttpServletRequest request) {

        int ingreso = Globales.Validarintnonull(request.getParameter("ingreso"));

        sql = "select cotizado, recibidolab, recibidoing, coalesce(reportado,0) as reportado, coalesce(certificado,0) as certificado, recibidocome, sitio, envtercero, coalesce(informetecnico,0) as informetecnico,  "
                + "coalesce(noautorizado,0) as noautorizado, convenio, informetercerizado, certificados_externos, "
                + " stiker as ticket, coalesce(p.descripcion,'') as plantilla, "
                + "  coalesce((select count(c.id) from certificados c where c.ingreso = rd.ingreso and c.estado in ('Registrado','Revisado') and idusuarioanula = 0),0) as noaprobados "
                + "  from remision_detalle rd left join ingreso_plantilla ip on ip.ingreso = rd.ingreso "
                + "                           left join plantillas p on p.id = ip.idplantilla "
                + "  where rd.ingreso = " + ingreso;

        String sql2 = "select e.descripcion as equipo, ma.descripcion as marca, mo.descripcion as modelo, m.descripcion as magnitud, "
                + "  r.observacion, r.estado, r.serie, r.ingreso, u.nombrecompleto as usuario, re.remision, r.fotos, "
                + "  case when r.idintervalo = 0 then 'VER ESPECIFICACIONES' ELSE '(' || desde || ' a ' || hasta || ') ' || medida end AS intervalo, "
                + "  ring.id, r.ingreso, to_char(ring.fecha, 'dd/MM/yyyy HH24:MI') as fecha, "
                + " rl.observacion as observaespeci, ring.idusuarioent, (select y || x from ingreso_ubicacion u where u.ingreso = r.ingreso) as ubicacion, "
                + " ring.observacion as observacioning, uri.nombrecompleto as usuariorecing,  to_char(case when ri.fecha is null then r.fechaing else ri.fecha  end, 'yyyy-MM-dd HH24:MI') as fecharecing, "
                + " re.remision, to_char(ri.fecha, 'dd/MM/yyyy HH24:MI') as fechaing, tiempo(case when rl.fecha is null then r.fechaing else rl.fecha end, now(),1) as tiempo "
                + " from remision_detalle r  inner join equipo e on r.idequipo = e.id "
                + "                       inner join remision re on re.id = r.idremision "
                + "			 inner join magnitudes m on m.id = e.idmagnitud "
                + "			 inner join modelos mo on mo.id = r.idmodelo   "
                + "			 inner join marcas ma on ma.id = mo.idmarca "
                + "                       inner join magnitud_intervalos i on i.id = r.idintervalo "
                + "                       left join ingreso_recibidolab rl on rl.ingreso = r.ingreso "
                + "                       left join reporte_ingreso ri  on ri.ingreso = r.ingreso "
                + "                       left join seguridad.rbac_usuario u on u.idusu = ri.idusuario "
                + "                       left join ingreso_recibidoing ring  on ring.ingreso = r.ingreso "
                + "                       left join seguridad.rbac_usuario uri on uri.idusu = ring.idusuario "
                + " where r.ingreso = " + ingreso;
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->BuscarIngresoRecbIng") + "|" + Globales.ObtenerDatosJSon(sql2, this.getClass() + "-->BuscarIngresoRecbIng");
    }

    public String TablaRecibidoLab(HttpServletRequest request) {
        String fecini = request.getParameter("fecini");
        String fecfin = request.getParameter("fecfin");
        int especialista = Globales.Validarintnonull(request.getParameter("especialista"));

        sql = "select row_number() OVER(order by r.ingreso) as fila, "
                + "  '<b>' || m.descripcion || '</b><br>' || e.descripcion || '<br><b>' || ma.descripcion || '</b><br>' || mo.descripcion || '<br> ' || case when r.idintervalo = 0 then 'VER ESPECIFICACIONES' ELSE '(' || desde || ' a ' || hasta || ') ' || medida end || '<br>' || r.serie as equipo, "
                + "  r.observacion, r.estado, r.ingreso, '<b>' || ur.nombrecompleto || '</b><br>' || u.nombrecompleto as usuario, "
                + "  rl.id, r.ingreso as ingreso, to_char(rl.fecha, 'yyyy/MM/dd HH24:MI') as fecha, "
                + "  rl.observacion as observaciones "
                + "  from remision_detalle r  inner join equipo e on r.idequipo = e.id "
                + "	                     inner join magnitudes m on m.id = e.idmagnitud "
                + "	                     inner join modelos mo on mo.id = r.idmodelo  "
                + "	                     inner join marcas ma on ma.id = mo.idmarca "
                + "	                    inner join ingreso_recibidolab rl on rl.ingreso = r.ingreso "
                + "	                    inner join seguridad.rbac_usuario u on u.idusu = rl.idusuario "
                + "	                    inner join seguridad.rbac_usuario ur on ur.idusu = rl.idusuarioent "
                + "	                    inner join magnitud_intervalos i on i.id = r.idintervalo "
                + "  WHERE rl.idusuario = " + especialista + " and  to_char(rl.fecha, 'yyyy-MM-dd') >= '" + fecini + "' AND to_char(rl.fecha, 'yyyy-MM-dd') <= '" + fecfin + "' "
                + "  ORDER BY rl.id DESC";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->TablaRecibidoLab");
    }

    public String TablaRecibidoCome(HttpServletRequest request) {
        String fecini = request.getParameter("fecini");
        String fecfin = request.getParameter("fecfin");
        int especialista = Globales.Validarintnonull(request.getParameter("especialista"));

        sql = "select row_number() OVER(order by r.ingreso) as fila, "
                + "  '<b>' || m.descripcion || '</b><br>' || e.descripcion || '<br><b>' || ma.descripcion || '</b><br>' || mo.descripcion || '<br> ' || case when r.idintervalo = 0 then 'VER ESPECIFICACIONES' ELSE '(' || desde || ' a ' || hasta || ') ' || medida end || '<br>' || r.serie as equipo, "
                + "  r.observacion, r.estado, r.ingreso, '<b>' || ur.nombrecompleto || '</b><br>' || u.nombrecompleto as usuario, "
                + "  rl.id, r.ingreso, to_char(rl.fecha, 'yyyy/MM/dd HH24:MI') as fecha, "
                + "  rl.observacion as observaciones, r.reportado, r.calibracion "
                + "  from remision_detalle r  inner join equipo e on r.idequipo = e.id "
                + "	                     inner join magnitudes m on m.id = e.idmagnitud "
                + "	                     inner join modelos mo on mo.id = r.idmodelo "
                + "	                     inner join marcas ma on ma.id = mo.idmarca "
                + "	                    inner join ingreso_recibidocome rl on rl.ingreso = r.ingreso "
                + "	                    inner join seguridad.rbac_usuario u on u.idusu = rl.idusuario "
                + "	                    inner join seguridad.rbac_usuario ur on ur.idusu = rl.idusuarioent "
                + "	                    inner join magnitud_intervalos i on i.id = r.idintervalo "
                + "  WHERE rl.idusuario = " + especialista + " and  to_char(rl.fecha, 'yyyy-MM-dd') >= '" + fecini + "' AND to_char(rl.fecha, 'yyyy-MM-dd') <= '" + fecfin + "' "
                + "  ORDER BY rl.id DESC";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->TablaRecibidoCome");
    }

    public String TablaRecibidoIng(HttpServletRequest request) {
        String fecini = request.getParameter("fecini");
        String fecfin = request.getParameter("fecfin");
        int especialista = Globales.Validarintnonull(request.getParameter("especialista"));

        sql = "select row_number() OVER(order by r.ingreso) as fila, "
                + "  '<b>' || m.descripcion || '</b><br>' || e.descripcion || '<br><b>' || ma.descripcion || '</b><br>' || mo.descripcion || '<br> ' || case when r.idintervalo = 0 then 'VER ESPECIFICACIONES' ELSE '(' || desde || ' a ' || hasta || ') ' || medida end || '<br>' || r.serie as equipo, "
                + "  r.observacion, r.estado, r.ingreso, '<b>' || ur.nombrecompleto || '</b><br>' || u.nombrecompleto as usuario, "
                + "  rl.id, r.ingreso, to_char(rl.fecha, 'yyyy/MM/dd HH24:MI') as fecha, (select y || x from ingreso_ubicacion u where u.ingreso = r.ingreso) as ubicacion, "
                + "  rl.observacion as observaciones "
                + "  from remision_detalle r  inner join equipo e on r.idequipo = e.id "
                + "	                     inner join magnitudes m on m.id = e.idmagnitud "
                + "	                     inner join modelos mo on mo.id = r.idmodelo "
                + "	                     inner join marcas ma on ma.id = mo.idmarca "
                + "	                    inner join ingreso_recibidoing rl on rl.ingreso = r.ingreso "
                + "	                    inner join seguridad.rbac_usuario u on u.idusu = rl.idusuario "
                + "	                    inner join seguridad.rbac_usuario ur on ur.idusu = rl.idusuarioent "
                + "	                    inner join magnitud_intervalos i on i.id = r.idintervalo "
                + "  WHERE rl.idusuario = " + especialista + " and  to_char(rl.fecha, 'yyyy-MM-dd') >= '" + fecini + "' AND to_char(rl.fecha, 'yyyy-MM-dd') <= '" + fecfin + "' "
                + "  ORDER BY rl.id DESC";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->TablaRecibidoIng");
    }

    public String TablaCertificadoIngreso(HttpServletRequest request) {
        int tipo = Globales.Validarintnonull(request.getParameter("tipo"));
        String sql2 = "";
        if (tipo == 1) {
            sql = "select '<b>' || row_number() OVER(order by r.ingreso) || '</b><br>' || case when r.fotos > 0 then '<img src=''" + url_archivo + "Adjunto/imagenes/ingresos/' || r.ingreso || '/1.jpg'' onclick=''LlamarFotoDet(' || r.ingreso || ',' || r.fotos || ')'' width=''80px''/><br>' else '' end  as fila,  "
                    + "          r.ingreso,  "
                    + "      '<b>' || m.descripcion || '</b><br>' || e.descripcion || '<br><b>' || ma.descripcion || '</b><br>' || mo.descripcion || '<br> ' || case when r.idintervalo = 0 then 'VER ESPECIFICACIONES' ELSE '(' || desde || ' a ' || hasta || ') ' || medida end || '<br>' || r.serie as equipo, "
                    + "          to_char(ri.fecha, 'yyyy/MM/dd HH24:MI') as fecha, "
                    + "      case when p.excel > 0 then '<a title=''Descargar Excel'' href=' || chr(34) || 'javascript:DescargarExcel(' || p.id || ',''' || p.tipoarchivo || ''')' ||  chr(34) || '>' || p.descripcion || '</a>' else p.descripcion end as plantilla, "
                    + "      tiempo(ri.fecha, now(),1) as tiempo "
                    + "  from remision_detalle r inner join equipo e on r.idequipo = e.id "
                    + "				                        inner join magnitudes m on m.id = e.idmagnitud "
                    + "				                        inner join modelos mo on mo.id = r.idmodelo  "
                    + "				                        inner join marcas ma on ma.id = mo.idmarca "
                    + "                          inner join ingreso_recibidolab ir on ir.ingreso = r.ingreso "
                    + "                          inner join reporte_ingreso ri on ri.ingreso = r.ingreso "
                    + "                          inner join magnitud_intervalos i on i.id = r.idintervalo "
                    + "                          inner join ingreso_plantilla ip on ip.ingreso = r.ingreso and ri.plantilla = ip.idplantilla "
                    + "                          inner join plantillas p on p.id = ip.idplantilla "
                    + "  WHERE ip.reportado = 1 and ip.calibracion = 1 and ip.certificado = 0 and r.idcliente <> 11 and ip.noautorizado = 0 and salida = 0 and ip.informetecnico = 0 and informetercerizado = '0' and m.id in (" + usumagnitud + ") "
                    + "  ORDER BY ingreso";
        } else {
            sql = "select '<b>' || row_number() OVER(order by r.ingreso) || '</b><br>' || case when r.fotos > 0 then '<img src=''" + url_archivo + "Adjunto/imagenes/ingresos/' || r.ingreso || '/1.jpg'' onclick=''LlamarFotoDet(' || r.ingreso || ',' || r.fotos || ')'' width=''80px''/><br>' else '' end  as fila, "
                    + "        r.ingreso, "
                    + "      '<b>' || m.descripcion || '</b><br>' || e.descripcion || '<br><b>' || ma.descripcion || '</b><br>' || mo.descripcion || '<br> ' || case when r.idintervalo = 0 then 'VER ESPECIFICACIONES' ELSE '(' || desde || ' a ' || hasta || ') ' || medida end || '<br>' || r.serie as equipo, "
                    + "          to_char(ir.fecha, 'yyyy/MM/dd HH24:MI') as fecha, Tercerizado' as plantilla, '--' as ot, tiempo(ir.fecha, now(),1) as tiempo "
                    + "  from remision_detalle r inner join equipo e on r.idequipo = e.id "
                    + "				                        inner join magnitudes m on m.id = e.idmagnitud "
                    + "                          inner join magnitud_intervalos i on i.id = r.idintervalo "
                    + "				                        inner join modelos mo on mo.id = r.idmodelo "
                    + "				                        inner join marcas ma on ma.id = mo.idmarca "
                    + "                          inner join ingreso_recibidocome ir on ir.ingreso = r.ingreso "
                    + "  WHERE  recibidoterc = 1 and certificados_externos = 0 and informetercerizado = '0' and m.id in (" + usumagnitud + ") "
                    + "  ORDER BY ingreso";
            sql2 = "<option value='1'>Certificado/Informe Nro 1</option> "
                    + "   <option value='2'>Certificado/Informe Nro 2</option> "
                    + "   <option value='3'>Certificado/Informe Nro 3</option> "
                    + "   <option value='4'>Certificado/Informe Nro 4</option> "
                    + "   <option value='5'>Certificado/Informe Nro 5</option> "
                    + "   <option value='6'>Certificado/Informe Nro 6</option> "
                    + "   <option value='7'>Certificado/Informe Nro 7</option> "
                    + "   <option value='8'>Certificado/Informe Nro 8</option> "
                    + "   <option value='9'>Certificado/Informe Nro 9</option> "
                    + "   <option value='10'>Certificado/Informe Nro 10</option> "
                    + "   <option value='11'>Certificado/Informe Nro 11</option> "
                    + "   <option value='12'>Certificado/Informe Nro 12</option>";
        }

        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->TablaCertificadoIngreso") + "||" + sql2;
    }

    public String TablaReportarIngreso(HttpServletRequest request) {
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        int tipobusqueda = Globales.Validarintnonull(request.getParameter("tipobusqueda"));
        int magnitud = Globales.Validarintnonull(request.getParameter("magnitud"));
        String Busqueda = "";
        if (cliente > 0) {
            Busqueda = " and r.idcliente = " + cliente;
        }

        if (tipobusqueda == 1) {
            sql = "select '<b>' || row_number() OVER(order by r.ingreso) || '</b><br>' || case when r.fotos > 0 then '<img src=''" + url_archivo + "Adjunto/imagenes/ingresos/' || r.ingreso || '/1.jpg'' onclick=''LlamarFotoDet(' || r.ingreso || ',' || r.fotos || ')'' width=''80px''/><br>' else '' end  as fila ,  "
                    + " '<b>' || r.ingreso || "
                    + " '</b><br>' || case when r.fechaaprocoti is not null then 'Aprobado' else coalesce((select co.cotizacion || '<br>' || co.estado from cotizacion co inner join cotizacion_detalle cd on cd.idcotizacion = co.id and cd.ingreso = r.ingreso and co.estado <> 'Anulado' order by co.cotizacion desc limit 1),'SIN COTIZACION') end  || "
                    + " '<br><a href=''javascript:CargarModalEmpresa(' || r.ingreso || ')''>Ver Cliente</a>' || case when habitual = 'SI' THEN '<br><b>Habitual</b>' else case when aprobar_cotizacion = 'NO' THEN '<br>no.re.ap.co' ELSE '' END END || CASE WHEN garantia = 'SI' THEN '<br><b>Garantía</b>' else '' end AS ingreso, "
                    + " case when p.excel > 0 then '<a title=''Descargar Excel'' href=' || chr(34) || 'javascript:DescargarExcel(' || p.id || ',''' || p.tipoarchivo || ''')' ||  chr(34) || '>' || p.descripcion || '</a>' else p.descripcion end as plantilla, "
                    + " '<b>' || m.descripcion || '</b><br>' || e.descripcion || '<br><b>' || ma.descripcion || '</b><br>' || mo.descripcion || '<br> ' || case when r.idintervalo = 0 then 'VER ESPECIFICACIONES' ELSE '(' || desde || ' a ' || hasta || ') ' || medida end || '<br>' || r.serie as equipo, "
                    + " coalesce(to_char(ir.fecha, 'yyyy/MM/dd HH24:MI'),'No Recibido')  || '<br><br><b>Fec. Entrega</b><br>' || to_char(pro.fechapro, 'yyyy/MM/dd HH24:MI') " + (magnitud != 0 ? " || '<br><b>' || 'Ope.Pre.: ' ||  coalesce(to_char(operacion_previa, 'yyyy/MM/dd HH24:MI'),'sin operación previa') || '<b>'" : "") + " as fecha, "
                    + " tiempo(ir.fecha, now(),1) as tiempo "
                    + " from remision_detalle r inner join equipo e on r.idequipo = e.id "
                    + "                  inner join clientes cli on cli.id = r.idcliente "
                    + "			                    inner join magnitudes m on m.id = e.idmagnitud "
                    + "			                    inner join modelos mo on mo.id = r.idmodelo  "
                    + "			                    inner join marcas ma on ma.id = mo.idmarca "
                    + "			                    inner join ingreso_plantilla ip on ip.ingreso = r.ingreso "
                    + "                  inner join ingreso_programacion pro on ip.id = pro.idip "
                    + "			                    inner join plantillas p on p.id = ip.idplantilla "
                    + "                  inner join magnitud_intervalos i on i.id = r.idintervalo "
                    + "                  left join ingreso_recibidolab ir on ir.ingreso = r.ingreso "
                    + " WHERE coalesce(ip.reportado,0) = 0 and coalesce(ip.certificado,0) = 0  and r.idcliente <> 11 and operacion_previa is null and coalesce(noautorizado,0) = 0 and convenio = 0 and recibidocome = 0 and envtercero = 0 and salidaterce = 0 and salida = 0 and coalesce(ip.informetecnico,0) = 0 and coalesce(informetercerizado,'0') = '0' " + Busqueda + " and tecnico = " + idusuario
                    + " ORDER BY pro.id, ingreso";
        } else {
            if (tipobusqueda == 2) {
                Busqueda += " and (coalesce((select c.estado from cotizacion c inner join cotizacion_detalle cd on cd.idcotizacion = c.id and r.ingreso = cd.ingreso and c.estado <> 'Anulado' order by c.id DESC limit 1),'') in  ('Aprobado', 'POR REEMPLAZAR') OR habitual = 'SI' OR garantia = 'SI' OR aprobar_cotizacion = 'NO' or r.fechaaprocoti is not null) ";
            }
            Busqueda += " and (coalesce((select tecnico from ingreso_programacion pro where pro.idip = ip.id),0) = " + idusuario + " or coalesce((select tecnico from ingreso_programacion pro where pro.idip = ip.id),0) = 0)";
            sql = "select '<b>' || row_number() OVER(order by r.ingreso) || '</b><br>' || case when r.fotos > 0 then '<img src=''" + url_archivo + "Adjunto/imagenes/ingresos/' || r.ingreso || '/1.jpg'' onclick=''LlamarFotoDet(' || r.ingreso || ',' || r.fotos || ')'' width=''80px''/><br>' else '' end  as fila , "
                    + "  '<b>' || r.ingreso || "
                    + " '</b><br>' || case when r.fechaaprocoti is not null then 'Aprobado' else coalesce((select co.cotizacion || '<br>' || co.estado from cotizacion co inner join cotizacion_detalle cd on cd.idcotizacion = co.id and cd.ingreso = r.ingreso and co.estado <> 'Anulado' order by co.cotizacion desc limit 1),'SIN COTIZACION') end  || "
                    + " '<br><a href=''javascript:CargarModalEmpresa(' || r.ingreso || ')''>Ver Cliente</a>' || case when habitual = 'SI' THEN '<br><b>Habitual</b>' else case when aprobar_cotizacion = 'NO' THEN '<br>no.re.ap.co' ELSE '' END END || CASE WHEN garantia = 'SI' THEN '<br><b>Garantía</b>' else '' end AS ingreso, "
                    + " case when p.excel > 0 then '<a title=''Descargar Excel'' href=' || chr(34) || 'javascript:DescargarExcel(' || p.id || ',''' || p.tipoarchivo || ''')' ||  chr(34) || '>' || p.descripcion || '</a>' else p.descripcion end as plantilla, "
                    + " '<b>' || m.descripcion || '</b><br>' || e.descripcion || '<br><b>' || ma.descripcion || '</b><br>' || mo.descripcion || '<br> ' || case when r.idintervalo = 0 then 'VER ESPECIFICACIONES' ELSE '(' || desde || ' a ' || hasta || ') ' || medida end || '<br>' || r.serie as equipo, "
                    + " coalesce(to_char(ir.fecha, 'yyyy/MM/dd HH24:MI'),'No Recibido') || '<br><b>' || coalesce(to_char(r.fechaaprocoti, 'yyyy/MM/dd HH24:MI'),'--') || '</b><br>' || coalesce(to_char(fechaaproajus, 'yyyy/MM/dd HH24:MI'),'--')   " + (magnitud != 0 ? " || '<br><b>' || 'Ope.Pre.: ' || coalesce(to_char(operacion_previa, 'yyyy/MM/dd HH24:MI'),'sin operación previa') || '<b>'" : "") + " as fecha, "
                    + " tiempo(ir.fecha, now(),1) as tiempo "
                    + " from remision_detalle r inner join equipo e on r.idequipo = e.id "
                    + "                       inner join clientes cli on cli.id = r.idcliente "
                    + "			                    inner join magnitudes m on m.id = e.idmagnitud "
                    + "			                    inner join modelos mo on mo.id = r.idmodelo "
                    + "			                    inner join marcas ma on ma.id = mo.idmarca "
                    + "                   inner join magnitud_intervalos i on i.id = r.idintervalo "
                    + "                  left join ingreso_plantilla ip on ip.ingreso = r.ingreso "
                    + "			                    left join plantillas p on p.id = ip.idplantilla "
                    + "                  left join ingreso_recibidolab ir on ir.ingreso = r.ingreso "
                    + " WHERE coalesce(ip.reportado,0) = 0 and coalesce(ip.certificado,0) = 0 and r.idcliente <> 11 and coalesce(noautorizado,0) = 0 and salida = 0 and convenio = 0 and envtercero = 0 and salidaterce = 0 and recibidocome = 0 and coalesce(ip.informetecnico,0) = 0 and coalesce(informetercerizado,'0') = '0' " + Busqueda + " and m.id in (" + usumagnitud + ")  " + (magnitud != 0 ? " and m.id = " + magnitud : "")
                    + " ORDER BY ingreso";
        }

        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->TablaReportarIngreso");

    }

    public String Plantillas_Ingreso(HttpServletRequest request) {
        int ingreso = Globales.Validarintnonull(request.getParameter("ingreso"));
        sql = "SELECT p.id, p.descripcion, "
                + "       coalesce((select ip.id from ingreso_plantilla ip where p.id = ip.idplantilla and ip.ingreso = rd.ingreso),0) as seleccionado "
                + "   from plantillas p inner join  plantilla_equipo pe on pe.idplantilla = p.id  "
                + "                     inner join remision_detalle rd on rd.idequipo = pe.idequipo "
                + "   where rd.ingreso= " + ingreso + " order by 2";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->Plantillas_Ingreso");
    }

    public String PlantillaReportes(HttpServletRequest request) {
        int ingreso = Globales.Validarintnonull(request.getParameter("ingreso"));
        int plantilla = Globales.Validarintnonull(request.getParameter("plantilla"));

        sql = "SELECT rp.id, rp.idusuario, rp.idconcepto, rp.ajuste, rp.suministro, to_char(rp.fecha, 'dd/MM/yyyy HH24:MI') as fecha, "
                + "  rp.observacion as observaciones, rp.nroreporte, u.nombrecompleto as usuario, conclusion, "
                + "  coalesce(' - <i>Aprobado por: ' || rp.usuarioapro || ', ' || rp.cedula || ', ' || rp.cargo || ', ' || rp.tipoaprobacion || '</i>',' ') as aprobado "
                + "  from reporte_ingreso rp INNER JOIN seguridad.rbac_usuario u on u.idusu = rp.idusuario "
                + "  where rp.ingreso = " + ingreso + " and plantilla=" + plantilla;

        String sql2 = "select coalesce(tecnico,-1) AS tecnico, " + idusuario + " as usuario, "
                + "  (select count(ingreso) "
                + "  from ingreso_programacion p2 inner join ingreso_plantilla ip2 on ip2.id = p2.idip "
                + "  where tecnico = " + idusuario + " and reportado = 0) as cantidad "
                + "  from ingreso_plantilla ip LEFT JOIN ingreso_programacion pro on ip.id = pro.idip "
                + "  where ip.ingreso = " + ingreso + " and ip.idplantilla = " + plantilla;

        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->Plantillas_Ingreso") + "||" + Globales.ObtenerDatosJSon(sql2, this.getClass() + "-->Plantillas_Ingreso");
    }

    public String Editar_Plantillas(HttpServletRequest request) {
        int ingreso = Globales.Validarintnonull(request.getParameter("ingreso"));
        String a_items = request.getParameter("a_items");
        String a_opciones = request.getParameter("a_opciones");

        if (Globales.PermisosSistemas("LABORATORIO EDITAR PANTILLAS INGRESO", idusuario) == 0) {
            return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
        }

        try {
            String[] items = a_items.split("\\|");
            String[] opciones = a_opciones.split("\\|");

            sql = "select ip.id, idplantilla, reportado, p.descripcion "
                    + " from ingreso_plantilla ip  inner join plantillas p on ip.idplantilla = p.id "
                    + " where ingreso = " + ingreso;
            datos = Globales.Obtenerdatos(sql, this.getClass() + "-->Editar_Plantillas", 1);
            int encontrado = 0;
            String reportado = "";
            String plantilla = "";
            sql = "";
            String mensaje = "";
            int guardado = 0;
            for (int x = 0; x < items.length; x++) {
                encontrado = 0;
                reportado = "0";
                datos.beforeFirst();
                while (datos.next()) {
                    if (datos.getString("idplantilla").equals(items[x])) {
                        encontrado = Globales.Validarintnonull(datos.getString("id"));
                        reportado = datos.getString("reportado");
                        plantilla = datos.getString("descripcion");
                        break;
                    }
                }
                if (encontrado == 0 && opciones[x].equals("1")) {
                    sql += "INSERT INTO ingreso_plantilla(ingreso, idplantilla) VALUES (" + ingreso + "," + items[x] + ");";
                    guardado++;
                }

                if (opciones[x].equals("0")) {
                    if (encontrado > 0 && reportado.equals("0")) {
                        sql += "DELETE FROM ingreso_plantilla where id = " + encontrado + ";";
                        guardado++;
                    } else if (encontrado > 0 && reportado.equals("1")) {
                        mensaje += "<br> No se puede eliminar la plantilla " + plantilla + " porque ya fue reportada en el laboratorio";
                    }
                }
            }

            if (guardado == 0) {
                if (!mensaje.equals("")) {
                    return "1|" + mensaje;
                } else {
                    return "1|No ha cambios para actualizar";
                }
            }

            if (Globales.DatosAuditoria(sql, "LABORATORIO", "EDITAR PANTILLAS INGRESO", idusuario, iplocal, this.getClass() + "-->Editar_Plantillas")) {
                return "0|<b>Plantillas editadas con éxito</b>|" + mensaje;
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }
        } catch (Exception e) {
            return "1|" + e.getMessage();
        }
    }

    public String RecibirLab(HttpServletRequest request) {
        int ingreso = Globales.Validarintnonull(request.getParameter("ingreso"));
        String observacion = request.getParameter("observacion");
        int usuarios = Globales.Validarintnonull(request.getParameter("usuario"));
        String fecha = request.getParameter("fecha");
        String a_items = request.getParameter("a_items");

        if (Globales.PermisosSistemas("LABORATORIO RECIBIR", idusuario) == 0) {
            return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
        }

        observacion = observacion.replace('|', ' ');

        String[] items = a_items.split("\\|");

        sql = "select recibidolab from remision_detalle "
                + "  where ingreso = " + ingreso;
        int encontrado = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
        if (encontrado > 0) {
            return "1|No se puede recibir este ingreso porque ya fue recibido anteriormente";
        }

        sql = "INSERT INTO ingreso_recibidolab(ingreso, idusuario, idusuarioent, observacion) "
                + "  VALUES(" + ingreso + "," + idusuario + "," + usuarios + ",'" + observacion + "');";
        sql += "INSERT INTO ingreso_estados(ingreso, idusuario, descripcion, tiempo) "
                + " VALUES(" + ingreso + "," + idusuario + ",'ENTRADA A LABORATORIO',tiempo('" + fecha + "', now(),1));";
        sql += "UPDATE remision_detalle SET recibidolab=1 WHERE ingreso = " + ingreso + ";";

        for (int x = 0; x < items.length; x++) {
            sql += "INSERT INTO ingreso_plantilla(ingreso, idplantilla) "
                    + " VALUES (" + ingreso + "," + items[x] + ");";
        }
        if (Globales.DatosAuditoria(sql, "LABORATORIO", "RECIBIR", idusuario, iplocal, this.getClass() + "-->RecibirLab")) {
            return "0|Ingreso recibido con éxito";
        } else {
            return "1|Error inesperado... Revise el log de errores";
        }
    }

    public String RecibirIng(HttpServletRequest request) {
        int noautorizado = Globales.Validarintnonull(request.getParameter("noautorizado"));
        int ingreso = Globales.Validarintnonull(request.getParameter("ingreso"));
        String observacion = request.getParameter("observacion");
        int usuarios = Globales.Validarintnonull(request.getParameter("usuario"));
        String fecha = request.getParameter("fecha");
        String ubicacion = request.getParameter("ubicacion");
        if (Globales.PermisosSistemas("INGRESO RECIBIR", idusuario) == 0) {
            return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
        }
        sql = "select recibidoing from remision_detalle "
                + "where ingreso = " + ingreso;
        int encontrado = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
        if (encontrado > 0) {
            return "1|No se puede recibir este ingreso porque ya fue recibido anteriormente";
        }
        observacion = observacion.replace("\\|", " ");
        sql = "";
        if (ubicacion != null) {
            if (!ubicacion.equals("")) {
                String x = ubicacion.substring(1, 2);
                String y = ubicacion.substring(0, 1);

                sql = "SELECT id FROM ingreso_ubicacion where ingreso = " + ingreso;
                encontrado = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
                sql = "";
                if (encontrado == 0) {
                    sql += "INSERT INTO ingreso_ubicacion(ingreso, idusuario, x, y) "
                            + "VALUES(" + ingreso + ", " + idusuario + ",'" + x + "'  ,'" + y + "'  );";
                } else {
                    sql += "UPDATE ingreso_ubicacion "
                            + " SET x ='" + x + "'  , y='" + y + "'  , eliminado = 0, fecha= now(), idusuario=" + idusuario + "  WHERE ingreso = " + ingreso + ";";
                }
            }
        }
        sql += "INSERT INTO ingreso_recibidoing(ingreso, idusuario, idusuarioent, observacion) "
                + "VALUES(" + ingreso + ", " + idusuario + ", " + usuarios + ",'" + observacion + "'  );";
        sql += "INSERT INTO ingreso_estados(ingreso, idusuario, descripcion, tiempo) "
                + "VALUES(" + ingreso + ", " + idusuario + ",'ENTRADA A LOGISTICA PARA PROCEDER LA DEVOLUCIÓN Y ENTREGA EN LA UBICACION " + ubicacion + "',tiempo('" + fecha + "'  , now(),1));";
        sql += "UPDATE remision_detalle SET recibidoing=1 WHERE ingreso = " + ingreso + ";";
        if (noautorizado > 0) {
            sql += "UPDATE ingreso_plantilla SET noautorizado=1 WHERE ingreso = " + ingreso;
        }
        if (Globales.DatosAuditoria(sql, "INGRESOS", "RECIBIR", idusuario, iplocal, this.getClass() + "-->RecibirIng")) {
            return "0|Ingreso recibido con éxito";
        } else {
            return "1|Error inesperado... Revise el log de errores";
        }
    }

    public String EliminarRecibido(HttpServletRequest request) {
        String opcion = request.getParameter("opciones");
        int ingreso = Globales.Validarintnonull(request.getParameter("ingreso"));
        String observacion = request.getParameter("observacion");
        int Encontrado = 0;
        String modulo = "";
        try {

            switch (opcion) {
                case "Recibir Logistica":
                    if (Globales.PermisosSistemas("RECIBIR INGRESO LOGISTICA ELIMINAR", idusuario) == 0) {
                        return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                    }
                    modulo = "RECIBIR INGRESO LOGISTICA";
                    sql = "SELECT ingreso FROM devolucion_detalle dd inner join devolucion d on d.id = dd.iddevolucion where dd.ingreso = " + ingreso + " and d.estado <> 'Anulado'";
                    Encontrado = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
                    if (Encontrado > 0) {
                        return "1|No se puede eliminar el recibido porque este ingreso posee una devolución activa";
                    }
                    sql = "DELETE FROM ingreso_recibidoing WHERE ingreso = " + ingreso + ";";
                    sql += "UPDATE remision_detalle set recibidoing = 0 where ingreso = " + ingreso + ";";
                    break;
                case "Recibir Comercial":
                    if (Globales.PermisosSistemas("RECIBIR INGRESO COMERCIAL ELIMINAR", idusuario) == 0) {
                        return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                    }
                    modulo = "RECIBIR INGRESO COMERCIAL";
                    sql = "SELECT ingreso FROM ordencompra_tercero oc inner join ordencompter_detalle ocd oc on oc.id = ocd.idorden where ocd.ingreso = " + ingreso + " and oc.estado <> 'Anulado'";
                    Encontrado = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
                    if (Encontrado > 0) {
                        return "1|No se puede eliminar el recibido porque este ingreso posee una orden de compra para tercerizar activa";
                    }
                    sql = "SELECT ingreso FROM salida s inner join salida_detalle sd oc on s.id = sd.idsalida where sd.ingreso = " + ingreso + " and s.estado <> 'Anulado'";
                    Encontrado = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
                    if (Encontrado > 0) {
                        return "1|No se puede eliminar el recibido porque este ingreso posee una salida para tercerizar activa";
                    }
                    sql = "DELETE FROM ingreso_recibidocome WHERE ingreso = " + ingreso + ";";
                    sql += "UPDATE remision_detalle set envtercero = 0, salidaterce = 0 where ingreso = " + ingreso + ";";
                    break;
                case "Recibir Laboratorio":
                    if (Globales.PermisosSistemas("RECIBIR INGRESO LABORATORIO ELIMINAR", idusuario) == 0) {
                        return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                    }
                    modulo = "RECIBIR INGRESO LABORATORIO";
                    sql = "SELECT ingreso FROM reporte_ingreso where ingreso = " + ingreso;
                    Encontrado = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
                    if (Encontrado > 0) {
                        return "1|No se puede eliminar el recibido porque este ingreso posee un reporte en el laboratorio";
                    }
                    sql = "DELETE FROM ingreso_recibidolab WHERE ingreso = " + ingreso + ";";
                    sql += "DELETE FROM ingreso_plantilla WHERE ingreso = " + ingreso + ";";
                    sql += "UPDATE remision_detalle set recibidolab = 0 where ingreso = " + ingreso + ";";
                    break;
            }

            sql += "INSERT INTO ingreso_recibidoeliminado(ingreso, idusuario, observacion, opcion) "
                    + " VALUES(" + ingreso + "," + idusuario + ",'" + observacion + "','" + opcion + "')";

            if (Globales.DatosAuditoria(sql, modulo, "ELIMINAR", idusuario, iplocal, this.getClass() + "-->EliminarRecibido")) {
                return "0|Recibido eliminado con éxito";
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }

        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String NoAutorizado(HttpServletRequest request) {
        int ingreso = Globales.Validarintnonull(request.getParameter("ingreso"));
        String observacion = request.getParameter("observacion");
        int Encontrado = 0;
        try {
            if (Globales.PermisosSistemas("INGRESO NO AUTORIZADO", idusuario) == 0) {
                return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
            }

            sql = "SELECT reportado FROM remision_detalle where ingreso = " + ingreso;
            Encontrado = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
            if (Encontrado > 0) {
                return "1|No se puede colocar como no autorizado, porque fue reportado en el laboratorio";
            }

            sql = "UPDATE ingreso_plantilla set noautorizado = 1, observacion_noautorizado='" + observacion + "' where ingreso = " + ingreso + ";";
            if (Globales.DatosAuditoria(sql, "INGRESO", "NO AUTORIZADO", idusuario, iplocal, this.getClass() + "-->NoAutorizado")) {
                return "0|No autorizado aplicado con éxito";
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String EnvioReporte(HttpServletRequest request) {
        int error = 0;
        String principal = request.getParameter("principal");
        String e_email = request.getParameter("e_email");
        int ingreso = Globales.Validarintnonull(request.getParameter("ingreso"));
        int reporte = Globales.Validarintnonull(request.getParameter("reporte"));
        int plantilla = Globales.Validarintnonull(request.getParameter("plantilla"));
        String observacion = request.getParameter("observacion");

        try {
            if (Globales.PermisosSistemas("OPERACIONES PREVIA ENVIAR", idusuario) == 0) {
                return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
            }

            sql = "SELECT ri.id, cd.idcontacto, clave, c.nombrecompleto as cliente, cc.nombres as contacto, u.nombrecompleto as usuario, u.cargo, c.email, uase.correoenvio as correoasesor, "
                    + "          rd.ingreso, case when rd.idintervalo = 0 then 'VER ESPECIFICACIONES' ELSE '(' || desde || ' a ' || hasta || ') ' || medida end AS intervalo, m.descripcion as magnitud, "
                    + "          mo.descripcion as modelo, ma.descripcion as marca, e.descripcion as equipo, rd.serie, ajuste, suministro "
                    + "      FROM remision_detalle rd inner join remision r on r.id = rd.idremision "
                    + "                               inner join ingreso_plantilla ip on ip.ingreso = rd.ingreso "
                    + "	                                     inner join clientes_contac cc on cc.id = r.idcontacto "
                    + "	                                     inner join reporte_ingreso ri on ri.ingreso = rd.ingreso "
                    + "	                                     inner join clientes c on c.id = r.idcliente "
                    + "	                                     inner join equipo e on rd.idequipo = e.id "
                    + "	                                     inner join magnitudes m on m.id = e.idmagnitud "
                    + "	                                     inner join modelos mo on mo.id = rd.idmodelo "
                    + "	                                     inner join marcas ma on ma.id = mo.idmarca "
                    + "	                                     inner join magnitud_intervalos i on i.id = rd.idintervalo "
                    + "	                                     INNER JOIN seguridad.rbac_usuario u on u.idusu = ri.idusuario "
                    + "                               inner JOIN seguridad.rbac_usuario uase on uase.idusu = c.asesor "
                    + "                               LEFT JOIN cotizacion_detalle cd on cd.ingreso = rd.ingreso and cd.estado in ('Cerrado','Cotizado','POR REEMPLAZAR') "
                    + " WHERE rd.ingreso = " + ingreso + " and nroreporte = " + reporte + " and ip.idplantilla=" + plantilla;
            datos = Globales.Obtenerdatos(sql, this.getClass() + "-->EnvioReporte", 1);
            datos.next();
            int idcontacto = Globales.Validarintnonull(datos.getString("idcontacto"));
            int idajuste = Globales.Validarintnonull(datos.getString("id"));
            String ClaveAcceso = datos.getString("clave");
            String cliente = datos.getString("cliente");
            String contacto = datos.getString("contacto");
            String nomusuario = datos.getString(3);
            String cargo = datos.getString(4);
            String correocliente = datos.getString("email");
            String correoasesor = datos.getString("correoasesor");

            if (idcontacto > 0) {
                contacto = Globales.ObtenerUnValor("SELECT nombres FROM clientes_contac where  id = " + idcontacto);
            }

            String tabla = "<table border='1' width='100%'> "
                    + "    <tr> "
                    + "        <th>INGRESO</th> "
                    + "        <th>INSTRUMENTO</th> "
                    + "        <th>MARCA / MODELO</th> "
                    + "        <th>SERIE</th> "
                    + "        <th>RANGO</th> "
                    + "        <th>AJUSTE</th> "
                    + "        <th>SUMINISTRO</th> "
                    + "    </tr> "
                    + "    <tr> "
                    + "        <td rowspan='3'>" + ingreso + "</td>"
                    + "        <td rowspan='3'>" + datos.getString("equipo") + "</td>"
                    + "<td rowspan='3'>" + datos.getString("marca") + " / " + datos.getString("modelo") + "</td>"
                    + "<td rowspan='3'>" + datos.getString("serie") + "</td>"
                    + "<td rowspan='3'>" + datos.getString("intervalo") + "</td>"
                    + "<td rowspan='3'>" + datos.getString("ajuste") + "</td>"
                    + "<td rowspan='3'>" + datos.getString("suministro") + "</td></tr>";
            tabla += "</table>";

            e_email += ";" + correoenvio + (!correoasesor.equals("--") ? ";" + correoasesor : "");
            String[] email = e_email.split(";");

            String correoemp = correoenvio;

            Date fecha = new Date();
            DateFormat dateFormat = new SimpleDateFormat("HH");

            int hora = Globales.Validarintnonull(dateFormat.format(fecha));
            String saludo = "";
            if (hora <= 11) {
                saludo = "Buenos días";
            } else if (hora <= 18) {
                saludo = "Buenas tardes";
            } else {
                saludo = "Buenas noches";
            }

            if ((correoenvio.equals("")) || (clavecorreo == "")) {
                return "1|Debe configurar los parámetros: Correo envio, Clave correo envio";
            }

            String mensaje = saludo
                    + "<br><br> "
                    + "<b>SEÑORES</b><br>" + cliente + "<br>" + contacto + "<br><b>Cordial Saludo</b><br><br> "
                    + "A continuación se le envía los ajustes o suministro que requiere el siguiente equipo:<br><br> " + tabla
                    + "<br> "
                    + "Solicitamos su autorización para realizar una de las siguientes opciones:<br> "
                    + "* Realizar el ajuste del instrumento y posteriormente generar el certificado de calibración o informe de instrumentos, según corresponda. Este procedimiento tomaría un máximo de 15 (quince) días hábiles a partir de la respectiva autorización o si ustedes así lo disponen, se generará el respectivo certificado de calibración.<br> "
                    + "* Generar informe técnico.<br>";

            mensaje += "<br><br> Solicitamos respetuosamente su autorización a travéz de nuestra página web. Para ingresar a su cuenta de<b>CALIBRATION SERVICE SAS</b> haga click en el link<br>"
                    + "<a href='http://sof.calibrationservicesas.com:8080/usuarios/AprobarAjuste.php?usuario=" + correocliente + "&ajuste=" + idajuste + "&clave=" + Base64.getEncoder().encodeToString(ClaveAcceso.getBytes()) + "'>http://sof.calibrationservicesas.com:8080/usuarios/AprobarAjuste.php?usuario=" + correocliente + "&ajuste=" + idajuste + "&clave=" + Base64.getEncoder().encodeToString(ClaveAcceso.getBytes()) + "</a><br><br>"
                    + "Sus datos para ingresar son los siguientes: <br><b>usuario</b> " + correocliente
                    + "<br><b>Clave de acceso</b> " + ClaveAcceso + "<br><br>"
                    + "<img src='http://www.calibrationservicesas.com/images/logo_320.png'><br><b>" + nomusuario + "</b><br>Calibration Service SAS<br>" + cargo + "<br>AREA TÉCNICA<br><b>" + datos.getString("magnitud") + "</b><br><font color='blue'>+57 (1) 2047699 - 7285146 <br>3138141058</font><br> "
                    + "Carrera 69 a No 55 - 16 Sur Barrio ( Villa del Río) /Bogotá<br> "
                    + "Visite nuestra web http://www.calibrationservicesas.com/";

            if (!observacion.equals("")) {
                mensaje += " < b>OBSERVACIÓN DE ENVÍO:</b><br>" + observacion;
            }

            /*mensaje += "<br><br>Debe de ingresar a su cuenta de <b>CALIBRATION SERVICE SAS</b> para aprobar o no la cotización a través del link <br><br" +
                       "<a href='http://190.144.188.100:8080/usuarios/index.php'>http://190.144.188.100:8080/usuarios/index.php</a><br><br>" +
                       "Sus datos para ingresar son los siguientes: <br><b>usuario</b> " + principal +
                       "<br><b>Clave de acceso</b> " + ClaveAcceso;*/
            String correo = Globales.EnviarEmail(principal, cliente, email, "Envío de Reporte de Ingreso Número " + ingreso, mensaje, correoemp, "", correoenvio, clavecorreo, "");

            if (!correo.trim().equals("")) {
                return "1|" + correo;
            }

            sql = "UPDATE reporte_ingreso set fechaenvio = now(),  obserenvio='" + observacion + "',  correoenvio = '" + principal + " " + e_email + "' WHERE ingreso = " + ingreso + " and nroreporte = " + reporte;

            if (Globales.DatosAuditoria(sql, "OPERACIONES PREVIA", "ENVIO DE CORREO A " + e_email, idusuario, iplocal, this.getClass() + "-->EnvioReporte")) {
                return error + "|Correo enviado a|" + principal + ";" + e_email;
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }
        } catch (Exception e) {
            return "1|" + e.getMessage();
        }
    }

    public String RpInformeTecnico(HttpServletRequest request) {
        int ingreso = Globales.Validarintnonull(request.getParameter("ingreso"));
        int tipo = Globales.Validarintnonull(request.getParameter("tipo"));
        int concepto = Globales.Validarintnonull(request.getParameter("concepto"));
        int ninforme = Globales.Validarintnonull(request.getParameter("ninforme"));
        int web = Globales.Validarintnonull(request.getParameter("web"));

        if (web == 0) {
            if (Globales.PermisosSistemas("INFORME TECNICO REPORTE", idusuario) == 0) {
                return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
            }
        }

        String reporte = "ImpInformeTecnico";
        int informe = 0;
        int foto = 0;

        int tipoinforme = 1;

        if (concepto == 5) {
            reporte = "ImpInformeMantenimiento";
            tipoinforme = 2;
        } else {
            sql = "SELECT fotoinforme FROM reporte_ingreso WHERE ingreso =" + ingreso + " and idconcepto = " + concepto;
            foto = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
            if (foto > 0) {
                reporte = "ImpInformeTecnicoFoto";
            }
        }
        if (foto > 0) {
            if (concepto == 5) {
                tipoinforme = 2;
                sql = "SELECT informe FROM informetecnico_dev WHERE tipo = 2 and estado <> 'Anulado' and ingreso = " + ingreso;
            } else {
                sql = "SELECT informe FROM informetecnico_dev WHERE tipo = 1 and estado <> 'Anulado' and ingreso = " + ingreso;
            }
            informe = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
        }

        if (tipo == 1 || concepto == 5) {
            sql = "SELECT c.nombrecompleto AS cliente,c.documento,cc.nombres AS contacto,cc.email,cc.telefonos,cc.fax, "
                    + " cs.direccion || ', ' || ci.descripcion || ' (' ||d.descripcion ||'), ' || pa.descripcion as sede, it.informe, "
                    + " ci.descripcion AS ciudad,d.descripcion AS departamento,it.fecha,btrim(to_char(rd.ingreso, '0000000'))  AS ingreso, "
                    + " it.observacion, it.conclusion,e.descripcion as equipo, ma.descripcion as marca, mo.descripcion as modelo, rd.serie as serie,m.descripcion as magnitud, "
                    + " CASE WHEN rd.idintervalo = 0 THEN 'VER ESPECIFICACIONES' ELSE '(' || desde || ' a ' || hasta || ') ' || medida end AS intervalo, "
                    + " u.nombrecompleto AS usuario,u.cargo, u.documento as usudocumento "
                    + " FROM remision r "
                    + "    JOIN remision_detalle rd ON rd.idremision = r.id "
                    + "    JOIN clientes c ON r.idcliente = c.id "
                    + "    JOIN equipo e ON e.id = rd.idequipo "
                    + "    JOIN magnitudes m ON m.id = e.idmagnitud "
                    + "    JOIN magnitud_intervalos i ON i.id = rd.idintervalo "
                    + "    JOIN modelos mo ON mo.id = rd.idmodelo "
                    + "    JOIN marcas ma ON ma.id = mo.idmarca "
                    + "    JOIN clientes_contac cc ON r.idcontacto = cc.id "
                    + "    JOIN informetecnico_dev it on it.ingreso = rd.ingreso "
                    + "    JOIN clientes_sede cs ON cs.id = r.idsede "
                    + "    JOIN ciudad ci ON ci.id = cs.idciudad "
                    + "    JOIN departamento d ON d.id = ci.iddepartamento "
                    + "    JOIN pais pa on pa.id = d.idpais "
                    + "    JOIN seguridad.rbac_usuario u on u.idusu = it.idusuario "
                    + "   where rd.ingreso = " + ingreso + " and it.estado <> 'Anulado' and it.tipo = " + tipoinforme;
        } else {
            reporte = "VPInformeTecnico";
            sql = "SELECT c.nombrecompleto AS cliente,c.documento,cc.nombres AS contacto,cc.email,cc.telefonos,cc.fax,cs.direccion || ', ' || ci.descripcion || ' (' ||d.descripcion ||'), ' || pa.descripcion as sede,"
                    + " ci.descripcion AS ciudad,d.descripcion AS departamento,btrim(to_char(rd.ingreso, '0000000'))  AS ingreso,ri.observacion, ri.conclusion, "
                    + " e.descripcion as equipo, ma.descripcion as marca, mo.descripcion as modelo, rd.serie as serie,m.descripcion as magnitud, "
                    + " CASE WHEN rd.idintervalo = 0 THEN 'VER ESPECIFICACIONES' ELSE '(' || desde || ' a ' || hasta || ') ' || medida end AS intervalo, "
                    + " u.nombrecompleto AS usuario,u.cargo, u.documento as usudocumento "
                    + " FROM remision r "
                    + "       JOIN remision_detalle rd ON rd.idremision = r.id "
                    + "       JOIN clientes c ON r.idcliente = c.id "
                    + "       JOIN equipo e ON e.id = rd.idequipo "
                    + "       JOIN magnitudes m ON m.id = e.idmagnitud "
                    + "       JOIN magnitud_intervalos i ON i.id = rd.idintervalo "
                    + "       JOIN modelos mo ON mo.id = rd.idmodelo "
                    + "       JOIN marcas ma ON ma.id = mo.idmarca "
                    + "       JOIN clientes_contac cc ON r.idcontacto = cc.id "
                    + "       JOIN clientes_sede cs ON cs.id = r.idsede "
                    + "       JOIN ciudad ci ON ci.id = cs.idciudad "
                    + "       JOIN reporte_ingreso ri ON ri.ingreso = rd.ingreso  "
                    + "       JOIN departamento d ON d.id = ci.iddepartamento "
                    + "       JOIN pais pa on pa.id = d.idpais "
                    + "       JOIN seguridad.rbac_usuario u on u.idusu = ri.idusuario "
                    + " where rd.ingreso = " + ingreso + " and idconcepto = " + concepto;
        }

        datos = Globales.Obtenerdatos(sql, this.getClass() + "-->GenerarSolicitud", 1);

        /*if (datos.Rows.Count == 0)
                return "1|No hay nada que reportar";

            String DirectorioReportesRelativo = "~/Reportes/";
            String directorio = "~/DocumPDF/";
            String unico = "Ingreso-" + ingreso + "_" + DateTime.Now.ToString("yyyyMMddHHmmss");
            String urlArchivo = String.Format("{0}.{1}", reporte, "rdlc");

            String urlArchivoGuardar;
            urlArchivoGuardar = String.Format("{0}.{1}", unico, "pdf");

            String FullPathReport = String.Format("{0}{1}",
                                    this.HttpContext.Server.MapPath(DirectorioReportesRelativo),
                                        urlArchivo);
            String FullGuardar = String.Format("{0}{1}",
                                    this.HttpContext.Server.MapPath(directorio),
                                        urlArchivoGuardar);
            Microsoft.Reporting.WebForms.ReportViewer Reporte = new ReportViewer();
            Reporte.Reset();
            Reporte.LocalReport.ReportPath = FullPathReport;
            Reporte.LocalReport.EnableExternalImages = true;

            if (foto > 0)
            {
                String imagen1 = ruta_urlarchivo + "/imagenes/informemantenimineto/" + informe + "/1.jpg";
                String imagen2 = ruta_urlarchivo + "/imagenes/informemantenimineto/" + informe + "/2.jpg";
                String imagen3 = ruta_urlarchivo + "/imagenes/informemantenimineto/" + informe + "/3.jpg";
                String imagen4 = ruta_urlarchivo + "/imagenes/informemantenimineto/" + informe + "/4.jpg";
                String imagen5 = ruta_urlarchivo + "/imagenes/informemantenimineto/" + informe + "/5.jpg";
                String imagen6 = ruta_urlarchivo + "/imagenes/informemantenimineto/" + informe + "/6.jpg";
                ReportParameter[] parameters = new ReportParameter[6];
                parameters[0] = new ReportParameter("imagen1", imagen1);
                parameters[1] = new ReportParameter("imagen2", imagen2);
                parameters[2] = new ReportParameter("imagen3", imagen3);
                parameters[3] = new ReportParameter("imagen4", imagen4);
                parameters[4] = new ReportParameter("imagen5", imagen5);
                parameters[5] = new ReportParameter("imagen6", imagen6);
                Reporte.LocalReport.SetParameters(parameters);
            }

            

            ReportDataSource DataSource = new ReportDataSource("DataSet1", datos);
            Reporte.LocalReport.DataSources.Add(DataSource);
            Reporte.LocalReport.Refresh();
            byte[] ArchivoPDF = Reporte.LocalReport.Render("PDF");
            FileStream fs = new FileStream(FullGuardar, FileMode.Create);
            fs.Write(ArchivoPDF, 0, ArchivoPDF.Length);
            fs.Close();
            Globales.GuardarAuditoria("REPORTE GENERADO CON EL NOMBRE " + urlArchivoGuardar, "INFORME TECNICO", "REPORTE", idusuario, iplocal);*/
        return "0|--"; //+ urlArchivoGuardar;

    }

    public String ReportarIngreso(HttpServletRequest request) {
        int ingreso = Globales.Validarintnonull(request.getParameter("ingreso"));
        int id = Globales.Validarintnonull(request.getParameter("id"));
        int idconcepto = Globales.Validarintnonull(request.getParameter("idconcepto"));
        String concepto = request.getParameter("concepto");
        String ajuste = request.getParameter("ajuste");
        String suministro = request.getParameter("suministro");
        String observacion = request.getParameter("observacion");
        int reporte = Globales.Validarintnonull(request.getParameter("reporte"));
        String fecha = request.getParameter("fecha");
        String conclusion = request.getParameter("conclusion");
        int plantilla = Globales.Validarintnonull(request.getParameter("plantilla"));

        if (Globales.PermisosSistemas("INGRESO REPORTAR", idusuario) == 0) {
            return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
        }

        int calibracion = 0;
        int noautorizado = 0;
        observacion = observacion.replace('|', ' ');
        conclusion = conclusion.replace('|', ' ');
        String cliente = "";

        try {
            switch (idconcepto) {
                case 1:
                    calibracion = 1;
                    ajuste = "";
                    suministro = "";
                    conclusion = "";
                    calibracion = 1;
                    break;
                case 2:
                    calibracion = 0;
                    observacion = "";
                    conclusion = "";
                    break;
                case 3:
                    calibracion = 0;
                    ajuste = "";
                    suministro = "";
                    break;
                case 4:
                    calibracion = 1;
                    ajuste = "";
                    suministro = "";
                    conclusion = "";
                    break;
                case 5:

                    sql = "SELECT calibracion from ingreso_plantilla WHERE ingreso = " + ingreso + " and idplantilla = " + plantilla;
                    calibracion = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
                    ajuste = "";
                    suministro = "";
                    break;
                case 6:
                    ajuste = "";
                    suministro = "";
                    conclusion = "";
                    calibracion = 0;
                    noautorizado = 1;
                    break;
                case 7:
                    calibracion = 1;
                    observacion = "";
                    conclusion = "";
                    ajuste = "";
                    break;
                case 8:
                    calibracion = 0;
                    observacion = "";
                    conclusion = "";
                    suministro = "";
                    break;
            }

            sql = "select rd.id from remision_detalle rd inner join ingreso_plantilla ip on ip.ingreso = rd.ingreso "
                    + " where rd.ingreso = " + ingreso + " and (salida = 1 or certificado = 1)  AND idplantilla=" + plantilla;
            int encontrado = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
            if (encontrado > 0) {
                return "1|No se puede reportar un ingreso Devuelto/Certificado";
            }

            sql = "select id from ingreso_ajuste where ingreso = " + ingreso + " and plantilla = " + plantilla;
            int ingreso_ajuste = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
            if (ingreso_ajuste > 0) {
                return "1|No se puede reportar un ingreso que posee un ajuste registrado... Si desea editarlo debe de realizarlo por Laboratorio-Ajustes";
            }

            sql = "SELECT habitual, coalesce(to_char(fechaaprocoti,'yyyy-MM-dd'),'') as fechaaprocoti, aprobar_cotizacion, garantia, sitio, convenio, c.nombrecompleto as cliente, u.nombrecompleto as asesor, "
                    + " (SELECT c.estado "
                    + "       from cotizacion c inner join cotizacion_detalle cd on cd.idcotizacion = c.id "
                    + "       where cd.ingreso = rd.ingreso and c.estado <> 'Anulado' order by c.id desc limit 1) as estado "
                    + "  FROM  clientes c inner join remision_detalle rd on rd.idcliente = c.id "
                    + "                   inner join seguridad.rbac_usuario u on u.idusu = c.asesor "
                    + "  where rd.ingreso = " + ingreso;
            datos = Globales.Obtenerdatos(sql, this.getClass() + "-->ReportarIngreso", 1);
            datos.next();
            String Habitual = datos.getString("habitual").trim();
            String AprobarCotizacion = datos.getString("aprobar_cotizacion").trim();
            String Garantia = datos.getString("garantia").trim();
            String Sitio = datos.getString("sitio").trim();
            String FechaAproCoti = datos.getString("fechaaprocoti").trim();
            String Convenio = datos.getString("convenio").trim();
            String Estado = datos.getString("estado").trim();
            cliente = datos.getString("cliente") + "|" + datos.getString("asesor") + "|" + Habitual;

            if (Habitual.equals("NO") && AprobarCotizacion.equals("SI") && Sitio.equals("NO") && Garantia.equals("NO") && FechaAproCoti.equals("") && Convenio.equals("NO")) {
                if (Estado.equals("")) {
                    return "1|No se puede guardar la operación previa porque el ingreso no ha sido cotizado";
                }
                if (!Estado.equals("Aprobado") && !Estado.equals("POR REEMPLAZAR")) {
                    return "1|No se puede guardar la operación previa porque la cotización del ingreso no ha sido aprobada";
                }
            }

            sql = "select id from informetecnico_dev WHERE estado <> 'Anulado' and  tipo = 1 and ingreso = " + ingreso + " and idplantilla = " + plantilla;
            int informe = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
            if (informe > 0) {
                return "1|No se puede reportar un ingreso que se le haya generado el informe técnico";
            }

            if (id == 0) {

                sql = "select max(nroreporte) from reporte_ingreso where ingreso = " + ingreso + " and plantilla = " + plantilla;
                int ultreporte = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
                if ((reporte - ultreporte) != 1) {
                    return "1|El reporte que debe guardar es el número " + (ultreporte + 1);
                }

                if (ultreporte > 0 && idconcepto != 5) {
                    sql = "select id from reporte_ingreso where ingreso = " + ingreso + " and plantilla = " + plantilla + " and idconcepto in (2,8)";
                    encontrado = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
                    if (encontrado > 0) {
                        return "1|No se puede agregar un segundo reporte, porque fue reportado requiere ajuste. Debe de reportalo en el módulo de ajuste";
                    }

                }
                sql = "INSERT INTO reporte_ingreso(ingreso, idusuario, idconcepto, concepto, ajuste, suministro, calibracion, observacion, nroreporte, conclusion, plantilla) "
                        + "  VALUES(" + ingreso + "," + idusuario + "," + idconcepto + ",'" + concepto + "','" + ajuste + "','" + suministro + "'," + calibracion + ",'" + observacion + "'," + reporte + ",'" + conclusion + "'," + plantilla + ");";
                sql += "INSERT INTO ingreso_estados(ingreso, idusuario, descripcion, tiempo) "
                        + " VALUES(" + ingreso + "," + idusuario + ",'REGISTRADO REPORTE NRO " + reporte + " EN LABORATORIO (" + concepto + ")',tiempo('" + fecha + "', now(),1));";
            } else {
                if (idconcepto > 1 && idconcepto != 4) {
                    sql = "select cotizado from reporte_ingreso ri where id = " + id;
                    int cotizado = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
                    if (cotizado > 0) {
                        return "1|No se puede actualizar un reporte cotizado";
                    }
                }
                sql = "UPDATE reporte_ingreso "
                        + "      SET idusuario=" + idusuario + ",idconcepto=" + idconcepto + ",concepto='" + concepto + "', "
                        + "      ajuste='" + ajuste + "',suministro='" + suministro + "', calibracion=" + calibracion
                        + "      ,observacion='" + observacion + "', conclusion='" + conclusion + "',  fecha=now() "
                        + "      WHERE id = " + id + ";";
                sql += "INSERT INTO ingreso_estados(ingreso, idusuario, descripcion, tiempo) "
                        + " VALUES(" + ingreso + "," + idusuario + ",'ACTUALIZADO REPORTE NRO " + reporte + " DE LABORATORIO (" + concepto + ")',tiempo('" + fecha + "', now(),1));";

                if (idconcepto == 1 || idconcepto == 4) {
                    sql += "update reporte_ingreso r set cotizado = coalesce(cd.id,0) "
                            + " from cotizacion_detalle cd "
                            + " where cd.ingreso = r.ingreso and idconcepto  = 1 and r.id = " + id + ";";
                }
            }
            sql += "UPDATE ingreso_plantilla SET calibracion = " + calibracion + ", reportado = 1, noautorizado =" + noautorizado + " WHERE ingreso = " + ingreso + " and idplantilla = " + plantilla + ";";
            if (idconcepto > 1 && idconcepto != 4) {
                sql += " UPDATE cotizacion c SET estado = 'POR REEMPLAZAR' "
                        + " from cotizacion_detalle cd "
                        + " WHERE c.id = cd.idcotizacion and cd.ingreso = " + ingreso + " and c.estado in ('Cerrado','Cotizado', 'Aprobado');";

                sql += " UPDATE cotizacion_detalle  SET estado = 'POR REEMPLAZAR' "
                        + " WHERE ingreso = " + ingreso;
            }
            if (Globales.DatosAuditoria(sql, "INGRESO", "REPORTAR", idusuario, iplocal, this.getClass() + "-->ReportarIngreso")) {
                if (id == 0) {
                    sql = "select max(id) from reporte_ingreso";
                    id = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
                }
                return "0|Ingreso reportado con éxito|" + id + "|" + cliente;
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }
        } catch (Exception e) {
            return "1|" + e.getMessage();
        }
    }

    public String ElimarReporteIngreso(HttpServletRequest request) {
        int ingreso = Globales.Validarintnonull(request.getParameter("ingreso"));
        String observacion = request.getParameter("observaciones");
        int reporte = Globales.Validarintnonull(request.getParameter("reporte"));
        int idreporte = Globales.Validarintnonull(request.getParameter("idreporte"));
        int plantilla = Globales.Validarintnonull(request.getParameter("plantilla"));
        String fecha = request.getParameter("fecha");

        if (Globales.PermisosSistemas("INGRESO ELIMINAR REPORTE", idusuario) == 0) {
            return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
        }

        sql = "select rd.id from remision_detalle rd inner join ingreso_plantilla ip on ip.ingreso = rd.ingreso "
                + " where rd.ingreso = " + ingreso + " and (salida = 1 or certificado = 1)  AND idplantilla=" + plantilla;
        int encontrado = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
        if (encontrado > 0) {
            return "1|No se puede ELIMINAR un  reporte de ingreso Devuelto/Certificado";
        }

        sql = "select id from ingreso_ajuste where ingreso = " + ingreso + " and plantilla = " + plantilla;
        int ingreso_ajuste = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
        if (ingreso_ajuste > 0) {
            return "1|No se puede reportar un ingreso que posee un ajuste registrado... Si desea editarlo debe de realizarlo por Laboratorio-Ajustes";
        }

        sql = "select c.id from cotizacion_detalle cd inner join cotizacion c on c.id = cd.idcotizacion "
                + "where ingreso = " + ingreso + " and c.estado <> 'Anulado' and c.estado <> 'Reemplazado' and cd.idservicio in (4,5) and idserviciodep > 0";
        encontrado = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
        if (encontrado > 0) {
            return "1|No se puede eliminar un reporte que haya sido cotizado";
        }

        sql = "select tipo_aprobacion "
                + "from reporte_ingreso where id = " + idreporte;
        encontrado = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
        if (encontrado > 0) {
            return "1|No se puede eliminar un reporte que haya sido aprobado por el cliente";
        }

        sql = "select id from informetecnico_dev WHERE estado <> 'Anulado' and  tipo = 1 and ingreso = " + ingreso + " and idplantilla = " + plantilla;
        int informe = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
        if (informe > 0) {
            return "1|No se puede reportar un ingreso que se le haya generado el informe técnico";
        }
        sql = "INSERT INTO public.reporteingreso_eliminados(ingreso, reporte, idusuario, concepto, ajuste, suministro, \n"
                + " plantilla, conclusion, observacion, idespecialista, fecha_reporte, observacion_eliminacion) "
                + " select ingreso, nroreporte, " + idusuario + ", concepto, ajuste, suministro, plantilla, conclusion, observacion, idusuario, fecha,'" + observacion + "' \n"
                + " from reporte_ingreso where id = " + idreporte + ";";
        sql += "DELETE FROM reporte_ingreso WHERE id = " + idreporte + ";";
        sql += "INSERT INTO ingreso_estados(ingreso, idusuario, descripcion, tiempo) "
                + " VALUES(" + ingreso + "," + idusuario + ",'REPORTE ELIMINADO NRO " + reporte + " EN LABORATORIO',tiempo('" + fecha + "', now(),1));";
        System.out.println(sql);
        if (Globales.DatosAuditoria(sql, "INGRESO", "ELIMINAR REPORTAR", idusuario, iplocal, this.getClass() + "-->ElimarReporteIngreso")) {
            return "0|Reportado eliminado con éxito";
        } else {
            return "1|Error inesperado... Revise el log de errores";
        }
    }

    public String TablaReporIngEliminados(HttpServletRequest request) {
        int ingreso = Globales.Validarintnonull(request.getParameter("ingreso"));
        sql = " SELECT ingreso, concepto, ajuste, suministro, u.nomusu || '<br>' || to_char(fecha,'yyyy/MM/dd HH24:MI') as eliminado, \n"
                + "       p.codigo as plantilla, conclusion, observacion, ur.nomusu || '<br>' || to_char(fecha_reporte,'yyyy/MM/dd HH24:MI') as reportado, \n"
                + "       observacion_eliminacion, reporte\n"
                + "  FROM public.reporteingreso_eliminados re inner join seguridad.rbac_usuario u on u.idusu = re.idusuario\n"
                + "					   inner join seguridad.rbac_usuario ur on ur.idusu = re.idespecialista\n"
                + "					   inner join plantillas p on p.id = re.plantilla "
                + " WHERE re.ingreso = " + ingreso
                + "  ORDER BY re.id DESC";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->TablaReporIngEliminados");
    }

    public String InformeTecnicoDev(HttpServletRequest request) {
        int ingreso = Globales.Validarintnonull(request.getParameter("ingreso"));
        int reporte = Globales.Validarintnonull(request.getParameter("reporte"));
        int plantilla = Globales.Validarintnonull(request.getParameter("plantilla"));

        int contador = 0;
        String observacion = "";
        String conclusion = "";
        try {
            sql = "SELECT id, idusuarioapro from informetecnico_dev where estado <> 'Anulado' and  tipo = 1 and ingreso = " + ingreso + " and idplantilla=" + plantilla;
            int id = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
            if (id > 0) {
                return "1|Ya se generó el informe técnico de este ingreso";
            }
            sql = "SELECT idconcepto, concepto,  observacion, conclusion from reporte_ingreso where ingreso = " + ingreso + " and nroreporte = " + reporte + " and plantilla = " + plantilla + " order by id desc limit 1";
            datos = Globales.Obtenerdatos(sql, this.getClass() + "-->InformeTecnicoDev", 1);
            datos.next();
            if (!datos.getString("idconcepto").equals("3") && !datos.getString("idconcepto").equals("5")) {
                return "1|No se puede generar un informe cuando el reporte está en estado " + datos.getString("concepto");
            }
            int tipo = (datos.getString("idconcepto").equals("3") ? 1 : 2);

            conclusion = datos.getString("conclusion");
            observacion = datos.getString("observacion");

            if (id == 0) {
                if (Globales.PermisosSistemas("INFORME TECNICO GUARDAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "SELECT informetecnico FROM contadores";
                contador = Globales.Validarintnonull(Globales.ObtenerUnValor(sql)) + 1;
                sql = "select id from version_documento WHERE documento = 'INFORMETECNICO' ORDER BY revision desc limit 1";
                int version = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));

                sql = "INSERT INTO informetecnico_dev(informe, ingreso, idusuario, observacion, conclusion, tipo, idplantilla, idversion) "
                        + "VALUES(" + contador + ", " + ingreso + ", " + idusuario + ",'" + observacion + "'  ,'" + conclusion + "'  ," + tipo + "  ," + plantilla + "," + version + ");";
                sql += " UPDATE ingreso_plantilla SET informetecnico = " + contador + " WHERE ingreso = " + ingreso + " and idplantilla = " + plantilla + ";";
                sql += " UPDATE contadores SET informetecnico = " + contador + ";";
                if (Globales.DatosAuditoria(sql, "INFORME TECNICO", "GUARDAR", idusuario, iplocal, this.getClass() + "-->InformeTecnicoDev")) {
                    id = Globales.Validarintnonull(Globales.ObtenerUnValor("select max(id) from informetecnico_dev"));
                    return "0|Informe tecnico realizado con el número " + contador + "|" + id;
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }

            } else {
                if (Globales.PermisosSistemas("INFORME TECNICO EDITAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "UPDATE informetecnico_dev SET fecha = now(), idusuario = " + idusuario + ", observacion ='" + observacion + "'  , conclusion='" + conclusion + "' "
                        + "WHERE id = " + id + ";";

                if (Globales.DatosAuditoria(sql, "INFORME TECNICO", "EDITAR", idusuario, iplocal, this.getClass() + "-->InformeTecnicoDev")) {
                    return "0|Informe tecnico actualizado con el número " + contador + "|" + id;
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            }

        } catch (Exception ex) {

            return "1|" + ex.getMessage();
        }

    }

    public String TablaCertificados(HttpServletRequest request) {
        String certificado = request.getParameter("certificado");
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        int ingreso = Globales.Validarintnonull(request.getParameter("ingreso"));
        String estado = request.getParameter("estado");
        String fechad = request.getParameter("fechad");
        String fechah = request.getParameter("fechah");
        int magnitud = Globales.Validarintnonull(request.getParameter("magnitud"));
        String equipo = request.getParameter("equipo");
        int marca = Globales.Validarintnonull(request.getParameter("marca"));
        String modelo = request.getParameter("modelo");
        String intervalo = request.getParameter("intervalo");
        String serie = request.getParameter("serie");
        int usuarios = Globales.Validarintnonull(request.getParameter("usuario"));

        int version = Globales.Validarintnonull(request.getParameter("version"));
        String generado = request.getParameter("generado");

        String busqueda = "WHERE r.recibidocome = 0 ";
        if (!certificado.equals("")) {
            busqueda += " AND c.numero = '" + certificado + "'";
        } else if (ingreso > 0) {
            busqueda += " AND r.ingreso = " + ingreso;
        } else {
            busqueda += " AND to_char(c.fecha,'yyyy-MM-dd') >= '" + fechad + "' and to_char(c.fecha,'yyyy-MM-dd') <= '" + fechah + "'";
            if (cliente > 0) {
                busqueda += " AND r.idcliente = " + cliente;
            }
            if (!estado.equals("Todos")) {
                if (estado.equals("NoAprobado")) {
                    busqueda += " AND c.estado in ('Registrado','Revisado')";
                } else {
                    busqueda += " AND c.estado = '" + estado + "'";
                }
            }
            if (magnitud > 0) {
                busqueda += " AND e.idmagnitud = " + magnitud;
            }
            if (!equipo.equals("")) {
                busqueda += " AND e.descripcion = '" + equipo + "'";
            }
            if (marca > 0) {
                busqueda += " AND mo.idmarca = " + marca;
            }
            if (!modelo.equals("")) {
                busqueda += " AND mo.descripcion = '" + modelo + "'";
            }
            if (!intervalo.equals("")) {
                busqueda += " AND '(' || desde || ' a ' || hasta || ') ' || medida = '" + intervalo + "'";
            }
            if (!serie.trim().equals("")) {
                busqueda += " AND r.serie ilike '%" + serie.trim() + "%'";
            }
            if (usuarios > 0) {
                busqueda += " AND c.idusuario = " + usuarios;
            }
            if (!generado.equals("Todos")) {
                busqueda += " AND c.generado = '" + generado + "'";
            }
            if (version > 0) {
                busqueda += " AND c.idversion = " + version;
            }
        }

        sql = "select DISTINCT r.ingreso,  c.numero,  c.id, "
                + "          '<b>' || m.descripcion || '</b><br>' || e.descripcion || '<br><b>' || ma.descripcion || '</b><br>' || mo.descripcion || '<br> ' || case when r.idintervalo = 0 then 'VER ESPECIFICACIONES' ELSE '(' || desde || ' a ' || hasta || ') ' || medida end || '<br>' || r.serie as equipo, "
                + "              to_char(c.fechaexp, 'yyyy/MM/dd HH24:MI') as fechaexp, "
                + "          '<a href=' || chr(34) || 'javascript:HistoricoRevision(''' || c.numero || ''')' || chr(34) || '>' ||  c.estado || '</a>' as estado, "
                + "          u.nombrecompleto as tecnico, cli.nombrecompleto as cliente, "
                + "          c.proximacali, to_char(c.fechaimpreso, 'yyyy/MM/dd HH24:MI') as impreso, c.observacion, "
                + "          case when coalesce(c.direccioncer,'') <> '' THEN c.direccioncer else case when coalesce(cd.direccion,'SEDE') = 'SEDE' OR trim(cd.direccion) = '' THEN cs.direccion || ', ' || ci.descripcion || ' (' || d.descripcion || ')' else cd.direccion end end as direccioncer, "
                + "              case when coalesce(c.nombrecer,'') <> '' THEN c.nombrecer else case when coalesce(cd.nombreca,'') <> '' then cd.nombreca ELSE cli.nombrecompleto END END as nombrecer, "
                + "          to_char(c.fechaanula, 'yyyy/MM/dd HH24:MI') || '<br><b>' || u3.nombrecompleto || '</b>' as anulado, c.observacionanula,"
                + "          generado, to_char(r.fechaaprocoti, 'yyyy/MM/dd HH24:MI') || '<br>' || to_char(ip.fechaaproajus, 'yyyy/MM/dd HH24:MI') as fechaapro, "
                + "          case when r.salidaterce = 0 then tiempo(r.fechaing, (select min(fecha) from reporte_ingreso ri where ri.ingreso = r.ingreso),2) else 'Tercerizado' end as treportado, "
                + "          case when r.salidaterce = 0 then tiempo((select min(fecha) from reporte_ingreso ri where ri.ingreso = r.ingreso), c.fechaexp, 2) else tiempo(r.fechaing, c.fechaexp, 2) end as tcertificado, "
                + "          '<button class=''btn btn-glow btn-primary'' title=''Aprobar o Anular Certificado'' type=''button'' onclick=' || chr(34) || 'AprobarCertificado(''' || c.numero || ''',''' || c.estado || ''')' ||chr(34) || '><span data-icon=''&#xe147;''></span></button>' || "
                + "          '<button class=''btn btn-glow btn-success imprimir'' title=''Imprimir Certificado'' type=''button'' onclick=' || chr(34) || 'ImprimirCertificadoPDF(''' || case when c.impreso = 0 then c.item || '-' || c.numero else c.numero || ' ' || to_char(r.ingreso,'0000000') end || ''')' || chr(34) || '><span data-icon=''&#xe0f7;''></span></button>"
                + "          <button class=''btn btn-glow btn-warning'' title=''Cálculos del Certificado'' type=''button'' onclick=' || chr(34) || 'CalculoCertificadoF(' || r.ingreso || ',' || c.idversion || ')' || chr(34) || '><span data-icon=''&#xe098;''></span></button>' as opciones, "
                + "          (select to_char(ca.fecha, 'yyyy/MM/dd HH24:MI') || '<br><b>' || ua.nombrecompleto from certificados_aprobados ca inner join seguridad.rbac_usuario ua on ua.idusu = ca.idusuario where ca.idcertificado = c.id) as  aprobado, "
                + "          case when c.impreso = 0 then c.item || '-' || c.numero else c.numero || ' ' || to_char(r.ingreso,'0000000') end as imprimir, "
                + "          '<b><center>' || ip.stiker || '</center></b>' || to_char(ip.fecha_stiker, 'yyyy/MM/dd HH24:MI')  as  sticker "
                + "      from remision_detalle r inner join equipo e on r.idequipo = e.id "
                + "				    INNER JOIN ingreso_plantilla ip on ip.ingreso = r.ingreso "
                + "                              INNER JOIN remision re on re.id = r.idremision "
                + "					                    inner join magnitudes m on m.id = e.idmagnitud "
                + "					                    inner join modelos mo on mo.id = r.idmodelo "
                + "					                    inner join marcas ma on ma.id = mo.idmarca "
                + "                              inner join certificados c on c.ingreso = r.ingreso "
                + "                              inner join seguridad.rbac_usuario u on u.idusu = c.idusuario "
                + "                              inner join seguridad.rbac_usuario u3 on u3.idusu = c.idusuarioanula "
                + "                              inner join magnitud_intervalos i on i.id = r.idintervalo "
                + "                              inner join clientes cli on cli.id = r.idcliente "
                + "                              INNER JOIN clientes_sede cs on re.idsede = cs.id "
                + "                              INNER JOIN clientes_contac cc on re.idcontacto = cc.id "
                + "				                        inner join ciudad ci on ci.id = cs.idciudad "
                + "				                        inner join departamento d on d.id = ci.iddepartamento  "
                + "                              left join cotizacion_detalle cd on cd.ingreso = r.ingreso "
                + "                              left join cotizacion co on co.id = cd.idcotizacion and co.estado in ('Cerrado','Aprobado') "
                + busqueda + " ORDER BY r.ingreso";

        System.out.println(sql);
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->TablaCertificados");
    }
    
    public String HistorialRevisionCertificado(HttpServletRequest request) {
        String certificado = request.getParameter("certificado");

        sql = "select certificado, to_char(fecha,'yyyy/MM/dd HH24:MI') as fecha, observacion, items, u.nombrecompleto "
                + "from certificados_revision cr inner join seguridad.rbac_usuario u on u.idusu = cr.idusuario "
                + "where certificado = '" + certificado + "'";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->HistorialRevisionCertificado");
    }

    protected void Servidor(HttpServletRequest request, HttpServletResponse response) {
        try {
            response.setContentType("text/html;charset=UTF-8");
            String ruta_origen = request.getHeader("Origin");

            HttpSession misession = request.getSession();
            idusuario = misession.getAttribute("idusuario").toString();
            iplocal = misession.getAttribute("IPLocal").toString();
            usuario = misession.getAttribute("codusu").toString();
            usumagnitud = misession.getAttribute("magnitud").toString();
            correoenvio = misession.getAttribute("correoenvio").toString();
            clavecorreo = misession.getAttribute("clavecorreo").toString();
            response.setHeader("Access-Control-Allow-Origin", request.getHeader("Origin"));
            response.setHeader("Access-Control-Allow-Credentials", "true");

            PrintWriter out = response.getWriter();
            if (Globales.ValidarRuta(ruta_origen) == false) {
                out.print("[{\"error\":\"505\",\"mensaje\":\"Usted no tiene permiso para acceder a esta ruta\"}]");
            } else if (misession.getAttribute("codusu") == null) {
                out.println("cerrada");
            } else if (!Globales.IpUsuario(misession.getAttribute("idusuario").toString()).equals(misession.getAttribute("IPLocal").toString())) {
                out.println("cambio-ip");
            } else {
                String opcion = request.getParameter("opcion");
                switch (opcion) {
                    case "NotifiReportes":
                        out.println(NotifiReportes());
                        break;
                    case "NotifiReportesApro":
                        out.println(NotifiReportesApro());
                        break;
                    case "ConsultarIngresos":
                        out.println(ConsultarIngresos(request));
                        break;
                    case "DetalleIngreso":
                        out.println(DetalleIngreso(request));
                        break;
                    case "BuscarIngresoOperacion":
                        out.println(BuscarIngresoOperacion(request));
                        break;
                    case "BuscarIngresoPlaCert":
                        out.println(BuscarIngresoPlaCert(request));
                        break;
                    case "BuscarReportesLab":
                        out.println(BuscarReportesLab(request));
                        break;
                    case "BuscarIngresoAjuste":
                        out.println(BuscarIngresoAjuste(request));
                        break;
                    case "BuscarIngresoReport":
                        out.println(BuscarIngresoReport(request));
                        break;
                    case "BuscarEstadoIngreso":
                        out.println(BuscarEstadoIngreso(request));
                        break;
                    case "BuscarIngresoCertificado":
                        out.println(BuscarIngresoCertificado(request));
                        break;
                    case "BuscarIngresoRecbLab":
                        out.println(BuscarIngresoRecbLab(request));
                        break;
                    case "BuscarIngresoRecbCome":
                        out.println(BuscarIngresoRecbCome(request));
                        break;
                    case "BuscarIngresoCome":
                        out.println(BuscarIngresoCome(request));
                        break;
                    case "BuscarIngresoRecbIng":
                        out.println(BuscarIngresoRecbIng(request));
                        break;
                    case "TablaRecibidoLab":
                        out.println(TablaRecibidoLab(request));
                        break;
                    case "TablaRecibidoCome":
                        out.println(TablaRecibidoCome(request));
                        break;
                    case "TablaRecibidoIng":
                        out.println(TablaRecibidoIng(request));
                        break;
                    case "TablaCertificadoIngreso":
                        out.println(TablaCertificadoIngreso(request));
                        break;
                    case "TablaReportarIngreso":
                        out.println(TablaReportarIngreso(request));
                        break;
                    case "Plantillas_Ingreso":
                        out.println(Plantillas_Ingreso(request));
                        break;
                    case "Editar_Plantillas":
                        out.println(Editar_Plantillas(request));
                        break;
                    case "RecibirLab":
                        out.println(RecibirLab(request));
                        break;
                    case "EliminarRecibido":
                        out.println(EliminarRecibido(request));
                        break;
                    case "NoAutorizado":
                        out.println(NoAutorizado(request));
                        break;
                    case "ReportarIngreso":
                        out.println(ReportarIngreso(request));
                        break;
                    case "EnvioReporte":
                        out.println(EnvioReporte(request));
                        break;
                    case "RpInformeTecnico":
                        out.println(RpInformeTecnico(request));
                        break;
                    case "PlantillaReportes":
                        out.println(PlantillaReportes(request));
                        break;
                    case "RecibirIng":
                        out.println(RecibirIng(request));
                        break;
                    case "InformeTecnicoDev":
                        out.println(InformeTecnicoDev(request));
                        break;
                    case "ElimarReporteIngreso":
                        out.println(ElimarReporteIngreso(request));
                        break;
                    case "TablaReporIngEliminados":
                        out.println(TablaReporIngEliminados(request));
                        break;
                    case "TablaCertificados":
                        out.println(TablaCertificados(request));
                        break;
                    case "HistorialRevisionCertificado":
                        out.println(HistorialRevisionCertificado(request)); 
                        break;
                    default:
                        throw new AssertionError();
                }
            }
        } catch (Exception ex) {
            try {
                PrintWriter out = response.getWriter();
                out.println(ex.getMessage());

            } catch (IOException exl) {
                Globales.GuardarLogger(exl.getMessage(), Laboratorio.class
                        .getName());
            }
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        Servidor(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {

        Servidor(request, response);
    }

}
