package DB;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * author leudi
 */
@WebServlet(urlPatterns = {"/Cotizacion"})
public class Cotizacion extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * param request servlet request param response servlet response throws
     * ServletException if a servlet-specific error occurs throws IOException if
     * an I/O error occurs
     */
    String sql;
    String idusuario = "";
    String usuario = "";
    ResultSet datos = null;
    String iplocal = "";
    String url_archivo = Globales.url_archivo;
    String archivoadjunto = null;
    String ArchivoPDF;
    String correoenvio;
    String clavecorreo;

    public String EstadosIngresos() {

        sql = "select 1, 'Ingresos no cotizados ' as descripcion, count(distinct ingreso) as cantidad "
                + "  from remision_detalle where cotizado = 0 and to_char(fechaing,'yyyy') >= '2019' "
                + "  UNION "
                + "  select 2, 'Ingresos no recibidos en laboratorio ó comercial ' as descripcion, count(distinct ingreso) as cantidad  "
                + "  from remision_detalle where recibidolab  = 0 and recibidocome = 0 and to_char(fechaing,'yyyy') >= '2019' "
                + "  UNION "
                + "  select 3, 'Ingresos no reportados en laboratorio o enviado a terceros' as descripcion, count(distinct rd.ingreso) as cantidad "
                + "  from remision_detalle rd inner join ingreso_plantilla ip on ip.ingreso = rd.ingreso "
                + "  where reportado  = 0 and envtercero = 0 and to_char(fechaing,'yyyy') >= '2019' "
                + "  UNION "
                + "  select 4, 'Ingresos sin  certificados' as descripcion, count(distinct rd.ingreso) as cantidad "
                + "  from remision_detalle rd inner join ingreso_plantilla ip on ip.ingreso = rd.ingreso "
                + "  where reportado  <> 2 and certificado = 0 and to_char(fechaing,'yyyy') >= '2019' "
                + "  UNION "
                + "  select 5, 'Ingresos no facturados' as descripcion, count(distinct ingreso) as cantidad "
                + "  from remision_detalle "
                + "  where facturado = 0  and to_char(fechaing,'yyyy') >= '2019' "
                + "  UNION "
                + "  select 6, 'Ingresos sin  devolucion' as descripcion, count(distinct ingreso) as cantidad "
                + "  from remision_detalle where salida  = 0  and to_char(fechaing,'yyyy') >= '2019' "
                + "  UNION "
                + "  select 7, 'Ingresos sin  entregar' as descripcion, count(distinct ingreso) as cantidad "
                + "  from remision_detalle where entregado  = 0 and to_char(fechaing,'yyyy') >= '2019' ORDER BY 1";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->EstadosIngresos");
    }

    public String AlertaComercial() {
        sql = "select r.remision, to_char(fecha, 'yyyy/MM/dd HH24:MI') as fecha, u.nombrecompleto as usuario, "
                + "  c.nombrecompleto || ' (' || c.documento || ')' as cliente, cc.nombres as contacto, "
                + "  cs.direccion || ', ' || ci.descripcion || ' (' ||d.descripcion ||')' as direccion,  "
                + "  cc.telefonos || ' / ' || coalesce(cc.fax,'') as telefonos, r.totales, tiempo(fechaasignacion,now(),3) as tiempo "
                + "  from remision r inner join seguridad.rbac_usuario u on r.idusuario = u.idusu "
                + "		                    inner join clientes c on c.id = r.idcliente "
                + "		                    inner join clientes_sede cs on cs.id = r.idsede "
                + "		                    inner join clientes_contac cc on cc.id = r.idcontacto "
                + "		                    inner join ciudad ci on ci.id = cs.idciudad "
                + "		                    inner join departamento d on d.id = ci.iddepartamento "
                + "		                    inner join remision_detalle rd on rd.idremision = r.id  "
                + "                    inner join equipo e on e.id = rd.idequipo "
                + "		    inner join modelos mo on mo.id = rd.idmodelo "
                + "    where to_char(fecha,'yyyy') >= '2019' and c.habitual = 'NO' and rd.garantia='NO' AND rd.convenio = 0 and rd.salida = 0 AND r.asesor = " + idusuario
                + "    group by r.remision, fecha, u.nombrecompleto,r.recepcion, r.id, cs.direccion, "
                + "    c.documento, c.nombrecompleto, cs.nombre, cc.nombres, ci.descripcion, d.descripcion, cc.telefonos, cc.fax, cc.email, r.totales "
                + "    having max(cotizado) = 0 "
                + "    ORDER BY r.id ";

        String sql2 = "select r.cotizacion, to_char(fecha, 'yyyy/MM/dd HH24:MI') as fecha, c.nombrecompleto || ' (' || c.documento || ')' as cliente, cc.nombres as contacto, "
                + "  cs.direccion || ', ' || ci.descripcion || ' (' ||d.descripcion ||')' as direccion,  "
                + "cc.telefonos || ' / ' || coalesce(cc.fax,'') as telefonos, r.estado, tiempo(fecha,now(),3) as tiempo "
                + "from cotizacion r inner join clientes c on c.id = r.idcliente "
                + " inner join clientes_sede cs on cs.id = r.idsede "
                + " inner join clientes_contac cc on cc.id = r.idcontacto "
                + " inner join ciudad ci on ci.id = cs.idciudad "
                + " inner join departamento d on d.id = ci.iddepartamento "
                + "WHERE coalesce((select count(rd.ingreso) from remision_detalle rd inner join cotizacion_detalle cd on cd.ingreso = rd.ingreso where r.id = cd.idcotizacion and cd.ingreso > 0 and rd.salida=0),1000) > 0 and "
                + "   r.idusuario = " + idusuario + " and c.habitual = 'NO' AND to_char(fecha,'yyyy') >= '2019' and r.estado in ('Cotizado','POR REEMPLAZAR','Cerrado','Temporal') ORDER BY r.id ";

        String sql3 = "SELECT '<a href=''javascript:ModalDetalleEstado(' || rd.ingreso || ')''>' || rd.ingreso || '(' || subString(m.descripcion, 1,3) || ')</a><br>' || cotizacion_ingreso(rd.ingreso) as ingreso, tiempo(ir.fecha,now(),3) as tiempo "
                + "  FROM public.reporte_ingreso ir inner join remision_detalle rd on rd.ingreso = ir.ingreso "
                + "                                 inner join equipo e on e.id = rd.idequipo "
                + "                                 inner join magnitudes m on m.id = e.idmagnitud "
                + "				                               inner join clientes c on c.id = rd.idcliente "
                + "  where asesor = " + idusuario + " and salida = 0 and to_char(fechaing,'yyyy') >= '2019' and c.habitual = 'NO' and rd.cotizado = 0 and rd.salida=0 and rd.garantia='NO' AND rd.convenio = 0 order by rd.ingreso";

        String sql4 = "SELECT '<a href=''javascript:ModalDetalleEstado(' || rd.ingreso || ')''>' || rd.ingreso || '(' || subString(m.descripcion, 1,3) || ')</a><br>' || cotizacion_ingreso(rd.ingreso) as ingreso, tiempo(ir.fecha,now(),3) as tiempo "
                + "  FROM public.reporte_ingreso ir inner join remision_detalle rd on rd.ingreso = ir.ingreso "
                + "                                 inner join equipo e on e.id = rd.idequipo "
                + "                                 inner join magnitudes m on m.id = e.idmagnitud "
                + "				                               inner join clientes c on c.id = rd.idcliente "
                + "  where asesor = " + idusuario + " and salida = 0 and c.habitual = 'NO' AND to_char(fechaing,'yyyy') >= '2019' and rd.cotizado = 1 and rd.orden = 0 and rd.salida=0 and rd.garantia='NO' AND rd.convenio = 0 order by rd.ingreso";

        String sql5 = "SELECT '<a href=''javascript:ModalDetalleEstado(' || rd.ingreso || ')''>' || rd.ingreso || '(' || subString(m.descripcion, 1,3) || ')</a>' as ingreso, tiempo(ir.fecha,now(),3) as tiempo "
                + "  FROM public.reporte_ingreso ir inner join remision_detalle rd on rd.ingreso = ir.ingreso "
                + "                                 inner join equipo e on e.id = rd.idequipo "
                + "                                 inner join magnitudes m on m.id = e.idmagnitud "
                + "				                               inner join clientes c on c.id = rd.idcliente "
                + "				                               INNER JOIN cotizacion_detalle cd on cd.ingreso = rd.ingreso "
                + "				                               inner join cotizacion co on co.id = cd.idcotizacion and co.estado in('Cerrado','Aprobado','Cotizado') "
                + "  where asesor = 8 and salida = 0 and cd.idproveedor > 0 and rd.salidaterce = 0 and rd.salida = 0 and to_char(fechaing,'yyyy') >= '2019' and rd.garantia='NO' AND rd.convenio = 0";
        String sql6 = "SELECT '<a href=''javascript:ModalDetalleEstado(' || rd.ingreso || ')''>' || rd.ingreso || '(' || subString(m.descripcion, 1,3) || ')</a>' as ingreso, tiempo(fechasal,now(),3) as tiempo "
                + " FROM public.reporte_ingreso ir inner join remision_detalle rd on rd.ingreso = ir.ingreso "
                + "                                inner join equipo e on e.id = rd.idequipo "
                + "                                inner join magnitudes m on m.id = e.idmagnitud "
                + "                                inner join salida_detalle sd on sd.ingreso = rd.ingreso "
                + "				                               inner join salida s on s.id = sd.idsalida "
                + "				                               inner join clientes c on c.id = rd.idcliente "
                + "   where asesor = 0 and rd.salida = 0 and rd.salidaterce > 0 and recibidoterc = 0 and rd.salida=0 and to_char(fechaing,'yyyy') >= '2019' and rd.garantia='NO' AND rd.convenio = 0";

        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->EstadosIngresos") + "|" + Globales.ObtenerDatosJSon(sql2, this.getClass() + "-->EstadosIngresos") + "|" + Globales.ObtenerDatosJSon(sql3, this.getClass() + "-->EstadosIngresos")
                + "|" + Globales.ObtenerDatosJSon(sql4, this.getClass() + "-->EstadosIngresos") + "|" + Globales.ObtenerDatosJSon(sql5, this.getClass() + "-->EstadosIngresos") + "|" + Globales.ObtenerDatosJSon(sql6, this.getClass() + "-->EstadosIngresos");

    }

    public String AlertaFacturacion() {

        String alerta = Globales.ObtenerUnValor("SELECT alerta FROM seguridad.rbac_usuario WHERE idusu = " + idusuario);
        if (alerta.equals("5")) {
            sql = "SELECT '<a href=''javascript:ModalDetalleEstado(' || rd.ingreso || ')''>' || rd.ingreso || '(' || subString(m.descripcion, 1,3) || ')</a>' as ingreso, tiempo(rd.fechaing,now(),3) as tiempo "
                    + "      FROM remision_detalle rd inner join equipo e on e.id = rd.idequipo "
                    + "                                     inner join magnitudes m on m.id = e.idmagnitud "
                    + "					                               inner join clientes c on c.id = rd.idcliente "
                    + "      where rd.orden = 1 and salida = 0 and rd.cotizado = 1 and facturado = 0 and rd.garantia='NO' AND rd.convenio = 0 order by rd.ingreso";
            return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->AlertaFacturacion");
        } else {
            return "[]";
        }
    }

    public String AlertaLaboratorio() {
        String magnitudes = Globales.ObtenerUnValor("SELECT magnitud FROM seguridad.rbac_usuario WHERE idusu = " + idusuario);
        String[] sql2 = new String[8];

        sql2[0] = "SELECT '<a href=''javascript:ModalDetalleEstado(' || rd.ingreso || ',' || p.id ||  ')''>' || rd.ingreso || '(' || subString(m.descripcion, 1,3) || ')</a><br>' || cotizacion_ingreso(rd.ingreso) as ingreso, tiempo_aprobacion(rd.ingreso, 0, fechaaproajus, rd.fechaaprocoti,rd.fechaing,now(),3) as tiempo "
                + "FROM public.ingreso_recibidolab ir inner join remision_detalle rd on rd.ingreso = ir.ingreso "
                + "                                   inner join equipo e on e.id = rd.idequipo "
                + "                                   inner join magnitudes m on m.id = e.idmagnitud "
                + "                                   inner join ingreso_plantilla ip on ip.ingreso = rd.ingreso "
                + "				                               inner join plantillas p on p.id = ip.idplantilla "
                + "where reportado = 0 and salida = 0 AND  noautorizado = 0 and recibidoing = 0 and to_char(ir.fecha,'yyyy') >= '2019' and e.idmagnitud in (" + magnitudes + ") and rd.garantia='NO' AND envtercero = 0 and salidaterce = 0 and recibidocome = 0 and convenio = 0 and idcliente <> 11 order by rd.ingreso";

        sql2[1] = "SELECT '<a href=''javascript:ModalDetalleEstado(' || rd.ingreso || ',' || p.id ||  ')''>' || rd.ingreso || '(' || subString(m.descripcion, 1,3) || ')</a>' as ingreso, tiempo_aprobacion(rd.ingreso, 0, fechaaproajus, rd.fechaaprocoti,rd.fechaing,now(),3) as tiempo "
                + "       FROM public.reporte_ingreso ir inner join remision_detalle rd on rd.ingreso = ir.ingreso "
                + "                                      inner join equipo e on e.id = rd.idequipo "
                + "                                      inner join magnitudes m on m.id = e.idmagnitud "
                + "					   inner join ingreso_plantilla ip on ip.ingreso = rd.ingreso "
                + "					   inner join plantillas p on p.id = ip.idplantilla "
                + "       where rd.recibidoing = 0 and certificado = 0 and salida = 0 and to_char(ir.fecha,'yyyy') >= '2019' and informetecnico = 0  and noautorizado = 0 and e.idmagnitud in (" + magnitudes + ") and rd.garantia='NO' AND rd.convenio = 0 order by rd.ingreso";

        sql2[2] = "SELECT '<a href=''javascript:ModalDetalleEstado(' || rd.ingreso || ',' || p.id ||  ')''>' || rd.ingreso || '(' || subString(m.descripcion, 1,3) || ')</a>' as ingreso, tiempo_aprobacion(rd.ingreso, 0, fechaaproajus, rd.fechaaprocoti,rd.fechaing,now(),3) as tiempo "
                + "       FROM public.certificados ir inner join remision_detalle rd on rd.ingreso = ir.ingreso "
                + "                                   inner join equipo e on e.id = rd.idequipo "
                + "                                   inner join magnitudes m on m.id = e.idmagnitud "
                + "				                            inner join ingreso_plantilla ip on ip.ingreso = rd.ingreso "
                + "				                                inner join plantillas p on p.id = ip.idplantilla "
                + "       WHERE rd.recibidoing = 0 and salida = 0 and rd.idcliente <> 11 and ir.estado <> 'Anulado' and to_char(ir.fecha,'yyyy') >= '2019' and e.idmagnitud in (" + magnitudes + ") and rd.garantia='NO' AND envtercero = 0 and salidaterce = 0 and recibidocome = 0 and convenio = 0  order by rd.ingreso";

        sql2[3] = "SELECT '<a href=''javascript:ModalDetalleEstado(' || rd.ingreso || ',' || p.id ||  ')''>' || rd.ingreso || '(' || subString(m.descripcion, 1,3) || ')</a>' as ingreso, tiempo_aprobacion(rd.ingreso, 0, fechaaproajus, rd.fechaaprocoti,rd.fechaing,now(),3) as tiempo "
                + "FROM public.certificados ir inner join remision_detalle rd on rd.ingreso = ir.ingreso "
                + "                                   inner join equipo e on e.id = rd.idequipo "
                + "                                   inner join magnitudes m on m.id = e.idmagnitud "
                + "					                            inner join ingreso_plantilla ip on ip.ingreso = rd.ingreso and ip.idplantilla = ir.item "
                + "				                                inner join plantillas p on p.id = ip.idplantilla "
                + "       WHERE stiker = 0 and salida = 0 and rd.idcliente <> 11 and ir.estado <> 'Anulado' and to_char(ir.fecha,'yyyy') >= '2019' and e.idmagnitud in (" + magnitudes + ") and rd.garantia='NO' AND envtercero = 0 and salidaterce = 0 and recibidocome = 0 and convenio = 0 and idcliente <> 11 order by rd.ingreso";

        sql2[4] = "SELECT DISTINCT '<a href=''javascript:ModalDetalleEstado(' || rd.ingreso || ',' || p.id || ')''>' || rd.ingreso || '(' || subString(m.descripcion, 1,3) || ')</a>' as ingreso, tiempo_aprobacion(rd.ingreso, 0, fechaaproajus, rd.fechaaprocoti,rd.fechaing,now(),3) as tiempo "
                + "FROM public.ingreso_recibidolab ir inner join remision_detalle rd on rd.ingreso = ir.ingreso "
                + "                                           inner join equipo e on e.id = rd.idequipo "
                + "                                           inner join magnitudes m on m.id = e.idmagnitud "
                + "				                                        inner join cotizacion_detalle cd on cd.ingreso = rd.ingreso "
                + "				                                        inner join cotizacion co on co.id = cd.idcotizacion "
                + "				                                        inner join servicio s on s.id = cd.idservicio "
                + "                                           inner join ingreso_plantilla ip on ip.ingreso = rd.ingreso "
                + "				                                        inner join plantillas p on p.id = ip.idplantilla "
                + "       where  ((ip.certificado = 0 AND ip.informetecnico = 0) OR recibidoing = 0) and idserviciodep = 0 and salida = 0 and s.proveedor = 0 and to_char(ir.fecha,'yyyy') >= '2019' and express = 2 and e.idmagnitud in (" + magnitudes + ") and rd.garantia='NO' AND rd.convenio = 0 order by 1";

        sql2[5] = "SELECT '<a href=''javascript:ModalDetalleEstado(' || rd.ingreso || ',' || p.id ||  ')''>' || rd.ingreso || '(' || subString(m.descripcion, 1,3) || ')</a>' as ingreso, tiempo_aprobacion(rd.ingreso, 0, fechaaproajus, rd.fechaaprocoti,rd.fechaing,now(),3) as tiempo "
                + "FROM public.certificados ir inner join remision_detalle rd on rd.ingreso = ir.ingreso "
                + "                                   inner join equipo e on e.id = rd.idequipo "
                + "                                   inner join magnitudes m on m.id = e.idmagnitud "
                + "					                            inner join ingreso_plantilla ip on ip.ingreso = rd.ingreso and ip.idplantilla = ir.item "
                + "				                                inner join plantillas p on p.id = ip.idplantilla "
                + "       WHERE salida = 0 and ir.estado <> 'Anulado' and ir.impreso = 0 and tempmax is not null and to_char(ir.fecha,'yyyy') >= '2019' and e.idmagnitud in (" + magnitudes + ") and envtercero = 0 and salidaterce = 0 and recibidocome = 0 and convenio = 0 and idcliente <> 11 order by rd.ingreso";

        sql2[6] = "SELECT '<a href=''javascript:ModalDetalleEstado(' || rd.ingreso || ',' || p.id ||  ')''>' || rd.ingreso || '(' || subString(m.descripcion, 1,3) || ')</a>' as ingreso, tiempo_aprobacion(rd.ingreso, 0, fechaaproajus, rd.fechaaprocoti,rd.fechaing,now(),3) as tiempo "
                + "FROM public.certificados ir inner join remision_detalle rd on rd.ingreso = ir.ingreso "
                + "                                   inner join equipo e on e.id = rd.idequipo "
                + "                                   inner join magnitudes m on m.id = e.idmagnitud "
                + "					                            inner join ingreso_plantilla ip on ip.ingreso = rd.ingreso and ip.idplantilla = ir.item "
                + "				                                inner join plantillas p on p.id = ip.idplantilla "
                + "WHERE salida = 0 and ir.estado = 'Registrado' and to_char(ir.fecha,'yyyy') >= '2019' and e.idmagnitud in  (" + magnitudes + ") and rd.garantia='NO' and envtercero = 0 and salidaterce = 0 and recibidocome = 0 and convenio = 0 and idcliente <> 11 order by rd.ingreso";

        sql2[7] = "SELECT '<a href=''javascript:ModalDetalleEstado(' || rd.ingreso || ',0)''>' || rd.ingreso || '(' || subString(m.descripcion, 1,3) || ')</a><br>' || cotizacion_ingreso(rd.ingreso) as ingreso, tiempo_aprobacion(rd.ingreso, 0, null, rd.fechaaprocoti,rd.fechaing,now(),3) as tiempo "
                + "FROM remision_detalle rd inner join equipo e on e.id = rd.idequipo "
                + "			     inner join magnitudes m on m.id = e.idmagnitud "
                + " where salida = 0 AND recibidolab = 0 and recibidocome = 0 and to_char(rd.fechaing,'yyyy') >= '2019' and e.idmagnitud in (" + magnitudes + ") and rd.garantia='NO' and envtercero = 0 and salidaterce = 0 and convenio = 0 and idcliente <> 11 order by rd.ingreso";
        String resultado = "{";
        String Tabla = "";
        for (int x = 0; x < sql2.length; x++) {
            if (!resultado.equals("{")) {
                resultado += ",";
            }
            if (x == 0) {
                Tabla = "Table";
            } else {
                Tabla = "Table" + x;
            }
            resultado += "\"" + Tabla + "\":" + Globales.ObtenerDatosJSon(sql2[x], this.getClass() + "-->AlertaLaboratorio");
        }
        resultado += "}";
        return resultado;
    }

    public String AlertaSolicitud() {
        sql = "SELECT solicitud, to_char(fecha,'yyyy/MM/dd HH24:MI') as fecha, s.direccion as direccioncli, contacto, telefonos, c.email, "
                + "      c.nombrecompleto || ' (' || c.documento || ')'  as cliente, sum(sd.cantidad) as cantidad, "
                + "      tiempo(fecha,now(),2) as tiempo "
                + "FROM web.solicitud s INNER JOIN web.solicitud_detalle sd using (solicitud) "
                + "                             INNER JOIN clientes c on c.id = s.idcliente "
                + "                     INNER JOIN seguridad.rbac_usuario u on u.idusu = s.asesor  "
                + " WHERE s.estado = 'Pendiente' and c.asesor = " + idusuario
                + "GROUP BY solicitud, fecha, s.direccion, contacto, telefonos, c.email, "
                + "     s.observacion, c.nombrecompleto, c.documento, s.idusuario, "
                + "     fechaanulado,observacionanula, s.estado,cotizacion, fechacotizado,u.nombrecompleto "
                + "order by solicitud DESC ";

        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->AlertaSolicitud");
    }

    public String AlertaLogistica() {

        String alerta = Globales.ObtenerUnValor("SELECT alerta FROM seguridad.rbac_usuario WHERE idusu = " + idusuario);
        if (alerta.equals("4")) {
            sql = "SELECT '<a href=''javascript:ModalDetalleEstado(' || rd.ingreso || ')''>' || rd.ingreso || '(' || subString(m.descripcion, 1,3) || ')</a>' as ingreso, tiempo(ir.fecha,now(),3) as tiempo "
                    + "      FROM public.ingreso_recibidoing ir inner join remision_detalle rd on rd.ingreso = ir.ingreso "
                    + "                                         inner join equipo e on e.id = rd.idequipo "
                    + "                                         inner join magnitudes m on m.id = e.idmagnitud "
                    + "                                         inner join seguridad.rbac_usuario u on ir.idusuario = u.idusu "
                    + "      where rd.salida = 0  and entregado = 0 and to_char(ir.fecha,'yyyy') >= '2019' order by rd.ingreso";
            String sql2 = "SELECT '<a href=''javascript:ModalDetalleEstado(' || rd.ingreso || ')''>' || rd.ingreso || '(' || subString(m.descripcion, 1,3) || ')</a>' as ingreso, tiempo(ir.fecha,now(),3) as tiempo "
                    + "FROM public.ingreso_recibidoing ir inner join remision_detalle rd on rd.ingreso = ir.ingreso "
                    + "                                   inner join equipo e on e.id = rd.idequipo "
                    + "                                   inner join magnitudes m on m.id = e.idmagnitud "
                    + "                                   inner join seguridad.rbac_usuario u on ir.idusuario = u.idusu "
                    + "where rd.salida = 1 and rd.entregado = 0 and to_char(ir.fecha,'yyyy') >= '2019' order by rd.ingreso";

            return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->AlertaLogistica") + "|" + Globales.ObtenerDatosJSon(sql2, this.getClass() + "-->AlertaLogistica");
        } else {
            return "[]|[]";
        }

    }

    public String AlertaCotizaApro() {

        sql = "select r.cotizacion, to_char(case when aprobada = 1 then fechaaprobacion else fecharechazada end, 'yyyy/MM/dd HH24:MI') as fecha, c.nombrecompleto || ' (' || c.documento || ')' as cliente, cc.nombres as contacto, "
                + "	            cs.direccion || ', ' || ci.descripcion || ' (' ||d.descripcion ||')' as direccion,  "
                + "      cc.telefonos || ' / ' || coalesce(cc.fax,'') as telefonos, r.estado, tiempo(r.fecha,case when aprobada = 1 then fechaaprobacion else fecharechazada end ,3) as tiempo "
                + "  from cotizacion r inner join clientes c on c.id = r.idcliente "
                + "		      inner join clientes_sede cs on cs.id = r.idsede "
                + "		      inner join clientes_contac cc on cc.id = r.idcontacto "
                + "		      inner join ciudad ci on ci.id = cs.idciudad "
                + "		      inner join departamento d on d.id = ci.iddepartamento "
                + "                   left join orden_compra oc on oc.ncotizacion = r.cotizacion "
                + " WHERE coalesce((select count(rd.ingreso) from remision_detalle rd inner join cotizacion_detalle cd on cd.ingreso = rd.ingreso where r.id = cd.idcotizacion and cd.ingreso > 0 and rd.salida=0),1000) > 0 and  "
                + "  r.idusuario = " + idusuario + " and oc.id is null and c.habitual = 'NO' AND to_char(case when aprobada = 1 then fechaaprobacion else fecharechazada end,'yyyy') >= '2019' and r.estado in ('Aprobado','Rechazado') ORDER BY r.id ";

        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->AlertaCotizaApro");
    }

    public String TipoPrecio(HttpServletRequest request) {
        int magnitud = Globales.Validarintnonull(request.getParameter("magnitud"));
        int servicio = Globales.Validarintnonull(request.getParameter("servicio"));

        int TipoPrecio = 0;
        if (magnitud > 0) {
            TipoPrecio = Globales.Validarintnonull(Globales.ObtenerUnValor("select tipoprecio from magnitudes where id = " + magnitud));
        }
        int precio = TipoPrecio;
        int PreProveedor = 0;
        if (servicio > 0) {
            PreProveedor = Globales.Validarintnonull(Globales.ObtenerUnValor("select proveedor from servicio where id = " + servicio));
            if (PreProveedor == 1) {
                TipoPrecio = 3;
            }
        }
        return TipoPrecio + "|" + precio;
    }

    public String DescripcionPrecios() {
        sql = "SELECT id, descripcion from tablaprecios order by id";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->DescripcionPrecios");
    }

    public String ModalClientes(HttpServletRequest request) {
        String Razon = request.getParameter("Razon");
        String Cedula = request.getParameter("Cedula");

        String Busqueda = "WHERE c.id <> 0 and (nombrecompleto ilike '%" + Razon + "%' or coalesce(nombrecomer,'') ilike '%" + Razon + "%')  and documento ilike '%" + Cedula + "%' ";

        sql = "select documento, '<b>' || nombrecompleto || '</b>' || "
                + "  case when coalesce(nombrecomer,'') <> nombrecompleto then '<br>' || coalesce(nombrecomer,'') else '' end as nombrecompleto, telefono, celular, email, direccion, t.descripcion as tipocliente, ci.descripcion as ciudad"
                + " from Clientes c inner join TipoCliente t ON t.id = c.idtipcli "
                + "              inner join ciudad ci on c.idciudad = ci.id " + Busqueda + " ORDER BY 2 ";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ModalClientes");
    }

    public String ModalEmpresa(HttpServletRequest request) {
        int ingreso = Globales.Validarintnonull(request.getParameter("ingreso"));
        int cotizado = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT id from cotizacion_detalle where idcotizacion > 0 and ingreso=" + ingreso));
        if (cotizado == 0) {
            sql = "select c.nombrecompleto as cliente, c.documento, cc.nombres as contacto, cc.email, cc.telefonos, cc.fax, r.remision, ci.descripcion as ciudad, d.descripcion as departamento, "
                    + "      to_char(r.fecha, 'dd/MM/yyyy HH24:MI') as fecha, r.remision, u.nombrecompleto as usuario, ua.nombrecompleto as asesor, "
                    + "      trim(to_char(rd.ingreso,'0000000')) as ingreso, cs.direccion || ', ' || ci.descripcion || ' (' ||d.descripcion ||'), ' || pa.descripcion as direccion, cs.nombre as sede, "
                    + "  nombreca, cd.direccion as direccionca, habitual, aprobar_cotizacion, aprobar_ajuste "
                    + " from remision r inner join remision_detalle rd on rd.idremision = r.id "
                    + "	                        inner join clientes  c on r.idcliente = c.id "
                    + "	                        inner join clientes_contac cc on r.idcontacto = cc.id "
                    + "	                        inner join clientes_sede cs on cs.id = r.idsede "
                    + "	                        inner join ciudad ci on ci.id = cs.idciudad "
                    + "	                        inner join departamento d on d.id = ci.iddepartamento "
                    + "	                        inner join pais pa on pa.id = d.idpais "
                    + "              inner join seguridad.rbac_usuario u on r.idusuario = u.idusu "
                    + "              inner join seguridad.rbac_usuario ua on c.asesor = ua.idusu "
                    + "              left join cotizacion_detalle cd on cd.ingreso = rd.ingreso "
                    + " where rd.ingreso = " + ingreso + " ORDER BY cd.id DESC LIMIT 1";
        } else {
            sql = "select c.nombrecompleto as cliente, c.documento, cc.nombres as contacto, cc.email, cc.telefonos, cc.fax, r.remision, ci.descripcion as ciudad, d.descripcion as departamento, "
                    + "      to_char(r.fecha, 'dd/MM/yyyy HH24:MI') as fecha, r.remision, u.nombrecompleto as usuario, ua.nombrecompleto as asesor, "
                    + "      trim(to_char(rd.ingreso,'0000000')) as ingreso, cs.direccion || ', ' || ci.descripcion || ' (' ||d.descripcion ||'), ' || pa.descripcion as direccion, cs.nombre as sede, "
                    + "  nombreca, cd.direccion as direccionca, habitual, aprobar_cotizacion, aprobar_ajuste "
                    + " from remision r inner join remision_detalle rd on rd.idremision = r.id "
                    + "              inner join cotizacion_detalle cd on cd.ingreso = rd.ingreso "
                    + "              inner join cotizacion co on co.id = cd.idcotizacion "
                    + "	                        inner join clientes  c on r.idcliente = c.id "
                    + "	                        inner join clientes_contac cc on co.idcontacto = cc.id "
                    + "	                        inner join clientes_sede cs on cs.id = co.idsede "
                    + "	                        inner join ciudad ci on ci.id = cs.idciudad "
                    + "	                        inner join departamento d on d.id = ci.iddepartamento "
                    + "	                        inner join pais pa on pa.id = d.idpais "
                    + "              inner join seguridad.rbac_usuario ua on c.asesor = ua.idusu "
                    + "              inner join seguridad.rbac_usuario u on r.idusuario = u.idusu "
                    + " where rd.ingreso = " + ingreso + " and co.estado <> 'Anulado' and cd.idserviciodep = 0 ORDER BY cd.id DESC LIMIT 1";
        }

        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ModalEmpresa");
    }

    public String BuscarCliente(HttpServletRequest request) {
        try {
            String documento = request.getParameter("documento");
            int bloquear = Globales.Validarintnonull(request.getParameter("bloquear"));

            String bloqueado = "";

            String Busqueda = "WHERE documento = '" + documento + "' ";

            sql = "select c.*, t.descripcion as tipocliente, p.descripcion as precio, p.id as idprecio, iv.valor, "
                    + "ci.descripcion as ciudad, d.descripcion as departamento, riva.porcentaje as porreteiva, rica.porcentaje as porreteica, "
                    + "    rfuente.porcentaje as porretefuente "
                    + "from Clientes c inner join TipoCliente t ON t.id = c.idtipcli "
                    + "               inner join ciudad ci on ci.id = c.idciudad "
                    + "              inner join departamento d on d.id = ci.iddepartamento "
                    + "             inner join iva iv on iv.id = c.idiva  "
                    + "            INNER JOIN tablaprecios p ON p.id = c.tablaprecio  "
                    + "           inner join contabilidad.retenciones riva on riva.id = reteiva "
                    + "          inner join contabilidad.retenciones rica on rica.id = reteica "
                    + "         inner join contabilidad.retenciones rfuente on rfuente.id = retefuente " + Busqueda + " ORDER BY 2 ";

            datos = Globales.Obtenerdatos(sql, this.getClass() + "-->BuscarCliente", 1);

            if (datos.next()) {
                if (bloquear != 0) {
                    bloqueado = datos.getString("bloqueado").trim();
                    if (!idusuario.equals(datos.getString("asesor").trim())) {
                        if (Globales.PermisosSistemas("COTIZACION COTIZAR CUALQUIER CLIENTE", idusuario) == 0) {
                            return "5|Usted no es el asesor comercial de este cliente... Consulte con su Director Comercial";
                        }
                    }

                    if (!bloqueado.equals("") && !bloqueado.equals(usuario)) {
                        return "8|Cliente " + datos.getString("documento") + " " + datos.getString("nombrecompleto") + " bloqueado por el usuario <b>" + bloqueado + "</b>";
                    }

                    String sql4 = "UPDATE clientes SET bloqueado = '" + usuario + "' WHERE documento = '" + documento + "'";
                    Globales.Obtenerdatos(sql4, this.getClass() + "-->BuscarCliente", 2);
                }

                String sql2 = "SELECT id, Nombres FROM clientes_contac where idcliente = " + datos.getString("id") + " ORDER BY 2";
                String sql3 = "SELECT cs.id, cs.direccion || ', ' || c.descripcion || ' (' ||d.descripcion ||'), ' || nombre as nombre "
                        + "FROM clientes_sede cs inner join ciudad c on c.id = cs.idciudad "
                        + "inner join departamento d on d.id = c.iddepartamento  "
                        + "where cs.idcliente = " + datos.getString("id") + " ORDER BY 2";

                return "0|" + Globales.ObtenerDatosJSon(sql, this.getClass() + "-->BuscarCliente") + "|" + Globales.ObtenerCombo(sql2, 1, 0, 0) + "|" + Globales.ObtenerCombo(sql3, 1, 0, 0);
            } else {
                return "99|";
            }
        } catch (SQLException ex) {
            return "1|" + ex.getMessage();
        }

    }

    public String DesbloquearCliente(HttpServletRequest request) {

        String documento = request.getParameter("documento");
        sql = "SELECT nombrecompleto  FROM clientes where documento = trim('" + documento + "')";
        String cliente = Globales.ObtenerUnValor(sql);
        if (cliente.equals("")) {
            return "1|El documento número " + documento + " NO pertenece a ningún cliente registrado";
        }

        sql = "UPDATE clientes SET bloqueado = '' WHERE documento = '" + documento + "'";
        if (Globales.DatosAuditoria(sql, "CLIENTES", "DESBLOQUEAR CLIENTE", idusuario, iplocal, this.getClass() + "-->DesbloquearCliente")) {
            return "0|Cliente " + cliente + " Desbloqueado";
        } else {
            return "1|Error inesperado... Revise el log de errores";
        }
    }

    public String TemporalIngresoCotiz(HttpServletRequest request) {

        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        int remision = Globales.Validarintnonull(request.getParameter("remision"));
        int buscar = Globales.Validarintnonull(request.getParameter("buscar"));
        int cotizacion = Globales.Validarintnonull(request.getParameter("cotizacion"));
        int sede = Globales.Validarintnonull(request.getParameter("sede"));
        int contacto = Globales.Validarintnonull(request.getParameter("contacto"));
        int numero = Globales.Validarintnonull(request.getParameter("numero"));
        try {

            if (remision > 0) {
                if (Globales.PermisosSistemas("COTIZACION REALIZAR COTIZACION DE REMISION NO ASIGNADA", idusuario) == 0) {
                    String asesor = Globales.ObtenerUnValor("select asesor from remision WHERE id = " + remision);
                    String asesorcliente = Globales.ObtenerUnValor("select asesor from clientes WHERE id = " + cliente);
                    if (!idusuario.equals(asesorcliente)) {
                        if (!idusuario.equals(asesor)) {
                            return "1|Usted no tiene asignada esta remisión... Consulte con su director comercial";
                        }
                    }
                }
            }

            if (cotizacion == -1) {
                if (buscar == 0) {
                    int id = Globales.Validarintnonull(Globales.ObtenerUnValor("select id from cotizacion_detalle WHERE idcliente = " + cliente + " and idcotizacion = 0"));
                    if (id > 0) {
                        return "9|";
                    } else {
                        buscar = 1;
                    }
                }
                if (buscar == 1) {
                    sql += "DELETE FROM cotizacion_detalle where idcotizacion = 0 and idcliente = " + cliente + ";";
                    sql += "UPDATE remision_detalle rd set estado = 'Cerrado' "
                            + "from cotizacion_detalle cd "
                            + "where rd.ingreso = cd.ingreso and cd.idcliente = " + cliente + " and idcotizacion = 0 and rd.estado = 'EN COTIZACION';";
                    sql += "UPDATE remision r  set estado = 'Cerrado' "
                            + "from cotizacion_detalle cd inner join remision_detalle rd on rd.ingreso = cd.ingreso "
                            + "where r.id = rd.idremision and cd.idcliente = " + cliente + " and idcotizacion = 0;";

                    Globales.Obtenerdatos(sql, this.getClass() + "-->TemporalIngresoCotiz", 2);
                    int idcotizacion = 0;
                    int idconcepto = 0;
                    int ingreso = 0;
                    String pagoinforme = "";

                    sql = "SELECT ri.id, idconcepto, concepto, ajuste, suministro, ri.observacion, paginformetecnico, ri.cotizado, nroreporte, rd.ingreso, ri.estado "
                            + "FROM remision_detalle rd INNER JOIN clientes c on c.id = rd.idcliente  left join reporte_ingreso ri on rd.ingreso = ri.ingreso "
                            + "where idremision = " + remision + " and rd.ncotizacion = 0 order by idconcepto";
                    datos = Globales.Obtenerdatos(sql, this.getClass() + "-->TemporalIngresoCotiz", 1);

                    while (datos.next()) {
                        idconcepto = Globales.Validarintnonull(datos.getString("idconcepto"));
                        ingreso = Globales.Validarintnonull(datos.getString("ingreso"));
                        pagoinforme = datos.getString("paginformetecnico");

                        sql = "INSERT INTO cotizacion_detalle(ingreso, idequipo, idmodelo, idintervalo,idintervalo2, cantidad, serie, idcliente, idusuario, idsede, idcontacto, remision) "
                                + " SELECT ingreso, idequipo, idmodelo, idintervalo, idintervalo2, cantidad, serie, idcliente, " + idusuario + "," + sede + "," + contacto + "," + numero
                                + " FROM remision_detalle "
                                + " WHERE ingreso =  " + ingreso + "and idcliente = " + cliente
                                + " and ingreso not in (SELECT ingreso FROM cotizacion_detalle WHERE idcliente = " + cliente + " and idcotizacion = 0);";

                        sql += "SELECT id from cotizacion_detalle WHERE ingreso = " + ingreso + " and idcotizacion = 0 and idcliente = " + cliente;
                        int id = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));

                        if (idconcepto != 1 && idconcepto != 4 && idconcepto != 0) {
                            if (!datos.getString("ajuste").trim().equals("")) {
                                sql = "INSERT INTO cotizacion_detalle(ingreso, idequipo, idmodelo, idintervalo, idintervalo2, cantidad, serie, idservicio, idserviciodep, idcliente, idusuario, idsede, idcontacto, remision, observacion, iva, inicial) "
                                        + " SELECT " + datos.getString("ingreso") + ", idequipo, idmodelo, idintervalo, idintervalo2, 1, serie, 6, " + id + ", idcliente," + idusuario + ", idsede, idcontacto, remision, '" + datos.getString("ajuste") + "', 19,1  "
                                        + " FROM cotizacion_detalle "
                                        + " WHERE id = " + id + ";select max(id) from cotizacion_detalle;";
                                idcotizacion = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
                                sql = "UPDATE reporte_ingreso set cotizado = " + idcotizacion + " WHERE id = " + datos.getString("id") + ";";
                                Globales.Obtenerdatos(sql, this.getClass() + "-->TemporalIngresoCotiz", 2);
                            }
                            if (!datos.getString("suministro").trim().equals("")) {
                                sql = "INSERT INTO cotizacion_detalle(ingreso, idequipo, idmodelo, idintervalo, idintervalo2, cantidad, serie, idservicio, idserviciodep, idcliente, idusuario, idsede, idcontacto, remision, observacion, iva,inicial) "
                                        + " SELECT " + ingreso + ", idequipo, idmodelo, idintervalo, idintervalo2, 1, serie, 4, " + id + ", idcliente," + idusuario + ", idsede, idcontacto, remision, '" + datos.getString("suministro") + "', 19,1 "
                                        + " FROM cotizacion_detalle "
                                        + " WHERE id = " + id + ";select max(id) from cotizacion_detalle;";
                                idcotizacion = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
                                sql = "UPDATE reporte_ingreso set cotizado = " + idcotizacion + " WHERE id = " + datos.getString("id") + ";";
                                Globales.Obtenerdatos(sql, this.getClass() + "-->TemporalIngresoCotiz", 2);
                            }
                            if (datos.getString("idconcepto").trim().equals("3") && !datos.getString("estado").equals("Anulado")) {
                                if (pagoinforme.equals("SI")) {
                                    sql = "INSERT INTO cotizacion_detalle(ingreso, idequipo, idmodelo, idintervalo,idintervalo2, cantidad, serie, idservicio, idserviciodep, idcliente, idusuario, idsede, idcontacto, remision, observacion, iva, inicial) "
                                            + "SELECT " + ingreso + ", idequipo, idmodelo, idintervalo, idintervalo2, 1, serie, 9, " + id + ", idcliente," + idusuario + ", idsede, idcontacto, remision, '" + datos.getString("observacion") + "', 19, 1  "
                                            + "FROM cotizacion_detalle "
                                            + "WHERE id = " + id + ";select max(id) from cotizacion_detalle;";
                                    idcotizacion = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
                                    sql = "UPDATE reporte_ingreso set cotizado = " + idcotizacion + " WHERE id = " + datos.getString("id") + ";";
                                    sql += "DELETE FROM cotizacion_detalle WHERE id = " + id + ";";
                                    sql += "DELETE FROM cotizacion_detalle WHERE ingreso = " + ingreso + " and idserviciodep = " + id + " and idservicio <> 9 and idcotizacion = " + cotizacion + ";";
                                    sql += "UPDATE ingreso_plantilla set calibracion = 2 WHERE ingreso = " + ingreso + ";";
                                } else {
                                    sql = "DELETE FROM cotizacion_detalle WHERE ingreso = " + ingreso + " and idserviciodep = " + id + " and idcotizacion = " + cotizacion + ";";
                                }

                                Globales.Obtenerdatos(sql, this.getClass() + "-->TemporalIngresoCotiz", 2);
                            }

                            if (datos.getString("idconcepto").trim().equals("5")) {
                                sql = "INSERT INTO cotizacion_detalle(ingreso, idequipo, idmodelo, idintervalo, idintervalo2, cantidad, serie, idservicio, idserviciodep, idcliente, idusuario, idsede, idcontacto, remision, observacion, iva, inicial) "
                                        + " SELECT " + ingreso + ", idequipo, idmodelo, idintervalo, idintervalo2, 1, serie, 12, " + id + ", idcliente," + idusuario + ", idsede, idcontacto, remision, '" + datos.getString("observacion") + "', 19, 1 "
                                        + " FROM cotizacion_detalle"
                                        + " WHERE id = " + id + ";select max(id) from cotizacion_detalle;";
                                idcotizacion = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
                                sql = "UPDATE reporte_ingreso set cotizado = " + idcotizacion + " WHERE id = " + datos.getString("id") + ";";
                                Globales.Obtenerdatos(sql, this.getClass() + "-->TemporalIngresoCotiz", 2);
                            }
                        }
                    }

                    //se agregar los ingreso con el segundo intervalo
                    /*sql = "SELECT ri.id, idconcepto, concepto, ajuste, suministro, ri.observacion, ri.cotizado, nroreporte, rd.ingreso, ri.intervalo
                     FROM remision_detalle rd left join reporte_ingreso ri on rd.ingreso = ri.ingreso  
                     where idremision = " + remision + " and rd.idintervalo2 > 0 and  coalesce(rd.estado,'Cerrado') in ('Cerrado','Ingresado')";
                     datos = Globales.Obtenerdatos(sql, conexion);
                     if (datos.Rows.Count > 0)
                     {
                     foreach (DataRow row in datos.Rows)
                     {
                     idconcepto = Globales.Validarintnonull(datos.getString("idconcepto"));
                     ingreso = Globales.Validarintnonull(datos.getString("ingreso"));
                     int intervalo = Globales.Validarintnonull(datos.getString("intervalo"));
                     sql = "INSERT INTO cotizacion_detalle(ingreso, idequipo, idmodelo, idintervalo, cantidad, serie, idcliente, idusuario, idsede, idcontacto, remision)
                     SELECT ingreso, idequipo, idmodelo, idintervalo2, cantidad, serie, idcliente, " + idusuario + "," + sede + "," + contacto + "," + numero + "  
                     FROM remision_detalle 
                     WHERE ingreso =  " + ingreso + "and idcliente = " + cliente +";";

                     sql += "SELECT id from cotizacion_detalle WHERE ingreso = " + ingreso + " and idcotizacion = 0 and idcliente = " + cliente;
                     int id = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
                     if (idconcepto != 1 && idconcepto != 4 && idconcepto != 0 && intervalo == 2)
                     {
                     if (datos.getString("ajuste").trim() != "")
                     {
                     sql = "INSERT INTO cotizacion_detalle(ingreso, idequipo, idmodelo, idintervalo, cantidad, serie, idservicio, idserviciodep, idcliente, idusuario, idsede, idcontacto, remision, observacion, iva, inicial)
                     SELECT " + datos.getString("ingreso") + ", idequipo, idmodelo, idintervalo2, 1, serie, 6, " + id + ", idcliente," + idusuario + ", idsede, idcontacto, remision, '" + datos.getString("ajuste") + "', 19,1 
                     FROM cotizacion_detalle
                     WHERE id = " + id + ";select max(id) from cotizacion_detalle;";
                     idcotizacion = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
                     sql = "UPDATE reporte_ingreso set cotizado = " + idcotizacion + " WHERE id = " + datos.getString("id") + ";";
                     Globales.Obtenerdatos(sql, conexion);
                     }
                     if (datos.getString("suministro").trim() != "")
                     {
                     sql = "INSERT INTO cotizacion_detalle(idcotizacion, ingreso, idequipo, idmodelo, idintervalo, cantidad, serie, idservicio, idserviciodep, idcliente, idusuario, idsede, idcontacto, remision, observacion, iva,inicial)
                     SELECT " + idcotizacion + "," + ingreso + ", idequipo, idmodelo, idintervalo2, 1, serie, 4, " + id + ", idcliente," + idusuario + ", idsede, idcontacto, remision, '" + datos.getString("suministro") + "', 19,1 
                     FROM cotizacion_detalle
                     WHERE id = " + id + ";select max(id) from cotizacion_detalle;";
                     idcotizacion = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
                     sql = "UPDATE reporte_ingreso set cotizado = " + idcotizacion + " WHERE id = " + datos.getString("id") + ";";
                     Globales.Obtenerdatos(sql, conexion);
                     }
                     if (datos.getString("idconcepto") == "3")
                     {
                     sql = "INSERT INTO cotizacion_detalle(idcotizacion, ingreso, idequipo, idmodelo, idintervalo, cantidad, serie, idservicio, idserviciodep, idcliente, idusuario, idsede, idcontacto, remision, observacion, iva, inicial)
                     SELECT " + idcotizacion + "," + ingreso + ", idequipo, idmodelo, idintervalo2, 1, serie, 9, " + id + ", idcliente," + idusuario + ", idsede, idcontacto, remision, '" + datos.getString("observacion") + "', 19, 1 
                     FROM cotizacion_detalle
                     WHERE id = " + id + ";select max(id) from cotizacion_detalle;";
                     idcotizacion = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
                     sql = "UPDATE reporte_ingreso set cotizado = " + idcotizacion + " WHERE id = " + datos.getString("id") + ";";
                     sql += "DELETE FROM cotizacion_detalle WHERE id = " + id + ";";
                     sql += "DELETE FROM cotizacion_detalle WHERE ingreso = " + ingreso + " and idserviciodep = " + id + " and idservicio <> 9;";
                     sql += "UPDATE remision_detalle set calibracion = 2 WHERE ingreso = " + ingreso + ";";
                     Globales.Obtenerdatos(sql, conexion);
                     }

                     if (datos.getString("idconcepto") == "5")
                     {
                     sql = "INSERT INTO cotizacion_detalle(idcotizacion, ingreso, idequipo, idmodelo, idintervalo, cantidad, serie, idservicio, idserviciodep, idcliente, idusuario, idsede, idcontacto, remision, observacion, iva, inicial)
                     SELECT " + idcotizacion + "," + ingreso + ", idequipo, idmodelo, idintervalo2, 1, serie, 12, " + id + ", idcliente," + idusuario + ", idsede, idcontacto, remision, '" + datos.getString("observacion") + "', 19, 1 
                     FROM cotizacion_detalle
                     WHERE id = " + id + ";select max(id) from cotizacion_detalle;";
                     idcotizacion = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
                     sql = "UPDATE reporte_ingreso set cotizado = " + idcotizacion + " WHERE id = " + datos.getString("id") + ";";
                     Globales.Obtenerdatos(sql, conexion);
                     }
                     }
                     }
                     }*/
                    sql = "UPDATE remision set estado = 'EN COTIZACION' WHERE id = " + remision + ";";
                    sql += "UPDATE remision_detalle set estado = 'EN COTIZACION' WHERE idremision = " + remision;
                    Globales.Obtenerdatos(sql, this.getClass() + "-->TemporalIngresoCotiz", 2);
                }
            }

            if (cotizacion == -1) {
                cotizacion = 0;
            }

            sql = "select orden, max(r.id) as id, r.lote, m.descripcion as magnitud, e.descripcion as equipo, ma.descripcion as marca, mo.descripcion as modelo, "
                    + " case when r.idintervalo = 0 then 'VER ESPECIFICACIONES' ELSE '(' || mi.desde || ' a ' || mi.hasta || ') ' || mi.medida end || case when r.idintervalo2 = 0 then '' ELSE '<br>(' || mi2.desde || ' a ' || mi2.hasta || ') ' || mi2.medida END as Intervalos, r.observacion, sum(r.cantidad) as cantidad, r.serie,u.nombrecompleto as usuario,r.estado, "
                    + " r.idintervalo, e.id as idequipo,  ma.id as idmarca, mo.id as idmodelo, r.ingreso, idserviciodep, to_char(fechareg,'dd/MM/yy') as fechaing,  "
                    + " r.idservicio, r.precio + precioadic as precio, r.descuento, r.punto, me.descripcion as metodo, r.direccion, r.certificado, r.proxima, r.declaracion, s.nombre as servicio, r.idcontacto, r.idsede, remision, entrega, p.nombrecompleto as proveedor, iva, (select max(fotos) from remision_detalle rd where rd.ingreso = r.ingreso) as fotos, r.nombreca, me.id as idmetodo,"
                    + " so.solicitud, to_char(so.fecha,'yyyy-MM-dd HH24:MI') as fechasoli, "
                    + " '<b>Acreditado:</b> ' || sd.acreditado || '<br><b>Sitio:</b> ' || sd.sitio || '<br><b>Express:</b> ' || sd.express || '<br><b>SubContratado:</b> ' || sd.subcontratado as serviciosoli, "
                    + " sd.id as idsolicitud, s.otro, (select concepto || case when coalesce(ajuste,'') <> '' then '<br><b>Ajuste:</b> ' || ajuste else '' end "
                    + " || case when coalesce(suministro,'') <> '' then '<br><b>Suministro:</b> ' || suministro else '' end "
                    + " || case when coalesce(observacion,'') <> '' then '<br><b>Observacion:</b> ' || observacion else '' end from reporte_ingreso ri WHERE ri.ingreso = r.ingreso and ri.estado <> 'Anulado' and r.ingreso > 0 order by ri.id DESC LIMIT 1) as reporte, calibracionadic "
                    + "from cotizacion_detalle r inner join equipo e on r.idequipo = e.id  "
                    + "inner join magnitudes m on m.id = e.idmagnitud "
                    + "inner join seguridad.rbac_usuario u on u.idusu = r.idusuario "
                    + "inner join magnitud_intervalos mi on mi.id = r.idintervalo "
                    + "inner join magnitud_intervalos mi2 on mi2.id = r.idintervalo2 "
                    + "left join web.solicitud_detalle sd on sd.id = r.idsolicitud "
                    + "left join web.solicitud so on so.solicitud = sd.solicitud "
                    + "left join metodo me on me.id = r.metodo "
                    + "left join servicio s on s.id = r.idservicio "
                    + "left join modelos mo on mo.id = r.idmodelo  "
                    + "left join marcas ma on ma.id = mo.idmarca "
                    + "left join proveedores p on p.id = r.idproveedor    "
                    + "where r.idcliente = " + cliente + " and idserviciodep = 0 and  idcotizacion = " + cotizacion
                    + "GROUP BY orden, m.descripcion, r.metodo, e.descripcion, ma.descripcion, mo.descripcion, r.idintervalo, mi.medida, mi.desde, mi.hasta, mi2.medida, mi2.desde, mi2.hasta, r.observacion, r.serie,u.nombrecompleto, r.estado,  "
                    + "r.idintervalo2, r.idintervalo, e.id,  ma.id, mo.id, r.ingreso, idserviciodep, fechareg,   "
                    + "r.idservicio, r.precio, r.descuento, r.punto, me.descripcion, r.direccion, r.certificado, r.proxima, r.declaracion,  s.nombre, r.idcontacto, r.idsede, remision, entrega, p.nombrecompleto, iva, lote,  "
                    + "r.nombreca, me.id, so.solicitud, so.fecha, sd.sitio, sd.express, sd.subcontratado, sd.id, s.otro, calibracionadic, precioadic";
            sql += " UNION ";
            sql += "select orden, r.id, lote, m.descripcion as magnitud, e.descripcion as equipo, ma.descripcion as marca, mo.descripcion as modelo,  "
                    + " case when r.idintervalo = 0 then 'VER ESPECIFICACIONES' ELSE '(' || mi.desde || ' a ' || mi.hasta || ') ' || mi.medida end || case when r.idintervalo2 = 0 then '' ELSE '<br>(' || mi2.desde || ' a ' || mi2.hasta || ') ' || mi2.medida END as Intervalos, observacion, r.cantidad, r.serie,u.nombrecompleto as usuario,r.estado, "
                    + " r.idintervalo, e.id as idequipo,  ma.id as idmarca, mo.id as idmodelo, ingreso, idserviciodep, to_char(fechareg,'dd/MM/yy') as fechaing,  "
                    + " r.idservicio, r.precio, r.descuento, null as punto, null as metodo, null as direccion, null as certificado, null as proxima, null as declaracion, s.descripcion as servicio, idcontacto, idsede, remision, entrega, null as proveedor, iva, (select max(fotos) from remision_detalle rd where rd.ingreso = r.ingreso) as fotos, nombreca,0 as idmetodo, "
                    + " 0 as solicitud, '' as fechasoli, '' as serviciosoli, 0 as idsolicitud, 0 as otro, "
                    + " (select concepto || case when coalesce(ajuste,'') <> '' then '<br><b>Ajuste:</b> ' || ajuste else '' end "
                    + "                 || case when coalesce(suministro,'') <> '' then '<br><b>Suministro:</b> ' || suministro else '' end "
                    + "                 || case when coalesce(observacion,'') <> '' then '<br><b>Observacion:</b> ' || observacion else '' end from reporte_ingreso ri WHERE ri.ingreso = r.ingreso and r.ingreso > 0 order by ri.id DESC LIMIT 1) as reporte, '' as calibracionadic "
                    + "from cotizacion_detalle r inner join equipo e on r.idequipo = e.id "
                    + "				                     inner join magnitudes m on m.id = e.idmagnitud "
                    + "                            inner join seguridad.rbac_usuario u on u.idusu = r.idusuario "
                    + "                           inner join otros_servicios s on s.id = r.idservicio "
                    + "inner join magnitud_intervalos mi on mi.id = r.idintervalo "
                    + "inner join magnitud_intervalos mi2 on mi2.id = r.idintervalo2 "
                    + "                     left join modelos mo on mo.id = r.idmodelo   "
                    + "                    left join marcas ma on ma.id = mo.idmarca  "
                    + "where idcliente = " + cliente + " and idserviciodep > 0 and  idcotizacion = " + cotizacion + " order by orden, id";
            return "0|" + Globales.ObtenerDatosJSon(sql, this.getClass() + "-->TemporalIngresoCotiz");
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String ConsultarCotizaciones(HttpServletRequest request) {
        int remision = Globales.Validarintnonull(request.getParameter("remision"));
        int cotizacion = Globales.Validarintnonull(request.getParameter("cotizacion"));
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        int ingreso = Globales.Validarintnonull(request.getParameter("ingreso"));
        String fechad = request.getParameter("fechad");
        String fechah = request.getParameter("fechah");

        int magnitud = Globales.Validarintnonull(request.getParameter("magnitud"));
        String equipo = request.getParameter("equipo");
        int marca = Globales.Validarintnonull(request.getParameter("marca"));
        String modelo = request.getParameter("modelo");
        String intervalo = request.getParameter("intervalo");
        String serie = request.getParameter("serie");
        int pais = Globales.Validarintnonull(request.getParameter("pais"));
        int departamento = Globales.Validarintnonull(request.getParameter("departamento"));

        int ciudad = Globales.Validarintnonull(request.getParameter("ciudad"));
        int usuarios = Globales.Validarintnonull(request.getParameter("usario"));
        int metodo = Globales.Validarintnonull(request.getParameter("metodo"));
        int servicio = Globales.Validarintnonull(request.getParameter("servicio"));
        int solicitud = Globales.Validarintnonull(request.getParameter("solicitud"));
        String estado = request.getParameter("estado");
        String cingreso = request.getParameter("cingreso");

        String busqueda = "WHERE 1 = 1 ";
        if (remision > 0) {
            busqueda += " AND cd.remision = " + remision;
        } else if (cotizacion > 0) {
            busqueda += " AND c.cotizacion = " + cotizacion;
        } else if (ingreso > 0) {
            busqueda += " AND cd.ingreso = " + ingreso;
        } else {
            busqueda += " and to_char(c.fecha,'yyyy-MM-dd') >= '" + fechad + "' and to_char(c.fecha,'yyyy-MM-dd') <= '" + fechah + "' ";
            if (solicitud > 0) {
                busqueda += " AND c.solicitud = " + solicitud;
            }
            if (cliente > 0) {
                busqueda += " AND c.idcliente = " + cliente;
            }
            if (magnitud > 0) {
                busqueda += " AND e.idmagnitud = " + magnitud;
            }
            if (metodo > 0) {
                busqueda += " AND cd.metodo = " + metodo;
            }
            if (servicio > 0) {
                busqueda += " AND cd.idservicio = " + servicio + " and idserviciodep = 0 ";
            }
            if (!equipo.equals("")) {
                busqueda += " AND e.descripcion = '" + equipo + "'";
            }
            if (marca > 0) {
                busqueda += " AND mo.idmarca = " + marca;
            }
            if (!modelo.equals("")) {
                busqueda += " AND mo.descripcion = '" + modelo + "'";
            }
            if (!intervalo.equals("")) {
                busqueda += " AND '(' || desde || ' a ' || hasta || ') ' || medida = '" + intervalo + "'";
            }
            if (!serie.trim().equals("")) {
                busqueda += " AND cd.serie = '" + serie.trim() + "'";
            }
            if (pais > 0) {
                busqueda += " AND d.idpais = " + pais;
            }
            if (departamento > 0) {
                busqueda += " AND ci.iddepartamento = " + departamento;
            }
            if (ciudad > 0) {
                busqueda += " AND ci.id = " + ciudad;
            }
            if (usuarios > 0) {
                busqueda += " AND c.idusuario = " + usuarios;
            }
            if (!estado.equals("Todos")) {
                busqueda += " AND c.estado = '" + estado + "'";
            }
            if (!cingreso.equals("T")) {
                if (!cingreso.equals("SI")) {
                    busqueda += " AND cd.ingreso > 0";
                } else {
                    busqueda += " AND cd.ingreso = 0 and cd.idequipo = 0 ";
                }
            }
        }

        sql = "select cotizacion, solicitud, c.estado, subtotal, sum(cantidad) as cantidad, sum(case when cd.ingreso > 0 then cantidad else 0 end) as itemingresado, c.descuento, c.iva, c.total, to_char(fecha, 'yyyy/MM/dd HH24:MI') as fecha,\n"
                + "cl.documento, cl.nombrecompleto as cliente, u.nombrecompleto as usuario, ci.descripcion as ciudad, d.descripcion as departamento, cs.nombre as sede, cc.nombres as contacto,\n"
                + "cc.telefonos || ' / ' || coalesce(cc.fax,'') as telefonos, cc.email,\n"
                + "(SELECT ca.usuario || '<br>' || ca.cedula || '<br>' || ca.cargo || '<br>' || ca.observacion \n"
                + "from cotizacion_aprobacion ca where ca.cotizacion = c.cotizacion and ca.estado='Aprobado') as aprobado,\n"
                + "(SELECT cr.usuario || '<br>' || cr.cedula || '<br>' || cr.cargo || '<br>' || cr.observacion \n"
                + "from cotizacion_aprobacion cr where cr.cotizacion = c.cotizacion and cr.estado<>'Aprobado') as rechazado,\n"
                + "'<b>' || to_char(c.fecenviada,'dd/MM/yyyy HH24:MI') || '</b><br>' || c.correoenviado as envio,\n"
                + "to_char(c.fechaanula,'yyyy/MM/dd HH24:MI')  || '<br>' || ua.nombrecompleto as anula,\n"
                + "(SELECT (select c2.cotizacion from cotizacion c2 where c2.id = idcotreem) || '<br>' || to_char(cre.fecha,'dd/MM/yyyy HH24:MI') || '<br>' || uree.nombrecompleto  || '<br>' || cre.observacion \n"
                + "from cotizacion_reemplazada cre INNER JOIN seguridad.rbac_usuario uree on uree.idusu = cre.idusuario  where idcotorigen = c.id order by cre.id desc limit 1) as reemplazado,\n"
                + "'<button class=''btn btn-glow btn-success imprimir'' title=''Imprimir Cotización'' type=''button'' onclick=' || chr(34) || 'ImprimirCotizacion(' || c.cotizacion || ',0)' ||chr(34) || '><span data-icon=''&#xe0f7;''></span></button>' as imprimir\n"
                + " from cotizacion c inner join cotizacion_detalle cd on cd.idcotizacion = c.id\n"
                + "inner join seguridad.rbac_usuario u on u.idusu = c.idusuario\n"
                + "inner join clientes cl on cl.id = c.idcliente\n"
                + "inner join magnitud_intervalos i on i.id = cd.idintervalo  \n"
                + "INNER JOIN seguridad.rbac_usuario ua on ua.idusu = c.idusuarioanula\n"
                + "INNER JOIN seguridad.rbac_usuario ue on ue.idusu = c.idusuenviada\n"
                + "inner join equipo e on e.id = cd.idequipo\n"
                + "inner join modelos mo on mo.id = cd.idmodelo \n"
                + "left join clientes_sede cs on cs.id = c.idsede\n"
                + "left join clientes_contac cc on cc.id = c.idcontacto\n"
                + "left join ciudad ci on ci.id = cl.idciudad\n"
                + "left join departamento d on d.id = ci.iddepartamento " + busqueda + " \n"
                + "group by c.id, cotizacion, solicitud, c.estado, subtotal, c.descuento, c.iva, c.total, fecha, \n"
                + "cl.documento, cl.nombrecompleto, u.nombrecompleto, ci.descripcion, d.descripcion, cs.nombre, cc.nombres,\n"
                + "cc.telefonos, cc.fax,cc.email, ua.nombrecompleto, c.correoenviado\n"
                + "ORDER BY c.id desc";
        busqueda = " and to_char(c.fecha,'yyyy-MM-dd') >= '" + fechad + "'  and to_char(c.fecha,'yyyy-MM-dd') <= '" + fechah + "' ";

        if (cliente > 0) {
            busqueda += " AND c.idcliente = " + cliente;
        }
        if (usuarios > 0) {
            busqueda += " AND c.idusuario = " + usuario;
        }
        String sql2 = "[]";
        if (remision == 0 && cotizacion == 0 && ingreso == 0) {
            sql2 = "delete from temporal.cotizacion_detalle;";
            sql2 += "INSERT INTO temporal.cotizacion_detalle (ingreso, idcotizacion) "
                    + "select DISTINCT ingreso "
                    + ", idcotizacion from cotizacion_detalle where ingreso > 0;";

            sql2 += "select to_char(c.fecha,'yyyy/MM') as fecha, sum(subtotal-descuento) as cotizado,  "
                    + "sum((select SUM  (fd.subtotal) from temporal.cotizacion_detalle cd inner join factura_datelle fd on fd "
                    + ".ingreso = cd.ingreso  "
                    + "inner join factura f on f "
                    + ".id = fd.idfactura "
                    + "where c "
                    + ".id = cd.idcotizacion and f.estado<> 'Anulado' "
                    + ")) as facturado  "
                    + "from cotizacion c where c.estado in ('Cerrado','Aprobado' "
                    + ",  "
                    + "'POR REEMPLAZAR') and cantequipos > 0" + busqueda + "  "
                    + "    group by to_char(c.fecha,'yyyy/MM' "
                    + ") order by 1";

            return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ConsultarCotizaciones") + "||" + Globales.ObtenerDatosJSon(sql2, this.getClass() + "-->ConsultarCotizaciones");

        } else {
            return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ConsultarCotizaciones")
                    + "||[]";
        }
    }

    public String ConsultarClienteAsesor(HttpServletRequest request) {
        int Cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        int Asesor = Globales.Validarintnonull(request.getParameter("asesor"));
        int Magnitud = Globales.Validarintnonull(request.getParameter("magnitud"));
        String fechad = request.getParameter("fechad");
        String fechah = request.getParameter("fechah");
        String busqueda = " WHERE 1 = 1 ";
        if (Cliente > 0) {
            busqueda += " AND c.id = " + Cliente;
        }
        if (Asesor > 0) {
            busqueda += " AND asesor = " + Asesor;
        }
        String busqueda2 = " AND to_char(fechaing,'yyyy-MM-dd') >= '" + fechad + "' and to_char(fechaing,'yyyy-MM-dd') <= '" + fechah + "'";
        if (Magnitud > 0) {
            busqueda2 += " AND idmagnitud = " + Magnitud;
        }
        sql = "select  row_number() OVER(order by c.id) as fila, c.documento, c.nombrecompleto || '<br><b>(' ||  u.nomusu || ')</b>' as nombrecompleto, coalesce(c.telefono,'') || ' ' || coalesce(c.celular,'') as telefonos, c.email, "
                + "'<a href=''javascript:DetalleCliente(1,' || c.id || ')''>' || (select count(distinct ingreso) FROM remision_detalle rd inner join equipo e on e.id = rd.idequipo where rd.idcliente = c.id " + busqueda2 + ") || '</a>' as equipos,"
                + "'<a href=''javascript:DetalleCliente(2,' || c.id || ')''>' || (select count(distinct ingreso) FROM remision_detalle rd inner join equipo e on e.id = rd.idequipo where rd.idcliente = c.id and ncotizacion > 0 " + busqueda2 + ") || '</a>' as cotizados,"
                + "'<a href=''javascript:DetalleCliente(3,' || c.id || ')''>' || (select count(distinct ingreso) FROM remision_detalle rd inner join equipo e on e.id = rd.idequipo where rd.idcliente = c.id  " + busqueda2 + ") || '</a>' as reportados,"
                + "'<a href=''javascript:DetalleCliente(4,' || c.id || ')''>' || (select count(distinct ingreso) FROM remision_detalle rd inner join equipo e on e.id = rd.idequipo where rd.idcliente = c.id " + busqueda2 + ") || '</a>' as certificado,"
                + "'<a href=''javascript:DetalleCliente(5,' || c.id || ')''>' || (select count(distinct ingreso) FROM remision_detalle rd inner join equipo e on e.id = rd.idequipo where rd.idcliente = c.id and salida <> 0 " + busqueda2 + ") || '</a>' as salida,"
                + "'<a href=''javascript:DetalleCliente(6,' || c.id || ')''>' || (select count(distinct ingreso) FROM remision_detalle rd inner join equipo e on e.id = rd.idequipo where rd.idcliente = c.id and salida = 0 " + busqueda2 + ") || '</a>' as pendientes"
                + " from clientes c inner join seguridad.rbac_usuario u  on c.asesor = u.idusu " + busqueda
                + " ORDER BY nombrecompleto";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ConsultarCotizaciones");
    }

    public String BuscarCotizacion(HttpServletRequest request) {
        int cotizacion = Globales.Validarintnonull(request.getParameter("cotizacion"));
        sql = "select co.id, co.cotizacion, co.revision, co.idcliente, idsede, idcontacto, remision, co.estado, observacion, to_char(fecha, 'dd/MM/yyyy HH24:MI') as fecha, u.nombrecompleto as usuario, c.documento, co.solicitud,\n"
                + "(select r.observacion from cotizacion_reemplazada r where idcotorigen = co.id order by r.id desc limit 1) as obserreemplazo,                \n"
                + "(select co2.cotizacion  from cotizacion co2 where co2.id = co.idcotiant) as cotizacionant, c.nombrecompleto  as cliente,\n"
                + "cs.direccion || ', ' || ci.descripcion || ' (' ||d.descripcion ||'), ' || cs.nombre as sede,\n"
                + "cc.nombres as contacto,validez,\n"
                + "(select (select co4.cotizacion  from cotizacion co4 where co4.id = co.idcotiant) || ' Usu. ' || u1.nombrecompleto || ' Fecha ' || to_char(r1.fecha, 'dd/MM/yyyy HH24:MI')\n"
                + "FROM  cotizacion_reemplazada r1 INNER JOIN cotizacion co1 ON co1.id = r1.idcotreem\n"
                + "INNER JOIN seguridad.rbac_usuario u1 on u1.idusu = r1.idusuario WHERE co.id = co1.id) as anterior,\n"
                + "CASE WHEN co.iva > 0 THEN co.reteiva * 100 /co.iva ELSE 0 END as reteiva, CASE WHEN (co.subtotal-co.descuento) > 0 THEN co.reteica* 100 /(co.subtotal-co.descuento) ELSE 0 END as reteica, CASE WHEN (co.subtotal-co.descuento) > 0 THEN co.retefuente* 100 / (co.subtotal-co.descuento) ELSE 0 END as retefuente\n"
                + "from cotizacion co inner join seguridad.rbac_usuario u on co.idusuario = u.idusu\n"
                + "inner join clientes c on c.id = co.idcliente\n"
                + "inner join clientes_contac cc on cc.id = co.idcontacto\n"
                + "inner join clientes_sede cs on cs.id = co.idsede\n"
                + "inner join ciudad ci on ci.id = cs.idciudad\n"
                + "inner join departamento d on d.id = ci.iddepartamento \n"
                + "where cotizacion = " + cotizacion;
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ConsultarCotizaciones");
    }

    public String ConsulIngSCotiza(HttpServletRequest request) {
        int remision = Globales.Validarintnonull(request.getParameter("remision"));
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        int ingreso = Globales.Validarintnonull(request.getParameter("ingreso"));
        String fechad = request.getParameter("fechad");
        String fechah = request.getParameter("fechah");
        int magnitud = Globales.Validarintnonull(request.getParameter("magnitud"));
        String equipo = request.getParameter("equipo");
        int marca = Globales.Validarintnonull(request.getParameter("marca"));
        String modelo = request.getParameter("modelo");
        String intervalo = request.getParameter("intervalo");
        String serie = request.getParameter("serie");
        int usuarios = Globales.Validarintnonull(request.getParameter("usuario"));
        String recepcion = request.getParameter("recepcion");

        String busqueda = "WHERE cotizado = 0 ";
        if (remision > 0) {
            busqueda += " AND r.remision = " + remision;
        } else if (ingreso > 0) {
            busqueda += " AND rd.ingreso = " + ingreso;
        } else {
            busqueda += " AND to_char(r.fecha,'yyyy-MM-dd') >= '" + fechad + "' and to_char(r.fecha,'yyyy-MM-dd') <= '" + fechah + "'";
            if (cliente > 0) {
                busqueda += " AND r.idcliente = " + cliente;
            }
            if (ingreso > 0) {
                busqueda += " AND rd.ingreso = " + ingreso;
            }
            if (magnitud > 0) {
                busqueda += " AND e.idmagnitud = " + magnitud;
            }
            if (!equipo.equals("")) {
                busqueda += " AND e.descripcion = '" + equipo + "'";
            }
            if (marca > 0) {
                busqueda += " AND mo.idmarca = " + marca;
            }
            if (!modelo.equals("")) {
                busqueda += " AND mo.descripcion = '" + modelo + "'";
            }
            if (!intervalo.equals("")) {
                busqueda += " AND '(' || desde || ' a ' || hasta || ') ' || medida = '" + intervalo + "'";
            }
            if (!serie.equals("")) {
                busqueda += " AND rd.serie ilike '%" + serie.trim() + "%'";
            }
            if (usuarios > 0) {
                busqueda += " AND r.idusuario = " + usuarios;
            }
            if (!recepcion.equals("") || !recepcion.isEmpty()) {
                busqueda += " AND r.recepcion = '" + recepcion + "'";
            }
        }

        sql = "select r.id, r.remision, rd.ingreso, rd.observacion, to_char(fechaing, 'yyyy/MM/dd HH24:MI') as fecha, u.nombrecompleto as usuario, "
                + "              c.nombrecompleto || ' (' || c.documento || ')' as cliente, rd.serie, r.recepcion, "
                + "               cs.direccion || ', ' || ci.descripcion || ' (' ||d.descripcion ||'), ' || nombre || '<br><b>'|| cc.nombres || '</b>' "
                + "as sede, "
                + "               '<b>'|| ma.descripcion || '</b><br>' "
                + "|| e.descripcion as equipo, "
                + "                '<b>'|| m.descripcion || '</b><br>' "
                + " || mo.descripcion as marca "
                + "  , '' as tiempo, "
                + "                   '<b>' "
                + "||case when rd.idintervalo = 0 then 'VER ESPECIFICACIONES' ELSE '(' || desde || ' a ' || hasta || ') ' || medida || '</b><br>' || rd.serie end as rango "
                + "from remision r inner join seguridad.rbac_usuario u on r.idusuario = u.idusu "
                + "inner join clientes c on c.id = r.idcliente "
                + "inner join clientes_contac cc on r.idcontacto = cc.id "
                + "inner join clientes_sede cs on r.idsede= cs.id "
                + "inner join ciudad ci on ci.id = cs.idciudad "
                + "inner join departamento d on d.id = ci.iddepartamento "
                + "inner join remision_detalle rd on rd.idremision = r.id "
                + "inner join equipo e on e.id = rd.idequipo "
                + "inner join modelos mo on mo.id = rd.idmodelo "
                + "inner join marcas m on m.id = mo.idmarca "
                + "inner join magnitudes ma on ma.id = e.idmagnitud "
                + "inner join magnitud_intervalos i on i.id = rd.idintervalo " + busqueda
                + "ORDER BY rd.ingreso";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ConsulIngSCotiza");
    }

    public String DatosContacto(HttpServletRequest request) {
        int id = Globales.Validarintnonull(request.getParameter("id"));

        sql = "SELECT idcliente, nombres, email, telefonos FROM clientes_contac WHERE id=" + id;
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->DatosContacto");
    }

    public String ModalRemision(HttpServletRequest request) {
        String cliente = request.getParameter("cliente");
        int remision = Globales.Validarintnonull(request.getParameter("remision"));
        int ingreso = Globales.Validarintnonull(request.getParameter("ingreso"));
        String Busqueda = "";
        Busqueda = " WHERE (c.nombrecompleto ilike '%" + cliente + "%' or c.documento ilike '%" + cliente + "%') and r.id > 0 and rd.ncotizacion = 0 and rd.salida = 0";

        if (remision > 0) {
            Busqueda += " and r.remision = " + remision;
        }
        if (ingreso > 0) {
            Busqueda += " and rd.ingreso = " + ingreso;
        }

        sql = "select r.id, r.remision, r.cotizacion, r.estado, obtener_ingresos(idremision) as ingresos, r.observacion, to_char(fecha, 'dd/MM/yyyy HH24:MI') as fecha, u.nombrecompleto as usuario, \n"
                + "c.nombrecompleto || ' (' || c.documento || ')' as cliente, r.totales\n"
                + "from remision r inner join seguridad.rbac_usuario u on r.idusuario = u.idusu\n"
                + "inner join clientes c on c.id = r.idcliente\n"
                + "inner join remision_detalle rd on rd.idremision = r.id " + Busqueda + " group by r.id, idremision, r.remision, r.cotizacion, r.estado, r.observacion, fecha, u.nombrecompleto,r.recepcion, r.totales, c.nombrecompleto, c.documento\n"
                + "ORDER BY r.id desc";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ModalRemision");
    }

    public String BuscarServicio(HttpServletRequest request) {
        int id = Globales.Validarintnonull(request.getParameter("id"));
        int lote = Globales.Validarintnonull(request.getParameter("lote"));

        if (lote > 0) {
            sql = "SELECT max(c.id) as id, lote, c.idequipo, descuento, c.idmodelo, mo.idmarca as idmarca, c.nombreca, c.idintervalo,c.idintervalo2, c.idservicio,c.idproveedor, c.ingreso, c.serie, c.metodo, c.proxima, c.certificado, c.declaracion, "
                    + "c.punto, c.observacion, c.direccion, c.entrega, c.iva, sum(c.cantidad) as cantidad, c.precio, idserviciodep, idsolicitud, calibracionadic, precioadic "
                    + "FROM cotizacion_detalle c LEFT JOIN modelos mo on mo.id = c.idmodelo "
                    + "where lote = " + lote
                    + "group by c.idequipo, c.idmodelo, mo.idmarca, c.nombreca, c.idintervalo, c.idservicio,c.idproveedor, c.ingreso, c.serie, c.metodo, c.proxima, c.certificado, c.declaracion, "
                    + "    c.punto, c.observacion, c.direccion, c.entrega, c.iva, c.precio, descuento, c.lote, idserviciodep, ingreso, idcotizacion, c.idintervalo2, idsolicitud, calibracionadic, precioadic";
        } else {
            sql = "SELECT c.id as id, lote, c.idequipo, descuento, c.idmodelo, mo.idmarca as idmarca, c.nombreca, c.idintervalo,c.idintervalo2, c.idservicio,c.idproveedor, c.ingreso, "
                    + "precio, iva, descuento, cantidad, c.serie, c.metodo, c.proxima, c.certificado, c.declaracion, idsolicitud, calibracionadic, precioadic, punto, observacion, entrega "
                    + "FROM cotizacion_detalle c LEFT JOIN modelos mo on mo.id = c.idmodelo "
                    + "where c.id = " + id;
        }

        return Globales.ObtenerDatosJSon(sql, this.getClass() + "--> BuscarServicio");
    }

    public String MagnitudEquipo(HttpServletRequest request) {
        int id = Globales.Validarintnonull(request.getParameter("id"));
        sql = "SELECT idmagnitud FROM equipo WHERE id=" + id;
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "--> MagnitudEquipo");
    }

    public String DetalleSolicitud(HttpServletRequest request) {
        int id = Globales.Validarintnonull(request.getParameter("id"));

        sql = "SELECT  descripcion as magnitud, "
                + "case when coalesce(equipo,'') <> '' then equipo else (select descripcion from equipo e where e.id = idequipo) end as equipo,  "
                + "case when coalesce(marca,'') <> '' then marca else (select descripcion from marcas m where m.id = idmarca) end as marca,  "
                + "case when coalesce(modelo,'') <> '' then modelo else (select descripcion from modelos mo where mo.id = idmodelo) end as modelo,  "
                + "case when s.idintervalo = 0 then 'VER ESPECIFICACIONES' ELSE '(' || mi.desde || ' a ' || mi.hasta || ') ' || mi.medida end as intervalo, acreditado, sitio, express, subcontratado, punto, "
                + "metodo, direccion, declaracion, certificado, proxima, cantidad, "
                + "serie, observacion, nombreca "
                + "FROM web.solicitud_detalle s inner join magnitudes m on m.id = idmagnitud "
                + "                         inner join magnitud_intervalos mi on mi.id = idintervalo "
                + "WHERE s.id = " + id;
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->DetalleSolicitud");
    }

    public String MedidaIntervalo(HttpServletRequest request) {
        try {
            int id = Globales.Validarintnonull(request.getParameter("id"));
            sql = "SELECT  m.id, mi.medida  FROM magnitud_intervalos mi left join medidas m on m.descripcion = mi.medida WHERE mi.id =" + id;
            datos = Globales.Obtenerdatos(sql, this.getClass() + "-->TemporalIngresoCotiz", 1);
            datos.next();
            int idmedida = Globales.Validarintnonull(datos.getString("id"));
            String medida = datos.getString("medida");
            if (idmedida == 0) {
                sql = "INSERT INTO medidas(descripcion) values('" + medida + "');";
                Globales.DatosAuditoria(sql, "MEDIDA", "AGREGAR", idusuario, iplocal, this.getClass() + "-->DesbloquearCliente");
            }
            return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->MedidaIntervalo");
        } catch (SQLException ex) {
            Globales.GuardarLogger(ex.getMessage(), this.getClass() + "-->MedidaIntervalo");
            return "[]";
        }
    }

    public String PrecioServicio(HttpServletRequest request) {
        int magnitud = Globales.Validarintnonull(request.getParameter("magnitud"));
        int equipo = Globales.Validarintnonull(request.getParameter("equipo"));
        int marca = Globales.Validarintnonull(request.getParameter("marca"));
        int intervalo = Globales.Validarintnonull(request.getParameter("intervalo"));
        int modelo = Globales.Validarintnonull(request.getParameter("modelo"));
        int precio = Globales.Validarintnonull(request.getParameter("precio"));
        int proveedor = Globales.Validarintnonull(request.getParameter("proveedor"));
        int servicio = Globales.Validarintnonull(request.getParameter("servicio"));
        int tipoprecio = Globales.Validarintnonull(request.getParameter("tipoprecio"));

        if (precio == 0) {
            precio = 1;
        }

        String medida = "";
        double desde = 0;
        double hasta = 0;

        try {
            if (tipoprecio == 0) {
                return "9|Tabla de precio no configurada en el tipo de magnitudes";
            } else {
                if (tipoprecio <= 3 || tipoprecio == 7) {
                    switch (tipoprecio) {
                        case 1:
                            sql = "SELECT * FROM magnitud_intervalos where id = " + intervalo;
                            datos = Globales.Obtenerdatos(sql, this.getClass() + "-->PrecioServicio", 1);
                            datos.next();
                            medida = datos.getString("medida");
                            desde = Globales.ValidardoublenoNull(datos.getString("desde"));
                            hasta = Globales.ValidardoublenoNull(datos.getString("hasta"));
                            sql = "select sp.id, costo, precio" + precio
                                    + ", abreviatura || '-' || trim(to_char(sp.contador,'0000')) as contador,sp.idmetodo "
                                    + "from servicio_precios sp inner join equipo e on e.id = sp.idequipo "
                                    + "inner join magnitudes m on m "
                                    + ".id = sp.idmagnitud "
                                    + "inner join medidas me on me "
                                    + ".id = sp.idmedida "
                                    + "where idproveedor = " + proveedor + " and idequipo = " + equipo + " and idservicio = " + servicio + " and me "
                                    + ".descripcion = '" + medida + "'  and " + desde + " >= sp.desde and " + hasta + " <= sp.hasta "
                                    + "ORDER BY hasta LIMIT 1";
                            break;
                        case 2:
                            sql = "select sp.id, costo,  precio" + precio
                                    + ", abreviatura || '-' || trim(to_char(sp.contador,'0000')) as contador,sp.idmetodo "
                                    + "            from servicio_precios sp inner join equipo e on e.id = sp.idequipo "
                                    + "inner join magnitudes m on m "
                                    + ".id = e.idmagnitud "
                                    + "where idproveedor = " + proveedor + " and idequipo = " + equipo + " and idservicio = " + servicio + " and idmarca = " + marca + "  and idmodelo = " + modelo;
                            break;
                        case 7:
                            sql = "select sp.id, costo, precio" + precio
                                    + ", abreviatura || '-' || trim(to_char(sp.contador,'0000')) as contador, sp.idmetodo "
                                    + "from servicio_precios sp inner join equipo e on e.id = sp.idequipo "
                                    + "inner join magnitudes m on m "
                                    + ".id = e.idmagnitud "
                                    + "where idequipo = " + equipo + " and idservicio = " + servicio;
                            break;
                    }
                } else {
                    return "0|0|0|||0";
                }

                datos = Globales.Obtenerdatos(sql, this.getClass() + "-->PrecioServicio", 1);
                if (datos.next()) {
                    return "0|" + datos.getString("id") + "|" + datos.getString("precio" + precio).replace(',', '.') + "|" + datos.getString("contador") + "|" + datos.getString("idmetodo") + "|" + datos.getString("costo");
                }
            }
        } catch (SQLException ex) {
            Globales.GuardarLogger(ex.getMessage(), this.getClass() + "-->PrecioServicio");
            return "0|0|0|||0";
        }

        return "0|0|0|||0";
    }

    public int VerificarServicio(HttpServletRequest request) {
        int servicio = Globales.Validarintnonull(request.getParameter("servicio"));
        return Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT otro FROM servicio WHERE id =" + servicio));
    }

    public String DiasEntregaSer(HttpServletRequest request) {
        int servicio = Globales.Validarintnonull(request.getParameter("servicio"));
        sql = "SELECT diaentrega || '|' || iva.valor || '|' || express || '|' ||  sitio || '|' || descuento \n"
                + "from servicio s inner join iva on s.idiva = iva.id \n"
                + "where s.id = " + servicio;
        return Globales.ObtenerUnValor(sql);
    }

    public String TablaPrecioServicio(HttpServletRequest request) {
        int id = Globales.Validarintnonull(request.getParameter("id"));
        sql = "SELECT sp.*, m.descripcion FROM servicio_precios sp inner join medidas m on m.id = sp.idmedida WHERE sp.id =" + id;
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->TablaPrecioServicio");
    }

    public String BuscarRemision(HttpServletRequest request) {
        int remision = Globales.Validarintnonull(request.getParameter("remision"));
        sql = "select r.id, r.remision, idcliente, idsede, idcontacto, r.estado, r.pdf, observacion, to_char(fecha, 'dd/MM/yyyy HH24:MI') as fecha, u.nombrecompleto as usuario, c.documento, r.recepcion, req_solicitud  \n"
                + "from remision r inner join seguridad.rbac_usuario u on r.idusuario = u.idusu\n"
                + "inner join clientes c on c.id = r.idcliente\n"
                + "where remision = " + remision;
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->BuscarRemision");
    }

    public String TablaCotizacion(HttpServletRequest request) {
        int cotizacion = Globales.Validarintnonull(request.getParameter("cotizacion"));
        sql = "select r.id, m.descripcion as magnitud, e.descripcion as equipo, ma.descripcion as marca, mo.descripcion as modelo,\n"
                + "case when r.idintervalo = 0 then 'VER ESPECIFICACIONES' ELSE '(' || desde || ' a ' || hasta || ') ' || medida end as Intervalos, observacion, r.cantidad, r.serie,u.nombrecompleto as usuario,r.estado,\n"
                + "r.idintervalo, e.id as idequipo,  ma.id as idmarca, mo.id as idmodelo, ingreso, idserviciodep, to_char(fechareg,'dd/MM/yy') as fechaing, \n"
                + "r.idservicio, r.precio, r.descuento, r.relacionado, r.punto, me.descripcion as metodo, r.direccion, r.certificado, r.proxima, r.declaracion, s.nombre as servicio, idcontacto, idsede, remision, entrega, p.nombrecompleto as proveedor, iva, (select max(fotos) from remision_detalle rd where rd.ingreso = r.ingreso) as fotos, nombreca\n"
                + "from cotizacion_detalle r inner join equipo e on r.idequipo = e.id\n"
                + "inner join magnitudes m on m.id = e.idmagnitud\n"
                + "inner join seguridad.rbac_usuario u on u.idusu = r.idusuario\n"
                + "left join metodo me on me.id = r.metodo\n"
                + "left join servicio s on s.id = r.idservicio\n"
                + "left join magnitud_intervalos mi on mi.id = r.idintervalo\n"
                + "left join modelos mo on mo.id = r.idmodelo  \n"
                + "left join marcas ma on ma.id = mo.idmarca \n"
                + "left join proveedores p on p.id = r.idproveedor    \n"
                + "where idcotizacion = " + cotizacion + " and idserviciodep = 0";

        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->TablaCotizacion");
    }

    public String ModalIngreso(HttpServletRequest request) {
        int idcliente = Globales.Validarintnonull(request.getParameter("idcliente"));
        int tipo = Globales.Validarintnonull(request.getParameter("tipo"));
        String magnitud = request.getParameter("magnitud");
        String equipo = request.getParameter("equipo");
        String intervalo = request.getParameter("intervalo");

        String Busqueda = "";

        if (tipo == 0) {
            Busqueda = " WHERE (c.id = " + idcliente + " OR cdp.idclientedep = " + idcliente + ") and rd.cotizado = 0 ";
        }
        if (tipo == 1) {
            Busqueda = " WHERE (c.id = " + idcliente + " OR cdp.idclientedep = " + idcliente + ") and rd.envtercero = 0 ";
        }
        if (tipo == 3) {
            Busqueda = " WHERE (c.id = " + idcliente + " OR cdp.idclientedep = " + idcliente + ") and rd.facturado = 0 ";
        }
        if (tipo == 4) {
            Busqueda = " WHERE r.remision = " + idcliente;
        }
        if (tipo == 5) {
            Busqueda = " WHERE (c.id = " + idcliente + " OR cdp.idclientedep = " + idcliente + ") and rd.cotizado = 0 ";
        }
        if (tipo == 6) {
            Busqueda = " WHERE c.id = " + idcliente + " and rd.solicitud = 0 and rd.cotizado = 0 ";
        }
        if (tipo == 7) {
            Busqueda = " WHERE c.id = " + idcliente + " and rd.recibidoterc = 1 ";
        }

        sql = "select r.id, r.remision, rd.ncotizacion, r.estado, rd.ingreso, rd.observacion, to_char(fechaing, 'yyyy/MM/dd HH24:MI') as fecha, u.nombrecompleto as usuario, \n"
                + "c.nombrecompleto || ' (' || c.documento || ')' as cliente, rd.serie, e.id as idequipo,\n"
                + "ma.descripcion || '<br><b>' || e.descripcion || '</b><br>' || m.descripcion || '<br>' || mo.descripcion || '<br> ' || \n"
                + "case when rd.idintervalo = 0 then 'VER ESPECIFICACIONES' ELSE '(' || i.desde || ' a ' || i.hasta || ') ' || i.medida END || \n"
                + "case when rd.idintervalo2 = 0 then '' ELSE '<br>(' || i2.desde || ' a ' || i2.hasta || ') ' || i2.medida END || \n"
                + "case when rd.idintervalo3 = 0 then '' ELSE '<br>(' || i3.desde || ' a ' || i3.hasta || ') ' || i3.medida END as equipo  \n"
                + "from remision r inner join seguridad.rbac_usuario u on r.idusuario = u.idusu\n"
                + "inner join clientes c on c.id = r.idcliente\n"
                + "inner join remision_detalle rd on rd.idremision = r.id\n"
                + "inner join equipo e on e.id = rd.idequipo\n"
                + "inner join modelos mo on mo.id = rd.idmodelo\n"
                + "inner join marcas m on m.id = mo.idmarca\n"
                + "inner join magnitudes ma on ma.id = e.idmagnitud\n"
                + "inner join magnitud_intervalos i on i.id = rd.idintervalo \n"
                + "inner join magnitud_intervalos i2 on i2.id = rd.idintervalo2 \n"
                + "inner join magnitud_intervalos i3 on i3.id = rd.idintervalo3 \n"
                + "left join clientes_dependencia cdp on cdp.idcliente = c.id " + Busqueda + "\n"
                + "ORDER BY rd.ingreso desc";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ModalIngreso");

    }

    public String RelaIngresoCoti(HttpServletRequest request) {
        int ingreso = Globales.Validarintnonull(request.getParameter("ingreso"));
        int id = Globales.Validarintnonull(request.getParameter("id"));
        int cotizacion = Globales.Validarintnonull(request.getParameter("cotizacion"));
        int remision = Globales.Validarintnonull(request.getParameter("remision"));
        try {
            if (Globales.PermisosSistemas("COTIZACION REALIZAR COTIZACION DE REMISION NO ASIGNADA", idusuario) == 0) {
                String asesor = Globales.ObtenerUnValor("select asesor from remision WHERE remision = " + remision);
                String asesorcliente = Globales.ObtenerUnValor("select asesor from clientes c inner join remision_detalle rd on rd.idcliente = c.id WHERE ingreso = " + ingreso);
                if (!idusuario.equals(asesorcliente)) {
                    if (!idusuario.equals(asesor)) {
                        return "1|Usted no tiene asignada esta remisión... Consulte con su director comercial";
                    }
                }
            }
            sql = "SELECT id, serie, cotizado FROM remision_detalle WHERE ingreso = " + ingreso;

            datos = Globales.Obtenerdatos(sql, this.getClass() + "-->RelaIngresoCoti", 1);

            int idingreso = Globales.Validarintnonull(datos.getString("id"));
            int cotizado = Globales.Validarintnonull(datos.getString("cotizado"));
            String serie = datos.getString("serie");
            if (cotizado > 0) {
                return "1|Este ingreso ya fue asignado a un ítem de cotización";
            }

            sql = "SELECT idcotizacion \n"
                    + "FROM cotizacion_detalle \n"
                    + "WHERE id = \" + id + \" and ingreso > 0";
            int idcotizacion = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));

            sql = "SELECT id \n"
                    + "FROM cotizacion_detalle \n"
                    + "WHERE id = \" + id + \" and ingreso > 0";
            int idncotizado = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
            if (idncotizado > 0) {
                return "1|Este ítem de cotización ya tiene un ingreso asignado";
            }

            sql = "SELECT TO_CHAR(fecha,'yyyy-MM-dd HH24:MI') as fecha \n"
                    + "FROM cotizacion_aprobacion\n"
                    + "WHERE estado = 'Aprobado' and cotizacion = " + cotizacion;
            String fechaapro = Globales.ObtenerUnValor(sql);

            sql = "SELECT s.solicitud, s.id "
                    + "FROM web.solicitud_detalle sd inner join web.solicitud s on sd.solicitud = s.solicitud "
                    + "WHERE sd.ingreso = " + ingreso + " and s.estado <> 'Anulado' order by s.solicitud desc limit 1";
            datos = Globales.Obtenerdatos(sql, this.getClass() + "-->RelaIngresoCoti", 1);

            int solicitud = 0;
            int idsolicitud = 0;

            if (datos.next()) {
                solicitud = Globales.Validarintnonull(datos.getString("solicitud"));
                idsolicitud = Globales.Validarintnonull(datos.getString("solicitud"));
            }

            sql = "SELECT id \n"
                    + "FROM public.cotizacion_detalle\n"
                    + "where  idservicio in (4,6) and idserviciodep > 0 and ingreso = " + ingreso + " and idcotizacion=" + idcotizacion;
            int ajuste = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));

            sql = " update remision_detalle set cotizado = 1, estado='Cotizado', ncotizacion = " + cotizacion + (solicitud > 0 ? ", solicitud=" + solicitud : "")
                    + " WHERE ingreso = " + ingreso + ";";
            sql += " UPDATE cotizacion_detalle SET ingreso = " + ingreso + ", serie='" + serie + "', relacionado=1, remision = " + remision + ", idsolicitud=" + idsolicitud
                    + " WHERE id = " + id + ";";
            if (!fechaapro.equals("")) {
                sql += "UPDATE remision_detalle set ncotizacionaprobada=" + cotizacion + ", fechaaprocoti = '" + fechaapro + "'"
                        + (ajuste > 0 ? ", fechaaproajus='" + fechaapro + "'" : "")
                        + " WHERE ingreso = " + ingreso + " and fechaaprocoti is null;";
            }
            sql += " INSERT INTO ingreso_estados(ingreso, idusuario, descripcion)\n"
                    + "values(" + ingreso + "," + idusuario + ",'INGRESO COTIZADO');";

            if (Globales.DatosAuditoria(sql, "COTIZACION", "RELACIONAR INGRESO A COTIZACION", idusuario, iplocal, this.getClass() + "-->RelaIngresoCoti")) {
                return "0|Ingreso número " + ingreso + " asignado a la cotizacion " + cotizacion;
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }
        } catch (SQLException ex) {
            Globales.GuardarLogger(ex.getMessage(), this.getClass() + "-->PrecioServicio");
            return "1";
        }
    }

    public String SepararCotizacion(HttpServletRequest request) {
        try {
            int cotizacion = Globales.Validarintnonull(request.getParameter("cotizacion"));
            String a_ids = request.getParameter("a_ids");
            String a_combos = request.getParameter("a_combos");

            if (Globales.PermisosSistemas("COTIZACION SEPARAR ITEMS", idusuario) == 0) {
                return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
            }

            String[] arre_combos = a_combos.split(",");
            String[] arre_ids = a_ids.split(",");
            int[] a_idcotizacion = new int[a_combos.length()];
            String cotizaciones = "";
            List numeros = new ArrayList();
            List ids = new ArrayList();
            int encontrado = 1;
            int idcotizacionnew = 0;

            sql = "select id, revision from cotizacion where cotizacion = " + cotizacion;
            datos = Globales.Obtenerdatos(sql, this.getClass() + "-->SepararCotizacion", 1);
            datos.next();
            String idcotizacion = datos.getString("id");
            int oferta = Globales.Validarintnonull(datos.getString("revision"));
            sql = "SELECT id from version_documento where codigo = 'DID-79' ORDER  BY version DESC LIMIT 1;";
            int version = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
            String textosql = "";
            int ncotizacion = 0;

            for (int x = 0; x < arre_combos.length; x++) {
                encontrado = 0;

                for (int y = 0; y < numeros.size(); y++) {
                    if (arre_combos[x] == numeros.get(y)) {
                        idcotizacionnew = a_idcotizacion[x];
                        encontrado = 1;
                        break;
                    }
                }

                if (encontrado == 0) {
                    numeros.add(Globales.Validarintnonull(arre_combos[x]));
                    sql = "INSERT INTO cotizacion(idcotiant, cotizacion, idcliente, idusuario, idsede, idcontacto, observacion, subtotal, descuento, gravable, exento, iva, total, \n"
                            + "estado, revision, solicitud, idversion)	\n"
                            + "SELECT " + idcotizacion + ", (select max(cotizacion)+1 from contadores), idcliente, " + idusuario + ", idsede, idcontacto, observacion, subtotal, descuento, gravable, exento, iva, total, \n"
                            + "'Cotizado', " + oferta + ", solicitud, " + version + " \n"
                            + "FROM cotizacion WHERE id = \" + idcotizacion + \";";
                    sql += "UPDATE contadores set cotizacion = (select max(cotizacion)+1 from contadores);";
                    sql += "UPDATE cotizacion_detalle SET estado = 'Reemplazado' WHERE idcotizacion = " + idcotizacion + ";";
                    sql += "UPDATE cotizacion SET estado = 'Reemplazado' where cotizacion = " + cotizacion + " and estado in('Cotizado','Cerrado','Aprobado','Rechazado');";
                    Globales.DatosAuditoria(sql, "COTIZACION", "SEPARAR ITEMS", idusuario, iplocal, this.getClass() + "-->SepararCotizacion");
                    idcotizacionnew = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT MAX(id) FROM cotizacion"));
                    for (int i = 0; i < arre_combos.length; i++) {
                        if (arre_combos[i].equals(arre_combos[x])) {
                            a_idcotizacion[i] = idcotizacionnew;
                        }
                    }
                    sql = "INSERT INTO cotizacion_reemplazada(idcotorigen, idcotreem, idusuario, observacion)\n"
                            + "VALUES (" + idcotizacion + "," + idcotizacionnew + "," + idusuario + ",'COTIZACION SEPARADA');";
                    Globales.Obtenerdatos(sql, this.getClass() + "-->SepararCotizacion", 2);
                    ncotizacion = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT cotizacion FROM cotizacion where id=" + idcotizacionnew));
                    ids.add(idcotizacionnew);
                    if (!cotizaciones.equals("")) {
                        cotizaciones += ",";
                        cotizaciones += ncotizacion;
                    }

                    textosql += "INSERT INTO cotizacion_detalle(idcotizacion, idequipo, ingreso, idmodelo, idintervalo, idservicio, \n"
                            + "idserviciodep, punto, direccion, declaracion, certificado, proxima, \n"
                            + "cantidad, serie, observacion, descuento, precio, fechareg, idusuario, \n"
                            + "idcliente, estado, idcontacto, idsede, remision, entrega, idproveedor, \n"
                            + "iva, facturado, metodo, nombreca, relacionado, lote, inicial, \n"
                            + "idintervalo2, idsolicitud)\n"
                            + "select " + idcotizacionnew + ", idequipo, ingreso, idmodelo, idintervalo, idservicio, \n"
                            + "idserviciodep, punto, direccion, declaracion, certificado, proxima, \n"
                            + "cantidad, serie, observacion, descuento, precio, fechareg, idusuario, \n"
                            + "idcliente, estado, idcontacto, idsede, remision, entrega, idproveedor, \n"
                            + "iva, facturado, metodo, nombreca, relacionado, lote, inicial, \n"
                            + "idintervalo2, idsolicitud\n"
                            + "from  cotizacion_detalle  where id = " + arre_ids[x] + ";";

                }

            }
            if (Globales.DatosAuditoria(textosql, "COTIZACION", "SEPARAR ITEMS", idusuario, iplocal, this.getClass() + "-->SepararCotizacion")) {
                return "0|Cotizaciones resultantes de la separacion: |" + cotizaciones;
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }
        } catch (SQLException ex) {
            Globales.GuardarLogger(ex.getMessage(), this.getClass() + "-->SepararCotizacion");
            return "1|" + ex.getMessage();
        }

    }

    public String GuardarPrecio(HttpServletRequest request) {

        int idprecio = Globales.Validarintnonull(request.getParameter("idprecio"));
        int tipoprecio = Globales.Validarintnonull(request.getParameter("tipoprecio"));
        int equipo = Globales.Validarintnonull(request.getParameter("equipo"));
        int marca = Globales.Validarintnonull(request.getParameter("marca"));
        int modelo = Globales.Validarintnonull(request.getParameter("modelo"));
        int intervalo = Globales.Validarintnonull(request.getParameter("intervalo"));
        int proveedor = Globales.Validarintnonull(request.getParameter("proveedor"));
        int servicio = Globales.Validarintnonull(request.getParameter("servicio"));

        double precio1 = Globales.ValidardoublenoNull(request.getParameter("precio1"));
        double precio2 = Globales.ValidardoublenoNull(request.getParameter("precio2"));
        double precio3 = Globales.ValidardoublenoNull(request.getParameter("precio3"));
        double precio4 = Globales.ValidardoublenoNull(request.getParameter("precio4"));
        double precio5 = Globales.ValidardoublenoNull(request.getParameter("precio5"));

        double precio6 = Globales.ValidardoublenoNull(request.getParameter("precio6"));
        double precio7 = Globales.ValidardoublenoNull(request.getParameter("precio7"));
        double precio8 = Globales.ValidardoublenoNull(request.getParameter("precio8"));
        double precio9 = Globales.ValidardoublenoNull(request.getParameter("precio9"));
        double precio10 = Globales.ValidardoublenoNull(request.getParameter("precio10"));

        if (idprecio == 0) {

            switch (tipoprecio) {
                case 1:
                    sql = "INSERT INTO servicio_precios("
                            + "idservicio, idequipo, idintervalo, "
                            + "precio1, precio2, precio3, precio4, precio5, "
                            + "precio6, precio7, precio8, precio9, precio10)"
                            + "VALUES (" + servicio + "," + equipo + "," + intervalo
                            + "," + precio1 + "," + precio2 + "," + precio3 + "," + precio4 + "," + precio5
                            + "," + precio6 + "," + precio7 + "," + precio8 + "," + precio9 + "," + precio10 + ")";
                    break;
                case 2:
                    sql = "INSERT INTO servicio_precios("
                            + "idservicio, idequipo, idmarca, idmodelo,"
                            + "precio1, precio2, precio3, precio4, precio5, "
                            + "precio6, precio7, precio8, precio9, precio10)"
                            + "VALUES (" + servicio + "," + equipo + "," + marca + "," + modelo
                            + ", " + precio1 + "," + precio2 + "," + precio3 + "," + precio4 + "," + precio5
                            + "," + precio6 + "," + precio7 + "," + precio8 + "," + precio9 + "," + precio10 + ")";
                    break;
                case 3:
                    sql = "INSERT INTO servicio_precios("
                            + "idservicio, idequipo, idmarca, idmodelo, idproveedor, "
                            + "precio1, precio2, precio3, precio4, precio5, "
                            + "precio6, precio7, precio8, precio9, precio10) "
                            + "VALUES (" + servicio + "," + equipo + "," + marca + "," + modelo + "," + proveedor + "," + precio1
                            + "," + precio2 + "," + precio3 + "," + precio4 + "," + precio5
                            + "," + precio6 + "," + precio7 + "," + precio8 + "," + precio9 + "," + precio10 + ")";
                    break;
            }

        } else {
            sql = "UPDATE servicio_precios\n"
                    + "SET precio1=" + precio1 + ",precio2=" + precio2 + ",precio3=" + precio3 + ",precio4=" + precio4 + ",precio5=" + precio5
                    + ",precio6=" + precio6 + ",precio7=" + precio7 + ",precio8=" + precio8 + ",precio9=" + precio9 + ",precio10=" + precio10
                    + "WHERE id = " + idprecio;
        }

        Globales.DatosAuditoria(sql, "COTIZACION", "ACTUALIZAR PRECIOS SERVICIOS", idusuario, iplocal, this.getClass() + "-->GuardarPrecio");
        return "0";

    }

    /*
     public String AlertaLaboratorio()
     {            
     String magnitudes = Globales.ObtenerUnValor("SELECT magnitud FROM seguridad.rbac_usuario WHERE idusu = " + idusuario);
     String[] sql = new String[8];
     sql[0] = "SELECT '<a href=''javascript:ModalDetalleEstado(' || rd.ingreso || ',' || p.id ||  ')''>' || rd.ingreso || '(' || subString(m.descripcion, 1,3) || ')</a><br>' || cotizacion_ingreso(rd.ingreso) as ingreso, tiempo_aprobacion(rd.ingreso, 0, fechaaproajus, rd.fechaaprocoti,rd.fechaing,now(),3) as tiempo " +
     " FROM public.ingreso_recibidolab ir inner join remision_detalle rd on rd.ingreso = ir.ingreso " +
     "                            inner join equipo e on e.id = rd.idequipo" +
     "                             inner join magnitudes m on m.id = e.idmagnitud" +
     "                              inner join ingreso_plantilla ip on ip.ingreso = rd.ingreso" +
     "			                               inner join plantillas p on p.id = ip.idplantilla" +
     "where reportado = 0 and salida = 0 AND  noautorizado = 0 and recibidoing = 0 and to_char(ir.fecha,'yyyy') >= '2019' and e.idmagnitud in (" + magnitudes + ") and rd.garantia='NO' AND envtercero = 0 and salidaterce = 0 and recibidocome = 0 and convenio = 0 and idcliente <> 11 order by rd.ingreso";

     sql[1] = "SELECT '<a href=''javascript:ModalDetalleEstado(' || rd.ingreso || ',' || p.id ||  ')''>' || rd.ingreso || '(' || subString(m.descripcion, 1,3) || ')</a>' as ingreso, tiempo_aprobacion(rd.ingreso, 0, fechaaproajus, rd.fechaaprocoti,rd.fechaing,now(),3) as tiempo" +
     "FROM public.reporte_ingreso ir inner join remision_detalle rd on rd.ingreso = ir.ingreso" +
     "                         inner join equipo e on e.id = rd.idequipo" +
     "                          inner join magnitudes m on m.id = e.idmagnitud" +
     "			   inner join ingreso_plantilla ip on ip.ingreso = rd.ingreso" +
     "			   inner join plantillas p on p.id = ip.idplantilla" +
     "where rd.recibidoing = 0 and certificado = 0 and salida = 0 and to_char(ir.fecha,'yyyy') >= '2019' and informetecnico = 0  and noautorizado = 0 and e.idmagnitud in (" + magnitudes + ") and rd.garantia='NO' AND rd.convenio = 0 order by rd.ingreso";
     sql[2] = "SELECT '<a href=''javascript:ModalDetalleEstado(' || rd.ingreso || ',' || p.id ||  ')''>' || rd.ingreso || '(' || subString(m.descripcion, 1,3) || ')</a>' as ingreso, tiempo_aprobacion(rd.ingreso, 0, fechaaproajus, rd.fechaaprocoti,rd.fechaing,now(),3) as tiempo" +
     "FROM public.certificados ir inner join remision_detalle rd on rd.ingreso = ir.ingreso" +
     "                      inner join equipo e on e.id = rd.idequipo" +
     "                       inner join magnitudes m on m.id = e.idmagnitud" +
     "			                            inner join ingreso_plantilla ip on ip.ingreso = rd.ingreso" +
     "		                                inner join plantillas p on p.id = ip.idplantilla" +
     "WHERE rd.recibidoing = 0 and salida = 0 and rd.idcliente <> 11 and ir.estado <> 'Anulado' and to_char(ir.fecha,'yyyy') >= '2019' and e.idmagnitud in (" + magnitudes + ") and rd.garantia='NO' AND envtercero = 0 and salidaterce = 0 and recibidocome = 0 and convenio = 0  order by rd.ingreso";
     sql[3] = "SELECT '<a href=''javascript:ModalDetalleEstado(' || rd.ingreso || ',' || p.id ||  ')''>' || rd.ingreso || '(' || subString(m.descripcion, 1,3) || ')</a>' as ingreso, tiempo_aprobacion(rd.ingreso, 0, fechaaproajus, rd.fechaaprocoti,rd.fechaing,now(),3) as tiempo" +
     "FROM public.certificados ir inner join remision_detalle rd on rd.ingreso = ir.ingreso" +
     "                      inner join equipo e on e.id = rd.idequipo" +
     "                       inner join magnitudes m on m.id = e.idmagnitud" +
     "			                            inner join ingreso_plantilla ip on ip.ingreso = rd.ingreso and ip.idplantilla = ir.item" +
     "		                                inner join plantillas p on p.id = ip.idplantilla" +
     "WHERE stiker = 0 and salida = 0 and rd.idcliente <> 11 and ir.estado <> 'Anulado' and to_char(ir.fecha,'yyyy') >= '2019' and e.idmagnitud in (" + magnitudes + ") and rd.garantia='NO' AND envtercero = 0 and salidaterce = 0 and recibidocome = 0 and convenio = 0 and idcliente <> 11 order by rd.ingreso";
     sql[4] = "SELECT DISTINCT '<a href=''javascript:ModalDetalleEstado(' || rd.ingreso || ',' || p.id || ')''>' || rd.ingreso || '(' || subString(m.descripcion, 1,3) || ')</a>' as ingreso, tiempo_aprobacion(rd.ingreso, 0, fechaaproajus, rd.fechaaprocoti,rd.fechaing,now(),3) as tiempo" +
     "FROM public.ingreso_recibidolab ir inner join remision_detalle rd on rd.ingreso = ir.ingreso" +
     "                              inner join equipo e on e.id = rd.idequipo" +
     "                               inner join magnitudes m on m.id = e.idmagnitud" +
     "		                                        inner join cotizacion_detalle cd on cd.ingreso = rd.ingreso" +
     "		                                        inner join cotizacion co on co.id = cd.idcotizacion" +
     "		                                        inner join servicio s on s.id = cd.idservicio" +
     "                               inner join ingreso_plantilla ip on ip.ingreso = rd.ingreso" +
     "		                                        inner join plantillas p on p.id = ip.idplantilla" +
     "where  ((ip.certificado = 0 AND ip.informetecnico = 0) OR recibidoing = 0) and idserviciodep = 0 and salida = 0 and s.proveedor = 0 and to_char(ir.fecha,'yyyy') >= '2019' and express = 2 and e.idmagnitud in (" + magnitudes + ") and rd.garantia='NO' AND rd.convenio = 0 order by 1";
     sql[5] = "SELECT '<a href=''javascript:ModalDetalleEstado(' || rd.ingreso || ',' || p.id ||  ')''>' || rd.ingreso || '(' || subString(m.descripcion, 1,3) || ')</a>' as ingreso, tiempo_aprobacion(rd.ingreso, 0, fechaaproajus, rd.fechaaprocoti,rd.fechaing,now(),3) as tiempo" +
     "FROM public.certificados ir inner join remision_detalle rd on rd.ingreso = ir.ingreso" +
     "                      inner join equipo e on e.id = rd.idequipo" +
     "                       inner join magnitudes m on m.id = e.idmagnitud" +
     "			                            inner join ingreso_plantilla ip on ip.ingreso = rd.ingreso and ip.idplantilla = ir.item" +
     "		                                inner join plantillas p on p.id = ip.idplantilla" +
     "WHERE salida = 0 and ir.estado <> 'Anulado' and ir.impreso = 0 and tempmax is not null and to_char(ir.fecha,'yyyy') >= '2019' and e.idmagnitud in (" + magnitudes + ") and envtercero = 0 and salidaterce = 0 and recibidocome = 0 and convenio = 0 and idcliente <> 11 order by rd.ingreso";
     sql[6] = "SELECT '<a href=''javascript:ModalDetalleEstado(' || rd.ingreso || ',' || p.id ||  ')''>' || rd.ingreso || '(' || subString(m.descripcion, 1,3) || ')</a>' as ingreso, tiempo_aprobacion(rd.ingreso, 0, fechaaproajus, rd.fechaaprocoti,rd.fechaing,now(),3) as tiempo" +
     "FROM public.certificados ir inner join remision_detalle rd on rd.ingreso = ir.ingreso" +
     "                      inner join equipo e on e.id = rd.idequipo" +
     "                       inner join magnitudes m on m.id = e.idmagnitud" +
     "			                            inner join ingreso_plantilla ip on ip.ingreso = rd.ingreso and ip.idplantilla = ir.item" +
     "		                                inner join plantillas p on p.id = ip.idplantilla" +
     "WHERE salida = 0 and ir.estado = 'Registrado' and to_char(ir.fecha,'yyyy') >= '2019' and e.idmagnitud in  (" + magnitudes + ") and rd.garantia='NO' and envtercero = 0 and salidaterce = 0 and recibidocome = 0 and convenio = 0 and idcliente <> 11 order by rd.ingreso";

     sql[7] = "SELECT '<a href=''javascript:ModalDetalleEstado(' || rd.ingreso || ',0)''>' || rd.ingreso || '(' || subString(m.descripcion, 1,3) || ')</a><br>' || cotizacion_ingreso(rd.ingreso) as ingreso, tiempo_aprobacion(rd.ingreso, 0, null, rd.fechaaprocoti,rd.fechaing,now(),3) as tiempo" +
     "FROM remision_detalle rd inner join equipo e on e.id = rd.idequipo" +
     "		     inner join magnitudes m on m.id = e.idmagnitud" +
     "where salida = 0 AND recibidolab = 0 and recibidocome = 0 and to_char(rd.fechaing,'yyyy') >= '2019' and e.idmagnitud in (" + magnitudes + ") and rd.garantia='NO' and envtercero = 0 and salidaterce = 0 and convenio = 0 and idcliente <> 11 order by rd.ingreso";
     DataSet ds = Globales.ObtenerDataSet(sql, conexion);
     return JsonConvert.SerializeObject(ds);

     }
     */
    public String ConsulIngReportado(HttpServletRequest request) {
        String estado = request.getParameter("estado");
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        int ingreso = Globales.Validarintnonull(request.getParameter("ingreso"));
        String fechad = request.getParameter("fechad");
        String fechah = request.getParameter("fechah");

        int magnitud = Globales.Validarintnonull(request.getParameter("magnitud"));
        int equipo = Globales.Validarintnonull(request.getParameter("equipo"));
        int marca = Globales.Validarintnonull(request.getParameter("marca"));
        int modelo = Globales.Validarintnonull(request.getParameter("modelo"));
        int intervalo = Globales.Validarintnonull(request.getParameter("intervalo"));
        String serie = request.getParameter("serie");
        int usuarios = Globales.Validarintnonull(request.getParameter("usuario"));
        int cotizado = Globales.Validarintnonull(request.getParameter("cotizado"));
        int asesor = Globales.Validarintnonull(request.getParameter("asesor"));
        int enviado = Globales.Validarintnonull(request.getParameter("enviado"));
        int noaprobado = Globales.Validarintnonull(request.getParameter("noaprobado"));

        String busqueda = "WHERE certificado = 0 AND informetecnico = 0 and noautorizado = 0 and rd.salida = 0 ";

        if (ingreso > 0) {
            busqueda += " AND rd.ingreso = " + ingreso;
        } else {
            busqueda += " AND to_char(ri.fecha,'yyyy-MM-dd') >= '" + fechad + "' and to_char(ri.fecha,'yyyy-MM-dd') <= '" + fechah + "'";
            if (cliente > 0) {
                busqueda += " AND r.idcliente = " + cliente;
            }
            if (ingreso > 0) {
                busqueda += " AND rd.ingreso = " + ingreso;
            }
            if (magnitud > 0) {
                busqueda += " AND e.idmagnitud = " + magnitud;
            }
            if (equipo > 0) {
                busqueda += " AND rd.idequipo = " + equipo;
            }
            if (marca > 0) {
                busqueda += " AND mo.idmarca = " + marca;
            }
            if (modelo > 0) {
                busqueda += " AND rd.idmodelo = " + modelo;
            }
            if (intervalo > 0) {
                busqueda += " AND rd.idintervalo = " + intervalo;
            }
            if (!serie.trim().equals("")) {
                busqueda += " AND rd.serie ilike '%" + serie.trim() + "%'";
            }
            if (usuarios > 0) {
                busqueda += " AND r.idusuario = " + usuarios;
            }
            if (cotizado != -1) {
                if (cotizado == 0) {
                    busqueda += " AND ri.cotizado = 0 ";
                } else if (cotizado == 1) {
                    busqueda += " AND ri.cotizado > 0 ";
                } else {
                    busqueda += " AND ri.idconcepto = 1 ";
                }
            }
            if (!estado.equals("TODOS")) {
                busqueda += " AND ri.tipoaprobacion = '" + estado + "'";
            }
            if (asesor > 0) {
                busqueda += " AND c.asesor = " + asesor;
            }
            if (enviado > 0) {
                busqueda += " AND ri.fechaenvio is not null";
            }
            if (noaprobado > 0) {
                busqueda += " AND ri.fecaprobacion is null";
            }
        }

        sql = "select r.id, '<b>' || r.remision || '</b><br>' || to_char(fechaing, 'yyyy/MM/dd HH24:MI') as remision, rd.ingreso, rd.observacion, to_char(ri.fecha, 'yyyy/MM/dd HH24:MI') as fecha, u.nombrecompleto as usuario, \n"
                + "c.nombrecompleto || ' (' || c.documento || ')<br><b>' || ua.nomusu || '</b>' || case when habitual = 'SI' THEN '<br><b>habitual</b>' else case when aprobar_ajuste = 'NO' THEN '<br><b>no.re.ap.aju</b>' else '' end end as cliente, rd.serie, r.recepcion,\n"
                + "concepto || case when suministro <> '' then '<br><b>Suministro:</b> ' || suministro else '' end \n"
                + "|| case when ajuste <> '' then '<br><b>Ajuste:</b> ' || ajuste else '' end \n"
                + "|| case when ri.observacion <> '' then '<br><b>Observación:</b> ' || ri.observacion else '' end as reporte, \n"
                + "'<b>' || ma.descripcion || '</b><br>' || e.descripcion as equipo, case when ri.cotizado = 0 then 'NO' else 'SI' END AS cotizado,\n"
                + "'<b>' || m.descripcion || '</b><br>' || mo.descripcion  as marca, case when ri.cotizado = 0 then tiempo(ri.fecha,now(),1) else '' end as tiempo,\n"
                + "'<b>' ||case when rd.idintervalo = 0 then 'VER ESPECIFICACIONES' ELSE '(' || desde || ' a ' || hasta || ') ' || medida || '</b><br>' || rd.serie end as rango, \n"
                + "case when coalesce(tipoaprobacion,'') = ''then 'EN ESPERA DEL CLIENTE' else tipoaprobacion end as estado, to_char(ri.fecaprobacion, 'yyyy/MM/dd HH24:MI') as fecaprobacion, observacionapro,\n"
                + "to_char(ri.fechaenvio, 'yyyy/MM/dd HH24:MI') as fechaenvio, ri.correoenvio,\n"
                + "coalesce(ri.usuarioapro,'') || '<br>' || coalesce(ri.cedula,'') || '<br>' || coalesce(ri.cargo,'') as aprobadopor,\n"
                + "ri.id as idreporte, p.descripcion as plantilla,\n"
                + "coalesce(c.telefono,'') || '<br>' || coalesce(c.celular,'') || '<br>' || \n"
                + "coalesce((select cc.telefonos || '(c)<br>' || cc.fax || '<br>(c)' || cc.email || '(c)' from cotizacion_detalle cd inner join clientes_contac cc on cd.idcontacto = cc.id where cd.ingreso = rd.ingreso limit 1),'') as telefono, \n"
                + "'<button class=''btn btn-glow btn-success imprimir'' title=''Reenvio Aprobar Ajuste'' type=''button'' onclick=' || chr(34) || 'EnvioAjuste(' || ri.ingreso || ',' || ri.nroreporte || ',''' || c.email || ''',''' || c.nombrecompleto || ''',''' || \n"
                + "coalesce((select cc.email from cotizacion_detalle cd inner join clientes_contac cc on cd.idcontacto = cd.idcontacto where cd.ingreso = rd.ingreso limit 1),'') || ''',' || ri.plantilla || ',' || ma.id || ')' ||chr(34) || '><span data-icon=''&#xe255;''></span></button>' as enviar\n"
                + "from remision r inner join remision_detalle rd on rd.idremision = r.id\n"
                + "inner join ingreso_plantilla ip on ip.ingreso = rd.ingreso\n"
                + "inner join plantillas p on p.id = ip.idplantilla\n"
                + "INNER JOIN reporte_ingreso ri on ri.ingreso = rd.ingreso\n"
                + "inner join seguridad.rbac_usuario u on ri.idusuario = u.idusu\n"
                + "inner join clientes c on c.id = r.idcliente\n"
                + "inner join equipo e on e.id = rd.idequipo\n"
                + "inner join modelos mo on mo.id = rd.idmodelo\n"
                + "inner join marcas m on m.id = mo.idmarca\n"
                + "inner join magnitudes ma on ma.id = e.idmagnitud\n"
                + "inner join seguridad.rbac_usuario ua on c.asesor = ua.idusu\n"
                + "inner join magnitud_intervalos i on i.id = rd.idintervalo " + busqueda + "\n"
                + "ORDER BY rd.ingreso";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ConsulIngReportado");
    }

    /*
     public String AprobarCotizacion(HttpServletRequest request) {
     int cotizacion = Globales.Validarintnonull(request.getParameter("cotizacion"));
     String usuario = request.getParameter("cliente");
     String cedula = request.getParameter("cedula");
     String cargo = request.getParameter("cargo");
     String observacion = request.getParameter("obsevacion");
     String operacion = request.getParameter("operacion");
     String fecha = request.getParameter("fecha");
     String hora = request.getParameter("hora");
     if (Globales.PermisosSistemas("COTIZACION " + operacion.toUpperCase(), idusuario) == 0) {
     return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
     }
     fecha = fecha + " " + hora;
     if (Session["ArchivoExcel"] != null) {
     String fileName = HostingEnvironment.MapPath("~/uploads/" + Session["ArchivoExcel"));
     String puerto = Request.Url.Port.ToString();
     String filedestino = HostingEnvironment.MapPath("~/CotizacionAprobada/" + cotizacion + ".pdf");
     System.IO.File.Copy(fileName, filedestino, true);
     } else {
     return "1|Error al subir el archivo";
     }
     String mensaje = "";
     sql = "SELECT cotizacion FROM cotizacion WHERE cotizacion = " + cotizacion + " and (aprobada = 1 or rechazada = 1) ";
     int encontrado = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
     if (encontrado > 0) {
     return "1|Esta cotización ya fue aprobada/rechazada anteriormente";
     }
     String sql2 = "INSERT INTO public.cotizacion_aprobacion(cotizacion, usuario, cedula, cargo, estado, observacion, idusuario) "
     + " VALUES(" + cotizacion + ",'" + usuario.trim().toUpperCase() + "','" + cedula.trim() + "','" + cargo.trim().toUpperCase() + "','" + operacion + "','" + observacion.trim() + "'," + idusuario + ");";
     if (operacion.equals("Aprobado")) {
     sql2 += "UPDATE cotizacion SET aprobada = 1, estado='Aprobado', fechaaprobacion = '" + fecha + "'"
     + " WHERE cotizacion = " + cotizacion + ";";
     mensaje = "0|Cotización aprobada con éxtio";
            
     sql2 += "INSERT INTO ingreso_estados(ingreso, idusuario, descripcion, fecha, tiempo) "
     + "SELECT distinct cd.ingreso, " + idusuario + ",'AUTORIZACIÓN DE AJUSTE','" + fecha + "'::timestamp with time zone,tiempo(ri.fecha,'" + fecha + "',1) "
     + "FROM reporte_ingreso ri INNER JOIN cotizacion_detalle cd on ri.ingreso = cd.ingreso "
     + "                       inner join cotizacion co on cd.idcotizacion = co.id"
     + "WHERE (coalesce(ri.usuarioapro,'') = '' or tipo_aprobacion = 3) and cd.idservicio in(4,6,9) and idconcepto = 2 and cd.idserviciodep > 0 and co.cotizacion = " + cotizacion + "";
     sql2 += "UPDATE ingreso_plantilla set fechaaproajus='" + fecha + "' "
     + "from cotizacion_detalle cd INNER JOIN cotizacion co on co.id = cd.idcotizacion  "
     + "                           INNER JOIN reporte_ingreso ri on ri.ingreso = cd.ingreso "
     + "WHERE fechaaproajus is null and  coalesce(ri.usuarioapro,'') = '' and ingreso_plantilla.ingreso = cd.ingreso and cd.idservicio in(4,6,9) and idconcepto = 2 and cd.idserviciodep > 0 and co.cotizacion = " + cotizacion + "";
            
     sql2 += "UPDATE reporte_ingreso SET archivo_aprueba='3|" + cotizacion + "', observacionapro='Aprobado por cotización', usuarioapro='" + usuario + "', cedula='" + cedula + "',cargo='" + cargo
     + "'" + ",fecaprobacion='" + fecha + "',tipoaprobacion='APROBADO AJUSTE / SUMINISTRO',tipo_aprobacion=1 "
     + "from cotizacion_detalle cd INNER JOIN cotizacion co on co.id = cd.idcotizacion "
     + "WHERE (coalesce(reporte_ingreso.usuarioapro,'') = '' or tipo_aprobacion = 3) and reporte_ingreso.ingreso = cd.ingreso and cd.idservicio in(4,6,9) and idconcepto = 2 and cd.idserviciodep > 0 and co.cotizacion = " + cotizacion + "";
     } else {
     mensaje = "0|Cotización rechazada con éxtio";
     sql2 += "UPDATE cotizacion SET rechazada = 1, estado='Rechazado', fecharechazada = '" + fecha + "'"
     + " WHERE cotizacion = " + cotizacion + ";";
            
     sql2 += "INSERT INTO ingreso_estados(ingreso, idusuario, descripcion, fecha, tiempo) "
     + "SELECT distinct cd.ingreso, " + idusuario + ",'RECHAZO DE AJUSTE','" + fecha + "'::timestamp with time zone,tiempo(ri.fecha,'" + fecha + "',1)  "
     + "FROM reporte_ingreso ri INNER JOIN cotizacion_detalle cd on ri.ingreso = cd.ingreso "
     + "                        inner join cotizacion co on cd.idcotizacion = co.id "
     + "WHERE coalesce(ri.usuarioapro,'') = '' and cd.idservicio in(4,6,9) and idconcepto = 2 and cd.idserviciodep > 0 and co.cotizacion = " + cotizacion + "";
     sql2 += "UPDATE reporte_ingreso SET archivo_aprueba='3|" + cotizacion + "', observacionapro='Rechazo por cotización web', usuarioapro='" + usuario + "', cedula='" + cedula + "',cargo='" + cargo
     + "',fecaprobacion='" + fecha + "',tipoaprobacion='Generar Informe de Devolución',tipo_aprobacion=3 "
     + "from cotizacion_detalle cd INNER JOIN cotizacion co on co.id = cd.idcotizacion "
     + "WHERE coalesce(reporte_ingreso.usuarioapro,'') = '' and reporte_ingreso.ingreso = cd.ingreso and cd.idservicio in(4,6,9) and idconcepto = 2 and cd.idserviciodep > 0 and co.cotizacion = " + cotizacion + "";
     }
     sql2 += "UPDATE remision_detalle set fechaaprocoti='" + fecha + "'"
     + "FROM cotizacion_detalle cd inner join cotizacion c on c.id = cd.idcotizacion "
     + "WHERE remision_detalle.ingreso = cd.ingreso  and fechaaprocoti is null and cotizacion = " + cotizacion;
     Globales.DatosAuditoria(sql2, "COTIZACION", operacion.toUpperCase(), idusuario, iplocal, this.getClass() + "-->AprobarCotizacion", 2);
     return mensaje;       

     }
     */
    public String EliminarRegCotizacion(HttpServletRequest request) {
        int id = Globales.Validarintnonull(request.getParameter("id"));
        int cotizacion = Globales.Validarintnonull(request.getParameter("cotizacion"));
        int cantidad = Globales.Validarintnonull(request.getParameter("cantidad"));
        int lote = Globales.Validarintnonull(request.getParameter("lote"));

        try {

            sql = "SELECT estado, ingreso, idserviciodep, lote "
                    + "from cotizacion_detalle "
                    + "where id = " + id;
            datos = Globales.Obtenerdatos(sql, this.getClass() + "-->EliminarRegCotizacion", 1);
            datos.next();

            String estado = datos.getString("estado");
            int ingreso = Globales.Validarintnonull(datos.getString("ingreso"));
            int serviciodep = Globales.Validarintnonull(datos.getString("idserviciodep"));
            lote = Globales.Validarintnonull(datos.getString("lote"));

            if (!estado.equals("Temporal") && !estado.equals("Cotizado")) {
                return "1|No se puede eliminar un itmes de la cotización con estado " + estado;
            }

            if (serviciodep == 0) {

                if (lote == 0) {
                    if (id > 0) {
                        sql = "delete from cotizacion_detalle where idserviciodep = " + id + " and idcotizacion=" + cotizacion + ";";
                        sql += "DELETE FROM cotizacion_detalle where id = " + id + ";";
                    }
                } else {
                    sql = "delete from cotizacion_detalle where lote = " + lote + " and idcotizacion=" + cotizacion + ";";
                }
            } else {
                sql = "DELETE FROM cotizacion_detalle where id = " + id + ";";
            }

            sql += " UPDATE cotizacion SET estado = 'Temporal' WHERE id = " + cotizacion + ";";

            if (ingreso > 0) {
                if (serviciodep == 0) {
                    sql += "UPDATE remision_detalle set estado ='Ingresado', cotizado = 0 WHERE ncotizacion = 0 and ingreso = " + ingreso + ";";
                }
                sql += "UPDATE remision_detalle set estado ='Ingresado', cotizado = 0 WHERE ingreso = " + ingreso + ";";
                sql += "UPDATE reporte_ingreso set cotizado ='0' WHERE ingreso = " + ingreso + " and aprobado_rep = 0;";
                sql += "UPDATE remision r set estado = 'Cerrado' "
                        + "FROM remision_detalle rd "
                        + "WHERE rd.idremision = r.id and rd.ingreso = " + ingreso;
            }

            if (Globales.DatosAuditoria(sql, "COTIZACION", "ELIMINAR ITEMS COTIZACION", idusuario, iplocal, this.getClass() + "-->EliminarRegCotizacion")) {
                return "0|Item eliminado";
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }

        } catch (SQLException ex) {

            return "1|" + ex.getMessage();

        }
    }

    public int Contadores(HttpServletRequest request, int tipo_p) {
        String campo = "";
        int tipo;
        if (request == null) {
            tipo = tipo_p;
        } else {
            tipo = Globales.Validarintnonull(request.getParameter("tipo"));
        }

        switch (tipo) {
            case 1:
                campo = "remision";
                break;
            case 2:
                campo = "ingreso";
                break;
            case 3:
                campo = "cotizacion";
                break;
            case 4:
                campo = "lote";
                break;
        }
        int contador = Globales.Validarintnonull(Globales.ObtenerUnValor("select " + campo + " FROM contadores")) + 1;
        return contador;
    }

    public String AgregarSolicitud(HttpServletRequest request) {
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        int solicitud = Globales.Validarintnonull(request.getParameter("solicitud"));
        int idsede = Globales.Validarintnonull(request.getParameter("idsede"));
        int idcontacto = Globales.Validarintnonull(request.getParameter("idcontacto"));

        int idequipo = 0;
        int idmarca = 0;
        int idmodelo = 0;

        try {
            sql = "SELECT count(id) FROM cotizacion_detalle where idcliente = " + cliente + " and idcotizacion = 0 ";
            int cantidad = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
            if (cantidad > 0) {
                return "1|No se puede agregar esta solicitud porque este cliente posee servicios por cotizar";
            }

            sql = "SELECT idcliente, estado FROM web.solicitud where solicitud = " + solicitud;
            datos = Globales.Obtenerdatos(sql, this.getClass() + "-->AgregarSolicitud", 1);
            datos.next();
            if (!datos.next()) {
                return "1|Este número de solicitud no se encuentra registrada en el sistema";
            } else {

                String estado = datos.getString("estado");
                int idcliente = Globales.Validarintnonull(datos.getString("idcliente"));
                if (idcliente != cliente) {
                    return "1|Esta solicitud no pertenece a este cliente";
                }
                if (!estado.equals("Pendiente")) {
                    return "1|No se puede agregar una solicitud en estado " + estado;
                }

                sql = "SELECT id, idcliente, solicitud, idmagnitud, equipo, marca, modelo, "
                        + "  idintervalo, acreditado, sitio, express, subcontratado, punto, "
                        + "   metodo, direccion, declaracion, certificado, proxima, cantidad, "
                        + "    serie, observacion, nombreca, idequipo, idmarca, idmodelo, ingreso "
                        + "FROM web.solicitud_detalle "
                        + "WHERE solicitud = " + solicitud;
                datos = Globales.Obtenerdatos(sql, this.getClass() + "-->AgregarSolicitud", 1);
                datos.next();

                int lote = 0;
                String textosql = "";

                while (datos.next()) {
                    lote = 0;
                    textosql = "";
                    cantidad = Globales.Validarintnonull(datos.getString("cantidad"));
                    if (cantidad > 1) {
                        lote = Contadores(null, 4);
                    }
                    if (Globales.Validarintnonull(datos.getString("idequipo")) > 0) {
                        for (int x = 1; x <= cantidad; x++) {
                            textosql += "INSERT INTO cotizacion_detalle(idcotizacion, idequipo, ingreso, idmodelo, idintervalo, idintervalo2, serie, metodo, punto, direccion, declaracion, certificado, proxima,  "
                                    + "   cantidad, observacion, idusuario, idcliente, idcontacto, idsede, nombreca,  lote, idsolicitud) "
                                    + "SELECT 0, idequipo, ingreso, idmodelo, idintervalo, 0, serie, -1, punto, direccion, declaracion, certificado, proxima, "
                                    + "1,observacion," + idusuario + "," + cliente + "," + idcontacto + "," + idsede + ",nombreca," + lote + "," + datos.getString("id")
                                    + " FROM  web.solicitud_detalle  WHERE id = " + datos.getString("id") + "";
                        }
                    } else {
                        idequipo = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT id FROM equipo WHERE descripcion = '" + datos.getString("equipo") + "'"));
                        if (idequipo == 0) {
                            idequipo = -1;
                        }
                        idmarca = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT id FROM marcas WHERE descripcion = '" + datos.getString("marca") + "'"));
                        if (idmarca == 0) {
                            sql = "INSERT INTO marcas (descripcion) VALUES('" + datos.getString("marca") + "')";
                            Globales.DatosAuditoria(sql, "MARCA", "AGREGAR", idusuario, iplocal, this.getClass() + "-->AgregarSolicitud");
                            sql = "SELECT id FROM marcas WHERE descripcion = '" + datos.getString("marca") + "'";
                            idmarca = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
                        }
                        idmodelo = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT id FROM modelos WHERE descripcion = '" + datos.getString("modelo") + "' and idmarca=" + idmarca));
                        if (idmodelo == 0) {
                            sql = "INSERT INTO modelos (idmarca, descripcion) VALUES(" + idmarca + ",'" + datos.getString("modelo") + "');";
                            Globales.DatosAuditoria(sql, "MODELO", "AGREGAR", idusuario, iplocal, this.getClass() + "-->AgregarSolicitud");
                            sql = "SELECT id FROM modelos WHERE descripcion = '" + datos.getString("modelo") + "' and idmarca=" + idmarca;
                            idmodelo = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
                        }

                        for (int x = 1; x <= cantidad; x++) {

                            textosql += "INSERT INTO cotizacion_detalle(idcotizacion, idequipo, ingreso, idmodelo, idintervalo, idintervalo2, serie, metodo, punto, direccion, declaracion, certificado, proxima, "
                                    + "cantidad, observacion, idusuario, idcliente, idcontacto, idsede, nombreca, lote, idsolicitud) "
                                    + "SELECT 0, " + idequipo + ",ingreso, " + idmodelo + ", idintervalo, 0, serie, -1, punto, direccion, declaracion, certificado, proxima, "
                                    + "1,observacion," + idusuario + "," + cliente + "," + idcontacto + "," + idsede + ",nombreca," + lote + "," + datos.getString("id")
                                    + "FROM  web.solicitud_detalle  WHERE id = " + datos.getString("id") + "";
                        }
                    }
                    if (lote > 0) {
                        textosql += "UPDATE contadores SET lote = " + lote;
                    }
                    Globales.DatosAuditoria(textosql, "COTIZACION", "AGREGAR SOLICITUD " + solicitud, idusuario, iplocal, this.getClass() + "-->AgregarSolicitud");
                }
                Globales.DatosAuditoria("UPDATE web.solicitud set estado='En Cotizacion' WHERE solicitud=" + solicitud, "SOLICITUD", "ACTUALIZADO ESTADO SOLICITUD", idusuario, iplocal, this.getClass() + "-->AgregarSolicitud");
                return "0|Solicitud agregada con éxito";
            }
        } catch (SQLException ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String DuplicarRegistro(HttpServletRequest request) {
        int id = Globales.Validarintnonull(request.getParameter("id"));
        String estado = Globales.ObtenerUnValor("select c.estado from cotizacion c inner join cotizacion_detalle cd on cd.idcotizacion = c.id and cd.id = " + id);
        if (!estado.equals("Cotizado") && !estado.equals("Temporal") && !estado.equals("")) {
            return "1|No se puede agregar un ingreso en estado " + estado;
        }
        sql = "INSERT INTO cotizacion_detalle(ingreso, idcotizacion, idequipo, idmodelo, idintervalo, idintervalo2, cantidad, serie, idcliente, idusuario, idsede, idcontacto, remision) "
                + "SELECT ingreso, idcotizacion, idequipo, idmodelo, idintervalo, idintervalo2, cantidad, serie, idcliente, idusuario, idsede, idcontacto, remision "
                + "FROM cotizacion_detalle cd "
                + "WHERE cd.id = " + id;
        Globales.DatosAuditoria(sql, "COTIZACION", "DUPLICAR ITEM", idusuario, iplocal, this.getClass() + "-->DuplicarRegistro");
        return "0|Registro duplicado con éxito";
    }

    public String ConsultarOrden(HttpServletRequest request) {
        int remision = Globales.Validarintnonull(request.getParameter("remision"));
        String orden = request.getParameter("orden");
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        String ingresos = request.getParameter("ingresos");
        String tipo = request.getParameter("tipo");

        String fechad = request.getParameter("fechad");
        String fechah = request.getParameter("fechah");
        int magnitud = Globales.Validarintnonull(request.getParameter("magnitud"));
        String equipo = request.getParameter("equipo");
        int marca = Globales.Validarintnonull(request.getParameter("marca"));
        String modelo = request.getParameter("modelo");
        String intervalo = request.getParameter("intervalo");

        String serie = request.getParameter("serie");

        int ingreso = Globales.Validarintnonull(ingresos);
        String[] a_ingreso = ingresos.split("\\-");
        String busqueda = "WHERE 1=1 ";

        if (remision > 0) {
            busqueda += " AND r.remision = " + remision;
        } else if (!orden.trim().equals("")) {
            busqueda += " AND oc.numero ilike '%" + orden + "%'";
        } else if (ingreso > 0) {
            busqueda += " AND rd.ingreso = " + ingreso;
        } else if (a_ingreso.length > 1) {
            if (Globales.Validarintnonull(a_ingreso[0]) > 0 && Globales.Validarintnonull(a_ingreso[1]) > 0) {
                busqueda += " AND rd.ingreso >= " + a_ingreso[0] + " and rd.ingreso <= " + a_ingreso[1];
            }
        } else {
            busqueda += " AND to_char(oc.fecha,'yyyy-MM-dd') >= '" + fechad + "' and to_char(oc.fecha,'yyyy-MM-dd') <= '" + fechah + "'";
            if (cliente > 0) {
                busqueda += " AND r.idcliente = " + cliente;
            }
            if (!tipo.equals("Todas")) {
                busqueda += " AND oc.tipo = '" + tipo + "'";
            }
            if (magnitud > 0) {
                busqueda += " AND e.idmagnitud = " + magnitud;
            }
            if (!equipo.equals("")) {
                busqueda += " AND e.descripcion = '" + equipo + "'";
            }
            if (marca > 0) {
                busqueda += " AND mo.idmarca = " + marca;
            }
            if (!modelo.equals("")) {
                busqueda += " AND mo.descripcion = '" + modelo + "'";
            }
            if (!intervalo.equals("")) {
                busqueda += " AND '(' || desde || ' a ' || hasta || ') ' || medida = '" + intervalo + "'";
            }
            if (!serie.trim().equals("")) {
                busqueda += " AND rd.serie ilike '%" + serie.trim() + "%'";
            }
        }
        sql = "select row_number() OVER(order by rd.ingreso) as fila, oc.ncotizacion || '<br>' || co.estado as ncotizacion, \n"
                + "rd.observacion, rd.cantidad, rd.ingreso, c.nombrecompleto || '<br><b>' || c.documento as cliente, rd.facturado as factura,\n"
                + "ma.descripcion || '<br>' || m.descripcion || '<br>' || mo.descripcion || '<br> ' || case when rd.idintervalo = 0 then 'VER ESPECIFICACIONES' ELSE '(' || desde || ' a ' || hasta || ') ' || medida end || '<br>' || rd.serie as equipo,\n"
                + "'<b>' || r.remision || '</b><br>' || to_char(rd.fechaing, 'dd/MM/yyyy HH24:MI') || '<br>' || u.nombrecompleto as remision, \n"
                + "oc.numero, oc.tipo, to_char(oc.fecha, 'yyyy/MM/dd HH24:MI') as fechacom, \n"
                + "uc.nombrecompleto as usuariocompra,\n"
                + "'<button class=''btn btn-glow btn-success'' title=''Ver PDF de Orden de Compra'' type=''button'' onclick=' || chr(34) || 'VerOrdenCompra(''' || oc.numero || ''')' ||chr(34) || '><span data-icon=''&#xe2c7;''></span></button>' as ver,\n"
                + "'<button class=''btn btn-glow btn-danger'' title=''Eliminar Orden de Compra'' type=''button'' onclick=' || chr(34) || 'EliminarOrden(' || oc.id || ',''' || quitarcaracter(e.descripcion || ' ' || m.descripcion || ' ' || mo.descripcion) || ''',''' || oc.numero || ''',' || rd.ingreso || ')' ||chr(34) || '><span data-icon=''&#xe0d8;''></span></button>' as eliminar\n"
                + "\n"
                + "from remision_detalle rd INNER JOIN remision r on rd.idremision = r.id\n"
                + "inner join equipo e on rd.idequipo = e.id\n"
                + "inner join magnitudes m on m.id = e.idmagnitud\n"
                + "inner join modelos mo on mo.id = rd.idmodelo\n"
                + "inner join seguridad.rbac_usuario ur on ur.idusu = r.idusuario\n"
                + "inner join marcas ma on ma.id = mo.idmarca \n"
                + "inner join magnitud_intervalos mi on mi.id = rd.idintervalo  \n"
                + "inner join seguridad.rbac_usuario u on u.idusu = r.idusuario\n"
                + "inner join orden_compra oc on oc.ingreso = rd.ingreso \n"
                + "inner join seguridad.rbac_usuario uc on uc.idusu = oc.idusuario \n"
                + "INNER JOIN clientes c on c.id = r.idcliente\n"
                + "LEFT JOIN cotizacion co on co.cotizacion = oc.ncotizacion " + busqueda + " \n"
                + "ORDER BY rd.ingreso desc";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ConsultarOrden");
    }

    public String GuardarMetodo(HttpServletRequest request) {
        String descripcion = request.getParameter("descripcion");
        int equipo = Globales.Validarintnonull(request.getParameter("equipo"));
        int idmetodo = 0;

        sql = "SELECT min(id) from metodo where descripcion = '" + descripcion + "'";
        idmetodo = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
        if (idmetodo == 0) {
            if (Globales.PermisosSistemas("METODO GUARDAR", idusuario) == 0) {
                return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
            }

            sql = "INSERT INTO metodo (descripcion) "
                    + "VALUES ('" + descripcion + "');";
            if (Globales.DatosAuditoria(sql, "METODO", "GUARDAR", idusuario, iplocal, this.getClass() + "-->GuardarMetodo")) {
                sql = "select MAX(id) FROM metodo";
                idmetodo = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }
        }

        sql = "SELECT idmetequipo from metodo_equipo where idmetodo = " + idmetodo + " and idequipo = " + equipo;
        int id = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
        if (id > 0) {
            return "1|Este método ya se encuentra registrado y asignado a este equipo";
        } else {
            if (Globales.PermisosSistemas("METODO AGREGAR EQUIPO", idusuario) == 0) {
                return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
            }
            sql = "INSERT INTO metodo_equipo(idequipo, idmetodo) "
                    + "VALUES (" + equipo + "," + idmetodo + ")";
            return "0|" + Globales.DatosAuditoria(sql, "METODO", "AGREGAR EQUIPO", idusuario, iplocal, this.getClass() + "-->GuardarMetodo") + "|" + idmetodo;
        }
    }

    public String GuardarOpcion(HttpServletRequest request) {
        String descripcion = request.getParameter("descripcion");
        int tipo = Globales.Validarintnonull(request.getParameter("tipo"));
        int magnitud = Globales.Validarintnonull(request.getParameter("magnitud"));
        int marca = Globales.Validarintnonull(request.getParameter("marca"));
        int tipoconcepto = Globales.Validarintnonull(request.getParameter("tipoconcepto"));
        int codigo = 0;
        int id = 0;
        int codigomax = 0;
        String resultado = "";

        Configuracion config = new Configuracion();

        switch (tipo) {
            case 1:

                if (Globales.PermisosSistemas("EQUIPOS GUARDAR", idusuario) == 0) {
                    return "XX|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }

                descripcion = descripcion.trim().toUpperCase();
                id = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT id FROM equipo WHERE descripcion = '" + descripcion + "' and idmagnitud = " + magnitud));
                if (id > 0) {
                    return "XX|Descripcíon ya existe como equipo";
                }
                codigo = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT codigoini FROM magnitudes WHERE id = " + magnitud));
                codigomax = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT max(codigo) FROM equipo WHERE idmagnitud = " + magnitud)) + 1;
                if (codigomax > codigo) {
                    codigo = codigomax;
                }
                sql = "INSERT INTO equipo (codigo, descripcion, idmagnitud) VALUES ('" + codigo + "','" + descripcion + "'," + magnitud + ")";
                Globales.DatosAuditoria(sql, "EQUIPO", "AGREGAR", idusuario, iplocal, this.getClass() + "-->GuardarOpcion");
                id = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT id FROM equipo WHERE descripcion = '" + descripcion + "' and idmagnitud=" + magnitud));
                resultado = id + "|" + config.CargarCombos(null, 2, 1, String.valueOf(magnitud), 0);
                break;
            case 2:
                if (Globales.PermisosSistemas("MARCA GUARDAR", idusuario) == 0) {
                    return "XX|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                descripcion = descripcion.trim().toUpperCase();
                id = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT id FROM marcas WHERE descripcion = '" + descripcion + "'"));
                if (id > 0) {
                    return "XX|Descripcíon ya existe como marca";
                }
                sql = "INSERT INTO marcas (descripcion) VALUES ('" + descripcion + "')";
                Globales.DatosAuditoria(sql, "MARCA", "GUARDAR", usuario, iplocal, this.getClass() + "-->GuardarOpcion");
                id = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT id FROM marcas WHERE descripcion = '" + descripcion + "'"));
                resultado = id + "|" + config.CargarCombos(null, 3, 1, "", 0);
                break;
            case 3:
                if (Globales.PermisosSistemas("MODELO GUARDAR", idusuario) == 0) {
                    return "XX|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                descripcion = descripcion.trim().toUpperCase();
                id = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT id FROM modelos WHERE descripcion = '" + descripcion + "' and idmarca = " + marca));
                if (id > 0) {
                    return "XX|Descripcíon ya existe modelo";
                }
                sql = "INSERT INTO modelos (descripcion, idmarca) VALUES ('" + descripcion + "'," + marca + ")";
                Globales.DatosAuditoria(sql, "MODELO", "AGREGAR", idusuario, iplocal, this.getClass() + "-->GuardarOpcion");
                id = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT id FROM modelos WHERE descripcion = '" + descripcion + "' and idmarca = " + marca));
                resultado = id + "|" + config.CargarCombos(null, 5, 1, String.valueOf(marca), 0);
                break;
            case 4:
                if (Globales.PermisosSistemas("MEDIDA GUARDAR", idusuario) == 0) {
                    return "XX|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                descripcion = descripcion.trim();
                id = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT id FROM medidas WHERE descripcion = '" + descripcion + "'"));
                if (id > 0) {
                    return "XX|Descripcíon ya existe como medida";
                }
                sql = "INSERT INTO medidas (descripcion) VALUES ('" + descripcion + "')";
                Globales.DatosAuditoria(sql, "MEDIDA", "GUARDAR", idusuario, iplocal, this.getClass() + "-->GuardarOpcion");
                id = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT id FROM medidas WHERE descripcion = '" + descripcion + "'"));
                resultado = id + "|" + config.CargarCombos(null, 8, 1, "", 0);
                break;
            case 5:
                descripcion = descripcion.trim().toUpperCase();
                id = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT id FROM accesorios WHERE descripcion = '" + descripcion + "'"));
                if (id > 0) {
                    return "XX|Descripcíon ya existe como accesorio";
                }
                sql = "INSERT INTO accesorios (descripcion) VALUES ('" + descripcion + "')";
                Globales.DatosAuditoria(sql, "ACCESORIOS", "GUARDAR", idusuario, iplocal, this.getClass() + "-->GuardarOpcion");
                id = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT id FROM accesorios WHERE descripcion = '" + descripcion + "'"));
                resultado = descripcion + "|" + config.CargarCombos(null, 33, 1, "", 0);
                break;
            case 6:
                descripcion = descripcion.trim().toUpperCase();
                id = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT id FROM obser_ingreso WHERE descripcion = '" + descripcion + "'"));
                if (id > 0) {
                    return "XX|Descripcíon ya existe comoobservación";
                }
                sql = "INSERT INTO obser_ingreso (descripcion) VALUES ('" + descripcion + "')";
                Globales.DatosAuditoria(sql, "OBSERVACION", "GAURDAR", idusuario, iplocal, this.getClass() + "-->GuardarOpcion");
                id = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT id FROM obser_ingreso WHERE descripcion = '" + descripcion + "'"));
                resultado = descripcion + "|" + config.CargarCombos(null, 34, 1, "", 0);
                break;
            case 7:
                if (Globales.PermisosSistemas("CONCEPTO DE CAJA GUARDAR", idusuario) == 0) {
                    return "XX|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                descripcion = descripcion.trim().toUpperCase();
                id = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT id FROM concepto_cm WHERE descripcion = '" + descripcion + "' and tipo=" + tipoconcepto));
                if (id > 0) {
                    return "XX|Descripcíon ya existe como concepto";
                }
                sql = "INSERT INTO concepto_cm (descripcion, tipo) VALUES ('" + descripcion + "'," + tipoconcepto + ")";
                Globales.DatosAuditoria(sql, "CONCEPTO", "GUARDAR", idusuario, iplocal, this.getClass() + "-->GuardarOpcion");
                id = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT id FROM concepto_cm WHERE descripcion = '" + descripcion + "' and tipo=" + tipoconcepto));
                resultado = descripcion + "|" + config.CargarCombos(null, 46, 1, String.valueOf(tipoconcepto), 0);
                break;
            case 8:
                if (Globales.PermisosSistemas("GRUPO GUARDAR", idusuario) == 0) {
                    return "XX|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                descripcion = descripcion.trim().toUpperCase();
                id = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT id FROM grupos WHERE descripcion = '" + descripcion + "'"));
                if (id > 0) {
                    return "XX|Descripcíon ya existe como grupo";
                }
                sql = "INSERT INTO grupos (descripcion) VALUES ('" + descripcion + "')";
                Globales.DatosAuditoria(sql, "GRUPO", "GUARDAR", idusuario, iplocal, this.getClass() + "-->GuardarOpcion");
                id = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT id FROM grupos WHERE descripcion = '" + descripcion + "'"));
                resultado = id + "|" + config.CargarCombos(null, 47, 1, String.valueOf(tipoconcepto), 0);
                break;
            case 9:
                if (Globales.PermisosSistemas("CARGO GUARDAR", idusuario) == 0) {
                    return "XX|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                descripcion = descripcion.trim().toUpperCase();
                id = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT id FROM rrhh.cargo WHERE descripcion = '" + descripcion + "'"));
                if (id > 0) {
                    return "XX|Descripcíon ya existe como cargo";
                }
                sql = "INSERT INTO rrhh.cargo (descripcion) VALUES ('" + descripcion + "')";
                Globales.DatosAuditoria(sql, "CARGO", "GUARDAR", idusuario, iplocal, this.getClass() + "-->GuardarOpcion");
                id = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT id FROM rrhh.cargo WHERE descripcion = '" + descripcion + "'"));
                resultado = id + "|" + config.CargarCombos(null, 26, 1, String.valueOf(tipoconcepto), 0);
                break;
        }
        return resultado;
    }

    public String GuardarIntervalo(HttpServletRequest request) {
        int magnitud = Globales.Validarintnonull(request.getParameter("magnitud"));
        String medida = request.getParameter("medida");
        String desde = request.getParameter("desde");
        String hasta = request.getParameter("hasta");

        sql = "SELECT id from magnitud_intervalos "
                + "where idmagnitud = " + magnitud + " and medida = '" + medida + "' and desde = '" + desde + "' and hasta = '" + hasta + "'";
        int id = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));

        if (id > 0) {
            return "XX";
        }

        sql = "INSERT INTO magnitud_intervalos(idmagnitud, medida, desde, hasta) "
                + "VALUES (" + magnitud + ",'" + medida + "','" + desde + "','" + hasta + "');";
        Globales.DatosAuditoria(sql, "INTERVALOS", "AGREGAR", idusuario, iplocal, this.getClass() + "-->GuardarIntervalo");
        sql = "SELECT  MAX(id) FROM magnitud_intervalos WHERE idmagnitud = " + magnitud;
        return Globales.ObtenerUnValor(sql);
    }

    public String GuardarTmpCotizacion(HttpServletRequest request) {
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        int id = Globales.Validarintnonull(request.getParameter("id"));
        int lote = Globales.Validarintnonull(request.getParameter("lote"));
        int cotizacion = Globales.Validarintnonull(request.getParameter("cotizacion"));
        int equipo = Globales.Validarintnonull(request.getParameter("equipo"));
        int modelo = Globales.Validarintnonull(request.getParameter("modelo"));
        String observacion = request.getParameter("observacion");
        String serie = request.getParameter("serie");
        int ingreso = Globales.Validarintnonull(request.getParameter("ingreso"));
        int cantidad = Globales.Validarintnonull(request.getParameter("cantidad"));
        int intervalo = Globales.Validarintnonull(request.getParameter("intervalo"));
        int intervalo2 = Globales.Validarintnonull(request.getParameter("intervalo2"));
        int servicio = Globales.Validarintnonull(request.getParameter("servicio"));
        int metodo = Globales.Validarintnonull(request.getParameter("metodo"));
        String punto = request.getParameter("punto");
        String calibracion = request.getParameter("calibracion");
        String nombreca = request.getParameter("nombreca");

        String declaracion = request.getParameter("declaracion");
        String direccion = request.getParameter("direccion");
        String certificado = request.getParameter("certificado");
        int descuento = Globales.Validarintnonull(request.getParameter("descuento"));
        double precio = Globales.ValidardoublenoNull(request.getParameter("precio"));
        int entrega = Globales.Validarintnonull(request.getParameter("entrega"));
        int proveedor = Globales.Validarintnonull(request.getParameter("proveedor"));
        int iva = Globales.Validarintnonull(request.getParameter("iva"));
        int express = Globales.Validarintnonull(request.getParameter("express"));
        int sitio = Globales.Validarintnonull(request.getParameter("sitio"));

        int sede = Globales.Validarintnonull(request.getParameter("sede"));
        int contacto = Globales.Validarintnonull(request.getParameter("contacto"));
        int remision = Globales.Validarintnonull(request.getParameter("remision"));
        String pagocertificado = request.getParameter("pagocertificado");
        int idsolicitud = Globales.Validarintnonull(request.getParameter("idsolicitud"));
        double costo = Globales.ValidardoublenoNull(request.getParameter("costo"));
        String calibracionadic = request.getParameter("calibracionadic");
        double precioadic = Globales.ValidardoublenoNull(request.getParameter("precioadic"));

        String mensaje = "";
        int serviciodep = 0;
        int idcotizacion = 0;
        int orden = 0;

        try {

            sql = "SELECT estado FROM cotizacion where id = " + cotizacion;
            String estado = Globales.ObtenerUnValor(sql).trim();
            if (!estado.equals("Temporal") && !estado.equals("Cotizado") && !estado.equals("")) {
                return "1|No se puede editar una cotizacion en estado " + estado;
            }

            if (id > 0) {
                sql = "SELECT orden FROM cotizacion_detalle where id = " + id;
                orden = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
                if (orden == 0) {
                    sql = "SELECT max(orden) + 1 FROM cotizacion_detalle";
                    orden = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
                }
            } else {
                sql = "SELECT max(orden) + 1 FROM cotizacion_detalle";
                orden = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
            }

            if (cantidad > 1 || lote > 0) {
                serie = "NA";

                if (lote > 0) {
                    sql = "SELECT id from cotizacion_detalle where lote = " + lote;
                    datos = Globales.Obtenerdatos(sql, this.getClass() + "-->GuardarTmpCotizacion", 1);

                    sql = "";
                    while (datos.next()) {
                        if (Globales.Validarintnonull(datos.getString("id")) > 0) {
                            sql += "DELETE FROM cotizacion_detalle where idserviciodep = " + datos.getString("id") + " and idserviciodep > 0 and idcotizacion=" + cotizacion + ";";
                        }
                    }

                    sql += "delete from cotizacion_detalle WHERE lote = " + lote + " and idcotizacion = " + cotizacion + ";";
                    Globales.Obtenerdatos(sql, this.getClass() + "-->GuardarTmpCotizacion", 2);
                }
                if (cantidad > 1) {
                    sql = "DELETE FROM cotizacion_detalle where idserviciodep = " + id + " and idserviciodep > 0 and idcotizacion=" + cotizacion + ";";
                    sql += "delete from cotizacion_detalle WHERE id = " + id + ";";
                    Globales.Obtenerdatos(sql, this.getClass() + "-->GuardarTmpCotizacion", 2);
                    lote = Contadores(null, 4) + 1;
                } else {
                    lote = 0;
                }
                id = 0;
            } else {
                lote = 0;
            }

            if (id == 0) {
                if (equipo == 0) {
                    serviciodep = 99999;
                }

                sql = "";

                if (metodo != 0) {
                    if (metodo == -1) {
                        metodo = 0;
                    }
                    for (int x = 1; x <= cantidad; x++) {
                        if (Globales.PermisosSistemas("COTIZACION AGREGAR ITEMS", idusuario) == 0) {
                            return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                        }

                        sql += "INSERT INTO cotizacion_detalle(idcotizacion, idcliente, idequipo, idmodelo, idintervalo, idintervalo2, serie, idservicio, metodo, punto, direccion, declaracion, "
                                + "                    certificado, proxima, cantidad, observacion, descuento, costo, precio, idusuario, entrega, idproveedor, iva,ingreso, "
                                + "     idcontacto, idsede, remision, idserviciodep,nombreca, lote, idsolicitud, calibracionadic, precioadic, orden) "
                                + " VALUES (" + cotizacion + "," + cliente + "," + equipo + "," + modelo + "," + intervalo + "," + intervalo2 + ",'" + serie + "'," + servicio + "," + metodo + ",'"
                                + punto + "','" + direccion + "','" + declaracion + "','" + certificado + "','" + calibracion + "',"
                                + "1" + ",'" + observacion + "'," + descuento + "," + costo + "," + precio + "," + idusuario + "," + entrega + "," + proveedor + "," + iva + "," + ingreso
                                + "," + contacto + "," + sede + "," + remision + "," + serviciodep + ",'" + nombreca + "'," + lote + "," + idsolicitud + ",'" + calibracionadic + "'," + precioadic + "," + orden + ");";
                    }
                    sql += (lote > 0 ? "update contadores set lote = " + lote + ";" : "");
                    if (Globales.DatosAuditoria(sql, "COTIZACION", "AGREGAR ITEMS", idusuario, iplocal, this.getClass() + "-->GuardarTmpCotizacion")) {
                        sql = "select max(id) from cotizacion_detalle where idcliente = " + cliente + ";";
                        id = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
                        mensaje = "0|Servicio agregado|" + (lote == 0 ? id : lote);
                    } else {
                        return "1|Error inesperado... Revise el log de errores";
                    }
                } else {
                    if (Globales.PermisosSistemas("COTIZACION AGREGAR ITEMS", idusuario) == 0) {
                        return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                    }
                    sql += "INSERT INTO cotizacion_detalle(idcotizacion, idcliente, idequipo, idmodelo, idintervalo, idintervalo2, serie, idservicio, metodo, punto, direccion, declaracion, "
                            + "                     certificado, proxima, cantidad, observacion, descuento, costo, precio, idusuario, entrega, idproveedor, iva,ingreso, "
                            + "     idcontacto, idsede, remision, idserviciodep,nombreca, lote, idsolicitud, calibracionadic, precioadic, orden) "
                            + " VALUES (" + cotizacion + "," + cliente + "," + equipo + "," + modelo + "," + intervalo + "," + intervalo2 + ",'" + serie + "'," + servicio + "," + metodo + ",'"
                            + punto + "','" + direccion + "','" + declaracion + "','" + certificado + "','" + calibracion + "',"
                            + cantidad + ",'" + observacion + "'," + descuento + "," + costo + "," + precio + "," + idusuario + "," + entrega + "," + proveedor + "," + iva + "," + ingreso
                            + "," + contacto + "," + sede + "," + remision + "," + serviciodep + ",'" + nombreca + "'," + lote + "," + idsolicitud + ",'" + calibracionadic + "'," + precioadic + "," + orden + ");";
                    if (Globales.DatosAuditoria(sql, "COTIZACION", "AGREGAR ITEMS", idusuario, iplocal, this.getClass() + "-->GuardarTmpCotizacion")) {
                        sql = "select max(id) from cotizacion_detalle where idcliente = " + cliente + ";";
                        id = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
                        mensaje = "0|Servicio agregado|" + id;
                    } else {
                        return "1|Error inesperado... Revise el log de errores";
                    }
                }
            } else {

                if (ingreso > 0) {
                    if (metodo == -1) {
                        metodo = 0;
                    }
                    String fecha = Globales.ObtenerUnValor("SELECT to_char(fechaing, 'yyyy-MM-dd HH24:MI') FROM remision_detalle WHERE ingreso = " + ingreso).trim();
                    sql = "SELECT idservicio FROM cotizacion_detalle WHERE id = " + id;
                    int idmetodo = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
                    if (idmetodo == 0) {
                        sql = "INSERT INTO ingreso_estados(ingreso, idusuario, descripcion, tiempo) "
                                + " VALUES (" + ingreso + "," + idusuario + ",'COTIZACION INGRESADA',tiempo('" + fecha + "', now(),1));";
                    } else {
                        sql = "INSERT INTO ingreso_estados(ingreso, idusuario, descripcion, tiempo) "
                                + " VALUES (" + ingreso + "," + idusuario + ",'COTIZACION ACTUALIZADA',tiempo('" + fecha + "', now(),1));";
                    }

                    sql += " UPDATE cotizacion_detalle SET idservicio = " + servicio + ", metodo = " + metodo + ", punto = '" + punto + "',direccion='"
                            + direccion + "', declaracion='" + declaracion + "', certificado = '" + certificado + "', proxima='" + calibracion + "',nombreca='" + nombreca
                            + "',precio=" + precio + ",descuento=" + descuento + ",cantidad=" + cantidad + ", idproveedor = " + proveedor + ",iva=" + iva
                            + ", observacion='" + observacion + "', idusuario= " + idusuario + ", entrega=" + entrega + ", fechareg = now(), "
                            + " calibracionadic='" + calibracionadic + "', precioadic=" + precioadic + " WHERE id = " + id;
                } else {
                    if (Globales.PermisosSistemas("COTIZACION ACTUALIZAR ITEMS", idusuario) == 0) {
                        return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                    }
                    if (metodo != 0) {
                        if (metodo == -1) {
                            metodo = 0;
                        }
                        sql = "UPDATE cotizacion_detalle SET idequipo=" + equipo + ",idmodelo=" + modelo + ",idintervalo=" + intervalo + ",idintervalo2=" + intervalo2 + ",serie='" + serie + "', idservicio = " + servicio + ", metodo = " + metodo + ", punto = '" + punto + "',direccion='"
                                + direccion + "', declaracion='" + declaracion + "', certificado = '" + certificado + "', proxima='" + calibracion + "', nombreca='" + nombreca
                                + "',costo=" + costo + ",precio=" + precio + ",descuento=" + descuento + ",cantidad=1, idproveedor = " + proveedor + ",iva=" + iva
                                + ", observacion='" + observacion + "', idusuario= " + idusuario + ", entrega=" + entrega + ", fechareg = now() "
                                + ",calibracionadic='" + calibracionadic + "', precioadic=" + precioadic + " WHERE case when " + cantidad + " = 1 then id else lote end = " + id;
                    } else {
                        sql = "UPDATE cotizacion_detalle SET idequipo=" + equipo + ",idmodelo=" + modelo + ",idintervalo=" + intervalo + ",idintervalo2=" + intervalo2 + ",serie='" + serie + "', idservicio = " + servicio + ", metodo = " + metodo + ", punto = '" + punto + "',direccion='"
                                + direccion + "', declaracion='" + declaracion + "', certificado = '" + certificado + "', proxima='" + calibracion + "', nombreca='" + nombreca
                                + "',costo=" + costo + ",precio=" + precio + ",descuento=" + descuento + ", idproveedor = " + proveedor + ",iva=" + iva + ",cantidad=" + cantidad
                                + ", observacion='" + observacion + "', idusuario= " + idusuario + ", entrega=" + entrega + ", fechareg = now(), "
                                + "calibracionadic='" + calibracionadic + "', precioadic=" + precioadic + " WHERE id = " + id;
                    }
                }

                if (Globales.DatosAuditoria(sql, "COTIZACION", "ACTUALIZAR ITEMS COTIZACION", idusuario, iplocal, this.getClass() + "-->GuardarTmpCotizacion")) {
                    mensaje = "0|Servicio actualizado|" + id;
                } else {
                    return "1|Error inesperado... Revise el log de errores";
                }
            }

            Globales.DatosAuditoria("UPDATE cotizacion set estado = 'Temporal' where id =" + cotizacion, "COTIZACION", "ACTUALIZAR ESTADO", idusuario, iplocal, this.getClass() + "-->GuardarTmpCotizacion");

            sql = "";
            if (ingreso > 0) {
                sql = "UPDATE reporte_ingreso r SET cotizado = 0 "
                        + "from cotizacion_detalle cd "
                        + "WHERE inicial = 0 and cd.ingreso = r.ingreso and cd.idserviciodep > 0 and cd.idservicio in (4,6,9) and cd.ingreso = " + ingreso + ";";
            }

            if (lote == 0 && id > 0) {
                sql += "DELETE FROM cotizacion_detalle WHERE idserviciodep = " + id + " AND inicial = 0 and idcotizacion=" + cotizacion + ";";
            }
            Globales.Obtenerdatos(sql, this.getClass() + "-->GuardarTmpCotizacion", 2);

            if (sitio > 0) {
                sql = "SELECT id FROM cotizacion_detalle WHERE idservicio = " + sitio + " and idserviciodep = " + id + " AND idequipo = " + equipo + " and metodo = " + metodo
                        + " and idcliente = " + cliente + " and idcotizacion = " + cotizacion;
                int idsitio = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
                int porcentaje = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT porcentaje FROM otros_servicios where id = " + sitio));
                if (idsitio == 0) {
                    sql = "INSERT INTO cotizacion_detalle( "
                            + "  idcotizacion, idequipo, ingreso, idmodelo, idintervalo,idintervalo2, idservicio, idserviciodep, cantidad, serie, precio, idusuario, idcliente, estado, idcontacto, idsede, remision, iva, idsolicitud, orden, lote) "
                            + " SELECT " + cotizacion + ", idequipo, ingreso, idmodelo, idintervalo, idintervalo2," + sitio + "," + id + "," + cantidad + ", serie, " + Math.round(precio * porcentaje) / 100 + ", idusuario, idcliente, 'Temporal', idcontacto, idsede, remision, iva, idsolicitud," + orden + "," + lote
                            + " FROM cotizacion_detalle "
                            + " WHERE id = " + id;
                }
                Globales.DatosAuditoria(sql, "COTIZACION", "AGREGANDO ITEMS SITIO", idusuario, iplocal, this.getClass() + "-->GuardarTmpCotizacion");
            }

            if (express > 0) {

                sql = "SELECT id FROM cotizacion_detalle WHERE idservicio = " + express + " and idserviciodep = " + id + " AND idequipo = " + equipo + " and metodo = " + metodo
                        + " and idcliente = " + cliente + " and idcotizacion = " + cotizacion;
                int idsitio = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
                int porcentaje = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT porcentaje FROM otros_servicios where id = " + express));
                if (idsitio == 0) {
                    sql = "INSERT INTO cotizacion_detalle( "
                            + "  idcotizacion, idequipo, ingreso, idmodelo, idintervalo, idintervalo2, idservicio, idserviciodep, cantidad, serie, precio, idusuario, idcliente, estado, idcontacto, idsede, remision, iva, idsolicitud, orden, lote) "
                            + " SELECT " + cotizacion + ", idequipo, ingreso, idmodelo, idintervalo, idintervalo2," + express + "," + id + "," + cantidad + ", serie, " + Math.round(precio * porcentaje) / 100 + ", idusuario, idcliente, 'Temporal', idcontacto, idsede, remision, iva, idsolicitud, " + orden + "," + lote
                            + " FROM cotizacion_detalle "
                            + " WHERE id = " + id;
                }
                Globales.DatosAuditoria(sql, "COTIZACION", "AGREGANDO ITEMS EXPRESS", idusuario, iplocal, this.getClass() + "-->GuardarTmpCotizacion");

            }

            if (certificado.equals("IMPRESO") && pagocertificado.equals("SI")) {
                sql = "SELECT id FROM cotizacion_detalle WHERE idservicio = 3 and idserviciodep = " + id + " AND idequipo = " + equipo + " and metodo = " + metodo
                        + " and idcliente = " + cliente + " and idcotizacion = " + cotizacion;
                int idsitio = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
                double precios = Globales.ValidardoublenoNull(Globales.ObtenerUnValor("SELECT precio FROM otros_servicios where id = 3"));
                if (idsitio == 0) {
                    sql = "INSERT INTO cotizacion_detalle( "
                            + "  idcotizacion, idequipo, ingreso, idmodelo, idintervalo, idintervalo2, idservicio, idserviciodep, cantidad, serie, precio, idusuario, idcliente, estado, idcontacto, idsede, remision, iva, idsolicitud, orden, lote) "
                            + " SELECT " + cotizacion + ", idequipo, ingreso, idmodelo, idintervalo, idintervalo2, 3," + id + "," + cantidad + ", serie, " + precios + ", idusuario, idcliente, 'Temporal', idcontacto, idsede, remision, iva, idsolicitud, " + orden + "," + lote
                            + " FROM cotizacion_detalle "
                            + " WHERE id = " + id;
                }
                Globales.DatosAuditoria(sql, "COTIZACION", "AGREGANDO ITEMS PAGO CERTIFICADO", idusuario, iplocal, this.getClass() + "-->GuardarTmpCotizacion");
            }

            if (ingreso > 0) {
                sql = "SELECT rp.id, idconcepto, concepto, ajuste, suministro, rp.observacion, rp.cotizado, rp.nroreporte, paginformetecnico "
                        + " FROM reporte_ingreso rp inner join remision_detalle rd on rd.ingreso = rp.ingreso "
                        + "                      inner join clientes c on c.id = rd.idcliente "
                        + " where idconcepto<> 1 and idconcepto<> 4 and rp.estado <> 'Anulado' and rp.cotizado = 0 AND rp.ingreso = " + ingreso + " ORDER BY idconcepto";
                datos = Globales.Obtenerdatos(sql, this.getClass() + "-->GuardarTmpCotizacion", 1);
                sql = "";
                idcotizacion = 0;
                while (datos.next()) {
                    if (!datos.getString("ajuste").trim().equals("")) {
                        sql = "INSERT INTO cotizacion_detalle( "
                                + "      idcotizacion, idequipo, ingreso, idmodelo, idintervalo, idintervalo2, idservicio, idserviciodep, cantidad, serie, precio, idusuario, idcliente, estado, idcontacto, idsede, remision, iva,idsolicitud, observacion, orden, lote) "
                                + "  SELECT " + cotizacion + ", idequipo, ingreso, idmodelo, idintervalo, idintervalo2, 6," + servicio + ",1, serie, 0, idusuario, idcliente, 'Temporal', idcontacto, idsede, remision, iva, idsolicitud,'" + datos.getString("ajuste") + "'," + orden + "," + lote
                                + "  FROM cotizacion_detalle "
                                + "  WHERE id = " + id + ";select max(id) from cotizacion_detalle;";
                        idcotizacion = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
                        sql = "UPDATE reporte_ingreso set cotizado = " + idcotizacion + " WHERE id = " + datos.getString("id") + ";";
                        Globales.DatosAuditoria(sql, "COTIZACION", "AGREGANDO AJUSTE EQUIPO", idusuario, iplocal, this.getClass() + "-->GuardarTmpCotizacion");
                    }
                    if (!datos.getString("suministro").trim().equals("")) {
                        sql = "INSERT INTO cotizacion_detalle( "
                                + "      idcotizacion, idequipo, ingreso, idmodelo, idintervalo, idintervalo2, idservicio, idserviciodep, cantidad, serie, precio, idusuario, idcliente, estado, idcontacto, idsede, remision, iva, idsolicitud, observacion, orden, lote) "
                                + "  SELECT " + cotizacion + ", idequipo, ingreso, idmodelo, idintervalo,idintervalo2, 4," + servicio + ",1, serie, 0, idusuario, idcliente, 'Temporal', idcontacto, idsede, remision, iva, idsolicitud,'" + datos.getString("suministro") + "'," + orden + "," + lote
                                + "  FROM cotizacion_detalle "
                                + "  WHERE id = " + id + ";select max(id) from cotizacion_detalle;";
                        idcotizacion = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
                        sql = "UPDATE reporte_ingreso set cotizado = " + idcotizacion + " WHERE id = " + datos.getString("id") + ";";
                        Globales.DatosAuditoria(sql, "COTIZACION", "AGREGANDO SUMINISTRO EQUIPO", idusuario, iplocal, this.getClass() + "-->GuardarTmpCotizacion");
                    }
                    if (datos.getString("idconcepto").equals("3")) {
                        if (datos.getString("paginformetecnico").equals("SI")) {
                            sql = "INSERT INTO cotizacion_detalle( "
                                    + "  idcotizacion, idequipo, ingreso, idmodelo, idintervalo,idintervalo2, idservicio, idserviciodep, cantidad, serie, precio, idusuario, idcliente, estado, idcontacto, idsede, remision, iva, idsolicitud, observacion, orden, lote) "
                                    + " SELECT " + cotizacion + ", idequipo, ingreso, idmodelo, idintervalo,idintervalo2, 9," + servicio + ",1, serie, 0, idusuario, idcliente, 'Temporal', idcontacto, idsede, remision, iva, idsolicitud,'" + datos.getString("observacion") + "'," + orden + "," + lote
                                    + " FROM cotizacion_detalle "
                                    + " WHERE id = " + id + ";select max(id) from cotizacion_detalle;";
                            idcotizacion = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
                            sql = "UPDATE reporte_ingreso set cotizado = " + idcotizacion + " WHERE id = " + datos.getString("id") + ";";
                            sql += "DELETE FROM cotizacion_detalle WHERE id = " + id + ";";
                            sql += "DELETE FROM cotizacion_detalle WHERE ingreso = " + ingreso + " and idcotizacion = " + cotizacion + " and idserviciodep > 0 and idservicio <> 9;";
                            sql += "UPDATE ingreso_plantilla set calibracion = 2 WHERE ingreso = " + ingreso + ";";
                        } else {
                            sql = "DELETE FROM cotizacion_detalle WHERE ingreso = " + ingreso + " and idserviciodep > 0 and idcotizacion = " + cotizacion;
                        }

                        Globales.DatosAuditoria(sql, "COTIZACION", "AGREGANDO INFORME TECNICO", idusuario, iplocal, this.getClass() + "-->GuardarTmpCotizacion");
                    }

                }
            }
        } catch (Exception ex) {
            return "1|" + ex.getMessage() + " " + sql;
        }

        return mensaje;
    }

    public String ActualizarPrecio(HttpServletRequest request) {

        int cotizacion = Globales.Validarintnonull(request.getParameter("cotizacion"));
        double precio = Globales.ValidardoublenoNull(request.getParameter("precio"));
        double descuento = Globales.ValidardoublenoNull(request.getParameter("descuento"));
        int id = Globales.Validarintnonull(request.getParameter("id"));
        int lote = Globales.Validarintnonull(request.getParameter("lote"));

        if (Globales.PermisosSistemas("COTIZACION ACTUALIZAR PRECIO", idusuario) == 0) {
            return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
        }

        sql = "SELECT estado from cotizacion where cotizacion = " + cotizacion;
        String estado = Globales.ObtenerUnValor(sql);
        if (!estado.equals("Temporal") && !estado.equals("Cotizado") && !estado.equals("")) {
            return "1|No se puede editar un precio en estado " + estado;
        }

        try {

            if (Globales.PermisosSistemas("COTIZACION ACTUALIZAR PRECIO", idusuario) == 0) {
                return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
            }

            sql = "UPDATE cotizacion_detalle "
                    + "     SET descuento=" + descuento + ", precio=" + precio + " WHERE " + (lote > 0 ? "lote = " + lote : " id = " + id);
            Globales.DatosAuditoria(sql, "COTTIZACION", "ACTUALIZAR PRECIO", idusuario, iplocal, this.getClass() + "-->ActualizarPrecio");
            return "0|Precio actualizado con éxito";
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }

    }

    public String AnularCotizacion(HttpServletRequest request) {
        int cotizacion = Globales.Validarintnonull(request.getParameter("cotizacion"));
        String observaciones = request.getParameter("observaciones");
        try {
            if (Globales.PermisosSistemas("COTIZACION ANULAR", idusuario) == 0) {
                return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
            }
            sql = "SELECT estado FROM cotizacion where id = " + cotizacion;
            String estado = Globales.ObtenerUnValor(sql);
            if (!estado.equals("Cerrado") && !estado.equals("Cotizado")) {
                return "1|No se puede anular una cotización con estado " + estado;
            }

            sql = "SELECT count (rd.ingreso) "
                    + "  FROM cotizacion_detalle cd inner join remision_detalle rd on rd.ingreso = cd.ingreso "
                    + "                             inner join ingreso_plantilla ip on ip.ingreso = rd.ingreso "
                    + "  WHERE ip.reportado <> 0 and idcotizacion = " + cotizacion;
            int encontrado = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
            if (encontrado > 0) {
                return "1|No se puede anular esta cotización porque posee " + encontrado + " equipo(s) reportado(s) en el laboratorio";
            }

            sql = "UPDATE remision_detalle rd SET estado ='Ingresado', cotizado=0, fechaaprocoti= null "
                    + "  FROM cotizacion_detalle cd where cd.ingreso = rd.ingreso and cd.idcotizacion = " + cotizacion + ";";
            sql += "UPDATE ingreso_plantilla rd SET fechaaproajus = null "
                    + " FROM cotizacion_detalle cd where cd.ingreso = rd.ingreso and cd.idcotizacion = " + cotizacion + ";";
            sql += "INSERT INTO ingreso_estados(ingreso, idusuario, descripcion, fecha, tiempo) "
                    + " SELECT cd.ingreso, " + idusuario + ",'COTIZACION ANULADA',now(),'' "
                    + " FROM cotizacion_detalle cd WHERE ingreso > 0 and cd.idcotizacion = " + cotizacion + ";";
            sql += "UPDATE cotizacion set estado='Anulado', fechaanula=now(), observacionanula='" + observaciones + "',idusuarioanula=" + idusuario
                    + " WHERE id=" + cotizacion + ";";
            sql += "UPDATE cotizacion_detalle SET estado='Anulado' WHERE idcotizacion = " + cotizacion + ";";
            sql += "UPDATE web.solicitud s SET estado ='Pendiente'  "
                    + " FROM cotizacion_detalle cd INNER JOIN web.solicitud_detalle sd on sd.id = cd.idsolicitud "
                    + " WHERE s.solicitud = sd.solicitud and cd.idcotizacion = " + cotizacion + ";";
            if (Globales.DatosAuditoria(sql, "COTIZACION", "ANULAR COTIZACION", idusuario, iplocal, this.getClass() + "-->AnularCotizacion")) {
                return "0|Cotización Anulada con éxito";
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }
        } catch (Exception ex) {
            return "9|" + ex.getMessage();
        }
    }

    public String AplicarDescuento(HttpServletRequest request) {
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        int id = Globales.Validarintnonull(request.getParameter("id"));

        if (Globales.PermisosSistemas("COTIZACION APLICAR DESCUENTO", idusuario) == 0) {
            return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
        }
        try {

            sql = "SELECT estado from cotizacion where id = " + id;
            String estado = Globales.ObtenerUnValor(sql);
            if (!estado.equals("Temporal") && !estado.equals("Cotizado") && !estado.equals("")) {
                return "1|No se puede aplicar un descuento a una cotización en estado " + estado;
            }

            double descuento = Globales.ValidardoublenoNull(Globales.ObtenerUnValor("select descuento FROM clientes where id = " + cliente));
            if (descuento <= 0) {
                return "1|Este cliente no posse descuento registrado";
            }

            sql = "UPDATE cotizacion_detalle cd SET descuento = " + descuento
                    + " WHERE idproveedor = 0 and idequipo > 0 and idcotizacion = " + id + " and idcliente = " + cliente
                    + " and CASE WHEN idserviciodep = 0 THEN idservicio else 0 END NOT IN (select id from servicio where descuento = 0)";
            if (Globales.DatosAuditoria(sql, "COTIZACION", "APLICAR DESCUENTO", idusuario, iplocal, this.getClass() + "-->AplicarDescuento")) {
                return "0|Descuento del " + descuento + "% aplicado con éxito|" + descuento;
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String ReemplazarCotizacion(HttpServletRequest request) {

        int cotizacion = Globales.Validarintnonull(request.getParameter("cotizacion"));
        String observaciones = request.getParameter("observaciones");

        try {
            if (Globales.PermisosSistemas("COTIZACION REEMPLAZAR", idusuario) == 0) {
                return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
            }
            sql = " SELECT * FROM reemplazar_cotizacion(" + cotizacion + ",'" + observaciones + "'," + idusuario + ")";
            datos = Globales.Obtenerdatos(sql, this.getClass() + "-->Reemplazar Cotizacion", 1);
            if (datos.next()) {
                if (datos.getString("_cotizacion").equals("0")) {
                    return "1|" + datos.getString("_mensaje");
                } else {
                    return "0|" + datos.getString("_cotizacion");
                }
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }
        } catch (Exception ex) {
            return "9|" + ex.getMessage();
        }
    }

    public String GuardarPrecioSer(HttpServletRequest request) {
        int id = Globales.Validarintnonull(request.getParameter("id"));
        double precio = Globales.ValidardoublenoNull(request.getParameter("precio"));
        int descuento = Globales.Validarintnonull(request.getParameter("descuento"));
        String observacion = request.getParameter("observacion");

        try {

            if (Globales.PermisosSistemas("COTIZACION ACTUALIZAR PRECIO", idusuario) == 0) {
                return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
            }

            sql = "UPDATE cotizacion_detalle SET precio = " + precio + ", descuento=" + descuento + ", observacion='" + observacion + "' WHERE id = " + id;
            if (Globales.DatosAuditoria(sql, "COTIZACION", "AGREGAR PRECIOS OTROS SERVICIOS", idusuario, iplocal, this.getClass() + "-->GuardarPrecioSer")) {
                return "0|Precio actualizado con éxito";
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String GuardarCotizacion(HttpServletRequest request) {

        int id = Globales.Validarintnonull(request.getParameter("id"));
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        int sede = Globales.Validarintnonull(request.getParameter("sede"));
        int contacto = Globales.Validarintnonull(request.getParameter("contacto"));
        double total = Globales.ValidardoublenoNull(request.getParameter("total"));
        double subtotal = Globales.ValidardoublenoNull(request.getParameter("subtotal"));
        double descuento = Globales.ValidardoublenoNull(request.getParameter("descuento"));
        double gravable = Globales.ValidardoublenoNull(request.getParameter("gravable"));
        double exento = Globales.ValidardoublenoNull(request.getParameter("exento"));
        double iva = Globales.ValidardoublenoNull(request.getParameter("iva"));
        int cotizacionant = Globales.Validarintnonull(request.getParameter("cotizacionant"));
        String observaciones = request.getParameter("observaciones");
        int solicitud = Globales.Validarintnonull(request.getParameter("solicitud"));
        double retefuente = Globales.ValidardoublenoNull(request.getParameter("retefuente"));
        double reteica = Globales.ValidardoublenoNull(request.getParameter("reteica"));
        double reteiva = Globales.ValidardoublenoNull(request.getParameter("reteiva"));
        String validez = request.getParameter("validez");

        String permiso;
        try {
            if (id == 0) {
                permiso = "GUARDAR";
                if (Globales.PermisosSistemas("COTIZACION GUARDAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
            } else {
                permiso = "EDITAR";
                if (Globales.PermisosSistemas("COTIZACION EDITAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
            }
            sql = " SELECT * FROM guardar_cotizacion(" + id + "," + cotizacionant + "," + cliente + "," + sede + "," + contacto + "," + subtotal
                    + "," + descuento + "," + exento + "," + gravable + "," + iva + "," + total + "," + retefuente + "," + reteiva + "," + reteica + ",'"
                    + observaciones + "'," + idusuario + "," + solicitud + ",'" + validez + "')";
            datos = Globales.Obtenerdatos(sql, this.getClass() + "-->GuardarCotizacion", 1);
            if (datos.next()) {
                if (datos.getString("_cotizacion").equals("0")) {
                    return "1|" + datos.getString("_mensaje");
                } else {
                    return "0|Cotización guardada con el número|" + datos.getString("_cotizacion") + "|" + datos.getString("_idcotizacion");
                }
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }
        } catch (Exception ex) {
            return "9|" + ex.getMessage();
        }
    }

    //--------------------------------------------------------------------
    public String AutoCompletarSerie() {
        sql = "select DISTINCT serie from remision_detalle";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->AutoCompletarSerie");
    }

    public String BuscarSedeCliente(HttpServletRequest request) {

        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));

        sql = "SELECT id, Nombres FROM clientes_contac where idcliente = " + cliente + " ORDER BY 2";
        String sql2 = "SELECT cs.id, cs.direccion || ', ' || c.descripcion || ' (' ||d.descripcion ||'), ' || nombre as nombre "
                + " FROM clientes_sede cs inner join ciudad c on c.id = cs.idciudad "
                + "                            inner join departamento d on d.id = c.iddepartamento  "
                + " where cs.idcliente = " + cliente + " ORDER BY 2";
        return Globales.ObtenerCombo(sql, 1, 0, 0) + "|" + Globales.ObtenerCombo(sql2, 1, 0, 0);
    }

    public String CambioSede(HttpServletRequest request) {

        int id = Globales.Validarintnonull(request.getParameter("id"));
        int sede = Globales.Validarintnonull(request.getParameter("sede"));
        int contacto = Globales.Validarintnonull(request.getParameter("contacto"));
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));

        if (Globales.PermisosSistemas("REMISION CAMBIO CLIENTE/SEDE/CONTACTO", idusuario) == 0) {
            return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
        }
        try {
            sql = "UPDATE remision SET idsede = " + sede + ", idcontacto=" + contacto + (cliente != 0 ? ", idcliente = " + cliente : "")
                    + " WHERE id = " + id + ";";
            if (cliente != 0) {
                sql += "UPDATE remision_detalle SET idcliente = " + cliente + " WHERE idremision = " + id;
            }
            if (Globales.DatosAuditoria(sql, "REMISION", "CAMBIO DE CLIENTE, SEDE, CONTACTO", idusuario, iplocal, this.getClass() + "-->CambioSede")) {
                return "0|" + (cliente != 0 ? "Cliente, " : "") + "Sede y Contacto actualizado con éxito";
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String ConsultarIngresoDet(HttpServletRequest request) {
        int remision = Globales.Validarintnonull(request.getParameter("remision"));
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        String ingresos = request.getParameter("ingresos");
        String fechad = request.getParameter("fechad");
        String fechah = request.getParameter("fechah");
        String sitio = request.getParameter("sitio");
        String garantia = request.getParameter("garantia");
        String convenio = request.getParameter("convenio");
        int magnitud = Globales.Validarintnonull(request.getParameter("magnitud"));
        String equipo = request.getParameter("equipo");
        int marca = Globales.Validarintnonull(request.getParameter("marca"));
        String modelo = request.getParameter("modelo");
        String intervalo = request.getParameter("intervalo");
        String serie = request.getParameter("serie");
        int usuarios = Globales.Validarintnonull(request.getParameter("usuario"));
        String recepcion = request.getParameter("recepcion");

        int ingreso = Globales.Validarintnonull(ingresos);
        String[] a_ingreso = ingresos.split("-");

        String busqueda = "WHERE 1=1 ";
        if (remision > 0) {
            busqueda += " AND r.remision = " + remision;
        } else if (ingreso > 0) {
            busqueda += " AND rd.ingreso = " + ingreso;
        } else if (Globales.Validarintnonull(a_ingreso[0]) > 0 && Globales.Validarintnonull(a_ingreso[1]) > 0) {
            busqueda += " AND rd.ingreso >= " + a_ingreso[0] + " and rd.ingreso <= " + a_ingreso[1];
        } else {
            busqueda += " AND to_char(rd.fechaing,'yyyy-MM-dd') >= '" + fechad + "' and to_char(rd.fechaing,'yyyy-MM-dd') <= '" + fechah + "'";
            if (cliente > 0) {
                busqueda += " AND r.idcliente = " + cliente;
            }
            if (magnitud > 0) {
                busqueda += " AND e.idmagnitud = " + magnitud;
            }
            if (!equipo.equals("")) {
                busqueda += " AND e.descripcion = '" + equipo + "'";
            }
            if (marca > 0) {
                busqueda += " AND mo.idmarca = " + marca;
            }
            if (!modelo.equals("")) {
                busqueda += " AND mo.descripcion = '" + modelo + "'";
            }
            if (!intervalo.equals("")) {
                busqueda += " AND '(' || i.desde || ' a ' || i.hasta || ') ' || i.medida = '" + intervalo + "'";
            }
            if (!sitio.equals("")) {
                busqueda += " AND rd.sitio = '" + sitio + "'";
            }
            if (!garantia.equals("")) {
                busqueda += " AND rd.garantia = '" + garantia + "'";
            }
            if (!convenio.equals("")) {
                busqueda += " AND rd.convenio = " + convenio;
            }
            if (!serie.trim().equals("")) {
                busqueda += " AND rd.serie ilike '%" + serie.trim() + "%'";
            }
            if (usuarios > 0) {
                busqueda += " AND r.idusuario = " + usuarios;
            }
            if (recepcion != null && !recepcion.equals("")) {
                busqueda += " AND r.recepcion = '" + recepcion + "'";
            }
        }

        sql = "select r.id, r.remision, rd.ingreso, r.cotizacion, r.estado, rd.observacion, to_char(rd.fechaing, 'yyyy/MM/dd HH24:MI') as fecha, u.nombrecompleto as usuario, r.recepcion, "
                + "  '<b>' || c.nombrecompleto || '</b><br>' || c.documento as cliente, "
                + "  '<b>' || cs.nombre || '</b><br>' || cs.direccion || ', ' || ci.descripcion || ' (' ||d.descripcion ||'), ' || pa.descripcion as direccion, "
                + "  '<b>' || cc.nombres || '</b><br>' || coalesce(cc.telefonos,'') || ' / ' || coalesce(cc.fax,'') || '<br>' || cc.email as contacto, "
                + "  e.descripcion as equipo, m.descripcion as magnitud, ma.descripcion as marca, mo.descripcion as modelo, serie, "
                + "  case when rd.idintervalo = 0 then 'VER ESPECIFICACIONES' ELSE '(' || i.desde || ' a ' || i.hasta || ') ' || i.medida end as rango1, "
                + "  case when rd.idintervalo2 = 0 then '' ELSE '(' || i2.desde || ' a ' || i2.hasta || ') ' || i2.medida end as rango2, "
                + "  case when rd.idintervalo3 = 0 then '' ELSE '(' || i3.desde || ' a ' || i3.hasta || ') ' || i3.medida end as rango3, "
                + "  garantia, sitio, accesorio, case when convenio = 1 then 'SI' ELSE 'NO' END as convenio, rd.facturado as factura, ncotizacion, "
                + "  (select fd.subtotal from factura_datelle fd where fd.ingreso = rd.ingreso and fd.descripcion like 'CALIBRACIÓN%' order by fd.id desc limit 1) AS facturado, "
                + "  (select cd.precio - (cd.precio*cd.descuento/100) from cotizacion_detalle cd where cd.ingreso = rd.ingreso and cd.idserviciodep = 0  order by cd.id desc limit 1) AS cotizado, "
                + "  (select fd.subtotal from factura_datelle fd where fd.ingreso = rd.ingreso and fd.descripcion like '%AJUSTE%' order by fd.id desc limit 1) AS ajuste, "
                + "  (select fd.subtotal from factura_datelle fd where fd.ingreso = rd.ingreso and fd.descripcion like '%SUMINISTRO%' order by fd.id desc limit 1) AS suministro "
                + "  from remision r inner join seguridad.rbac_usuario u on r.idusuario = u.idusu "
                + "		                    inner join clientes c on c.id = r.idcliente "
                + "		                    inner join clientes_sede cs on cs.id = r.idsede "
                + "		                    inner join clientes_contac cc on cc.id = r.idcontacto "
                + "		                    inner join ciudad ci on ci.id = cs.idciudad "
                + "		                    inner join departamento d on d.id = ci.iddepartamento "
                + "		                    inner join pais pa on pa.id = d.idpais "
                + "		                    inner join remision_detalle rd on rd.idremision = r.id  "
                + "                  inner join equipo e on e.id = rd.idequipo "
                + "                  inner join magnitudes m on m.id = e.idmagnitud "
                + "		                    inner join modelos mo on mo.id = rd.idmodelo "
                + "                  inner join marcas ma on ma.id = mo.idmarca "
                + "                  inner join magnitud_intervalos i on i.id = rd.idintervalo  "
                + "                  inner join magnitud_intervalos i2 on i2.id = rd.idintervalo2 "
                + "                  inner join magnitud_intervalos i3 on i3.id = rd.idintervalo3 " + busqueda
                + "  ORDER BY rd.ingreso ";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ConsultarIngresoDet");
    }

    public String ConsultarRemision(HttpServletRequest request) {
        int remision = Globales.Validarintnonull(request.getParameter("remision"));
        int cotizacion = Globales.Validarintnonull(request.getParameter("cotizacion"));
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        int ingreso = Globales.Validarintnonull(request.getParameter("ingreso"));
        String estado = request.getParameter("estado");
        String fechad = request.getParameter("fechad");
        String fechah = request.getParameter("fechah");
        int magnitud = Globales.Validarintnonull(request.getParameter("magnitud"));
        String equipo = request.getParameter("equipo");
        int marca = Globales.Validarintnonull(request.getParameter("marca"));
        String modelo = request.getParameter("modelo");
        String intervalo = request.getParameter("intervalo");
        String serie = request.getParameter("serie");
        int pais = Globales.Validarintnonull(request.getParameter("pais"));
        int departamento = Globales.Validarintnonull(request.getParameter("departamento"));
        int ciudad = Globales.Validarintnonull(request.getParameter("ciudad"));
        int usuarios = Globales.Validarintnonull(request.getParameter("usuario"));
        String recepcion = request.getParameter("recepcion");

        String busqueda = "WHERE 1=1 ";
        if (remision > 0) {
            busqueda += " AND r.remision = " + remision;
        } else if (cotizacion > 0) {
            busqueda += " AND r.cotizacion = " + cotizacion;
        } else if (ingreso > 0) {
            busqueda += " AND rd.ingreso = " + ingreso;
        } else {
            busqueda += " AND to_char(r.fecha,'yyyy-MM-dd') >= '" + fechad + "' and to_char(r.fecha,'yyyy-MM-dd') <= '" + fechah + "'";
            if (cliente > 0) {
                busqueda += " AND r.idcliente = " + cliente;
            }
            if (!estado.equals("0")) {
                busqueda += " AND r.estado = '" + estado + "'";
            }
            if (magnitud > 0) {
                busqueda += " AND e.idmagnitud = " + magnitud;
            }
            if (!equipo.equals("")) {
                busqueda += " AND e.descripcion = '" + equipo + "'";
            }
            if (marca > 0) {
                busqueda += " AND mo.idmarca = " + marca;
            }
            if (!modelo.equals("")) {
                busqueda += " AND mo.descripcion = '" + modelo + "'";
            }
            if (!intervalo.equals("")) {
                busqueda += " AND '(' || desde || ' a ' || hasta || ') ' || medida = '" + intervalo + "'";
            }
            if (!serie.trim().equals("")) {
                busqueda += " AND rd.serie ilike '%" + serie.trim() + "%'";
            }
            if (pais > 0) {
                busqueda += " AND d.idpais = " + pais;
            }
            if (departamento > 0) {
                busqueda += " AND ci.iddepartamento = " + departamento;
            }
            if (ciudad > 0) {
                busqueda += " AND ci.id = " + ciudad;
            }
            if (usuarios > 0) {
                busqueda += " AND r.idusuario = " + usuarios;
            }
            if (recepcion != null && !recepcion.equals("")) {
                busqueda += " AND r.recepcion = '" + recepcion + "'";
            }
        }
        sql = "select r.id, r.remision, r.cotizacion, r.estado, r.observacion, to_char(fecha, 'yyyy/MM/dd HH24:MI') as fecha, u.nombrecompleto as usuario, r.recepcion, "
                + "  c.documento, c.nombrecompleto as cliente, cs.nombre as sede, cc.nombres as contacto, ci.descripcion as ciudad, d.descripcion as departamento, "
                + "  cc.telefonos || ' / ' || coalesce(cc.fax,'') as telefonos, cc.email, r.totales, "
                + "  '<button class=''btn btn-glow btn-success imprimir'' title=''Imprimir Remisión'' type=''button'' onclick=' || chr(34) || 'ImprimirRemision(' || r.remision || ',''' || r.estado || ''',0)' ||chr(34) || '><span data-icon=''&#xe0f7;''></span></button>' as imprimir " + "  from remision r inner join seguridad.rbac_usuario u on r.idusuario = u.idusu "
                + "		                    inner join clientes c on c.id = r.idcliente "
                + "		                    inner join clientes_sede cs on cs.id = r.idsede "
                + "		                    inner join clientes_contac cc on cc.id = r.idcontacto "
                + "		                    inner join ciudad ci on ci.id = cs.idciudad "
                + "		                    inner join departamento d on d.id = ci.iddepartamento "
                + "		                    inner join remision_detalle rd on rd.idremision = r.id  "
                + "                  inner join magnitud_intervalos i on i.id = rd.idintervalo "
                + "                  inner join equipo e on e.id = rd.idequipo "
                + "		                    inner join modelos mo on mo.id = rd.idmodelo " + busqueda
                + " group by r.id, r.remision, r.cotizacion, r.estado, r.observacion, fecha, u.nombrecompleto,r.recepcion, "
                + " c.documento, c.nombrecompleto, cs.nombre, cc.nombres, ci.descripcion, d.descripcion, cc.telefonos, cc.fax, cc.email, r.totales "
                + " ORDER BY r.id desc";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ConsultarRemision");
    }

    public int EquiposAgregados(HttpServletRequest request) {
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        int id = Globales.Validarintnonull(request.getParameter("id"));
        int equipo = Globales.Validarintnonull(request.getParameter("equipo"));
        int modelo = Globales.Validarintnonull(request.getParameter("modelo"));
        int intervalo = Globales.Validarintnonull(request.getParameter("intervalo"));

        sql = "select sum(cantidad) from remision_detalle "
                + "  where idcliente = " + cliente + " and idequipo=" + equipo + " and idmodelo=" + modelo + " and idintervalo=" + intervalo
                + " and ((idremision = 0 and idusuario = " + idusuario + ") or (idremision = " + id + "))";
        return Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
    }

    public String GuardarTmpIngreso(HttpServletRequest request) {
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        int id = Globales.Validarintnonull(request.getParameter("id"));
        int equipo = Globales.Validarintnonull(request.getParameter("equipo"));
        int modelo = Globales.Validarintnonull(request.getParameter("modelo"));
        String observacion = request.getParameter("observacion");
        int convenio = Globales.Validarintnonull(request.getParameter("convenio"));
        String serie = request.getParameter("serie");
        int ingreso = Globales.Validarintnonull(request.getParameter("ingreso"));
        int cantidad = Globales.Validarintnonull(request.getParameter("cantidad"));
        int intervalo = Globales.Validarintnonull(request.getParameter("intervalo"));
        int intervalo2 = Globales.Validarintnonull(request.getParameter("intervalo2"));
        int intervalo3 = Globales.Validarintnonull(request.getParameter("intervalo3"));
        int remision = Globales.Validarintnonull(request.getParameter("remision"));
        int cotizacion = Globales.Validarintnonull(request.getParameter("cotizacion"));
        String sitio = request.getParameter("sitio");
        String garantia = request.getParameter("garantia");
        String accesorio = request.getParameter("accesorio");

        sql = "select id from remision_detalle where idequipo = " + equipo + " and serie = '" + serie + "' and idmodelo =" + modelo + " and salida = 0";
        int encontrado = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
        String mensaje;

        if (encontrado != id && encontrado > 0) {
            return "1|Esta serie ya se encuentra registrada en un ingreso activo";
        }
        try {
            if (id == 0) {
                if (Globales.PermisosSistemas("INGRESO AGREGAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                int contador = Contadores(null, 2);
                sql = "INSERT INTO remision_detalle(idremision, idcliente, idequipo, idmodelo, serie, ingreso, observacion, idusuario, "
                        + " cantidad, idintervalo, idintervalo2, idintervalo3, sitio, garantia, accesorio, convenio) "
                        + " VALUES (" + remision + "," + cliente + "," + equipo + "," + modelo + ",'" + serie + "'," + contador + ",'" + observacion + "',"
                        + idusuario + "," + cantidad + "," + intervalo + "," + intervalo2 + "," + intervalo3
                        + ",'" + sitio + "','" + garantia + "','" + accesorio + "'," + convenio + ");"
                        + " UPDATE contadores SET ingreso = " + contador + ";";
                sql += "INSERT INTO ingreso_estados(ingreso, idusuario, descripcion) "
                        + " VALUES (" + contador + "," + idusuario + ",'INGRESO REGISTRADO');";
                if (cotizacion > 0) {
                    String textosql = "SELECT to_char(fecha,'yyyy-MM-dd HH24:MI) FROM cotizacion where cotizacion = " + cotizacion;
                    String fecha = Globales.ObtenerUnValor(textosql);
                    sql += "INSERT INTO ingreso_estados(ingreso, idusuario, descripcion,fecha) "
                            + " VALUES (" + contador + "," + idusuario + ",'COTIZADO REGISTRADA','" + fecha + "');";
                }

                mensaje = "Ingreso agregado";
            } else {
                encontrado = Globales.Validarintnonull(Globales.ObtenerUnValor("SELECT ingreso from remision_detalle where salida > 0 and id = " + id));
                if (encontrado > 0) {
                    return "1|No se puede editar un ingreso con salida";
                }
                if (Globales.PermisosSistemas("INGRESO EDITAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
                sql = "UPDATE remision_detalle "
                        + "      SET idequipo=" + equipo + ",idmodelo=" + modelo + ",serie='" + serie + "'"
                        + "      ,observacion='" + observacion + "', idintervalo=" + intervalo + ","
                        + "        estado = 'Temporal', idintervalo2=" + intervalo2 + ", idintervalo3=" + intervalo3
                        + ",sitio='" + sitio + "', garantia='" + garantia + "', accesorio='" + accesorio + "',convenio=" + convenio
                        + " WHERE id=" + id + ";";
                mensaje = "Ingreso actualizado";
                sql += "INSERT INTO ingreso_estados(ingreso, idusuario, descripcion) "
                        + " VALUES (" + ingreso + "," + idusuario + ",'INGRESO ACTUALIZADO');";
            }
            sql += "UPDATE remision SET estado = 'Temporal' where id = " + remision + ";";
            if (Globales.DatosAuditoria(sql, "REMISION", "AGREGAR ITEM A LA REMISION", idusuario, iplocal, this.getClass() + "-->GenerarSolicitud")) {
                return "0|" + mensaje;
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String CambioSerie(HttpServletRequest request) {

        int ingreso = Globales.Validarintnonull(request.getParameter("ingreso"));
        String serie = request.getParameter("serie");
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));

        int encontrado = Globales.Validarintnonull(Globales.ObtenerUnValor("select id from remision_detalle where salida = 0 and ingreso <> " + ingreso + " and serie='" + serie + "' and idcliente=" + cliente));
        if (encontrado > 0) {
            return "XX";
        } else {
            return "0";
        }
    }

    public String BuscarIngreso(HttpServletRequest request) {
        int ingreso = Globales.Validarintnonull(request.getParameter("ingreso"));
        String serie = request.getParameter("serie");

        if (ingreso > 0) {
            sql = "select r.id, r.fotos, e.descripcion as equipo, ma.descripcion as marca, mo.descripcion as modelo, "
                    + "      r.observacion, r.estado, r.serie, r.ingreso, e.id as idequipo,  m.id as idmagnitud, ma.id as idmarca, mo.id as idmodelo,   "
                    + "      r.idintervalo, r.idintervalo2, r.idintervalo3, sitio, garantia, accesorio, convenio  "
                    + "  from remision_detalle r inner join equipo e on r.idequipo = e.id "
                    + "				                         inner join magnitudes m on m.id = e.idmagnitud "
                    + "				                         inner join modelos mo on mo.id = r.idmodelo "
                    + "				                         inner join marcas ma on ma.id = mo.idmarca "
                    + "                           inner join seguridad.rbac_usuario u on u.idusu = r.idusuario"
                    + "  where r.ingreso = " + ingreso;
        } else {
            sql = "select r.id, r.fotos, e.descripcion as equipo, ma.descripcion as marca, mo.descripcion as modelo, "
                    + "      r.observacion, r.estado, r.serie, r.ingreso, e.id as idequipo,  m.id as idmagnitud, ma.id as idmarca, mo.id as idmodelo, "
                    + "      r.idintervalo, r.idintervalo2, r.idintervalo3, sitio, garantia, accesorio, convenio, r.salida "
                    + "  from remision_detalle r inner join equipo e on r.idequipo = e.id "
                    + "				                         inner join magnitudes m on m.id = e.idmagnitud "
                    + "				                         inner join modelos mo on mo.id = r.idmodelo "
                    + "				                         inner join marcas ma on ma.id = mo.idmarca "
                    + "                           inner join seguridad.rbac_usuario u on u.idusu = r.idusuario "
                    + "  where r.serie = '" + serie + "' order by r.id desc";
        }
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->BuscarIngreso");
    }

    public String RpRemision(HttpServletRequest request) {
        int remision = Globales.Validarintnonull(request.getParameter("cotizacion"));

        String reporte = "ImpRemision";

        sql = "select c.nombrecompleto as cliente, c.documento, cc.nombres as contacto, cc.email, cc.telefonos, cc.fax, r.remision, r.observacion as observaciones, "
                + "              ci.descripcion as ciudad, d.descripcion as departamento, r.fecha, to_char(r.fecha,'yyyy-MM-dd') as fecharem, u.telefono as telefonousuario, u.documento as docuusuario, "
                + "              trim(to_char(rd.ingreso,'0000000')) as ingreso, rd.observacion || case when coalesce(rd.accesorio,'') <> '' then '<br><b>Accesorios: </b>' || rd.accesorio else '' end as observacion, rd.serie, e.descripcion as equipo, recepcion, "
                + "              case when rd.idintervalo = 0 then 'VER ESPECIFICACIONES' ELSE '(' || i.desde || ' a ' || i.hasta || ') ' || i.medida END || "
                + "              case when rd.idintervalo2 = 0 then '' ELSE '<br>(' || i2.desde || ' a ' || i2.hasta || ') ' || i2.medida END || "
                + "              case when rd.idintervalo3 = 0 then '' ELSE '<br>(' || i3.desde || ' a ' || i3.hasta || ') ' || i3.medida END  as intervalos, "
                + "              m.descripcion as modelo, ma.descripcion as marca, u.nombrecompleto as usuario, u.cargo, cs.direccion || ', ' || ci.descripcion || ' (' ||d.descripcion ||'), ' || pa.descripcion as direccion, cs.nombre as sede "
                + " from remision r inner join remision_detalle rd on rd.idremision = r.id  "
                + "		                            inner join clientes  c on r.idcliente = c.id "
                + "		                            inner join clientes_contac cc on r.idcontacto = cc.id "
                + "		                            inner join clientes_sede cs on cs.id = r.idsede "
                + "		                            inner join ciudad ci on ci.id = cs.idciudad "
                + "		                            inner join departamento d on d.id = ci.iddepartamento "
                + "                          inner join pais pa on pa.id = d.idpais "
                + "		                            inner join equipo e on e.id = rd.idequipo "
                + "		                            inner join modelos m on m.id = rd.idmodelo "
                + "		                            inner join marcas ma on ma.id = m.idmarca "
                + "		                            inner join seguridad.rbac_usuario u on u.idusu = rd.idusuario "
                + "                          inner join magnitud_intervalos i on i.id = rd.idintervalo "
                + "                          inner join magnitud_intervalos i2 on i2.id = rd.idintervalo2 "
                + "                          inner join magnitud_intervalos i3 on i3.id = rd.idintervalo3 "
                + " where r.remision = " + remision
                + " ORDER BY rd.ingreso";
        /*datos = Globales.Obtenerdatos(sql, conexion);

         if (datos.Rows.Count == 0)
         return "1|No hay nada que reportar";

         DateTime fecharem = DateTime.Parse(datos.Rows[0]["fecharem"].ToString());
         String cliente = datos.Rows[0]["cliente"].ToString();

         String DirectorioReportesRelativo = "~/Reportes/";
         String directorio = "~/DocumPDF/";
         String unico = "Remision-" + remision + "_" + DateTime.Now.ToString("yyyyMMddHHmmss");
         String urlArchivo = String.Format("{0}.{1}", reporte, "rdlc");

         String urlArchivoGuardar;
         urlArchivoGuardar = String.Format("{0}.{1}", unico, "pdf");

         String FullPathReport = String.Format("{0}{1}",
         this.HttpContext.Server.MapPath(DirectorioReportesRelativo),
         urlArchivo);
         String FullGuardar = String.Format("{0}{1}",
         this.HttpContext.Server.MapPath(directorio),
         urlArchivoGuardar);
         Microsoft.Reporting.WebForms.ReportViewer Reporte = new ReportViewer();
         Reporte.Reset();
         Reporte.LocalReport.ReportPath = FullPathReport;
         Reporte.LocalReport.EnableExternalImages = true;

         ReportDataSource DataSource = new ReportDataSource("DataSet1", datos);
         Reporte.LocalReport.DataSources.Add(DataSource);
         Reporte.LocalReport.Refresh();
         byte[] ArchivoPDF = Reporte.LocalReport.Render("PDF");
         FileStream fs = new FileStream(FullGuardar, FileMode.Create);
         fs.Write(ArchivoPDF, 0, ArchivoPDF.Length);
         fs.Close();

         String puerto = Request.Url.Port.ToString();

         try
         {
         if (puerto == "80" || puerto == "90")
         {
         if (fecharem.Year > 2018)
         {
         String rutaremision = HostingEnvironment.MapPath("~/DocumPDF/" + urlArchivoGuardar);
         String rutagestion = System.Configuration.ConfigurationManager.AppSettings["sistemagestion"];
         rutagestion += fecharem.Year + "\\7. ÁREA DE LOGÍSTICA\\1. INTERNA\\DID-101\\DID-101-" + remision + " " + cliente + ".pdf";
         System.IO.File.Copy(rutaremision, rutagestion, true);
         }
         }
         }
         catch (Exception)
         {
         return "0|" + urlArchivoGuardar;
         }*/

        return "0|"; // + urlArchivoGuardar;

    }

    public String EnvioRemision(HttpServletRequest request) {
        String principal = request.getParameter("principal");
        String remision = request.getParameter("remision");
        String e_email = request.getParameter("e_email");
        String archivo = request.getParameter("archivo");
        String cliente = request.getParameter("cliente");
        int id = Globales.Validarintnonull(request.getParameter("id"));
        int error = 0;
        String[] email = e_email.split(";");

        String correoemp = "calibration.servicios@gmail.com";
        String clavecorreo = "envio.2018";

        if ((correoemp.trim() == "") || (clavecorreo.trim() == "")) {
            return "1|Debe configurar los parámetros: Correo gmail envio, Clave correo gmail envio";
        }

        String mensaje = "Envio de <b>Remisión Número</b> " + remision + " <br><b>Cliente</b> " + cliente;
        mensaje += "<br><br>En tres(3) días hábiles se le enviará la cotización de los intrumentos ingresados";

        String directorio = "~/DocumPDF/";

        /*String adjunto = String.Format("{0}{1}",
         this.HttpContext.Server.MapPath(directorio),
         archivo);*/

 /*String correo = Globales.EnviarEmail(principal, empresa, email, "Envío de Remisión Número " + remision, mensaje, correoemp, adjunto, correoemp, clavecorreo, "remision.pdf");

         if (correo.Trim() != "")
         {
         return "1|" + correo;
         }

         sql = " UPDATE remision set Estado = 'Cerrado' WHERE remision = " + remision + " and estado = 'Ingresado';";
         sql += "UPDATE remision_detalle set estado = 'Cerrado' WHERE idremision = " + id; // + " and estado = 'Ingresado';";
         Globales.DatosAuditoria(sql,"REMISION","ENVIAR REMISION",idusuario, iplocal);*/
        return error + "|Correo enviado a|" + principal + " " + email;

    }

    public String EnvioCotizacion(HttpServletRequest request) {
        try {
            String principal = request.getParameter("principal");
            String cotizacion = request.getParameter("cotizacion");
            String e_email = request.getParameter("e_email");
            String archivo = request.getParameter("archivo");
            int id = Globales.Validarintnonull(request.getParameter("id"));
            String observacion = request.getParameter("observacion");
            int canequipos = Globales.Validarintnonull(request.getParameter("canequipos"));
            int remision = Globales.Validarintnonull(request.getParameter("remision"));
            String tipoenvio = request.getParameter("tipoenvio");
            int error = 0;

            if (Globales.PermisosSistemas("COTIZACION ENVIAR", idusuario) == 0) {
                return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
            }

            sql = "SELECT clave, c.nombrecompleto as cliente, cc.nombres as contacto, u.nombrecompleto as usuario, u.cargo, c.email "
                    + "  from clientes c inner join cotizacion co on co.idcliente = c.id "
                    + "                  inner join clientes_contac cc on cc.id = co.idcontacto "
                    + "                  INNER JOIN seguridad.rbac_usuario u on u.idusu = co.idusuario "
                    + "  where cotizacion = " + cotizacion;
            datos = Globales.Obtenerdatos(sql, this.getClass() + "-->EnvioCotizacion", 1);
            datos.next();
            String ClaveAcceso = datos.getString("clave");
            String cliente = datos.getString("cliente");
            String contacto = datos.getString("contacto");
            String nomusuario = datos.getString("usuario");
            String cargo = datos.getString("cargo");
            String correocliente = datos.getString("email");

            e_email += ";" + correoenvio;
            String[] email = e_email.split(";");

            String correoemp = correoenvio;

            DateFormat formato = new SimpleDateFormat("HH");
            DateFormat formatomes = new SimpleDateFormat("MM");
            DateFormat formatoanio = new SimpleDateFormat("MM");

            int hora = Globales.Validarintnonull(formato.format(new Date()));
            String saludo = "";
            if (hora <= 11) {
                saludo = "Buenos días";
            } else if (hora <= 18) {
                saludo = "Buenas tardes";
            } else {
                saludo = "Buenas noches";
            }

            String nombrearchivo = "DID-79-" + cotizacion + ".pdf";

            if ((correoemp.trim().equals("")) || (clavecorreo.trim().equals(""))) {
                return "1|Debe configurar los parámetros: Correo envio, Clave correo envio";
            }

            String mensaje = saludo
                    + "<br><br><b>SEÑORES</b><br>" + cliente + "<br>" + contacto
                    + "<br><br> Reciba un cordial saludo por parte de nuestro equipo de trabajo"
                    + (Globales.Validarintnonull(formatomes.format(new Date())) >= 11 ? ", esperamos que las metas planteadas para este año " + formatoanio.format(new Date()) + " se hayan cumplido satisfactoriamente." : ".") + "<br>"
                    + "Adjunto encontrará la cotización <b>DID-79-" + cotizacion + "</b> correspondiente a su solicitud de servicio, la cual contiene todos los criterios exigidos por la norma ISO 17025:2005 y el ONAC (ORGANISMO NACIONAL DE ACREDITACIÓN DE COLOMBIA).<br><br>";

            if (tipoenvio.equals("Cotizacion")) {
                mensaje += "<b><font color='red'>Recuerde que el tiempo de entrega correrá a partir de la aprobación de la primera cotización y la llegada de los equipos a nuestras instalaciones.</font></b>";
            }

            mensaje += "<br><br>Teniendo en cuenta lo anterior solicitamos respetuosamente analizar detalladamente esta cotización, posteriormente debe de ingresar a su cuenta de <b>CALIBRATION SERVICE SAS</b> para aprobar o no la cotización a través del link <br><br>"
                    + "<a href='http://sof.calibrationservicesas.com:8080/usuarios/login.php?usuario=" + correocliente + "&clave=" + Globales.EncryptKey(ClaveAcceso) + "'>http://sof.calibrationservicesas.com:8080/usuarios/login.php?usuario=" + correocliente + "&clave=" + Globales.EncryptKey(ClaveAcceso) + "</a><br><br>"
                    + "Sus datos para ingresar son los siguientes: <br><b>usuario</b> " + correocliente
                    + "<br><b>Clave de acceso</b> " + ClaveAcceso + "<br><br>"
                    + (Globales.Validarintnonull(formatomes.format(new Date())) >= 11 ? "Les deseamos unas felices fiestas, una feliz navidad y un próspero año nuevo." : "");

            if (!observacion.equals("")) {
                mensaje += "<b>OBSERVACIÓN DE ENVÍO:</b><br><br>" + observacion + "<br>";
            }

            String directorio = "~/DocumPDF/";

            String adjunto = Globales.ruta_archivo + "DocumPDF/" + archivo;

            String adjunto1 = "";
            String adjunto2 = "";
            String fileName = "";

            if (archivoadjunto != null) {

            }

            /*if (Session["ArchivoAdjunto1"] != null || Session["ArchivoAdjunto2"] != null) {
                String path = HostingEnvironment.MapPath("~/AdjuntoCotizacion/" + cotizacion);
                if (!Directory.Exists(path)) {
                    DirectoryInfo di = Directory.CreateDirectory(path);
                }
            }

            if (Session["ArchivoAdjunto1"] != null) {
                fileName = HostingEnvironment.MapPath("~/uploads/" + Session["ArchivoAdjunto1"].ToString());
                adjunto1 = HostingEnvironment.MapPath("~/AdjuntoCotizacion/" + cotizacion + "/" + Session["ArchivoAdjunto1"].ToString().Replace(',', ' '));
                System.IO.File.Copy(fileName, adjunto1, true);
                nombrearchivo += "," + Session["ArchivoAdjunto1"].ToString().Replace(',', ' ');
                adjunto += "," + adjunto1;
            }

            if (Session["ArchivoAdjunto2"] != null) {
                fileName = HostingEnvironment.MapPath("~/uploads/" + Session["ArchivoAdjunto2"].ToString());
                adjunto2 = HostingEnvironment.MapPath("~/AdjuntoCotizacion/" + cotizacion + "/" + Session["ArchivoAdjunto2"].ToString().Replace(',', ' '));
                System.IO.File.Copy(fileName, adjunto2, true);
                nombrearchivo += "," + Session["ArchivoAdjunto2"].ToString().Replace(',', ' ');
                adjunto += "," + adjunto2;
            }

            if (adjunto1 != "") {
                mensaje += "<b>ADJUNTO DE ARCHIVO 1:</b><br>" + Session["ArchivoAdjunto1"].ToString() + "<br>";
            }

            if (adjunto2 != "") {
                mensaje += "<b>ADJUNTO DE ARCHIVO 2:</b><br>" + Session["ArchivoAdjunto2"].ToString() + "<br>";
            }*/
            mensaje += "<br><br>Cordialmente<br><br>";

            mensaje += "<img src='" + Globales.url_archivo + "Adjunto/imagenes/logo.png' width='30%'><br><b>" + nomusuario + "</b><br>Calibration Service SAS<br>" + cargo + "<br><font color='blue'>+57 (1) 2047699 - 7285146 <br>3138141058</font><br><br>";

            String correo = Globales.EnviarEmail(principal, Globales.Nombre_Empresa, email, "Envío de Cotización Número " + cotizacion, mensaje, correoemp, adjunto, correoemp, clavecorreo, nombrearchivo);

            if (!correo.trim().equals("")) {
                return "1|" + correo;
            }

            archivoadjunto = null;

            sql = " UPDATE cotizacion set Estado = 'Cerrado', fecenviada = now(),  idusuenviada = " + idusuario + ", obserenvio='" + observacion + "', "
                    + "correoenviado = '" + principal + " " + e_email + "', adjunto1='" + adjunto1 + "', adjunto2='" + adjunto2 + "' WHERE id = " + id + " and estado = 'Cotizado';";
            sql += "UPDATE remision_detalle rd set estado = 'Cotizado-Enviado' "
                    + " FROM cotizacion_detalle cd WHERE rd.ingreso = cd.ingreso and cd.ingreso <> 0 and idservicio > 0 and idcotizacion = " + id + ";";
            if (remision > 0 && canequipos > 0) {
                sql += " UPDATE remision set Estado = 'Cerrado'  WHERE remision = " + remision + " and estado ='Cotizado';";
                sql += "UPDATE remision_detalle rd set estado = 'Cerrado' "
                        + " from cotizacion_detalle cd "
                        + " where rd.ingreso = cd.ingreso and cd.idservicio = 0 and idserviciodep = 0 and idcotizacion = " + id + " and rd.estado ='EN COTIZACION'";
            }
            Globales.DatosAuditoria(sql, "COTIZACION", "ENVIAR COTIZACION", idusuario, iplocal, this.getClass() + "-->EnvioCotizacion");
            return error + "|Correo enviado a|" + e_email;
        } catch (SQLException ex) {
            return "1|" + ex.getMessage();
        }

    }

    public String GuardarIngreso(HttpServletRequest request) {

        int id = Globales.Validarintnonull(request.getParameter("id"));
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        int sede = Globales.Validarintnonull(request.getParameter("sede"));
        int contacto = Globales.Validarintnonull(request.getParameter("contacto"));
        double totales = Globales.ValidardoublenoNull(request.getParameter("totales"));
        String observaciones = request.getParameter("observaciones");
        String recepcion = request.getParameter("recepcion");
        String ErrorGestion = "";

        try {
            if (archivoadjunto == null && id == 0) {

                return "1|Error al adjuntar el archivo pdf dela remisión... Intentelo de nuevo";
            }

            if (id == 0) {
                if (Globales.PermisosSistemas("REMISION GUARDAR", idusuario) == 0) {
                    return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
                }
            } else if (Globales.PermisosSistemas("REMISION EDITAR", idusuario) == 0) {
                return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
            }

            sql = " SELECT * FROM guardar_remision(" + id + "," + cliente + "," + sede + "," + contacto + "," + totales + ",'" + observaciones + "'," + idusuario + ",'" + recepcion + "')";
            datos = Globales.Obtenerdatos(sql, this.getClass() + "-->GuardarIngreso", 1);
            datos.next();
            if (datos.getString("_remision").equals("0")) {
                return "1|" + datos.getString("_mensaje");
            } else {

                if (archivoadjunto != null) {
                    File uploads = new File(Globales.ruta_archivo + "uploads/" + archivoadjunto);
                    File destino = new File(Globales.ruta_archivo + "RemisionCliente/" + datos.getString("_remision") + "." + Globales.ExtensionArchivo(archivoadjunto));
                    Files.copy(uploads.toPath(), destino.toPath(), StandardCopyOption.REPLACE_EXISTING);

                    String ruta_gestion = Globales.ValidarRutaGestion("Remision_Cliente", datos.getString("_remision"));
                    if (ruta_gestion.equals("")) {
                        ErrorGestion = "DEBE DE CONFIGURAR LA RUTA DE GESTIÓN PARA EL DOCUMENTO REMISION CLIENTE EN LA BASE DE DATOS</h3></center>";
                    } else {
                        destino = new File(ruta_gestion + datos.getString("_remision") + "." + Globales.ExtensionArchivo(archivoadjunto));
                        Files.copy(uploads.toPath(), destino.toPath(), StandardCopyOption.REPLACE_EXISTING);
                        Globales.DatosAuditoria("UPDATE remision SET pdf = 1 WHERE remision = " + datos.getString("_remision"), "REMISION", "AGREGAR PDF", idusuario, iplocal, this.getClass() + "-->GuardarIngreso");
                        archivoadjunto = null;
                    }
                }
                return "0|Remisión guardada con el número|" + datos.getString("_remision") + "|" + datos.getString("_idremision") + "|" + ErrorGestion;
            }
        } catch (Exception ex) {
            return "9|" + ex.getMessage();
        }
    }

    public String EliminarFoto(HttpServletRequest request) {

        int ingreso = Globales.Validarintnonull(request.getParameter("ingreso"));

        if (Globales.PermisosSistemas("REMISION ELIMINAR FOTO", idusuario) == 0) {
            return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
        }

        try {
            sql = "select fotos from remision_detalle where ingreso = " + ingreso;
            int fotos = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
            if (fotos == 0) {
                return "No hay más fotos para eliminar|0";
            }
            File archivo = new File(Globales.ruta_archivo + "Adjunto/imagenes/ingresos/" + ingreso + "/" + fotos + ".jpg");
            if (archivo.exists()) {
                archivo.delete();
            }

            archivo = new File(Globales.ruta_archivo + "Adjunto/imagenes/ingresos/" + ingreso + "/" + fotos + "_copia.jpg");
            if (archivo.exists()) {
                archivo.delete();
            }

            if (Globales.DatosAuditoria("UPDATE remision_detalle set fotos = fotos-1 where ingreso = " + ingreso, "REMISION", "ELIMINAR FOTO", idusuario, iplocal, this.getClass() + "-->EliminarFoto")) {
                return "Foto Eliminada|" + (fotos - 1);
            } else {
                return "Error inesperado... Revise el log de errores|X";
            }
        } catch (Exception ex) {
            return ex.getMessage() + "|X";
        }
    }

    public String EliminarFotoIndividual(HttpServletRequest request) {

        int ingreso = Globales.Validarintnonull(request.getParameter("ingreso"));
        int numero = Globales.Validarintnonull(request.getParameter("numero"));
        File original = null;
        File destino = null;

        if (Globales.PermisosSistemas("REMISION ELIMINAR FOTO INDIVIDUAL", idusuario) == 0) {
            return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
        }

        try {
            sql = "select fotos from remision_detalle where ingreso = " + ingreso;
            int fotos = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
            if (fotos == 0) {
                return "1|No hay más fotos para eliminar";
            }
            File archivo = new File(Globales.ruta_archivo + "Adjunto/imagenes/ingresos/" + ingreso + "/" + numero + ".jpg");
            if (archivo.exists()) {
                archivo.delete();
            }
            archivo = new File(Globales.ruta_archivo + "Adjunto/imagenes/ingresos/" + ingreso + "/" + numero + "_copia.jpg");
            if (archivo.exists()) {
                archivo.delete();
            }
            if (Globales.DatosAuditoria("UPDATE remision_detalle set fotos = fotos-1 where ingreso = " + ingreso, "REMISION", "ELIMINAR FOTO INDIVIDUAL", idusuario, iplocal, this.getClass() + "-->EliminarFoto")) {
                if (numero < fotos) {
                    for (int x = (numero + 1); x <= fotos; x++) {
                        original = new File(Globales.ruta_archivo + "Adjunto/imagenes/ingresos/" + ingreso + "/" + x + ".jpg");
                        destino = new File(Globales.ruta_archivo + "Adjunto/imagenes/ingresos/" + ingreso + "/" + (x - 1) + ".jpg");
                        original.renameTo(destino);

                        original = new File(Globales.ruta_archivo + "Adjunto/imagenes/ingresos/" + ingreso + "/" + x + "_copia.jpg");
                        destino = new File(Globales.ruta_archivo + "Adjunto/imagenes/ingresos/" + ingreso + "/" + (x - 1) + "_copia.jpg");
                        original.renameTo(destino);
                    }
                }
                return "0|Foto Eliminada";
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String EliminarFotoDev(HttpServletRequest request) {

        int devolucion = Globales.Validarintnonull(request.getParameter("devolucion"));

        if (Globales.PermisosSistemas("DEVOLUCION ELIMINAR FOTO", idusuario) == 0) {
            return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
        }

        try {
            sql = "select fotos from devolucion where devolucion = " + devolucion;
            int fotos = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
            if (fotos == 0) {
                return "No hay más fotos para eliminar|0";
            }
            File archivo = new File(Globales.ruta_archivo + "Adjunto/imagenes/Devoluciones/" + devolucion + "/" + fotos + ".jpg");
            if (archivo.exists()) {
                archivo.delete();
            }

            archivo = new File(Globales.ruta_archivo + "Adjunto/imagenes/Devoluciones/" + devolucion + "/" + fotos + "_copia.jpg");
            if (archivo.exists()) {
                archivo.delete();
            }

            if (Globales.DatosAuditoria("UPDATE devolucion set fotos = fotos-1 where devolucion = " + devolucion, "DEVOLUCION", "ELIMINAR FOTO", idusuario, iplocal, this.getClass() + "-->EliminarFoto")) {
                return "Foto Eliminada|" + (fotos - 1);
            } else {
                return "Error inesperado... Revise el log de errores|X";
            }
        } catch (Exception ex) {
            return ex.getMessage() + "|X";
        }
    }

    public String EliminarFotoDevIndividual(HttpServletRequest request) {

        int devolucion = Globales.Validarintnonull(request.getParameter("devolucion"));
        int numero = Globales.Validarintnonull(request.getParameter("numero"));
        File original = null;
        File destino = null;

        if (Globales.PermisosSistemas("DEVOLUCION ELIMINAR FOTO INDIVIDUAL", idusuario) == 0) {
            return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
        }

        try {
            sql = "select fotos from devolucion where devolucion = " + devolucion;
            int fotos = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
            if (fotos == 0) {
                return "1|No hay más fotos para eliminar";
            }
            File archivo = new File(Globales.ruta_archivo + "Adjunto/imagenes/Devoluciones/" + devolucion + "/" + numero + ".jpg");
            if (archivo.exists()) {
                archivo.delete();
            }
            archivo = new File(Globales.ruta_archivo + "Adjunto/imagenes/Devoluciones/" + devolucion + "/" + numero + "_copia.jpg");
            if (archivo.exists()) {
                archivo.delete();
            }
            if (Globales.DatosAuditoria("UPDATE devolucion set fotos = fotos-1 where devolucion = " + devolucion, "DEVOLUCION", "ELIMINAR FOTO INDIVIDUAL", idusuario, iplocal, this.getClass() + "-->EliminarFoto")) {
                if (numero < fotos) {
                    for (int x = (numero + 1); x <= fotos; x++) {
                        original = new File(Globales.ruta_archivo + "Adjunto/imagenes/Devoluciones/" + devolucion + "/" + x + ".jpg");
                        destino = new File(Globales.ruta_archivo + "Adjunto/imagenes/Devoluciones/" + devolucion + "/" + (x - 1) + ".jpg");
                        original.renameTo(destino);

                        original = new File(Globales.ruta_archivo + "Adjunto/imagenes/Devoluciones/" + devolucion + "/" + x + "_copia.jpg");
                        destino = new File(Globales.ruta_archivo + "Adjunto/imagenes/Devoluciones/" + devolucion + "/" + (x - 1) + "_copia.jpg");
                        original.renameTo(destino);
                    }
                }
                return "0|Foto Eliminada";
            } else {
                return "1|Error inesperado... Revise el log de errores";
            }
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String CantidadFoto(HttpServletRequest request) {
        int ingreso = Globales.Validarintnonull(request.getParameter("ingreso"));
        sql = "select fotos from remision_detalle where ingreso = " + ingreso;
        return Globales.ObtenerUnValor(sql);
    }

    public String CantidadFotoDev(HttpServletRequest request) {
        int devolucion = Globales.Validarintnonull(request.getParameter("devolucion"));
        sql = "select fotos from devolucion where devolucion = " + devolucion;
        return Globales.ObtenerUnValor(sql);
    }

    /*public String GuardarFoto(String archivo, int ingreso)
     {
            
     try
     {
     sql = "select fotos from remision_detalle where ingreso = " + ingreso;
     int numero = Globales.Validarintnonull(Globales.ObtenerUnValor(sql, conexion, 0)) + 1;

     String base64 = archivo.Replace("data:image/png;base64,", "").Replace(" ", "+");
     if (base64.Length % 4 > 0)
     base64 = base64.PadRight(base64.Length + 4 - base64.Length % 4, '=');
     Image foto;
     byte[] imageBytes = Convert.FromBase64String(base64);
     using (var ms = new MemoryStream(imageBytes, 0, imageBytes.Length))
     {
     Image image = Image.FromStream(ms, true);
     foto = image;
     }

     String path = HostingEnvironment.MapPath("~/Adjunto/imagenes/ingresos/" + ingreso);
     if (!Directory.Exists(path))
     {
     DirectoryInfo di = Directory.CreateDirectory(path);
     }

     ImageCodecInfo myImageCodecInfo;
     Encoder myEncoder;
     EncoderParameter myEncoderParameter;
     EncoderParameters myEncoderParameters;
     myImageCodecInfo = GetEncoderInfo("image/jpeg");
     myEncoder = Encoder.Quality;
     myEncoderParameters = new EncoderParameters(1);
     myEncoderParameter = new EncoderParameter(myEncoder, 100L);
     myEncoderParameters.Param[0] = myEncoderParameter;

     String ruta = HostingEnvironment.MapPath("~/Adjunto/imagenes/ingresos/" + ingreso + "/" + numero + ".jpg");
     foto.Save(ruta, myImageCodecInfo, myEncoderParameters);
     sql = "update remision_detalle set fotos = " + numero + " where ingreso = " + ingreso;
     Globales.DatosAuditoria(sql, "INGRESO", "SUBIR FOTO DE INGRESO NUMERO " + ingreso + " FOTO = " + numero, idusuario, iplocal);
     return numero.ToString();
     }
     catch (Exception ex)
     {
     return "1|" + ex.getMessage();
     }
     }*/
    public String EliminarIngreso(HttpServletRequest request) {
        int id = Globales.Validarintnonull(request.getParameter("id"));
        String equipo = request.getParameter("equipo");
        int ingreso = Globales.Validarintnonull(request.getParameter("ingreso"));
        int remision = Globales.Validarintnonull(request.getParameter("remision"));

        sql = "SELECT estado from remision_detalle where id = " + id;
        String estado = Globales.ObtenerUnValor(sql);
        if (!estado.equals("Temporal") && !estado.equals("Ingresado")) {
            return "1|No se puede eliminar un ingreso en estado " + estado;
        }
        if (Globales.PermisosSistemas("INGRESO ELIMINAR", idusuario) == 0) {
            return "1|Usted no posee permisos para esta operación.... Consulte con el administrador de sistema";
        }

        sql = "INSERT INTO ingresos_eliminados(ingreso, equipo, idusuario) "
                + "  VALUES (" + ingreso + ",'" + equipo + "'," + idusuario + "); "
                + " DELETE FROM remision_detalle where id = " + id + ";"
                + " UPDATE remision SET estado = 'TEMPORAL' WHERE id = " + remision + ";";
        if (Globales.DatosAuditoria(sql, "REMISION", "ELIMINAR INGRESO", idusuario, iplocal, this.getClass() + "-->EliminarIngreso")) {
            return "0|Ingreso eliminado";
        } else {
            return "1|Error inesperado... Revise el log de errores";
        }
    }

    public String TemporalIngreso(HttpServletRequest request) {
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        int remision = Globales.Validarintnonull(request.getParameter("remision"));

        sql = "select r.idremision, m.descripcion as magnitud, e.descripcion as equipo, ma.descripcion as marca, mo.descripcion as modelo, re.cotizacion, "
                + "      case when r.idintervalo = 0 then 'VER ESPECIFICACIONES' ELSE '(' || mi.desde || ' a ' || mi.hasta || ') ' || mi.medida END || "
                + "      case when r.idintervalo2 = 0 then '' ELSE '<br>(' || mi2.desde || ' a ' || mi2.hasta || ') ' || mi2.medida END || "
                + "      case when r.idintervalo3 = 0 then '' ELSE '<br>(' || mi3.desde || ' a ' || mi3.hasta || ') ' || mi3.medida END  as intervalo, "
                + "      r.observacion, r.cantidad, r.serie,nombrecompleto as usuario,r.estado, garantia, sitio, case when convenio = 1 then 'SI' ELSE 'NO' END as convenio, accesorio, "
                + "      r.idintervalo, e.id as idequipo, m.id as idmagnitud, ma.id as idmarca, mo.id as idmodelo, ingreso, to_char(fechaing, 'dd/MM/yyyy HH24:MI') as fechaing, r.id, fotos "
                + "  from remision_detalle r inner join equipo e on r.idequipo = e.id "
                + "				                     inner join magnitudes m on m.id = e.idmagnitud "
                + "				                     inner join modelos mo on mo.id = r.idmodelo   "
                + "				                     inner join marcas ma on ma.id = mo.idmarca  "
                + "                           inner join seguridad.rbac_usuario u on u.idusu = r.idusuario "
                + "                           INNER join magnitud_intervalos mi on mi.id = r.idintervalo "
                + "                           INNER join magnitud_intervalos mi2 on mi2.id = r.idintervalo2 "
                + "                           INNER join magnitud_intervalos mi3 on mi3.id = r.idintervalo3  "
                + "                           left join remision re on re.id = r.idremision "
                + "  where r.idcliente = " + cliente + " and idremision = " + remision + " ORDER BY r.ingreso DESC";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->TemporalIngreso");
    }

    public String AnularDevolucion(HttpServletRequest request) {
        int id = Globales.Validarintnonull(request.getParameter("id"));
        String Observaciones = request.getParameter("Observaciones");

        try {
            sql = "SELECT estado, fechaentrega FROM devolucion where id = " + id;
            datos = Globales.Obtenerdatos(sql, this.getClass() + "-->AnularDevolucion", 1);
            datos.next();
            String estado = datos.getString("estado");
            String fecha = datos.getString("fechaentrega");
            if (estado.equals("Anulado")) {
                return "1|Esta devolución ya se encuentra anulada";
            }
            if (!fecha.equals("")) {
                return "1|No se puede anular una devolución entregada";
            }

            sql = "UPDATE public.devolucion "
                    + "SET fechaanulacion=now(), estado='Anulado', idusuarioanula=" + idusuario + ",observacionanula='" + Observaciones + "' "
                    + "WHERE id = " + id + "";

            sql += "UPDATE remision_detalle rd SET salida = 0, entregado = 0 "
                    + "FROM devolucion_detalle dd "
                    + "WHERE dd.ingreso = rd.ingreso and iddevolucion = " + id;
            Globales.Obtenerdatos(sql, this.getClass() + "-->AnularDevolucion", 2);
            return "0|Devolución anulada con éxito";
        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String TemporalDevolucion(HttpServletRequest request) {
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        int devolucion = Globales.Validarintnonull(request.getParameter("devolucion"));
        String sitio = request.getParameter("sitio");

        sql = "select m.descripcion as magnitud, e.descripcion as equipo, ma.descripcion as marca, mo.descripcion as modelo, re.cotizacion, coalesce(ri.observacion,'Ninguna') as observadev, informetercerizado,\n"
                + "case when r.idintervalo = 0 then 'VER ESPECIFICACIONES' ELSE mi.desde || ' - ' || mi.hasta || ' ' || mi.medida END as intervalo, r.observacion, r.cantidad, r.serie,u.nombrecompleto as usuario,r.estado,\n"
                + "r.ingreso, " + (devolucion != 0 ? "de.certificado" : "''") + " as certificadodevo, ' ' as informetecnico,  obtener_certificados(r.ingreso) as certificado,obtener_certificadospdf(r.ingreso) as certificadospdf, to_char(fechaing, 'dd/MM/yyyy HH24:MI') as fechaing, r.id, r.fotos,\n"
                + "coalesce((select y || x from ingreso_ubicacion iu where iu.ingreso = r.ingreso),'') as ubicacion, r.convenio\n"
                + "from remision_detalle r inner join equipo e on r.idequipo = e.id\n"
                + "inner join magnitudes m on m.id = e.idmagnitud\n"
                + "inner join modelos mo on mo.id = r.idmodelo  \n"
                + "inner join marcas ma on ma.id = mo.idmarca \n"
                + "inner join seguridad.rbac_usuario u on u.idusu = r.idusuario\n"
                + "inner join remision re on re.id = r.idremision\n"
                + "inner join clientes cli on cli.id = r.idcliente\n"
                + "inner join magnitud_intervalos mi on mi.id = r.idintervalo\n"
                + "left join ingreso_recibidoing ri on ri.ingreso = r.ingreso\n"
                + (devolucion != 0 ? " inner join devolucion_detalle de on de.ingreso = r.ingreso INNER JOIN devolucion d on d.id = de.iddevolucion " : " ") + " \n"
                + "where r.idcliente = " + cliente + " and " + (devolucion == 0 ? " r.salida = 0 " : " devolucion = " + devolucion) + (devolucion == 0 ? (sitio == "NO" ? " AND ri.id is not null " : " and r.sitio = 'SI' AND (coalesce((SELECT count(ip.id) from ingreso_plantilla ip where r.ingreso = ip.ingreso and certificado = 0 and informetecnico = 0),1) = 0) and recibidolab=1") : "") + " ORDER BY r.ingreso";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->TemporalDevolucion");
    }

    public String BuscarDevolucion(HttpServletRequest request) {
        int devolucion = Globales.Validarintnonull(request.getParameter("devolucion"));

        sql = "select d.id, d.devolucion, d.idcliente, d.idsede, d.idcontacto, d.entregado, d.observacion, to_char(d.fecha, 'dd/MM/yyyy HH24:MI') as fecha, u.nombrecompleto as usuario, c.documento,\n"
                + "to_char(fechaentrega, 'dd/MM/yyyy HH24:MI') as fechaentrega,  nombreentrega, observacionentrega, (select count(dd.id) from devolucion_detalle dd WHERE dd.iddevolucion = d.id) as cantidad,\n"
                + "cs.direccion || ', ' || ci.descripcion || ' (' ||de.descripcion ||'), ' || cs.nombre as sede, d.empresa_envia as idempresa,\n"
                + "cc.nombres as contacto, c.nombrecompleto as cliente, d.fotos, d.guia, ev.empresa as empresa_envia, fotoentrega,\n"
                + "to_char(d.fechaanulacion, 'dd/MM/yyyy HH24:MI') as fechaanulacion, observacionanula, ua.nombrecompleto as usuarioanula, d.sitio \n"
                + "from devolucion d inner join seguridad.rbac_usuario u on d.idusuario = u.idusu\n"
                + "inner join clientes c on c.id = d.idcliente\n"
                + "inner join clientes_contac cc on cc.id = d.idcontacto\n"
                + "inner join clientes_sede cs on cs.id = d.idsede\n"
                + "inner join ciudad ci on ci.id = cs.idciudad\n"
                + "inner join departamento de on de.id = ci.iddepartamento \n"
                + "inner join empresa_envio ev on ev.id = d.empresa_envia \n"
                + "inner join seguridad.rbac_usuario ua on d.idusuarioanula = ua.idusu\n"
                + "where devolucion = " + devolucion;
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->BuscarDevolucion");
    }

    public String GuardarDevolucion(HttpServletRequest request) {
        int id = Globales.Validarintnonull(request.getParameter("id"));
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        int sede = Globales.Validarintnonull(request.getParameter("sede"));
        int contacto = Globales.Validarintnonull(request.getParameter("contacto"));
        String a_ingreso = request.getParameter("a_ingreso");
        String a_observacion = request.getParameter("a_observacion");
        String observacion = request.getParameter("observacion");
        String a_certificados = request.getParameter("a_certificados");
        String sitio = request.getParameter("sitio");

        try {
            sql = " SELECT * FROM guardar_devolucion(" + id + "," + cliente + "," + sede + "," + contacto + "," + a_ingreso + "," + a_certificados + "," + a_observacion + ",'" + observacion + "'," + idusuario + ",'" + sitio + "')";
            datos = Globales.Obtenerdatos(sql, this.getClass() + "-->GuardarDevolucion", 1);
            datos.next();
            if (datos.getString("_devolucion").equals("0")) {
                return "1|" + datos.getString("_mensaje");
            } else {
                return "0|Devolución guardada con el número|" + datos.getString("_devolucion") + "|" + datos.getString("_iddevolucion");
            }
        } catch (Exception ex) {
            return "9|" + ex.getMessage();
        }
    }

    /*
     public String RpDevolucion(HttpServletRequest request) {
     int devolucion = Globales.Validarintnonull(request.getParameter("devolucion"));
            
     String reporte = "ImpDevolucion";


     String sql = "SELECT c.nombrecompleto AS cliente,\n" +
     "                            c.documento,\n" +
     "                            c.id as idcliente,\n" +
     "                            cc.nombres AS contacto,\n" +
     "                            cc.email,\n" +
     "                            cc.telefonos,\n" +
     "                            cc.fax,\n" +
     "                            cs.direccion || ', ' || ci.descripcion || ' (' ||d.descripcion ||'), ' || pa.descripcion as sede,\n" +
     "                            de.devolucion,\n" +
     "                            de.observacion as observaciones,\n" +
     "                            ci.descripcion AS ciudad,\n" +
     "                            d.descripcion AS departamento,\n" +
     "                            de.fecha,\n" +
     "                            to_char(de.fecha,'yyyy-MM-dd') as fechadev,\n" +
     "                            btrim(to_char(rd.ingreso, '0000000'))  AS ingreso,\n" +
     "                            ir.observacion as observacionfin,\n" +
     "                            de.impreso, de.cargor, de.recibio, de.identificacionr, de.telefonor, de.observacionentrega, \n" +
     "                            e.descripcion || '<BR> MARCA: ' || ma.descripcion || ' MODELO: ' || m.descripcion || ' <BR>SERIE: ' || rd.serie || '<BR> INTERVALO: ' ||  \n" +
     "                                CASE\n" +
     "                                    WHEN rd.idintervalo = 0 THEN 'VER ESPECIFICACIONES'\n" +
     "                                    ELSE '(' || desde || ' a ' || hasta || ') ' || medida   \n" +
     "                                 || coalesce('<br><b>Ubicacion:</b> ' || (select max(y || x) from ingreso_ubicacion iu where iu.ingreso = rd.ingreso),'') end AS equipo,\n" +
     "                            u.nombrecompleto AS usuario,\n" +
     "                            u.cargo, u.documento as usudocumento,\n" +
     "                            dd.certificado, rd.observacion || case when coalesce(rd.accesorio,'') <> '' then '<br><b>Accesorios: </b>' || rd.accesorio else '' end as observainicial\n" +
     "                            \n" +
     "                           FROM remision r\n" +
     "                             JOIN remision_detalle rd ON rd.idremision = r.id\n" +
     "                             JOIN clientes c ON r.idcliente = c.id\n" +
     "                             JOIN equipo e ON e.id = rd.idequipo\n" +
     "                             JOIN magnitud_intervalos i ON i.id = rd.idintervalo\n" +
     "                             JOIN modelos m ON m.id = rd.idmodelo\n" +
     "                             JOIN marcas ma ON ma.id = m.idmarca\n" +
     "                             JOIN devolucion_detalle dd on dd.ingreso = rd.ingreso\n" +
     "                             JOIN devolucion de on de.id = dd.iddevolucion 	\n" +
     "                             JOIN clientes_contac cc ON de.idcontacto = cc.id\n" +
     "                             JOIN clientes_sede cs ON cs.id = de.idsede\n" +
     "                             JOIN ciudad ci ON ci.id = cs.idciudad\n" +
     "                             JOIN departamento d ON d.id = ci.iddepartamento\n" +
     "                             JOIN pais pa on pa.id = d.idpais\n" +
     "                             JOIN seguridad.rbac_usuario u on u.idusu = de.idusuario\n" +
     "                             LEFT JOIN ingreso_recibidoing ir on ir.ingreso = rd.ingreso\n" +
     "                            where de.devolucion = " + devolucion + " \n" +
     "                            ORDER BY rd.ingreso";
     datos = Globales.Obtenerdatos(sql, this.getClass() + "-->RpDevolucion", 1);

     if (datos.next())
     return "1|No hay nada que reportar";

     int impreso = Globales.Validarintnonull(datos.getString("impreso"));
     Date fechadev = Date.Parse(datos.getString("fechadev"));
     int idcliente = Globales.Validarintnonull(datos.getString("idcliente"));
     String cliente = datos.getString("cliente");

     if (impreso == 0)
     {

     sql = "select sum(case when extract(days from(now() - vencimiento)) > diasbloqueo then saldo else 0 end) as vencido " +
     " FROM factura f inner join clientes c on c.id = f.idcliente where f.estado  <> 'Anulado' and saldo <> 0 and f.idcliente = " + idcliente;
     double saldo = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));

     if (saldo > 0)
     {
     sql = "SELECT s.id " +
     " FROM public.seguimiento_cartera s inner join seguridad.rbac_usuario u on s.idusuario = u.idusu " +
     " where devolucion like '%$" + devolucion + "$%' and to_char(fecha + CAST('3 days' AS INTERVAL),'yyyy-MM-dd') >= '" + Date.Now.AddDays(3).ToString("yyyy-MM-dd") + "'";
     int encontrado = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
     if (encontrado == 0)
     return "1|No se puede imprimir la devolución porque el cliente posee saldos pendiente... diríjase a cartera para gestionar su pago";
     }
     }
     String DirectorioReportesRelativo = "~/Reportes/";
     String directorio = "~/DocumPDF/";
     String unico = "Devolucion-" + devolucion + "_" + DateTime.Now.ToString("yyyyMMddHHmmss");
     String urlArchivo = String.format("{0}.{1}", reporte, "rdlc");
     String firma = ruta_urlarchivo + "Adjunto/imagenes/FirmaDevolucion/" + devolucion + ".jpg";

     String urlArchivoGuardar;
     urlArchivoGuardar = String.Format("{0}.{1}", unico, "pdf");

     String FullPathReport = String.Format("{0}{1}",
     this.HttpContext.Server.MapPath(DirectorioReportesRelativo),
     urlArchivo);
     String FullGuardar = String.Format("{0}{1}",
     this.HttpContext.Server.MapPath(directorio),
     urlArchivoGuardar);
     ReportViewer Reporte = new ReportViewer();
     Reporte.Reset();
     Reporte.LocalReport.ReportPath = FullPathReport;
     Reporte.LocalReport.EnableExternalImages = true;

     ReportParameter[] parameters = new ReportParameter[1];
     parameters[0] = new ReportParameter("firma", firma);
     Reporte.LocalReport.SetParameters(parameters);

     ReportDataSource DataSource = new ReportDataSource("DataSet1", datos);
     Reporte.LocalReport.DataSources.Add(DataSource);
     Reporte.LocalReport.Refresh();
     byte[] ArchivoPDF = Reporte.LocalReport.Render("PDF");
     FileStream fs = new FileStream(FullGuardar, FileMode.Create);
     fs.Write(ArchivoPDF, 0, ArchivoPDF.Length);
     fs.Close();

     sql = "UPDATE devolucion set impreso = 1 WHERE devolucion = " + devolucion;
     Globales.Obtenerdatos(sql, conexion);

     String puerto = Request.Url.Port.ToString();

     if (puerto == "80" || puerto == "90")
     {
     if (fechadev.Year > 2018)
     {
     String rutadevolucion = HostingEnvironment.MapPath("~/DocumPDF/" + urlArchivoGuardar);
     String rutagestion = System.Configuration.ConfigurationManager.AppSettings["sistemagestion"];
     rutagestion += fechadev.Year + "\\7. ÁREA DE LOGÍSTICA\\1. INTERNA\\DID-106\\DID-106-" + devolucion + " " + cliente + ".pdf";
     System.IO.File.Copy(rutadevolucion, rutagestion, true);
     }
     }

     return "0|" + urlArchivoGuardar;

     }
     */
    public String EntregarDevolucion(HttpServletRequest request) {
        int devolucion = Globales.Validarintnonull(request.getParameter("devolucion"));
        String persona = request.getParameter("persona");
        String observacion = request.getParameter("observacion");
        String fecha = request.getParameter("fecha");
        int empresa = Globales.Validarintnonull(request.getParameter("empresa"));
        String guia = request.getParameter("guia");

        try {
            if (empresa == 0) {
                empresa = 0;
            }
            sql = "SELECT entregado FROM devolucion where devolucion = " + devolucion;
            int econtrado = Globales.Validarintnonull(Globales.ObtenerUnValor(sql));
            if (econtrado == 1) {
                return "1|Devolución ya entregada";
            }
            sql = "UPDATE devolucion\n"
                    + "SET fechaentrega='" + fecha + "', fotodev = 1, entregado = 1, nombreentrega='" + persona + "', observacionentrega='" + observacion + "',\n"
                    + "idusuarioentrega =" + idusuario + ",empresa_envia = " + empresa + ", guia='" + guia + "' WHERE devolucion = " + devolucion;
            sql += "UPDATE remision_detalle rd set entregado = 1 \n"
                    + "FROM devolucion_detalle dd INNER JOIN devolucion d on d.id = dd.iddevolucion\n"
                    + "WHERE dd.ingreso = rd.ingreso and d.devolucion = " + devolucion;

            //SubirFotoDevFirmada(devolucion);
            if (archivoadjunto != null) {
                File fileName = new File(Globales.ruta_archivo + "uploads/" + archivoadjunto);
                if (fileName.exists()) {
                    File filedestino = new File("~/Adjunto/imagenes/DevoFirma/" + devolucion + ".pdf");
                    Files.copy(fileName.toPath(), filedestino.toPath());
                } else {
                    return "1|Error al subir el archivo";
                }
            } else {
                return "1|Error al subir el archivo";
            }

            Globales.Obtenerdatos(sql, this.getClass() + "-->EntregarDevolucion", 1);

            return "0|Devolución número " + devolucion + " entregada con éxtio|" + fecha;

        } catch (Exception ex) {
            return "1|" + ex.getMessage();
        }
    }

    public String ConsultarDevoluciones(HttpServletRequest request) {
        int devolucion = Globales.Validarintnonull(request.getParameter("devolucion"));
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        int ingreso = Globales.Validarintnonull(request.getParameter("ingreso"));
        String fechad = request.getParameter("fechad");
        String fechah = request.getParameter("fechah");

        int magnitud = Globales.Validarintnonull(request.getParameter("magnitud"));
        String equipo = request.getParameter("equipo");
        int marca = Globales.Validarintnonull(request.getParameter("marca"));
        String modelo = request.getParameter("modelo");
        String intervalo = request.getParameter("intervalo");
        String serie = request.getParameter("serie");
        int usuarios = Globales.Validarintnonull(request.getParameter("codusu"));
        String entregado = request.getParameter("entregado");

        String busqueda = "WHERE 1=1 ";
        if (devolucion > 0) {
            busqueda += " AND de.devolucion = " + devolucion;
        } else if (ingreso > 0) {
            busqueda += " AND rd.ingreso = " + ingreso;
        } else {
            busqueda += " AND to_char(de.fecha,'yyyy-MM-dd') >= '" + fechad + "' and to_char(de.fecha,'yyyy-MM-dd') <= '" + fechah + "'";
            if (cliente > 0) {
                busqueda += " AND de.idcliente = " + cliente;
            }
            if (ingreso > 0) {
                busqueda += " AND rd.ingreso = " + ingreso;
            }
            if (magnitud > 0) {
                busqueda += " AND e.idmagnitud = " + magnitud;
            }
            if (!equipo.equals("")) {
                busqueda += " AND e.descripcion = '" + equipo + "'";
            }
            if (marca > 0) {
                busqueda += " AND mo.idmarca = " + marca;
            }
            if (!modelo.equals("")) {
                busqueda += " AND mo.descripcion = '" + modelo + "'";
            }
            if (!intervalo.equals("")) {
                busqueda += " AND '(' || desde || ' a ' || hasta || ') ' || medida = '" + intervalo + "'";
            }
            if (!serie.trim().equals("")) {
                busqueda += " AND rd.serie ilike '%" + serie.trim() + "%'";
            }
            if (usuarios > 0) {
                busqueda += " AND de.idusuario = " + usuarios;
            }
            if (!entregado.equals("T")) {
                busqueda += " AND de.entregado = " + entregado;
            }

        }
        sql = "select de.id, de.devolucion, de.observacion, to_char(de.fecha, 'yyyy/MM/dd HH24:MI') as fecha, u.nombrecompleto as usuario, \n"
                + "c.documento, c.nombrecompleto as cliente, cs.nombre as sede, cc.nombres as contacto, ci.descripcion as ciudad, d.descripcion as departamento,\n"
                + "cc.telefonos || ' / ' || coalesce(cc.fax,'') as telefonos, cc.email, count(dd.id) as totales, '<b>' || nombreentrega || '</b><br>' || to_char(fechaentrega, 'yyyy/MM/dd HH24:MI') as entregado,\n"
                + "'<button class=''btn btn-glow btn-success imprimir'' title=''Imprimir Devolucion'' type=''button'' onclick=' || chr(34) || 'ImprimirDevolucion(' || de.devolucion || ',0)' ||chr(34) || '><span data-icon=''&#xe0f7;''></span></button>' as imprimir,\n"
                + "to_char(de.fechaanulacion, 'dd/MM/yyyy HH24:MI') || '<br>' || ua.nombrecompleto as anulado\n"
                + "from devolucion de inner join devolucion_detalle dd on dd.iddevolucion = de.id\n"
                + "inner join remision_detalle rd on rd.ingreso = dd.ingreso\n"
                + "inner join magnitud_intervalos i on i.id = rd.idintervalo \n"
                + "inner join seguridad.rbac_usuario u on de.idusuario = u.idusu\n"
                + "inner join clientes c on c.id = de.idcliente\n"
                + "inner join clientes_sede cs on cs.id = de.idsede\n"
                + "inner join clientes_contac cc on cc.id = de.idcontacto\n"
                + "inner join ciudad ci on ci.id = cs.idciudad\n"
                + "inner join departamento d on d.id = ci.iddepartamento\n"
                + "inner join equipo e on e.id = rd.idequipo\n"
                + "inner join seguridad.rbac_usuario ua on de.idusuarioanula = ua.idusu\n"
                + "inner join modelos mo on mo.id = rd.idmodelo  " + busqueda + " \n"
                + "group by de.id, de.devolucion, de.observacion, de.fecha, u.nombrecompleto,\n"
                + "c.documento, c.nombrecompleto, cs.nombre, cc.nombres, ci.descripcion, d.descripcion, cc.telefonos, cc.fax, cc.email,\n"
                + "nombreentrega, de.entregado, de.fechaanulacion, ua.nombrecompleto\n"
                + "ORDER BY de.id desc";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ConsultarDevoluciones");
    }

    public String BuscarOrdenCompra(HttpServletRequest request) {
        String orden = request.getParameter("orden");
        String documento = "";
        sql = "select distinct documento "
                + "from Clientes c inner join remision_detalle rd on rd.idcliente = c.id "
                + "inner join orden_compra o on o.ingreso = rd.ingreso and o.idcliente = c.id "
                + "where o.numero = '" + orden + "'";
        documento = Globales.ObtenerUnValor(sql);
        if (documento.trim().equals("")) {
            documento = "XX";
        }
        return documento;

    }

    public String TablaOrdenCompra(HttpServletRequest request) {
        int cliente = Globales.Validarintnonull(request.getParameter("cliente"));
        String orden = request.getParameter("orden");
        int tipo = Globales.Validarintnonull(request.getParameter("tipo"));
        String busqueda = "";
        int estado = 0;

        if (orden.trim().equals("") || orden.equals("0")) {
            busqueda += " AND rd.orden = 0 ";
        } else {
            busqueda += " AND rd.orden = 1 ";
            estado = 1;
        }

        sql = "select DISTINCT co.cotizacion, \n"
                + "rd.observacion, rd.cantidad, rd.serie, u.nombrecompleto as usuarioing,co.estado,\n"
                + "ma.descripcion || '<br>' || m.descripcion || '<br>' || mo.descripcion || '<br> ' || case when rd.idintervalo = 0 then 'VER ESPECIFICACIONES' ELSE '(' || desde || ' a ' || hasta || ') ' || medida end || '<br>' || rd.serie as equipo,\n"
                + "rd.ingreso, to_char(rd.fechaing, 'dd/MM/yyyy HH24:MI') as fechaing, \n"
                + "obtener_orden_compra(rd.ingreso, coalesce(co.cotizacion,0), '" + (orden.trim().equals("") || orden.equals("0") ? "" : orden) + "') as ordencompra,                               \n"
                + "case when coalesce(cd.ingreso,0) > 0 then  \n"
                + "case when cd.idserviciodep > 0 then (SELECT descripcion from  otros_servicios os where os.id = cd.idservicio) || '<br>' || coalesce(cd.observacion,'') else\n"
                + "(SELECT nombre from servicio s where s.id = cd.idservicio) || case when cd.metodo > 0 then '<br>' || coalesce((SELECT descripcion from metodo m where m.id = cd.metodo),'') else '' end end else '<b>NO<br>COTIZADO</b>' end as servicio\n"
                + "from remision_detalle rd INNER JOIN remision r on rd.idremision = r.id\n"
                + "inner join equipo e on rd.idequipo = e.id\n"
                + "inner join magnitudes m on m.id = e.idmagnitud\n"
                + "inner join modelos mo on mo.id = rd.idmodelo\n"
                + "inner join seguridad.rbac_usuario ur on ur.idusu = r.idusuario\n"
                + "inner join marcas ma on ma.id = mo.idmarca \n"
                + "inner join magnitud_intervalos mi on mi.id = rd.idintervalo  \n"
                + "inner join seguridad.rbac_usuario u on u.idusu = r.idusuario\n"
                + "inner join cotizacion_detalle cd on cd.ingreso = rd.ingreso \n"
                + "inner join cotizacion co on co.id = cd.idcotizacion " + (estado == 0 ? " and co.estado in ('Cerrado','Aprobado') " : "")
                + "WHERE r.idcliente = " + cliente + busqueda + " ORDER BY rd.ingreso, co.cotizacion";

        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->TablaOrdenCompra");
    }

    public String GuardarOrden(HttpServletRequest request) {
        try {
            String numero = request.getParameter("numero");
            String tipo = request.getParameter("tipo");
            String ingresos = request.getParameter("ingresos");
            int idcliente = Globales.Validarintnonull(request.getParameter("idcliente"));
            String fecha = request.getParameter("fecha");
            String hora = request.getParameter("hora");

            String usuarios = request.getParameter("usuario");
            String cedula = request.getParameter("cedula");
            String cargo = request.getParameter("cargo");

            String[] a_ingreso_b = ingresos.split(",");
            //String[] a_ingreso = a_ingreso_b.Distinct().toArray();
            String[] a_ingreso = Arrays.stream(a_ingreso_b).distinct().toArray(String[]::new);
            String estado = "";
            String mensaje = "";

            String habitual = "NO";
            String aprobar_cotizacion = "NO";
            String aprobar_ajuste = "NO";

            if (archivoadjunto == null) {
                return "1|Error al subir el archivo";
            }

            sql = "SELECT aprobar_cotizacion, aprobar_ajuste, habitual FROM clientes where id = " + idcliente;
            datos = Globales.Obtenerdatos(sql, this.getClass() + "-->GuardarOrden", 1);
            datos.next();
            habitual = datos.getString("habitual").trim();
            aprobar_cotizacion = datos.getString("aprobar_cotizacion").trim();
            aprobar_ajuste = datos.getString("aprobar_ajuste").trim();

            fecha = fecha + " " + hora;

            if (usuarios == null) {
                usuarios = "";
            }
            if (cedula == null) {
                cedula = "";
            }
            if (cargo == null) {
                cargo = "";
            }

            for (int x = 0; x < a_ingreso.length; x++) {
                if (habitual.equals("NO") && aprobar_cotizacion.equals("SI")) {
                    sql = "select co.estado\n"
                            + "                        from cotizacion co inner join cotizacion_detalle cd on cd.idcotizacion = co.id\n"
                            + "                        where cd.ingreso = " + a_ingreso[x];
                    estado = Globales.ObtenerUnValor(sql);
                    if (estado.equals("Cerrado") && (usuarios.equals("") || cargo.equals(""))) {
                        mensaje += (!mensaje.equals("") ? "<br>" : "") + "Debe ingresa el usuario y/o cargo para aprobar la cotización del ingreso número " + a_ingreso[x];
                    }
                }

                if (habitual.equals("NO") && aprobar_ajuste.equals("SI")) {
                    sql = "select co.estado\n"
                            + "	                    from reporte_ingreso ri inner join cotizacion_detalle cd on cd.id = ri.cotizado \n"
                            + "				                    inner join cotizacion co on co.id = cd.idcotizacion\n"
                            + "                        where cotizado > 0 and idconcepto = 2 and co.estado  in ('Cerrado','Aprobado') and ri.usuarioapro is null and ri.ingreso = " + a_ingreso[x];
                    if (!estado.equals("") && (usuarios.equals("") || cargo.equals(""))) {
                        mensaje += (!mensaje.equals("") ? "<br>" : "") + "Debe ingresa el usuario y/o cargo para aprobar el ajuste del ingreso número " + a_ingreso[x];
                    }
                }
            }

            if (!mensaje.equals("")) {
                return "1|" + mensaje;
            }

            sql = "INSERT INTO orden_compra(numero, ingreso, tipo, idusuario, idcliente, ncotizacion, fechaorden) ";
            for (int x = 0; x < a_ingreso.length; x++) {
                if (x != 0) {
                    sql += ",";
                } else {
                    sql += " VALUES ";
                }
                sql += " ('" + numero + "'," + a_ingreso[x] + ",'" + tipo + "'," + idusuario + "," + idcliente + ",(select ncotizacion from remision_detalle where ingreso = " + a_ingreso[x] + "), '" + fecha + "')";
            }

            sql += ";";

            for (int x = 0; x < a_ingreso.length; x++) {
                sql += "INSERT INTO ingreso_estados(ingreso, idusuario, descripcion, tiempo)\n"
                        + "VALUES(" + a_ingreso[x] + "," + idusuario + ",'ORDEN DE COMPRA DEL PROVEEDOR REGISTRADA',tiempo((SELECT fechaing FROM remision_detalle where ingreso = " + a_ingreso[x] + "),now(),1));";
                sql += "INSERT INTO cotizacion_aprobacion(cotizacion, usuario, cedula, cargo, estado, observacion)\n"
                        + "SELECT distinct co.cotizacion," + idusuario + ",'" + cedula + "','" + cargo + "','Aprobado','Aprobado por órden de compra'" + "    \n"
                        + "FROM cotizacion co inner join cotizacion_detalle cd on cd.idcotizacion = co.id\n"
                        + "WHERE co.estado = 'Cerrado' and cd.ingreso = " + a_ingreso[x] + ";";

                sql += "INSERT INTO ingreso_estados(ingreso, idusuario, descripcion, fecha, tiempo)\n"
                        + "SELECT distinct cd.ingreso, " + idusuario + ",'COTIZACION APROBADA','" + fecha + "'::timestamp with time zone,tiempo(co.fecha,'" + fecha + "',1) \n"
                        + "FROM cotizacion co inner join cotizacion_detalle cd on cd.idcotizacion = co.id\n"
                        + "WHERE co.estado = 'Cerrado' and cd.ingreso = " + a_ingreso[x] + ";";

                sql += "INSERT INTO ingreso_estados(ingreso, idusuario, descripcion, fecha, tiempo)\n"
                        + "SELECT distinct cd.ingreso, " + idusuario + ",'AUTORIZACIÓN DE AJUSTE','" + fecha + "'::timestamp with time zone,tiempo(ri.fecha,'" + fecha + "',1) \n"
                        + "FROM reporte_ingreso ri INNER JOIN cotizacion_detalle cd on ri.ingreso = cd.ingreso \n"
                        + "inner join cotizacion co on cd.idcotizacion = co.id\n"
                        + "WHERE coalesce(ri.usuarioapro,'') = '' and cd.idservicio in(4,6,9) and idconcepto = 2 and cd.idserviciodep > 0 and cd.ingreso = " + a_ingreso[x] + ";";
                sql += "UPDATE cotizacion SET archivo_aprueba = '2|" + numero + "', aprobada = 1, estado='Aprobado', fechaaprobacion = '" + fecha + "' \n"
                        + "from cotizacion_detalle cd inner join cotizacion co on co.id = cd.idcotizacion\n"
                        + "WHERE cotizacion.id = cd.idcotizacion and co.estado = 'Cerrado' and cd.ingreso = " + a_ingreso[x] + ";";

                sql += "UPDATE ingreso_plantilla set fechaaproajus='" + fecha + "'\n"
                        + "from cotizacion_detalle cd INNER JOIN cotizacion co on co.id = cd.idcotizacion \n"
                        + "INNER JOIN reporte_ingreso ri on ri.ingreso = cd.ingreso\n"
                        + "WHERE fechaaproajus is null and  coalesce(ri.usuarioapro,'') = '' and ingreso_plantilla.ingreso = cd.ingreso and ingreso_plantilla.idplantilla = ri.plantilla and cd.idservicio in(4,6,9) and idconcepto = 2 and cd.idserviciodep > 0 and cd.ingreso = " + a_ingreso[x] + ";";

                sql += "UPDATE reporte_ingreso SET archivo_aprueba='2|" + numero + "', observacionapro='Aprobado por órden de compra', usuarioapro='" + usuario + "', cedula='" + cedula + "',cargo='" + cargo + "'"
                        + ",fecaprobacion='" + fecha + "',tipoaprobacion='APROBADO AJUSTE / SUMINISTRO',tipo_aprobacion=1\n"
                        + "from cotizacion_detalle cd INNER JOIN cotizacion co on co.id = cd.idcotizacion \n"
                        + "WHERE coalesce(reporte_ingreso.usuarioapro,'') = '' and reporte_ingreso.ingreso = cd.ingreso and cd.idservicio in(4,6,9) and idconcepto = 2 and cd.idserviciodep > 0 and cd.ingreso = " + a_ingreso[x] + ";";

                sql += "UPDATE remision_detalle set fechaaprocoti='" + fecha + "' \n"
                        + "WHERE fechaaprocoti is null and ingreso = " + a_ingreso[x] + ";";
            }

            sql += " UPDATE remision_detalle set orden = 1 WHERE ingreso in (" + ingresos + ");";

            Globales.DatosAuditoria(sql, "ORDEN DE COMPRA", "AGREGANDO ORDEN DE COMPRA A INGRESOS", idusuario, iplocal, this.getClass() + "-->GuardarOrden");

            if (archivoadjunto != null) {
                File fileName = new File(Globales.ruta_archivo + "uploads/" + archivoadjunto);
                if (fileName.exists()) {
                    File filedestino = new File(Globales.ruta_archivo + "OrdenCompra/" + numero + ".pdf");
                    Files.copy(fileName.toPath(), filedestino.toPath());
                    archivoadjunto = null;
                } else {
                    return "1|Error al subir el archivo";
                }
            } else {
                return "1|Error al subir el archivo";
            }

            return "0|Orden de compra registrada con éxito";
        } catch (Exception ex) {
            return "9|" + ex.getMessage();
        }

    }

    public String EliminarOrden(HttpServletRequest request) {
        try {
            int id = Globales.Validarintnonull(request.getParameter("id"));
            int ingreso = Globales.Validarintnonull(request.getParameter("ingreso"));

            sql = "SELECT facturado, to_char(fechaing,'yyyy-MM-dd HH24:MI') as fecha "
                    + "from remision_detalle WHERE ingreso = " + ingreso;
            datos = Globales.Obtenerdatos(sql, this.getClass() + "-->EliminarOrden", 1);
            int encontrado;
            datos.next();

            encontrado = Globales.Validarintnonull(datos.getString("facturado"));

            String fecha = datos.getString("fecha");

            if (encontrado > 0) {
                return "1|No se puede eliminar la Orden de Compra a este equipo/ingreso que ya fue facturado";
            }

            sql = "DELETE FROM orden_compra WHERE id = " + id + "; "
                    + "UPDATE remision_detalle SET orden = 0 WHERE ingreso = " + ingreso + ";";
            sql += " INSERT INTO ingreso_estados (ingreso, idusuario, descripcion, tiempo) "
                    + "VALUES (" + ingreso + "," + idusuario + ",'ORDEN DE COMPRA DEL PROVEEDOR ELIMINADA',tiempo('" + fecha + "',now(),1))";
            Globales.DatosAuditoria(sql, "ORDEN DE COMPRA", "ELIMINAR ORDEN DE COMPRA", idusuario, iplocal, this.getClass() + "-->EliminarOrden");
            return "0|";
        } catch (SQLException ex) {
            return "9|" + ex.getMessage();
        }
    }

    public String ModalProveedores(HttpServletRequest request) {
        String Razon = request.getParameter("Razon");
        String Cedula = request.getParameter("Cedula");

        String Busqueda = "WHERE p.id <> 0 and (nombrecompleto ilike '%" + Razon + "%' or coalesce(nombrecom,'')  ilike '%" + Razon + "%') and documento ilike '%" + Cedula + "%' ";

        sql = "select documento, '<b>' || nombrecompleto || '</b>' || "
                + " case when coalesce(nombrecom,'') <> nombrecompleto then '<br>' || coalesce(nombrecom,'') else '' end as nombrecompleto, telefono, celular, email, direccion, pa.descripcion as pais "
                + "from proveedores p inner join pais pa on pa.id = p.idpais " + Busqueda + " ORDER BY 2 ";
        return Globales.ObtenerDatosJSon(sql, this.getClass() + "-->ModalProveedores");
    }

    protected void Servidor(HttpServletRequest request, HttpServletResponse response) {
        try {
            response.setContentType("text/html;charset=UTF-8");
            String ruta_origen = request.getHeader("Origin");

            HttpSession misession = request.getSession();
            idusuario = misession.getAttribute("idusuario").toString();
            iplocal = misession.getAttribute("IPLocal").toString();
            usuario = misession.getAttribute("codusu").toString();
            correoenvio = misession.getAttribute("correoenvio").toString();
            clavecorreo = misession.getAttribute("clavecorreo").toString();
            if (misession.getAttribute("ArchivosAdjuntos") != null) {
                archivoadjunto = misession.getAttribute("ArchivosAdjuntos").toString();
            }
            response.setHeader("Access-Control-Allow-Origin", request.getHeader("Origin"));
            response.setHeader("Access-Control-Allow-Credentials", "true");

            PrintWriter out = response.getWriter();
            if (Globales.ValidarRuta(ruta_origen) == false) {
                out.print("[{\"error\":\"505\",\"mensaje\":\"Usted no tiene permiso para acceder a esta ruta\"}]");
            } else if (misession.getAttribute("codusu") == null) {
                out.println("cerrada");
            } else if (!Globales.IpUsuario(misession.getAttribute("idusuario").toString()).equals(misession.getAttribute("IPLocal").toString())) {
                out.println("cambio-ip");
            } else {
                String opcion = request.getParameter("opcion");
                switch (opcion) {
                    case "EstadosIngresos":
                        out.println(EstadosIngresos());
                        break;
                    case "AlertaComercial":
                        out.println(AlertaComercial());
                        break;
                    case "AlertaFacturacion":
                        out.println(AlertaFacturacion());
                        break;
                    case "AlertaSolicitud":
                        out.println(AlertaSolicitud());
                        break;
                    case "AlertaLaboratorio":
                        out.println(AlertaLaboratorio());
                        break;
                    case "AlertaLogistica":
                        out.println(AlertaLogistica());
                        break;
                    case "TipoPrecio":
                        out.println(TipoPrecio(request));
                        break;
                    case "DescripcionPrecios":
                        out.println(DescripcionPrecios());
                        break;
                    case "AlertaCotizaApro":
                        out.println(AlertaCotizaApro());
                        break;

                    case "ModalClientes":
                        out.println(ModalClientes(request));
                        break;
                    case "BuscarCliente":
                        out.println(BuscarCliente(request));
                        break;
                    case "DesbloquearCliente":
                        out.println(DesbloquearCliente(request));
                        break;
                    case "TemporalIngresoCotiz":
                        out.println(TemporalIngresoCotiz(request));
                        break;
                    case "ConsultarCotizaciones":
                        out.println(ConsultarCotizaciones(request));
                        break;
                    case "ConsultarClienteAsesor":
                        out.println(ConsultarClienteAsesor(request));
                        break;
                    case "BuscarCotizacion":
                        out.println(BuscarCotizacion(request));
                        break;
                    case "ConsulIngSCotiza":
                        out.println(ConsulIngSCotiza(request));
                        break;
                    case "DatosContacto":
                        out.println(DatosContacto(request));
                        break;
                    case "ModalRemision":
                        out.println(ModalRemision(request));
                        break;
                    case "BuscarServicio":
                        out.println(BuscarServicio(request));
                        break;
                    case "MagnitudEquipo":
                        out.println(MagnitudEquipo(request));
                        break;
                    case "DetalleSolicitud":
                        out.println(DetalleSolicitud(request));
                        break;
                    case "MedidaIntervalo":
                        out.println(MedidaIntervalo(request));
                        break;
                    case "PrecioServicio":
                        out.println(PrecioServicio(request));
                        break;
                    case "VerificarServicio":
                        out.println(VerificarServicio(request));
                        break;
                    case "DiasEntregaSer":
                        out.println(DiasEntregaSer(request));
                        break;
                    case "TablaPrecioServicio":
                        out.println(TablaPrecioServicio(request));
                        break;
                    case "BuscarRemision":
                        out.println(BuscarRemision(request));
                        break;
                    case "TablaCotizacion":
                        out.println(TablaCotizacion(request));
                        break;
                    case "ModalIngreso":
                        out.println(ModalIngreso(request));
                        break;
                    case "RelaIngresoCoti":
                        out.println(RelaIngresoCoti(request));
                        break;
                    case "SepararCotizacion":
                        out.println(SepararCotizacion(request));
                        break;
                    case "GuardarPrecio":
                        out.println(GuardarPrecio(request));
                        break;
                    case "ConsulIngReportado":
                        out.println(ConsulIngReportado(request));
                        break;
                    case "AprobarCotizacion":
                        //out.println(AprobarCotizacion(request));
                        break;
                    case "EliminarRegCotizacion":
                        out.println(EliminarRegCotizacion(request));
                        break;
                    case "AgregarSolicitud":
                        out.println(AgregarSolicitud(request));
                        break;
                    case "DuplicarRegistro":
                        out.println(DuplicarRegistro(request));
                        break;
                    case "ConsultarOrden":
                        out.println(ConsultarOrden(request));
                        break;
                    case "GuardarMetodo":
                        out.println(GuardarMetodo(request));
                        break;
                    case "GuardarOpcion":
                        out.println(GuardarOpcion(request));
                        break;
                    case "GuardarIntervalo":
                        out.println(GuardarIntervalo(request));
                        break;
                    case "GuardarTmpCotizacion":
                        out.println(GuardarTmpCotizacion(request));
                        break;
                    case "ActualizarPrecio":
                        out.println(ActualizarPrecio(request));
                        break;
                    case "AplicarDescuento":
                        out.println(AplicarDescuento(request));
                        break;
                    case "ReemplazarCotizacion":
                        out.println(ReemplazarCotizacion(request));
                        break;
                    case "AnularCotizacion":
                        out.println(AnularCotizacion(request));
                        break;
                    case "GuardarPrecioSer":
                        out.println(GuardarPrecioSer(request));
                        break;
                    case "GuardarCotizacion":
                        out.println(GuardarCotizacion(request));
                        break;
                    case "AutoCompletarSerie":
                        out.println(AutoCompletarSerie());
                        break;
                    case "BuscarSedeCliente":
                        out.println(BuscarSedeCliente(request));
                        break;
                    case "CambioSede":
                        out.println(CambioSede(request));
                        break;
                    case "ConsultarIngresoDet":
                        out.println(ConsultarIngresoDet(request));
                        break;
                    case "ConsultarRemision":
                        out.println(ConsultarRemision(request));
                        break;
                    case "EquiposAgregados":
                        out.println(EquiposAgregados(request));
                        break;
                    case "GuardarTmpIngreso":
                        out.println(GuardarTmpIngreso(request));
                        break;
                    case "CambioSerie":
                        out.println(CambioSerie(request));
                        break;
                    case "BuscarIngreso":
                        out.println(BuscarIngreso(request));
                        break;
                    case "RpRemision":
                        out.println(RpRemision(request));
                        break;
                    case "EnvioRemision":
                        out.println(EnvioRemision(request));
                        break;
                    case "GuardarIngreso":
                        out.println(GuardarIngreso(request));
                        break;
                    case "EliminarFoto":
                        out.println(EliminarFoto(request));
                        break;
                    case "CantidadFoto":
                        out.println(CantidadFoto(request));
                        break;
                    case "EliminarIngreso":
                        out.println(EliminarIngreso(request));
                        break;
                    case "TemporalIngreso":
                        out.println(TemporalIngreso(request));
                        break;
                    case "Contadores":
                        out.println(Contadores(request, 0));
                        break;
                    case "ModalEmpresa":
                        out.println(ModalEmpresa(request));
                        break;
                    case "AnularDevolucion":
                        out.println(AnularDevolucion(request));
                        break;
                    case "TemporalDevolucion":
                        out.println(TemporalDevolucion(request));
                        break;
                    case "BuscarDevolucion":
                        out.println(BuscarDevolucion(request));
                        break;
                    case "GuardarDevolucion":
                        out.println(GuardarDevolucion(request));
                        break;
                    case "EntregarDevolucion":
                        out.println(EntregarDevolucion(request));
                        break;
                    case "ConsultarDevoluciones":
                        out.println(ConsultarDevoluciones(request));
                        break;
                    case "BuscarOrdenCompra":
                        out.println(BuscarOrdenCompra(request));
                        break;
                    case "TablaOrdenCompra":
                        out.println(TablaOrdenCompra(request));
                        break;
                    case "GuardarOrden":
                        out.println(GuardarOrden(request));
                        break;
                    case "EliminarOrden":
                        out.println(EliminarOrden(request));
                        break;
                    case "ModalProveedores":
                        out.println(ModalProveedores(request));
                        break;
                    case "EliminarFotoIndividual":
                        out.println(EliminarFotoIndividual(request));
                        break;
                    case "CantidadFotoDev":
                        out.println(CantidadFotoDev(request));
                        break;
                    case "EliminarFotoDev":
                        out.println(EliminarFotoDev(request));
                        break;
                    case "EliminarFotoDevIndividual":
                        out.println(EliminarFotoDevIndividual(request));
                        break;
                    case "EnvioCotizacion":
                        out.println(EnvioCotizacion(request));
                        break;
                    //FIN
                    default:
                        throw new AssertionError();
                }
            }
        } catch (Exception ex) {
            try {
                PrintWriter out = response.getWriter();
                out.println(ex.getMessage());

            } catch (IOException exl) {
                Globales.GuardarLogger(exl.getMessage(), Laboratorio.class
                        .getName());
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * param request servlet request param response servlet response throws
     * ServletException if a servlet-specific error occurs throws IOException if
     * an I/O error occurs
     *
     * @param request
     * @param response
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        Servidor(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * param request servlet request param response servlet response throws
     * ServletException if a servlet-specific error occurs throws IOException if
     * an I/O error occurs
     *
     * @param request
     * @param response
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        Servidor(request, response);
    }

}
