package socker;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public final class Message {

    @JsonProperty("username")
    private final String userName;
    @JsonProperty
    private final String message;
    @JsonProperty
    private final String tipo;
    @JsonProperty
    private final String vista;
    @JsonProperty
    private final String fecha;

    @JsonCreator
    public Message(@JsonProperty("username") final String userName, @JsonProperty("message") final String message, @JsonProperty("tipo") final String tipo, @JsonProperty("vista") final String vista, @JsonProperty("fecha") final String fecha) {
        Objects.requireNonNull(userName);
        Objects.requireNonNull(message);
        Objects.requireNonNull(tipo);
        Objects.requireNonNull(vista);
        Objects.requireNonNull(fecha);

        this.userName = userName;
        this.message = message;
        this.tipo = tipo;
        this.vista = vista;
        this.fecha = fecha;
    }

    String getUserName() {
        return this.userName;
    }

    String getMessage() {
        return this.message;
    }
    
    String getTipo() {
        return this.tipo;
    }
    
    String getVista() {
        return this.vista;
    }
    
    String getFecha() {
        return this.fecha;
    }
}
