package socker;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.websocket.CloseReason.CloseCodes;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;


@ServerEndpoint(value = "/{username}", encoders = MessageEncoder.class, decoders = MessageDecoder.class)
public final class socketConec {
    String sql = "";
        
    @OnOpen
    public void onOpen(@PathParam(Constants.USER_NAME_KEY) final String userName, final Session session) {
            if (Objects.isNull(userName) || userName.isEmpty()) {
            throw new RegistrationFailedException("User name is required");
        } else {
            session.getUserProperties().put(Constants.USER_NAME_KEY, userName);
            if (ChatSessionManager.register(session)) {
                System.out.printf("Session opened for %s\n", userName);
                sql = "update seguridad.rbac_usuario set chat=1 where nomusu= '" + userName + "'";
                DB.Globales.Obtenerdatos(sql, "Activar_Chat", 2);
                ChatSessionManager.publish(new Message((String) session.getUserProperties().get(Constants.USER_NAME_KEY), "***Inicio de sesión del usuario: " + userName + "***", "100","",""), session);
            } else {
                throw new RegistrationFailedException("Unable to register, username already exists, try another");
            }
        }
    }

    @OnError
    public void onError(final Session session, final Throwable throwable) {
        if (throwable instanceof RegistrationFailedException) {
            ChatSessionManager.close(session, CloseCodes.VIOLATED_POLICY, throwable.getMessage());
        }
    }

    @OnMessage
    public void onMessage(final Message message, final Session session) {
        if (message.getTipo().equals("1") || message.getTipo().equals("3") || message.getTipo().equals("4")){
             sql = "INSERT INTO mensajes(codusu, fecha, mensaje, idusuario) " +
                   "VALUES ('" + message.getUserName() + "','" + message.getFecha() + "','" + message.getMessage() + "','" + message.getVista() + "')";
             DB.Globales.Obtenerdatos(sql, "Guardar_Chat", 2);
        }
        ChatSessionManager.publish(message, session);
    }

    @OnClose
    public void onClose(final Session session) {
        if (ChatSessionManager.remove(session)) {
            sql = "update seguridad.rbac_usuario set chat=0 where nomusu= '" + session.getUserProperties().get(Constants.USER_NAME_KEY) + "'";
            DB.Globales.Obtenerdatos(sql, "Activar_Chat", 2);
            ChatSessionManager.publish(new Message((String) session.getUserProperties().get(Constants.USER_NAME_KEY), "***El usuario " + session.getUserProperties().get(Constants.USER_NAME_KEY) + " cerró sesión***", "101","",""),  session);
        }
    }

    private static final class RegistrationFailedException extends RuntimeException {

        private static final long serialVersionUID = 1L;

        public RegistrationFailedException(final String message) {            
            super(message);
            System.out.println("Aqui fallo");
        }
    }


}
