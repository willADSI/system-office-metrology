﻿angular.module('ngRouteMenu', ['ngRoute'])
    .controller('HomeController', function ($scope, $route) { $scope.$route = $route; })
    .controller('CajaController', function ($scope, $route) { $scope.$route = $route; })
    .config(function ($routeProvider) {

        $routeProvider.
            when('/home', {
                templateUrl: 'vistas/Home/Index.html',
                controller: 'HomeController'
            }).
            when('/caja', {
                templateUrl: '../vistas/Caja/RCaja.html',
                controller: 'CajaController'
            }).
            otherwise({
                redirectTo: '/home'
            });
    });