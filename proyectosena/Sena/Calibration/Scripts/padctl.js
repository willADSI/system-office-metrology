﻿var canvas, ctx;
var mouseX, mouseY, mouseDown = 0;
var touchX, touchY;

var lastX, lastY = -1;

var sise_p = 3;

function drawLine(ctx, x, y, size) {
    //console.log("X: " + x + " Y: " + y);
    // If lastX is not set, set lastX and lastY to the current position
    if (lastX == -1) {
        lastX = x;
        lastY = y;
    }

    // Let's use black by setting RGB values to 0, and 255 alpha (completely opaque)
    r = 0; g = 0; b = 0; a = 255;

    // Select a fill style
    ctx.strokeStyle = "rgba(" + r + "," + g + "," + b + "," + (a / 255) + ")";

    // Set the line "cap" style to round, so lines at different angles can join into each other
    ctx.lineCap = "round";
    //ctx.lineJoin = "round";

    // Draw a filled line
    ctx.beginPath();

    // First, move to the old (previous) position
    ctx.moveTo(lastX, lastY);

    // Now draw a line to the current touch/pointer position
    ctx.lineTo(x, y);

    // Set the line thickness and draw the line
    ctx.lineWidth = size;
    ctx.stroke();

    ctx.closePath();

    // Update the last position to reference the current position
    lastX = x;
    lastY = y;

    localStorage.setItem("FirmaCertificado", "1");

}

function clearCanvas(canvas, ctx) {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    localStorage.setItem("FirmaCertificado", "");
}

function sketchpad_mouseDown() {
    mouseDown = 1;
    drawLine(ctx, mouseX, mouseY, sise_p);
}

function sketchpad_mouseUp() {
    mouseDown = 0;

    lastX = -1;
    lastY = -1;
}

function sketchpad_mouseMove(e) {
    getMousePos(e);

    if (mouseDown == 1) {
        drawLine(ctx, mouseX, mouseY, sise_p);
    }
}

function getMousePos(e) {
    if (!e)
        var e = event;

    if (e.offsetX) {
        mouseX = e.offsetX;
        mouseY = e.offsetY;
    }
    else if (e.layerX) {
        mouseX = e.layerX;
        mouseY = e.layerY;
    }
}

function sketchpad_touchStart(e) {
    getTouchPos();

    drawLine(ctx, touchX, touchY, sise_p);

    event.preventDefault();
}

function sketchpad_touchEnd() {
    lastX = -1;
    lastY = -1;
}

function sketchpad_touchMove(e) {
    getTouchPos(e);

    drawLine(ctx, touchX, touchY, sise_p);

    event.preventDefault();
}

function getTouchPos(e) {
    if (!e)
        var e = event;
    
    if (e.touches) {
        if (e.touches.length == 1) {
            var touch = e.touches[0];
            console.log("pageX: " + touch.pageX + " pageY: " + touch.pageY + "clientX: " + touch.clientX + " clienteY: " + touch.clientY);
            //touchX = touch.pageX - touch.target.offsetLeft;
            //touchY = touch.pageY - touch.target.offsetTop;
            //touchX = touch.pageX - (this.getCanvasPos().left + window.pageXOffset);
            //touchY = touch.pageY - (this.getCanvasPos().top + window.pageYOffset);
            touchX = touch.pageX - (this.getCanvasPos().left);
            touchY = touch.pageY - (this.getCanvasPos().top);

            //console.log("X: " + touchX + " Y: " + touchY);
        }
    }
}
function getCanvas () {
    return this.canvas;
};

function getCanvasPos (el) {
    var canvas = document.getElementById(el) || this.getCanvas();
    var _x = canvas.offsetLeft;
    var _y = canvas.offsetTop;

    while (canvas = canvas.offsetParent) {
        _x += canvas.offsetLeft - canvas.scrollLeft;
        _y += canvas.offsetTop - canvas.scrollTop;
    }
    //console.log("X: " + _x + " Y: " + _y);
    return {
        left: _x,
        top: _y
    }
};
function initPad(ctl) {
    canvas = ctl;
    localStorage.setItem("FirmaCertificado", "");
        
    if (canvas.getContext)
        ctx = canvas.getContext('2d');

    if (ctx) {
        canvas.addEventListener('mousedown', sketchpad_mouseDown, false);
        canvas.addEventListener('mousemove', sketchpad_mouseMove, false);
        window.addEventListener('mouseup', sketchpad_mouseUp, false);

        canvas.addEventListener('touchstart', sketchpad_touchStart, false);
        canvas.addEventListener('touchend', sketchpad_touchEnd, false);
        canvas.addEventListener('touchmove', sketchpad_touchMove, false);
    }
}
