<html idioma="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
    <meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
    <title>System Office &amp; Metrology</title>
    <link href="../Scripts/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <link href="../Scripts/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="../Scripts/build/css/custom.css" rel="stylesheet" />
    <link href="../Scripts/css/londinium-theme.css" rel="stylesheet" type="text/css">
    <link href="../Scripts/css/sweetalert2.min.css" rel="stylesheet" />
    <link href="../Scripts/css/styles.css" rel="stylesheet" type="text/css">
    <link href="../Scripts/css/icons.css" rel="stylesheet" type="text/css">
    <link href="../Scripts/css/select2.min.css" rel="stylesheet" />
    <link href="../Scripts/cropper/dist/cropper.css" rel="stylesheet" />
    <link href="../Scripts/css/lestas.css" rel="stylesheet" />
    <link href="../Scripts/css/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="../Scripts/css/buttons.dataTables.min.css" rel="stylesheet" />
    <link href="../Scripts/css/tableexport.css" rel="stylesheet" />
    <link href="../Scripts/css/colReorder.dataTables.min.css" rel="stylesheet" />
    <link href="../Scripts/css/gallery.css" rel="stylesheet" />
    <link href="../Scripts/css/email-application.css" rel="stylesheet" />
    <link href="../Scripts/css/colors.css" rel="stylesheet" />
    <link href="../Scripts/css/jquery.spin.css" rel="stylesheet" />

    <script src="../Scripts/js/jquery.min.js"></script>
    <script src="../Scripts/js/jquery-ui.min.js"></script>
    <script src="../Scripts/jquery.modal.min.js"></script>
    <script src="../Scripts/json2.js"></script>
    <script type="text/javascript" src="../Scripts/js/plugins/charts/sparkline.min.js"></script>
    <script type="text/javascript" src="../Scripts/js/plugins/forms/uniform.min.js"></script>
    <script type="text/javascript" src="../Scripts/js/plugins/forms/inputmask.js"></script>
    <script type="text/javascript" src="../Scripts/js/plugins/forms/autosize.js"></script>
    <script type="text/javascript" src="../Scripts/js/plugins/forms/inputlimit.min.js"></script>
    <script type="text/javascript" src="../Scripts/js/plugins/forms/listbox.js"></script>
    <script type="text/javascript" src="../Scripts/js/plugins/forms/multiselect.js"></script>
    <script type="text/javascript" src="../Scripts/js/plugins/forms/validate.min.js"></script>
    <script type="text/javascript" src="../Scripts/js/plugins/forms/tags.min.js"></script>
    <script type="text/javascript" src="../Scripts/js/plugins/forms/switch.min.js"></script>
    <script type="text/javascript" src="../Scripts/js/plugins/forms/uploader/plupload.full.min.js"></script>
    <script type="text/javascript" src="../Scripts/js/plugins/forms/uploader/plupload.queue.min.js"></script>
    <script type="text/javascript" src="../Scripts/js/plugins/forms/wysihtml5/wysihtml5.min.js"></script>
    <script type="text/javascript" src="../Scripts/js/plugins/forms/wysihtml5/toolbar.js"></script>
    <script type="text/javascript" src="../Scripts/js/plugins/interface/daterangepicker.js"></script>
    <script type="text/javascript" src="../Scripts/js/plugins/interface/fancybox.min.js"></script>
    <script type="text/javascript" src="../Scripts/js/plugins/interface/moment.js"></script>
    <script type="text/javascript" src="../Scripts/js/plugins/interface/jgrowl.min.js"></script>

    <script src="../Scripts/js/datatable/jquery.dataTables.min.js"></script>
    <script src="../Scripts/js/datatable/dataTables.buttons.min.js"></script>
    <script src="../Scripts/js/datatable/buttons.flash.min.js"></script>
    <script src="../Scripts/js/datatable/jszip.min.js"></script>
    <script src="../Scripts/js/datatable/pdfmake.min.js"></script>
    <script src="../Scripts/js/datatable/vfs_fonts.js"></script>
    <script src="../Scripts/js/datatable/buttons.html5.min.js"></script>
    <script src="../Scripts/js/datatable/buttons.print.min.js"></script>
    <script src="../Scripts/js/datatable/buttons.colVis.min.js"></script>
    <script src="../Scripts/js/datatable/dataTables.colReorder.min.js"></script>

    <script type="text/javascript" src="../Scripts/js/plugins/interface/colorpicker.js"></script>
    <script type="text/javascript" src="../Scripts/js/plugins/interface/fullcalendar.min.js"></script>
    <script type="text/javascript" src="../Scripts/js/plugins/interface/timepicker.min.js"></script>
    <script type="text/javascript" src="../Scripts/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../Scripts/js/application.js"></script>
    <script src="../Scripts/js/jquery.spin.js"></script>
    <script src="../Scripts/js/tableexport.js"></script>
    <script src="../Scripts/js/select2.min.js"></script>
    <script src="../Scripts/js/sweetalert2.min.js"></script>
    <script src="../Scripts/js/jquery.backstretch.min.js"></script>
    <script src="../Scripts/js/nicetitle.js"></script>
</head>

<body class="full-width page-condensed">
    <!-- Navbar -->
    <div class="navbar navbar-inverse" role="navigation" style="height:120px; padding:0px 0px 0px 0px; background-color:aliceblue">
        <a class="navbar-brand" href="http://www.calibrationservicesas.com/">
            <img src="../logos/logo.png" height="90px" />
        </a>
    </div>
    <!-- /navbar -->
    <!-- Login wrapper -->
    <div class="login-wrapper">
        <form id="formAcceso">
            <input type="hidden" id="Idioma" value="1" />
            <input type="hidden" id="intentos" value="0" />
            <div class="popup-header">
                <span class="text-semibold">Control de Acceso</span>
                <div class="btn-group pull-right">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-cogs"></i></a>
                    <ul class="dropdown-menu icons-right dropdown-menu-right">
                        <li><a href="javascript:ReenviarClave()">Recordar Contraseña</a></li>
                    </ul>
                </div>
            </div>
            <div class="well">
                <div class="form-group has-feedback">
                    <label>Usuario</label>
                    <input type="text" class="form-control" placeholder="Usuario" name="usuario" id="usuario" autofocus autocomplete="off" required>
                    <i class="icon-users form-control-feedback"></i>
                </div>
                <div class="form-group has-feedback">
                    <label>Clave</label>
                    <input type="password" class="form-control" placeholder="Clave" name="clave" id="clave" required>
                    <i class="icon-lock form-control-feedback"></i>
                </div>
                <div class="row form-actions">
                    <div class="col-xs-6">
                        &nbsp;
                    </div>
                    <div class="col-xs-6">
                        <button type="submit" class="btn btn-glow btn-warning pull-right"><i class="icon-menu2"></i> Acceder</button>
                    </div>
                </div>

                <div class="form-group has-feedback">

                </div>

            </div>
        </form>
        <br />
        <div class="row">
            <div class="col-xs-6 col-md-6">
                <a target="_blank" href="https://chrome.google.com/webstore/detail/webrtc-network-limiter/npeicpdbkakmehahjeeohfdhnlpdklia/related?hl=en-US">Activador de IP Local</a>
            </div>
            <div class="col-xs-6 col-md-6">
                <a href="javascript:ModalCerrarSesionIni()">Cerrar Sesión Activa</a>
            </div>
        </div>
    </div>

    <!-- /login wrapper -->
    <!-- Footer -->
    <div class="footer clearfix">
        <div class="pull-left">&copy; 2013. Londinium Admin Template by <a href="http://themeforest.net/user/Kopyov">Eugene Kopyov</a></div>
        <div class="pull-right icons-group">
            <a href="#"><i class="icon-screen2"></i></a>
            <a href="#"><i class="icon-balance"></i></a>
            <a href="#"><i class="icon-cog3"></i></a>
        </div>
    </div>
    <!-- /footer -->

    <div id="modalCerrarSesion" class="modal fade" role="dialog">
        <form id="FormDesbAcceso">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header bg-success">

                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title"><i class="icon-accessibility"></i><span idioma="Cerrar Sesión">Cerrar Sesión</span></h4>
                    </div>
                    <div class="modal-body with-padding">
                        <div class="form-group has-feedback">
                            <label>Usuario</label>
                            <input type="text" class="form-control" name="CeUsuario" id="CeUsuario" value="" required autofocus>
                            <i class="icon-users form-control-feedback"></i>
                        </div>
                        <div class="form-group has-feedback">
                            <label>Clave</label>
                            <input type="password" class="form-control" name="CeClave" id="CeClave" required>
                            <i class="icon-lock form-control-feedback"></i>
                        </div>
                        <div class="form-group has-feedback">
                            <label>Cédula/Pasaporte/PP</label>
                            <input type="password" class="form-control" name="CeDocumento" id="CeDocumento" required>
                            <i class="icon-lock form-control-feedback"></i>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-glow btn-success" type="submit"><span idioma="Liberar Sesión">Liberar Sesión</span></button>
                        <button class="btn btn-glow btn-warning" type="button" data-dismiss="modal"><span idioma="Cerrar">Cerrar</span></button>
                    </div>
                </div>
            </div>
        </form>

    </div>

    <script>


        function ModalCerrarSesionIni() {
            //IPLocal();
            $("#CeUsuario").val("");
            $("#CeClave").val("");
            $("#CeDocumento").val("");
            $("#CeEmail").val("");
            $("#modalCerrarSesion").modal("show");
        }

        $("#formAcceso").submit(function(e){
            e.preventDefault();
            var parametros = $("#formAcceso").serialize() + "&opcion=BuscarUsuario";
            var datos = LlamarAjax("InicioSesion", parametros);
            var data = JSON.parse(datos);
            var _error = data[0].error;
            var _mensaje = data[0].mensaje;
            switch (_error) {
                case "0":
                    window.location.href =url_cliente + "Inicio/";
                    break;
                case "1":
                case "8":
                case "505":
                    $("#usuario").val("");
                    $("#clave").val("");
                    $("#usuario").focus();
                    swal("Acción Cancelada",_mensaje,"warning");
                    if (_error == "8")
                        IPLocal()
                    break;
                case "9":
                    localStorage.setItem("usuario_cambio",data[0].usuario);
                    window.location.href =url_cliente + "InicioSesion/CambioClave.php";
                    break;
              
            }
        });

        $("#FormDesbAcceso").submit(function (e) {
            e.preventDefault();
            
            var parametros = "usuario=" + $("#CeUsuario").val() + "&clave=" + $("#CeClave").val() + "&documento=" + $("#CeDocumento").val();
            var datos = LlamarAjax("InicioSesion","opcion=BuscarUsuario&" + parametros);
            var data = JSON.parse(datos);
            var _error = data[0].error;
            var _mensaje = data[0].mensaje;
            switch (_error) {
                case "0":
                    window.location.href =url_cliente + "Inicio/";
                    break;
                case "1":
                case "8":
                case "505":
                    $("#CeUsuario").val("");
                    $("#CeClave").val("");
                    $("#CeDocumento").val("");
                    $("#CeUsuario").focus();
                    swal("Acción Cancelada",_mensaje,"warning");
                    if (_error == "8")
                        IPLocal()
                    break;
            }
        })

        function ReenviarClave() {
            var usuario = $("#Usuario").val();
            if (usuario == "") {
                $("#Usuario").focus();
                swal("Acción Cancelada", "Debe de ingresar el usuario", "warning");
                return;
            }
            var datos = LlamarAjax("InicioSesion/EnvioClaveCorreo", "usuario=" + usuario).split("|");
            if (datos[0] == "0") {
                swal("", datos[1], "success");
            } else {
                swal("Acción Cancelada", datos[1], "warning");
            }
        }


    </script>
</body>
<script src="../js/FuncionesGenerales.js"></script>
<script>
    LlamarAjax("InicioSesion", "&opcion=CerrarSession");
    localStorage.clear();
    //IPLocal();
</script>
</html>
