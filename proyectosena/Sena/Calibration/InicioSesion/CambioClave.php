﻿<html idioma="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
    <meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
    <title>System Office &amp; Metrology</title>
    <link href="../Scripts/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <link href="../Scripts/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="../Scripts/build/css/custom.css" rel="stylesheet" />
    <link href="../Scripts/css/londinium-theme.css" rel="stylesheet" type="text/css">
    <link href="../Scripts/css/sweetalert2.min.css" rel="stylesheet" />
    <link href="../Scripts/css/styles.css" rel="stylesheet" type="text/css">
    <link href="../Scripts/css/icons.css" rel="stylesheet" type="text/css">
    <link href="../Scripts/css/select2.min.css" rel="stylesheet" />
    <link href="../Scripts/cropper/dist/cropper.css" rel="stylesheet" />
    <link href="../Scripts/css/lestas.css" rel="stylesheet" />
    <link href="../Scripts/css/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="../Scripts/css/buttons.dataTables.min.css" rel="stylesheet" />
    <link href="../Scripts/css/tableexport.css" rel="stylesheet" />
    <link href="../Scripts/css/colReorder.dataTables.min.css" rel="stylesheet" />
    <link href="../Scripts/css/gallery.css" rel="stylesheet" />
    <link href="../Scripts/css/email-application.css" rel="stylesheet" />
    <link href="../Scripts/css/colors.css" rel="stylesheet" />
    <link href="../Scripts/css/jquery.spin.css" rel="stylesheet" />

    <script src="../Scripts/js/jquery.min.js"></script>
    <script src="../Scripts/js/jquery-ui.min.js"></script>
    <script src="../Scripts/jquery.modal.min.js"></script>
    <script src="../Scripts/json2.js"></script>
    <script type="text/javascript" src="../Scripts/js/plugins/charts/sparkline.min.js"></script>
    <script type="text/javascript" src="../Scripts/js/plugins/forms/uniform.min.js"></script>
    <script type="text/javascript" src="../Scripts/js/plugins/forms/inputmask.js"></script>
    <script type="text/javascript" src="../Scripts/js/plugins/forms/autosize.js"></script>
    <script type="text/javascript" src="../Scripts/js/plugins/forms/inputlimit.min.js"></script>
    <script type="text/javascript" src="../Scripts/js/plugins/forms/listbox.js"></script>
    <script type="text/javascript" src="../Scripts/js/plugins/forms/multiselect.js"></script>
    <script type="text/javascript" src="../Scripts/js/plugins/forms/validate.min.js"></script>
    <script type="text/javascript" src="../Scripts/js/plugins/forms/tags.min.js"></script>
    <script type="text/javascript" src="../Scripts/js/plugins/forms/switch.min.js"></script>
    <script type="text/javascript" src="../Scripts/js/plugins/forms/uploader/plupload.full.min.js"></script>
    <script type="text/javascript" src="../Scripts/js/plugins/forms/uploader/plupload.queue.min.js"></script>
    <script type="text/javascript" src="../Scripts/js/plugins/forms/wysihtml5/wysihtml5.min.js"></script>
    <script type="text/javascript" src="../Scripts/js/plugins/forms/wysihtml5/toolbar.js"></script>
    <script type="text/javascript" src="../Scripts/js/plugins/interface/daterangepicker.js"></script>
    <script type="text/javascript" src="../Scripts/js/plugins/interface/fancybox.min.js"></script>
    <script type="text/javascript" src="../Scripts/js/plugins/interface/moment.js"></script>
    <script type="text/javascript" src="../Scripts/js/plugins/interface/jgrowl.min.js"></script>

    <script src="../Scripts/js/datatable/jquery.dataTables.min.js"></script>
    <script src="../Scripts/js/datatable/dataTables.buttons.min.js"></script>
    <script src="../Scripts/js/datatable/buttons.flash.min.js"></script>
    <script src="../Scripts/js/datatable/jszip.min.js"></script>
    <script src="../Scripts/js/datatable/pdfmake.min.js"></script>
    <script src="../Scripts/js/datatable/vfs_fonts.js"></script>
    <script src="../Scripts/js/datatable/buttons.html5.min.js"></script>
    <script src="../Scripts/js/datatable/buttons.print.min.js"></script>
    <script src="../Scripts/js/datatable/buttons.colVis.min.js"></script>
    <script src="../Scripts/js/datatable/dataTables.colReorder.min.js"></script>

    <script type="text/javascript" src="../Scripts/js/plugins/interface/colorpicker.js"></script>
    <script type="text/javascript" src="../Scripts/js/plugins/interface/fullcalendar.min.js"></script>
    <script type="text/javascript" src="../Scripts/js/plugins/interface/timepicker.min.js"></script>
    <script type="text/javascript" src="../Scripts/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../Scripts/js/application.js"></script>
    <script src="../Scripts/js/jquery.spin.js"></script>
    <script src="../Scripts/js/tableexport.js"></script>
    <script src="../Scripts/js/select2.min.js"></script>
    <script src="../Scripts/js/sweetalert2.min.js"></script>
    <script src="../Scripts/js/jquery.backstretch.min.js"></script>
    <script src="../Scripts/js/nicetitle.js"></script>
</head>

<body class="full-width page-condensed">
    <!-- Navbar -->
    <div class="navbar navbar-inverse" role="navigation" style="height:120px; padding:0px 0px 0px 0px; background-color:aliceblue">
        <a class="navbar-brand" href="http://www.calibrationservicesas.com/">
            <img src="~/logos/logo.png" width="35%" />
        </a>
    </div>
    
    <!-- /navbar -->
    <!-- Login wrapper -->
    <div class="login-wrapper">
        <form id="formCambioClave" role="form" method="post">
            <div class="popup-header">
                <span class="text-semibold">Cambio de Clave</span>
                
            </div>
            <div class="well">
                <div class="form-group has-feedback">
                    <label>Usuario</label>
                    <input type="text" class="form-control" name="Usuario" id="usuario" value="" readonly>
                    <i class="icon-users form-control-feedback"></i>
                </div>
                <div class="form-group has-feedback">
                    <label>Nueva Clave</label>
                    <input type="password" class="form-control" name="ClaveN" id="ClaveN" required autofocus>
                    <i class="icon-lock form-control-feedback"></i>
                </div>
                <div class="form-group has-feedback">
                    <label>Confirmar Clave</label>
                    <input type="password" class="form-control" name="ClaveC" id= "ClaveC" required>
                    <i class="icon-lock form-control-feedback"></i>
                </div>
                <div class="row form-actions">
                    <div class="col-xs-6">
                        &nbsp;
                    </div>
                    <div class="col-xs-6">
                        <button type="submit" class="btn btn-glow btn-warning pull-right"><i class="icon-menu2"></i> Acceder</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- /login wrapper -->
    <!-- Footer -->
    <div class="footer clearfix">
        <div class="pull-left">&copy; 2013. Londinium Admin Template by <a href="http://themeforest.net/user/Kopyov">Eugene Kopyov</a></div>
        <div class="pull-right icons-group">
            <a href="#"><i class="icon-screen2"></i></a>
            <a href="#"><i class="icon-balance"></i></a>
            <a href="#"><i class="icon-cog3"></i></a>
        </div>
    </div>
    <!-- /footer -->
    <script src="../js/FuncionesGenerales.js"></script>
    <script>
        var usuario = localStorage.getItem("usuario_cambio");
        if ($.trim(usuario) == ""){
            window.location.href =url_cliente;
            wewew = wewew23232;
        }

        $("#formCambioClave").submit(function(e){
            e.preventDefault();
            var parametros = $("#formCambioClave").serialize() + "&opcion=CambiarClaveIni";
            var datos = LlamarAjax("InicioSesion", parametros);
            var data = JSON.parse(datos);
            var _error = data[0].error;
            var _mensaje = data[0].mensaje;
            if (_error == "0"){
                window.location.href =url_cliente + "Inicio/";
            }else{
                $("#ClaveC").val("");
                $("#ClaveN").val("");
                $("#ClaveN").focus();
                swal("Acción Cancelada",_mensaje,"warning");
            }
        });

        $("#usuario").val(usuario);
    </script>
</body>
</html>