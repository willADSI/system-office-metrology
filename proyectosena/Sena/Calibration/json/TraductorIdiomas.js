﻿var ArchivoIdioma = null;
var LenguajeDataTable;
var Nuevo = 0;

function LlamarIdioma(idioma) {
    if ((localStorage.getItem("ArchivoIdioma") == null) || (localStorage.getItem("ArchivoIdioma") == "")) {
        localStorage.setItem("ArchivoIdioma", LlamarAjax("Idioma","opcion=LlamarArchivoIdioma&Idioma=" + idioma));
    }

    ArchivoIdioma = JSON.parse(localStorage.getItem("ArchivoIdioma"));

    return ArchivoIdioma;
}

function TablaIdiomas(Idioma) {

    ActivarLoad();
    setTimeout(function () {

        localStorage.setItem("ArchivoIdioma", "");
        datos = LlamarIdioma(Idioma);
                
        //var data = JSON.parse(datos);
        resultado = '<table class="table display  table- bordered" id="tablaidioma"><thead><tr><th><span idioma="Texto">' + ValidarTraduccion("Texto") + '</span></th> <th><span idioma="Traducción">' + ValidarTraduccion("Traducción") + '</span></th> <th width="5%">&nbsp;</th> <th width="5%">&nbsp;</th></tr ></thead > <tbody>';
        var editar = "";
        var eliminar = "";
        var x = 0;
        $.each(datos, function (i, item) {
            x++;
            resultado += "<tr id='td'" + x + ">";
            resultado += "<td>" + i + "</td>";
            resultado += "<td>" + item + "</td>";
            editar = "<button class='btn btn-info' type='button' onClick=\"EditarTraduccion('" + i + "','" + item + "')\"><span data-icon='&#xe16f;'></span></button > ";
            eliminar = "<button class='btn btn-danger' type='button' onClick=\"EliminarTraduccion('" + i + "')\"><span data-icon='&#xe0d8;'></span></button > ";
            resultado += "<td>" + editar + "</td>";
            resultado += "<td>" + eliminar + "</td>";
            resultado += "</tr>";
        });

        resultado += "</tbody></table>";

        $("#dividioma").html(resultado);
        ActivarDataTable("tablaidioma", 1);
        DesactivarLoad();
    },15);
    
}

function EliminarTraduccion(Titulo) {
    var idioma = $("#SIdioma").val();
    swal.queue([{
        title: '¿' + ValidarTraduccion("Seguro que desea eliminar esta fila de traducción") + '?',
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Si'),
        cancelButtonText: ValidarTraduccion('No'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        preConfirm: function () {
            return new Promise(function (resolve) {
                $.post(url + "Base/EliminarTraduccion?Titulo=" + Titulo+"&idioma=" + idioma)
                    .done(function (data) {
                        TablaIdiomas(idioma);
                        swal.insertQueueStep(ValidarTraduccion(data));
                        resolve()
                    })
            })
        }
    }]);
}

function TraducirTexto() {

    var idioma = $("#SIdioma").val() * 1;

    if (!(idioma))
        idioma = $("#menu_idioma").val();

    var sourceText = $.trim($("#Tra_Titulo").val());
    var sourceLang = "es";
    var targetLang = "en";

    switch (idioma) {

        case 1:
            targetLang = "es"
            break;
        case 2:
            targetLang = "en"
            break;
    }
        
    
    if (sourceText == "")
        return false;

    var datos = LlamarAjax("Base/TraducirTexto", "Titulo=&Idioma=1");
        
    var url = "https://translate.googleapis.com/translate_a/single";
    var parametros = "client=gtx&sl="+ sourceLang + "&tl=" + targetLang + "&dt=t&q=" + encodeURI(sourceText);

    //var result = JSON.parse(UrlFetchApp.fetch(url).getContentText());
    var result = LlamarAjaxJSon(url, parametros);
    
    var translatedText = result[0][0][0];

    $("#Traduccion").val(translatedText);
}

function TraducirLable(sourceText) {

    var sourceLang = "es";
    var targetLang = "en";

    if (sourceText == "")
        return false;

    var url = "https://translate.googleapis.com/translate_a/single";
    var parametros = "client=gtx&sl=" + sourceLang + "&tl=" + targetLang + "&dt=t&q=" + encodeURI(sourceText);

    //var result = JSON.parse(UrlFetchApp.fetch(url).getContentText());
    var result = LlamarAjaxJSon(url, parametros);
        
    if (result) 
        return translatedText = result[0][0][0];
    else
        return sourceText
        
}

function EditarTraduccion(titulo, traduccion) {

    $("#Tra_Titulo").val(titulo);
    $("#Tra_Traduccion").val(traduccion);
    $("#Tra_Titulo").prop("disabled", true);
    $("#ModalTraducirTexto").modal("show");
}

function NuevaTraduccion() {
        
    $("#Tra_Titulo").prop("disabled",false);
    $("#ModalTraducirTexto").modal("show");
}

function GuardarTraducir() {

    var idioma = $("#SIdioma").val();
    if (!(idioma))
        idioma = $("#menu_idioma").val();
    var titulo = $.trim($("#Tra_Titulo").val());
    var traduccion = $.trim($("#Tra_Traduccion").val());

    if (titulo == "") {
        $("#Tra_Titulo").focus();
        swal(ValidarTraduccion("Acción Cancelada"), ValidarTraduccion("Debe de ingresar un título"), "warning");
        return false;
    }

    if (traduccion == "") {
        $("#Tra_Traduccion").focus();
        swal(ValidarTraduccion("Acción Cancelada"), ValidarTraduccion("Debe de ingresar la traducción"), "warning");
        return false;
    }
    idioma = 2;
    var parametros = "idioma=" + idioma + "&Traduccion=" + traduccion + "&Titulo=" + titulo;
    var datos = LlamarAjax('Base/GuardarTraduccion', parametros).split("|");
    if (datos[0] == "0") {
        $("#Tra_Titulo").val("");
        $("#Tra_Traduccion").val("");
        $("#Tra_Titulo").prop("disabled", false);
        //TablaIdiomas(idioma);
        swal("", datos[1], "success");
        /*setTimeout(function () {
            TraducirIdioma(idioma);
        }, 100);*/
    } else
        swal("Acción Cancelada", datos[1], "warning");

};

function GuardarDocumento() {
    var documento = document.getElementById("documento");
    if ($.trim(documento.value) != "") {

        var file = documento.files[0];
        var data = new FormData();
        data.append('documento', file);
        var resultado = '';
        var direccion = url + "Base/Adjuntar";
        $.ajax({
            url: direccion,
            type: 'POST',
            contentType: false,
            data: data,
            processData: false,
            async: false,
            success: function (datos) {
                datos = datos.split("|");
                if (datos[0] != "0") {
                    swal(ValidarTraduccion("Error al cargar el archivo"), "", "error");
                } else {
                    if (datos[2] != "XX")
                        swal(ValidarTraduccion(datos[2]), "", "error");
                }
            }
        });
    }
}

function ImportarTraduccion() {
    var idioma = $("#SIdioma").val();
    swal.queue([{
        html: '<h3>' + ValidarTraduccion("Seleccione el archivo de excel de Idiomas") + '</h3><input type="file" class="form-control" id="documento" onchange="GuardarDocumento()" name="documento" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" required />',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Importar'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        preConfirm: function () {
            return new Promise(function (resolve) {
                $.post(url + "Base/Importartraduccion?idioma=" + idioma)
                    .done(function (data) {
                        swal.insertQueueStep(ValidarTraduccion(data))
                        TablaIdiomas(idioma);
                        resolve()
                    })
            })
        }
    }])
}


function TraducirIdioma(Idioma, Cambiar) {
        
    if (Cambiar)
        localStorage.setItem("ArchivoIdioma", "");
    Idioma = Idioma * 1;
    localStorage.setItem('idioma', Idioma);

    for (var x = 1; x <= 2; x++) {
        if (x == Idioma)
            $("#idioma" + x).addClass("active");
        else
            $("#idioma" + x).removeClass("active");
    }

    
    LlamarAjax("Idioma" , "opcion=CambioIdioma&idioma=" + Idioma);
    $("#menu_idioma").val(Idioma);
    
    switch (Idioma) {
        case 1:
            $("#ImagenIdioma").prop("src", url_cliente + "Scripts/images/flags/spanish.png");
            LenguajeDataTable = url_cliente + "json/Spanish.json";
            datos = LlamarIdioma(Idioma);
            ModulosTraducir(datos);
            $("#TipoIdiomaMenu").html("Español");
            break;
        case 2:
            $("#ImagenIdioma").prop("src", url_cliente + "Scripts/images/flags/english.png");
            LenguajeDataTable = url_cliente + "json/PortugueseBrasil.json";
            datos = LlamarIdioma(Idioma);
            ModulosTraducir(datos);
            $("#TipoIdiomaMenu").html("English");
            break;
        /*case 3:
            $("#TipoIdioma").html("Portugues");
            LenguajeDataTable = url_cliente + "json/PortugueseBrasil.json";
            datos = LlamarIdioma(Idioma);
            ModulosTraducir(datos);
            break;*/
    }
}

function escribir_archivo(valor, traduccion) {
    datos = LlamarAjax("Base/GuardarArchivo", "url=" + url + "&valor=" + valor + "&traduccion=" + traduccion);
}

function ModulosTraducir(datos) {
    Archivo = datos;
    var label2 = $("label");

            
    $("label[idioma]").each(function (index) {
        $(this).attr("title", $(this).attr("idioma"));
        $(this).html(ValidarTraduccion($(this).attr("idioma")));
    });

    $("span[idioma]").each(function (index) {
        $(this).attr("title", $(this).attr("idioma"));
        $(this).html(ValidarTraduccion($(this).attr("idioma")));
    });

    $("a[idioma]").each(function (index) {
        $(this).attr("title", $(this).attr("idioma"));
        $(this).html(ValidarTraduccion($(this).attr("idioma")));
    });

    $("title[idioma]").each(function (index) {
        $(this).html(ValidarTraduccion($(this).attr("idioma")));
    });

    $("th[idioma]").each(function (index) {
        $(this).attr("title", $(this).attr("idioma"));
        $(this).html(ValidarTraduccion($(this).attr("idioma")));
    });
    $("td[idioma]").each(function (index) {
        $(this).attr("title", $(this).attr("idioma"));
        $(this).html(ValidarTraduccion($(this).attr("idioma")));
    });

    $("option[idioma]").each(function (index) {
        $(this).html(ValidarTraduccion($(this).attr("idioma")));
    });
                
}

function TrasladoLote() {

    ActivarLoad();
    setTimeout(function () {

        $("span[idioma]").each(function (index, e) {
            original = $(this).attr("idioma")
            texto = e.innerHTML;
            console.log(texto);
            texto = texto.replace(/">/g, '|').replace(/"/g, '|').replace(/"/g, '');
            texto = texto.split("|");
            if (texto.length >= 5) {
                console.log(texto);
                texto = texto[4].split("</font>");
                traducido = texto[0];
            }else
                traducido = texto[0];
            if ($.trim(traducido) != $.trim(original)) {
                var parametros = "idioma=2&Traduccion=" + traducido + "&Titulo=" + original;
                LlamarAjax('Base/GuardarTraduccion', parametros);
            }
            
        });

        $("label[idioma]").each(function (index, e) {
            original = $(this).attr("idioma")
            texto = e.innerHTML;
            console.log(texto);
            texto = texto.replace(/">/g, '|').replace(/"/g, '|').replace(/"/g, '');
            texto = texto.split("|");
            if (texto.length >= 5) {
                console.log(texto);
                texto = texto[4].split("</font>");
                traducido = texto[0];
            } else
                traducido = texto[0];
            if ($.trim(traducido) != $.trim(original)) {
                var parametros = "idioma=2&Traduccion=" + traducido + "&Titulo=" + original;
                LlamarAjax('Base/GuardarTraduccion', parametros);
            }

        });

        $("th[idioma]").each(function (index, e) {
            original = $(this).attr("idioma")
            texto = e.innerHTML;
            console.log(texto);
            texto = texto.replace(/">/g, '|').replace(/"/g, '|').replace(/"/g, '');
            texto = texto.split("|");
            if (texto.length >= 5) {
                console.log(texto);
                texto = texto[4].split("</font>");
                traducido = texto[0];
            } else
                traducido = texto[0];
            if ($.trim(traducido) != $.trim(original)) {
                var parametros = "idioma=2&Traduccion=" + traducido + "&Titulo=" + original;
                LlamarAjax('Base/GuardarTraduccion', parametros);
            }

        });

        $("td[idioma]").each(function (index, e) {
            original = $(this).attr("idioma")
            texto = e.innerHTML;
            console.log(texto);
            texto = texto.replace(/">/g, '|').replace(/"/g, '|').replace(/"/g, '');
            texto = texto.split("|");
            if (texto.length >= 5) {
                console.log(texto);
                texto = texto[4].split("</font>");
                traducido = texto[0];
            } else
                traducido = texto[0];
            if ($.trim(traducido) != $.trim(original)) {
                var parametros = "idioma=2&Traduccion=" + traducido + "&Titulo=" + original;
                LlamarAjax('Base/GuardarTraduccion', parametros);
            }

        });

        DesactivarLoad();
    }, 15);
}

function ValidarTraduccion(valor) {
    var resultado = Archivo[$.trim(valor)];
    if (!(resultado))
        resultado = valor;
    //resultado = valor + "-NT";
        //resultado = "Traducido";
    //escribir_archivo(valor, resultado);
    return resultado;
}

$(this).bind("contextmenu", function (e) {
    console.log(e);
    
    //if (e.toElement.attributes.idioma) {
    if (e.originalEvent.path[2].attributes.idioma) {
        e.preventDefault();
        //var traduccion = e.toElement.innerHTML;
        traduccion = e.toElement.outerText;
        //var titulo = e.toElement.attributes.idioma.nodeValue;
        var titulo = e.originalEvent.path[2].attributes.idioma.nodeValue;

        $("#Tra_Titulo").val(titulo);
        $("#Tra_Traduccion").val(traduccion);
        $("#Tra_Titulo").prop("disabled", true);
        $("#ModalTraducirTexto").modal("show");
    }
        
});
