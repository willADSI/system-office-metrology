﻿
d = new Date();
month = d.getMonth() + 1;
day = d.getDate();

output = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-01';

output2 = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-' +
    (day < 10 ? '0' : '') + day;

$("#RFechaI").val(output);
$("#RFechaF").val(output2);

$("#RAsesor").html(CargarCombo(19, 1));
$("#RCliente").html(CargarCombo(9, 1));
var nfilas = 0;
var OpcionReporte = 1;

function Reporte(opcion) {

    var cliente = $("#RCliente").val() * 1;
    var recibo = $("#RNumero").val() * 1;
    var factura = $("#RFactura").val() * 1;
    var asesor = $("#RAsesor").val() * 1;
    var fechaf = $("#RFechaF").val();
    var fechad = $("#RFechaI").val();

    var ordenado = 0;
    var anulado = 0;

    if ($("#ROrdenarAse").prop("checked"))
        ordenado = 1

    if ($("#RAnulado").prop("checked"))
        anulado = 1


    $("#resultadopdf").removeAttr("src");

    ActivarLoad();
    setTimeout(function () {
        var parametros = "fechad=" + fechad + "&fechah=" + fechaf + "&recibo=" + recibo + "&factura=" + factura + "&cliente=" + cliente + "&asesor=" + asesor + "&anulado=" + anulado + "&opcion=" + opcion;
        var datos = LlamarAjax("Caja","opcion=ImprimirReporteRecibos&"+ parametros);
        datos = datos.split("|");
        if (datos[0] == "0") {
            switch (opcion) {
                case 1:
                    CargarTabla(datos[1]);
                    break;
                case 2:
                    window.open("DocumPDF/" + datos[1]);
                    break;

            }
        } else
            swal("Alerta", datos[1], "warning");
        DesactivarLoad();
    }, 15);
}

function CargarTabla(datos) {
    if (datos != "[]") {
        var datajson = JSON.parse(datos);
        $('#tablaconsulta').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "_caja" },
                { "data": "_concepto" },
                { "data": "_factura" },
                { "data": "_cliente" },
                { "data": "_asesor" },
                { "data": "_fecha" },
                { "data": "_valorfactura", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "_abonado", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "_efectivo", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "_tarjeta", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "_cheque", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "_consignacion", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "_fechamovimiento" },
                { "data": "_estado" },
                { "data": "_usuarioanula" },
                { "data": "_fechaanula" },
                { "data": "_reteiva", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "_retefuente", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "_reteica", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "_retecree", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "_descuento", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "_oingreso", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "_oegreso", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "_observacion" }
            ],

            "columnDefs": [
                { "type": "numeric-comma", targets: 3 }
            ],

            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy','colvis'
            ]
        });
    }

}


tb = $('input');
$(tb).keypress(enter2tab);

tb = $('select');
$(tb).keypress(enter2tab);

function enter2tab(e) {
    id = $(this).attr("id");
    if ((e.keyCode == 13) && ((id != "Codigo") || (id != "CodigoB"))) {
        cb = parseInt($(this).attr('tabindex'));
        if ($(':input[tabindex=\'' + (cb + 1) + '\']') != null) {
            $(':input[tabindex=\'' + (cb + 1) + '\']').focus();
            $(':input[tabindex=\'' + (cb + 1) + '\']').select();
            e.preventDefault();
            return false;
        }
    }
}

$('select').select2();
DesactivarLoad();
