﻿
d = new Date();
month = d.getMonth() + 1;
day = d.getDate();

output = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-' +
    (day < 10 ? '0' : '') + day;

$("#RFechaI").val(output);
$("#RFechaF").val(output);

CarCombFact();
var nfilas = 0;
var OpcionReporte = 1;

$("form").submit(function (e) {
    e.preventDefault();
})

function CarCombFact() {

    var datos = LlamarAjax(url + "Consignacion/CombosReportes", "");
    data = datos.split("|");

    $("#ReAsesor").html(data[0]);
    $("#ReDistrito").html(data[1]);
    $("#ReGrupo").html(data[2]);
    $("#ReCiudad").html(data[3]);
    $("#ReZVenta").html(data[4]);
    $("#ReZUbicacion").html(data[5]);

}

function Reporte(opcion) {

    var grupo = $("#ReGrupo").val() * 1;
    var distrito = $("#ReDistrito").val() * 1;
    var ciudad = $("#ReCiudad").val() * 1;
    var estado = $("#ReEstado").val();
    var asesor = $("#ReAsesor").val() * 1;
    var zventa = $("#ReZVenta").val() * 1;
    var zubicacion = $("#ReZUbicacion").val() * 1;

    var devolucion = $("#ReDevolucion").val() * 1;

    var cliente = $("#ReCliente").val() * 1;
    var tipo = $("#ReTipo").val();
    var estado = $("#ReEstado").val();
    var fechad = $("#ReFechaDesde").val();
    var fechah = $("#ReFechaHasta").val();
    var dias = $("#ReUltimoDia").val() * 1;

    if (fechad == "" && fechah == "" && dias == 0) {
        swal("", ValidarTraduccion("debe de ingresar una fecha o números de días"), "error");
        return false;
    }

    ActivarLoad();
    setTimeout(function () {
        var parametros = "opcion=" + opcion + "&devolucion=" + devolucion + "&cliente=" + cliente + "&estado=" + estado + "&tipo=" + tipo + "&fechad=" + fechad + "&fechah=" + fechah + "&dias=" + dias +
            "&asesor=" + asesor + "&distrito=" + distrito + "&ciudad=" + ciudad + "&estado=" + estado + "&grupo=" + grupo + "&zventa=" + zventa + "&zubicacion=" + zubicacion;
        var datos = LlamarAjax(url + "Consignacion/ReporteDevolucion", parametros);
        DesactivarLoad();

        datos = datos.split("|");

        if (datos[0] == "0") {
            if (opcion == 1) {
                var datajson = JSON.parse(datos[1]);
                $('#tablamodalrepfac').DataTable({
                    data: datajson,
                    bProcessing: true,
                    bDestroy: true,
                    columns: [
                        { "data": "Imprimir" },
                        { "data": "FvnNumero" },
                        { "data": "CodCli" },
                        { "data": "NomRazSoc" },
                        { "data": "FVnFechaFactura" },
                        { "data": "Estado" },
                        { "data": "VMontura", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                        { "data": "Subtotal", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                        { "data": "Iva", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                        { "data": "Total", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                        { "data": "usuario" },
                        { "data": "Observaciones" },
                        { "data": "usu_anul" },
                        { "data": "fecha_anula" },
                        { "data": "obser_anula" },
                    ],

                    "columnDefs": [
                        { "type": "numeric-comma", targets: 3 }
                    ],

                    "language": {
                        "url": LenguajeDataTable
                    },
                    dom: 'Bfrtip',
                    buttons: [
                        'excel', 'csv', 'copy','colvis'
                    ]
                });
            } else {
                window.open(url + "DocumPDF/" + datos[1]);
            }
        } else {
            swal(datos[1], "", "warning");
        }

    }, 15);

}

tb = $('input');
$(tb).keypress(enter2tab);

tb = $('select');
$(tb).keypress(enter2tab);

function enter2tab(e) {
    id = $(this).attr("id");
    if ((e.keyCode == 13) && ((id != "Codigo") || (id != "CodigoB"))) {
        cb = parseInt($(this).attr('tabindex'));
        if ($(':input[tabindex=\'' + (cb + 1) + '\']') != null) {
            $(':input[tabindex=\'' + (cb + 1) + '\']').focus();
            $(':input[tabindex=\'' + (cb + 1) + '\']').select();
            e.preventDefault();
            return false;
        }
    }
}

$('select').select2();