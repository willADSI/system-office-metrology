﻿
d = new Date();
month = d.getMonth() + 1;
day = d.getDate();

var Anio = $.trim(d.getFullYear());
Anio = $.trim(Anio.substr(2, 2))


output = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-01';

output2 = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-' +
    (day < 10 ? '0' : '') + day;

$("#CFechaDesde").val(output);
$("#CFechaHasta").val(output2);

$("#Pais").html(CargarCombo(18, 1));
$("#Pais").val(1).trigger("change");

tb = $('input');
$(tb).keypress(enter2tab);

tb = $('select');
$(tb).keypress(enter2tab);

var TablaEmpleados = "";
var IdActa = "";

function enter2tab(e) {
    if (e.keyCode == 13) {
        id = $(this).attr("id");

        cb = parseInt($(this).attr('tabindex'));

        if (id == "Documento") {
            if ($.trim($(this).val()) != "") {

                BuscarProveedor($(this).val());
                $("#Sede").focus();
                return false;
            }
        }

        if ($(':input[tabindex=\'' + (cb + 1) + '\']') != null) {
            $(':input[tabindex=\'' + (cb + 1) + '\']').focus();
            $(':input[tabindex=\'' + (cb + 1) + '\']').select();
            e.preventDefault();

            return false;
        }

        if ($(':select[tabindex=\'' + (cb + 1) + '\']') != null) {
            console.log("1");
            $(':select[tabindex=\'' + (cb + 1) + '\']').focus();
            $(':select[tabindex=\'' + (cb + 1) + '\']').select();
            e.preventDefault();

            return false;
        }
    }
}

$("#formActa").submit(function (e) {
    e.preventDefault();
    
    var fecha = $("#Fecha").val();
    var horad = $("#Horad").val();
    var horah = $("#Horah").val();
    var proximafecha = $("#ProximaFecha").val();
    var empleadocar = "";
    var empleadoasi = "";
    var a_empleadocar = document.getElementsByName("EmpleadoCargo[]");
    var a_empleadoasi = document.getElementsByName("EmpleadoAsis[]");

    if (fecha > output2) {
        $("#Fecha").focus()
        swal("Acción Cancelada", "La fecha del acta no puede ser mayor a " + output2, "warning");
        return false;
    }

    if (proximafecha != "") {
        if (proximafecha <= output2) {
            $("#ProximaFecha").focus()
            swal("Acción Cancelada", "La próxima fecha tentativa debe ser mayor a la fecha actual", "warning");
            return false;
        }
    }

    if (horad > horah) {
        $("#Horad").focus()
        swal("Acción Cancelada", "La hora inicial no puede ser mayor a la hora final", "warning");
        return false;
    }

    for (var x = 0; x < a_empleadocar.length; x++) {
        if (empleadocar != "")
            empleadocar += ",";
        empleadocar += a_empleadocar[x].value;
    }

    for (var x = 0; x < a_empleadoasi.length; x++) {
        if (empleadoasi != "")
            empleadoasi += ",";
        empleadoasi += a_empleadoasi[x].value;
    }

    var parametros = $("#formActa").serialize() + "&EmpleadoCar=" + empleadocar + "&EmpleadoAsi=" + empleadoasi;
           
    if (empleadocar == "") {
        swal("Acción Cancelada", "Debe de seleccionar por lo mínimo un encargado", "warning");
        return false;
    }
    
    var datos = LlamarAjax("Recursohumano/GuardarActaReunion", parametros).split("|");
    if (datos["0"] == "0") {
        if (IdActa == 0) {
            IdActa = datos[1];
            $("#IdActa").val(IdActa);
            $("#Usuario").val(datos[2]);
            $("#FechaReg").val(datos[3]);
            $("#Estado").val("Registrado");
            swal("", "Acta de reunión guardada con éxito", "success");
        } else
            swal("", "Acta de reunión actualizada con éxito", "success");
        return false;
    } else {
        swal("Acción Cancelada", datos[1], "warning");
        return false;
    }
});

function LimpiarTodo() {
    $("#IdActa").val("0");
    $("#Usuario").val("");
    $("#Fecha").val("");
    $("#FechaSus").val("");
    $("#Estado").val("Temporal");
    $("#Empleado").val("").trigger("change");
    $("#Magnitud").val("").trigger("change");
    $("#Procedimiento").val("").trigger("change");
    $("#Pais").val("1").trigger("change");
    $("#Ciudad").html("<option value''>--Seleccione--</option>");
}


$("#TablaAutorizaciones > tbody").on("dblclick", "tr", function () {
    var row = $(this).parents("td").context.cells;
    var id = row[1].innerText;
    var datos = LlamarAjax("Recursohumano/BuscarAutorizacion", "id=" + id);
    var data = JSON.parse(datos);
    $("#IdActa").val(id);
    $("#Usuario").val(data[0].usuario);
    $("#Fecha").val(data[0].fecha);
    $("#FechaSus").val(data[0].fechasus);
    $("#Estado").val(data[0].estado);
    $("#Empleado").val(data[0].idempleado).trigger("change");
    $("#Cargo").val(data[0].cargo);
    $("#Equipo").val(data[0].idequipo).trigger("change");
    $("#Procedimiento").val(data[0].idprocedimiento).trigger("change");
    $("#Pais").val(data[0].idpais).trigger("change");
    $("#Departamento").val(data[0].iddepartamento).trigger("change");
    $("#Ciudad").val(data[0].idciudad).trigger("change");
    $("#AreaCalibracion").val(data[0].idarecalibracion).trigger("change");
    $("#AreaMantenimiento").val(data[0].idaremantenimiento).trigger("change");

    $('#tab-autorizacion a[href="#tabcrear"]').tab('show')
});


function ConsultarAutorizacion() {

    var empleado = $("#CEmpleado").val() * 1;
    var cargo = $("#CCargo").val() * 1;
    var estado = $.trim($("#CEstado").val());
    var procedimiento = $("#CProcedimiento").val() * 1;

    var magnitud = $("#CMagnitud").val() * 1;
    var equipo = $("#CEquipo").val() * 1;
    var intervalo = $("#CIntervalo").val() * 1;
    var areamant = $("#CAreaMantenimiento").val() * 1;
    var areacalib = $("#CAreaCalibracion").val() * 1;

    var fechad = $("#CFechaDesde").val();
    var fechah = $("#CFechaHasta").val();

    if (fechad == "" && fechah == "") {
        swal("Acción Cancelada", ValidarTraduccion("debe de ingresar la fecha de búsqueda"), "warning");
        return false;
    }

    ActivarLoad();
    setTimeout(function () {
        var parametros = "empleado=" + empleado + "&cargo=" + cargo + "&procedimiento=" + procedimiento + "&estado=" + estado + "&magnitud=" +
            magnitud + "&equipo=" + equipo + "&intervalo=" + intervalo + "&areamant=" + areamant + "&areacalib=" + areacalib + "&fechad=" + fechad + "&fechah=" + fechah;
        var datos = LlamarAjax("Recursohumano/ConsultarAutorizacion", parametros);
        DesactivarLoad();
        var datajson = JSON.parse(datos);
        $('#TablaAutorizaciones').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "imprimir" },
                { "data": "id" },
                { "data": "empleado" },
                { "data": "cargo" },
                { "data": "magnitud" },
                { "data": "equipo" },
                { "data": "intervalo" },
                { "data": "procedimiento" },
                { "data": "ciudad" },
                { "data": "areacalibracion" },
                { "data": "areamantenimiento" },
                { "data": "fecha" },
                { "data": "estado" },
                { "data": "usuario" },
                { "data": "actualizado" },
                { "data": "suspendido" },
                { "data": "observacionsus" }
            ],

            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy','colvis'
            ]
        });
    }, 15);
}

function CambioDepartamento(pais) {
    $("#Departamento").html(CargarCombo(11, 1, "", pais));
    $("#Ciudad").html("");
}

function CambioCiudad(departamento) {
    $("#Ciudad").html(CargarCombo(12, 1, "", departamento));
}

function Suspender() {
    var id = $("#IdActa").val() * 1;
    var estado = $("#Estado").val();

    var texto = $("#Empleado option:selected").text();
    var arreglo = texto.split("|");
    var empleado = arreglo[0];

    var equipo = $("#Equipo option:selected").text();

    if (id == 0) {
        swal("Acción Cancelada", "Debe de seleccionar una autorización registrada", "warning");
        return false;
    }
    if (estado != "Autorizado") {
        swal("Acción Cancelada", "Esta autorización ya fue suspendida", "warning");
        return false;
    }

    swal({
        title: 'Advertencia',
        text: '¿Desea suspender la autorización del empleado ' + empleado + ' de la magnitud y equipo ' + equipo + '?',
        input: 'text',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Suspender'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',

        inputValidator: function (value) {
            return new Promise(function (resolve, reject) {
                if (value) {
                    var datos = LlamarAjax("Recursohumano/SuspenderAutorizacion", "id=" + id + "&observacion=" + value);
                    datos = datos.split("|");
                    if (datos[0] == "0") {
                        $("#Estado").val("Suspendido");
                        $("#FechaSus").val(datos[1]);
                        resolve()
                    } else {
                        reject(datos[1]);
                    }

                } else {
                    reject(ValidarTraduccion('Debe de ingresar el motivo de la suspención'));
                }
            })
        }
    }).then(function (result) {
        swal({
            type: 'success',
            html: 'Suspención realizada con éxito'
        })
    });
}

function LlamarPersonal(tipo) {
    if (tipo == 1)
        $("#TipoEmpleado").html("ENCARGADO")
    else
        $("#TipoEmpleado").html("ASISTENCIA")

    CargarModalEmpleadosActa()
    $("#modalAsistencia").modal("show");
}

$('#tablamodalempleados tbody').on('click', 'tr', function () {
    $(this).toggleClass('selected');
});


function CargarModalEmpleadosActa() {

    var Nombre = "";
    var Documento = 0;

    var parametros = "nombre=" + Nombre + "&documento=" + Documento + "&tipodocumento=T&estado=TODOS&cargo=0";
    var datos = LlamarAjax('Recursohumano/ConsultarEmpleados', parametros);
    var datajson = JSON.parse(datos);
    TablaEmpleados = $('#tablamodalempleados').DataTable({
        data: datajson,
        bProcessing: true,
        bDestroy: true,
        paging: true,
        ordering: true,
        columns: [
            { "data": "tipodocumento" },
            { "data": "documento" },
            { "data": "nombrecompleto" },
            { "data": "cargo" },
            { "data": "sueldo", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
            { "data": "celular" },
            { "data": "email" }
        ],
        "lengthMenu": [[5], [5]],
        "language": {
            "url": LenguajeDataTable
        }

    });


}

function BuscarEmpleado(tipo, documento) {
    var arregloe;
    if (tipo == "ENCARGADO")
        arregloe = document.getElementsByName("EmpleadoAsis[]");
    else
        arregloe = document.getElementsByName("EmpleadoCargo[]");

    for (var x = 0; x < arregloe.length; x++) {
        if (documento == arregloe[x].value) {
            return x
        }
    }
    return -1;
}

function CargarTabla(tipo) {
    var clase = "";
    var tabla = "";
    resultado = "";
    var posicion = -1;
    if (tipo == "ENCARGADO") {
        clase = ".classcargo";
        tabla = "Responsables";
    } else {
        clase = ".classasis";
        tabla = "Asistentes";
    }
    $(clase).each(function () {
        posicion++;
        resultado += "<tr class='" + (tipo == "ENCARGADO" ? "classcargo" : "classasis") + "' id='tr-" + (tipo == "ENCARGADO" ? "cargo" : "asis") + "-" + posicion + "'>" +
            "<td>" + (posicion + 1) + "</td>" +
            "<td>" + $(this).find("td").eq(1).html() + "</td>" +
            "<td>" + $(this).find("td").eq(2).html()  + "</td>" +
            "<td>&nbsp;</td>" +
            "<td align='center'>" + $(this).find("td").eq(4).html() + "</td>" +
            "</tr>";
    });
    $("#" + tabla).html(resultado);
}


function AgregarEmpleados() {
    var tipo = $("#TipoEmpleado").html();
    var datos = TablaEmpleados.rows('.selected').data();
    var posicion;
    console.log(datos);
    var agregado = -1;
    var resultado = "";
    var arreglo;
    if (tipo == "ENCARGADO") {
        arreglo = document.getElementsByName("EmpleadoCargo[]")
        resultado = $("#Responsables").html();
    } else {
        resultado = $("#Asistentes").html();
        arreglo = document.getElementsByName("EmpleadoAsis[]");
    }
    if (arreglo.length > 0)
        agregado += arreglo.length

    for (var x = 0; x < datos.length; x++) {
        posicion = BuscarEmpleado(tipo, datos[x].documento)
        if (tipo == "ENCARGADO") {
            if (posicion >= 0) {
                $("#tr-asis-" + posicion).remove();
                CargarTabla("ASISTENTE")
            }
            posicion = BuscarEmpleado("ASISTENTE", datos[x].documento)
        } else {
            if (posicion == -1)
                posicion = BuscarEmpleado("ENCARGADO", datos[x].documento)
        }
            
        if (posicion == -1) {
            agregado++;
            resultado += "<tr class='" + (tipo == "ENCARGADO" ? "classcargo" : "classasis") + "' id='tr-" + (tipo == "ENCARGADO" ? "cargo" : "asis") + "-" + agregado + "'>" +
                "<td name='orden-" + (tipo == "ENCARGADO" ? "cargo" : "asis") + "[]'>" + (agregado + 1) + "</td>" +
                "<td>" + datos[x].nombrecompleto + "</td>" +
                "<td>" + datos[x].cargo + "<input type ='hidden' value='" + datos[x].documento + "' name = '" + (tipo == "ENCARGADO" ? "EmpleadoCargo[]" : "EmpleadoAsis[]") + "' ></td > " +
                "<td>&nbsp;</td>" +
                "<td align='center'><button class='btn btn-danger " + (tipo == "ENCARGADO" ? "borrarcargo" : "borrarasis")  + "' title='Eliminar Empleado' type='button'><span data-icon='&#xe0d8;'></span></button></td>" +
                "</tr>";
        }
    }
    if (tipo == "ENCARGADO") {
        $("#Responsables").html(resultado);
    } else {
        $("#Asistentes").html(resultado);
    }
}

$(document).on('click', '.borrarcargo', function (event) {
    event.preventDefault();
    $(this).closest('tr').remove();
    CargarTabla("ENCARGADO");
});

$(document).on('click', '.borrarasis', function (event) {
    event.preventDefault();
    $(this).closest('tr').remove();
    CargarTabla("ASISTENTE");
});

function QuitarTodos() {
    $("#Asistentes").html("");
}

function AgregarAnexos() {
    var id = $("#IdActa").val() * 1;
    if (id > 0) {
        swal.queue([{
            html: '<h3>' + ValidarTraduccion("Seleccione el archivo que desea subir como anexo") + '</h3><input type="file" class="form-control" id="documento" onchange="GuardarDocumento()" name="documento" required />',
            showLoaderOnConfirm: true,
            showCancelButton: true,
            confirmButtonText: ValidarTraduccion('OK'),
            cancelButtonText: ValidarTraduccion('No'),
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            preConfirm: function () {
                return new Promise(function (resolve, reject) {
                    resolve();
                })
            }
        }]);
    } else
        swal("Acción Cancelada", "Debe primero guardar la acta de reunión", "warning");
}

$('select').select2();
DesactivarLoad();


