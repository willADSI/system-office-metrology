﻿var Cuenta = 0;
var IdCuenta = 0;

$("#Tipo, #BTipo").html(CargarCombo(85, 7));
$("#Grupo, #BGrupo").html(CargarCombo(86, 7));
$("#Clasificacion, #BClasificacion").html(CargarCombo(87, 7));
$("#Nivel").val("1").trigger("change");
$("#Ret_Estado").html(CargarCombo(37, 0));

$("#Ret_Debito, #Ret_Credito").html(CargarCombo(83, 7));


$("#formCuentas").submit(function (e) {
    e.preventDefault();
    var parametros = $("#formCuentas").serialize() + "&Id=" + IdCuenta;
    var datos = LlamarAjax("ConfigContable/GuardarCuenta", parametros).split("|");
    if (datos[0] == "0") {
        swal("", datos[1], "success");
        Cuenta = "";
        $("#Cuenta").val("");
        LimpiarDatos(1);
        TablaCuentas();
        $("#Ret_Debito, #Ret_Credito").html(CargarCombo(83, 1));
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
});

function ValidarCuenta(Caja) {
    documento = Caja*1;
    if (Cuenta != 0) {
        if (Cuenta != documento) {
            Cuenta = 0;
            LimpiarDatos(1);
        }
    }
}

$("#TablaCuenta > tbody").on("dblclick", "tr", function (e) {
    var row = $(this).parents("td").context.cells;
    var cuenta = $.trim(row[0].innerText).replace(/-/g, '');
    $("#Cuenta").val(cuenta);
    BuscarCuenta(cuenta);
});

function BuscarCuenta(cuenta) {
    cuenta = cuenta * 1;
    if (cuenta == 0)
        return false;
    var datos = LlamarAjax("ConfigContable/BuscarCuenta", "cuenta=" + cuenta);
    if (datos != "[]") {
        var data = JSON.parse(datos);
        IdCuenta = data[0].id * 1;
        Cuenta = data[0].cuenta * 1;
        $("#Nivel").val(data[0].nivel).trigger("change");
        $("#Nombre").val(data[0].nombre);
        $("#SubCuenta").val(data[0].subcuenta);
        $("#Tipo").val(data[0].idtipo).trigger("change");
        $("#Grupo").val(data[0].idgrupo).trigger("change");
        $("#Clasificacion").val(data[0].idclasificacion).trigger("change");
        $("#CuentaContable").val(data[0].idcuecontable).trigger("change");
        if (data[0].visible*1 == 0)
            $("#Visible").prop("checked", "");
        if (data[0].ajusteinflacion * 1 > 0)
            $("#Ajuste").prop("checked", "");
        $("#Nivel").focus();
    }

}

function LimpiarTodo() {
    $("#Nivel").val("1").trigger("change");
    LimpiarDatos(1);
    Cuenta = "";
    $("#Cuenta").val("");
}

function LimpiarDatos(tipo) {
    switch (tipo) {
        case 1:
            IdCuenta = 0;
            $("#Nombre").val("");
            $("#SubCuenta").val("");
            $("#Tipo").val("0").trigger("change");
            $("#Grupo").val("0").trigger("change");
            $("#Clasificacion").val("0").trigger("change");
            $("#Visible").prop("checked", "");
            $("#Ajuste").prop("checked", "");
            $("#Visible").prop("checked", "checked");
            break;
        case 2:
            $("#Ret_Id").val("0");
            $("#Ret_Descripcion").val("");
            $("#Ret_Porcentaje").val("");
            $("#Ret_Tipo").val("").trigger("change");
            $("#Ret_Debito").val("0").trigger("change");
            $("#Ret_Credito").val("0").trigger("change");
            $("#Ret_Estado").val("1").trigger("change");
            $("#Ret_Operacion").val("").trigger("change");
            break;
    }
}


function TablaCuentas() {
    var nivel = $("#BNivel").val() * 1;
    var cuenta = $("#BCuenta").val()*1;
    var subcuenta = $("#BSubCuenta").val()*1;
    var visible = $("#BVisible").val();
    var grupo = $("#BGrupo").val() * 1;
    var tipo = $("#BTipo").val() * 1;
    var clasificacion = $("#BClasificacion").val() * 1;
    var parametros = "nivel=" + nivel + "&cuenta=" + cuenta + "&subcuenta=" + subcuenta + "&visible=" + visible + "&grupo=" + grupo + "&tipo=" + tipo + "&clasificacion=" + clasificacion;
    ActivarLoad();
    setTimeout(function () {
        var datos = LlamarAjax('ConfigContable/ListarCuenta', parametros);
        var dataconjson = JSON.parse(datos);
        $('#TablaCuenta').DataTable({
            data: dataconjson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "cuenta" },
                { "data": "nombre" },
                { "data": "subcuenta" },
                { "data": "nivel" },
                { "data": "tipo" },
                { "data": "grupo" },
                { "data": "clasificacion" },
                { "data": "visible" },
                { "data": "inactiva" },
                { "data": "ajusteinflacion" },
                { "data": "eliminar" }
            ],
            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy', 'colvis'
            ],
            "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                if (aData.inactiva == "NO") {
                    $('td', nRow).css('background-color', 'aquamarine');
                }
            }
        });
        DesactivarLoad();
    }, 15);
}

function CambioNivel(nivel) {
    var datos = LlamarAjax("ConfigContable/DatosNivelCuenta", "nivel=" + nivel) * 1;
    $("#Cuenta").attr("maxlength", datos);
    $("#Cuenta").attr("pattern", "[0-9]{" + datos + "," + datos + "}");

    $("#caracterescuenta").html(datos + " caracter(es)");

}

function EliminarCuenta(id, Cuenta, Nombre) {
    var accion = 1;
    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: ValidarTraduccion('¿Desea eliminar la cuenta ' + Cuenta + ' ' + Nombre + '?'),
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Eliminar'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        allowOutsideClick: false,

        preConfirm: function (result) {
            return new Promise(function (resolve, reject) {
                $.post("Configuracion/EliminarCuenta", "Id=" + id + "&Cuenta=" + Cuenta)
                    .done(function (data) {
                        datos = data.split("|");
                        if (datos[0] == "0")
                            resolve()
                        else
                            reject(datos[1]);

                    })
            })
        }
    }]).then(function (data) {
        LimpiarDatos(1);
        TablaCuentas();
        $("#Ret_Debito, #Ret_Credito").html(CargarCombo(83, 1));
    });
}

//////////////////////////////////////////******** RETENCIONES ********** ///////////////////////////////////////

function TablaRetenciones() {
    ActivarLoad();
    setTimeout(function () {
        var datos = LlamarAjax('ConfigContable/ListarRetenciones', '');
        var dataconjson = JSON.parse(datos);
        $('#TablaRetenciones').DataTable({
            data: dataconjson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "id" },
                { "data": "operacion" },
                { "data": "tipo" },
                { "data": "descripcion" },
                { "data": "porcentaje" },
                { "data": "cuedebito" },
                { "data": "cuecredito" },
                { "data": "desestado" },
                { "data": "eliminar" }
            ],
            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy', 'colvis'
            ],
            "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                if (aData.inactiva == "NO") {
                    $('td', nRow).css('background-color', 'aquamarine');
                }
            }
        });
        DesactivarLoad();
    }, 15);
}

$("#formRetencion").submit(function (e) {
    e.preventDefault();
    var parametros = $("#formRetencion").serialize();
    var datos = LlamarAjax("ConfigContable/GuardarRetencion", parametros).split("|");
    if (datos[0] == "0") {
        swal("", datos[1], "success");
        LimpiarDatos(2);
        TablaRetenciones();
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
});

$("#TablaRetenciones > tbody").on("dblclick", "tr", function (e) {
    var row = $(this).parents("td").context.cells;
    var retencion = row[0].innerText;
    $("#Ret_Id").val(retencion);
    BuscarRetencion(retencion);
});

function BuscarRetencion(retencion) {
    retencion = retencion * 1;
    if (retencion == 0)
        return false;
    var datos = LlamarAjax("ConfigContable/BuscarRetencion", "retencion=" + retencion);
    if (datos != "[]") {
        var data = JSON.parse(datos);
        IdCuenta = data[0].id * 1;
        Cuenta = data[0].cuenta * 1;
        $("#Ret_Id").val(data[0].id);
        $("#Ret_Tipo").val(data[0].tipo).trigger("change");
        $("#Ret_Operacion").val(data[0].operacion).trigger("change");
        $("#Ret_Descripcion").val(data[0].descripcion);
        $("#Ret_Porcentaje").val(formato_numero(data[0].porcentaje,5,_CD,_CM,""));
        $("#Ret_Debito").val(data[0].cuenta_debito).trigger("change");
        $("#Ret_Credito").val(data[0].cuenta_credito).trigger("change");
        $("#Ret_Estado").val(data[0].estado).trigger("change");
        $("#Ret_Tipo").focus();
    }
}

function EliminarRetencion(id, retencion) {
    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: ValidarTraduccion('¿Desea eliminar la retención ' + retencion + '?'),
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Eliminar'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        allowOutsideClick: false,

        preConfirm: function (result) {
            return new Promise(function (resolve, reject) {
                $.post("ConfigContable/EliminarRetencion", "Id=" + id)
                    .done(function (data) {
                        datos = data.split("|");
                        if (datos[0] == "0")
                            resolve()
                        else
                            reject(datos[1]);

                    })
            })
        }
    }]).then(function (data) {
        LimpiarDatos(2);
        TablaRetenciones();
    });
}

TablaCuentas();
TablaRetenciones();



$('select').select2();
DesactivarLoad();
