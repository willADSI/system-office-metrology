﻿var IdEmpleado = 0;
var DocumentoEmp = "";




function GuardarDocumentoArchivo() {
    if (IdEmpleado > 0) {
        var archivo = $.trim($("#ArchivoEmpleado").val());

        var tipo = $("#TipoDocumentoEmpleado").val() * 1;
        var numero = $("#NroDocumentoEmpleado").val() * 1;

        if (tipo == 0) {
            $("#TipoDocumentoEmpleado").focus();
            swal("Acción Cancelada", "debe de seleccionar tipo de documento", "warning");
            return false;
        }
        if (archivo == "") {
            $("#ArchivoEmpleado").focus();
            swal("Acción Cancelada", "debe de seleccionar el archivo en pdf del documento", "warning");
            return false;
        }

        var parametros = "Empleado=" + IdEmpleado + "&Documento=" + tipo + "&Numero=" + numero;
        console.log(parametros);
        var datos = LlamarAjax("Recursohumano","opcion=GuardarDocumentosEmpleados&" + parametros).split("|");
        if (datos[0] == "0") {
            CambioDocumento(tipo);
        } else
            swal("Acción Cancelada", datos[1], "warning");
    }
}


function CambioDocumentoEmpleado(documento) {
    $("#ArchivoEmpleado").val("");

    if (documento * 1 == 0) {
        $("#DocumentoEmpleado").attr("src", url_cliente + "DocumentosEmpleados/0.pdf?id=" + NumeroAleatorio());
        $("#CreacionDocumento").html("");
        return false;
    }

    if (IdEmpleado > 0) {

        var parametros = "Empleado=" + IdEmpleado + "&Documento=" + documento + "&Numero=" + $("#NroDocumentoEmpleado").val();
        var datos = LlamarAjax("Recursohumano","opcion=DocumentosEmpleado&" + parametros);
        if (datos == "[]") {
            $("#DocumentoEmpleados").attr("src", url_cliente + "DocumentosEmpleados/0.pdf?id=" + NumeroAleatorio());
            $.jGrowl("Este documento no se le ha cargado al Empleado", { life: 3000, theme: 'growl-warning', header: '' });
        } else {
            var data = JSON.parse(datos);
            $("#DocumentoEmpleados").attr("src", url_cliente + "DocumentosEmpleados/" + IdEmpleado + "/" + documento + "-"+ $("#NroDocumentoEmpleado").val() + ".pdf?id=" + NumeroAleatorio());
            $("#CreacionDocumento").html("Creado por " + data[0].usuario + " el día " + data[0].fecha);
        }
    } else {
        $("#TipoDocumentoEmpleado").val("").trigger("change");
        swal("Acción Cancelada", "Debe de seleccionar un empleado, o guardar los datos", "warning");
    }
}


function TabDocumentoEmpleados() {
    
    var parametros = "Empleado=" + IdEmpleado + "&Documento=0";
    var datos = LlamarAjax("Recursohumano","opcion=TablaDocumentosEmpleado&" + parametros);
    var data = JSON.parse(datos);
    var resultado = "";
    for (var x = 0; x < data.length; x++) {
        let descripcion = (data[x].descripcion !== undefined ? (data[x].descripcion) : descripcion);
        resultado += "<tr " + (data[x].obligatorio*1 == 1 ? "class='bg-rojoclaro' " : "") + ">" +
            "<td>" + (data[x].nrodocumento * 1 > 0 ? data[x].nrodocumento : '' ) + "</td>" + 
            "<td>" + ((data[x].nrodocumento * 1 == 0) ? "<strong>" + descripcion + "</strong>" : "     - Documento-" + data[x].nrodocumento * 1 ) + "</td>" +
            "<td>" +
            ((data[x].nrodocumento * 1 == 0) ? "" : "<button type = 'button' class='btn btn-glow btn-info btn-icon2' style = 'max-width:50px; padding-bottom: 3px;' title = 'Descargar PDF' onclick = 'javascript:DescargarPDF(" + (data[x].nrodocumento * 1 > 0 ? descripcion : '') + ",\"" + (data[x].nrodocumento * 1 > 0 ? data[x].id + "-" + data[x].nrodocumento : '') + "\")'><span style='font-size:18px; ' data-icon='&#xe22d;'></span></button >") +
            "</td > " +
            "</tr>";
    }
    $("#TablaDocumentos").html(resultado);
}

function DescargarPDF(empleado, id) {
    window.open(url_cliente + "DocumentosEmpleados/" + empleado + "/" + id + ".pdf?id=" + NumeroAleatorio());
}

function CargarCombos() {
    $("#EPS, #CEPS").html(CargarCombo(43, 1));
    $("#AFP, #CAFP").html(CargarCombo(44, 1));
    $("#CESANTIA, #CCESANTIA").html(CargarCombo(69, 1));
    $("#Cargo, #CCargo").html(CargarCombo(26, 1));
    $("#Banco").html(CargarCombo(24, 1));
    $("#Horario").html(CargarCombo(30, 1));
    $("#Pais").html(CargarCombo(18, 1));
    $("#Departamento").html(CargarCombo(11, 1));
    $("#Ciudad").html(CargarCombo(12, 1));
    $("#Area, #CArea").html(CargarCombo(54, 1));
    $("#ClaseRiesgo").html(CargarCombo(80, 0));
    $("#CentroCosto").html(CargarCombo(61, 1));
    $("#TipoDocumentoEmpleado").html(CargarCombo(73, 1));
}
 
CargarCombos();


tb = $('input');
$(tb).keypress(enter2tab);

tb = $('select');
$(tb).keypress(enter2tab);

function enter2tab(e) {
    if (e.keyCode == 13) {
        id = $(this).attr("id");

        cb = parseInt($(this).attr('tabindex'));

        if (id == "DocumentoEmp") {
            if ($.trim($(this).val()) != "") {

                BuscarEmpleados($(this).val());
                return false;
            }
        }

        if ($(':input[tabindex=\'' + (cb + 1) + '\']') != null) {
            $(':input[tabindex=\'' + (cb + 1) + '\']').focus();
            $(':input[tabindex=\'' + (cb + 1) + '\']').select();
            e.preventDefault();

            return false;
        }

        if ($(':select[tabindex=\'' + (cb + 1) + '\']') != null) {
            console.log("1");
            $(':select[tabindex=\'' + (cb + 1) + '\']').focus();
            $(':select[tabindex=\'' + (cb + 1) + '\']').select();
            e.preventDefault();

            return false;
        }
    }
}

function LimpiarTodo() {
    $("#TipoDocumento").val("CC").trigger("change");
    $("#Documento").val("");
    DocumentoEmp = "";
    LimpiarDatos();
}

function LimpiarDatos() {
    IdEmpleado = 0;
    DocumentoEmp = "";
    TipoDocumento = "";
    $("#Nombres").val("");
    $("#Cargo").val("").trigger("change");
    $("#Pais").val("1").trigger("change");
    $("#Ciudad").html("");
    $("#EstadoCivil").val("").trigger("change");
    $("#FechaNacimiento").val("").trigger("change");
    $("#Banco").val("").trigger("change");
    $("#EPS").val("").trigger("change");
    $("#AFP").val("").trigger("change");
    $("#CESANTIA").val("").trigger("change");
    $("#TipoCuenta").val("").trigger("change");
    $("#ClaseRiesgo").val("").trigger("change");
    $("#Telefonos").val("");
    $("#Celular").val("");
    $("#Email").val("");
    $("#IdEmpleado").val("0");
    $("#Direccion").val("");
    $("#Sueldo").val("");
    $("#Cuenta").val("");
    $("#FechaInicio").val("");
    $("#FechaFinal").val("");
    $("#Estado").val("ACTIVO").trigger("change");
    $("#CentroCosto").val("").trigger("change");
    $("#PeriodoPago").val("quincenal").trigger("change");
    $("#Huella").val("");
    $("#Horario").val("").trigger("change");

    $("#Foto_Empleado").attr("src", url_cliente + "imagenes/img_avatar.png?id=" + NumeroAleatorio());

    $("#Area").val("").trigger("change");
    $("#Genero").val("").trigger("change");
    $("#Barrio").val("");
    $("#TipoSangre").val("");
    $("#Camisa").val("");
    $("#Pantalon").val("");

    $("#TablaDocumentos").html("");
    $("#TipoDocumentoEmpleado").val("").trigger("change")

}

function CambioDepartamento(pais, combo) {
    $("#" + combo).html(CargarCombo(11, 1, "", pais));
}

function CambioCiudad(departamento, combo) {
    $("#" + combo).html(CargarCombo(12, 1, "", departamento));
}

function ValidarEmpleado(documento) {
    if ($.trim(DocumentoEmp) != "") {
        if (DocumentoEmp != documento) {
            DocumentoEmp = "";
            LimpiarDatos();
        }
    }
}

function BuscarEmpleado(documento) {

    if ($.trim(documento) == "")
        return false;

    if (DocumentoEmp == documento)
        return false;

    LimpiarDatos();
    DocumentoEmp = documento;
    var parametros = "documento=" + documento;
    var datos = LlamarAjax("Recursohumano","opcion=BuscarEmpleados&" + parametros);
    if (datos != "[]") {
        var data = JSON.parse(datos);
        IdEmpleado = data[0].id;
        $("#TipoDocumento").val(data[0].tipodocumento).trigger("change");
        $("#EPS").val(data[0].ideps).trigger("change");
        $("#AFP").val(data[0].idafp).trigger("change");
        $("#CESANTIA").val(data[0].idcesantia).trigger("change");
        $("#Area").val(data[0].idarea).trigger("change");
        $("#Cargo").val((data[0].idcargo > 0 ? data[0].idcargo : "")).trigger("change");
        $("#Banco").val((data[0].idbanco > 0 ? data[0].idbanco : "")).trigger("change");
        $("#Estado").val(data[0].estado).trigger("change");
        $("#Sueldo").val(formato_numero(data[0].sueldo, 0, _CD, _CM, ""));
        $("#ClaseRiesgo").val(data[0].idclaseriesgo).trigger("change");
        $("#Cuenta").val(data[0].cuenta);
        $("#TipoCuenta").val((data[0].tipocuenta ? data[0].tipocuenta : "")).trigger("change");
        $("#CentroCosto").val(data[0].idcentrocosto).trigger("change");
        $("#PeriodoPago").val(data[0].periodopago).trigger("change");
        $("#Nombres").val(data[0].nombrecompleto);
        $("#FechaInicio").val(data[0].fechainicio);
        $("#FechaFinal").val(data[0].fechafin);
        $("#Telefonos").val(data[0].telefono);
        $("#Celular").val(data[0].celular);
        $("#Email").val(data[0].email);
        $("#Huella").val(data[0].huella);
        $("#Horario").val((data[0].idhorario > 0 ? data[0].idhorario : "")).trigger("change");
        $("#IdEmpleado").val(data[0].id);
        $("#Direccion").val(data[0].direccion);
        $("#Pais").val(data[0].idpais).trigger("change");
        $("#Departamento").val(data[0].iddepartamento).trigger("change");
        $("#Ciudad").val(data[0].idciudad).trigger("change");
        $("#EstadoCivil").val(data[0].estadocivil).trigger("change");
        $("#FechaNacimiento").val(data[0].fechanacimiento);

        $("#Genero").val(data[0].genero).trigger("change");
        $("#Barrio").val(data[0].barrio);
        $("#TipoSangre").val(data[0].tiposangre);
        $("#Camisa").val(data[0].camisa);
        $("#Pantalon").val(data[0].pantalon);

        var Foto = $.trim(data[0].foto);
        if (Foto != "") {
            $("#Foto_Empleado").attr("src", url_cliente + "imagenes/FotoEmpleados/" + Foto + "?id=" + NumeroAleatorio());
        } else
            $("#Foto_Empleado").attr("src", url_cliente + "imagenes/img_avatar.png?id=" + NumeroAleatorio());

        TabDocumentoEmpleados();

    }
}

$("#formEmpleados").submit(function (e) {
    e.preventDefault();
    var banco = $("#Banco").val() * 1;
    var cuenta = $.trim($("#Cuenta").val());
    var tipocuenta = $.trim($("#TipoCuenta").val());
    if (banco > 0) {
        if (cuenta == "") {
            $("#Cuenta").focus();
            swal("Acción Cancelada", "Debe de ingresar el número de cuenta", "warning");
            return false;
        }
        if (tipocuenta == "") {
            $("#TipoCuenta").focus();
            swal("Acción Cancelada", "Debe de seleccionar el tipo de cuenta", "warning");
            return false;
        }
    }
    if (cuenta != "") {
        if (banco == 0) {
            $("#Banco").focus();
            swal("Acción Cancelada", "Debe de seleccionar el banco ", "warning");
            return false;
        }

        if (tipocuenta == "") {
            $("#TipoCuenta").focus();
            swal("Acción Cancelada", "Debe de seleccionar el tipo de cuenta", "warning");
            return false;
        }
    }

    if (tipocuenta != "") {
        if (banco == 0) {
            $("#Banco").focus();
            swal("Acción Cancelada", "Debe de seleccionar el banco ", "warning");
            return false;
        }

        if (cuenta == "") {
            $("#Cuenta").focus();
            swal("Acción Cancelada", "Debe de ingresar el número de cuenta", "warning");
            return false;
        }
    }

    var parametros = $("#formEmpleados").serialize().replace(/\%2C/g, "");
    var datos = LlamarAjax("Recursohumano","opcion=GuardarEmpleados&" + parametros).split("|");
    if (datos["0"] == "0") {
        if (IdEmpleado == 0) {
            IdEmpleado = datos[2] * 1;
            $("#IdEmpleado").val(IdEmpleado);
            swal("", "Empleado guardado con éxito", "success");
        } else
            swal("", "Empleado actualizado con éxito", "success");
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }

});

$("#TablaEmpleados > tbody").on("dblclick", "tr", function () {
    var row = $(this).parents("td").context.cells;
    var numero = row[4].innerText;
    $('#tab-Empleados a[href="#tabcrear"]').tab('show');
    $("#Documento").val(numero);
    DocumentoEmp = "";
    BuscarEmpleado(numero);
});


function ConsultarEmpleados() {

    var tipodocumento = $("#CTipoDocumento").val();
    var documento = $("#CDocumento").val() * 1;
    var nombres = $.trim($("#CNombre").val());
    var cargo = $("#CCargo").val() * 1;
    var area = $("#CArea").val();
    var eps = $("#CEPS").val();
    var afp = $("#CAFP").val();
    var cesantia = $("#CCESANTIA").val();
    var estado = $.trim($("#CEstado").val());


    ActivarLoad();
    setTimeout(function () {
        var parametros = "documento=" + documento + "&nombre=" + nombres + "&tipodocumento=" + tipodocumento + "&estado=" + estado + "&cargo=" + cargo +
            "&area=" + area + "&eps=" + eps + "&afp=" + afp + "&cesantia=" + cesantia;
        var datos = LlamarAjax("Recursohumano","opcion=ConsultarEmpleados&" + parametros);
        DesactivarLoad();

        var datajson = JSON.parse(datos);
        console.log(datos);
        $('#TablaEmpleados').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "foto" },
                { "data": "id" },
                { "data": "huella" },
                { "data": "tipodocumento" },
                { "data": "documento" },
                { "data": "nombrecompleto" },
                { "data": "genero" },
                { "data": "fechanacimiento" },
                { "data": "lugarnacimiento" },
                { "data": "cargo" },
                { "data": "area" },
                { "data": "sueldo", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "banco" },
                { "data": "tipocuenta" },
                { "data": "cuenta" },
                { "data": "eps" },
                { "data": "afp" },
                { "data": "cesantia" },
                { "data": "fechainicio" },
                { "data": "fechafin" },
                { "data": "horario" },
                { "data": "direccion" },
                { "data": "barrio" },
                { "data": "telefono" },
                { "data": "celular" },
                { "data": "camisa" },
                { "data": "pantalon" },
                { "data": "tiposangre" },
                { "data": "email" },
                { "data": "estado" }
            ],

            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy','colvis'
            ]
        });
    }, 15);
}

function CambioDocumento() {
    if (IdEmpleado == 0) {
        swal("Acción Cancelada", "Debe seleccionar el Empleados", "warning");
        return false;
    }
    $("#SCambioNit").html(DocumentoCli);
    $("#modalCambioNitCli").modal("show");

}

function AgregarCargo() {
    swal({
        title: 'Agregar Opción',
        text: 'CARGO',
        input: 'text',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Agregar'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',

        inputValidator: function (value) {
            return new Promise(function (resolve, reject) {
                if ($.trim(value)) {
                    var datos = LlamarAjax("Cotizacion","opcion=GuardarOpcion&tipo=9&magnitud=0&marca=0&descripcion=" + value + "&tipoconcepto=0");
                    datos = datos.split("|");
                    if (datos[0] != "XX") {
                        $("#Cargo").html(datos[1]);
                        $("#Cargo").val(datos[0]).trigger("change");
                        resolve();
                    } else {
                        reject(datos[1]);
                    }

                } else {
                    reject(ValidarTraduccion('Debe de ingresar la descripción del CARGO'));
                }
            })
        }
    }).then(function (result) {
        $("#Cargo").focus();
    })
}

function MaximizarFoto(foto) {
    window.open(url_cliente + "imagenes/FotoEmpleados/" + foto + "?id=" + NumeroAleatorio());
} 

function CambioDocumento() {
    if (IdEmpleado == 0) {
        swal("Acción Cancelada", "Debe seleccionar un empleado", "warning");
        return false;
    }
    $("#SCambioDocumento").html($("#Nombres").val());
    $("#DDocumento").val(DocumentoEmp);
    $("#DTipoDocumento").val($("#TipoDocumento").val()).trigger("change");
    $("#modalCambioDocumento").modal("show");

}

function CambiarDocumento() {
    var documento = $.trim($("#DDocumento").val());
    var tipo = $("#DTipoDocumento").val();
    if (documento == "") {
        $("#DDocumento").focus();
        swal("Acción Cancelada", "Debe de ingresar el nuevo NIT/Cédula", "warning");
        return false;
    }

    var datos = LlamarAjax("Recursohumano","opcion=CambioDocumento&documento=" + documento + "&id=" + IdEmpleado + "&tipo=" + tipo).split("|");
    if (datos[0] == "0") {
        $("#modalCambioDocumento").modal("hide");
        $("#Documento").val(documento);
        DocumentoEmp = "";
        BuscarEmpleado(documento);
        swal("", datos[1], "success");
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
}


$('select').select2();
DesactivarLoad();


