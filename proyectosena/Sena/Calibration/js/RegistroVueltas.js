﻿
d = new Date();
month = d.getMonth() + 1;
day = d.getDate();

var Anio = $.trim(d.getFullYear());
Anio = $.trim(Anio.substr(2, 2))


output = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-01';

output2 = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-' +
    (day < 10 ? '0' : '') + day;

var fechaeje = LlamarAjax("Configuracion/FechaEjeVuelta", "");
$("#Fecha").val(fechaeje);


var AutoContacto = [];
var AutoDireccion = [];
var arrayOpciones = new Array(); //Es el array global
var CorreoEmpresa = "";
var NombreCliente = "";
var TipoCliente = "";
var PlazoPago = "";
var ComboClientes = CargarCombo(9, 1);
var ComboProveedores = CargarCombo(7, 1);

function GenerarPDF() {
    var fecha = $("#FechaPro").val();
    if (fecha == "") {
        $("#FechaPro").focus();
        swal("Acción Cancelada", "Debe seleccionar la fecha de programación", "warning");
        return false;
    }
    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: ValidarTraduccion('¿Seguro que desea GENERAR EL PDF DE LAS DILIGENCIAS de fecha ' + fecha + '?'),
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Si'),
        cancelButtonText: ValidarTraduccion('No'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                $.post("Configuracion/RpVueltas", "fecha=" + fecha)
                    .done(function (data) {
                        datos = data.split("|");
                        if (datos[0] == "0") {
                            window.open(url + "DocumPDF/" + datos[1]);
                            resolve();
                        } else {
                            reject(datos[1]);
                        }
                    })
            })
        }
    }]);
}

function BuscarDatos(cliente) {

    
    
    var tipo = $("#Tipo").val();
    $("#Direccion").val("");
    $("#Contacto").val("");
    $("#Ingresos").html("");
    $("#EmailCartera").val("");

    if (cliente * 1 == 0)
        return false;

    TipoCliente = "";
    PlazoPago = "";
    CorreoEmpresa = "";
    NombreCliente = "";

    var tipovuelta = $("#TipoVuelta").val();
    console.log("datos_clientes " + cliente);

        
    SaldoActual(cliente, 1);
    $("#SaldoClienteDev").html(SaldoTotal(cliente));

    AutoContacto.splice(0, AutoContacto.length);
    AutoDireccion.splice(0, AutoDireccion.length);
    var datos = LlamarAjax("Configuracion/AutoComplete", "cliente=" + cliente + "&tipo=" + tipo).split("|");
    if (tipo == "PROVEEDOR") {
        $("#Direccion").val(datos[1])
        $("#Contacto").val(datos[0]);
        $("#Ingresos").html("");
    } else {
        var data = JSON.parse(datos[0]);
        for (var x = 0; x < data.length; x++) {
            AutoContacto.push(data[x].nombres);
        }
        var data = JSON.parse(datos[1]);
        for (var x = 0; x < data.length; x++) {
            AutoDireccion.push(data[x].direccion);
        }
        if (tipovuelta == "ENTREGAR EQUIPO")
            $("#Ingresos").html(datos[2]);
        var data = JSON.parse(datos[3]);
        $("#EmailCartera").val(data[0].emailcartera);
        CorreoEmpresa = data[0].emailcartera;
        PlazoPago = data[0].plazopago;
        TipoCliente = data[0].tipo;
        NombreCliente = data[0].cliente;
    }
}

$("#Direccion").autocomplete({ source: AutoDireccion, minLength: 0 }).focus(function () {
    $(this).data("uiAutocomplete").search($(this).val());
});
$("#Contacto").autocomplete({ source: AutoContacto, minLength: 0 }).focus(function () {
    $(this).data("uiAutocomplete").search($(this).val());
});

$("#FechaInicio").val(output);
$("#FechaFin").val(output2);
$("#Usuario").html(CargarCombo(13, 1));
$("#UsuarioAsig").html($("#Usuario").html());


function CambioCliente(tipo) {
    AutoContacto.splice(0, AutoContacto.length);
    AutoDireccion.splice(0, AutoDireccion.length);
    $("#Direccion").val("");
    $("#Contacto").val("");
    $("#Ingresos").html("");
    if (tipo == "CLIENTE")
        $("#Cliente").html(ComboClientes);
    else
        $("#Cliente").html(ComboProveedores);
    console.log("cambio_cliente");
}

CambioCliente("CLIENTE");


function GuardarVueltas() {

    var PermisioEspecial = PerGeneSistema("VUELTAS PROGRAMAR CON DEUDA");

    var actividad = $.trim($("#Actividad").val());
    var contacto = $.trim($("#Contacto").val()).toUpperCase();
    var direccion = $.trim($("#Direccion").val());
    var fecha = $.trim($("#Fecha").val());
    var id = $("#Id").val() * 1;
    var cliente = $("#Cliente").val() * 1;
    var tipo = $("#Tipo").val();
    var tipovuelta = $("#TipoVuelta").val();

    var ingresos = $("#Ingresos").val(); 

    if (!ingresos)
        ingresos = "";
                    
    var mensaje = "";

    if (tipovuelta == "ENTREGAR EQUIPO" && !(ingresos)) {
        $("#Ingresos").focus();
        mensaje = "<br> Debe seleccionar los ingresos que va entregar" + mensaje;
    }

    if (fecha == "") {
        $("#Fecha").focus();
        mensaje = "<br> Debe ingresar la fecha de programación" + mensaje;
    } else {
        if (fecha < output2) {
            $("#Fecha").focus();
            mensaje = "<br> La fecha debe ser mayor o igual a la fecha actual " + output2 + mensaje;
        }
    }

    if (actividad == "") {
        $("#Actividad").focus();
        mensaje = "<br> Debe de ingresar la actividad de la vuelta" + mensaje;
    }

    if (direccion == "") {
        $("#Direccion").focus();
        mensaje = "<br> Debe de ingresar la dirección de la vuelta" + mensaje;
    }

    if (contacto == "") {
        $("#Contacto").focus();
        mensaje = "<br> Debe de ingresar el contacto del cliente" + mensaje;
    }

    if (cliente == 0) {
        $("#Cliente").focus();
        mensaje = "<br> Debe de ingresar el cliente de la vuelta" + mensaje;
    }

    if (tipovuelta == "") {
        $("#TipoVuelta").focus();
        mensaje = "<br> Debe de ingresar el tipo de vuelta" + mensaje;
    }

    if (mensaje != "") {
        swal("Acción Cancelada", mensaje,"warning")
        return false;
    }
    if (tipovuelta == "ENTREGAR EQUIPO") {
        if (SaldoActual(cliente, 0) > 0) {
            if (PermisioEspecial > 0)
                actividad = actividad + " <b>Entregar equipos sin certificado por dedudas pendientes hasta validar el pago</b>";
            else {
                swal("Acción Cancelada", "No se puede guardar la vuelta porque el cliente posee saldos vencidos", "warning");
                return false
            }
        }
    }
    

    var validaringresos = ValidarIngreso();
        
    var parametros = "operacion=1&id=" + id + "&actividad=" + actividad + "&ingresos=" + ingresos + "&fecha=" + fecha + "&cliente=" + cliente + "&direccion=" + direccion + "&contacto=" + contacto + "&tipo=" + tipo + "&tipovuelta=" + tipovuelta;
    var datos = LlamarAjax("Configuracion/OperacionVuelta", parametros).split("|");
    if (datos[0] == "0") {
        ConsultarVueltas();
        LimpiarVueltas();
        var mensaje = "";
        
        if (validaringresos != "XX") {
            mensaje = "<br><b>Ingresos que no están en el área de logística:<br>" + validaringresos + "<br>LLAMAR AL ASESOR TECNICO</b>";
        }
        swal("", datos[1] + mensaje, "success");
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
}

ConsultarVueltas();

function TipoVuelta() {
    $("#Cliente").val("").trigger("change");
}

function ConsultarVueltas() {

    var fecini = $("#FechaInicio").val();
    var fecfin = $("#FechaFin").val();
    var usuario = $("#Usuario").val()*1;
    var estado = $("#Estado").val();
    var fechapro = $("#FechaPro").val();


    if (fecini == "" || fecfin == "") {
        $("#FechaInicio").focus();
        swal("Acción Cancelada", "Debeb de seleccionar fecha inicio y fecha final", "warning");
        return false;
    }

    ActivarLoad();
    setTimeout(function () {
        var parametros = "fechaini=" + fecini + "&fechafin=" + fecfin + "&usuario=" + usuario + "&estado=" + estado + "&fechapro=" + fechapro;
        var datos = LlamarAjax("Configuracion/TablasVuelta", parametros);
        DesactivarLoad();
        var datajson = JSON.parse(datos);
        $('#TablaVueltas').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "imagen" },
                { "data": "opcion" },
                { "data": "id" },
                { "data": "fecha" },
                { "data": "usuario" },
                { "data": "tipo" },
                { "data": "idcliente" },
                { "data": "cliente" },
                { "data": "contacto" },
                { "data": "direccion" },
                { "data": "tipovuelta" },
                { "data": "actividad" },
                { "data": "ingresos" },
                { "data": "fechapro" },
                { "data": "estado" },
                { "data": "tiempo" },
                { "data": "aprobado" },
                { "data": "ejecutado" },
                { "data": "asignado" },
                { "data": "observacion" }
            ],
                       
            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy','colvis'
            ]
        });
    }, 15);
}


function opcionSelected() {
    var opciones = document.getElementById("Ingresos").options;
    for (var i = 0; i < opciones.length; i++) {
        var opcion = opciones[i].value;
        if (opciones[i].selected) {
            if (!enArray(opcion)) {  //enArray() busca si opcion está en arrayOpciones                              
                arrayOpciones[arrayOpciones.length] = opcion;
                alert("Has señalado la opción" + opcion);
            }
        }
    }
}

function ValidarIngreso() {
    var ingreso = $('#Ingresos').val(); 
    var datos = LlamarAjax("Configuracion/ValidarIngreso", "ingreso=" + ingreso);
    return datos;
}

$("#Ingresos").on("multiselectclick", function () {
    var latest_value = $('option', this).filter(':selected:last').val();
    alert(latest_value);
}); 

function AsignarVuelta(id, cliente) {
    var datos = LlamarAjax("Configuracion/DescripcionVuelta", "id=" + id).split("||");
    var vuelta = datos[0];
    $("#IdAEjecutar").val(id);
    $("#AVuelta").val(vuelta);
    $("#ACliente").val(cliente);
    $("#UsuarioAsig").val("").trigger("change");
    
    $("#modalAsignar").modal("show");
}  

function GuaAsigVuelta() {
    var id = $("#IdAEjecutar").val() * 1;
    var asignacion = $("#UsuarioAsig").val()*1;

    if (asignacion == 0) {
        $("#UsuarioAsig").focus();
        swal("Acción Cancelada", "Debe seleccionar el usuario que se le asigne la vuelta", "warning");
        return false;
    }
    var datos = LlamarAjax("Configuracion/OperacionVuelta", "id=" + id + "&operacion=5&fecha=1900-01-01&asignacion=" + asignacion).split("|");
    if (datos[0] == "0") {
        ConsultarVueltas();
        $("#modalAsignar").modal("hide");
        swal("", datos[1], "success");
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
}

function EjecutarVuelta(id, cliente) {
    var datos = LlamarAjax("Configuracion/DescripcionVuelta", "id=" + id).split("||");
    var vuelta = datos[0];
    var observacion = datos[1];

    $("#IdEjecutar").val(id);
    $("#EjeVuelta").val("").trigger("change");
    $("#DVuelta").val(vuelta);
    $("#DCliente").val(cliente);
    $("#OVuelta").val(observacion);

    $("#modalEjecutar").modal("show");

}

function GuaEjecVuelta() {
    var id = $("#IdEjecutar").val()*1;
    var ejecutada = $("#EjeVuelta").val();
    var observacion = $.trim($("#OVuelta").val());

    if (ejecutada == "") {
        $("#EjeVuelta").focus();
        swal("Acción Cancelada", "Debe seleccionar si la vuelta fue ejecutada", "warning");
        return false;
    } else {
        if (ejecutada == "NO" && observacion == "") {
            $("#OVuelta").focus();
            swal("Acción Cancelada", "Si la vuelta no fue ejecutada debe de ingresar una observación", "warning");
            return false;
        }
        if (ejecutada == "RECHAZADA" && observacion == "") {
            $("#OVuelta").focus();
            swal("Acción Cancelada", "Si la vuelta se rechaza debe de ingresar una observación", "warning");
            return false;
        }
    }

    var datos = LlamarAjax("Configuracion/OperacionVuelta", "id=" + id + "&operacion=" + 4 + "&fecha=" + fechaeje + "&ejecutado=" + ejecutada + "&observacion=" + observacion).split("|");
    if (datos[0] == "0") {
        ConsultarVueltas();
        $("#modalEjecutar").modal("hide");
        swal("", datos[1], "success");
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
}





function AprobarVuelta(id) {

    var datos = LlamarAjax("Configuracion/DescripcionVuelta", "id=" + id).split("||");
    var descripcion = datos[0];
         
    swal({
        title: 'Advertencia',
        text: ValidarTraduccion('¿Seguro que desea APROBAR la vuelta ' + descripcion + '?'),
        input: 'text',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Si'),
        cancelButtonText: ValidarTraduccion('No'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',

        inputValidator: function (value) {
            return new Promise(function (resolve, reject) {
                if (!value)
                    value = "";
                var datos = LlamarAjax("Configuracion/OperacionVuelta", "id=" + id + "&operacion=" + 2 + "&fecha=" + fechaeje + "&observacion=" + value)
                datos = datos.split("|");
                if (datos[0] == "0") {
                    resultado = datos[1];
                    resolve()
                } else {
                    reject(datos[1]);
                }
            })
        }
    }).then(function (result) {
        ConsultarVueltas();
        swal({
            type: 'success',
            html: resultado
        })
    });

}

function RechazarVuelta(id, descripcion) {

    var datos = LlamarAjax("Configuracion/DescripcionVuelta", "id=" + id).split("||");
    var descripcion = datos[0];

    swal({
        title: 'Advertencia',
        text: ValidarTraduccion('¿Seguro que desea RECHAZAR la vuelta ' + descripcion + '?'),
        input: 'text',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Si'),
        cancelButtonText: ValidarTraduccion('No'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',

        inputValidator: function (value) {
            return new Promise(function (resolve, reject) {
                if (value) {
                    var datos = LlamarAjax("Configuracion/OperacionVuelta", "id=" + id + "&operacion=" + 3 + "&fecha=" + fechaeje + "&observacion=" + value)
                    datos = datos.split("|");
                    if (datos[0] == "0") {
                        resultado = datos[1];
                        resolve()
                    } else {
                        reject(datos[1]);
                    }

                } else {
                    reject(ValidarTraduccion('Debe de ingresar un observación'));
                }
            })
        }
    }).then(function (result) {
        ConsultarVueltas();
        swal({
            type: 'success',
            html: resultado
        })
    });

}

$("#TablaVueltas > tbody").on("dblclick", "tr", function () {
    var row = $(this).parents("td").context.cells;

    ActivarLoad();
    setTimeout(function () {
        if (row[14].innerText != "POR APROBAR") {
            DesactivarLoad();
            swal("Acción Cancelada", "No se puede editar una vuelta con estado " + row[14].innerText, "warning");
            EscucharMensaje("No se puede editar una vuelta con estado " + row[14].innerText);
            return false;
        }


        $("#Id").val(row[2].innerText);
        $("#Tipo").val(row[5].innerText).trigger("change");
        $("#Fecha").val($.trim(row[13].innerText).replace("/", "-").replace("/", "-"));
        $("#TipoVuelta").val(row[10].innerText).trigger("change");
        $("#Cliente").val(row[6].innerText).trigger("change");
        $("#Actividad").val(row[11].innerText);
        $("#Contacto").val(row[8].innerText);
        $("#Direccion").val(row[9].innerText);

        var ingresos = $.trim(row[12].innerText).split(",");
        for (var x = 0; x < ingresos.length; x++) {
            $("#Ingresos option[value=" + ingresos[x] + "]").prop("selected", true);
        }
        $("#Ingresos").trigger("change");
        DesactivarLoad();
        $("#Actividad").focus();
    }, 15);
});

function LimpiarVueltas() {
    $("#Id").val("0");
    $("#Fecha").val(fechaeje);
    $("#Actividad").val("");
    $("#Contacto").val("");
    $("#Direccion").val("");
    $("#Tipo").val("CLIENTE").trigger("change");
    $("#TipoVuelta").val("").trigger("change");
    $("#Ingresos").html("");
    $("#SaldoClienteDev").html("");
    $("#EmailCartera").val("");
    CorreoEmpresa = "";
    TipoCliente = "";
    PlazoPago = "";
    NombreCliente = "";
}

function VerEstadoCuenta() {
    EstadoCuenta($("#Cliente").val(), NombreCliente, CorreoEmpresa, TipoCliente, PlazoPago, CorreoEmpresa);
}

function LlamarFotoDet(ingreso, fotos, cliente) {
    if (fotos == 0)
        return false
    CargarModalFoto(ingreso, fotos, 5, cliente);
}

$('select').select2();

