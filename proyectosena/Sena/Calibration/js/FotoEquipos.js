﻿var Ingreso = 0;
var IdRemision = 0;
var Fotos = 0;
var Error = 0;
var url = $("#urlprincipal").val();

function LimpiarTodo() {
    LimpiarDatos();
    $("#Ingreso").val("");
}

function CambiarFoto2(numero) {
    $("#FotoIngreso").attr("src", url +  "imagenes/ingresos/" + Ingreso + "/" + numero + ".jpg");
}

function CargarFotos() {
    var contenedor = "";
    contenedor = "<div class='row'><div class='col-xs-12 col-sm-11'>" +
        "<img id='FotoIngreso' " + (Fotos > 0 ? "src='" + url + "imagenes/ingresos/" + Ingreso + "/1.jpg'" : "") + " width='90%'></div><div class='col-xs-12 col-sm-1'>";
    for (var x = 1; x <= Fotos; x++) {
        contenedor += "<button class='btn btn-primary' type='button' onclick='CambiarFoto2(" + x + ")'>" + x + "</button>&nbsp;";
    }
    contenedor += "</div></div>";
    $("#AlbunFotosIng").html(contenedor);
}

function LimpiarDatos() {
    IdRemision = 0;
    Ingreso = 0;
    Fotos = 0;
    $("#Equipo").val("");
    $("#Marca").val("");
    $("#Modelo").val("");
    $("#Serie").val("");
    $("#Observacion").val("");
    $("#AlbunFotosIng").html("");
    $("#Accesorio").val("");
}

function BuscarIngreso(numero, code, tipo) {
    if ((Ingreso != (numero*1)) && ($("#Equipo").val() != ""))
        LimpiarDatos();
    if ((code.keyCode == 13 || tipo == 1) && Error == 0) {
        if (numero * 1 > 0) {
            var datos = LlamarAjax( "Cotizacion/BuscarIngreso?ingreso=" + numero);
            if (datos != "[]") {
                var data = JSON.parse(datos);
                IdRemision = data[0].id * 1;
                Ingreso = data[0].ingreso * 1;
                Fotos = data[0].fotos * 1;
                $("#Equipo").val(data[0].equipo);
                $("#Marca").val(data[0].marca);
                $("#Modelo").val(data[0].modelo);
                $("#Serie").val(data[0].serie);
                $("#Observacion").val(data[0].observacion);
                $("#Accesorio").val(data[0].accesorio);

                CargarFotos();

            } else {
                $("#Ingreso").select();
                $("#Ingreso").focus();
                Error = 1;
                swal("Acción Cancelada", "El Ingreso número " + numero + " no se encuentra registrado", "warning");
                return false;
            }
        }
    } else
        Error = 0;
}
    
function GuardarDocumento() {
    var documento = document.getElementById("documento");
    if ($.trim(documento.value) != "") {

        var file = documento.files[0];
        var data = new FormData();
        data.append('documento', file);
        var resultado = '';
        var direccion =  url + "Cotizacion/AdjuntarFoto";
        $.ajax({
            url: direccion,
            type: 'POST',
            contentType: false,
            data: data,
            processData: false,
            async: false,
            success: function (datos) {
                datos = datos.split("|");
                if (datos[0] != "0") {
                    swal(ValidarTraduccion("Error al cargar el archivo"), "", "error");
                } else {
                    if (datos[2] != "XX") {
                        var datos = LlamarAjax("Cotizacion/SubirFoto?ingreso=" + Ingreso + "&remision=" + IdRemision);
                        if (datos == "0") {
                            Fotos = Fotos + 1;
                            CargarFotos();
                        } else {
                            swal(data);
                        }
                    }
                }
            }
        });
    }
}

function SubirFoto() {
    if (Ingreso > 0) {

        swal.queue([{
            html: '<h3>' + ValidarTraduccion("Seleccione la foto que desea subir del ingreso número " + Ingreso) + '</h3><input type="file" class="form-control" id="documento" onchange="GuardarDocumento()" name="documento" accept="image/*" required />',
            showLoaderOnConfirm: true,
            showCancelButton: true,
            confirmButtonText: ValidarTraduccion('OK'),
            cancelButtonText: ValidarTraduccion('No'),
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            preConfirm: function () {
                return new Promise(function (resolve, reject) {
                    resolve();
                })
            }
        }]);
    }
}

Webcam.set({
    width: 290,
    height: 320,
    dest_width: 620,
    dest_height: 750,
    image_format: 'png',
    jpeg_quality: 100
});
Webcam.attach('#my_cameraing');

function TomarFoto() {

    if (Ingreso > 0) {
        Webcam.snap(function (data_uri) {
            //var raw_image_data = data_uri.replace(/^data\:image\/\w+\;base64\,/, '');
            GuardarFoto(Ingreso, data_uri);
        });
    }

}

function EliminarFoto() {
    if (Ingreso > 0) {
        ingreso = $("#FotoIngreso").html() * 1;
        datos = LlamarAjax("Cotizacion/EliminarFoto", "ingreso=" + ingreso).split("|");
        if (datos[1] != "X") {
            Fotos = datos[1] * 1;
            CargarFotos(Devolucion, datos[1]);
            CambiarFoto2(datos[1], Devolucion);
        }
    }
}

function GuardarFoto(ingreso, archivo) {


    $.ajax({
        type: 'POST',
        url: url + "Cotizacion/GuardarFoto",
        data: "archivo=" + archivo + "&ingreso=" + ingreso,
        success: function (data) {
            Fotos = data * 1;
            CargarFotos(ingreso, data);
            CambiarFoto2(data, ingreso);
        }
    });
}



$('select').select2();
DesactivarLoad();
