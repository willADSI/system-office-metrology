﻿var Ingreso = 0;
var IdRecibido = 0;
var Foto = 0;
var Sitio = "";
var ErrorEnter = 0;
var idusuario = localStorage.getItem('idusuario');

d = new Date();
month = d.getMonth() + 1;
day = d.getDate();

var Anio = $.trim(d.getFullYear());
Anio = $.trim(Anio.substr(2, 2))


output = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-' +
    (day < 10 ? '0' : '') + day;

$("#FechaInicio").val(output);
$("#FechaFin").val(output);

$("#Especialista").html(CargarCombo(13, 0));
$("#Especialista").val(idusuario).trigger("change");

$("#Asesor").html(CargarCombo(13, 1,"","1"));
localStorage.setItem("Ingreso", "0");

function LimpiarTodo() {
    LimpiarDatos();
    $("#Ingreso").val("");
}

function LimpiarDatos() {
    Ingreso = 0;
    IdRecibido = 0;
    Foto = 0;
    localStorage.setItem("Ingreso", "0");
    $("#btnguardar").removeClass("hidden");
    $("#Equipo").val("");
    $("#Marca").val("");
    $("#Modelo").val("");
    $("#Serie").val("");
    $("#Magnitud").val("");
    $("#Observacion").val("");
    $("#Observaciones").val("");
    $("#Remision").val("");
    $("#Rango").val("");
    $("#Asesor").val("").trigger("change");
    $("#registrorecibirlab").html("");
    $("#TBPlantillas").html("");

    $("#Sitio").val("");
    $("#Garantia").val("");
    $("#Accesorio").val("");

    $("#Usuario").val("");
    $("#FechaIng").val("");
    $("#Tiempo").val("");
    
}


function ConsultarRecibidoLab() {

    var fecini = $("#FechaInicio").val();
    var fecfin = $("#FechaFin").val();
    var especialista = $("#Especialista").val() * 1;

    if (fecini == "" || fecfin == "") {
        $("#FechaInicio").focus();
        swal("Acción Cancelada", "Debeb de seleccionar fecha inicio y fecha final", "warning");
        return false;
    }
                
    ActivarLoad();
    setTimeout(function () {
        var parametros = "fecini=" + fecini + "&fecfin=" + fecfin + "&especialista=" + especialista;
        var datos = LlamarAjax("Laboratorio","opcion=TablaRecibidoLab&" + parametros);
        DesactivarLoad();
        var datajson = JSON.parse(datos);
        $('#TablaReportar').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "fila" },
                { "data": "ingreso" },
                { "data": "equipo" },
                { "data": "observaciones" },
                { "data": "usuario" },
                { "data": "fecha" }
            ],

            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy','colvis'
            ]
        });
    }, 15);
}

ConsultarRecibidoLab();


function BuscarIngreso(numero, code, tipo) {
    if ((Ingreso != (numero*1)) && ($("#Equipo").val() != ""))
        LimpiarDatos();
    if ((code.keyCode == 13 || tipo == "0") && ErrorEnter == 0) {
        if (numero * 1 > 0) {
            var datos = LlamarAjax("Laboratorio","opcion=BuscarIngresoRecbLab&ingreso=" + numero).split("||");
            if (datos[0] != "[]") {
                var data = JSON.parse(datos[0]);
                IdRemision = data[0].idremision * 1;
                Ingreso = data[0].ingreso * 1;
                Foto = data[0].fotos * 1;
                Sitio = data[0].sitio;
                if (Foto == 0 && Sitio == "NO") {
                    ErrorEnter = 1;
                    LimpiarTodo();
                    swal("Acción Cancelada", "No se puede recibir un ingreso sin fotos", "warning");
                    return false;
                }

                if (IdRemision == 0) {
                    ErrorEnter = 1;
                    LimpiarTodo();
                    swal("Acción Cancelada", "Este ingreso no posee una remision guardada", "warning");
                    return false;
                }
                
                localStorage.setItem("Ingreso", Ingreso);
                $("#Equipo").val(data[0].equipo);
                $("#Marca").val(data[0].marca);
                $("#Modelo").val(data[0].modelo);
                $("#Serie").val(data[0].serie);
                $("#Rango").val(data[0].intervalo);
                $("#Remision").val(data[0].remision);
                $("#Magnitud").val(data[0].magnitud);
                $("#Observacion").val(data[0].observacion);

                $("#Sitio").val(data[0].sitio);
                $("#Garantia").val(data[0].garantia);
                $("#Accesorio").val(data[0].accesorio);

                $("#Usuario").val(data[0].usuarioing);
                $("#FechaIng").val(data[0].fechaing);
                $("#Tiempo").val(data[0].tiempo);

                if (data[0].id) {

                    swal("Acción Cancelada", "Este ingreso ya fue recibido", "warning");
                    ErrorEnter = 1;
                    IdRecibido = data[0].id;
                    Certificado = data[0].numero;
                    $("#Asesor").val(data[0].idusuarioent).trigger("change");
                    $("#Observaciones").val(data[0].observaciones);
                    $("#registrorecibirlab").html("<b>Recibido el día " + data[0].fecha + " por el técnico " + data[0].usuario + "</b>");
                    $("#btnguardar").addClass("hidden");

                } else {
                    $("#Asesor").focus();
                    if (datos[1] == "[]") {
                        ErrorEnter = 1;
                        LimpiarTodo();
                        swal("Acción Cancelada", "Debe de configurar las plantillas del equipo... Consulte con su director técnico", "warning");
                        return false;
                    }
                    if (Foto == 0)
                        swal("Acción cancelada", "El ingreso no posee foto", "warning");
                }

                BuscarPlantillas(1, datos[1], IdRecibido);
                
            } else {
                $("#Ingreso").select();
                $("#Ingreso").focus();
                ErrorEnter = 1;
                swal("Acción Cancelada", "El Ingreso número " + numero + " no se encuentra registrado", "warning");
                return false;
            }
        }
    } else
        ErrorEnter = 0;
}

function BuscarPlantillas(opcion, datos, guardado) {
    if (opcion == 2)
        datos = LlamarAjax("Laboratorio","opcion=Plantillas_Ingreso&ingreso=" + Ingreso);
    var data = JSON.parse(datos);

    var resultado = "";
    var activo = "";
    for (var x = 0; x < data.length; x++) {
        activo = (data[x].seleccionado * 1 == 0 ? "" : "checked")
        if (guardado > 0)
            activo += " disabled ";
        resultado += "<tr>" +
            "<td>" + data[x].descripcion + "</td>" +
            "<td><input type='checkbox' class='form-control' " + activo + " name = 'NOpciones[]'> <input type='hidden' name='NItems[]' value='" + data[x].id + "'></td></tr >";
    }
    $("#TBPlantillas").html(resultado);
}


function Guardar() {

    if (Ingreso == 0)
        return false;

    var asesor = $("#Asesor").val()*1;
    var observacion = $.trim($("#Observaciones").val());
    var fecha = $("#FechaIng").val();

    var items = document.getElementsByName("NItems[]");
    var opciones = document.getElementsByName("NOpciones[]");
    var opcion = "";

    var a_items = "";
    var seleccionado = 0;


    for (var x = 0; x < opciones.length; x++) {
        if (opciones[x].checked) {
            seleccionado = 1;
            if (a_items != "") {
                a_items += "|";
            }
            a_items += items[x].value;
        }
    }
        
    var mensaje = "";

    if (seleccionado == 0) {
        mensaje = "<br> Debe de seleccionar por lo menos una plantilla de calibración " + mensaje;
    }

    if (observacion == "") {
        $("#Observaciones").focus()
        mensaje = "<br> Debe de ingresar la observación del recibido " + mensaje;
    }
    if (asesor == 0) {
        $("#Asesor").focus();
        mensaje = "<br> Debe de seleccionar el asesor que entrega el ingreso " + mensaje;
    }
    
    if (mensaje != "") {
        swal("Acción Cancelada", mensaje, "warning");
        return false;
    }

    var parametros = "ingreso=" + Ingreso + "&usuario=" + asesor + "&observacion=" + observacion + "&fecha=" + fecha + "&a_items=" + a_items;
    var datos = LlamarAjax("Laboratorio","opcion=RecibirLab&" + parametros).split("|");
    if (datos[0] == "0") {
        LimpiarTodo();
        ConsultarRecibidoLab();
        $("#Ingreso").focus();
        swal("", datos[1], "success");
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
}

function VerFotos() {
    if (Ingreso == 0)
        return false;
    CargarModalFoto(Ingreso, Foto,1);
}

function EliminarRecIngresoLab() {
    if (Ingreso == 0)
        return false;
    var mensaje = "";

    swal({
        title: 'Advertencia',
        text: '¿Desea eliminar el recibido del ingreso número ' + Ingreso + '?',
        input: 'text',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Eliminar'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',

        inputValidator: function (value) {
            return new Promise(function (resolve, reject) {
                if (value) {
                    var datos = LlamarAjax("Laboratorio","opcion=EliminarRecibido&opciones=Recibir Laboratorio&observacion=" + value + "&ingreso=" + Ingreso)
                    datos = datos.split("|");
                    if (datos[0] == "0") {
                        mensaje = datos[1];
                        ConsultarRecibidoLab();
                        resolve()
                    } else {
                        reject(datos[1]);
                    }

                } else {
                    reject(ValidarTraduccion('Debe de ingresar un observación'));
                }
            })
        }
    }).then(function (result) {
        LimpiarTodo();
        $("#Ingreso").focus();
        swal({
            type: 'success',
            html: mensaje
        })
    });
}

function ModalEditarPlantillas() {
    if (Ingreso == 0 || IdRecibido == 0)
        return false;

    var datos = LlamarAjax("Laboratorio","opcion=Plantillas_Ingreso&ingreso=" + Ingreso);
    var data = JSON.parse(datos);
    var resultado = "";
    var activo = "";
    for (var x = 0; x < data.length; x++) {
        activo = (data[x].seleccionado * 1 == 0 ? "" : "checked ")
        resultado += "<tr>" +
            "<td>" + data[x].descripcion + "</td>" +
            "<td><input type='checkbox' class='form-control' " + activo + " name = 'EOpciones[]' required > <input type='hidden' name='EItems[]' value='" + data[x].id + "'></td></tr >";
    }
    $("#TBEditarPlantillas").html(resultado);

    $("#ModalPlantillas").modal("show");
}

function EditarPlantillas() {

    var items = document.getElementsByName("EItems[]");
    var opciones = document.getElementsByName("EOpciones[]");

    var opcion = "";

    var a_items = "";
    var a_opciones = "";
    var seleccionado = 0;
    var opcion = 0;

    for (var x = 0; x < opciones.length; x++) {
        opcion = 0;
        if (opciones[x].checked) {
            opcion = 1;
            seleccionado = 1;
        }
            
        if (a_items != "") {
            a_items += "|";
            a_opciones += "|";
        }
        a_items += items[x].value;
        a_opciones += opcion;
    }

    if (seleccionado == 0) {
        swal("Acción Cancelada", "Debe de seleccionar por lo mínimo una plantilla de calibración", "warning");
        return false;
    }

    var parametros = "ingreso=" + Ingreso + "&a_items=" + a_items + "&a_opciones=" + a_opciones;
    var datos = LlamarAjax("Laboratorio","opcion=Editar_Plantillas&" + parametros).split("|");
    if (datos[0] == "0") {
        swal("", datos[1] + datos[2], "success");
        BuscarPlantillas(2, "", 1);
        $("#ModalPlantillas").modal("hide");
    }
    else
        swal("", datos[1], "warning");
}

function NoAutorizado() {
    if (IdRecibido == 0)
        return false;
    swal({
        title: 'Advertencia',
        text: '¿Desea retirar el ingreso ' + Ingreso + ' como no autorizado?',
        input: 'text',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('No Autorizado'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',

        inputValidator: function (value) {
            return new Promise(function (resolve, reject) {
                if (value) {
                    var datos = LlamarAjax("Laboratorio","opcion=NoAutorizado&ingreso=" + Ingreso + "&observacion=" + value)
                    datos = datos.split("|");
                    if (datos[0] == "0") {
                        mensaje = datos[1];
                        resolve()
                    } else {
                        reject(datos[1]);
                    }

                } else {
                    reject(ValidarTraduccion('Debe de ingresar un observación'));
                }
            })
        }
    }).then(function (result) {
        LimpiarTodo();
        $("#Ingreso").focus();
        swal({
            type: 'success',
            html: mensaje
        })
    });

}

$('select').select2();    

