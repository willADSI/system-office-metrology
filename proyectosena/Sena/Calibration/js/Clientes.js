﻿var IdCliente = 0;
var IdSede = 0;
var IdContacto = 0;
var DocumentoCli = "";

$("#TablaPrecios").html(CargarCombo(15, 1));
$("#TipoCliente, #MTipo").html(CargarCombo(14, 1));
$("#SDepartamento").html(CargarCombo(11, 1));
$("#Pais, #SPais").html(CargarCombo(18, 1));
$("#MPais").html($("#Pais").html());
$("#MDepartamento").html(CargarCombo(11, 1));
$("#MCiudad").html(CargarCombo(12, 1));
$("#Asesor, #MAsesor").html(CargarCombo(13, 1));
$("#Iva").html(CargarCombo(35, 0));
$("#TipoDocumento").html(CargarCombo(53, 1));
$("#CuentaContable").html(CargarCombo(83, 1));

$("#ReteFuente").html(CargarCombo(90, 7, "", "ReteFuente"));
$("#ReteIva").html(CargarCombo(90, 7, "", "ReteIVA"));
$("#ReteIca").html(CargarCombo(90, 7, "", "ReteICA"));


$("#Pais, #SPais").val(1).trigger("change");


tb = $('input');
$(tb).keypress(enter2tab);

tb = $('select');
$(tb).keypress(enter2tab);

function enter2tab(e) {
    if (e.keyCode == 13) {
        id = $(this).attr("id");

        cb = parseInt($(this).attr('tabindex'));

        if (id == "Documento") {
            if ($.trim($(this).val()) != "") {

                BuscarCliente($(this).val());
                $("#Sede").focus();
                return false;
            }
        }

        if ($(':input[tabindex=\'' + (cb + 1) + '\']') != null) {
            $(':input[tabindex=\'' + (cb + 1) + '\']').focus();
            $(':input[tabindex=\'' + (cb + 1) + '\']').select();
            e.preventDefault();

            return false;
        }

        if ($(':select[tabindex=\'' + (cb + 1) + '\']') != null) {
            console.log("1");
            $(':select[tabindex=\'' + (cb + 1) + '\']').focus();
            $(':select[tabindex=\'' + (cb + 1) + '\']').select();
            e.preventDefault();

            return false;
        }
    }
}

function ValidarCliente(Caja) {
    documento = Caja.value;
    if ($.trim(DocumentoCli) != "") {
        if (DocumentoCli != documento) {
            DocumentoCli = "";
            LimpiarDatos();
        }
    }
}

function LimpiarTodo() {
    $("#Documento").val("");
    DocumentoCli = "";
    $("#TipoCliente").val("").trigger("change");
    LimpiarDatos();
    TablaSede();
    TablaContacto();
}

function CambioDepartamento(pais, combo,combo2) {
    $("#" + combo).html(CargarCombo(11, 1, "", pais));
    $("#" + combo2).html("");
}

function CambioCiudad(departamento, combo) {
    $("#" + combo).html(CargarCombo(12, 1, "", departamento));
}

function LimpiarDatos() {
    IdCliente = 0;
    DocumentoCli = "";
    $("#TablaPrecios").val("").trigger("change");
    $("#Nombres").val("");
    $("#Comercial").val("");
    $("#Telefonos").val("");
    $("#Celular").val("");
    $("#Email").val("");
    $("#EmailCartera").val("");
    $("#IdCliente").val("0");
    $("#Direccion").val("");
    $("#Pais").val(1).trigger("change");
    $("#Descuento").val("");
    $("#Estado").val("1").trigger("change");
    $("#Iva").val("1").trigger("change");
    $("#Certificado").val("").trigger("change");
    $("#CuentaContable").val("").trigger("change");
    $("#InformeTecnico").val("SI").trigger("change");
    $("#Limite").val("");
    $("#PlazoPago").val("");
    $("#DiasPre").val("");
    $("#DiasBlo").val("");
    $("#ReteFuente").val("0").trigger("change");
    $("#ReteIca").val("0").trigger("change");
    $("#ReteIva").val("0").trigger("change");
    $("#Solicitud").val("SI").trigger("change");
    $("#Cotizacion").val("SI").trigger("change");
    $("#Ajuste").val("SI").trigger("change");
    $("#Asesor").val("").trigger("change");
    $("#Habitual").val("NO").trigger("change");
    $("#EnviarCertificado").val("NO").trigger("change");
    $("#FirmaCertificado").val("DIGITAL").trigger("change");
    LimpiarContacto();
    LimpiarSede();
    CliEstadoCuenta(0);
    $("#DocumentoCliente").attr("src", url_cliente + "DocumentosClientes/0.pdf");
    $("#CreacionDocumento").html("");
    $("#TipoDocumento").val("").trigger("change");
    $("#ArchivoCliente").val("");

    $("#CliESaldo").val("0");
    $("#CliEVencido").val("0");
    $("#CliEcTipo").val("");
    $("#CliEcPlazo").val("");

}

function CongTipoCliente(tipo) {
        
    if (tipo * 1 == 1) {
        $("#Limite").prop("disabled", true);
        $("#PlazoPago").prop("disabled", true);
        $("#DiasPre").prop("disabled", true);
        $("#DiasBlo").prop("disabled", true);
    } else {
        $("#Limite").prop("disabled", false);
        $("#PlazoPago").prop("disabled", false);
        $("#DiasPre").prop("disabled", false);
        $("#DiasBlo").prop("disabled", false);
    }
}

function TablaSede() {
    var datos = LlamarAjax('Configuracion',"opcion=TablaSede&cliente=" + IdCliente);
    var datasedjson = JSON.parse(datos);
    $('#TablaSede').DataTable({
        data: datasedjson,
        bProcessing: true,
        bDestroy: true,
        columns: [
            { "data": "id" },
            { "data": "nombre" },
            { "data": "direccion" },
            { "data": "departamento" },
            { "data": "ciudad" },
            { "data": "eliminar" }
        ],
        "language": {
            "url": LenguajeDataTable
        }
    });
}

function TablaContacto() {
    var datos = LlamarAjax('Configuracion',"opcion=TablaContacto&cliente=" + IdCliente);
    var dataconjson = JSON.parse(datos);
    $('#TablaContacto').DataTable({
        data: dataconjson,
        bProcessing: true,
        bDestroy: true,
        columns: [
            { "data": "id" },
            { "data": "nombres" },
            { "data": "email" },
            { "data": "telefonos" },
            { "data": "fax" },
            { "data": "eliminar" },
            { "data": "correo" },
            { "data": "permiso" }
        ],
        "language": {
            "url": LenguajeDataTable
        }
    });
}

function LlamarPermisos(id, correo, contacto) {

    $(".permiso").prop("checked", "");
    
    var datos = LlamarAjax("Configuracion","opcion=BuscarPermisosContac&contacto=" + id);

    if (datos != "[]") {
        var data = JSON.parse(datos);
       
        if (data[0].cotizacion * 1 == 1)
            $("#PermCotizacion").prop("checked", "checked");
        if (data[0].ingreso * 1 == 1)
            $("#PermIngreso").prop("checked", "checked");
        if (data[0].estado_cuenta * 1 == 1)
            $("#PermEstadoCuenta").prop("checked", "checked");
        if (data[0].solicitudes * 1 == 1)
            $("#PermSolicitudes").prop("checked", "checked");
        if (data[0].certificado * 1 == 1)
            $("#PermCertificados").prop("checked", "checked");
        if (data[0].inventario * 1 == 1)
            $("#PermInventarios").prop("checked", "checked");
        if (data[0].queja * 1 == 1)
            $("#PermQuejas").prop("checked", "checked");
    }
    
    $("#ContacPermiso").html(contacto);
    $("#IdContacPermiso").val(id);
    $("#modalPermisoContacto").modal("show");
}

$("#FormPermisos").submit(function (e) {
    e.preventDefault();
    var datos = LlamarAjax("Configuracion","opcion=AsignarPermisos&" + $("#FormPermisos").serialize()).split("|");
    if (datos[0] == "0") {
        $("#modalPermisoContacto").modal("hide");
        swal("", datos[1], "success");
    } else
        swal("Acción Cancelada", datos[1], "warning");
}); 

function TablaContactoReep() {
    var datos = LlamarAjax('Configuracion',"opcion=TablaContacto&cliente=" + IdCliente);
    var dataconjson = JSON.parse(datos);
    $('#TablaReeContacto').DataTable({
        data: dataconjson,
        bProcessing: true,
        bDestroy: true,
        columns: [
            { "data": "id" },
            { "data": "nombres" },
            { "data": "email" },
            { "data": "telefonos" },
            { "data": "fax" }
        ],
        "language": {
            "url": LenguajeDataTable
        }
    });
}

function BuscarCliente(documento) {

    if ($.trim(documento) == "")
        return false;

    if (DocumentoCli == documento)
        return false;

    
    var parametros = "documento=" + documento;
    var datos = LlamarAjax('Configuracion','opcion=BuscarCliente&' + parametros);
    if (datos != "[]") {
        DocumentoCli = documento;
        var data = JSON.parse(datos);
        IdCliente = data[0].id;
        $("#TipoCliente").val(data[0].idtipcli).trigger("change");
        $("#CliEcTipo").val($('#TipoCliente option:selected').text());
        $("#TablaPrecios").val(data[0].tablaprecio).trigger("change");
        $("#Pais").val(data[0].idpais).trigger("change");
        $("#Iva").val(data[0].idiva).trigger("change");
        $("#Departamento").val(data[0].iddepartamento).trigger("change");
        $("#EnviarCertificado").val(data[0].enviar_certificado).trigger("change");
        $("#Ciudad").val(data[0].idciudad).trigger("change");
        $("#Nombres").val(data[0].nombrecompleto);
        $("#Comercial").val((data[0].nombrecomer ? data[0].nombrecomer : ""));

        $("#ReteFuente").val(data[0].retefuente).trigger("change");
        $("#ReteIca").val(data[0].reteica).trigger("change");
        $("#ReteIva").val(data[0].reteiva).trigger("change");

        $("#Solicitud").val(data[0].req_solicitud).trigger("change");
        $("#Cotizacion").val(data[0].aprobar_cotizacion).trigger("change");
        $("#Ajuste").val(data[0].aprobar_ajuste).trigger("change");
        $("#FirmaCertificado").val(data[0].firmacertificado).trigger("change");
        
        $("#Telefonos").val(data[0].telefono);
        $("#Celular").val(data[0].celular);
        $("#Email").val(data[0].email);
        $("#EmailCartera").val(data[0].emailcartera);
        $("#IdCliente").val(data[0].id);
        $("#Direccion").val(data[0].direccion);

        $("#Asesor").val(data[0].asesor).trigger("change");
        $("#Habitual").val(data[0].habitual).trigger("change");

        $("#Descuento").val(data[0].descuento);
        $("#Estado").val(data[0].estado).trigger("change");
        if (data[0].idtipcli * 1 > 1) {
            $("#Limite").val(formato_numero(data[0].limitecre, 0,_CD,_CM,""));
            $("#PlazoPago").val(data[0].plazopago);
            $("#DiasPre").val(data[0].diasprebloqueo);
            $("#DiasBlo").val(data[0].diasbloqueo);
            
            $("#CliEcPlazo").val(data[0].plazopago);
        }
        
        $("#Certificado").val(data[0].pagcertificado).trigger("change");
        $("#InformeTecnico").val(data[0].paginformetecnico).trigger("change");
        $("#Entregasinfac").val(data[0].retirarsinfac).trigger("change");
        $("#CuentaContable").val(data[0].idcuecontable > 0 ? data[0].idcuecontable : "").trigger("change");

        TablaSede();
        TablaContacto();
    }
}

$("#formClientes").submit(function (e) {
    e.preventDefault();
    var cuenta = $("#CuentaContable").val() * 1;
    if (cuenta == 0) {
        swal("Acción Cancelada", "Debe de seleccionar la cuenta contable del cliente", "warning");
        return false;
    }
    var parametros = $("#formClientes").serialize().replace(/\%2C/g, "");
    var datos = LlamarAjax("Configuracion","opcion=GuardarCliente&" + parametros).split("|");
    if (datos["0"] == "0") {
        if (IdCliente == 0) {
            IdCliente = datos[1] * 1;
            $("#IdCliente").val(IdCliente);
            swal("", "Cliente guardado con éxito", "success");
        } else
            swal("", "Cliente actualizado con éxito", "success");
        TablaSede();
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
    
});

function LimpiarSede() {
    IdSede = 0;
    $("#SNombre").val("");
    $("#SDireccion").val("");
    $("#SDepartamento").val("").trigger("change");
    //$("#SCiudad").val(data[0].idciudad).trigger("change");
}

function GuardarSede() {

    if (IdCliente == 0) {
        swal("Accipon Cancelada", "Debe primero guardar los datos del clientes", "warning");
        return false;
    }

    var nombre = $.trim($("#SNombre").val());
    var direccion = $.trim($("#SDireccion").val());
    var ciudad = $("#SCiudad").val() * 1;
    var departamento = $("#SDepartamento").val() * 1;

    var mensaje = "";

    if (ciudad == 0) {
        $("#SCiudad").focus();
        mensaje = "<br> Debe seleccionar una ciudad";
    }
    if (departamento == 0) {
        $("#SDepartamento").focus();
        mensaje = "<br> Debe seleccionar un departamento " + mensaje;
    }
    if (direccion == "") {
        $("#SDireccion").focus();
        mensaje = "<br> Debe de ingresar la dirección de la sede" + mensaje;
    }
    if (nombre == "") {
        $("#SNombre").focus();
        mensaje = "<br> Debe de ingresar el nombre de la sede" + mensaje;
    }

    if (mensaje != "") {
        swal("Acción Cancelada", mensaje, "warning");
        return false;
    }

    var parametros = "id=" + IdSede + "&cliente=" + IdCliente + "&nombre=" + nombre + "&ciudad=" + ciudad + "&direccion=" + direccion;
    var datos = LlamarAjax("Configuracion","opcion=GuardarSede&" + parametros).split("|");
    if (datos[0] == "0") {
        TablaSede();
        swal("", "Sede guardada con éxito", "success");
        LimpiarSede();
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
}


function BuscarSede(id) {
    var datos = LlamarAjax("Configuracion","opcion=BuscarSede&id=" + id);
    var data = JSON.parse(datos);
    IdSede = data[0].id * 1;
    $("#SNombre").val(data[0].nombre);
    $("#SDireccion").val(data[0].direccion);
    $("#SDepartamento").val(data[0].iddepartamento).trigger("change");
    $("#SCiudad").val(data[0].idciudad).trigger("change");
}

$("#TablaSede > tbody").on("click", "tr", function (e) {
    var row = $(this).parents("td").context.cells;
    var id = row[0].innerText;
    BuscarSede(id);
});

function LimpiarContacto() {
    IdContacto = 0;
    $("#CNombre").val("");
    $("#CEmail").val("");
    $("#CTelefonos").val("");
    $("#CCelular").val("");
}


function GuardarContacto() {

    if (IdCliente == 0) {
        swal("Accipon Cancelada", "Debe primero guardar los datos del clientes", "warning");
        return false;
    }

    var nombre = $.trim($("#CNombre").val());
    var email = $.trim($("#CEmail").val());
    var telefono= $.trim($("#CTelefonos").val());
    var celular = $.trim($("#CCelular").val());
        
    var mensaje = "";

    if (celular == "" && telefono == "") {
        $("#CCelular").focus();
        mensaje = "<br> Debe ingresar un teléfono o celular";
    }
    if (email != "") {
        if (ValidarCorreo(email) == 2) {
            $("#CEmail").focus();
            mensaje = "<br> Debe de ingresar un correo electrónico válido" + mensaje;
        }
    }
    if (nombre == "") {
        $("#SNombre").focus();
        mensaje = "<br> Debe de ingresar el nombre completo del contacto" + mensaje;
    }

    if (mensaje != "") {
        swal("Acción Cancelada", mensaje, "warning");
        return false;
    }

    var parametros = "id=" + IdContacto + "&cliente=" + IdCliente + "&nombres=" + nombre + "&correo=" + email + "&telefono=" + telefono + "&celular=" + celular;
    var datos = LlamarAjax("Configuracion","opcion=GuardarContacto&" +  parametros).split("|");
    if (datos[0] == "0") {
        TablaContacto();
        swal("", "Contacto guardado con éxito", "success");
        LimpiarContacto();
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
}

$("#TablaContacto > tbody").on("click", "tr", function (e) {
    var row = $(this).parents("td").context.cells;
    IdContacto = row[0].innerText;
    $("#CNombre").val(row[1].innerText);
    $("#CEmail").val(row[2].innerText);
    $("#CTelefonos").val(row[3].innerText);
    $("#CCelular").val(row[4].innerText);
});

$("#TablaClientes > tbody").on("dblclick", "tr", function () {
    var row = $(this).parents("td").context.cells;
    var numero = row[0].innerText;
    $('#tab-clientes a[href="#tabcrear"]').tab('show');
    $('#tab-opciones a[href="#tabcomerciales"]').tab('show');
    $("#Documento").val(numero);
    BuscarCliente(numero);
});


function ConsultarClientes() {


    var documento =$.trim($("#MDocumento").val());
    var cliente= $.trim($("#MCliente").val());
    var comercial = $.trim($("#MComercial").val());
    var tipo = $("#MTipo").val() * 1;
    var estado = $.trim($("#MEstado").val());
    var habitual = $("#MHabitual").val();
    var aprobar_cotizacion = $("#MAprob_Cotizacion").val();
    var aprobar_ajuste = $("#MAprob_Ajuste").val();
    var pais = $("#MPais").val() * 1;
    var departamento = $("#MDepartamento").val() * 1
    var ciudad = $("#MCiudad").val() * 1;
    var asesor = $("#MAsesor").val() * 1;
        
    ActivarLoad();
    setTimeout(function () {
        var parametros = "documento=" + documento + "&nombrecomp=" + cliente + "&nombrecomer=" + comercial + "&estado=" + estado + "&tipo=" + tipo +
            "&pais=" + pais + "&departamento=" + departamento + "&ciudad=" + ciudad + "&asesor=" + asesor +
            "&habitual=" + habitual + "&aprobar_cotizacion=" + aprobar_cotizacion + "&aprobar_ajuste=" + aprobar_ajuste;
        var datos = LlamarAjax("Configuracion","opcion=ConsultarClientes&" + parametros);
        DesactivarLoad();

        var datajson = JSON.parse(datos);
        $('#TablaClientes').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "documento" },
                { "data": "nombrecompleto" },
                { "data": "nombrecomer" },
                { "data": "habitual" },
                { "data": "tipocliente" },
                { "data": "estado" },
                { "data": "direccion" },
                { "data": "telefono" },
                { "data": "celular" },
                { "data": "email" },
                { "data": "pais" },
                { "data": "departamento" },
                { "data": "ciudad" },
                { "data": "cuenta" }
            ],

            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy','colvis'
            ]
        });
    }, 15);
}

function CambioNIT() {
    if (IdCliente == 0) {
        swal("Acción Cancelada", "Debe seleccionar el cliente", "warning");
        return false;
    }
    $("#SCambioNit").html(DocumentoCli);
    $("#modalCambioNitCli").modal("show");

}

function CambiarNIT() {
    var documento = $.trim($("#DDocumento").val());
    if (documento == "") {
        $("#DDocumento").focus();
        swal("Acción Cancelada", "Debe de ingresar el nuevo NIT/Cédula", "warning");
        return false;
    }

    var datos = LlamarAjax("Configuracion","opcion=CambioNit&documento=" + documento + "&id=" + IdCliente + "&tipo=1").split("|");
    if (datos[0] == "0") {
        $("#modalCambioNitCli").modal("hide");
        $("#Documento").val(documento);
        BuscarCliente(documento);
        swal("", datos[1], "success");
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
}

function EnviarClave() {
    if (IdCliente == 0)
        return false;
    ActivarLoad();
    setTimeout(function () {
        var datos = LlamarAjax("Configuracion","opcion=EnviarClave&idcliente=" + IdCliente).split("|");
        DesactivarLoad();
        if (datos[0] == "0")
            swal("", datos[1], "success");
        else
            swal("Acción Cancelada", datos[1], "warning");
    }, 15);
}

function ClaveContacto(contacto, correo, nombre) {
    if (ValidarCorreo(correo) == 2) {
        swal("Acción Cancelada", "El " + nombre + " no posee un correo válido", "warning");
        return false;
    }
    ActivarLoad();
    setTimeout(function () {
        var datos = LlamarAjax("Configuracion","opcion=EnviarClaveContac&contacto=" + contacto).split("|");
        DesactivarLoad();
        if (datos[0] == "0")
            swal("", datos[1], "success");
        else
            swal("Acción Cancelada", datos[1], "warning");
    }, 15);

}

function EliminarContacto(id, contacto) {
    var accion = 1;
    var opcion = 0;
    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: ValidarTraduccion('¿Desea eliminar el contacto ' + contacto + '?'),
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Eliminar'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        allowOutsideClick: false,

        preConfirm: function (result) {
            return new Promise(function (resolve, reject) {
                $.post("Configuracion/EliminarContacto", "Id=" + id + "&IdReemplazo=0")
                    .done(function (data) {
                        datos = data.split("|");
                        if (datos[0] == "0")
                            resolve()
                        else {
                            accion = 0;
                            resolve();
                            opcion = datos[0] * 1;
                        }

                    })
            })
        }
    }]).then(function (data) {
        if (accion == 1) {
            TablaContacto();
            LimpiarContacto();
        } else {
            $("#IdReemplazo").val(id);
            TablaContactoReep();
            $("#modalReeContacto").modal("show");
        }
    });
}

function CambioDocumento(documento) {
    $("#ArchivoCliente").val("");
    if (documento * 1 == 0) {
        $("#DocumentoCliente").attr("src", url_cliente + "DocumentosClientes/0.pdf");
        $("#CreacionDocumento").html("");
        return false;
    }

    if (IdCliente > 0) {
        
        var parametros = "Cliente=" + IdCliente + "&Documento=" + documento;
        var datos = LlamarAjax("Configuracion","opcion=DocumentosCliente&" + parametros);
        if (datos == "[]") {
            $("#ArchivoCliente").attr("src", "");
            $("#DocumentoCliente").attr("src", url_cliente + "DocumentosClientes/0.pdf");
            swal("Acción Cancelada", "Este documento no se le ha cargado al cliente", "warning");
        } else {
            var data = JSON.parse(datos);
            $("#DocumentoCliente").attr("src", url_cliente + "DocumentosClientes/" + IdCliente + "/" + documento + ".pdf");
            $("#CreacionDocumento").html("Creado por " + data[0].usuario + " el día " + data[0].fecha);
        }
    } else {
        $("#TipoDocumento").val("").trigger("change");
        swal("Acción Cancelada", "Debe de seleccionar un cliente, o guardar los datos", "warning");
    }
}

function GuardarDocumento() {
    if (IdCliente > 0) {
        var archivo = $.trim($("#ArchivoCliente").val());
        var tipo = $("#TipoDocumento").val() * 1;
        if (tipo == 0) {
            $("#TipoDocumento").focus();
            swal("Acción Cancelada", "debe de seleccionar tipo de documento", "warning");
            return false;
        }
        if (archivo == "") {
            $("#ArchivoCliente").focus();
            swal("Acción Cancelada", "debe de seleccionar el archivo en pdf del documento", "warning");
            return false;
        }

        var parametros = "Cliente=" + IdCliente + "&Documento=" + tipo;
        var datos = LlamarAjax("Configuracion","opcion=GuardarDocumentosCliente&" + parametros).split("|");
        if (datos[0] == "0") {
            CambioDocumento(tipo);
        } else
            swal("Acción Cancelada", datos[1], "warning");
    }
}

function CliEstadoCuenta() {

    if (IdCliente == 0)
        return false;
    
    var saldo = 0;
    var vencido = 0;

    var datos = LlamarAjax("Caja","opcion=EstadoCuenta&cliente=" + IdCliente);
    var datajson = JSON.parse(datos);
    var table = $('#TabCliEEstCuen').DataTable({
        data: datajson,
        bProcessing: true,
        bDestroy: true,
        columns: [
            { "data": "factura" },
            { "data": "fecha" },
            { "data": "vencimiento" },
            { "data": "dias" },
            { "data": "subtotal", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
            { "data": "descuento", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
            { "data": "iva", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
            { "data": "total", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
            { "data": "abonado", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
            { "data": "saldo", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') }
        ],

        "language": {
            "url": LenguajeDataTable
        },
        dom: 'Bfrtip',
        buttons: [
            'excel', 'csv', 'copy','colvis'
        ]
    });

    table.rows().data().each(function (datos, index) {
        if (datos.dias * 1 > 0)
            vencido += datos.saldo;
        saldo += datos.saldo;
    });

    $("#CliESaldo").val(formato_numero(saldo, 0,_CD,_CM,""));
    $("#CliEVencido").val(formato_numero(vencido, 0,_CD,_CM,""));
}

function EnviarCliEEstCuen() {
    var cliente = IdCliente
    var correo = $("#EmailCartera").val();
    var principal = $("#Email").val();
    var saldo = NumeroDecimal($("#CliESaldo").val());

    if (IdCliente == 0)
        return false;

    if (saldo <= 0) {
        swal("Acción Cancelada", "No hay facturas pendientes para enviar", "warning");
        return false;
    }

    if (ValidarCorreo(principal) == 2){
        swal(ValidarTraduccion("Acción Cancelada"), ValidarTraduccion("El correo principal del cliente no es válido"), "warning");
        return false;
    }

    if (ValidarCorreo(correo) == 2){
        swal(ValidarTraduccion("Acción Cancelada"), ValidarTraduccion("El correo de cartera del cliente no es válido"), "warning");
        return false;
    }

    ActivarLoad();
    setTimeout(function () {
        var parametros = "principal=" + principal + "&e_email=" + correo + " &idcliente=" + cliente;
        var datos = LlamarAjax("Caja","opcion=EnvioEstadoCuenta&" + parametros);
        DesactivarLoad();
        datos = datos.split("|");
        if (datos[0] == "0") {
            swal("", ValidarTraduccion(datos[1]) + " " + principal + " " + correo, "success");
        } else
            swal("Error", datos[1], "error");
    }, 15);

}

$('select').select2();
DesactivarLoad();


