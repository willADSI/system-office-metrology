﻿
d = new Date();
month = d.getMonth() + 1;
day = d.getDate();

output = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-' +
    (day < 10 ? '0' : '') + day;

$("#RFechaI").val(output);
$("#RFechaF").val(output);

$("#RAsesor").html(CargarCombo(19, 1));
$("#RCuenta").html(CargarCombo(22, 1));

var nfilas = 0;
var OpcionReporte = 1;

$("form").submit(function (e) {
    e.preventDefault();
})

function Reporte(opcion) {

    var cliente = $.trim($("#RCliente").val());
    var codcli = $("#RCodigo").val() * 1;
    var recibo = $("#RNumero").val() * 1;
    var nit = $.trim($("#RNit").val())
    var asesor = $("#RAsesor").val() * 1;
    var cuenta = $("#RCuenta").val() * 1;
    var fechaf = $("#RFechaF").val();
    var fechad = $("#RFechaI").val();

    var ordenado = 0;
    var anulado = 0;

    if ($("#ROrdenarAse").prop("checked"))
        ordenado = 1

    if ($("#RAnulado").prop("checked"))
        anulado = 1


    $("#resultadopdf").removeAttr("src");

    ActivarLoad();
    setTimeout(function () {
        var parametros = "fechad=" + fechad + "&fechah=" + fechaf + "&recibo=" + recibo + "&codcli=" + codcli + "&cliente=" + cliente + "&nit=" + nit + "&cuenta=" + cuenta + "&asesor=" + asesor + "&anulado=" + anulado + "&ordenado=" + ordenado + "&tipo=1&opcion=" + opcion;
        var datos = LlamarAjax(url + "Caja/ImprimirConsignacion", parametros);
        datos = datos.split("|");
        if (datos[0] == "0") {
            switch (opcion) {
                case 1:
                    CargarTabla(datos[1]);
                    $("#pdf").removeClass("active");
                    $("#opcion2").removeClass("active");
                    $("#consultar").addClass("active");
                    $("#opcion1").addClass("active");
                    break;
                case 2:
                    $("#consultar").removeClass("active");
                    $("#opcion1").removeClass("active");
                    $("#pdf").addClass("active");
                    $("#opcion2").addClass("active");

                    $("#resultadopdf").attr("src", url + "DocumPDF/" + datos[1]);
                    break;

            }
        } else
            swal("Alerta", datos[1], "warning");
        DesactivarLoad();
    }, 15);
}

function CargarTabla(datos) {
    if (datos != "[]") {
        var datajson = JSON.parse(datos);
        $('#tablaconsulta').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "ChqReciboCaja" },
                { "data": "ChqConsec" },
                { "data": "ChqNumero" },
                { "data": "chqFechaConsig" },
                { "data": "ChqHora" },
                { "data": "ChqCodCliente" },
                { "data": "NomRazSoc" },
                { "data": "NomVenCal" },
                { "data": "ChqValorConsig" },
                { "data": "Movimiento" },
                { "data": "Estado" },
                { "data": "Cuenta" }
            ],
            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv',  'copy'
            ]
        });
    }

}


tb = $('input');
$(tb).keypress(enter2tab);

tb = $('select');
$(tb).keypress(enter2tab);

function enter2tab(e) {
    id = $(this).attr("id");
    if ((e.keyCode == 13) && ((id != "Codigo") || (id != "CodigoB"))) {
        cb = parseInt($(this).attr('tabindex'));
        if ($(':input[tabindex=\'' + (cb + 1) + '\']') != null) {
            $(':input[tabindex=\'' + (cb + 1) + '\']').focus();
            $(':input[tabindex=\'' + (cb + 1) + '\']').select();
            e.preventDefault();
            return false;
        }
    }
}

$('select').select2();