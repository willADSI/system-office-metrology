﻿d = new Date();
month = d.getMonth() + 1;
day = d.getDate();

var output = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-' +
    (day < 10 ? '0' : '') + day;

var Anio = $.trim(d.getFullYear());
Anio = $.trim(Anio.substr(2, 2))

var Anio = $.trim(d.getFullYear());
var ErrorCli = 0;
var IdCliente = 0;
var Cli_Correo = "";
var NombreCliente = "";
var DocumentoCli = "";
var Movimiento = 0;
var Anticipo = 0;

var opciones = "<option value=''>--Seleccione--</option>";
for (var x = Anio; x >= 2018; x--) {
    opciones += "<option value='" + x + "'>" + x + "</option>";
}
$("#MAnio").html(opciones);

var Guia = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-' +
    (day < 10 ? '0' : '') + day + '-' + d.getHours() + '-' + d.getMinutes() + '-' + d.getSeconds();


document.onkeydown = function (e) {
    tecla = (document.all) ? e.keyCode : e.which;

    switch (tecla) {
        case 112: //F1
            ModalClientes('Ant');
            return false;
            break;
        case 113: //F2
            CargarModaltarjeta();
            return false;
            break;
        case 114: //F3
            CargarModalCheque();
            return false;
            break;
        case 115: //F4
            CargarModalConsig()
            return false;
            break;
        case 117: //F6
            LimpiarPagos();
            return false;
            break;
        case 118: //f7
            LimpiarTodo();
            return false;
            break;
        case 119: //f8
            ModalRecibo();
            return false;
            break;
        case 120: //f9
            GuardarRecibo();
            return false;
            break;
        case 121: //f10
            EnviarRecibo();
            return false;
            break;
        case 122: //f7
            ImprimirRecibo(1);
            return false;
            break;
        case 123: //f7
            AnularRecibo();
            return false;
            break;
    }
    
}

$("#ChFecha").val(output);
$("#ttipo").html(CargarCombo(23, 1));
$("#con_cuenta, #MCuenta").html(CargarCombo(25, 1));

$("#ChBanco").html(CargarCombo(24, 1));

$("#ttipo").val("");

function LlamarModalMovimientos() {
    if (IdCliente == 0 || Anticipo > 0 || NumeroDecimal($("#Anticipo").val()) > 0)
        return false;
    CargarModalMovimientos();
    $("#ModalMovBancarioAnti").modal("show");
}

function CargarModalMovimientos() {

    var cuenta = $("#MCuenta").val()*1;
    var anio = $("#MAnio").val()*1;
    var mes = $("#MMes").val()*1;

    var datos = LlamarAjax("Caja","opcion=TablaMovimientos&tipo=1&cuenta=" + cuenta + "&anio=" + anio + "&mes=" + mes);
    var datajson = JSON.parse(datos);
    var tabla = $('#MTablaMovimientoAnti').DataTable({
        data: datajson,
        bProcessing: true,
        bDestroy: true,
        paging: true,
        ordering: true,
        info: false,
        searching: true,
        columns: [
            { "data": "id" },
            { "data": "fecha" },
            { "data": "referencia" },
            { "data": "descripcion" },
            { "data": "oficina" },
            { "data": "entrada", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') }
        ],

        "language": {
            "url": LenguajeDataTable
        }

    });
}

function AnularAnticipo() {

    if (Anticipo == 0)
        return false;

    var resultado = "";
    swal({
        title: 'Advertencia',
        text: '¿' + ValidarTraduccion('Desea Anular el Anticipo') + ' ' + Anticipo + ' ' + ValidarTraduccion('del cliente') + ' ' + NombreCliente,
        input: 'text',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Anular'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',

        inputValidator: function (value) {
            return new Promise(function (resolve, reject) {
                if (value) {
                    var datos = LlamarAjax("Caja","opcion=AnularAnticipo&anticipo=" + Anticipo + "&observaciones=" + value)
                    resultado = Anticipo;
                    datos = datos.split("|");
                    if (datos[0] == "0") {
                        resolve()
                    } else {
                        reject(datos[1]);
                    }

                } else {
                    reject(ValidarTraduccion('Debe de ingresar una observación'));
                }
            })
        }
    }).then(function (result) {
        Limpiar();
        IdCliente = 0;
        $("#Cliente").val("");
        $("#Cliente").focus();
        swal({
            type: 'success',
            html: 'Anticipo Nro ' + resultado + ' anulado con éxito'
        })
    })

}

function LimpiarTarjeta() {
    $("#tnumero").val("").trigger("change");
    $("#Tarjeta").val("");
    $("#ttipo").val("").trigger("change");
    $("#tcodseguridad").val("");
    $("#tfvencimiento").val("");
    $("#tautoriza").val("");
    CalcularTotal("");
}

function GuardarCheque(parametros) {
    var cantidad = $("#ChCantidad").val() * 1;
    var valor = NumeroDecimal($("#Cheque").val());
    var autorizado = $.trim($("#ChAutorizado").val()).toUpperCase();
    var autorizadopor = $.trim($("#ChAutorizacion").val()).toUpperCase();

    if (cantidad == 0) {
        $("#ChCantidad").focus()
        swal(ValidarTraduccion(ValidarTraduccion("Acción Cancelada")), "Debe de ingresar la cantidad de cheques", "warning");
        return false;
    }

    if (valor == 0) {
        $("#Cheque").focus()
        swal(ValidarTraduccion(ValidarTraduccion("Acción Cancelada")), "Debe de ingresar el valor en cheques", "warning");
        return false;
    }

    if (noVericarCheque == 0) {
        if ((autorizado.substr(0, 2) != "D-") && (autorizado.substring(1, 2) != "C-") && (autorizado.substring(1, 2) != "F-")) {
            $("#ChAutorizado").select();
            $("#ChAutorizado").focus();
            swal(ValidarTraduccion(ValidarTraduccion("Acción Cancelada")), "D-(nombre autorizador): para Datacheque <br> C- (nombre autorizador): para Covinoc <br> F - (nombre autorizador): para Fenalcheque", "warning");
            return false;
        }

        if (autorizado == "" || autorizadopor == "") {

            if (autorizado == "") {
                $("#ChAutorizado").select();
                $("#ChAutorizado").focus();
            } else {
                $("#ChAutorizacion").select();
                $("#ChAutorizacion").focus();
            }
            swal(ValidarTraduccion(ValidarTraduccion("Acción Cancelada")), "Debe de completar la autorización del cheque", "warning");
            return false;
        }
    }

    parametros += "&Guia=" + Guia + "&Valor=" + valor;

    var datos = LlamarAjax("Caja","opcion=GuardarCheque&"+ parametros);
    datos = datos.split("|");
    if (datos[0] == "0") {
        swal("", datos[1], "success");
        CargarTablaCheque();
        LimpiarCheque();
        
    } else {
        swal("", datos[1], "error");
    }
}

function EliminarTemporalCheque(id,numero) {

    swal({
        title: '¿Eliminar el cheque número ' + numero + '?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Sí',
        cancelButtonText: 'No'

    }).then(function () {

        var parametros = "id=" + id+"&guia=";
        var datos = LlamarAjax("Caja","opcion=EliminarCheque&"+ parametros);
        datos = datos.split("|");
        if (datos[0] == "0") {
            CargarTablaCheque();
            LimpiarCheque();
        } else {
            $.jGrowl(ValidarTraduccion(datos[1]), { life: 6000, theme: 'growl-warning', header: '' });
        }

    }, function (dismiss) {
        // dismiss can be 'cancel', 'overlay',
        // 'close', and 'timer'
        if (dismiss === 'cancel') {
        }
    })

}

function EditarTemporalCheque(id) {
    var datos = LlamarAjax("Caja","opcion=TablaCheque&id=" + id);
    eval("data=" + datos);

    $("#id_cheque").val(data[0].id);
    $("#ChAutorizado").val(data[0].AutorizadoPor)
    $("#ChAutorizacion").val(data[0].Autoriza)
    EditTotalCheques = data[0].Valor * 1;
    $("#Cheque").val(formato_numero(data[0].Valor, "0", ".", ",", ""));
    $("#ChCantidad").val("1");
    if (data[0].chqEstado == "F") {
        $("#ChTipoC_B").attr("checked", false);
        $("#ChTipoC_C").attr("checked", false);
    } else {
        if (chqEstado == "P") 
            $("#ChTipoC_B").attr("checked", true);
        else
            $("#ChTipoC_B").attr("checked", false);
    }
        
    $("#ChPersona_J").attr("checked", true);

    $("#ChNombre").val(data[0].Girador);
    $("#ChCedula").val(data[0].Cedula);
    $("#ChTelefono").val(data[0].Telefono);
    $("#ChDireccion").val(data[0].Direccion);
    $("#ChFecha").val(data[0].chqFechaCheque.substr(0, 10));

    $("#ChCuenta").val(data[0].Cuenta);
    $("#ChNumero").val(data[0].Numero);
    $("#ChBanco").val(data[0].chqCodBanco).trigger('change');
    $("#ChPlaza").val(data[0].chqCodPlaza).trigger('change');
    
}

function CargarTablaCheque() {
    var datos = LlamarAjax("Caja","opcion=TablaCheque&Guia=" + Guia);
    CargarTablaJsonTitulo(datos, "tablacheque", "divtabcheque");
    CalcularTotal("");
}

function LimpiarPagos() {

    if (Anticipo > 0)
        return false;

    LimpiarTarjeta();
    LimpiarConsignacion();
    CargarTablaCheque();
    LimpiarCheque();
    $("#Efectivo").val("0")
    $("#Convenio").val("0")
    Guia = Guia = d.getFullYear() + '-' +
        (month < 10 ? '0' : '') + month + '-' +
        (day < 10 ? '0' : '') + day + '-' + d.getHours() + '-' + d.getMinutes() + '-' + d.getSeconds();
    CalcularTotal("");

}

function LimpiarCheque() {
    chCambioPersona('J');
    $("#ChAutorizado").val("")
    $("#ChAutorizacion").val("")
    $("#Cheque").val("0");
    EditTotalCheques = 0;
    $("#Cheque").val("");
    $("#ChCantidad").val("1");
    CalcularTotal("");
    $("#ChTipoC_B").attr("checked", false);
    $("#ChTipoC_C").attr("checked", false);

    $("#ChPersona_J").attr("checked", true);
    chCambioPersona('J');
    
    $("#ChCuenta").val("");
    $("#ChNumero").val("");

    $("#ChBanco").val("").trigger('change');
    $("#ChPlaza").val("").trigger('change');
    $("#id_cheque").val("0")

}

$("#formCheques").submit(function (e) {

    e.preventDefault();
    var Parametros = $("#formCheques").serialize() 
    GuardarCheque(Parametros);

})

function LimpiarTodo() {
    Limpiar();
    $("#Cliente").val("");
    IdCliente = "";
}

function Limpiar() {
    $("#NombreCliente").html("");
    NombreCliente = "";
    DocumentoCli = "";
    Movimiento = 0;
    Diferencia = 0;
    Anticipo = 0;
    $("#divAnulaciones").addClass("hidden");
    $("#btnImprimir").addClass("hidden");
    $("#btnImprimirmov").addClass("hidden");
    $("#btnEnviar").addClass("hidden");
    $("#btnAnular").addClass("hidden");
    $("#btnGuardar").removeClass("hidden");
    $("#Diferencia").val("");
    $("#Detalle_Movimiento").html("");
    
    LimpiarConsignacion();
    $("#chTotales").html("0");
    $("#divtabcheque").html("");
    LimpiarCheque();
        
    CorreoEmpresa = "";
    NombreCliente = "";
    Guia = Guia = d.getFullYear() + '-' +
        (month < 10 ? '0' : '') + month + '-' +
        (day < 10 ? '0' : '') + day + '-' + d.getHours() + '-' + d.getMinutes() + '-' + d.getSeconds();

    $("#tnumero").val("");
    $("#tvalor").val("");
    $("#ttipo").val("");
    $("#tcodseguridad").val("");
    $("#tfvencimiento").val("");
    $("#tautoriza").val();
            
    $("#Convenio").val("0");
    $("#ReciboCaja").val("0");
    $("#Concepto").val("");
    $("#Anticipo").val("");
    $("#Saldo").val("0");
    $("#NAnticipo").html("0");
    $("#TTarjeta").val("0");
    $("#Tarjeta").val("0");
    $("#Efectivo").val("0");
    $("#Consignacion").val("0");
    $("#Cheque").val("0");
    $("#TConsignacion").val("0");
    $("#TCheque").val("0");
        
    $("#Efectivo").prop("disabled", false);
    $("#Convenio").prop("disabled", false);
    $("#Concepto").prop("disabled", false);
    $("#Anticipo").prop("disabled", false);
        
    $("#NombreCliente").html("");
    $("#ObsAnulacion").html("");
            
}

$("#Cliente").bind('keypress', function (e) {
    if (e.keyCode == 13) {
        $("#Cliente").blur();
    }
})


function ImprimirRecibo() {
    if (NRecibo > 0) {
        ActivarLoad();
        setTimeout(function () {
            var datos = LlamarAjax("Caja","opcion=ImprimirRecibo&recibo=" + NRecibo);
            DesactivarLoad();
            datos = datos.split("|");
            if (datos[0] == "0") {
                window.open("Documpdf/" + datos[1])
            } else {
                swal(ValidarTraduccion(ValidarTraduccion("Acción Cancelada")), datos[1], "error");
            }
        }, 15);
    }
}

function EnviarRecibo() {
    if (NRecibo > 0) {
        swal({
            title: 'Correo Electrónico',
            text: NombreCliente,
            input: 'email',
            showCancelButton: true,
            confirmButtonText: 'Enviar',
            showLoaderOnConfirm: true,
            inputValue: Cli_Correo,
            preConfirm: (email) => {
                return new Promise((resolve, reject) => {

                    setTimeout(() => {

                        var datos = LlamarAjax("Caja","opcion=ImprimirRecibo&recibo=" + NRecibo);
                        DesactivarLoad();
                        datos = datos.split("|");
                        var Archivo = "";
                        if (datos[0] == "0") {
                            Archivo = datos[1];
                        } else {
                            reject(datos[1]);
                        }

                        var parametros = "recibo=" + NRecibo + "&e_email=" + email + "&archivo=" + Archivo + "&cliente=" + NombreCliente + "&codigo=" + IdCliente;
                        var datos = LlamarAjax("Caja","opcion=EnvioRecibo&"+ parametros);
                        datos = datos.split("|");
                        if (datos[0] == "0") {
                            resolve()
                        } else {
                            reject(datos[1]);
                        }
                           
                    }, 15);
                    
                })
            },
            allowOutsideClick: () => !swal.isLoading()
        }).then((result) => {
            if (result) {
                
                swal({
                    type: 'success',
                    title: 'Recibo Nro ' + NRecibo +  ' Enviado',
                    html: 'Correo Electrónico: ' + result
                })
            }
        })
    }
}

function CargarModaltarjeta() {
    if (NRecibo > 0)
        return false
    if ($("#NombreCliente").html() == "")
        return false;
    if (Recaudado > 0)
        $("#ModalTarjeta").modal("show");
    else {
        $("#VRecaudado").focus();
        $.jGrowl(ValidarTraduccion("Debe de ingresar el valor a recaudar"), { life: 4000, theme: 'growl-warning', header: '' });
    }
}

function CargarModalCheque() {
    if (NRecibo > 0)
        return false
    if ($("#NombreCliente").html() == "")
        return false;
    if (Recaudado > 0) {
        LimpiarCheque();
        CargarTablaCheque();
        $("#ModalCheque").modal("show");
    }else {
        $("#VRecaudado").focus();
        $.jGrowl(ValidarTraduccion("Debe de ingresar el valor a recaudar"), { life: 4000, theme: 'growl-warning', header: '' });
    }
}

function CargarVerPagos(tipo, forma) {
    
    if (Anticipo == 0)
        return false;
    if ((tipo == "C") && (NumeroDecimal($("#TCheque").val()) * 1 == 0))
        return false;
    if ((tipo == "T") && (NumeroDecimal($("#TTarjeta").val()) * 1 == 0))
        return false;
    if ((tipo == "CO") && (NumeroDecimal($("#TConsignacion").val()) * 1 == 0))
        return false;

    $("#VerChequeRecibo").html(LlamarAjax("Caja","opcion=FormaPagoAnticipo&anticipo=" + Anticipo + "&tipo=" + tipo))
    $("#TituloVerPagos").html(forma);
    $("#ModalVerCheque").modal("show");
    
}

function CargarModalConsig() {

    var monto = NumeroDecimal($("#Anticipo").val());
    var diferencia = NumeroDecimal($("#Diferencia").val());
    var consignado = NumeroDecimal($("#TConsignacion").val());
    if (Anticipo > 0)
        return false
    if (IdCliente == 0)
        return false;
    if (monto == 0) {
        $("#Anticipo").focus();
        $.jGrowl(ValidarTraduccion("Debe de ingresar el valor del anticipo"), { life: 4000, theme: 'growl-warning', header: '' });
        return false;
    }
        
    if ((diferencia == monto) || (consignado == monto)) {
        if (diferencia == monto) {
            $("#Consignacion").val(formato_numero(monto, 0,_CD,_CM,""));
            $("#TConsignacion").val(formato_numero(monto, 0,_CD,_CM,""));
            $("#Diferencia").val("0");
        }
        $("#ModalConsignacion").modal("show");
    } else {
        swal(ValidarTraduccion(ValidarTraduccion("Acción Cancelada")), ValidarTraduccion("La consignación debe de ser por el monto total"), "warning")
    }
}

function CalcularTotal(caja) {

    if (IdCliente == 0) {
        caja.value = "";
        return false; 
    }

    if (caja != "")
        ValidarTexto(caja, 1);

    var Diferencia = 0;
    var Anticipo = NumeroDecimal($("#Anticipo").val());
    var Convenio = NumeroDecimal($("#Convenio").val());
    var Banco = NumeroDecimal($("#TConsignacion").val());
    var Tarjeta = NumeroDecimal($("#TTarjeta").val());
    var Efectivo = NumeroDecimal($("#Efectivo").val());
    var Cheque = NumeroDecimal($("#TCheque").val());

    Diferencia = Anticipo - Convenio - Banco - Tarjeta - Efectivo - Cheque


    if (Diferencia < 0) {
        $.jGrowl(ValidarTraduccion("Lo abonado en") + " " + caja.id + " " + ValidarTraduccion("no puede ser mayor al monto del anticipo"), { life: 4000, theme: 'growl-warning', header: '' });
        caja.value = "";
    }

    $("#Diferencia").val(formato_numero(Diferencia, 0, ".", ","));
}

function LimpiarConsignacion() {

    $("#con_fecha").val("");
    $("#con_tipo").val("").trigger("change");
    $("#con_cuenta").val("").trigger("change");
    $("#con_numero").val("");
    $("#Consignacion").val("");
    CalcularTotal("");
}

function chCambioPersona(tipo) {
    if (tipo == "N") {
        $("#ChNombre").val("");
        $("#ChCedula").val("");
        $("#ChTelefono").val("");
        $("#ChDireccion").val("");
    } else {
        $("#ChNombre").val($("#ChNombre").prop("inicial"));
        $("#ChCedula").val($("#ChCedula").prop("inicial"));
        $("#ChTelefono").val($("#ChTelefono").prop("inicial"));
        $("#ChDireccion").val($("#ChDireccion").prop("inicial"));
    }
}


function GuardarAnticipo() {

    if (($("#NombreCliente").html() == "") || (Anticipo != 0))
        return false;

    var VAnticipo = NumeroDecimal($("#Anticipo").val());
    var Convenio = NumeroDecimal($("#Convenio").val());
    var Efectivo = NumeroDecimal($("#Efectivo").val());
    var Consignacion = NumeroDecimal($("#TConsignacion").val());
    var Diferencia = NumeroDecimal($("#Diferencia").val());
    
    
    var numtarj = $("#tnumero").val() * 1;
    var valtarj = NumeroDecimal($("#Tarjeta").val()) * 1;
    var tiptarj = $("#ttipo").val() * 1;
    var auttarj = $.trim($("#tautoriza").val());
    var con_fecha = $("#con_fecha").val();
    var con_tipo = $("#con_tipo").val();
    var con_cuenta = $("#con_cuenta").val();
    var con_numero = $("#con_numero").val() * 1;
    var Concepto = $.trim($("#Concepto").val());

    if (Consignacion > 0) {
        if (con_cuenta == "") {
            $("#ModalConsignacion").modal("show");
            $("#con_cuenta").focus();
            swal(ValidarTraduccion(ValidarTraduccion("Acción Cancelada")), ValidarTraduccion("Debe de Seleccionar la Cuenta a Consignar"), "error");
            return false
        }

        if (con_numero == 0) {
            $("#ModalConsignacion").modal("show");
            $("#con_numero").focus();
            swal(ValidarTraduccion(ValidarTraduccion("Acción Cancelada")), "Debe de Ingresar el número de control de la consignación", "error");
            return false
        }

        if (con_fecha == "") {
            $("#ModalConsignacion").modal("show");
            $("#con_fecha").focus();
            swal(ValidarTraduccion(ValidarTraduccion("Acción Cancelada")), ValidarTraduccion("Debe de Ingresar la Fecha de Consignación"), "error");
            return false
        }
    }

    if (valtarj > 0) {
        if (tiptarj == 0) {
            $("#ModalTarjeta").modal("show");
            swal(ValidarTraduccion(ValidarTraduccion("Acción Cancelada")), ValidarTraduccion("Debe de ingresar la entidad de la tarjeta"), "error");
            return false
        }
        if ($.trim(numtarj).length < 4) {
            $("#ModalTarjeta").modal("show");
            swal(ValidarTraduccion(ValidarTraduccion("Acción Cancelada")), ValidarTraduccion("El número de la tarjeta debe ser de 4 dígistos mínimos"), "error");
            return false
        }
        if (auttarj == "") {
            $("#ModalTarjeta").modal("show");
            swal(ValidarTraduccion(ValidarTraduccion("Acción Cancelada")), ValidarTraduccion("Debe de ingresar la autorización de la tarjeta"), "error");
            return false
        }
    }


    

    if (Concepto == "") {
        $("#Concepto").focus();
        swal(ValidarTraduccion("Acción Cancelada"), ValidarTraduccion("Debe de ingresar el concepto del anticipo"), "warning");
        return false;
    }
        

    if (VAnticipo <= 0) {
        $("#VRecaudado").focus();
        swal(ValidarTraduccion("Acción Cancelada"), ValidarTraduccion("Debe de ingresar el monto del anticipo"), "warning");
        return false;
    }

        
    if (Diferencia != 0) {
        swal(ValidarTraduccion("Acción Cancelada"), ValidarTraduccion("No puede haber diferencia en el anticipo"), "warning");
        return false;
    }
            
    ActivarLoad();
    setTimeout(function () {
        var parametros = "Cliente=" + IdCliente + "&Concepto=" + Concepto + "&Anticipo=" + VAnticipo + "&Efectivo=" + Efectivo +
            "&Convenio=" + Convenio + 
            "&numtarj=" + numtarj + "&valtarj=" + valtarj + "&tiptarj=" + tiptarj + "&auttarj=" + auttarj + "&Guia=" + Guia +
            "&confecha=" + con_fecha + "&contipo=" + con_tipo + "&concuenta=" + con_cuenta + "&connumero=" + con_numero + "&Consignacion=" + Consignacion +
            "&NombreCliente=" + NombreCliente + "&Movimiento=" + Movimiento;
        var datos = LlamarAjax("Caja","opcion=GuardarAnticipo&"+ parametros);
        DesactivarLoad();
        datos = datos.split("|");
        if (datos[0] == "0") {
            swal("", ValidarTraduccion(datos[1]) + " " + datos[2], "success");
            Anticipo = datos[2] * 1;
            $("#NAnticipo").html(Anticipo);
            $("#btnImprimir").removeClass("hidden")
            $("#btnImprimirmov").removeClass("hidden")
            $("#btnEnviar").removeClass("hidden")
            $("#btnAnular").removeClass("hidden")
        } else {
            swal(ValidarTraduccion("Acción Cancelada"), datos[1], "warning");
        }
    }, 15);
    
}

function ModalAnticipo() {
    $("#modalAnticipos").modal("show");
}

function BuscarAnticipo(anticipo, cliente) {

    $("#modalConsultarRecibos").modal("hide");
    ActivarLoad();
    setTimeout(function () {
        $("#Cliente").val(cliente);
        $("#NAnticipo").html(anticipo);
        Anticipo = anticipo;
        IdCliente = 0;
        DocumentoCli = "";
        BuscarCliente(cliente);
        var datos = LlamarAjax("Caja","opcion=BuscarAnticipo&anticipo=" + anticipo);
        var data = JSON.parse(datos);
        Diferencia = data[0].monto - data[0].convenio - data[0].consignacion - data[0].tarjeta - data[0].efectivo - data[0].Cheque;
        DesactivarLoad();
        $("#btnImprimir").removeClass("hidden");
        $("#btnImprimirmov").removeClass("hidden");
        $("#btnEnviar").removeClass("hidden");
        $("#btnGuardar").addClass("hidden");

        $("#Convenio").prop("disabled",true);
        $("#Efectivo").prop("disabled", true);
        $("#Anticipo").prop("disabled", true);
        $("#Concepto").prop("disabled", true);

        if (data[0].fechaanula) {
            $("#divAnulaciones").removeClass("hidden");
            $("#ObsAnulacion").html("Usuario que Anuló: " + data[0].usuarioanula + ", Fecha: " + data[0].fechaanula + ", Observación: " + data[0].observaanula);
            $("#btnAnular").addClass("hidden");
        } else {
            $("#btnAnular").removeClass("hidden");
            $("#divAnulaciones").addClass("hidden");
        }

        $("#Detalle_Movimiento").html(data[0].movimiento);
        $("#Concepto").val(data[0].concepto);
        $("#ReciboCaja").val(formato_numero(data[0].recibocaja, 0, ".", ","));
        $("#Saldo").val(formato_numero(data[0].saldo, 0, ".", ","));
        $("#Anticipo").val(formato_numero(data[0].monto, 0, ".", ","));
        $("#Efectivo").val(formato_numero(data[0].efectivo, 0, ".", ","));
        $("#TConsignacion").val(formato_numero(data[0].consignacion, 0, ".", ","));
        $("#TTarjeta").val(formato_numero(data[0].tarjeta, 0, ".", ","));
        $("#Convenio").val(formato_numero(data[0].convenio, 0, ".", ","));
        $("#TCheque").val(formato_numero(data[0].cheque, 0, ".", ","));
        $("#Diferencia").val("0");

        $("#modalAnticipos").modal("hide");
               
    }, 15);

}


function CargarModalAnticipos() {

    var anticipo = $("#BNAnticipo").val() * 1;
    var documento = $("#BDocumento").val() * 1;
    var cliente = $.trim($("#BCliente").val());
    var estado = $("#BEstado").val();
    var fechad = $("#BFechaDesde").val();
    var fechah = $("#BFechaHasta").val();
    var dias = $("#BUltimoDia").val() * 1;

    if (fechad == "" && fechah == "" && dias == 0) {
        swal("Alerta", "debe de ingresar una fecha o números de días ", "error");
        return false;
    }
    
    ActivarLoad();

    setTimeout(function () {
        var parametros = "anticipo=" + anticipo + "&documento=" + documento + "&cliente=" + cliente + "&estado=" + estado + "&fechad=" + fechad + "&fechah=" + fechah + "&dias=" + dias;
        var datos = LlamarAjax('Caja','opcion=ModalAnticipos&'+ parametros);
        DesactivarLoad();
        var datajson = JSON.parse(datos);
        $('#tablamodalanticipos').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "anticipo" },
                { "data": "documento" },
                { "data": "cliente" },
                { "data": "fecha" },
                { "data": "monto", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "saldo", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "estado" },
                { "data": "usuario" },
                { "data": "anulado" },
                { "data": "fechaenvio" },
            ],

            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy','colvis'
            ]
        });
    }, 15);
}

$("#tablamodalanticipos > tbody").on("dblclick", "tr", function () {
    var row = $(this).parents("td").context.cells;
    var anticipo = row[0].innerText;
    var cliente = row[1].innerText;
    BuscarAnticipo(anticipo, cliente); 
});


$("#tablamodalclienteAnt > tbody").on("click", "tr", function (e) {
    var row = $(this).parents("td").context.cells;
    var numero = row[1].innerText;
    $("#modalClientesAnt").modal("hide");
    $("#Cliente").val(numero);
    BuscarCliente(numero, 0);
});

function ValidarCliente(documento) {

    if (ErrorCli == 0) {
        if ($.trim(DocumentoCli) != "") {
            if (DocumentoCli != documento) {
                DocumentoCli = "";
                Limpiar();
            }
        }
    } else {
        ErrorCli = 0;
    }
    
}


function BuscarCliente(documento, recibo) {

        
    NRecibo = recibo;
    $("#Recibo").html(recibo);

    if ($.trim(documento) == "")
        return false;

    if (DocumentoCli == documento)
        return false;

    DocumentoCli = documento;

    ActivarLoad();

    setTimeout(function () {

        var parametros = "documento=" + documento;
        var datos = LlamarAjax('Cotizacion','opcion=BuscarCliente&'+ parametros);
        datos = datos.split("|");
        DesactivarLoad();
        
        if (datos[0] == "0") {
            eval("data=" + datos[1]);
            IdCliente = data[0].id * 1;

            CorreoEmpresa = data[0].email;

            $("#ChNombre").val(data[0].nombrecompleto);
            $("#ChNombre").prop("inicial", data[0].nombrecompleto);
            $("#ChCedula").val(data[0].documento);
            $("#ChCedula").prop("inicial", data[0].documento);
            $("#ChTelefono").val((data[0].telefono ? data[0].telefono : "") + ", " + (data[0].celular ? data[0].celular : ""));
            $("#ChTelefono").prop("inicial", (data[0].telefono ? data[0].telefono : "") + ", " + (data[0].celular ? data[0].celular : ""));
            $("#ChDireccion").val(data[0].direccion);
            $("#ChDireccion").prop("inicial", data[0].direccion);
            Cli_Correo = (data[0].email ? data[0].email : "");
            $("#NombreCliente").html(data[0].nombrecompleto + "<br>" + data[0].direccion + " (" + data[0].ciudad + ") Tel: " + (data[0].telefono ? data[0].telefono : "") + ", " + (data[0].celular ? data[0].celular : "") + ", " + Cli_Correo);
            NombreCliente = data[0].nombrecompleto;
                        
        } else {
            ErrorCli = 1;
            $("#Cliente").select();
            $("#Cliente").focus();
            swal("Acción Cancelada", "Cliente no Registrado", "warning");
            IdCliente = 0;
        }
        
    }, 15);
}


$("#MTablaMovimientoAnti > tbody").on("dblclick", "tr", function () {
    var row = $(this).parents("td").context.cells;
    Movimiento = row[0].innerText;
    var descripcion = "<b>Fecha:</b> " + row[1].innerText + ", <b>Referencia:</b> " + row[2].innerText + ", <b>Descripción:</b> " + row[3].innerText + ", <b>Monto:</b> " + row[5].innerText;
    $("#Detalle_Movimiento").html(descripcion);
    Consignado = NumeroDecimal(row[5].innerText);
    $("#Diferencia, #Anticipo, #Consignacion").val(formato_numero(Consignado, 0,_CD,_CM,""));
    $("#con_cuenta").val($("#MCuenta").val()).trigger("change");
    $("#con_numero").val(row[2].innerText);
    $("#con_fecha").val(row[1].innerText);
    $("#con_tipo").val("E").trigger("change");
    $("#ModalMovBancarioAnti").modal("hide");
    CargarModalConsig();
});

function VerEstadoCuenta() {
    EstadoCuenta(IdCliente, NombreCliente, CorreoEmpresa, TipoCliente, PlazoPago, CorreoEmpresa);
}

$('select').select2();
DesactivarLoad();
