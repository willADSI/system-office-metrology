﻿
d = new Date();
month = d.getMonth() + 1;
day = d.getDate();

var Anio = $.trim(d.getFullYear());
Anio = $.trim(Anio.substr(2, 2))


output = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-01';

output2 = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-' +
    (day < 10 ? '0' : '') + day;

$("#FechaF").val(output2);

$("#Cliente").html(CargarCombo(9, 1));
$("#Asesor").html(CargarCombo(13, 1));
$("#Departamento").html(CargarCombo(11, 1));
$("#Ciudad").html(CargarCombo(12, 1));

var nfilas = 0;
var OpcionReporte = 1;


function CambioTitulo(tipo) {
    
}

function Reporte(opcion) {

    var opcionreporte = $('input:radio[name=opcion]:checked').val()*1;
    if (!opcionreporte)
        opcionreporte = 0;



    var dias = ValidarTraduccion("días");
    switch (opcionreporte) {
        case 1:
        case 4:
            $("#opc_car1").html("1-7 " + dias);
            $("#opc_car2").html("8-15 " + dias);
            $("#opc_car3").html("16-21 " + dias);
            $("#opc_car4").html("22-30 " + dias);
            $("#opc_car5").html("31-37 " + dias);
            $("#opc_car6").html("Más 37 " + dias);
            break;
        case 2:
        case 5:
            $("#opc_car1").html("1-15 " + dias);
            $("#opc_car2").html("16-30 " + dias);
            $("#opc_car3").html("31-45 " + dias);
            $("#opc_car4").html("46-60 " + dias);
            $("#opc_car5").html("61-75 " + dias);
            $("#opc_car6").html("Más 75 " + dias);
            break;
        case 3:
        case 6:
            $("#opc_car1").html("1-30 " + dias);
            $("#opc_car2").html("31-60 " + dias);
            $("#opc_car3").html("61-90 " + dias);
            $("#opc_car4").html("91-120 " + dias);
            $("#opc_car5").html("121-150 " + dias);
            $("#opc_car6").html("Más 150 " + dias);
            break;
    }
    
    var cliente = $("#Cliente").val() * 1;
    var factura = $("#Factura").val();
    var departamento = $("#Departamento").val() * 1;
    var ciudad = $("#Ciudad").val() * 1;
    var estado = $("#Estado").val();
    var asesor = $("#Asesor").val() * 1;
    var fechaf = $("#FechaF").val();
    var fechai = $("#FechaI").val();
    var estadocar = $("#EstadoCar").val() * 1;
    var orden = $("#Orden").val();
        
    if (opcionreporte == 0) {
        swal("Alerta", "Debe de seleccionar una opción del reporte", "warning");
        return false;
    }

    if (fechaf == "") {
        $("#FechaF").focus();
        swal("Alerta", "Debe ingresar la fecha hasta", "warning");
        return false;
    }

          
        
    ActivarLoad();
    setTimeout(function () {
        var parametros = "fechaf=" + fechaf + "&cliente=" + cliente + "&asesor=" + asesor + "&departamento=" + departamento + "&ciudad=" + ciudad + "&estado=" + estado +
            "&factura=" + factura + "&opcionreporte=" + opcionreporte + "&fechai=" + fechai + "&opcion=" + opcion + "&estadocar=" + estadocar + "&orden=" + orden;
        var datos = LlamarAjax("Caja","opcion=ReporteCartera&"+ parametros);
        datos = datos.split("|");
        if (datos[0] == "0") {
            switch (opcion) {
                case 1:
                    CargarTabla(datos[1]);
                    break;
                case 2:
                    window.open(url + "DocumPDF/" + datos[1]);
                    break;
            }
        } else
            swal("Alerta", datos[1], "warning");
        DesactivarLoad();
    }, 15);
}

function CargarTabla(datos) {
    if (datos != "[]") {
        var datajson = JSON.parse(datos);
        $('#tablaconsulta').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "cliente" },
                { "data": "departamento" },
                { "data": "ciudad" },
                { "data": "direccion" },
                { "data": "telefono" },
                { "data": "email" },
                { "data": "factura" },
                { "data": "fecha" },
                { "data": "vencimiento" },
                { "data": "dias" },
                { "data": "estado" },
                { "data": "asesor" },
                { "data": "total", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "abonado", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "valoractual", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "opcion1", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "opcion2", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "opcion3", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "opcion4", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "opcion5", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "opcion6", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
            ],

            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy', 'colvis'
            ]
        });
    }

}


tb = $('input');
$(tb).keypress(enter2tab);

tb = $('select');
$(tb).keypress(enter2tab);

function enter2tab(e) {
    id = $(this).attr("id");
    if ((e.keyCode == 13) && ((id != "Codigo") || (id != "CodigoB"))) {
        cb = parseInt($(this).attr('tabindex'));
        if ($(':input[tabindex=\'' + (cb + 1) + '\']') != null) {
            $(':input[tabindex=\'' + (cb + 1) + '\']').focus();
            $(':input[tabindex=\'' + (cb + 1) + '\']').select();
            e.preventDefault();
            return false;
        }
    }
}

$('select').select2();
DesactivarLoad();
