﻿d = new Date();
month = d.getMonth() + 1;
day = d.getDate();

var ctbarra = document.getElementById('myCharBarra').getContext('2d');
var ctbarra2 = document.getElementById('myCharBarra2').getContext('2d');
var ctbarra3 = document.getElementById('myCharBarra3').getContext('2d');

var DataPunto;
var TablaDt = null;
var mygraficammo = null;

var Anio = $.trim(d.getFullYear());
Anio = $.trim(Anio.substr(2, 2))


output = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-01';

output2 = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-' +
    (day < 10 ? '0' : '') + day;

$("#CFechaDesde, #CIFechaDesde").val(output);
$("#CFechaHasta, #CIFechaHasta").val(output2);

$("#CMagnitud").html(CargarCombo(1, 1));
$("#CEquipo").html(CargarCombo(2, 1));
$("#CMarca").html(CargarCombo(3, 1));
$("#CCliente").html(CargarCombo(9, 1));
$("#CModelo").html(CargarCombo(5, 1, "", "-1"));
$("#CIntervalo").html(CargarCombo(6, 1, "", "-1"));
$("#CUsuario, #CAseCome, #CTecRepo, #CTecCer").html(CargarCombo(13, 1));

tb = $('input');
$(tb).keypress(enter2tab);

tb = $('select');
$(tb).keypress(enter2tab);

function enter2tab(e) {
    if (e.keyCode == 13) {
        id = $(this).attr("id");

        cb = parseInt($(this).attr('tabindex'));

        if ($(':input[tabindex=\'' + (cb + 1) + '\']') != null) {
            $(':input[tabindex=\'' + (cb + 1) + '\']').focus();
            $(':input[tabindex=\'' + (cb + 1) + '\']').select();
            e.preventDefault();

            return false;
        }

        if ($(':select[tabindex=\'' + (cb + 1) + '\']') != null) {
            console.log("1");
            $(':select[tabindex=\'' + (cb + 1) + '\']').focus();
            $(':select[tabindex=\'' + (cb + 1) + '\']').select();
            e.preventDefault();

            return false;
        }
    }
}

function ReemplazarDetalle(detalle) {
    detalle = $.trim(detalle);
    console.log(detalle);
    if ($.trim(detalle) == "")
        return "";
    return detalle.replace(/<br>/g, ' - ').replace(/<BR>/g, ' - ');
}


function DeltalleTable(d) {
    console.log(d);
    var detalle = "<table width='1400px' boder='1'>";
    detalle += "<tr><th width='50px' class='bg-celeste'>INGRESO:</th><td width='500px'>" + ReemplazarDetalle(d.ingreso) + "</td>";
    detalle += "<th width='50px' class='bg-celeste'>REMISION:</th><td width='500px'>" + ReemplazarDetalle(d.remision) + "</td><td rowspan='16' width='400px'>" + $.trim(d.imagen).replace('80px', '100%') + "</td></tr>";
    detalle += "<tr><th width='50px' class='bg-celeste'>Ident_Equipo:</th><td width='500px'>" + ReemplazarDetalle(d.ident_equipo) + "</td>";
    detalle += "<th width='50px' class='bg-celeste'>OT<br>Plantilla:</th><td width='500px'>" + ReemplazarDetalle(d.ot) + "<br>" + d.plantilla + "</td></tr>";
    detalle += "<tr><th class='bg-celeste'>CLIENTE:</th><td>" + d.cliente + "</td>";
    detalle += "<th class='bg-celeste'>GARANTIA<br>SITIO<br>CONVENIO</th><td>" + d.garantia + "</td></tr>";
    detalle += "<tr><th class='bg-celeste'>ACCESORIOS:</th><td>" + d.accesorio + "</td>";
    detalle += "<th class='bg-celeste'>OBSERVACION:</th><td>" + d.observacioning + "</td></tr>";
    detalle += "<tr><th class='bg-celeste'>MAGNITUD<br>EQUIPO<br>MARCA<br>MODELO:</th><td>" + d.equipo + "</td>";
    detalle += "<th class='bg-celeste'>RANGO<br>SERIE:</th><td>" + d.rango + "</td></tr>";
    detalle += "<tr><th class='bg-celeste'>FEC.APRO.COT:</th><td>" + ReemplazarDetalle(d.fechaaprocoti) + "</td>";
    detalle += "<th class='bg-celeste'>FEC.APRO.AJUS:</th><td>" + ReemplazarDetalle(d.fechaaproajus) + "</td></tr>";
    detalle += "<tr><th class='bg-celeste'>TIEMPO.LAB:</th><td>" + ReemplazarDetalle(d.tiempolab) + "</td>";
    detalle += "<th class='bg-celeste'>TIEMPO.APRO:</th><td>" + ReemplazarDetalle(d.tiempoapro) + "</td></tr>";
    detalle += "<tr><th class='bg-celeste'>DIAS.LAB:</th><td>" + ReemplazarDetalle(d.diaapro) + "</td>";
    detalle += "<th class='bg-celeste'>FEC.ENTREGA:</th><td>" + ReemplazarDetalle(d.fechaentrega) + "</td></tr>";
    detalle += "<tr><th class='bg-celeste'>SOLICITUD:</th><td>" + ReemplazarDetalle(d.solicitud) + "</td>";
    detalle += "<th class='bg-celeste'>COTIZACION:</th><td>" + ReemplazarDetalle(d.cotizado) + "</td></tr>";
    detalle += "<tr><th class='bg-celeste'>RECIBIDO.LAB:</th><td>" + ReemplazarDetalle(d.recibidolab) + "</td>";
    detalle += "<th class='bg-celeste'>REPORTADO</th><td>" + ReemplazarDetalle(d.reportado) + "</td></tr>";
    detalle += "<tr><th class='bg-celeste'>INFORME:</th><td>" + ReemplazarDetalle(d.informe) + "</td>";
    detalle += "<th class='bg-celeste'>CERTIFICADO:</th><td>" + ReemplazarDetalle(d.certificado) + "</td></tr>";
    detalle += "<tr><th class='bg-celeste'>INFORME.TERCE:</th><td>" + ReemplazarDetalle(d.informetercerizado) + "</td>";
    detalle += "<th class='bg-celeste'>NO.AUTORIZADO:</th><td>" + ReemplazarDetalle(d.noautorizado) + "</td></tr>";
    detalle += "<tr><th class='bg-celeste'>RECIBIDO.COM:</th><td>" + ReemplazarDetalle(d.recibidocom) + "</td>";
    detalle += "<th class='bg-celeste'>ORDEN.TERC:</th><td>" + ReemplazarDetalle(d.envtercero) + "</td></tr>";
    detalle += "<tr><th class='bg-celeste'>SALIDA:</th><td>" + ReemplazarDetalle(d.salida) + "</td>";
    detalle += "<th class='bg-celeste'>RECI.SALIDA:</th><td>" + ReemplazarDetalle(d.rectercero) + "</td></tr>";
    detalle += "<tr><th class='bg-celeste'>ORD.COMP.CLIE:</th><td>" + ReemplazarDetalle(d.orden) + "</td>";
    detalle += "<th class='bg-celeste'>FACTURA:</th><td>" + ReemplazarDetalle(d.facturado) + "</td></tr>";
    detalle += "<tr><th class='bg-celeste'>PAGADO:</th><td>" + ReemplazarDetalle(d.pagado) + "</td>";
    detalle += "<th class='bg-celeste'>DEVOLUCION:</th><td>" + ReemplazarDetalle(d.devolucion) + "</td></tr>";
    detalle += "<tr><th class='bg-celeste'>ENTREGADO:</th><td>" + ReemplazarDetalle(d.entregado) + "</td>";
    detalle += "<th class='bg-celeste'>ENCUESTA:</th><td>" + ReemplazarDetalle(d.encuesta) + "</td></tr>";

    detalle += "</table>"
    return detalle;
}

function ConsularIngresos() {


    var magnitud = $("#CMagnitud").val() * 1;
    var equipo = $("#CEquipo").val();
    var modelo = $("#CModelo").val();
    var marca = $("#CMarca").val() * 1;
    var intervalo = $("#CIntervalo").val();
    var ingreso = $("#CIngreso").val();
    var serie = $.trim($("#CSerie").val());
    var idequipo = $.trim($("#CIdEquipo").val());

    var remision = $("#CRemision").val() * 1;
    var cotizacion = $("#CSerie").val() * 1;
    var cliente = $("#CCliente").val() * 1;
    var usuario = $("#CUsuario").val() * 1;

    var asecome = $("#CAseCome").val() * 1;
    var tecrep = $("#CTecRepo").val() * 1;
    var teccer = $("#CTecCer").val() * 1;

    var estado = $("#CEstado").val();
    var fechad = $("#CFechaDesde").val();
    var fechah = $("#CFechaHasta").val();
    var fechaentrega = $("#CFechaEntrega").val();
    var operacion = 0;
    if ($("#Operacion1").prop("checked"))
        operacion = 1;

    var vcotizado = ($("#VCotizado").prop("checked") ? 1 : 0);
    var vrecibidolab = ($("#VRecibidoLAb").prop("checked") ? 1 : 0);
    var vreportado = ($("#VReportado").prop("checked") ? 1 : 0);
    var venvtercero = ($("#VEnvTercero").prop("checked") ? 1 : 0);
    var vrectercero = ($("#VRecTercero").prop("checked") ? 1 : 0);
    var vcertificado = ($("#VCertificado").prop("checked") ? 1 : 0);
    var vordencompra = ($("#VOrdenCompra").prop("checked") ? 1 : 0);
    var vrecibidoing = ($("#VRecibidoIng").prop("checked") ? 1 : 0);
    var vfacturado = ($("#VFacturado").prop("checked") ? 1 : 0);
    var vdevolucion = ($("#VDevolucion").prop("checked") ? 1 : 0);
    var ventregado = ($("#VEntregado").prop("checked") ? 1 : 0);
    var vencuesta = ($("#VEncuesta").prop("checked") ? 1 : 0);


    var vncotizado = ($("#VNCotizado").prop("checked") ? 1 : 0);
    var vnrecibidolab = ($("#VNRecibidoLAb").prop("checked") ? 1 : 0);
    var vnreportado = ($("#VNReportado").prop("checked") ? 1 : 0);
    var vnenvtercero = ($("#VNEnvTercero").prop("checked") ? 1 : 0);
    var vnrectercero = ($("#VNRecTercero").prop("checked") ? 1 : 0);
    var vncertificado = ($("#VNCertificado").prop("checked") ? 1 : 0);
    var vnordencompra = ($("#VNOrdenCompra").prop("checked") ? 1 : 0);
    var vnrecibidoing = ($("#VNRecibidoIng").prop("checked") ? 1 : 0);
    var vnfacturado = ($("#VNFacturado").prop("checked") ? 1 : 0);
    var vndevolucion = ($("#VNDevolucion").prop("checked") ? 1 : 0);
    var vnentregado = ($("#VNEntregado").prop("checked") ? 1 : 0);
    var vnencuesta = ($("#VNEncuesta").prop("checked") ? 1 : 0);

    var graficar = $("#CGraficar").val() * 1;

    if (fechad == "" && fechah == "") {
        swal("", ValidarTraduccion("debe de ingresar una fecha o números de días"), "error");
        return false;
    }

    ActivarLoad();
    setTimeout(function () {
        var parametros = "remision=" + remision + "&cotizacion=" + cotizacion + "&cliente=" + cliente + "&ingresos=" + ingreso + "&estado=" + estado +
            "&fechad=" + fechad + "&fechah=" + fechah + "&magnitud=" + magnitud + "&equipo=" + equipo + "&marca=" + marca + "&modelo=" + modelo +
            "&intervalo=" + intervalo + "&serie=" + serie + "&usuario=" + usuario + "&asecome=" + asecome + "&tecrep=" + tecrep + "&teccer=" + teccer +
            "&vcotizado=" + vcotizado + "&vrecibidolab=" + vrecibidolab + "&vreportado=" + vreportado + "&venvtercero=" + venvtercero +
            "&vrectercero=" + vrectercero + "&vcertificado=" + vcertificado + "&vordencompra=" + vordencompra + "&vrecibidoing=" + vrecibidoing +
            "&vfacturado=" + vfacturado + "&vdevolucion=" + vdevolucion + "&ventregado=" + ventregado + "&vencuesta=" + vencuesta + "&fechaentrega=" + fechaentrega +
            "&vncotizado=" + vncotizado + "&vnrecibidolab=" + vnrecibidolab + "&vnreportado=" + vnreportado + "&vnenvtercero=" + vnenvtercero +
            "&vnrectercero=" + vnrectercero + "&vncertificado=" + vncertificado + "&vnordencompra=" + vnordencompra + "&vnrecibidoing=" + vnrecibidoing +
            "&vnfacturado=" + vnfacturado + "&vndevolucion=" + vndevolucion + "&vnentregado=" + vnentregado + "&vnencuesta=" + vnencuesta + "&idequipo=" + idequipo + "&operacion=" + operacion;
        var datos = LlamarAjax("Laboratorio","opcion=ConsultarIngresos&" + parametros);
        DesactivarLoad();
        if (graficar == 1) {
            GraficarBarra(parametros);
            GraficarBarra2(parametros);
        } else {
            if (window.myBarra2 != undefined) {
                window.myBarra2.destroy();
            }
            if (window.myBarra != undefined) {
                window.myBarra.destroy();
            }
        }
        var datajson = JSON.parse(datos);

        if (TablaDt != null)
            TablaDt.colReorder.reset();

        TablaDt = $('#TablaIngresos').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                {
                    "className": 'details-control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": ''
                },
                { "data": "imagen" },
                { "data": "ingreso", "className": "text-XX" },
                { "data": "remision" },
                { "data": "garantia" },
                { "data": "accesorio", "className": "text-XX8" },
                { "data": "observacioning", "className": "text-XX8" },
                { "data": "cliente" },
                { "data": "equipo" },
                { "data": "rango" },
                { "data": "plantilla" },
                { "data": "fechaaprocoti" },
                { "data": "fechaaproajus" },
                { "data": "tiempolab", "className": "text-center" },
                { "data": "tiempoapro", "className": "text-center" },
                { "data": "diaapro", "className": "text-center" },
                { "data": "fechaentrega", "className": "text-center" },
                { "data": "programacion", "className": "text-center" },
                { "data": "solicitud", "className": "text-center" },
                { "data": "cotizado", "className": "text-center" },
                { "data": "recibidolab", "className": "text-center" },
                { "data": "recibidocom", "className": "text-center" },
                { "data": "operaciones", "className": "text-center" },
                { "data": "reportado", "className": "text-center" },
                { "data": "informe", "className": "text-center" },
                { "data": "informetercerizado", "className": "text-center" },
                { "data": "noautorizado", "className": "text-center" },
                { "data": "envtercero", "className": "text-center" },
                { "data": "salida", "className": "text-center" },
                { "data": "rectercero", "className": "text-center" },
                { "data": "certificado", "className": "text-center" },
                { "data": "recingreso", "className": "text-center" },
                { "data": "orden", "className": "text-center" },
                { "data": "facturado", "className": "text-center" },
                { "data": "pagado", "className": "text-center" },
                { "data": "devolucion", "className": "text-center" },
                { "data": "entregado", "className": "text-center" },
                { "data": "encuesta", "className": "text-center" }
            ],

            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy', 'colvis'
            ],
            "order": [[2, 'asc']],
            "colReorder": true
        });

        TablaDt.on('draw', function () {
            $.each(detailRows, function (i, id) {
                $('#' + id + ' td.details-control').trigger('click');
            });
        });


    }, 15);
}


var detailRows = [];

$('#TablaIngresos tbody').on('click', 'tr td.details-control', function () {
    var tr = $(this).closest('tr');
    var row = TablaDt.row(tr);
    var idx = $.inArray(tr.attr('id'), detailRows);

    if (row.child.isShown()) {
        tr.removeClass('details');
        row.child.hide();

        // Remove from the 'open' array
        detailRows.splice(idx, 1);
    }
    else {
        tr.addClass('details');
        row.child(DeltalleTable(row.data())).show();

        // Add to the 'open' array
        if (idx === -1) {
            detailRows.push(tr.attr('id'));
        }
    }
});



function VerDetalle(opcion, ingreso) {
    switch (opcion) {
        case 1:
            $("#titulodetalle").html("COTIZACIONES DEL INGRESO NRO " + ingreso);
            break;
        case 2:
            $("#titulodetalle").html("RECIBO EN LABORATORIO DEL INGRESO NRO " + ingreso);
            break;
        case 3:
            $("#titulodetalle").html("REPORTES DE LABORATORIO DEL INGRESO NRO " + ingreso);
            break;
        case 4:
            $("#titulodetalle").html("ÓRDENES DE COMPRA TERCERIZADA DEL INGRESO NRO " + ingreso);
            break;
        case 6:
            $("#titulodetalle").html("CERTIFICADO DEL INGRESO NRO " + ingreso);
            break;
        case 7:
            $("#titulodetalle").html("RECIBO EN LOGISTICA DEL INGRESO NRO " + ingreso);
            break;
        case 8:
            $("#titulodetalle").html("ORDEN DE COMPRA DEL CLIENTE DEL INGRESO NRO " + ingreso);
            break;
        case 9:
            $("#titulodetalle").html("FACTURAS DEL INGRESO NRO " + ingreso);
            break;
        case 10:
            $("#titulodetalle").html("DEVOLUCION DEL INGRESO NRO " + ingreso);
            break;
        case 11:
            $("#titulodetalle").html("ENTREGA DEL INGRESO NRO " + ingreso);
            break;
        case 12:
            $("#titulodetalle").html("INFORMES TÉCNICOS DEL INGRESO NRO " + ingreso);
            break;
        case 13:
            RecibirIngreso(ingreso)
            return false;
            break;
        case 14:
            $("#titulodetalle").html("RECIBIDO EN COMERCIAL EL INGRESO NRO " + ingreso);
            break;
        case 15:
            $("#titulodetalle").html("SALIDA A TERCERIZAR DEL INGRESO NRO " + ingreso);
            break;
        case 16:
            $("#titulodetalle").html("SOLICITUDES DEL INGRESO NRO " + ingreso);
            break;
        case 17:
            $("#titulodetalle").html("INFORMES TÉCNICOS TERCERIZADOS DEL INGRESO NRO " + ingreso);
            break;
    }

    var datos = LlamarAjax("Laboratorio","opcion=DetalleIngreso&opciones=" + opcion + "&ingreso=" + ingreso);
    $("#detallemovimiento").html(datos);

    $("#modalDetalle").modal("show");
}

function ImprimirCotizacion(cotizacion) {

    ActivarLoad();
    setTimeout(function () {
        var parametros = "cotizacion=" + cotizacion;
        var datos = LlamarAjax("Cotizacion/RpCotizacion", parametros);
        DesactivarLoad();
        datos = datos.split("|");
        if (datos[0] == "0") {
            window.open("DocumPDF/" + datos[1]);
        } else {
            swal("", ValidarTraduccion(datos[1]), "warning");
        }

    }, 15);
}

function ImprimirRemision(remision, estado, tipo) {
    ActivarLoad();
    setTimeout(function () {
        var parametros = "remision=" + remision;
        var datos = LlamarAjax("Cotizacion/RpRemision", parametros);
        DesactivarLoad();
        datos = datos.split("|");
        if (datos[0] == "0") {
            if (tipo == 0)
                window.open("DocumPDF/" + datos[1]);
            else {
                $("#resultadopdfremision").attr("src", "DocumPDF/" + datos[1]);
                Pdf_Remision = datos[1];
            }

        } else {
            swal("", ValidarTraduccion(datos[1]), "warning");
        }

    }, 15);
}

function ImprimirCertificado(numero) {
    window.open(url_cliente + "Certificados/" + numero + ".pdf");
}

function ImprimirInformeTerce(numero) {
    window.open(url_cliente + "InformesTercerizados/" + numero + ".pdf");
}

function VerAprobacion(cotizacion) {
    var datos = LlamarAjax("Cotizacion/UsurioAprobarCotizacion", "cotizacion=" + cotizacion);
    var data = JSON.parse(datos);
    if (data[0].idusuario * 1 > 0) {
        if (data[0].archivo_aprueba == "1")
            window.open(url_cliente + "CotizacionAprobada/" + cotizacion + ".pdf");
        else {
            var orden = data[0].archivo_aprueba.split("|");
            window.open(url_cliente + "OrdenCompra/" + orden[1] + ".pdf");
        }
    }
}

function VerAprobacionReporte(reporte) {
    window.open(url_cliente + "ReportesAprobados/" + reporte + ".pdf");
}




function ImprimirOrden(orden) {
    window.open(url_cliente + "OrdenCompra/" + orden + ".pdf");
}

function LlamarFotoDet(ingreso, fotos) {
    if (fotos == 0)
        return false
    CargarModalFoto(ingreso, fotos, 1);
}

function ImprimirDevolucionFirma(devolucion) {
    window.open(url_cliente + "imagenes/DevoFirma/" + devolucion + ".pdf");
}

function VerGuia(guia, pagina) {
    window.open(pagina + guia);
}

function VerRemision(remision) {
    window.open(url_cliente + "RemisionCliente/" + remision + ".pdf");
}

function ImprimirDevolucion(numero) {
    ActivarLoad();
    setTimeout(function () {
        var parametros = "devolucion=" + numero;
        var datos = LlamarAjax("Cotizacion/RpDevolucion", parametros);
        DesactivarLoad();
        datos = datos.split("|");
        if (datos[0] == "0") {
            window.open("DocumPDF/" + datos[1]);
        } else {
            swal("", ValidarTraduccion(datos[1]), "warning");
        }
    }, 15);
}

function RecibirIngreso(ingreso) {
    var datos = LlamarAjax("Pagina/TablaRecibirIngreso", "ingreso=" + ingreso).split("|");
    if (datos[0] != "[]") {
        var data = JSON.parse(datos[0]);
        var resultado = "";
        for (var x = 0; x < data.length; x++) {
            resultado += "<tr>" +
                "<td>" + data[x].pregunta + "</td>" +
                "<td>" + data[x].respuesta + "</td></tr>";
        }

        $("#TBEncuesta").html(resultado);
        $("#ObservacionRe").val(datos[1]);
        $("#NECIngreso").html(ingreso + ", Fecha de encuesta: " + data[0].fecha);
        $("#modalEncuestaVer").modal("show");
    } else {
        swal("Acción Cancelada", "El cliente no llenó la encuesta de satisfacción", "warning");
    }
}

function ImprimirFactura(factura) {
    ActivarLoad();
    setTimeout(function () {
        var parametros = "factura=" + factura;
        var datos = LlamarAjax("Facturacion/RpFactura", parametros);
        DesactivarLoad();
        datos = datos.split("|");
        if (datos[0] == "0") {
            window.open("DocumPDF/" + datos[1]);
        } else {
            swal("", ValidarTraduccion(datos[1]), "warning");
        }
    }, 15);
}

function ImprimirSolicitud(solicitud) {
    var parametros = "solicitud=" + solicitud;
    ActivarLoad();
    setTimeout(function () {
        var datos = LlamarAjax("Solicitud/RpSolicitud", parametros);
        DesactivarLoad();
        datos = datos.split("|");
        if (datos[0] == "0") {
            window.open(url_cliente + "DocumPDF/" + datos[1]);
        } else {
            swal("", datos[1], "warning");
        }
    }, 15);
}

function ImprimirOrdenTercero(numero) {
    ActivarLoad();
    setTimeout(function () {
        var parametros = "orden=" + numero;
        var datos = LlamarAjax("Facturacion/RpOrdenCompra", parametros);
        DesactivarLoad();
        datos = datos.split("|");
        if (datos[0] == "0") {
            window.open(url_cliente + "DocumPDF/" + datos[1]);
        } else {
            swal("", ValidarTraduccion(datos[1]), "warning");
        }
    }, 15);
}

function ImprimirSalida(numero) {
    ActivarLoad();
    setTimeout(function () {
        var parametros = "salida=" + numero;
        var datos = LlamarAjax("Salida/RpSalida", parametros);
        DesactivarLoad();
        datos = datos.split("|");
        if (datos[0] == "0") {
            window.open("DocumPDF/" + datos[1]);
        } else {
            swal("", ValidarTraduccion(datos[1]), "warning");
        }
    }, 15);
}

function ImprimirInforme(informe, tipo, ingreso) {
    ActivarLoad();
    setTimeout(function () {

        var parametros = "ingreso=" + ingreso + "&informe=" + informe + "&tipo=" + tipo + "&concepto=" + (tipo == 1 ? 3 : 5);
        var datos = LlamarAjax("Laboratorio/RpInformeTecnico", parametros);
        DesactivarLoad();
        datos = datos.split("|");
        if (datos[0] == "0") {
            window.open("DocumPDF/" + datos[1]);
        } else {
            swal("", ValidarTraduccion(datos[1]), "warning");
        }

    }, 15);
}

function GraficarBarra(parametros) {

    var colores = ['rgb(255, 99, 132)', 'rgb(255, 159, 64)', 'rgb(255, 205, 86)', 'rgb(75, 192, 192)', 'rgb(54, 162, 235)', 'rgb(153, 102, 255)', 'rgb(201, 203, 207)', 'rgb(104, 255, 51)', 'rgb(255, 60, 51)', 'rgb(8, 1, 0)', 'rgb(8, 89, 57)', 'rgb(105, 9, 102)'];
    var lecturas = [];
    var opciones = ['Ingresos', 'Cotizados', 'RecibidoLab', 'Reportados', 'SalidaTer', 'Certificados', 'Informes', 'Facturados', 'RecibidoIng', 'Devolucion', 'Entregados', 'Encuesta'];
    var datos = LlamarAjax("Laboratorio/Magnitudes", "graficar=1");
    var data = JSON.parse(datos);
    for (x = 0; x < data.length; x++)
        lecturas.push(data[x].descripcion);


    var config = {
        type: 'bar',
        data: {
            labels: lecturas,
            datasets: []
        },
        options: {
            responsive: true,
            title: {
                display: true,
                text: ''
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            legend: {
                position: 'top',
                label: {
                    FontSize: '20px'
                }
            }
        },
        lineAtIndex: 2
    }



    for (var p = 0; p < opciones.length; p++) {
        var newDataset = {
            label: opciones[p],
            backgroundColor: colores[p],
            borderColor: colores[p],
            data: [],
        };
        var punto;

        for (var i = 0; i < lecturas.length; i++) {
            punto = LlamarAjax("Laboratorio/GraficarIngreso", parametros + "&desmagnitud=" + lecturas[i] + "&opcion=" + opciones[p]) * 1;
            newDataset.data.push(punto);
        }
        config.data.datasets.push(newDataset);
    }

    if (window.myBarra != undefined) {
        window.myBarra.destroy();
    }

    window.myBarra = new Chart(ctbarra, config);
    window.myBarra.update();

}

function BuscarPunto(magnitud, fecha, tipo) {
    var punto = 0;

    for (var x = 0; x < DataPunto.length; x++) {
        if (DataPunto[x].magnitud == magnitud && DataPunto[x].fecha == fecha) {
            if (tipo == 1)
                punto = DataPunto[x].valor;
            else
                punto = DataPunto[x].ingreso;
            return punto;

        }
    }
    return punto;
}

function GraficarOperacion() {

    var cliente = $("#CCliente").val() * 1;
    var fechad = $("#CIFechaDesde").val();
    var fechah = $("#CIFechaHasta").val();
    var todas = ($("#TodasMagnitud").prop("checked") ? 1 : 0);

    var opcion = $("#TipoOperacion").val();

    var parametros = "cliente=" + cliente + "&fechad=" + fechad + "&fechah=" + fechah + "&todas=" + todas;

    var colores = ['rgb(255, 99, 132)', 'rgb(255, 159, 64)', 'rgb(255, 205, 86)', 'rgb(75, 192, 192)', 'rgb(54, 162, 235)', 'rgb(153, 102, 255)', 'rgb(201, 203, 207)', 'rgb(104, 255, 51)', 'rgb(255, 60, 51)', 'rgb(8, 1, 0)', 'rgb(8, 89, 57)', 'rgb(105, 9, 102)'];
    var magnitudes = [];
    var fechas = [];
    if (todas == 0) {
        var datos = LlamarAjax("Laboratorio/Magnitudes", "graficar=1");
        var data = JSON.parse(datos);
        for (x = 0; x < data.length; x++) {
            magnitudes.push(data[x].descripcion);
        }
    } else
        magnitudes.push("Todas");

    var config3 = {
        type: 'bar',
        data: {
            labels: fechas,
            datasets: []
        },
        options: {
            responsive: true,
            title: {
                display: true,
                text: ''
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            legend: {
                position: 'top',
                label: {
                    FontSize: '20px'
                }
            }
        },
        lineAtIndex: 2
    }

    if (opcion == "") {
        window.myBarra3 = new Chart(ctbarra3, config3);
        window.myBarra3.update();
        return false;
    }

    datos = LlamarAjax("Laboratorio/GraficarIngresoFe", parametros);
    data = JSON.parse(datos);
    for (x = 0; x < data.length; x++) {
        fechas.push(data[x].fecha);
    }

    var tabla = "";

    DataPunto = JSON.parse(LlamarAjax("Laboratorio/GraficarIngresoMMO", parametros + "&campo=" + opcion));

    for (var p = 0; p < magnitudes.length; p++) {

        var newDataset = [{
            label: magnitudes[p],
            backgroundColor: colores[p],
            borderColor: colores[p],
            stack: 'Stack ' + p,
            data: []

        }, {
            label: 'ING-' + magnitudes[p].substr(0, 3),
            borderDash: [5, 5],
            pointRadius: 10,
            pointHoverRadius: 15,
            backgroundColor: colores[p],
            borderColor: colores[p],
            fill: false,
            stack: 'Stack ' + p,
            type: 'line',
            data: []


        }];
        if (todas == 1) {
            var newDatasetIng = {
                type: 'line',
                label: 'Ingreso',
                fill: false,
                backgroundColor: 'rgb(255, 255, 255)',
                borderColor: 'rgb(0, 0, 0)',
                data: [],
            };
        }

        var punto;

        for (var i = 0; i < fechas.length; i++) {
            punto = BuscarPunto(magnitudes[p], fechas[i], 1);
            newDataset[0].data.push(punto);
            if (todas == 0) {
                punto = BuscarPunto(magnitudes[p], fechas[i], 2);
                newDataset[1].data.push(punto);
            }

            if (p == 0) {
                if (todas == 1) {
                    punto = BuscarPunto(magnitudes[p], fechas[i], 2);
                    newDatasetIng.data.push(punto);
                }
            }

        }
        if (p == 0) {
            if (todas == 1) {
                config3.data.datasets.push(newDatasetIng);
            }
        }

        config3.data.datasets.push(newDataset[0]);
        if (todas == 0) {
            config3.data.datasets.push(newDataset[1]);
        }
    }

    if (window.myBarra3 != undefined) {
        window.myBarra3.destroy();
    }

    window.myBarra3 = new Chart(ctbarra3, config3);
    window.myBarra3.update();
}

function GraficarBarra2(parametros) {

    var colores = ['rgb(255, 99, 132)', 'rgb(255, 159, 64)', 'rgb(255, 205, 86)', 'rgb(75, 192, 192)', 'rgb(54, 162, 235)', 'rgb(153, 102, 255)', 'rgb(201, 203, 207)', 'rgb(104, 255, 51)', 'rgb(255, 60, 51)', 'rgb(8, 1, 0)', 'rgb(8, 89, 57)', 'rgb(105, 9, 102)'];
    var lecturas = ["Todas"];
    var opciones = ['Ingresos', 'Cotizados', 'RecibidoLab', 'Reportados', 'SalidaTer', 'Certificados', 'Informes', 'Facturados', 'RecibidoIng', 'Devolucion', 'Entregados', 'Encuesta'];

    var config = {
        type: 'horizontalBar',
        data: {
            labels: lecturas,
            datasets: []
        },
        options: {
            responsive: true,
            title: {
                display: true,
                text: ''
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            legend: {
                position: 'top',
                label: {
                    FontSize: '20px'
                }
            }
        },
        lineAtIndex: 2
    }

    for (var p = 0; p < opciones.length; p++) {
        var newDataset = {
            label: opciones[p],
            backgroundColor: colores[p],
            borderColor: colores[p],
            data: [],
        };
        var punto;

        for (var i = 0; i < lecturas.length; i++) {
            punto = LlamarAjax("Laboratorio/GraficarIngreso", parametros + "&desmagnitud=" + lecturas[i] + "&opcion=" + opciones[p]) * 1;
            newDataset.data.push(punto);
        }
        config.data.datasets.push(newDataset);
    }

    if (window.myBarra2 != undefined) {
        window.myBarra2.destroy();
    }

    window.myBarra2 = new Chart(ctbarra2, config);
    window.myBarra2.update();

}


function VerFotosDevolucion(devolucion, fotos) {
    CargarModalFoto(devolucion, fotos, 2);
}

function VerFotosEntrega(devolucion, fotos) {
    CargarModalFoto(devolucion, fotos, 3);
}



$('select').select2();
DesactivarLoad();
