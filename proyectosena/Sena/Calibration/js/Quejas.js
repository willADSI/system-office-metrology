﻿var DocumentoCli = "";
var IdCliente = 0;
var CorreoEmpresa = ""
var Queja = "";
var IdQueja = 0;
var Estado = "";
var TipoCliente = "";
var PlazoPago = 0;

d = new Date();
month = d.getMonth() + 1;
day = d.getDate();

var Anio = $.trim(d.getFullYear());
Anio = $.trim(Anio.substr(2, 2))
var AprobarCotiza = "";

output = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-01';

output2 = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-' +
    (day < 10 ? '0' : '') + day;

$("#CFechaDesde").val(output);
$("#CFechaHasta").val(output2);

$("#Fecha").val(output2);
$("#CCliente").html(CargarCombo(9, 1));
$("#CAsesor").html(CargarCombo(13, 1));

$("#tablamodalcliente > tbody").on("click", "tr", function (e) {
    var row = $(this).parents("td").context.cells;
    var numero = row[1].innerText;
    $("#modalClientes").modal("hide");
    $("#Cliente").val(numero);
    BuscarCliente(numero, 0);
});

function LimpiarTodo() {
    LimpiarDatos();
    $("#Cliente").val("");
    $("#Cliente").focus();
}


function LimpiarDatos() {
    DocumentoCli = "";
    IdCliente = "";
    CorreoEmpresa = ""
    Queja = "";
    IdQueja = 0;
    $("#Fecha").val(output2);
    Estado = "Temporal";
    $("#Estado").val(Estado);

    $("#Queja").val("");
    $("#AsesorReg").val("");
    $("#Estado").val("");
    $("#FechaReg").val("");
    $("#Hora").val("");
    $("#Asesor").val("");
    $("#Cargo").val("");
    $("#Descripcion").val("");
    $("#Ingresos").html("");

    $("#FechaEnv").val("");
    $("#CorreoEnv").val("");
    $("#AsesorEnv").val("");
    $("#CargoEnv").val("");

    $("#Validacion").val("");
    $("#FechaVal").val("");
    $("#AsesorVal").val("");
    $("#CargoVal").val("");

    $("#ProcedeQueja").val("--").trigger("change");
    $("#FechaEva").val("");
    $("#AsesorEva").val("");
    $("#CargoEva").val("");

    $("#Investigacion").val("");
    $("#FechaInv").val("");
    $("#AsesorInv").val("");
    $("#CargoInv").val("");

    $("#Respuesta").val("");
    $("#FechaRes").val("");
    $("#AsesorRes").val("");
    $("#CargoRes").val("");
    $("#FechaEnvRes").val("");
    $("#CorreoEnvRes").val("");
    $("#AsesorEnvRes").val("");
    $("#CargoEnvRes").val("");

    $("#Cierre").val("");
    $("#FechaCie").val("");
    $("#AsesorCie").val("");
    $("#CargoCie").val("");
    $("#FechaEnvCie").val("");
    $("#CorreoEnvCie").val("");
    $("#AsesorEnvCie").val("");
    $("#CargoEnvCie").val("");
    $("#Cliente").val("");
    $("#NombreCliente").val("");
    $("#Sede").html("");
    $("#Contacto").html("");

    $("#AsesorReg").html("");
    $("#Email").val("");
    $("#Telefonos").val("");

    $("#ArchivosVal").val("");
    $("#ArchivosInv").val("");

    for (var x = 0; x <= 5; x++) {
        $("#Opcion" + x + "1").prop("checked", "");
        $("#Opcion" + x + "2").prop("checked", "");
        $("#Opcion" + x + "3").prop("checked", "");
    }
    
}

function ValidarCliente(documento) {
    if ($.trim(DocumentoCli) != "") {
        if (DocumentoCli != documento)
            LimpiarDatos();
    }
}

function DatosContacto(contacto) {
    $("#Email").val("");
    $("#Telefonos").val("");
    if (contacto * 1 == 0)
        return false;
    var datos = LlamarAjax("Cotizacion/DatosContacto", "id=" + contacto);
    IdContacto = contacto;
    if (datos != "[]") {
        var data = JSON.parse(datos);
        CorreoEmpresa = data[0].email;
        $("#Email").val(data[0].email);
        $("#Telefonos").val(data[0].telefonos);
    }
}


function GuardarDocumento(id,tipo) {
    if (IdQueja == 0)
        return false;
        
    var documento = document.getElementById(id);
    if ($.trim(documento.value) != "") {
        var file = documento.files[0];
        var data = new FormData();
        data.append('documento', file);
        var direccion = url + "Quejas/AdjuntarQuejas";
        $.ajax({
            url: direccion,
            type: 'POST',
            contentType: false,
            data: data,
            processData: false,
            async: false,
            success: function (datos) {
                datos = datos.split("|");
                if (datos[0] == "0") {
                    var datos = LlamarAjax("Quejas/GuardarDocumentos", "Id=" + IdQueja + "&Tipo=" + tipo + "&Documento=" + datos[1]).split("|");
                    if (datos[0] == "0")
                        swal("", datos[1], "success");
                    else
                        swal("Acción Cancelada", datos[1], "warning");
                } else {
                    swal("Acción Cancelada", ValidarTraduccion("Error al cargar el archivo"), "warning");
                }
            }
        });
    }
}

function BuscarCliente(documento) {

    if ($.trim(documento) == "")
        return false;

    if (DocumentoCli == documento)
        return false;

    DocumentoCli = documento;

    var parametros = "documento=" + $.trim(documento) + "&bloquear=0";
    var datos = LlamarAjax('Cotizacion/BuscarCliente', parametros);
    datos = datos.split("|");
    if (datos[0] == "0") {
        var data = JSON.parse(datos[1]);
        IdCliente = data[0].id;
        TipoCliente = data[0].tipocliente;
        PlazoPago = data[0].plazopago;
                        
        $("#NombreCliente").val(data[0].nombrecompleto);
        $("#Contacto").html(datos[2]);
        $("#Sede").html(datos[3]);
        $("#Sede").focus();

        $("#Ingresos").html(LlamarAjax("Quejas/IngresosCliente", "cliente=" + IdCliente));

        SaldoActual(IdCliente, 1);

        $("#SaldoClienteDev").html(SaldoTotal(IdCliente));

    } else {
        DocumentoCli = "";
        $("#Cliente").val("");
        $("#Cliente").focus();
        if (datos[0] == "8")
            swal("Acción Cancelada", datos[1], "warning");
        else
            swal("Acción Cancelada", "El documento " + documento + ", no se encuentra registrado como cliente", "warning");
    }

}

function GuardaQueja() {
    if (IdCliente == 0)
        return false;
    var descripcion = $.trim($("#Descripcion").val());
    var fecha = $.trim($("#Fecha").val());
    var hora = $.trim($("#Hora").val());
    var ingresos = $("#Ingresos").val();

    var idsede = $("#Sede").val() * 1;
    var idcontacto = $("#Contacto").val() * 1;

    if (idsede == 0) {
        $("#Sede").focus();
        swal("Acción Cancelada", "Debe de seleccionar una sede del cliente", "warning");
        return false;
    }
    if (idcontacto == 0) {
        $("#Contacto").focus();
        swal("Acción Cancelada", "Debe de seleccionar un contacto del cliente", "warning");
        return false;
    }

    if (descripcion == "") {
        $("#Descripcion").focus();
        swal("Acción Cancelada", "Debe de ingresar la descripción de la queja", "warning");
        return false;
    }

    if (fecha == "") {
        $("#Fecha").focus();
        swal("Acción Cancelada", "Debe de ingresar la fecha de la queja", "warning");
        return false;
    }
        
    var parametros = "idqueja=" + IdQueja + "&cliente=" + IdCliente + " &idsede=" + idsede + " &idcontacto=" + idcontacto + " &descripcion=" + descripcion + " &fecha=" + fecha + " &hora=" + hora + " &ingresos=" + ingresos;
    var datos = LlamarAjax("Quejas/GuardarQueja", parametros).split("|");
    if (datos[0] == "0") {
        if (IdQueja == 0) {
            IdQueja = datos[3] * 1;
            Queja = datos[2] * 1;
            $("#Queja").val(Queja);
            Estado = "Registrado";
            $("#Estado").val(Estado);
            var data = JSON.parse(datos[4]);
            $("#FechaReg").val(data[0].fecha);
            $("#AsesorReg").val(data[0].asesor + " (" + data[0].cargo + ")");
            $("#Cargo").val(data[0].cargo);
            $("#Asesor").val(data[0].asesor);
            swal("", datos[1] + " " + Queja, "success");
        } else
            swal("", datos[1], "success");
    } else
        swal("Acción Cancelada", datos[1], "warning");
}

function EvaluarQueja() {

    var procede = $("#ProcedeQueja").val();


    if (IdCliente == 0)
        return false;

    if (IdQueja == 0) {
        swal("Acción Cancelada", "Debe primero Gaurdar la queja", "warning");
        return false;
    }

    if (Estado != "Evaluado-Pro" && Estado != "Evaluado-NoPro" && Estado != "Validado") {
        swal("Acción Cancelada", "No se puede validar una queja en estado " + Estado, "warning");
        return false;
    }
        
    if (procede == "--") {
        swal("Acción Cancelada", "Debe seleccionar si procede o no la queja", "warning");
        return false;
    }

    var descripcion = document.getElementsByName("DesOpcion[]");
    
    var a_descripcion= "";
    var a_opcion = "";
    var opcion = 0;
    for (var x = 0; x < descripcion.length; x++) {
        opcion = 0;
        if ($("#Opcion" + x + "1").prop("checked"))
            opcion = 1;
        else {
            if ($("#Opcion" + x + "2").prop("checked"))
                opcion = 2;
            else {
                if ($("#Opcion" + x + "3").prop("checked"))
                    opcion = 3;
            }
        }
        if (opcion == 0) {
            swal("Acción Cancelada", "Debe de seleccionar una opción del criterio " + descripcion[x].innerHTML, "warning");
            return false;
        }
        if (a_descripcion == "") {
            a_opcion += opcion;
            a_descripcion += "'" + $.trim(descripcion[x].innerHTML) + "'";
        } else {
            a_opcion += "," + opcion;
            a_descripcion += ",'" + $.trim(descripcion[x].innerHTML) + "'";
        }
    }

    var datos = LlamarAjax("Quejas/EvaluarQuejas", "procede=" + procede + "&a_descripcion= " + a_descripcion + " &a_opcion=" + a_opcion + " &Id=" + IdQueja).split("|");
    if (datos[0] == "0") {
        swal("", "Evaluación registrada con éxito", "success");
        var data = JSON.parse(datos[1]);
        Estado = data[0].estado;
        $("#Estado").val(Estado);
        $("#FechaEva").val(data[0].fecha);
        $("#AsesorEva").val(data[0].asesor);
        $("#CargoEva").val(data[0].cargo);
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }

}

function ValidarQueja() {
    if (IdCliente == 0)
        return false;

    if (IdQueja == 0) {
        swal("Acción Cancelada", "Debe primero Gaurdar la queja", "warning");
        return false;
    }

    if (Estado != "Registrado" && Estado != "Validado") {
        swal("Acción Cancelada", "No se puede validar una queja en estado " + Estado, "warning");
        return false;
    }

    var descripcion = $.trim($("#Validacion").val());
    if (descripcion == "") {
        $("#Validacion").focus();
        swal("Acción Cancelada", "Debe de ingresar la validación de la queja", "warning");
        return false;
    }

    var datos = LlamarAjax("Quejas/OperacionesQuejas", "Operacion=1&Descripcion=" + descripcion + "&Id=" + IdQueja).split("|");
    if (datos[0] == "0") {
        swal("", "Validación registrada con éxito", "success");
        var data = JSON.parse(datos[1]);
        Estado = "Validado";
        $("#Estado").val(Estado);
        $("#FechaVal").val(data[0].fecha);
        $("#AsesorVal").val(data[0].asesor);
        $("#CargoVal").val(data[0].cargo);
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
}

function InvestigarQueja() {
    if (IdCliente == 0)
        return false;

    if (IdQueja == 0) {
        swal("Acción Cancelada", "Debe primero Gaurdar la queja", "warning");
        return false;
    }

    if (Estado != "Evaluado-Pro" && Estado != "Investigado") {
        swal("Acción Cancelada", "No se puede investigar una queja en estado " + Estado, "warning");
        return false;
    }

    var descripcion = $.trim($("#Investigacion").val());
    if (descripcion == "") {
        $("#Validacion").focus();
        swal("Acción Cancelada", "Debe de ingresar la investigacion de la queja", "warning");
        return false;
    }

    var datos = LlamarAjax("Quejas/OperacionesQuejas", "Operacion=2&Descripcion=" + descripcion + "&Id=" + IdQueja).split("|");
    if (datos[0] == "0") {
        swal("", "Investigación registrada con éxito", "success");
        var data = JSON.parse(datos[1]);
        Estado = "Investigado";
        $("#Estado").val(Estado);
        $("#FechaInv").val(data[0].fecha);
        $("#AsesorInv").val(data[0].asesor);
        $("#CargoInv").val(data[0].cargo);
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
}

function ResponderQueja() {
    if (IdCliente == 0)
        return false;

    if (IdQueja == 0) {
        swal("Acción Cancelada", "Debe primero Gaurdar la queja", "warning");
        return false;
    }

    if (Estado != "Investigado" && Estado != "Respuesta") {
        swal("Acción Cancelada", "No se puede responder una queja en estado " + Estado, "warning");
        return false;
    }

    var descripcion = $.trim($("#Respuesta").val());
    if (descripcion == "") {
        $("#Respuesta").focus();
        swal("Acción Cancelada", "Debe de ingresar la respuesta de la queja", "warning");
        return false;
    }

    var datos = LlamarAjax("Quejas/OperacionesQuejas", "Operacion=3&Descripcion=" + descripcion + "&Id=" + IdQueja).split("|");
    if (datos[0] == "0") {
        swal("", "Respuesta registrada con éxito", "success");
        var data = JSON.parse(datos[1]);
        Estado = "Respuesta";
        $("#Estado").val(Estado);
        $("#FechaRes").val(data[0].fecha);
        $("#AsesorRes").val(data[0].asesor);
        $("#CargoRes").val(data[0].cargo);
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
}

function CerrarQueja() {
    if (IdCliente == 0)
        return false;

    if (IdQueja == 0) {
        swal("Acción Cancelada", "Debe primero Gaurdar la queja", "warning");
        return false;
    }

    if (Estado != "Respuesta" && Estado != "Cerrado") {
        swal("Acción Cancelada", "No se puede cerrar una queja en estado " + Estado, "warning");
        return false;
    }

    var descripcion = $.trim($("#Cierre").val());
    if (descripcion == "") {
        $("#Cierre").focus();
        swal("Acción Cancelada", "Debe de ingresar el cierre de la queja", "warning");
        return false;
    }

    var datos = LlamarAjax("Quejas/OperacionesQuejas", "Operacion=4&Descripcion=" + descripcion + "&Id=" + IdQueja).split("|");
    if (datos[0] == "0") {
        swal("", "Cierre registrado con éxito", "success");
        var data = JSON.parse(datos[1]);
        Estado = "Cerrado";
        $("#Estado").val(Estado);
        $("#FechaCie").val(data[0].fecha);
        $("#AsesorCie").val(data[0].asesor);
        $("#CargoCie").val(data[0].cargo);
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
}

function BuscarQueja(queja) {
    queja = queja * 1;
    if (Queja == queja)
        return talse;
    LimpiarDatos();
    Queja = queja;
    
    if (queja == 0)
        return false;
    var datos = LlamarAjax("Quejas/BuscarQueja", "queja=" + queja).split("|");
    if (datos[0] != "[]") {
        var data = JSON.parse(datos[0]);
        var documento = data[0].documento;
        var idsede = data[0].idsede;
        var idcontacto = data[0].idcontacto;
        IdQueja = data[0].id * 1;
        Estado = data[0].estado;
        var ingreso = data[0].ingresos.split(",");
        
        $("#Queja").val(queja);
        $("#AsesorReg").val(data[0].usuarioreg);
        $("#Estado").val(data[0].estado);
        $("#FechaReg").val(data[0].fechareg);
        $("#Fecha").val(data[0].fecha);
        $("#Hora").val(data[0].hora);
        $("#Asesor").val(data[0].usuarioreg);
        $("#Cargo").val(data[0].cargoreg);
        $("#Descripcion").val(data[0].descripcion);

        $("#FechaEnv").val(data[0].fechaenv);
        $("#CorreoEnv").val(data[0].correoenv);
        $("#AsesorEnv").val(data[0].usuarioenv);
        $("#CargoEnv").val(data[0].cargoenv);

        $("#Validacion").val(data[0].validacion);
        $("#FechaVal").val(data[0].fechaval);
        $("#AsesorVal").val(data[0].usuarioval);
        $("#CargoVal").val(data[0].cargoval);

        $("#ProcedeQueja").val(data[0].procede).trigger("change");
        $("#FechaEva").val(data[0].fechaeva);
        $("#AsesorEva").val(data[0].usuarioeva);
        $("#CargoEva").val(data[0].cargoeva);

        $("#Investigacion").val(data[0].investigacion);
        $("#FechaInv").val(data[0].fechainv);
        $("#AsesorInv").val(data[0].usuarioinv);
        $("#CargoInv").val(data[0].cargoinv);

        $("#Respuesta").val(data[0].respuesta);
        $("#FechaRes").val(data[0].fechares);
        $("#AsesorRes").val(data[0].usuariores);
        $("#CargoRes").val(data[0].cargores);
        $("#FechaEnvRes").val(data[0].fechaenvres);
        $("#CorreoEnvRes").val(data[0].correoenvres);
        $("#AsesorEnvRes").val(data[0].usuarioenvres);
        $("#CargoEnvRes").val(data[0].cargoenvres);

        $("#Cierre").val(data[0].cierre);
        $("#FechaCie").val(data[0].fechacie);
        $("#AsesorCie").val(data[0].usuariocie);
        $("#CargoCie").val(data[0].cargocie);
        $("#FechaEnvCie").val(data[0].fechaenvcie);
        $("#CorreoEnvCie").val(data[0].correoenvcie);
        $("#AsesorEnvCie").val(data[0].usuarioenvcie);
        $("#CargoEnvCie").val(data[0].cargoenvcie);
        $("#Cliente").val(documento);
        BuscarCliente(documento);
        $("#Sede").val(idsede).trigger("change");
        $("#Contacto").val(idcontacto).trigger("change");

        for (var x = 0; x < ingreso.length; x++) {
            $("#Ingresos option[value=" + ingreso[x] + "]").prop("selected", true);
        }
        $("#Ingresos").trigger("change");

        var data = JSON.parse(datos[1]);
        for (var x = 0; x < data.length; x++) {
            $("#Opcion" + x + data[x].opcion).prop("checked", "checked");
        }

    } else {
        $("#Queja").val("");
        $("#Queja").focus();
        swal("Acción Cancelada", "La queja número " + queja + " no se encuentra registrada en el sistema", "warning");
    }
}

function VerArchivos(tipo,titulo) {
    if (Queja > 0) {

        var datos = LlamarAjax("Quejas/TablaDocumentos", "Id=" + IdQueja + "&Tipo=" + tipo);
        if (datos != "[]") {
            $("#Ingresos").prop("disabled", true);
            var resultado = "";
            var data = JSON.parse(datos);
            for (var x = 0; x < data.length; x++) {
                resultado += "<tr>" +
                    "<td><a class='text-XX' href=\"javascript:window.open('Documentos/Quejas/" + (tipo == 1 ? "Validacion" : "Investigacion") + "/" + data[x].documento + "')\">" + data[x].documento + "</a></td> " +
                    "<td>" + data[x].asesor + "</td>" +
                    "<td>" + data[x].fecha + "</td></tr>";
            }
            $("#TBDocumentos").html(resultado);
            $("#DetQueja").html("Documentos de " + titulo + " de la queja número " + Queja);
            $("#ModalDocumentos").modal({ backdrop: 'static', keyboard: false }, "show");
        } else {
            swal("Acción Cancelada", "La " + titulo + " de la queja número " + Queja + " no posee documentos adjuntados", "warning");
        }
    }
}

function CerrarModal() {
    $('#Ingresos').prop('disabled', false);
}

function VerEstadoCuenta() {
    var cliente = $("#NombreCliente").val();
    EstadoCuenta(IdCliente, cliente, CorreoEmpresa, TipoCliente, PlazoPago, CorreoEmpresa);
}

function ModalEnviarCorreo(tipo) {

    if (Queja == 0)
        return false;

    var fecha = "";
    var titulo = "";
    var cliente = $("#NombreCliente").val();

    $("#eCliente").val(cliente);
    $("#eCorreo").val(CorreoEmpresa);
    $("#eDescripcion").val($("#Descripcion").val());
    $("#eObservacion").val("");
    $("#eTipoEnvio").val(tipo);

    var datos = LlamarAjax("Quejas/BuscarQueja", "queja=" + Queja).split("|");
    var data = JSON.parse(datos[0]);

    if (tipo == 1) {
        fecha = $("#FechaRes").val();
        titulo = "la respuesta";
        $("#DetEnvio").html("Enviando respuesta de la queja número " + Queja);
        $("#tipoenvio").html("Respuesta de la Respuesta");
        $("#eEnvio").val(data[0].respuesta);
    } else {
        fecha = $("#FechaCie").val();
        titulo = "el cierre";
        $("#DetEnvio").html("Enviando cierre de la queja número " + Queja);
        $("#tipoenvio").html("Cierre de la Respuesta");
        $("#eEnvio").val(data[0].cierre);
    }

    if (fecha == "") {
        swal("Acción Cancelada", "No se ha guardado " + titulo + " de la queja", "warning");
        return false
    }

    if (ValidarCorreo(CorreoEmpresa) == 2) {
        swal("Acción Cancela", "Correo inválido del contacto del cliente", "warning");
        return false;
    }

    $("#ModalEnvio").modal("show");
}

function EnviarCorreo() {
    
    var observacion = $.trim($("#eObservacion").val())
    var correo = $.trim($("#eCorreo").val());
    var tipo = $("#eTipoEnvio").val() * 1;
    a_correo = correo.split(";");
    for (var x = 0; x < a_correo.length; x++) {
        if (ValidarCorreo($.trim(a_correo[x])) == 2) {
            $("#eCorreo").focus();
            swal(ValidarTraduccion("Acción Cancelada"), ValidarTraduccion("Formato de correo onválido") + ":" + a_correo[x], "warning");
            return false;
        }
    }
    
    ActivarLoad();
    setTimeout(function () {
        var parametros = "Queja=" + Queja + "&principal=" + CorreoEmpresa + "&e_email=" + correo + "&observacion=" + observacion + "&tipo=" + tipo;
        var datos = LlamarAjax("Quejas/EnvioQuejas", parametros);
        DesactivarLoad();
        datos = datos.split("|");
        if (datos[0] == "0") {
            swal("", ValidarTraduccion(datos[1]) + " " + datos[2], "success");
            var data = JSON.parse(datos[3]);
            if (tipo == 1) {
                $("#FechaEnvRes").val(data[0].fecha);
                $("#AsesorEnvRes").val(data[0].asesor);
                $("#CargoEnvRes").val(data[0].cargo);
                $("#CorreoEnvRes").val(data[0].correo);
            } else {
                $("#FechaEnvCie").val(data[0].fecha);
                $("#AsesorEnvCie").val(data[0].asesor);
                $("#CargoEnvCie").val(data[0].cargo);
                $("#CorreoEnvCie").val(data[0].correo);
            }
            $("#ModalEnvio").modal("hide");
        } else
            swal("Acción Cancelada", datos[1], "warning");
    }, 15);
}

function EnviarAcuse() {

    if (Queja == 0)
        return false;

    if (ValidarCorreo(CorreoEmpresa) == 2) {
        swal("Acción Cancela", "Correo inválido del contacto del cliente", "warning");
        return false;
    }

    ActivarLoad();
    setTimeout(function () {
        var parametros = "Queja=" + Queja + "&principal=" + CorreoEmpresa;
        var datos = LlamarAjax("Quejas/EnvioAcuse", parametros);
        DesactivarLoad();
        datos = datos.split("|");
        if (datos[0] == "0") {
            swal("", ValidarTraduccion(datos[1]) + " " + datos[2], "success");
            var data = JSON.parse(datos[3]);
            $("#FechaEnv").val(data[0].fecha);
            $("#AsesorEnv").val(data[0].asesor);
            $("#CargoEnv").val(data[0].cargo);
            $("#CorreoEnv").val(data[0].correo);
        } else
            swal("Acción Cancelada", datos[1], "warning");
    }, 15);
    
}

function ImprimirQueja(numero) {
    if (numero * 1 == 0)
        numero = Queja;
    if (numero * 1 != 0) {
        ActivarLoad();
        setTimeout(function () {
            var parametros = "queja=" + numero;
            var datos = LlamarAjax("Quejas/RpQuejas", parametros);
            DesactivarLoad();
            datos = datos.split("|");
            if (datos[0] == "0") {
                window.open("DocumPDF/" + datos[1]);
            } else {
                swal("", ValidarTraduccion(datos[1]), "warning");
            }
        }, 15);
    }
}

function ConsularQuejas() {


    var queja = $("#CQueja").val() * 1;
    var ingreso = $("#CIngreso").val() * 1;
    var cliente = $("#CCliente").val() * 1;
    var asesor = $("#CAsesor").val() * 1;

    var acuse = $("#CAcuse").val();
    var validada = $("#CValidada").val();
    var evaluada = $("#CEvaluada").val();
    var procede = $("#CProcede").val();
    var investigada = $("#CInvestigada").val();
    var respondida = $("#CRespondida").val();
    var envrespondida = $("#CEnvRespondida").val();
    var cerrada = $("#CCerrada").val();
    var envcerrada = $("#CEnvCerrada").val();
    
    var estado = $("#CEstado").val();
    var fechad = $("#CFechaDesde").val();
    var fechah = $("#CFechaHasta").val();

    if (fechad == "" && fechah == "") {
        swal("Acción Cancelada", ValidarTraduccion("debe de ingresar una fecha o números de días"), "warning");
        return false;
    }

    ActivarLoad();
    setTimeout(function () {
        var parametros = "queja=" + queja + "&ingreso=" + ingreso + "&cliente=" + cliente + "&asesor=" + asesor + "&estado=" + estado +
            "&fechad=" + fechad + "&fechah=" + fechah + "&acuse=" + acuse + "&validada=" + validada + "&evaluada=" + evaluada + "&procede=" + procede +
            "&investigada=" + investigada + "&respondida=" + respondida + "&envrespondida=" + envrespondida + "&cerrada=" + cerrada + "&envcerrada=" + envcerrada;
        var datos = LlamarAjax("Quejas/ConsultarQuejas", parametros);
        DesactivarLoad();

        var datajson = JSON.parse(datos);
        $('#TablaQuejas').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "imprimir" },
                { "data": "queja" },
                { "data": "cliente" },
                { "data": "telefonos" },
                { "data": "descripcion" },
                { "data": "ingresos" },
                { "data": "estado" },
                { "data": "registro" },
                { "data": "acuse" },
                { "data": "validado" },
                { "data": "evaluado" },
                { "data": "procede" },
                { "data": "investigado" },
                { "data": "respuesta" },
                { "data": "envrespuesta" },
                { "data": "cerrado" },
                { "data": "envcerrado" }
            ],

            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy','colvis'
            ]
        });
    }, 15);
}

$("#TablaQuejas > tbody").on("click", "tr", function (e) {
    var row = $(this).parents("td").context.cells;
    var numero = row[1].innerText;
    $("#Queja").val(numero);
    BuscarQueja(numero);
    $('#tabquejas a[href="#tabcrear"]').tab('show')
});


$('select').select2();
DesactivarLoad();
