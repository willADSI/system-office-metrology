﻿var d = new Date();
var month = d.getMonth() + 1;
var day = d.getDate();
var DecimalesP = [20];
var IdVersion = 7;
var HoraInicio = "";
var HoraFinal = "";
var HoraCondicion = "";
var CerImpreso = 0;
var ErrorHume = 0;
var ErrorTemp = 0;
var IntervaloTiempo = null;
var FirmaCertificado = "";
var Tolerancia = 0;
var Cuadrante = 0;
var IdCliente = 0;
var SensorLectura = "";

var canvafirma = document.getElementById("padfirmar");
initPad(canvafirma);

var Solicitante = "";
var Direccion = "";
var CantLecturas = 5;
var SensorTemperatura; 

var Reproducibilidad = [0, 0, 0, 0];
var Salida = [0, 0, 0, 0];
var Interfaz = [0, 0, 0, 0];
var Longitud = [0, 0];

var PermisioEspecial = PerGeneSistema("PERMISO ADMINISTRADOR LABORATORIO");

var ctxgra = document.getElementById('myChart1').getContext('2d');
var gratemp = document.getElementById('GraTemperatura').getContext('2d');
var grahume = document.getElementById('GraHumedad').getContext('2d');
var ctdesviacion = document.getElementById('myCharDesviacion').getContext('2d');

var datastuden = JSON.parse(LlamarAjax("Laboratorio/TStuden", ""));

var COLORS = [
    '#4dc9f6',
    '#f67019',
    '#f53794',
    '#537bc4',
    '#acc236',
    '#166a8f',
    '#00a950',
    '#58595b',
    '#8549ba'
];



var Aniofull = $.trim(d.getFullYear());
var Anio = $.trim(Aniofull.substr(2, 2))
var AprobarCotiza = "";

$("#NumCertificado").html(CargarCombo(72, 1, "", "'DID-162 SH','DID-162 SCH'"));
$("#Sensor").html(CargarCombo(74, 0, "", "6"));
$("#NumCertificado").val(22).trigger("change");
$("#Puntos").val("5").trigger("change");

function CambioSensor(sensor) {
    var datos = LlamarAjax("Laboratorio/DatosSensor", "sensor=" + sensor)
    SensorTemperatura = sensor;
    SensorLectura = datos;
    if (SensorLectura == "Excel") {
        $("#documento").prop("disabled", false);
    } else {
        $("#documento").prop("disabled", true);
    }
}

$("#Sensor").trigger("change");

DatosTemperatura(6,SensorTemperatura);

output = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-01';

var FechaCal = d.getFullYear() + "-" + (month < 10 ? '0' : '') + month + "-" + (day < 10 ? "0" : "") + day;
var FechaActual = d.getFullYear() + "-" + (month < 10 ? '0' : '') + month + "-" + (day < 10 ? "0" : "") + day;

var Ingreso = 0;
var IdReporte = 0;
var IdInstrumento = 0;
var ErrorEnter = 0;
var Foto = 0;
var NroReporte = 1;
var RangoHasta = 0;
var GuardaTemp = 0;
var CorreoEmpresa = "";
var AplicarTemporal = 0;
var InfoTecIngreso = 0;
var Factor = 0;
var TipoInst = "";
var Descripcion = "";
var Resolucion = 0;
var CantRegitro = 5;
var DesPerm1 = 0;
var DesPerm2 = 0;
var Certificado = "";
var IdCalibracion = 0;
var NumCertificado = 22;
var Revision = 1;
var Foto = Foto;

var TempMax = 0;
var TempMin = 100000000;
var HumeMax = 0;
var HumeMin = 100000000;
var TempVar = 0;
var HumeVar = 0;

function FormatoSalida2(Caja, decimal) {
    FormatoSalida(Caja, decimal, NumCertificado);
}

function CambioCertificado(tipo) {
    NumCertificado = tipo;
    LimpiarDatos();
    $("#Ingreso").val("");
    
    if (tipo*1 == 2)
        $(".negativo").html("<h3>VALORES EN NEGATIVO CUANDO LA CALIBRACION ES EN SENTIDO ANTIHORARIO</h3>");
    else
        $(".negativo").html("");
}

localStorage.setItem("Ingreso", "0");

function LimpiarTodo() {
    
    localStorage.removeItem(NumCertificado + "-" + "PlaPT1" + "-" + Ingreso + "-" + Revision);
    LimpiarDatos();
    $("#Ingreso").val("");
}

function LimpiarDatos() {
    FechaCal = d.getFullYear() + "-" + (month < 10 ? '0' : '') + month + "-" + (day < 10 ? "0" : "") + day;
    IdCalibracion = 0;
    AplicarTemporal = 0;
    GuardaTemp = 0;
    clearInterval(IntervaloTiempo);
    Ingreso = 0;
    Foto = 0
    HoraInicio = "";
    HoraFinal = "";
    CerImpreso = 0;
    ErrorHume = 0;
    ErrorTemp = 0;
    Revision = 1;
    Certificado = "";

    TempMax = 0;
    TempMin = 100000000;
    HumeMax = 0;
    HumeMin = 100000000;
    TempVar = 0;
    HumeVar = 0;

    for (var x = 0; x < 3; x++) {
        Reproducibilidad[x] = 0;
        Salida[x] = 0;
        Interfaz[x] = 0;
        Longitud[x] = 0;
    }

    InfoTecIngreso = 0;
    $("#Solicitante").html("");
    $("#Certificado").html("");
    $("#Direccion").html("");
    $("#FechaIng").html("");
    $("#FechaCal").html("");
    $("#FechaExp").html("");
    $("#RangoMaximo").html("")
    $("#TIngreso").html("");
    $("#Medida").html("");
    $("#ValorCon").html("");
    $("#UnidadCon").html("");
    $("#UnidadCon").html("");
    $("#ProximaCali").val("");
    $("#Observaciones").val("La calibración se realizó en las instalaciones permanentes de Calibration Service S.A.S.");
    localStorage.setItem("FirmaCertificado", "");
    $(".medida").html("");
    $("#CertificadoAnulado").html("");
    $("#TiempoCali").val("0").trigger("change");
    $("#DescripcionAjuste").prop("disabled", true);

    $(".repetibilidad").val("");
    $(".repetibilidad").prop("readonly", false);
    $(".repetibilidad").removeClass("bg-lineas-diagonales");

    $("#Equipo").html("");
    $("#Marca").html("");
    $("#Modelo").html("");
    $("#Serie").html("");
    $("#Remision").val("");
    $("#Usuario").val("");
    $("#FechaRec").val("");
    $("#Tiempo").val("");

    $("#RangoMedida").html("");
    $("#MedResolucion").html("");
    RangoHasta = 0;

    $("#Descripcion").html("");
    $("#Tolerancia").html("");
    $("#Resolucion").html("");
    $("#Indicacion").html("");
    $("#Clase").html("");
    $("#Clasificacion").html("");
    $("#Revision").val("1").trigger("change");

    $("#ButCertFirMan").addClass("hidden");

    $("#CMetodo").val("");
    $("#CPunto").val("");
    $("#CServicio").val("");
    $("#CObservCerti").val("");
    $("#CProxima").val("");
    $("#CEntrega").val("");
    $("#CAsesorComer").val("");

    DesPerm1 = 0;
    $("#DevPermitida1").html(DesPerm1);
    DesPerm2 = 0;
    $("#DevPermitida2").html(DesPerm2);

    $("#PrSerie1").val("");
    $("#PrSerie2").val("");
    $("#PrSerie3").val("");
    $("#PrSerie4").val("");
    $("#PrSerie5").val("");
    
    $("#CaHoraIni").val(""); 
    $("#CaHoraFin").val(""); 
    $("#CaTiempo").val(""); 

    $("#Concepto").val("1").trigger("change");
    $("#Ajuste").val("");
    $("#Suministro").val("");
    $("#Conclusion").val("");
    $("#DescripcionAjuste").val("");
    $("#DescripcionAjuste").prop("disabled", true);

    for (var x = 1; x <= 10; x++) {
        if (x <= 5) {
            $("#SerRepI_" + x).val("");
            $("#SerRepII_" + x).val("");
            $("#SerRepIII_" + x).val("");
            $("#SerRepIV_" + x).val("");
        }
        
        $("#SerVarSal0_" +x).val("");
        $("#SerVarSal90_" + x).val("");
        $("#SerVarSal180_" + x).val("");
        $("#SerVarSal270_" + x).val("");
        
        $("#SerVarInt0_" +x).val("");
        $("#SerVarInt90_" + x).val("");
        $("#SerVarInt180_" + x).val("");
        $("#SerVarInt270_" + x).val("");
        
        $("#SerVarLon1_"+x).val("");
        $("#SerVarLon2_" + x).val("");
    }

    for (var x = 1; x <= CantRegitro; x++) {
        $("#PaPorLectura" + x).val("");
        $("#Patron" + x).val("").trigger("change");
    }

    $("#ProximaCali").prop("disabled", false);
    $("#Observacion").prop("disabled", false);
    $("#TiempoCali").prop("disabled", false);
    
    $("#PrSerie1").prop("disabled", false);
    $("#PrSerie2").prop("disabled", false);
    $("#PrSerie3").prop("disabled", false);
    $("#PrSerie4").prop("disabled", false);
    $("#PrSerie5").prop("disabled", false);
    $("#PrPosicion").prop("disabled", false);

    ConfigurarTablas(0);
        
    IdInstrumento = 0;
    IdCalibracion = 0;
    IdReporte = 0;

    CambioConcepto(1, 1);

    Graficar();
    GraficarDesviacion();
    CalcularTemperatura(0);
}



function GuardarDocumento() {
    var documento = document.getElementById("documento");
    if ($.trim(documento.value) != "") {

        var file = documento.files[0];
        var data = new FormData();
        data.append('documento', file);
        var resultado = '';
        var direccion = url + "Cotizacion/AdjuntarPDF";
        $.ajax({
            url: direccion,
            type: 'POST',
            contentType: false,
            data: data,
            processData: false,
            async: false,
            success: function (datos) {
                datos = datos.split("|");
                if (datos[0] != "0") {
                    swal(ValidarTraduccion("Error al cargar el archivo"), "", "error");
                } else {
                    if (datos[2] != "XX")
                        swal(ValidarTraduccion(datos[2]), "", "error");
                }
            }
        });
    }
}

function MostrarFoto(numero) {
    var informe = $("#InformeTecnico").val() * 1;
    $("#FotoMantenimiento").attr("src", url + "imagenes/informemantenimineto/" + informe + "/" + numero + ".jpg");
}


function GuardarDocumento2(id) {
    var documento = document.getElementById(id);
    var informe = $("#InformeTecnico").val() * 1;
    var numero = $("#NumFoto").val() * 1;


    if ($.trim(documento.value) != "") {

        if (informe == 0) {
            swal("Acción Cancelada", "Debe generar primero el informe técnico", "warning");
            return false;
        }

        var file = documento.files[0];
        var data = new FormData();
        data.append('documento', file);
        var resultado = '';
        var direccion = url + "Cotizacion/AdjuntarFoto";
        $.ajax({
            url: direccion,
            type: 'POST',
            contentType: false,
            data: data,
            processData: false,
            async: false,
            success: function (datos) {
                datos = datos.split("|");
                if (datos[0] == "0") {
                    datos = LlamarAjax("Laboratorio/SubirFoto", "ingreso=" + Ingreso + "&informe=" + informe + "&numero=" + numero + "&foto=" + datos[1]).split("|");
                    swal(datos[1]);
                    if (datos[0] == "0")
                        MostrarFoto(numero)
                } else {
                    swal(ValidarTraduccion(datos[1]), "", "error");
                }
            }
        });
    }
}

function CambioTiempoCali(tiempo) {
    if (tiempo * 1 == 0)
        $("#ProximaCali").val("");
    else {
        var fecha = (d.getFullYear()*1 + tiempo*1) + "-" + (month < 10 ? '0' : '') + month + "-" + (day < 10 ? "0" : "") + day;
        $("#ProximaCali").val(fecha);
    }
}

function GeneralPlantilla() {
    var concepto = $("#Concepto").val() * 1;
    if (Ingreso == 0)
        return false;

    if (GuardaTemp == 1 || IdCalibracion == 0) {
        swal("Acción Cancelada", "Debe primero guardar el reporte", "warning");
        return false;
    }

    if ((concepto != 2) && (concepto != 1)) {
        swal("Acción Cancelada", "No se puede generar la planilla del certificado cuando no está apto a calibrar o en subcontrataciones", "warning");
        return false;
    }

    ActivarLoad();
    setTimeout(function () {
        var datos = LlamarAjax("Laboratorio/GenerarPlantilla", "ingreso=" + Ingreso + "&magnitud=6").split("|");
        DesactivarLoad();
        if (datos[0] == "0")
            swal("", datos[1], "success");
        else
            swal("Acción Cancelada", datos[1], "warning");
    }, 15);
}

function EnviarCotizacion() {

    var concepto = $("#Concepto").val() * 1;


    if (Ingreso == 0)
        return false;
    if (GuardaTemp == 1 || IdReporte == 0) {
        swal("Acción Cancelada", "Debe primero guardar el reporte", "warning");
        return false;
    }

    $("#eCliente").val($("#Solicitante").html());
    $("#eCorreo").val(CorreoEmpresa);


    if (concepto == 2) {
        $("#spcotizacion").html(Ingreso);
        $("#enviar_cotizacion").modal("show");
    } else {
        swal("Acción Cancelada", "Solo se podra enviar cotizaciones cuando el ingreso requiera ajuste o suministro", "warning");
    }
    
}

function BuscarIngreso(numero, code, tipo) {
    if ((Ingreso != (numero * 1)) && ($("#Equipo").html() != "")) {
        LimpiarDatos();
    }
        
    if ((code.keyCode == 13 || tipo == "0") && ErrorEnter == 0) {
        if (numero * 1 > 0) {
            ActivarLoad();
            setTimeout(function () {
                var datos = LlamarAjax("Laboratorio/BuscarIngresoPlaCert", "magnitud=6&ingreso=" + numero + "&medida=N·m" + "&numero=" + NumCertificado + "&version=" + IdVersion).split("|");
                DesactivarLoad();
                if (datos[0] != "[]") {
                    var data = JSON.parse(datos[0]);

                    if (data[0].idmagnitud != "6") {
                        swal("Acción Cancelada", "El ingreso número " + numero + " no pertenece a la magnitud <br><b>PAR TORSIONAL</b>", "warning");
                        ErrorEnter = 1;
                        $("#Ingreso").select();
                        $("#Ingreso").focus();
                        return false;
                    }

                    if (datos[4] != "[]") {
                        var datagen = JSON.parse(datos[4]);
                        Certificado = datagen[0].numero;
                    }
   
                    Ingreso = data[0].ingreso * 1;
                    localStorage.setItem("Ingreso", Ingreso);
                    Foto = data[0].fotos * 1;
                    Solicitante = data[0].cliente;
                    IdCliente = data[0].idcliente;
                    $("#Solicitante").html(Solicitante);
                    Direccion = data[0].direccion;
                    $("#Direccion").html(Direccion);
                    $("#FechaIng").html(data[0].fechaing);
                    $("#FechaCal").html(FechaCal);
                    CorreoEmpresa = data[0].email;
                    CantLecturas = data[0].lectura * 1;
                    Revision = data[0].revision*1;
                    $("#Revision").val(Revision).trigger("change");
                    CantRegitro = data[0].puntos*1;
                    Factor = data[0].medidacon;

                    

                    if (Revision > 1) {
                        $("#DescripcionAjuste").prop("disabled", false);
                    }
                    
                    if (data[0].idinstrumento * 1 == 0) {
                        ErrorEnter = 1;
                        swal("Acción Cancelada", "Este ingreso no posee operaciones previas", "warning");
                        LimpiarDatos();
                        return false;
                    }

                    if (NumCertificado == 22) {
                        $("#RangoMedida").html("(" + data[0].desde + " a " + data[0].hasta + ") " + data[0].medida);
                        RangoHasta = data[0].hasta;
                        $("#RangoMaximo").html(RangoHasta);
                        $("#Tolerancia").html(data[0].tolerancia);
                        $("#Resolucion").html(data[0].resolucion);
                        Tolerancia = data[0].tolerancia * 1;
                        Resolucion = data[0].resolucion*1;
                    } else {
                        $("#RangoMedida").html("(" + (data[0].desde * -1) + " a " + (data[0].hasta * -1) + ") " + data[0].medida);
                        RangoHasta = data[0].hasta * -1;
                        $("#RangoMaximo").html(RangoHasta * -1);
                        $("#Tolerancia").html(data[0].tolerancia_sch);
                        $("#Resolucion").html(data[0].resolucion_sch);
                        Tolerancia = data[0].tolerancia_sch * 1;
                        Resolucion = data[0].resolucion_sch*1;
                    }

                    $("#Puntos").val(CantRegitro).trigger("change");
                                                           
                    FirmaCertificado = data[0].firmacertificado;
                    $("#firmacertificado").html("FIRMA " + FirmaCertificado);
                    if (FirmaCertificado == "MANUAL")
                        $("#ButCertFirMan").removeClass("hidden");
                    
                    $("#TIngreso").html(data[0].ingreso);
                    $("#Medida").html(data[0].medida);
                    $("#ValorCon").html(data[0].medidacon);
                    
                    $("#UnidadCon").html("1 " + data[0].medida + " =");

                    $(".medida").html(data[0].medida);
                    Cuadrante = data[0].cuadrante;
                    $("#Cuadrante").html(data[0].cuadrante);
                    $("#Equipo").html(data[0].equipo);
                    $("#Marca").html(data[0].marca);
                    $("#Modelo").html(data[0].modelo);
                    $("#Serie").html(data[0].serie);
                    $("#Remision").val(data[0].remision);
                    InfoTecIngreso = data[0].informetecnico;
                    $(".cuadrante").prop("readonly", false);
                    $(".cuadrante").val("");
                    $(".cuadrante").removeClass("bg-lineas-diagonales");
                                                                                
                    $("#MedResolucion").html(data[0].medida);

                    DesPerm1 = data[0].tolerancia * 1;
                    $("#DevPermitida1").html(DesPerm1);
                    DesPerm2 = Tolerancia * -1;
                    $("#DevPermitida2").html(DesPerm2);

                    $("#Descripcion").html(data[0].descripcion);
                    
                    $("#Indicacion").html(data[0].indicacion);
                    $("#Clase").html(data[0].clase);
                    $("#Clasificacion").html(data[0].clasificacion);
                    TipoInst = data[0].clasificacion;
                    Descripcion = data[0].descripcion1;

                    $("#Usuario").val(data[0].usuarioing);
                    $("#FechaRec").val(data[0].fecharec);
                    $("#Tiempo").val(data[0].tiempo);

                    if (datos[4] != "[]") {
                        var revisioncer = datagen[0].revision * 1;
                        var numcer = datagen[0].item * 1;
                        Certificado = datagen[0].numero;
                        $("#Certificado").html(Certificado);
                        AplicarTemporal = 0;
                        if (datagen[0].nombrecer != "")
                            $("#Solicitante").html(datagen[0].nombrecer);
                        if (datagen[0].direccioncer != "")
                            $("#Direccion").html(datagen[0].direccioncer);

                        /*if (datagen[0].idusuarioanula * 1 > 0) {
                            Revision = revisioncer + 1;
                            return false;
                        }*/

                        $("#FechaExp").html(datagen[0].fechaexp);
                        $("#ProximaCali").prop("disabled", true);
                        $("#Observacion").prop("disabled", true);
                        $("#TiempoCali").prop("disabled", true);
                                                   
                        ErrorEnter = 1;
                        swal("Advertencia", "El certificado ya fue generado con el número " + Certificado, "warning")
                        EscucharMensaje("El certificado ya fue generado");

                        $("#PrSerie1").prop("disabled", true);
                        $("#PrSerie2").prop("disabled", true);
                        $("#PrSerie3").prop("disabled", true);
                        $("#PrSerie4").prop("disabled", true);
                        $("#PrSerie5").prop("disabled", true);
                        $("#ProximaCali").prop("disabled", true);
                        $("#Observaciones").prop("disabled", true);
                        $("#TiempoCali").prop("disabled", true);
                        $("#Concepto").prop("disabled", true);
                        $("#Ajuste").prop("disabled", true);
                        $("#Suministro").prop("disabled", true);
                        for (var x = 1; x <= CantRegitro; x++) {
                            
                            $("#Serie1" + x).prop("disabled", true);
                            $("#Serie2" + x).prop("disabled", true);
                            $("#Serie3" + x).prop("disabled", true);
                            $("#Serie4" + x).prop("disabled", true);
                            $("#Serie5" + x).prop("disabled", true);
                            $("#Serie6" + x).prop("disabled", true);
                            $("#Serie7" + x).prop("disabled", true);
                            $("#Serie8" + x).prop("disabled", true);
                            $("#Serie9" + x).prop("disabled", true);
                            $("#Serie10" + x).prop("disabled", true);
                            $("#PaPorLectura" + x).prop("disabled", true);
                            $("#Patron" + x).prop("disabled", true);
                        }

                        
                        for (var x = 1; x <= 10; x++) {
                            if (x <= 5) {
                                $("#SerRepI_" + x).prop("disabled", true);
                                $("#SerRepII_" + x).prop("disabled", true);
                                $("#SerRepIII_" + x).prop("disabled", true);
                                $("#SerRepIV_" + x).prop("disabled", true);
                            }

                            $("#SerVarSal0_" + x).prop("disabled", true);
                            $("#SerVarSal90_" + x).prop("disabled", true);
                            $("#SerVarSal180_" + x).prop("disabled", true);
                            $("#SerVarSal270_" + x).prop("disabled", true);

                            $("#SerVarInt0_" + x).prop("disabled", true);
                            $("#SerVarInt90_" + x).prop("disabled", true);
                            $("#SerVarInt180_" + x).prop("disabled", true);
                            $("#SerVarInt270_" + x).prop("disabled", true);

                            $("#SerVarLon1_" + x).prop("disabled", true);
                            $("#SerVarLon2_" + x).prop("disabled", true);
                        }
                                                                                                
                    } else {

                        $("#PrSerie1").prop("disabled", false);
                        $("#PrSerie2").prop("disabled", false);
                        $("#PrSerie3").prop("disabled", false);
                        $("#PrSerie4").prop("disabled", false);
                        $("#PrSerie5").prop("disabled", false);
                        $("#ProximaCali").prop("disabled", false);
                        $("#Observaciones").prop("disabled", false);
                        $("#TiempoCali").prop("disabled", false);
                        $("#Concepto").prop("disabled", false);
                        $("#Ajuste").prop("disabled", false);
                        $("#Suministro").prop("disabled", false);
                        for (var x = 1; x <= CantRegitro; x++) {
                            $("#Serie1" + x).prop("disabled", false);
                            $("#Serie2" + x).prop("disabled", false);
                            $("#Serie3" + x).prop("disabled", false);
                            $("#Serie4" + x).prop("disabled", false);
                            $("#Serie5" + x).prop("disabled", false);
                            $("#Serie6" + x).prop("disabled", false);
                            $("#Serie7" + x).prop("disabled", false);
                            $("#Serie8" + x).prop("disabled", false);
                            $("#Serie9" + x).prop("disabled", false);
                            $("#Serie10" + x).prop("disabled", false);
                            $("#PaPorLectura" + x).prop("disabled", false);
                            $("#Patron" + x).prop("disabled", false);
                        }

                        for (var x = 1; x <= 10; x++) {
                            if (x <= 5) {
                                $("#SerRepI_" + x).prop("disabled", false);
                                $("#SerRepII_" + x).prop("disabled", false);
                                $("#SerRepIII_" + x).prop("disabled", false);
                                $("#SerRepIV_" + x).prop("disabled", false);
                            }

                            $("#SerVarSal0_" + x).prop("disabled", false);
                            $("#SerVarSal90_" + x).prop("disabled", false);
                            $("#SerVarSal180_" + x).prop("disabled", false);
                            $("#SerVarSal270_" + x).prop("disabled", false);

                            $("#SerVarInt0_" + x).prop("disabled", false);
                            $("#SerVarInt90_" + x).prop("disabled", false);
                            $("#SerVarInt180_" + x).prop("disabled", false);
                            $("#SerVarInt270_" + x).prop("disabled", false);

                            $("#SerVarLon1_" + x).prop("disabled", false);
                            $("#SerVarLon2_" + x).prop("disabled", false);
                        }

                        AplicarTemporal = 1;
                        
                    }
                                       
                    if (datos[2] != "[]") {

                        AplicarTemporal = 0; 

                        var datacerpre = JSON.parse(datos[2]);
                        IdCalibracion = datacerpre[0].id * 1;
                        IdReporte = datacerpre[0].idreporte * 1;
                        FechaCal = datacerpre[0].fecha;
                        $("#FechaCal").html(FechaCal);
                        $("#ProximaCali").val(datacerpre[0].proximacali);
                        Revision = datacerpre[0].revision * 1;
                        HoraInicio = datacerpre[0].horaini;
                        HoraFinal = datacerpre[0].horafin;

                        $("#CaHoraIni").val(HoraInicio);
                        $("#CaHoraFin").val(HoraFinal);
                        $("#CaTiempo").val(datacerpre[0].tiempotrans);
                        
                        CerImpreso = datacerpre[0].impreso * 1;
                        $("#Concepto").val(datacerpre[0].idconcepto).trigger("change");
                        $("#Ajuste").val(datacerpre[0].ajuste);
                        $("#Suministro").val(datacerpre[0].suministro);
                        $("#Observaciones").val(datacerpre[0].observacion)
                        $("#Conclusion").val(datacerpre[0].conclusion)
                        $("#InformeTecnico").val(datacerpre[0].informetecnico);
                        MostrarFoto(1);

                        var datapunto = JSON.parse(datos[3]);
                        CantRegitro = datapunto.length;
                        ConfigurarTablas(datapunto.length);
                        for (var x = 0; x < datapunto.length; x++) {

                            $("#Patron" + datapunto[x].fila).val(datapunto[x].patron).trigger("change");
                            $("#PaPorLectura" + datapunto[x].fila).val(formato_numero(datapunto[x].porlectura, 3, ".", ",", ""));
                            $("#PrSerie1").val(formato_numero(datapunto[x].prserie1, 3, ".", ",", ""));
                            $("#PrSerie2").val(formato_numero(datapunto[x].prserie2, 3, ".", ",", ""));
                            $("#PrSerie3").val(formato_numero(datapunto[x].prserie3, 3, ".", ",", ""));
                            $("#PrSerie4").val(formato_numero(datapunto[x].prserie4, 3, ".", ",", ""));
                            $("#PrSerie5").val(formato_numero(datapunto[x].prserie5, 3, ".", ",", ""));
                            DecimalesP[datapunto[x].fila] = datapunto[x].decimalpatron;
                            $("#DecimalPatron" + datapunto[x].fila).val(DecimalesP[datapunto[x].fila]);
                            CambioDecimales(DecimalesP[datapunto[x].fila], datapunto[x].fila);

                            $("#Serie1" + datapunto[x].fila).val(formato_numero(datapunto[x].serie1, datapunto[x].decimalpatron, ".", ",", ""));
                            $("#Serie2" + datapunto[x].fila).val(formato_numero(datapunto[x].serie2, datapunto[x].decimalpatron, ".", ",", ""));
                            $("#Serie3" + datapunto[x].fila).val(formato_numero(datapunto[x].serie3,datapunto[x].decimalpatron, ".", ",", ""));
                            $("#Serie4" + datapunto[x].fila).val(formato_numero(datapunto[x].serie4,datapunto[x].decimalpatron, ".", ",", ""));
                            $("#Serie5" + datapunto[x].fila).val(formato_numero(datapunto[x].serie5,datapunto[x].decimalpatron, ".", ",", ""));
                            $("#Serie6" + datapunto[x].fila).val(formato_numero(datapunto[x].serie6,datapunto[x].decimalpatron, ".", ",", ""));
                            $("#Serie7" + datapunto[x].fila).val(formato_numero(datapunto[x].serie7,datapunto[x].decimalpatron, ".", ",", ""));
                            $("#Serie8" + datapunto[x].fila).val(formato_numero(datapunto[x].serie8,datapunto[x].decimalpatron, ".", ",", ""));
                            $("#Serie9" + datapunto[x].fila).val(formato_numero(datapunto[x].serie9,datapunto[x].decimalpatron, ".", ",", ""));
                            $("#Serie10" + datapunto[x].fila).val(formato_numero(datapunto[x].serie10,datapunto[x].decimalpatron, ".", ",", ""));
                            $("#SerPromedio" + datapunto[x].fila).val(datapunto[x].serpromedio);

                            if (datos[4] != "[]") {

                                if (CantLecturas * 1 == 5) {
                                    $(".punto10").addClass("bg-lineas-diagonales");
                                }

                                $("#NmSerie1" + datapunto[x].fila).val(formato_numero(datapunto[x].nmserie1, datapunto[x].decimalpatron, ".", ",", ""));
                                $("#NmSerie2" + datapunto[x].fila).val(formato_numero(datapunto[x].nmserie2, datapunto[x].decimalpatron, ".", ",", ""));
                                $("#NmSerie3" + datapunto[x].fila).val(formato_numero(datapunto[x].nmserie3, datapunto[x].decimalpatron, ".", ",", ""));
                                $("#NmSerie4" + datapunto[x].fila).val(formato_numero(datapunto[x].nmserie4, datapunto[x].decimalpatron, ".", ",", ""));
                                $("#NmSerie5" + datapunto[x].fila).val(formato_numero(datapunto[x].nmserie5, datapunto[x].decimalpatron, ".", ",", ""));
                                $("#NmSerie6" + datapunto[x].fila).val(formato_numero(datapunto[x].nmserie6, datapunto[x].decimalpatron, ".", ",", ""));
                                $("#NmSerie7" + datapunto[x].fila).val(formato_numero(datapunto[x].nmserie7, datapunto[x].decimalpatron, ".", ",", ""));
                                $("#NmSerie8" + datapunto[x].fila).val(formato_numero(datapunto[x].nmserie8, datapunto[x].decimalpatron, ".", ",", ""));
                                $("#NmSerie9" + datapunto[x].fila).val(formato_numero(datapunto[x].nmserie9, datapunto[x].decimalpatron, ".", ",", ""));
                                $("#NmSerie10" + datapunto[x].fila).val(formato_numero(datapunto[x].nmserie10, datapunto[x].decimalpatron, ".", ",", ""));
                                $("#NmSerPromedio" + datapunto[x].fila).val(formato_numero(datapunto[x].nmpromedio, datapunto[x].decimalpatron, ".", ",", ""));
                                                                
                                $("#CNmSerie1" + datapunto[x].fila).val(formato_numero(datapunto[x].cnmserie1, datapunto[x].decimalpatron, ".", ",", ""));
                                $("#CNmSerie2" + datapunto[x].fila).val(formato_numero(datapunto[x].cnmserie2, datapunto[x].decimalpatron, ".", ",", ""));
                                $("#CNmSerie3" + datapunto[x].fila).val(formato_numero(datapunto[x].cnmserie3, datapunto[x].decimalpatron, ".", ",", ""));
                                $("#CNmSerie4" + datapunto[x].fila).val(formato_numero(datapunto[x].cnmserie4, datapunto[x].decimalpatron, ".", ",", ""));
                                $("#CNmSerie5" + datapunto[x].fila).val(formato_numero(datapunto[x].cnmserie5, datapunto[x].decimalpatron, ".", ",", ""));
                                $("#CNmSerie6" + datapunto[x].fila).val(formato_numero(datapunto[x].cnmserie6, datapunto[x].decimalpatron, ".", ",", ""));
                                $("#CNmSerie7" + datapunto[x].fila).val(formato_numero(datapunto[x].cnmserie7, datapunto[x].decimalpatron, ".", ",", ""));
                                $("#CNmSerie8" + datapunto[x].fila).val(formato_numero(datapunto[x].cnmserie8, datapunto[x].decimalpatron, ".", ",", ""));
                                $("#CNmSerie9" + datapunto[x].fila).val(formato_numero(datapunto[x].cnmserie9, datapunto[x].decimalpatron, ".", ",", ""));
                                $("#CNmSerie10" + datapunto[x].fila).val(formato_numero(datapunto[x].cnmserie10, datapunto[x].decimalpatron, ".", ",", ""));
                                $("#CNmSerPromedio" + datapunto[x].fila).val(formato_numero(datapunto[x].cnmpromedio, datapunto[x].decimalpatron, ".", ",", ""));

                                $("#CMeSerie1" + datapunto[x].fila).val(formato_numero(datapunto[x].cnmserie1, datapunto[x].decimalpatron, ".", ",", ""));
                                $("#CMeSerie2" + datapunto[x].fila).val(formato_numero(datapunto[x].cnmserie2, datapunto[x].decimalpatron, ".", ",", ""));
                                $("#CMeSerie3" + datapunto[x].fila).val(formato_numero(datapunto[x].cnmserie3, datapunto[x].decimalpatron, ".", ",", ""));
                                $("#CMeSerie4" + datapunto[x].fila).val(formato_numero(datapunto[x].cnmserie4, datapunto[x].decimalpatron, ".", ",", ""));
                                $("#CMeSerie5" + datapunto[x].fila).val(formato_numero(datapunto[x].cnmserie5, datapunto[x].decimalpatron, ".", ",", ""));
                                $("#CMeSerie6" + datapunto[x].fila).val(formato_numero(datapunto[x].cnmserie6, datapunto[x].decimalpatron, ".", ",", ""));
                                $("#CMeSerie7" + datapunto[x].fila).val(formato_numero(datapunto[x].cnmserie7, datapunto[x].decimalpatron, ".", ",", ""));
                                $("#CMeSerie8" + datapunto[x].fila).val(formato_numero(datapunto[x].cnmserie8, datapunto[x].decimalpatron, ".", ",", ""));
                                $("#CMeSerie9" + datapunto[x].fila).val(formato_numero(datapunto[x].cnmserie9, datapunto[x].decimalpatron, ".", ",", ""));
                                $("#CMeSerie10" + datapunto[x].fila).val(formato_numero(datapunto[x].cnmserie10, datapunto[x].decimalpatron, ".", ",", ""));
                                $("#CMeSerPromedio" + datapunto[x].fila).val(formato_numero(datapunto[x].cmepromedio, datapunto[x].decimalpatron, ".", ",", ""));

                                $("#DeSerie1" + datapunto[x].fila).val(formato_numero(datapunto[x].deserie1, datapunto[x].decimalpatron, ".", ",", ""));
                                $("#DeSerie2" + datapunto[x].fila).val(formato_numero(datapunto[x].deserie2, datapunto[x].decimalpatron, ".", ",", ""));
                                $("#DeSerie3" + datapunto[x].fila).val(formato_numero(datapunto[x].deserie3, datapunto[x].decimalpatron, ".", ",", ""));
                                $("#DeSerie4" + datapunto[x].fila).val(formato_numero(datapunto[x].deserie4, datapunto[x].decimalpatron, ".", ",", ""));
                                $("#DeSerie5" + datapunto[x].fila).val(formato_numero(datapunto[x].deserie5, datapunto[x].decimalpatron, ".", ",", ""));
                                $("#DeSerie6" + datapunto[x].fila).val(formato_numero(datapunto[x].deserie6, datapunto[x].decimalpatron, ".", ",", ""));
                                $("#DeSerie7" + datapunto[x].fila).val(formato_numero(datapunto[x].deserie7, datapunto[x].decimalpatron, ".", ",", ""));
                                $("#DeSerie8" + datapunto[x].fila).val(formato_numero(datapunto[x].deserie8, datapunto[x].decimalpatron, ".", ",", ""));
                                $("#DeSerie9" + datapunto[x].fila).val(formato_numero(datapunto[x].deserie9, datapunto[x].decimalpatron, ".", ",", ""));
                                $("#DeSerie10" + datapunto[x].fila).val(formato_numero(datapunto[x].deserie10, datapunto[x].decimalpatron, ".", ",", ""));
                                $("#DeSerPromedio" + datapunto[x].fila).val(formato_numero(datapunto[x].despromedio, datapunto[x].decimalpatron, ".", ",", ""));

                                if (datapunto[x].deserie1 >= DesPerm2 && datapunto[x].deserie1 <= DesPerm1) {
                                    $("#DeSerie1" + datapunto[x].fila).addClass("bg-default");
                                    $("#DeSerie1" + datapunto[x].fila).removeClass("bg-danger");
                                } else {
                                    $("#DeSerie1" + datapunto[x].fila).removeClass("bg-default");
                                    $("#DeSerie1" + datapunto[x].fila).addClass("bg-danger");
                                }

                                if (datapunto[x].deserie2 >= DesPerm2 && datapunto[x].deserie2 <= DesPerm1) {
                                    $("#DeSerie2" + datapunto[x].fila).addClass("bg-default");
                                    $("#DeSerie2" + datapunto[x].fila).removeClass("bg-danger");
                                } else {
                                    $("#DeSerie2" + datapunto[x].fila).removeClass("bg-default");
                                    $("#DeSerie2" + datapunto[x].fila).addClass("bg-danger");
                                }

                                if (datapunto[x].deserie3 >= DesPerm2 && datapunto[x].deserie3 <= DesPerm1) {
                                    $("#DeSerie3" + datapunto[x].fila).addClass("bg-default");
                                    $("#DeSerie3" + datapunto[x].fila).removeClass("bg-danger");
                                } else {
                                    $("#DeSerie3" + datapunto[x].fila).removeClass("bg-default");
                                    $("#DeSerie3" + datapunto[x].fila).addClass("bg-danger");
                                }

                                if (datapunto[x].deserie4 >= DesPerm2 && datapunto[x].deserie4 <= DesPerm1) {
                                    $("#DeSerie4" + datapunto[x].fila).addClass("bg-default");
                                    $("#DeSerie4" + datapunto[x].fila).removeClass("bg-danger");
                                } else {
                                    $("#DeSerie4" + datapunto[x].fila).removeClass("bg-default");
                                    $("#DeSerie4" + datapunto[x].fila).addClass("bg-danger");
                                }

                                if (datapunto[x].deserie5 >= DesPerm2 && datapunto[x].deserie5 <= DesPerm1) {
                                    $("#DeSerie5" + datapunto[x].fila).addClass("bg-default");
                                    $("#DeSerie5" + datapunto[x].fila).removeClass("bg-danger");
                                } else {
                                    $("#DeSerie5" + datapunto[x].fila).removeClass("bg-default");
                                    $("#DeSerie5" + datapunto[x].fila).addClass("bg-danger");
                                }

                                if (CantLecturas > 5) {
                                    if (datapunto[x].deserie6 >= DesPerm2 && datapunto[x].deserie6 <= DesPerm1) {
                                        $("#DeSerie6" + datapunto[x].fila).addClass("bg-default");
                                        $("#DeSerie6" + datapunto[x].fila).removeClass("bg-danger");
                                    } else {
                                        $("#DeSerie6" + datapunto[x].fila).removeClass("bg-default");
                                        $("#DeSerie6" + datapunto[x].fila).addClass("bg-danger");
                                    }
                                    if (datapunto[x].deserie7 >= DesPerm2 && datapunto[x].deserie7 <= DesPerm1) {
                                        $("#DeSerie7" + datapunto[x].fila).addClass("bg-default");
                                        $("#DeSerie7" + datapunto[x].fila).removeClass("bg-danger");
                                    } else {
                                        $("#DeSerie7" + datapunto[x].fila).removeClass("bg-default");
                                        $("#DeSerie7" + datapunto[x].fila).addClass("bg-danger");
                                    }
                                    if (datapunto[x].deserie8 >= DesPerm2 && datapunto[x].deserie8 <= DesPerm1) {
                                        $("#DeSerie8" + datapunto[x].fila).addClass("bg-default");
                                        $("#DeSerie8" + datapunto[x].fila).removeClass("bg-danger");
                                    } else {
                                        $("#DeSerie8" + datapunto[x].fila).removeClass("bg-default");
                                        $("#DeSerie8" + datapunto[x].fila).addClass("bg-danger");
                                    }
                                    if (datapunto[x].deserie9 >= DesPerm2 && datapunto[x].deserie9 <= DesPerm1) {
                                        $("#DeSerie9" + datapunto[x].fila).addClass("bg-default");
                                        $("#DeSerie9" + datapunto[x].fila).removeClass("bg-danger");
                                    } else {
                                        $("#DeSerie9" + datapunto[x].fila).removeClass("bg-default");
                                        $("#DeSerie9" + datapunto[x].fila).addClass("bg-danger");
                                    }
                                    if (datapunto[x].deserie10 >= DesPerm2 && datapunto[x].deserie10 <= DesPerm1) {
                                        $("#DeSerie10" + datapunto[x].fila).addClass("bg-default");
                                        $("#DeSerie10" + datapunto[x].fila).removeClass("bg-danger");
                                    } else {
                                        $("#DeSerie10" + datapunto[x].fila).removeClass("bg-default");
                                        $("#DeSerie10" + datapunto[x].fila).addClass("bg-danger");
                                    }
                                }

                                $("#DeSerie1" + datapunto[x].fila).val(formato_numero(datapunto[x].deserie1, datapunto[x].decimalpatron, ".", ",", ""));
                                $("#DeSerie2" + datapunto[x].fila).val(formato_numero(datapunto[x].deserie2, datapunto[x].decimalpatron, ".", ",", ""));
                                $("#DeSerie3" + datapunto[x].fila).val(formato_numero(datapunto[x].deserie3, datapunto[x].decimalpatron, ".", ",", ""));
                                $("#DeSerie4" + datapunto[x].fila).val(formato_numero(datapunto[x].deserie4, datapunto[x].decimalpatron, ".", ",", ""));
                                $("#DeSerie5" + datapunto[x].fila).val(formato_numero(datapunto[x].deserie5, datapunto[x].decimalpatron, ".", ",", ""));
                                $("#DeSerie6" + datapunto[x].fila).val(formato_numero(datapunto[x].deserie6, datapunto[x].decimalpatron, ".", ",", ""));
                                $("#DeSerie7" + datapunto[x].fila).val(formato_numero(datapunto[x].deserie7, datapunto[x].decimalpatron, ".", ",", ""));
                                $("#DeSerie8" + datapunto[x].fila).val(formato_numero(datapunto[x].deserie8, datapunto[x].decimalpatron, ".", ",", ""));
                                $("#DeSerie9" + datapunto[x].fila).val(formato_numero(datapunto[x].deserie9, datapunto[x].decimalpatron, ".", ",", ""));
                                $("#DeSerie10" + datapunto[x].fila).val(formato_numero(datapunto[x].deserie10, datapunto[x].decimalpatron, ".", ",", ""));
                                $("#DeSerPromedio" + datapunto[x].fila).val(formato_numero(datapunto[x].despromedio, datapunto[x].decimalpatron, ".", ",", ""));

                                $("#ReResolucion" + datapunto[x].fila).val(formato_numero(datapunto[x].reresolucion, datapunto[x].decimalpatron, ".", ",", ""));
                                $("#ReReproducibilidad" + datapunto[x].fila).val(formato_numero(datapunto[x].rereproducibilidad, datapunto[x].decimalpatron, ".", ",", ""));
                                $("#ReGeoSalida" + datapunto[x].fila).val(formato_numero(datapunto[x].resalida, datapunto[x].decimalpatron, ".", ",", ""));
                                $("#ReGeoInterfaz" + datapunto[x].fila).val(formato_numero(datapunto[x].reinterfaz, datapunto[x].decimalpatron, ".", ",", ""));
                                $("#ReLongitud" + datapunto[x].fila).val(formato_numero(datapunto[x].relongitud, datapunto[x].decimalpatron, ".", ",", ""));
                                $("#ReRepetibilidad" + datapunto[x].fila).val(formato_numero(datapunto[x].rerepetibilidad, datapunto[x].decimalpatron, ".", ",", ""));
                                $("#ReUPatron" + datapunto[x].fila).val(formato_numero(datapunto[x].reupatron, datapunto[x].decimalpatron, ".", ",", ""));
                                $("#ReRPatron" + datapunto[x].fila).val(formato_numero(datapunto[x].rerpatron, datapunto[x].decimalpatron, ".", ",", ""));
                                $("#ReDPatron" + datapunto[x].fila).val(formato_numero(datapunto[x].redpatron, datapunto[x].decimalpatron, ".", ",", ""));


                                $("#CoResolucion" + datapunto[x].fila).val(formato_numero(datapunto[x].coresolucion, 4, ".", ",", ""));
                                $("#CoReproducibilidad" + datapunto[x].fila).val(formato_numero(datapunto[x].coreproducibilidad, 4, ".", ",", ""));
                                $("#CoGeoSalida" + datapunto[x].fila).val(formato_numero(datapunto[x].cosalida, 4, ".", ",", ""));
                                $("#CoGeoInterfaz" + datapunto[x].fila).val(formato_numero(datapunto[x].cointerfaz, 4, ".", ",", ""));
                                $("#CoLongitud" + datapunto[x].fila).val(formato_numero(datapunto[x].colongitud, 4, ".", ",", ""));
                                $("#CoRepetibilidad" + datapunto[x].fila).val(formato_numero(datapunto[x].corepetibilidad, 4, ".", ",", ""));
                                $("#CoUPatron" + datapunto[x].fila).val(formato_numero(datapunto[x].coupatron, 4, ".", ",", ""));
                                $("#CoRPatron" + datapunto[x].fila).val(formato_numero(datapunto[x].corpatron, 4, ".", ",", ""));
                                $("#CoDPatron" + datapunto[x].fila).val(formato_numero(datapunto[x].codpatron, 4, ".", ",", ""));

                                $("#InValorMed" + datapunto[x].fila).val(formato_numero(datapunto[x].valormedio, 3, ".", ",", ""));
                                $("#InValorMax" + datapunto[x].fila).val(formato_numero(datapunto[x].valorerror, 5, ".", ",", ""));
                                $("#InCombinada" + datapunto[x].fila).val(formato_numero(datapunto[x].incombinada, 3, ".", ",", ""));
                                $("#InFactor" + datapunto[x].fila).val(formato_numero(datapunto[x].factores, 2, ".", ",", ""));
                                $("#InExpandida" + datapunto[x].fila).val(formato_numero(datapunto[x].increlativa, 3, ".", ",", ""));
                                $("#InNGrados" + datapunto[x].fila).val(formato_numero(datapunto[x].gradoslibertad, 3, ".", ",", ""));
                                $("#InPromedio" + datapunto[x].fila).val(formato_numero(datapunto[x].promedio, 4, ".", ",", ""));
                                $("#InDesPromedio" + datapunto[x].fila).val(formato_numero(datapunto[x].despromedio, 3, ".", ",", ""));

                                $("#ReMeLecturaIBC" + datapunto[x].fila).html(formato_numero(datapunto[x].lectura, 3, ".", ",", ""));
                                $("#ReMeLecturapatron" + datapunto[x].fila).html(formato_numero(datapunto[x].cmepromedio, 3, ".", ",", ""));
                                $("#ReMeErroMediMed" + datapunto[x].fila).val(formato_numero(datapunto[x].meerrormed, 3, ".", ",", ""));
                                $("#ReMeErroMediPor" + datapunto[x].fila).val(formato_numero(datapunto[x].meerrormedpor, 2, ".", ",", ""));
                                $("#ReMeIncExpMed" + datapunto[x].fila).val(formato_numero(datapunto[x].meincerexpmed, calculardecimal(datapunto[x].meincerexpmed), ".", ",", ""));
                                $("#ReMeIncExpPor" + datapunto[x].fila).val(formato_numero(datapunto[x].meincerexppor, calculardecimal(datapunto[x].meincerexppor), ".", ",", ""));
                                $("#ReMeFacCober" + datapunto[x].fila).val(formato_numero(datapunto[x].factores, 2, ".", ",", ""));

                                $("#ReMnLecturaIBC" + datapunto[x].fila).html(formato_numero(datapunto[x].lectura * datapunto[x].factor, 3, ".", ",", ""));
                                $("#ReMnLecturapatron" + datapunto[x].fila).html(formato_numero(datapunto[x].cnmpromedio, 3, ".", ",", ""));
                                $("#ReMnErroMediMed" + datapunto[x].fila).val(formato_numero(datapunto[x].mmerrormed, 3, ".", ",", ""));
                                $("#ReMnErroMediPor" + datapunto[x].fila).val(formato_numero(datapunto[x].mmerrormedpor, 2, ".", ",", ""));
                                $("#ReMnIncExpMed" + datapunto[x].fila).val(formato_numero(datapunto[x].mmincerexpmed, calculardecimal(datapunto[x].mmincerexpmed), ".", ",", ""));
                                $("#ReMnIncExpPor" + datapunto[x].fila).val(formato_numero(datapunto[x].mmincerexppor, calculardecimal(datapunto[x].mmincerexppor), ".", ",", ""));
                                $("#ReMnFacCober" + datapunto[x].fila).val(formato_numero(datapunto[x].factores, 2, ".", ",", ""));

                                Graficar();
                                GraficarDesviacion();

                                if (datapunto[x].serpromedio == "REQUIERE AJUSTE") {
                                    $("#SerPromedio" + datapunto[x].fila).removeClass("text-success");
                                    $("#SerPromedio" + datapunto[x].fila).addClass("text-danger");
                                } else {
                                    $("#SerPromedio" + datapunto[x].fila).addClass("text-success");
                                    $("#SerPromedio" + datapunto[x].fila).removeClass("text-danger");
                                }
                            } 

                        }

                        datarep = JSON.parse(datos[6]);
                        for (var x = 0; x < datarep.length; x++) {
                            if (datarep[x].descripcion == "repetibilidad") {
                                $("#" + datarep[x].descripcion + "_1").val(formato_numero(datarep[x].lectura1, 3, ".", ",", ""));
                                $("#" + datarep[x].descripcion + "_2").val(formato_numero(datarep[x].lectura2, 3, ".", ",", ""));
                                $("#" + datarep[x].descripcion + "_3").val(formato_numero(datarep[x].lectura3, 3, ".", ",", ""));
                                $("#" + datarep[x].descripcion + "_4").val(formato_numero(datarep[x].lectura4, 3, ".", ",", ""));
                                $("#" + datarep[x].descripcion + "_5").val(formato_numero(datarep[x].lectura5, 3, ".", ",", ""));
                            } else {
                                $("#" + datarep[x].descripcion + "_1").val(formato_numero(datarep[x].lectura1, 3, ".", ",", ""));
                                $("#" + datarep[x].descripcion + "_2").val(formato_numero(datarep[x].lectura2, 3, ".", ",", ""));
                                $("#" + datarep[x].descripcion + "_3").val(formato_numero(datarep[x].lectura3, 3, ".", ",", ""));
                                $("#" + datarep[x].descripcion + "_4").val(formato_numero(datarep[x].lectura4, 3, ".", ",", ""));
                                $("#" + datarep[x].descripcion + "_5").val(formato_numero(datarep[x].lectura5, 3, ".", ",", ""));
                                $("#" + datarep[x].descripcion + "_6").val(formato_numero(datarep[x].lectura6, 3, ".", ",", ""));
                                $("#" + datarep[x].descripcion + "_7").val(formato_numero(datarep[x].lectura7, 3, ".", ",", ""));
                                $("#" + datarep[x].descripcion + "_8").val(formato_numero(datarep[x].lectura8, 3, ".", ",", ""));
                                $("#" + datarep[x].descripcion + "_9").val(formato_numero(datarep[x].lectura9, 3, ".", ",", ""));
                                $("#" + datarep[x].descripcion + "_10").val(formato_numero(datarep[x].lectura10, 3, ".", ",", ""));
                            }
                        }

                        CalcularRepetibilidad();
                        
                        CalcularTemperatura(1);

                    } 

                    if (datos[4] != "[]") {
                        var datagen = JSON.parse(datos[4]);
                        var revisioncer = datagen[0].revision * 1;
                        var numcer = datagen[0].item * 1;
                        Certificado = datagen[0].numero;
                        $("#Certificado").html(Certificado);
                        AplicarTemporal = 0;
                        if (datagen[0].nombrecer != "")
                            $("#Solicitante").html(datagen[0].nombrecer);
                        if (datagen[0].direccioncer != "")
                            $("#Direccion").html(datagen[0].direccioncer);

                        
                        $("#FechaExp").html(datagen[0].fechaexp);
                        $("#ProximaCali").prop("disabled", true);
                        $("#Observacion").prop("disabled", true);
                        $("#TiempoCali").prop("disabled", true);

                        ErrorEnter = 1;
                        swal("Advertencia", "El certificado ya fue generado con el número " + Certificado, "warning")
                        EscucharMensaje("El certificado ya fue generado");

                        $("#PrSerie1").prop("disabled", true);
                        $("#PrSerie2").prop("disabled", true);
                        $("#PrSerie3").prop("disabled", true);
                        $("#PrSerie4").prop("disabled", true);
                        $("#PrSerie5").prop("disabled", true);
                        $("#ProximaCali").prop("disabled", true);
                        $("#Observaciones").prop("disabled", true);
                        $("#TiempoCali").prop("disabled", true);
                        $("#Concepto").prop("disabled", true);
                        $("#Ajuste").prop("disabled", true);
                        $("#Suministro").prop("disabled", true);
                        $("#Conclusion").prop("disabled", true);
                        for (var x = 1; x <= CantRegitro; x++) {

                            $("#Serie1" + x).prop("disabled", true);
                            $("#Serie2" + x).prop("disabled", true);
                            $("#Serie3" + x).prop("disabled", true);
                            $("#Serie4" + x).prop("disabled", true);
                            $("#Serie5" + x).prop("disabled", true);
                            $("#Serie6" + x).prop("disabled", true);
                            $("#Serie7" + x).prop("disabled", true);
                            $("#Serie8" + x).prop("disabled", true);
                            $("#Serie9" + x).prop("disabled", true);
                            $("#Serie10" + x).prop("disabled", true);
                            $("#PaPorLectura" + x).prop("disabled", true);
                            $("#Patron" + x).prop("disabled", true);
                        }


                        for (var x = 1; x <= 10; x++) {
                            if (x <= 5) {
                                $("#SerRepI_" + x).prop("disabled", true);
                                $("#SerRepII_" + x).prop("disabled", true);
                                $("#SerRepIII_" + x).prop("disabled", true);
                                $("#SerRepIV_" + x).prop("disabled", true);
                            }

                            $("#SerVarSal0_" + x).prop("disabled", true);
                            $("#SerVarSal90_" + x).prop("disabled", true);
                            $("#SerVarSal180_" + x).prop("disabled", true);
                            $("#SerVarSal270_" + x).prop("disabled", true);

                            $("#SerVarInt0_" + x).prop("disabled", true);
                            $("#SerVarInt90_" + x).prop("disabled", true);
                            $("#SerVarInt180_" + x).prop("disabled", true);
                            $("#SerVarInt270_" + x).prop("disabled", true);

                            $("#SerVarLon1_" + x).prop("disabled", true);
                            $("#SerVarLon2_" + x).prop("disabled", true);
                        }

                    } else {

                        $("#PrSerie1").prop("disabled", false);
                        $("#PrSerie2").prop("disabled", false);
                        $("#PrSerie3").prop("disabled", false);
                        $("#PrSerie4").prop("disabled", false);
                        $("#PrSerie5").prop("disabled", false);
                        $("#ProximaCali").prop("disabled", false);
                        $("#TiempoCali").prop("disabled", false);
                        $("#Concepto").prop("disabled", false);
                        for (var x = 1; x <= CantRegitro; x++) {
                            $("#Serie1" + x).prop("disabled", false);
                            $("#Serie2" + x).prop("disabled", false);
                            $("#Serie3" + x).prop("disabled", false);
                            $("#Serie4" + x).prop("disabled", false);
                            $("#Serie5" + x).prop("disabled", false);
                            $("#Serie6" + x).prop("disabled", false);
                            $("#Serie7" + x).prop("disabled", false);
                            $("#Serie8" + x).prop("disabled", false);
                            $("#Serie9" + x).prop("disabled", false);
                            $("#Serie10" + x).prop("disabled", false);
                            $("#PaPorLectura" + x).prop("disabled", false);
                            $("#Patron" + x).prop("disabled", false);
                        }

                        for (var x = 1; x <= 10; x++) {
                            if (x <= 5) {
                                $("#SerRepI_" + x).prop("disabled", false);
                                $("#SerRepII_" + x).prop("disabled", false);
                                $("#SerRepIII_" + x).prop("disabled", false);
                                $("#SerRepIV_" + x).prop("disabled", false);
                            }

                            $("#SerVarSal0_" + x).prop("disabled", false);
                            $("#SerVarSal90_" + x).prop("disabled", false);
                            $("#SerVarSal180_" + x).prop("disabled", false);
                            $("#SerVarSal270_" + x).prop("disabled", false);

                            $("#SerVarInt0_" + x).prop("disabled", false);
                            $("#SerVarInt90_" + x).prop("disabled", false);
                            $("#SerVarInt180_" + x).prop("disabled", false);
                            $("#SerVarInt270_" + x).prop("disabled", false);

                            $("#SerVarLon1_" + x).prop("disabled", false);
                            $("#SerVarLon2_" + x).prop("disabled", false);
                        }
                    }

                    if (datos[5] != "[]") {
                        var dataco = JSON.parse(datos[5]);
                        for (var x = 0; x < dataco.length; x++) {
                            if (x == 0) {
                                $("#CMetodo").val(dataco[x].metodo);
                                $("#CPunto").val(dataco[x].punto);
                                $("#CServicio").val(dataco[x].servicio);
                                $("#CObservCerti").val(dataco[x].observacion);
                                $("#CProxima").val(dataco[x].proxima);
                                $("#CEntrega").val(dataco[x].entrega);
                                $("#CAsesorComer").val(dataco[x].asesor);
                            } else {
                                $("#CMetodo").val("<br>" + dataco[x].metodo);
                                $("#CPunto").val("<br>" + dataco[x].punto);
                                $("#CServicio").val("<br>" + dataco[x].servicio);
                                $("#CObservCerti").val("<br>" + dataco[x].observacion);
                                $("#CProxima").val("<br>" + dataco[x].proxima);
                                $("#CEntrega").val("<br>" + dataco[x].entrega);
                                $("#CAsesorComer").val("<br>" + dataco[x].asesor);
                            }
                        }
                    } else {
                        if (IdCalibracion == 0) {
                            ErrorEnter = 1;
                            swal("Acción Cancelada", "Este ingreso no ha sido cotizado por el asesor comercial", "warning");
                        }
                    }

                    if (IdCalibracion == 0) {
                        
                        if (localStorage.getItem(NumCertificado + "-" + "PlaPT1" + "-" + Ingreso + "-" + Revision)) {
                            var data = JSON.parse(localStorage.getItem(NumCertificado + "-" + "PlaPT1" + "-" + Ingreso + "-" + Revision));
                            AplicarTemporal = 0; 
                            $("#PrSerie1").val(data[0].PrSerie1);
                            $("#PrSerie2").val(data[0].PrSerie2);
                            $("#PrSerie3").val(data[0].PrSerie3);
                            $("#PrSerie4").val(data[0].PrSerie4);
                            $("#PrSerie5").val(data[0].PrSerie5);
                            $("#ProximaCali").val(data[0].ProximaCali);

                            $("#Concepto").val(data[0].idconcepto).trigger("change");
                            $("#Ajuste").val(data[0].Ajuste);
                            $("#Suministro").val(data[0].Suministro);
                            $("#Observaciones").val(data[0].Observaciones);
                            $("#Conclusion").val(data[0].Conclusiones);
                            $("#DescripcionAjuste").val(data[0].DescripcionAjuste);
                            
                            HoraInicio = data[0].HoraInicio;
                            GuardaTemp = 1;
                            TiempoTrascurrido();
                            clearInterval(IntervaloTiempo);
                            IntervaloTiempo = null; 
                            IntervaloTiempo = setInterval(TiempoTrascurrido, 180000);
                            $("#CaHoraIni").val(HoraInicio);
                            if (HoraInicio != "") {
                                d = new Date();
                                horaactual = (d.getHours() * 1 < 10 ? "0" : "") + d.getHours() * 1 + ":" + (d.getMinutes() * 1 < 10 ? "0" : "") + d.getMinutes();
                                $("#CaTiempo").val(RestarHoras(HoraInicio, horaactual));
                            }

                            
                            $("#SerRepI_1").val(data[0].SerRepI_1);
                            $("#SerRepI_2").val(data[0].SerRepI_2);
                            $("#SerRepI_3").val(data[0].SerRepI_3);
                            $("#SerRepI_4").val(data[0].SerRepI_4);
                            $("#SerRepI_5").val(data[0].SerRepI_5);
                            $("#SerRepII_1").val(data[0].SerRepII_1);
                            $("#SerRepII_2").val(data[0].SerRepII_2);
                            $("#SerRepII_3").val(data[0].SerRepII_3);
                            $("#SerRepII_4").val(data[0].SerRepII_4);
                            $("#SerRepII_5").val(data[0].SerRepII_5);
                            $("#SerRepIII_1").val(data[0].SerRepIII_1);
                            $("#SerRepIII_2").val(data[0].SerRepIII_2);
                            $("#SerRepIII_3").val(data[0].SerRepIII_3);
                            $("#SerRepIII_4").val(data[0].SerRepIII_4);
                            $("#SerRepIII_5").val(data[0].SerRepIII_5);
                            $("#SerRepIV_1").val(data[0].SerRepIV_1);
                            $("#SerRepIV_2").val(data[0].SerRepIV_2);
                            $("#SerRepIV_3").val(data[0].SerRepIV_3);
                            $("#SerRepIV_4").val(data[0].SerRepIV_4);
                            $("#SerRepIV_5").val(data[0].SerRepIV_5);

                            $("#SerVarSal0_1").val(data[0].SerVarSal0_1);
                            $("#SerVarSal0_2").val(data[0].SerVarSal0_2);
                            $("#SerVarSal0_3").val(data[0].SerVarSal0_3);
                            $("#SerVarSal0_4").val(data[0].SerVarSal0_4);
                            $("#SerVarSal0_5").val(data[0].SerVarSal0_5);
                            $("#SerVarSal0_6").val(data[0].SerVarSal0_6);
                            $("#SerVarSal0_7").val(data[0].SerVarSal0_7);
                            $("#SerVarSal0_8").val(data[0].SerVarSal0_8);
                            $("#SerVarSal0_9").val(data[0].SerVarSal0_9);
                            $("#SerVarSal0_10").val(data[0].SerVarSal0_10);

                            $("#SerVarSal90_1").val(data[0].SerVarSal90_1);
                            $("#SerVarSal90_2").val(data[0].SerVarSal90_2);
                            $("#SerVarSal90_3").val(data[0].SerVarSal90_3);
                            $("#SerVarSal90_4").val(data[0].SerVarSal90_4);
                            $("#SerVarSal90_5").val(data[0].SerVarSal90_5);
                            $("#SerVarSal90_6").val(data[0].SerVarSal90_6);
                            $("#SerVarSal90_7").val(data[0].SerVarSal90_7);
                            $("#SerVarSal90_8").val(data[0].SerVarSal90_8);
                            $("#SerVarSal90_9").val(data[0].SerVarSal90_9);
                            $("#SerVarSal90_10").val(data[0].SerVarSal90_10);

                            $("#SerVarSal180_1").val(data[0].SerVarSal180_1);
                            $("#SerVarSal180_2").val(data[0].SerVarSal180_2);
                            $("#SerVarSal180_3").val(data[0].SerVarSal180_3);
                            $("#SerVarSal180_4").val(data[0].SerVarSal180_4);
                            $("#SerVarSal180_5").val(data[0].SerVarSal180_5);
                            $("#SerVarSal180_6").val(data[0].SerVarSal180_6);
                            $("#SerVarSal180_7").val(data[0].SerVarSal180_7);
                            $("#SerVarSal180_8").val(data[0].SerVarSal180_8);
                            $("#SerVarSal180_9").val(data[0].SerVarSal180_9);
                            $("#SerVarSal180_10").val(data[0].SerVarSal180_10);

                            $("#SerVarSal270_1").val(data[0].SerVarSal270_1);
                            $("#SerVarSal270_2").val(data[0].SerVarSal270_2);
                            $("#SerVarSal270_3").val(data[0].SerVarSal270_3);
                            $("#SerVarSal270_4").val(data[0].SerVarSal270_4);
                            $("#SerVarSal270_5").val(data[0].SerVarSal270_5);
                            $("#SerVarSal270_6").val(data[0].SerVarSal270_6);
                            $("#SerVarSal270_7").val(data[0].SerVarSal270_7);
                            $("#SerVarSal270_8").val(data[0].SerVarSal270_8);
                            $("#SerVarSal270_9").val(data[0].SerVarSal270_9);
                            $("#SerVarSal270_10").val(data[0].SerVarSal270_10);

                            $("#SerVarInt0_1").val(data[0].SerVarInt0_1);
                            $("#SerVarInt0_2").val(data[0].SerVarInt0_2);
                            $("#SerVarInt0_3").val(data[0].SerVarInt0_3);
                            $("#SerVarInt0_4").val(data[0].SerVarInt0_4);
                            $("#SerVarInt0_5").val(data[0].SerVarInt0_5);
                            $("#SerVarInt0_6").val(data[0].SerVarInt0_6);
                            $("#SerVarInt0_7").val(data[0].SerVarInt0_7);
                            $("#SerVarInt0_8").val(data[0].SerVarInt0_8);
                            $("#SerVarInt0_9").val(data[0].SerVarInt0_9);
                            $("#SerVarInt0_10").val(data[0].SerVarInt0_10);

                            $("#SerVarInt90_1").val(data[0].SerVarInt90_1);
                            $("#SerVarInt90_2").val(data[0].SerVarInt90_2);
                            $("#SerVarInt90_3").val(data[0].SerVarInt90_3);
                            $("#SerVarInt90_4").val(data[0].SerVarInt90_4);
                            $("#SerVarInt90_5").val(data[0].SerVarInt90_5);
                            $("#SerVarInt90_6").val(data[0].SerVarInt90_6);
                            $("#SerVarInt90_7").val(data[0].SerVarInt90_7);
                            $("#SerVarInt90_8").val(data[0].SerVarInt90_8);
                            $("#SerVarInt90_9").val(data[0].SerVarInt90_9);
                            $("#SerVarInt90_10").val(data[0].SerVarInt90_10);

                            $("#SerVarInt180_1").val(data[0].SerVarInt180_1);
                            $("#SerVarInt180_2").val(data[0].SerVarInt180_2);
                            $("#SerVarInt180_3").val(data[0].SerVarInt180_3);
                            $("#SerVarInt180_4").val(data[0].SerVarInt180_4);
                            $("#SerVarInt180_5").val(data[0].SerVarInt180_5);
                            $("#SerVarInt180_6").val(data[0].SerVarInt180_6);
                            $("#SerVarInt180_7").val(data[0].SerVarInt180_7);
                            $("#SerVarInt180_8").val(data[0].SerVarInt180_8);
                            $("#SerVarInt180_9").val(data[0].SerVarInt180_9);
                            $("#SerVarInt180_10").val(data[0].SerVarInt180_10);

                            $("#SerVarInt270_1").val(data[0].SerVarInt270_1);
                            $("#SerVarInt270_2").val(data[0].SerVarInt270_2);
                            $("#SerVarInt270_3").val(data[0].SerVarInt270_3);
                            $("#SerVarInt270_4").val(data[0].SerVarInt270_4);
                            $("#SerVarInt270_5").val(data[0].SerVarInt270_5);
                            $("#SerVarInt270_6").val(data[0].SerVarInt270_6);
                            $("#SerVarInt270_7").val(data[0].SerVarInt270_7);
                            $("#SerVarInt270_8").val(data[0].SerVarInt270_8);
                            $("#SerVarInt270_9").val(data[0].SerVarInt270_9);
                            $("#SerVarInt270_10").val(data[0].SerVarInt270_10);

                            $("#SerVarLon1_1").val(data[0].SerVarLon1_1);
                            $("#SerVarLon1_2").val(data[0].SerVarLon1_2);
                            $("#SerVarLon1_3").val(data[0].SerVarLon1_3);
                            $("#SerVarLon1_4").val(data[0].SerVarLon1_4);
                            $("#SerVarLon1_5").val(data[0].SerVarLon1_5);
                            $("#SerVarLon1_6").val(data[0].SerVarLon1_6);
                            $("#SerVarLon1_7").val(data[0].SerVarLon1_7);
                            $("#SerVarLon1_8").val(data[0].SerVarLon1_8);
                            $("#SerVarLon1_9").val(data[0].SerVarLon1_9);
                            $("#SerVarLon1_10").val(data[0].SerVarLon1_10);

                            $("#SerVarLon2_1").val(data[0].SerVarLon2_1);
                            $("#SerVarLon2_2").val(data[0].SerVarLon2_2);
                            $("#SerVarLon2_3").val(data[0].SerVarLon2_3);
                            $("#SerVarLon2_4").val(data[0].SerVarLon2_4);
                            $("#SerVarLon2_5").val(data[0].SerVarLon2_5);
                            $("#SerVarLon2_6").val(data[0].SerVarLon2_6);
                            $("#SerVarLon2_7").val(data[0].SerVarLon2_7);
                            $("#SerVarLon2_8").val(data[0].SerVarLon2_8);
                            $("#SerVarLon2_9").val(data[0].SerVarLon2_9);
                            $("#SerVarLon2_10").val(data[0].SerVarLon2_10);

                            for (var x = 1; x <= CantRegitro; x++) {
                                switch (x) {
                                    case 1:
                                        $("#DecimalPatron" + x).val(data[0].Decimal1);
                                        CambioDecimales(data[0].Decimal1, x);
                                        $("#Serie1" + x).val(data[0].Serie11);
                                        $("#Serie2" + x).val(data[0].Serie21);
                                        $("#Serie3" + x).val(data[0].Serie31);
                                        $("#Serie4" + x).val(data[0].Serie41);
                                        $("#Serie5" + x).val(data[0].Serie51);
                                        $("#Serie6" + x).val(data[0].Serie61);
                                        $("#Serie7" + x).val(data[0].Serie71);
                                        $("#Serie8" + x).val(data[0].Serie81);
                                        $("#Serie9" + x).val(data[0].Serie91);
                                        $("#Serie10" + x).val(data[0].Serie101);
                                        $("#PaPorLectura" + x).val(data[0].PaPorLectura1);
                                        $("#Patron" + x).val(data[0].Patron1).trigger("change");
                                        break;
                                    case 2:
                                        $("#DecimalPatron" + x).val(data[0].Decimal2);
                                        CambioDecimales(data[0].Decimal2, x);
                                        $("#Serie1" + x).val(data[0].Serie12);
                                        $("#Serie2" + x).val(data[0].Serie22);
                                        $("#Serie3" + x).val(data[0].Serie32);
                                        $("#Serie4" + x).val(data[0].Serie42);
                                        $("#Serie5" + x).val(data[0].Serie52);
                                        $("#Serie6" + x).val(data[0].Serie62);
                                        $("#Serie7" + x).val(data[0].Serie72);
                                        $("#Serie8" + x).val(data[0].Serie82);
                                        $("#Serie9" + x).val(data[0].Serie92);
                                        $("#Serie10" + x).val(data[0].Serie102);
                                        $("#PaPorLectura" + x).val(data[0].PaPorLectura2);
                                        $("#Patron" + x).val(data[0].Patron2).trigger("change");
                                        break;
                                    case 3:
                                        $("#DecimalPatron" + x).val(data[0].Decimal3);
                                        CambioDecimales(data[0].Decimal3, x);
                                        $("#Serie1" + x).val(data[0].Serie13);
                                        $("#Serie2" + x).val(data[0].Serie23);
                                        $("#Serie3" + x).val(data[0].Serie33);
                                        $("#Serie4" + x).val(data[0].Serie43);
                                        $("#Serie5" + x).val(data[0].Serie53);
                                        $("#Serie6" + x).val(data[0].Serie63);
                                        $("#Serie7" + x).val(data[0].Serie73);
                                        $("#Serie8" + x).val(data[0].Serie83);
                                        $("#Serie9" + x).val(data[0].Serie93);
                                        $("#Serie10" + x).val(data[0].Serie103);
                                        $("#PaPorLectura" + x).val(data[0].PaPorLectura3);
                                        $("#Patron" + x).val(data[0].Patron3).trigger("change");
                                        break;
                                    case 4:
                                        $("#DecimalPatron" + x).val(data[0].Decimal4);
                                        CambioDecimales(data[0].Decimal4, x);
                                        $("#Serie1" + x).val(data[0].Serie14);
                                        $("#Serie2" + x).val(data[0].Serie24);
                                        $("#Serie3" + x).val(data[0].Serie34);
                                        $("#Serie4" + x).val(data[0].Serie44);
                                        $("#Serie5" + x).val(data[0].Serie54);
                                        $("#Serie6" + x).val(data[0].Serie64);
                                        $("#Serie7" + x).val(data[0].Serie74);
                                        $("#Serie8" + x).val(data[0].Serie84);
                                        $("#Serie9" + x).val(data[0].Serie94);
                                        $("#Serie10" + x).val(data[0].Serie104);
                                        $("#PaPorLectura" + x).val(data[0].PaPorLectura4);
                                        $("#Patron" + x).val(data[0].Patron4).trigger("change");
                                        break;
                                    case 5:
                                        $("#DecimalPatron" + x).val(data[0].Decimal5);
                                        CambioDecimales(data[0].Decimal5, x);
                                        $("#Serie1" + x).val(data[0].Serie15);
                                        $("#Serie2" + x).val(data[0].Serie25);
                                        $("#Serie3" + x).val(data[0].Serie35);
                                        $("#Serie4" + x).val(data[0].Serie45);
                                        $("#Serie5" + x).val(data[0].Serie55);
                                        $("#Serie6" + x).val(data[0].Serie65);
                                        $("#Serie7" + x).val(data[0].Serie75);
                                        $("#Serie8" + x).val(data[0].Serie85);
                                        $("#Serie9" + x).val(data[0].Serie95);
                                        $("#Serie10" + x).val(data[0].Serie105);
                                        $("#PaPorLectura" + x).val(data[0].PaPorLectura5);
                                        $("#Patron" + x).val(data[0].Patron5).trigger("change");
                                        break;
                                    case 6:
                                        $("#DecimalPatron" + x).val(data[0].Decimal6);
                                        CambioDecimales(data[0].Decimal6, x);
                                        $("#Serie1" + x).val(data[0].Serie16);
                                        $("#Serie2" + x).val(data[0].Serie26);
                                        $("#Serie3" + x).val(data[0].Serie36);
                                        $("#Serie4" + x).val(data[0].Serie46);
                                        $("#Serie5" + x).val(data[0].Serie56);
                                        $("#Serie6" + x).val(data[0].Serie66);
                                        $("#Serie7" + x).val(data[0].Serie76);
                                        $("#Serie8" + x).val(data[0].Serie86);
                                        $("#Serie9" + x).val(data[0].Serie96);
                                        $("#Serie10" + x).val(data[0].Serie106);
                                        $("#PaPorLectura" + x).val(data[0].PaPorLectura6);
                                        $("#Patron" + x).val(data[0].Patron6).trigger("change");
                                        break;

                                }
                                                                
                            }
                        }
                        AplicarTemporal = 1; 
                    }

                    if (Certificado == "") {
                        
                        CalcularTotal();
                        RequerirAjuste();
                        $(".punto10").prop("readonly", false);
                        $(".punto10").removeClass("bg-lineas-diagonales");
                        if (CantLecturas * 1 == 5) {
                            $(".punto10").prop("readonly", true);
                            $(".punto10").val("0");
                            $(".punto10").addClass("bg-lineas-diagonales");
                        }
                        AplicarTemporal = 1;

                        if (Cuadrante == "FIJO") {
                            $(".cuadrante").prop("readonly", true);
                            $(".cuadrante").val("0");
                            $(".cuadrante").addClass("bg-lineas-diagonales");
                        }
                    }

                    

                } else {
                    $("#Ingreso").select();
                    $("#Ingreso").focus();
                    ErrorEnter = 1;
                    swal("Acción Cancelada", "El Ingreso número " + numero + " no se ha recibido en el laboratorio", "warning");
                    return false;
                }


                                
                if (ErrorTemp == 1 || ErrorHume == 1)
                    $("#documento").focus();

                
            }, 15);
        }
    } else
        ErrorEnter = 0;
}

function EliminarTemporal() {
    if (IdCalibracion > 0 && Certificado == "") {
        var numero = Ingreso;

        swal.queue([{
            title: ValidarTraduccion('Advertencia'),
            text: ValidarTraduccion('¿Seguro que desea eliminar el temporal del certificado del ingreso número ' + Ingreso + '?'),
            type: 'question',
            showLoaderOnConfirm: true,
            showCancelButton: true,
            confirmButtonText: ValidarTraduccion('Si'),
            cancelButtonText: ValidarTraduccion('No'),
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            preConfirm: function () {
                return new Promise(function (resolve, reject) {
                    $.post("Laboratorio/EliminarCalibTemporal", "numcertificado=" + NumCertificado + "&revision=" + Revision + "&ingreso=" + Ingreso)
                        .done(function (data) {
                            datos = data.split("|");
                            if (datos[0] == "0") {
                                LimpiarTodo();
                                $("#Ingreso").val(numero);
                                BuscarIngreso(numero, "", "0");
                                swal("", datos[1], "");
                                resolve()
                            } else {
                                reject(datos[1]);
                            }
                        })
                })
            }
        }]);
    }
}

function AprobarAjuste(tipo, mensaje) {
    if (IdCalibracion > 0 && Certificado == "") {
        var numero = Ingreso;
        swal.queue([{
            title: ValidarTraduccion('Advertencia'),
            text: ValidarTraduccion('¿Seguro que desea anular los datos para ' + mensaje  + ' del ingreso número ' + Ingreso + ' ? '),
            type: 'question',
            showLoaderOnConfirm: true,
            showCancelButton: true,
            confirmButtonText: ValidarTraduccion('Anular'),
            cancelButtonText: ValidarTraduccion('Cancelar'),
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            preConfirm: function () {
                return new Promise(function (resolve, reject) {
                    $.post("Laboratorio/AnularDatosCertPrevio", "item=" + NumCertificado + "&ingreso=" + Ingreso + "&cliente=" + IdCliente + "&tipo=" + tipo + "&opcion=" + mensaje + "&revision=" + Revision)
                        .done(function (data) {
                            datos = data.split("|");
                            if (datos[0] == "0") {
                                LimpiarTodo();
                                $("#Ingreso").val(numero);
                                BuscarIngreso(numero, "", "0");
                                swal("", datos[1], "");
                                resolve()
                            } else {
                                reject(datos[1]);
                            }
                        })
                })
            }
        }]);
    }
}

function CalcularTotal() {
    CalcularRepetibilidad();
    for (var z = 1; z <= CantRegitro; z++) {
        CalcularSerie("1", "", z,2);
    }
}

function GuardarTemporal() {
    if (Certificado != "")
        return false;

    if (Ingreso > 0 && AplicarTemporal == 1) {

        if (IdCalibracion == 0) {
            if (HoraInicio == "") {
                d = new Date();
                HoraInicio = (d.getHours()*1 < 10 ? "0" : "") + d.getHours() + ":" + (d.getMinutes()*1 < 10 ? "0" : "") + d.getMinutes();
                $("#CaHoraIni").val(HoraInicio);
                HoraCondicion = HoraInicio;
                horaactual = (d.getHours()*1 < 10 ? "0" : "") + d.getHours() + ":" + (d.getMinutes()*1 < 10 ? "0" : "") + d.getMinutes();
                $("#CaTiempo").val(RestarHoras(HoraInicio, horaactual));
                IntervaloTiempo = null;
                IntervaloTiempo = setInterval(TiempoTrascurrido, 60000);
            }
        }
                        
        var archivo = "[{";
        archivo += '"PrSerie1":"' + $("#PrSerie1").val() + '","PrSerie2":"' + $("#PrSerie2").val() + '","PrSerie3":"' + $("#PrSerie3").val() + '",' +
            '"PrSerie4":"' + $("#PrSerie4").val() + '","PrSerie5":"' + $("#PrSerie5").val() + '",' +
            '"HoraInicio":"' + HoraInicio + '","ProximaCali":"' + $("#ProximaCali").val() + '",' +
            '"idconcepto":"' + $("#Concepto").val() + '","Ajuste":"' + $("#Ajuste").val() + '",' +
            '"Suministro":"' + $("#Suministro").val() + '","Observaciones":"' + $("#Observaciones").val() + '","Conclusiones":"' + $("#Conclusion").val() + '","DescripcionAjuste":"' + $("#DescripcionAjuste").val() + '"';
        
        
        for (var x = 1; x <= CantRegitro; x++) {
            archivo += ',"Patron' + x + '":"' + $("#Patron" + x).val() + '"';
            archivo += ',"Decimal' + x + '":"' + $("#DecimalPatron" + x).val() + '"';
            archivo += ',"PaPorLectura' + x + '":"' + $("#PaPorLectura" + x).val() + '"';
            for (var y = 1; y <= 10; y++) {
                archivo += ',"Serie' + y + x + '":"' + $("#Serie" + y + x).val() + '"';
            }
        }

        for (var x = 1; x <= 5; x++) {
            archivo += ',"SerRepI_' + x + '":"' + $("#SerRepI_" + x).val() + '"';
            archivo += ',"SerRepII_' + x + '":"' + $("#SerRepII_" + x).val() + '"';
            archivo += ',"SerRepIII_' + x + '":"' + $("#SerRepIII_" + x).val() + '"';
            archivo += ',"SerRepIV_' + x + '":"' + $("#SerRepIV_" + x).val() + '"';
        }

        for (var x = 1; x <= 10; x++) {
            archivo += ',"SerVarSal0_' + x + '":"' + $("#SerVarSal0_" + x).val() + '"';
            archivo += ',"SerVarSal90_' + x + '":"' + $("#SerVarSal90_" + x).val() + '"';
            archivo += ',"SerVarSal180_' + x + '":"' + $("#SerVarSal180_" + x).val() + '"';
            archivo += ',"SerVarSal270_' + x + '":"' + $("#SerVarSal270_" + x).val() + '"';

            archivo += ',"SerVarInt0_' + x + '":"' + $("#SerVarInt0_" + x).val() + '"';
            archivo += ',"SerVarInt90_' + x + '":"' + $("#SerVarInt90_" + x).val() + '"';
            archivo += ',"SerVarInt180_' + x + '":"' + $("#SerVarInt180_" + x).val() + '"';
            archivo += ',"SerVarInt270_' + x + '":"' + $("#SerVarInt270_" + x).val() + '"';

            archivo += ',"SerVarLon1_' + x + '":"' + $("#SerVarLon1_" + x).val() + '"';
            archivo += ',"SerVarLon2_' + x + '":"' + $("#SerVarLon2_" + x).val() + '"';

        }

        GuardaTemp = 1;
                
        archivo += '}]';
                                
        localStorage.setItem(NumCertificado + "-" + "PlaPT1" + "-" + Ingreso + "-" + Revision, archivo);

        $.jGrowl("Guardando Temporal", { life: 1500, theme: 'growl-warning', header: '' });
    } 
}

function CalcularRepetibilidad() {
    var tipo = [1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 4, 4];
    var repe = [5, 5, 5, 5, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10,];
    var caja_repe = ['SerRepI', 'SerRepII', 'SerRepIII', 'SerRepIV', 'SerVarSal0', 'SerVarSal90', 'SerVarSal180', 'SerVarSal270', 'SerVarInt0', 'SerVarInt90', 'SerVarInt180', 'SerVarInt270', 'SerVarLon1', 'SerVarLon2'];
    var prorep = [1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4, 1, 2]
    var promediorep = 0;
    var cantidadrep = 0;
    for (var x = 0; x < repe.length; x++) {
        promediorep = 0;
        cantidadrep = 0;
        for (var y = 1; y <= repe[x]; y++) {
            if ($.trim($("#" + caja_repe[x] + "_" + y).val()) != "") {
                cantidadrep++;
                promediorep += NumeroDecimal($("#" + caja_repe[x] + "_" + y).val());
            }
        }
        switch (tipo[x]) {
            case 1:
                $("#ProRep" + prorep[x]).val(formato_numero(promediorep / cantidadrep, 3, ".", ",", ""));
                if (cantidadrep > 0)
                    Reproducibilidad[(prorep[x] - 1)] = promediorep / cantidadrep;
                break;
            case 2:
                $("#ProVarSal" + prorep[x]).val(formato_numero(promediorep / cantidadrep, 3, ".", ",", ""));
                if (cantidadrep > 0)
                    Salida[(prorep[x] - 1)] = promediorep / cantidadrep;
                break;
            case 3:
                $("#ProVarInt" + prorep[x]).val(formato_numero(promediorep / cantidadrep, 3, ".", ",", ""));
                if (cantidadrep > 0)
                    Interfaz[(prorep[x] - 1)] = promediorep / cantidadrep;
                break;
            case 4:
                $("#ProVarLon" + prorep[x]).val(formato_numero(promediorep / cantidadrep, 3, ".", ",", ""));
                if (cantidadrep > 0)
                    Longitud[(prorep[x] - 1)] = promediorep / cantidadrep;
                break;
        }
    }
}

function CalcularSerie(code, Caja, fila, tipo) {
    if (code != "1") {
        if (code.keyCode >= 37 && code.keyCode <= 40 || code.keyCode == 13 || code.keyCode == 9)
            return false;
    }


    
        
    if (Caja != "")
        ValidarTexto(Caja, 3);

    var serie1 = NumeroDecimal($("#Serie1" + fila).val());
    var serie2 = NumeroDecimal($("#Serie2" + fila).val());
    var serie3 = NumeroDecimal($("#Serie3" + fila).val());
    var serie4 = NumeroDecimal($("#Serie4" + fila).val());
    var serie5 = NumeroDecimal($("#Serie5" + fila).val());
    var serie6 = NumeroDecimal($("#Serie6" + fila).val());
    var serie7 = NumeroDecimal($("#Serie7" + fila).val());
    var serie8 = NumeroDecimal($("#Serie8" + fila).val());
    var serie9 = NumeroDecimal($("#Serie9" + fila).val());
    var serie10 = NumeroDecimal($("#Serie10" + fila).val());
    var patron = $("#Patron" + fila).val() * 1;

    if (Tolerancia == 0)
        return false;

    var porlectura = 0;
    var lectura = 0;

    if (tipo == 1) {
        porlectura = NumeroDecimal($("#PaPorLectura" + fila).val()) * 1;
        lectura = RangoHasta * porlectura / 100;
    } else {

        porlectura = NumeroDecimal($("#PaPorLectura" + fila).val()) * 1;
        lectura = RangoHasta * porlectura / 100;
    }

    if (fila == 1) {
        $("#PorRep, #PorVarSal, #PorVarInt, #PorVarLon").val(formato_numero(porlectura, 2, ".", ",", ""));
        $("#LecRep, #LecVarSal, #LecVarInt, #LecVarLon").val(formato_numero(lectura, 2, ".", ",", ""));
    }

    
              
        
    $("#SePorcentaje" + fila).html(porlectura);
    $("#NnPorcencate" + fila).html(porlectura);
    $("#CNmPorcencate" + fila).html(porlectura);
    $("#CMePorcencate" + fila).html(porlectura);
    $("#DePorcentaje" + fila).html(porlectura);
    $("#InPorcentaje" + fila).html(porlectura);
    $("#RePorcentaje" + fila).html(porlectura);
    $("#CoPorcentaje" + fila).html(porlectura);
    $("#CoePorLectura" + fila).html(porlectura);
                           
    $("#PaLectura" + fila).val(formato_numero(lectura, 1, ".", ",", ""));
    $("#SeLectura" + fila).html(formato_numero(lectura, 1, ".", ",", ""));
    $("#CoeLectura" + fila).html(formato_numero(lectura, 1, ".", ",", ""));

    $("#NnLectura" + fila).html(formato_numero(lectura * Factor, 1, ".", ",", ""));
    $("#CNmLectura" + fila).html(formato_numero(lectura * Factor, 1, ".", ",", ""));
    $("#CMeLectura" + fila).html(formato_numero(lectura * Factor, 1, ".", ",", ""));
    $("#DeLectura" + fila).html(formato_numero(lectura * Factor, 1, ".", ",", ""));

    $("#ReLectura" + fila).html(formato_numero(lectura * Factor, 1, ".", ",", ""));
    $("#InLectura" + fila).html(formato_numero(lectura * Factor, 1, ".", ",", ""));

    
    $("#PValor1" + fila).html("");
    $("#PValor2" + fila).html("");
    $("#PValor3" + fila).html("");
    $("#PValor4" + fila).html("");
    $("#PValor5" + fila).html("");
    $("#PValor6" + fila).html("");
    $("#PValor7" + fila).html("");
    $("#PValor8" + fila).html("");
    $("#PValor9" + fila).html("");

    $("#PaPunto" + fila).val("");
    $("#UPatron" + fila).val("");
    $("#DPatron" + fila).val("");
    $("#RPatron" + fila).val("");
    $("#IPatron" + fila).val("");

    $("#IncerPretendida" + fila).val("");
    $("#ErrorPretendido" + fila).val("");

   
    if (patron > 0) {
        var parametros = "patron=" + patron + "&factor=" + Factor + "&serie1=" + serie1 + "&serie2=" + serie2 + "&serie3=" + serie3 + "&serie4=" + serie4 + "&serie5=" + serie5 +
            "&serie6=" + serie6 + "&serie7=" + serie7 + "&serie8=" + serie8 + "&serie9=" + serie9 + "&serie10=" + serie10 + "&lectura=" + (lectura * Factor) + "&factor=" + Factor + "&Lecturas=" + CantLecturas + "&plantilla=" + NumCertificado;
        var datos = LlamarAjax("Laboratorio/DatosPatronCerVer07", parametros).split("|");

                
        if (datos[0] == "0") {

            var data = JSON.parse(datos[1]);
            $("#PValor1" + fila).html(data[0].alcance);
            $("#PValor2" + fila).html(data[0].marca);
            $("#PValor3" + fila).html(data[0].modelo);
            $("#PValor4" + fila).html(data[0].serie);
            $("#PValor5" + fila).html(data[0].certificado);
            $("#PValor6" + fila).html(data[0].laboratorio);
            $("#PValor7" + fila).html(data[0].fechacertificado);
            $("#PValor8" + fila).html(data[0].proximacalibracion);
            $("#PValor9" + fila).html(data[0].resolucion);

            var incpatron = datos[12];
            var derpatron = datos[13]
            var incpre = (Tolerancia * (lectura/100)) * (2 / 5);
            var errorpre = ((lectura * Tolerancia)/100) * (1/4);
            var respatron = data[0].resolucion;

            $("#PaPunto" + fila + ", #CoePunto" + fila).html(formato_numero(lectura * Factor, 3, ".", ",", ""));
            $("#IncerExpandida" + fila).val(formato_numero(incpatron, 3, ".", ",", ""));
            $("#DPatron" + fila).val(formato_numero(derpatron, 3, ".", ",", ""));
            $("#RPatron" + fila).val(respatron);
            $("#IncerPretendida" + fila).val(formato_numero(incpre, 5, ".", ",", ""));
            $("#ErrorPretendido" + fila).val(formato_numero(errorpre, 5, ".", ",", ""));
            $("#CoePatron" + fila).html($("#Patron" + fila + " option:selected").text());
            $("#IPatron" + fila).val("");
            if ($("#DecimalPatron" + fila).val() * 1 == 0) {
                numdecimal = $.trim(data[0].resolucion).split(".");
                DecimalesP[fila] = numdecimal[1].length;
                CambioDecimales(DecimalesP[fila], fila);
                $("#DecimalPatron" + fila).val(DecimalesP[fila]); 
            }
            $("#C1Patron" + fila).val(data[0].a0); 
            $("#C2Patron" + fila).val(data[0].a1); 
            $("#C3Patron" + fila).val(data[0].a2); 
            $("#C4Patron" + fila).val(data[0].a3); 

            if (Certificado != "")
                return false;

            var dserie = 0;
            var despromedio = 0;
            var despromedio2 = 0;
            for (var i = 1; i <= CantLecturas; i++) {
                dserie = ((lectura * Factor) - datos[(i + 1)]) * 100 / datos[(i + 1)];
                despromedio += dserie;
                despromedio2 += (datos[(i + 1)] * 1);
                if (dserie >= DesPerm2 && dserie <= DesPerm1) {
                    $("#DeSerie" + i + fila).addClass("bg-default");
                    $("#DeSerie" + i + fila).removeClass("bg-danger");
                } else {
                    $("#DeSerie" + i + fila).removeClass("bg-default");
                    $("#DeSerie" + i + fila).addClass("bg-danger");
                }
            }

            despromedio = despromedio / 5;
            despromedio2 = despromedio2 / 5;


            $("#CNmSerie1" + fila).val(formato_numero(datos[2], DecimalesP[fila], ".", ",", ""));
            $("#CNmSerie2" + fila).val(formato_numero(datos[3], DecimalesP[fila], ".", ",", ""));
            $("#CNmSerie3" + fila).val(formato_numero(datos[4], DecimalesP[fila], ".", ",", ""));
            $("#CNmSerie4" + fila).val(formato_numero(datos[5], DecimalesP[fila], ".", ",", ""));
            $("#CNmSerie5" + fila).val(formato_numero(datos[6], DecimalesP[fila], ".", ",", ""));
            $("#CNmSerie6" + fila).val(formato_numero(datos[7], DecimalesP[fila], ".", ",", ""));
            $("#CNmSerie7" + fila).val(formato_numero(datos[8], DecimalesP[fila], ".", ",", ""));
            $("#CNmSerie8" + fila).val(formato_numero(datos[9], DecimalesP[fila], ".", ",", ""));
            $("#CNmSerie9" + fila).val(formato_numero(datos[10], DecimalesP[fila], ".", ",", ""));
            $("#CNmSerie10" + fila).val(formato_numero(datos[11], DecimalesP[fila], ".", ",", ""));

            $("#F_CNmSerie1" + fila).html($("#F_CNmSerie1" + fila).attr("formula") + "<br>Fila = " + fila + "<br>=" + serie1 + "*" + Factor);
            $("#F_CNmSerie2" + fila).html($("#F_CNmSerie2" + fila).attr("formula") + "<br>Fila = " + fila + "<br>=" + serie2 + "*" + Factor);
            $("#F_CNmSerie3" + fila).html($("#F_CNmSerie3" + fila).attr("formula") + "<br>Fila = " + fila + "<br>=" + serie3 + "*" + Factor);
            $("#F_CNmSerie4" + fila).html($("#F_CNmSerie4" + fila).attr("formula") + "<br>Fila = " + fila + "<br>=" + serie4 + "*" + Factor);
            $("#F_CNmSerie5" + fila).html($("#F_CNmSerie5" + fila).attr("formula") + "<br>Fila = " + fila + "<br>=" + serie5 + "*" + Factor);
            $("#F_CNmSerie6" + fila).html($("#F_CNmSerie6" + fila).attr("formula") + "<br>Fila = " + fila + "<br>=" + serie6 + "*" + Factor);
            $("#F_CNmSerie7" + fila).html($("#F_CNmSerie7" + fila).attr("formula") + "<br>Fila = " + fila + "<br>=" + serie7 + "*" + Factor);
            $("#F_CNmSerie8" + fila).html($("#F_CNmSerie8" + fila).attr("formula") + "<br>Fila = " + fila + "<br>=" + serie8 + "*" + Factor);
            $("#F_CNmSerie9" + fila).html($("#F_CNmSerie9" + fila).attr("formula") + "<br>Fila = " + fila + "<br>=" + serie9 + "*" + Factor);
            $("#F_CNmSerie10" + fila).html($("#F_CNmSerie10" + fila).attr("formula") + "<br>Fila = " + fila + "<br>=" + serie10 + "*" + Factor);
            $("#F_NmSerPromedio" + fila).html($("#F_NmSerPromedio" + fila).attr("formula") + "<br>Fila = " + fila + "<br>=Promedio(" + (serie1 * Factor) + "+" + (serie2 * Factor) + "+" + (serie3 * Factor) + "+<br>" + (serie4 * Factor) + "+" + (serie5 * Factor) + "+" + (serie6 * Factor) + "+" + (serie7 * Factor) + "+<br>" + (serie8 * Factor) + "+" + (serie9 * Factor) + "+" + (serie10 * Factor) + ")");
            

            $("#CMeSerie1" + fila).val(formato_numero(datos[2] / Factor, DecimalesP[fila], ".", ",", ""));
            $("#CMeSerie2" + fila).val(formato_numero(datos[3] / Factor, DecimalesP[fila], ".", ",", ""));
            $("#CMeSerie3" + fila).val(formato_numero(datos[4] / Factor, DecimalesP[fila], ".", ",", ""));
            $("#CMeSerie4" + fila).val(formato_numero(datos[5] / Factor, DecimalesP[fila], ".", ",", ""));
            $("#CMeSerie5" + fila).val(formato_numero(datos[6] / Factor, DecimalesP[fila], ".", ",", ""));
            $("#CMeSerie6" + fila).val(formato_numero(datos[7] / Factor, DecimalesP[fila], ".", ",", ""));
            $("#CMeSerie7" + fila).val(formato_numero(datos[8] / Factor, DecimalesP[fila], ".", ",", ""));
            $("#CMeSerie8" + fila).val(formato_numero(datos[9] / Factor, DecimalesP[fila], ".", ",", ""));
            $("#CMeSerie9" + fila).val(formato_numero(datos[10] / Factor, DecimalesP[fila], ".", ",", ""));
            $("#CMeSerie10" + fila).val(formato_numero(datos[11] / Factor, DecimalesP[fila], ".", ",", ""));


            if (despromedio >= DesPerm2 && despromedio <= DesPerm1) {
                $("#Promedio1" + fila).addClass("bg-default");
                $("#Promedio1" + fila).removeClass("bg-danger");
            } else {
                $("#Promedio1" + fila).removeClass("bg-default");
                $("#Promedio1" + fila).addClass("bg-danger");
            }

            $("#DeSerie1" + fila).val(formato_numero(((lectura * Factor) - datos[2]) * 100 / datos[2], DecimalesP[fila], ".", ",", ""));
            $("#DeSerie2" + fila).val(formato_numero(((lectura * Factor) - datos[3]) * 100 / datos[3], DecimalesP[fila], ".", ",", ""));
            $("#DeSerie3" + fila).val(formato_numero(((lectura * Factor) - datos[4]) * 100 / datos[4], DecimalesP[fila], ".", ",", ""));
            $("#DeSerie4" + fila).val(formato_numero(((lectura * Factor) - datos[5]) * 100 / datos[5], DecimalesP[fila], ".", ",", ""));
            $("#DeSerie5" + fila).val(formato_numero(((lectura * Factor) - datos[6]) * 100 / datos[6], DecimalesP[fila], ".", ",", ""));
            $("#DeSerie6" + fila).val(formato_numero(((lectura * Factor) - datos[7]) * 100 / datos[7], DecimalesP[fila], ".", ",", ""));
            $("#DeSerie7" + fila).val(formato_numero(((lectura * Factor) - datos[8]) * 100 / datos[8], DecimalesP[fila], ".", ",", ""));
            $("#DeSerie8" + fila).val(formato_numero(((lectura * Factor) - datos[9]) * 100 / datos[9], DecimalesP[fila], ".", ",", ""));
            $("#DeSerie9" + fila).val(formato_numero(((lectura * Factor) - datos[10]) * 100 / datos[10], DecimalesP[fila], ".", ",", ""));
            $("#DeSerie10" + fila).val(formato_numero(((lectura * Factor) - datos[11]) * 100 / datos[11], DecimalesP[fila], ".", ",", ""));

            var promedio = 0;
            var promedionm = 0;
            var promediocnm = 0;
            var promediocor = 0;
            var promediodes = 0;
            var cantidad = 0
            var desviacion = datos[14];
            var bet = datos[15];
            for (var x = 1; x <= CantLecturas; x++) {
                if ($("#Serie" + x + fila).val() != "") {
                    cantidad++;
                    promedio += NumeroDecimal($("#Serie" + x + fila).val());
                    promedionm += NumeroDecimal($("#Serie" + x + fila).val()) * Factor;
                    promediocnm += datos[(x + 1)]*1;
                    promediocor += datos[(x + 1)] / Factor;
                    promediodes += ((lectura * Factor) - datos[(x + 1)]) * 100 / datos[(x + 1)];
                }
            }

            promedio = promedio / cantidad;
            promedionm = promedionm / cantidad;
                       
            promediocnm = promediocnm / cantidad;
            promediocor = promediocor / cantidad;
            promediodes = promediodes / cantidad;
                        
            $("#NmSerPromedio" + fila).val(formato_numero(promedionm, DecimalesP[fila], ".", ",", ""));
            $("#CNmSerPromedio" + fila).val(formato_numero(promediocnm, DecimalesP[fila], ".", ",", ""));
            $("#CMeSerPromedio" + fila).val(formato_numero(promediocor, DecimalesP[fila], ".", ",", ""));
            $("#DeSerPromedio" + fila).val(formato_numero(promediodes, DecimalesP[fila], ".", ",", ""));
            
            var promedio2 = Math.abs(lectura - promedio);

            if (cantidad == CantLecturas) {
                if (promedio2 > ((Tolerancia / 100) * promedio)) {
                    $("#SerPromedio" + fila).val("REQUIERE AJUSTE");
                    $("#SerPromedio" + fila).removeClass("text-success");
                    $("#SerPromedio" + fila).addClass("text-danger");
                } else {
                    $("#SerPromedio" + fila).val("DENTRO");
                    $("#SerPromedio" + fila).addClass("text-success");
                    $("#SerPromedio" + fila).removeClass("text-danger");
                }
            } else {
                $("#SerPromedio" + fila).val("");
            }
                        
                                    
            var maxrep = 0;
            var minrep = 990000000;
            var maxsal = 0;
            var minsal = 990000000;
            var maxint = 0;
            var minint =9900000000;
                        
            for (var x = 0; x < 4; x++) {
                if (Reproducibilidad[x]*1 > maxrep*1)
                    maxrep = Reproducibilidad[x];
                if (Reproducibilidad[x]*1 < minrep*1)
                    minrep = Reproducibilidad[x];
                                
                if (Salida[x] > maxsal)
                    maxsal = Salida[x];
                if (Salida[x] < minsal)
                    minsal = Salida[x];

                if (Interfaz[x] > maxint)
                    maxint = Interfaz[x];
                if (Interfaz[x] < minint)
                    minint = Interfaz[x];
                                
            }
                                                
            var reresolucion = Resolucion * Factor;
            var rereproducibilidad = maxrep - minrep;
            var resalida = maxsal - minsal;
            var reinterfaz = maxint - minint;
            var relongitud = Longitud[0] - Longitud[1];

            var rerepetibilidad = desviacion;
            var reupatron = incpatron;
            var rerpatron = respatron;
            var redpatron = derpatron;
                                               
            $("#ReResolucion" + fila).val(formato_numero(reresolucion, DecimalesP[fila], ".", ",", ""));
            $("#ReReproducibilidad" + fila).val(formato_numero(rereproducibilidad, DecimalesP[fila], ".", ",", ""));
            $("#ReGeoSalida" + fila).val(formato_numero(resalida, DecimalesP[fila], ".", ",", ""));
            $("#ReGeoInterfaz" + fila).val(formato_numero(reinterfaz, DecimalesP[fila], ".", ",", ""));
            $("#ReLongitud" + fila).val(formato_numero(relongitud, DecimalesP[fila], ".", ",", ""));
            $("#ReRepetibilidad" + fila).val(formato_numero(rerepetibilidad, DecimalesP[fila], ".", ",", ""));
            $("#ReUPatron" + fila).val(formato_numero(reupatron, DecimalesP[fila], ".", ",", ""));
            $("#ReRPatron" + fila).val(formato_numero(rerpatron, DecimalesP[fila], ".", ",", ""));
            $("#ReDPatron" + fila).val(formato_numero(redpatron, DecimalesP[fila], ".", ",", ""));

            var coresolucion = ((reresolucion * 0.5) / Math.sqrt(3)) * (100 / promediocnm);
            var coreproducibilidad = ((rereproducibilidad * 0.5) / Math.sqrt(3)) * (100 / promediocnm);
            var cosalida = ((resalida * 0.5) / Math.sqrt(3)) * (100 / promediocnm);
            var cointerfaz = ((reinterfaz * 0.5) / Math.sqrt(3)) * (100 / promediocnm);
            var colongitud = ((relongitud * 0.5) / Math.sqrt(3)) * (100 / promediocnm);
            var corepetibilidad = (rerepetibilidad / Math.sqrt(CantLecturas)) * (100 / promediocnm);
            var coupatron = (reupatron / 2) * (100 / promediocnm);
            var corpatron = ((rerpatron * 0.5) / Math.sqrt(3)) * (100 / promediocnm);
            var codpatron = (redpatron / Math.sqrt(3)) * (100 / promediocnm);
                        
            $("#CoResolucion" + fila).val(formato_numero(coresolucion, 4, ".", ",", ""));
            $("#CoReproducibilidad" + fila).val(formato_numero(coreproducibilidad, 4, ".", ",", ""));
            $("#CoGeoSalida" + fila).val(formato_numero(cosalida, 4, ".", ",", ""));
            $("#CoGeoInterfaz" + fila).val(formato_numero(cointerfaz, 4, ".", ",", ""));
            $("#CoLongitud" + fila).val(formato_numero(colongitud, 4, ".", ",", ""));
            $("#CoRepetibilidad" + fila).val(formato_numero(corepetibilidad, 4, ".", ",", ""));
            $("#CoUPatron" + fila).val(formato_numero(coupatron, 4, ".", ",", ""));
            $("#CoRPatron" + fila).val(formato_numero(corpatron, 4, ".", ",", ""));
            $("#CoDPatron" + fila).val(formato_numero(codpatron, 4, ".", ",", ""));

            var valormedio = Math.abs(promediodes);
            var valorerror = bet*1;
            var sumacuadra = (coresolucion * coresolucion) + (coreproducibilidad * coreproducibilidad) + (cosalida * cosalida) + (cointerfaz * cointerfaz) +
                (colongitud * colongitud) + (corepetibilidad * corepetibilidad) + (coupatron * coupatron) + (corpatron * corpatron) + (codpatron * codpatron);
            var incertidumbre = Math.sqrt(sumacuadra);
            var sumapotencia = (Math.pow(coresolucion, 4) / 400) + (Math.pow(coreproducibilidad, 4) / 400) + (Math.pow(cosalida, 4) / 400) + (Math.pow(cointerfaz, 4) / 400) +
                (Math.pow(colongitud, 4) / 400) + (Math.pow(corepetibilidad, 4) / 400) + (Math.pow(coupatron, 4) / 400) + (Math.pow(corpatron, 4) / 400) + (Math.pow(codpatron, 4) / 400);
                        
            
            var gradolibertad = Math.pow(incertidumbre, 4) / sumapotencia;
            TStuden = Calcular_TStudent(gradolibertad);

            
            var expandida = (incertidumbre * TStuden) + valormedio + valorerror;

            
            //$("#InNExpandida2" + fila).val(formato_numero(valormedio, calculardecimal(expandida2), ".", ",", ""));
                        
            $("#InValorMed" + fila).val(formato_numero(valormedio, 3, ".", ",", ""));
            $("#InValorMax" + fila).val(formato_numero(valorerror, 5, ".", ",", ""));
            $("#InCombinada" + fila).val(formato_numero(incertidumbre, 3, ".", ",", ""));
            $("#InFactor" + fila).val(formato_numero(TStuden, 2, ".", ",", ""));
            $("#InExpandida" + fila).val(formato_numero(expandida, 3, ".", ",", ""));
            $("#InNGrados" + fila).val(formato_numero(gradolibertad, 3, ".", ",", ""));
            $("#InPromedio" + fila).val(formato_numero(promediocnm, 4, ".", ",", ""));
            $("#InDesPromedio" + fila).val(formato_numero(promediodes, 3, ".", ",", ""));
            
            //$("#InNExpandida3" + fila).val(formato_numero(expandida3, calculardecimal(expandida3), ".", ",", ""));

            
            var lecurapatron = promediocor;
            var erromedpor = promediodes;
            var errormed = lectura - promediocor;
            var incertexpmed = lectura * (expandida / 100);
            var incertexppor = expandida;
            var faccobertura = TStuden;
                                    
            $("#ReMeLecturaIBC" + fila).html(formato_numero(Math.round(lectura), 0, ".", ",", ""));
            $("#ReMeLecturapatron" + fila).html(formato_numero(lecurapatron, DecimalesP[fila], ".", ",", ""));
            $("#ReMeErroMediMed" + fila).val(formato_numero(errormed, calculardecimal(incertexpmed), ".", ",", ""));
            $("#ReMeErroMediPor" + fila).val(formato_numero(erromedpor, calculardecimal(incertexppor), ".", ",", ""));
            $("#ReMeIncExpMed" + fila).val(formato_numero(incertexpmed, calculardecimal(incertexpmed), ".", ",", ""));
            $("#ReMeIncExpPor" + fila).val(formato_numero(incertexppor, calculardecimal(incertexppor), ".", ",", ""));
            $("#ReMeFacCober" + fila).val(formato_numero(faccobertura, 2, ".", ",", ""));

            var mnlecurapatron = promediocnm;
            var mnerromedpor = promediodes;
            var mnerrormed = (lectura * Factor) - promediocnm;
            var mnincertexppor = expandida;
            var mnincertexpmed = (lectura*Factor)*(expandida/100);
            var mnfaccobertura = TStuden;

            $("#ReMnLecturaIBC" + fila).html(formato_numero(Math.round(lectura * Factor), 0, ".", ",", ""));
            $("#ReMnLecturapatron" + fila).html(formato_numero(mnlecurapatron, DecimalesP[fila], ".", ",", ""));
            $("#ReMnErroMediMed" + fila).val(formato_numero(mnerrormed, calculardecimal(incertexpmed), ".", ",", ""));
            $("#ReMnErroMediPor" + fila).val(formato_numero(mnerromedpor, calculardecimal(incertexppor), ".", ",", ""));
            $("#ReMnIncExpMed" + fila).val(formato_numero(mnincertexpmed, calculardecimal(incertexpmed), ".", ",", ""));
            $("#ReMnIncExpPor" + fila).val(formato_numero(mnincertexppor, calculardecimal(incertexppor), ".", ",", ""));
            $("#ReMnFacCober" + fila).val(formato_numero(mnfaccobertura, 2, ".", ",", ""));

        } else {
            swal("Acción Cancelada", datos[1], "warning");
            despatron = "";
            $("#Patron" + fila).val("").trigger("change");
        }
    }



    $("#NmSerie1" + fila).val(formato_numero(serie1 * Factor, DecimalesP[fila], ".", ",", ""));
    $("#NmSerie2" + fila).val(formato_numero(serie2 * Factor, DecimalesP[fila], ".", ",", ""));
    $("#NmSerie3" + fila).val(formato_numero(serie3 * Factor, DecimalesP[fila], ".", ",", ""));
    $("#NmSerie4" + fila).val(formato_numero(serie4 * Factor, DecimalesP[fila], ".", ",", ""));
    $("#NmSerie5" + fila).val(formato_numero(serie5 * Factor, DecimalesP[fila], ".", ",", ""));
    $("#NmSerie6" + fila).val(formato_numero(serie6 * Factor, DecimalesP[fila], ".", ",", ""));
    $("#NmSerie7" + fila).val(formato_numero(serie7 * Factor, DecimalesP[fila], ".", ",", ""));
    $("#NmSerie8" + fila).val(formato_numero(serie8 * Factor, DecimalesP[fila], ".", ",", ""));
    $("#NmSerie9" + fila).val(formato_numero(serie9 * Factor, DecimalesP[fila], ".", ",", ""));
    $("#NmSerie10" + fila).val(formato_numero(serie10 * Factor, DecimalesP[fila], ".", ",", ""));

    $("#F_NmSerie1" + fila).html($("#F_NmSerie1" + fila).attr("formula") + "<br>Fila = " + fila + "<br>=" + serie1 + "*" + Factor);
    $("#F_NmSerie2" + fila).html($("#F_NmSerie2" + fila).attr("formula") + "<br>Fila = " + fila + "<br>=" + serie2 + "*" + Factor);
    $("#F_NmSerie3" + fila).html($("#F_NmSerie3" + fila).attr("formula") + "<br>Fila = " + fila + "<br>=" + serie3 + "*" + Factor);
    $("#F_NmSerie4" + fila).html($("#F_NmSerie4" + fila).attr("formula") + "<br>Fila = " + fila + "<br>=" + serie4 + "*" + Factor);
    $("#F_NmSerie5" + fila).html($("#F_NmSerie5" + fila).attr("formula") + "<br>Fila = " + fila + "<br>=" + serie5 + "*" + Factor);
    $("#F_NmSerie6" + fila).html($("#F_NmSerie6" + fila).attr("formula") + "<br>Fila = " + fila + "<br>=" + serie6 + "*" + Factor);
    $("#F_NmSerie7" + fila).html($("#F_NmSerie7" + fila).attr("formula") + "<br>Fila = " + fila + "<br>=" + serie7 + "*" + Factor);
    $("#F_NmSerie8" + fila).html($("#F_NmSerie8" + fila).attr("formula") + "<br>Fila = " + fila + "<br>=" + serie8 + "*" + Factor);
    $("#F_NmSerie9" + fila).html($("#F_NmSerie9" + fila).attr("formula") + "<br>Fila = " + fila + "<br>=" + serie9 + "*" + Factor);
    $("#F_NmSerie10" + fila).html($("#F_NmSerie10" + fila).attr("formula") + "<br>Fila = " + fila + "<br>=" + serie10 + "*" + Factor);
    $("#F_NmSerPromedio" + fila).html($("#F_NmSerPromedio" + fila).attr("formula") + "<br>Fila = " + fila + "<br>=Promedio(" + (serie1 * Factor) + "+" + (serie2 * Factor) + "+" + (serie3 * Factor) + "+<br>" + (serie4 * Factor) + "+" + (serie5 * Factor) + "+" + (serie6 * Factor) + "+" + (serie7 * Factor) + "+<br>" + (serie8 * Factor) + "+" + (serie9 * Factor) + "+" + (serie10 * Factor) + ")");
    
              

    Graficar();
    GraficarDesviacion();
        
        
}


function calculardecimal(numero) {
    numero = $.trim(numero);
    var contador = 0;
    var punto = 0;
    decimal = 0;
    for (var i = 0; i < numero.length; i++) {
        if (numero.charAt(i) != "." && numero.charAt(i) != ",") {
            if (numero.charAt(i) * 1 > 0 || contador > 0) {
                contador++;
                if (punto > 0)
                    decimal++;
                if (contador == 2)
                    break;
            }
        } else
            punto++;
    }
    return decimal;
}


function Graficar() {
    
    var colores = ['rgb(255, 99, 132)', 'rgb(255, 159, 64)', 'rgb(255, 205, 86)', 'rgb(75, 192, 192)', 'rgb(54, 162, 235)', 'rgb(153, 102, 255)', 'rgb(201, 203, 207)'];
    var varserie = 0;
    var TablaGrafica = "";

    var config = {
        type: 'scatter',
        data: {
            datasets: []
        },
        options: {
            responsive: true,
            title: {
                display: true,
                text: 'PAR TORSIONAL NOMINAL (' + $("#Medida").html() + ') VS DESVIACIONES (%)'
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            legend: {
                position: 'right',
                label: {
                    FontSize: '20px'
                }
            }
        },
        lineAtIndex: 2
    }

    TablaGrafica += '<tr class="tabcertr"><th class="bg-gris">EPM(+)</th>';
    
    var newDataset = {
        label: 'EPM(+)',
        fill: false,
        backgroundColor: colores[0],
        borderColor: colores[0],
        borderDash: [5, 5],
        data: [],
    };
    var punto;
    for (var i = 1; i <= CantRegitro; i++) {
        TablaGrafica += "<td>" + NumeroDecimal($("#PaLectura" + i).val()) + " ; " + DesPerm1 + "</td>"; 
        punto = {
            x: NumeroDecimal($("#PaLectura" + i).val()),
            y: DesPerm1
        }
        newDataset.data.push(punto);
    }
    config.data.datasets.push(newDataset);

    
    for (var serie = 1; serie <= 5; serie++) {
        TablaGrafica += '<tr class="tabcertr"><th class="bg-gris">Serie' + serie + '</th>';
        newDataset = {
            label: 'Serie ' + serie,
            fill: false,
            borderDash: [5, 5],
            pointRadius: 10,
            pointHoverRadius: 15,
            showLine: false,
            borderColor: colores[serie],
            backgroundColor: colores[serie],
            data: [],
        };
        for (var i = 1; i <= CantRegitro; i++) {
            varserie = ($("#DeSerie" + serie + i).val() != "Infinity" ? NumeroDecimal($("#DeSerie" + serie + i).val()) : 0);
            TablaGrafica += "<td>" + NumeroDecimal($("#PaLectura" + i).val()) + " ; " + varserie + "</td>"; 
            punto = {
                x: NumeroDecimal($("#PaLectura" + i).val()),
                y: varserie
            }
            newDataset.data.push(punto);
        }
        config.data.datasets.push(newDataset);
    }

    TablaGrafica += '<tr class="tabcertr"><th class="bg-gris">EPM(-)</th>';
    
    newDataset = {
        label: 'EPM(-)',
        fill: false,
        borderColor: colores[0],
        backgroundColor: colores[0],
        borderDash: [5, 5],
        data: [],
    };
    for (var i = 1; i <= CantRegitro; i++) {
        TablaGrafica += "<td>" + NumeroDecimal($("#PaLectura" + i).val()) + " ; " + DesPerm2 + "</td>"; 
        punto = {
            x: NumeroDecimal($("#PaLectura" + i).val()),
            y: DesPerm2
        }
        newDataset.data.push(punto);
    }
    TablaGrafica += "</tr>";
    config.data.datasets.push(newDataset);

    if (window.myLine != undefined) {
        window.myLine.destroy();
    }
    window.myLine = new Chart(ctxgra, config);   
    window.myLine.update();
    $("#TBGrafica1").html(TablaGrafica);
}

function GraficarDesviacion() {

    var colores = ['rgb(255, 99, 132)', 'rgb(255, 159, 64)', 'rgb(255, 205, 86)', 'rgb(75, 192, 192)', 'rgb(54, 162, 235)', 'rgb(153, 102, 255)', 'rgb(201, 203, 207)', 'rgb(156, 156, 156)'];
    var varserie = 0;
    var TablaGrafica;
    var config = {
        type: 'scatter',
        data: {
            datasets: []
        },
        options: {
            legendCallback: function (chart) {
                '<b>Prueba de leyenda en html</b>'
            },
            
            responsive: true,
            
            title: {
                display: true,
                text: 'PAR TORSIONAL NOMINAL (' + $("#Medida").html() + ') VS DESVIACION PROMEDIO (%)'
            },
            scales: {
                xAxes: [{
                    display: true,
                    labelString: 'Par torsional nominal en lbf·ft'

                }],
                yAxes: [{
                    display: true,
                    labelString: 'Desviación ± U expandida en %'
                }]
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            legend: {
                position: 'right'
            }
        },
        lineAtIndex: 2
    }

    TablaGrafica += '<tr class="tabcertr"><th class="bg-gris">EPM(+)</th>';

    var newDataset = {
        label: 'EPM(+)',
        fill: false,
        backgroundColor: colores[0],
        borderColor: colores[0],
        borderDash: [5, 5],
        data: [],
    };
    var punto;
    for (var i = 1; i <= CantRegitro; i++) {
        TablaGrafica += "<td>" + NumeroDecimal($("#InLectura" + i).val()) + " ; " + DesPerm1 + "</td>"; 
        punto = {
            x: NumeroDecimal($("#InLectura" + i).html()),
            y: DesPerm1
        }
        newDataset.data.push(punto);
    }
    config.data.datasets.push(newDataset);

    TablaGrafica += "</tr>";
    TablaGrafica += '<tr class="tabcertr"><th class="bg-gris">Desviación Promedi</th>';

    var newDataset = {
        label: 'Desviación Promedio',
        fill: false,
        backgroundColor: colores[4],
        borderColor: colores[4],
        pointRadius: 10,
        pointHoverRadius: 15,
        showLine: false,
        data: [],
    };
    var punto;
    for (var i = 1; i <= CantRegitro; i++) {
        TablaGrafica += "<td>" + NumeroDecimal($("#InLectura" + i).html()) + " ; " + NumeroDecimal($("#InDesPromedio" + i).val()) + "</td>"; 
        punto = {
            x: NumeroDecimal($("#InLectura" + i).html()),
            y: NumeroDecimal($("#InDesPromedio" + i).val())
        }
        newDataset.data.push(punto);
    }
    config.data.datasets.push(newDataset);

    TablaGrafica += "</tr>";

    TablaGrafica += '<tr class="tabcertr"><th class="bg-gris">EPM(-)</th>';

    newDataset = {
        label: 'EPM(-)',
        fill: false,
        borderColor: colores[0],
        backgroundColor: colores[0],
        borderDash: [5, 5],
        data: [],
    };
    for (var i = 1; i <= CantRegitro; i++) {
        TablaGrafica += "<td>" + NumeroDecimal($("#InLectura" + i).val()) + " ; " + DesPerm2 + "</td>"; 
        punto = {
            x: NumeroDecimal($("#InLectura" + i).html()),
            y: DesPerm2
        }
        newDataset.data.push(punto);
    }
    config.data.datasets.push(newDataset);

    TablaGrafica += "</tr>";

    if (window.myDesviacion != undefined) {
        window.myDesviacion.destroy();
    }

    window.myDesviacion = new Chart(ctdesviacion, config);
    window.myDesviacion.update();

   
    for (var fila = 1; fila <= CantRegitro; fila++) {
        newDataset = {
            display: false,
            label:'Error',
            borderColor: colores[7],
            backgroundColor: colores[7],
            data: [],
        };
        varserie = NumeroDecimal($("#InDesPromedio" + fila).val()) - NumeroDecimal($("#InExpandida" + fila).val());
        punto = {
            x: NumeroDecimal($("#InLectura" + fila).html()),
            y: varserie
        }
        newDataset.data.push(punto);

        punto = {
            x: NumeroDecimal($("#InLectura" + fila).html()),
            y: NumeroDecimal($("#InDesPromedio" + fila).val())
        }
        newDataset.data.push(punto);

        varserie = NumeroDecimal($("#InDesPromedio" + fila).val()) + NumeroDecimal($("#InExpandida" + fila).val());
        punto = {
            x: NumeroDecimal($("#InLectura" + fila).html()),
            y: varserie
        }
        newDataset.data.push(punto);

        config.data.datasets.push(newDataset);
    }
    

    if (window.myDesviacion != undefined) {
        window.myDesviacion.destroy();
    }

    window.myDesviacion = new Chart(ctdesviacion, config);
    window.myDesviacion.update();

    $("#TBGrafica2").html(TablaGrafica);
    

}


function CambioPatron(fila) {
    CalcularSerie("1", "", fila,2);
    GuardarTemporal();
}


$.connection.hub.start().done(function () {
    $("#FormPrevia").submit(function (e) {
        e.preventDefault();
        var concepto = $("#Concepto").val() * 1;
        var desconcepto = $("#Concepto option:selected").text();
        var ajuste = $.trim($("#Ajuste").val());
        var suministro = $.trim($("#Suministros").val());
        if (concepto == 2) {
            if (ajuste == "" && suministro == "") {
                $("#Ajuste").focus();
                swal("Acción Cancelada", "Debe de ingresar un ajuste o suministro", "warning");
                return false;
            }
        }

        if (Certificado != "") {
            swal("Acción Cancelada", "Ya se generó el certificado de este ingreso", "warning");
            return false;
        }
    

        if (concepto == 1) {

            CalcularTemperatura(1);

            if (TempMax == 0 || TempMin == 100000000 || HumeMax == 0 || HumeMin == 100000000) {
                swal("Acción Cancelada", "No hay condiciones ambientales cargadas", "warning");
                return false;
            }

            var mensaje = "";
            if (TempVar > 1 || HumeMax >= 75)
                mensaje = "temperatura y humedad";
            else {
                if (TempVar == 1)
                    mensaje = "temperatura";
                if (HumeMax >= 75)
                    mensaje = "humedad";
            }

            if (mensaje != "") {
                $("#documento").focus();
                swal("Acción Cancelada", "Condiciones de " + mensaje + " no adecuadas, para la calibración... No podrá General el certificado", "warning");
                EscucharMensaje("Condiciones de " + mensaje + " no adecuadas, para la calibración. No podrá General el certificado");
                return false;
            }
        }
    
        ActivarLoad();
        setTimeout(function () {
            d = new Date();
            HoraFinal = (d.getHours() * 1 < 10 ? "0" : "") + d.getHours() + ":" + (d.getMinutes() * 1 < 10 ? "0" : "") + d.getMinutes();
            tiempo = RestarHoras(HoraInicio, HoraFinal);
            var parametros = $("#FormPrevia").serialize().replace(/\%2C/g, "") + "&id=" + IdCalibracion + "&HoraIni=" + HoraInicio + "&HoraFinal=" + HoraFinal + "&TiempoTrans=" + tiempo + "&magnitud=6" +
                "&FechaRec=" + $("#FechaIng").html() + "&Factor=" + Factor + "&revision=" + Revision + "&version=" + IdVersion + "&Lecturas=" + CantLecturas + "&Concepto=" + desconcepto;

            var datos = LlamarAjax("Laboratorio/GuardaCertificadoPT1_V07", parametros).split("|");
            DesactivarLoad();
            if (datos[0] == "0") {
                if (IdCalibracion == 0) {
                    $("#Certificado").html("");
                    parametros = "tempmax=" + $("#CoTempMax").val().replace(".", ",") + "&tempmin=" + $("#CoTempMin").val().replace(".", ",") + "&tempvar=" + formato_numero(TempVar, 1, ",", ".", "") + " °C" +
                        "&humemax=" + $("#CoHumeMax").val().replace(".", ",") + "&humemin=" + $("#CoHumeMin").val().replace(".", ",") + "&humevar=" + formato_numero(HumeVar, 1, ",", ".", "") + " %HR&ingreso=" + Ingreso + "&numero=" + NumCertificado + "&revision=" + Revision;
                    LlamarAjax("Laboratorio/GuardarTemperatura", parametros);
                }
            
                clearInterval(IntervaloTiempo);
                IdCalibracion = datos[2];
                GuardaTemp = 0;
                $("#CaTiempo").val(tiempo);
                $("#CaHoraFin").val(HoraFinal);
                localStorage.removeItem(NumCertificado + "PlaPT1" + Ingreso);
                swal("", datos[1], "success");
            } else {
                if (datos[1] == "Debe de ingresar el ajuste que se le realizó al equipo") {
                    $("#DescripcionAjuste").prop("disabled", false);
                    $("#DescripcionAjuste").val("");
                    $("#DescripcionAjuste").focus();
                }
                swal("Acción Cancelada", datos[1], "warning");
            }
        }, 15);
    });
});

function CambioDecimales(decimal, fila) {
    DecimalesP[fila] = decimal
    for (var x = 0; x <= 5; x++) {
        if (fila == (CantRegitro-1))
            $("#PrSerie" + x).attr("onblur", "FormatoSalida2(this," + decimal + ")");

        $("#Serie" + x + fila).attr("onblur", "FormatoSalida(this," + decimal + "," + NumCertificado + ")");
        $("#Serie" + x + fila).attr("onblur", "FormatoSalida(this," + decimal + "," + NumCertificado + ")");
    }
    $("#Tiempo" + fila).attr("onblur", "FormatoSalida(this," + decimal + "," + NumCertificado + ")");
    $("#Posicion" + fila).attr("onblur", "FormatoSalida(this," + decimal + "," + NumCertificado + ")");
    $("#Brazo" + fila).attr("onblur", "FormatoSalida(this," + decimal + "," + NumCertificado + ")");
    if (fila == (CantRegitro - 1))
        $("#PrPosicion").attr("onblur", "FormatoSalida(this," + decimal + "," + NumCertificado + ")");

    GuardarTemporal();
}

function TablaCMC() {
    var datos = LlamarAjax("Laboratorio/TablaCMC", "magnitud=6");
    var resultado = "";
    var data = JSON.parse(datos);
    for (var x = 0; x < data.length; x++) {
        resultado += "<tr>" +
            "<td>></td>" +
            "<td align='center'>" + data[x].desde + "</td>" +
            "<td align='center'>" + data[x].hasta + "</td>" +
            "<td align='center'>" + data[x].valor + "</td></tr>";
    }
    $("#TBPublicadaONAC").html(resultado);
}

function ConfigurarTablas(can) {
    var resultado = "";
    var resultado2 = "";
    var resultado3 = "";
    var resultado4 = "";
    var resultado5 = "";
    var resultado6 = "";
    var resultado7 = "";
    var resultado8 = "";
    var resultado9 = "";
    var resultado10 = "";
    var porlectura = "";
    var MejorPatron = "";
    var resultado11 = "";
    var resultado12 = "";

    var resultado20 = '<tr class="tabcertr"><th width="25% " class="bg-gris" >&nbsp;</th>';
    var resultado21 = '<tr class="tabcertr"><th width="25% " class="bg-gris" >Alcance en N·m:</th>';
    var resultado22 = '<tr class="tabcertr"><th width="25% " class="bg-gris" >Marca:</th>';
    var resultado23 = '<tr class="tabcertr"><th width="25% " class="bg-gris" >Modelo:</th>';
    var resultado24 = '<tr class="tabcertr"><th width="25% " class="bg-gris" >Serie:</th>';
    var resultado25 = '<tr class="tabcertr"><th width="25% " class="bg-gris" >Número de Certificado:</th>';
    var resultado26 = '<tr class="tabcertr"><th width="25% " class="bg-gris" >Laboratorio:</th>';
    var resultado27 = '<tr class="tabcertr"><th width="25% " class="bg-gris" >Fecha de  Calibración:</th>';
    var resultado28 = '<tr class="tabcertr"><th width="25% " class="bg-gris" >Próxima  Calibración:</th>';
    var resultado29 = '<tr class="tabcertr"><th width="25% " class="bg-gris" >Resolución:</th>';
    
    var patrones = CargarCombo(20, 1, "", "6");
    for (var x = 1; x <= can; x++) {

        if (can == 5) {
            switch (x) {
                case 1:
                    porlectura = "20";
                    break;
                case 2:
                    porlectura = "40";
                    break;
                case 3:
                    porlectura = "60";
                    break;
                case 4:
                    porlectura = "80";
                    break;
                case 5:
                    porlectura = "100";
                    break;
            }
        }

        resultado += '<tr class="tabcertr">' +
             '<th><input type="text" name="PaPorLectura" value="' + porlectura + '"  id="PaPorLectura' + x + '" required onfocus="FormatoEntrada(this, 1)" onblur="FormatoSalida(this,3,1)" onkeyup="CalcularSerie(event,this,' + x + ')" class="form-control sinbordecon text-XX2 text-right bg-amarillo"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="PaLectura" id="PaLectura' + x + '" readonly class="form-control sinbordecon text-XX2 text-right"/></th>' +
            '<th class="text-center text-XX2" id="PaPunto' + x + '">&nbsp;</th>' +
            '<th>' +
            '<select style="width:100%" id="Patron' + x + '" required name="Patron" onchange="CambioPatron(' + x + ')">' +
            patrones +
            '</select>' +
            '</th>' +
            '<th class="text-center text-XX2"><input type="text" name="IncerExpandida" id="IncerExpandida' + x + '" readonly class="form-control sinbordecon text-XX2 text-right"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="DPatron" id="DPatron' + x + '" readonly class="form-control sinbordecon text-XX2 text-right"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="RPatron" id="RPatron' + x + '" readonly class="form-control sinbordecon text-XX2 text-right"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="IncerPretendida" id="IncerPretendida' + x + '" readonly class="form-control sinbordecon text-XX2 text-right"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="ErrorPretendido" id="ErrorPretendido' + x + '" readonly class="form-control sinbordecon text-XX2 text-right"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="DecimalPatron" id="DecimalPatron' + x + '" required onfocus="FormatoEntrada(this, 1)" onchange="CambioDecimales(this.value,' + x + ')" onblur="FormatoSalida(this,0)" onkeyup="ValidarTexto(this,3)" class="form-control sinbordecon text-XX text-center bg-amarillo"/></th>' +
            '</tr>';

        resultado10 += '<tr class="tabcertr">' +
            '<th class="text-center text-XX2" id="CoePorLectura' + x + '"></th>' +
            '<th class="text-center text-XX2" id="CoeLectura' + x + '"></th>' +
            '<th class="text-center text-XX2" id="CoePunto' + x + '">&nbsp;</th>' +
            '<th class="text-center text-XX2" id="CoePatron' + x + '">&nbsp;</th>' +
            '<th class="text-center text-XX2"><input type="text" name="C1Patron" id="C1Patron' + x + '" readonly class="form-control sinbordecon text-XX2 text-right"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="C2Patron" id="C2Patron' + x + '" readonly class="form-control sinbordecon text-XX2 text-right"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="C3Patron" id="C3Patron' + x + '" readonly class="form-control sinbordecon text-XX2 text-right"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="C4Patron" id="C4Patron' + x + '" readonly class="form-control sinbordecon text-XX2 text-right"/></th>' +
            '</tr>';



        resultado2 += '<tr class="tabcertr">' +
            '<th class="text-right bg-gris" id="SePorcentaje' + x + '">&nbsp;</th>' +
            '<td class="text-right bg-gris text-XX2" id="SeLectura' + x + '">&nbsp;</td>' +
            '<th><input type="text" onchange="RequerirAjuste()" name="Serie1" id="Serie1' + x + '" required onfocus="FormatoEntrada(this, 1)" onblur="FormatoSalida(this,3,' + NumCertificado + ')" onkeyup="CalcularSerie(event,this, ' + x + ')" class="form-control sinbordecon text-XX2  bg-amarillo " /></th>' +
            '<th><input type="text" onchange="RequerirAjuste()" name="Serie2" id="Serie2' + x + '" required onfocus="FormatoEntrada(this, 1)" onblur="FormatoSalida(this,3,' + NumCertificado + ')" onkeyup="CalcularSerie(event,this,' + x + ')" class="form-control sinbordecon text-XX2 text-right bg-amarillo " /></th>' +
            '<th><input type="text" onchange="RequerirAjuste()" name="Serie3" id="Serie3' + x + '" required onfocus="FormatoEntrada(this, 1)" onblur="FormatoSalida(this,3,' + NumCertificado + ')" onkeyup="CalcularSerie(event,this,' + x + ')" class="form-control sinbordecon text-XX2 text-right bg-amarillo " /></th>' +
            '<th><input type="text" onchange="RequerirAjuste()" name="Serie4" id="Serie4' + x + '" required onfocus="FormatoEntrada(this, 1)" onblur="FormatoSalida(this,3,' + NumCertificado + ')" onkeyup="CalcularSerie(event,this,' + x + ')" class="form-control sinbordecon text-XX2 text-right bg-amarillo " /></th>' +
            '<th><input type="text" onchange="RequerirAjuste()" name="Serie5" id="Serie5' + x + '" required onfocus="FormatoEntrada(this, 1)" onblur="FormatoSalida(this,3,' + NumCertificado + ')" onkeyup="CalcularSerie(event,this,' + x + ')" class="form-control sinbordecon text-XX2 text-right bg-amarillo " /></th>' +
            '<th><input type="text" onchange="RequerirAjuste()" name="Serie6" id="Serie6' + x + '" required onfocus="FormatoEntrada(this, 1)" onblur="FormatoSalida(this,3,' + NumCertificado + ')" onkeyup="CalcularSerie(event,this,' + x + ')" class="form-control sinbordecon text-XX2 text-right bg-amarillo punto10" /></th>' +
            '<th><input type="text" onchange="RequerirAjuste()" name="Serie7" id="Serie7' + x + '" required onfocus="FormatoEntrada(this, 1)" onblur="FormatoSalida(this,3,' + NumCertificado + ')" onkeyup="CalcularSerie(event,this,' + x + ')" class="form-control sinbordecon text-XX2 text-right bg-amarillo punto10" /></th>' +
            '<th><input type="text" onchange="RequerirAjuste()" name="Serie8" id="Serie8' + x + '" required onfocus="FormatoEntrada(this, 1)" onblur="FormatoSalida(this,3,' + NumCertificado + ')" onkeyup="CalcularSerie(event,this,' + x + ')" class="form-control sinbordecon text-XX2 text-right bg-amarillo punto10" /></th>' +
            '<th><input type="text" onchange="RequerirAjuste()" name="Serie9" id="Serie9' + x + '" required onfocus="FormatoEntrada(this, 1)" onblur="FormatoSalida(this,3,' + NumCertificado + ')" onkeyup="CalcularSerie(event,this,' + x + ')" class="form-control sinbordecon text-XX2 text-right bg-amarillo punto10" /></th>' +
            '<th><input type="text" onchange="RequerirAjuste()" name="Serie10" id="Serie10' + x + '" required onfocus="FormatoEntrada(this, 1)" onblur="FormatoSalida(this,3,' + NumCertificado + ')" onkeyup="CalcularSerie(event,this,' + x + ')" class="form-control sinbordecon text-XX2 text-right bg-amarillo punto10" /></th>' +
            '<th><input type="text" name="SerPromedio" id="SerPromedio' + x + '" value="" readonly class="form-control sinbordecon text-XX2 text-right"/></th>' +
            '</tr>';
        resultado3 += '<tr class="tabcertr">' +
            '<th class="text-right bg-gris text-XX2" id="NnPorcencate' + x + '"></th>' +
            '<td class="text-right bg-gris text-XX2" id="NnLectura' + x + '"></td>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="NmSerie1" id="NmSerie1' + x + '" readonly class="form-control sinbordecon text-XX2 text-right"/><span class="custom help" style="color:black;"><img src="../../Imagenes/Help.png" alt="Help" height="48" width="48" /><em>Fórmula</em><label id="F_NmSerie1' + x + '" formula="=Datos_Serie1 * Factor">=Datos_Serie1 * Factor</label></span></a></th>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="NmSerie2" id="NmSerie2' + x + '" readonly class="form-control sinbordecon text-XX2 text-right"/><span class="custom help" style="color:black;"><img src="../../Imagenes/Help.png" alt="Help" height="48" width="48" /><em>Fórmula</em><label id="F_NmSerie2' + x + '" formula="=Datos_Serie2 * Factor">=Datos_Serie2 * Factor</label></span></a></th>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="NmSerie3" id="NmSerie3' + x + '" readonly class="form-control sinbordecon text-XX2 text-right"/><span class="custom help" style="color:black;"><img src="../../Imagenes/Help.png" alt="Help" height="48" width="48" /><em>Fórmula</em><label id="F_NmSerie3' + x + '" formula="=Datos_Serie3 * Factor">=Datos_Serie3 * Factor</label></span></a></th>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="NmSerie4" id="NmSerie4' + x + '" readonly class="form-control sinbordecon text-XX2 text-right"/><span class="custom help" style="color:black;"><img src="../../Imagenes/Help.png" alt="Help" height="48" width="48" /><em>Fórmula</em><label id="F_NmSerie4' + x + '" formula="=Datos_Serie4 * Factor">=Datos_Serie4 * Factor</label></span></a></th>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="NmSerie5" id="NmSerie5' + x + '" readonly class="form-control sinbordecon text-XX2 text-right"/><span class="custom help" style="color:black;"><img src="../../Imagenes/Help.png" alt="Help" height="48" width="48" /><em>Fórmula</em><label id="F_NmSerie5' + x + '" formula="=Datos_Serie5 * Factor">=Datos_Serie5 * Factor</label></span></a></th>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="NmSerie6" id="NmSerie6' + x + '" readonly class="form-control sinbordecon text-XX2 text-right punto10"/><span class="custom help" style="color:black;"><img src="../../Imagenes/Help.png" alt="Help" height="48" width="48" /><em>Fórmula</em><label id="F_NmSerie6' + x + '" formula="=Datos_Serie6 * Factor">=Datos_Serie6 * Factor</label></span></a></th>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="NmSerie7" id="NmSerie7' + x + '" readonly class="form-control sinbordecon text-XX2 text-right punto10"/><span class="custom help" style="color:black;"><img src="../../Imagenes/Help.png" alt="Help" height="48" width="48" /><em>Fórmula</em><label id="F_NmSerie7' + x + '" formula="=Datos_Serie7 * Factor">=Datos_Serie7 * Factor</label></span></a></th>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="NmSerie8" id="NmSerie8' + x + '" readonly class="form-control sinbordecon text-XX2 text-right punto10"/><span class="custom help" style="color:black;"><img src="../../Imagenes/Help.png" alt="Help" height="48" width="48" /><em>Fórmula</em><label id="F_NmSerie8' + x + '" formula="=Datos_Serie8 * Factor">=Datos_Serie8 * Factor</label></span></a></th>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="NmSerie9" id="NmSerie9' + x + '" readonly class="form-control sinbordecon text-XX2 text-right punto10"/><span class="custom help" style="color:black;"><img src="../../Imagenes/Help.png" alt="Help" height="48" width="48" /><em>Fórmula</em><label id="F_NmSerie9' + x + '" formula="=Datos_Serie9 * Factor">=Datos_Serie9 * Factor</label></span></a></th>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="NmSerie10" id="NmSerie10' + x + '" readonly class="form-control sinbordecon text-XX2 text-right punto10"/><span class="custom help" style="color:black;"><img src="../../Imagenes/Help.png" alt="Help" height="48" width="48" /><em>Fórmula</em><label id="F_NmSerie10' + x + '" formula="=Datos_Serie10 * Factor">=Datos_Serie10 * Factor</label></span></a></th>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="NmSerPromedio" id="NmSerPromedio' + x + '" readonly class="form-control sinbordecon text-XX2 text-right"/><span class="custom help" style="color:black;"><img src="../../Imagenes/Help.png" alt="Help" height="48" width="48" /><em>Fórmula</em><label id="F_NmSerPromedio' + x + '" formula="=Promedio(Datos_Serie1:Datos_Serie10)">==Promedio(Datos_Serie1:Datos_Serie10)</label></span></a></th>';
            '</tr>';
        resultado4 += '<tr class="tabcertr">' +
            '<th class="text-right bg-gris text-XX2" id="CMePorcencate' + x + '" ></th>' +
            '<td class="text-right bg-gris text-XX2" id="CMeLectura' + x + '"></td>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="CMeSerie1" id="CMeSerie1' + x + '" readonly class="form-control sinbordecon text-XX2 text-right"/><span class="custom help" style="color:black;"><img src="../../Imagenes/Help.png" alt="Help" height="48" width="48" /><em>Fórmula</em><label id="F_CMeSerie1' + x + '" formula="=Serie1_N·m/Factor">=Serie1_N·m/Factor</label></span></a></th>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="CMeSerie2" id="CMeSerie2' + x + '" readonly class="form-control sinbordecon text-XX2 text-right"/><span class="custom help" style="color:black;"><img src="../../Imagenes/Help.png" alt="Help" height="48" width="48" /><em>Fórmula</em><label id="F_CMeSerie2' + x + '" formula="=Serie2_N·m/Factor">=Serie2_N·m/Factor</label></span></a></th>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="CMeSerie3" id="CMeSerie3' + x + '" readonly class="form-control sinbordecon text-XX2 text-right"/><span class="custom help" style="color:black;"><img src="../../Imagenes/Help.png" alt="Help" height="48" width="48" /><em>Fórmula</em><label id="F_CMeSerie3' + x + '" formula="=Serie3_N·m/Factor">=Serie3_N·m/Factor</label></span></a></th>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="CMeSerie4" id="CMeSerie4' + x + '" readonly class="form-control sinbordecon text-XX2 text-right"/><span class="custom help" style="color:black;"><img src="../../Imagenes/Help.png" alt="Help" height="48" width="48" /><em>Fórmula</em><label id="F_CMeSerie4' + x + '" formula="=Serie4_N·m/Factor">=Serie4_N·m/Factor</label></span></a></th>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="CMeSerie5" id="CMeSerie5' + x + '" readonly class="form-control sinbordecon text-XX2 text-right"/><span class="custom help" style="color:black;"><img src="../../Imagenes/Help.png" alt="Help" height="48" width="48" /><em>Fórmula</em><label id="F_CMeSerie5' + x + '" formula="=Serie5_N·m/Factor">=Serie5_N·m/Factor</label></span></a></th>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="CMeSerie6" id="CMeSerie6' + x + '" readonly class="form-control sinbordecon text-XX2 text-right punto10"/><span class="custom help" style="color:black;"><img src="../../Imagenes/Help.png" alt="Help" height="48" width="48" /><em>Fórmula</em><label id="F_CMeSerie6' + x + '" formula="=Serie6_N·m/Factor">=Serie6_N·m/Factor</label></span></a></th>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="CMeSerie7" id="CMeSerie7' + x + '" readonly class="form-control sinbordecon text-XX2 text-right punto10"/><span class="custom help" style="color:black;"><img src="../../Imagenes/Help.png" alt="Help" height="48" width="48" /><em>Fórmula</em><label id="F_CMeSerie7' + x + '" formula="=Serie7_N·m/Factor">=Serie7_N·m/Factor</label></span></a></th>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="CMeSerie8" id="CMeSerie8' + x + '" readonly class="form-control sinbordecon text-XX2 text-right punto10"/><span class="custom help" style="color:black;"><img src="../../Imagenes/Help.png" alt="Help" height="48" width="48" /><em>Fórmula</em><label id="F_CMeSerie8' + x + '" formula="=Serie8_N·m/Factor">=Serie8_N·m/Factor</label></span></a></th>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="CMeSerie9" id="CMeSerie9' + x + '" readonly class="form-control sinbordecon text-XX2 text-right punto10"/><span class="custom help" style="color:black;"><img src="../../Imagenes/Help.png" alt="Help" height="48" width="48" /><em>Fórmula</em><label id="F_CMeSerie9' + x + '" formula="=Serie9_N·m/Factor">=Serie9_N·m/Factor</label></span></a></th>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="CMeSerie10" id="CMeSerie10' + x + '" readonly class="form-control sinbordecon text-XX2 text-right punto10"/><span class="custom help" style="color:black;"><img src="../../Imagenes/Help.png" alt="Help" height="48" width="48" /><em>Fórmula</em><label id="F_CMeSerie10' + x + '" formula="=Serie10_N·m/Factor">=Serie10_N·m/Factor</label></span></a></th>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="CMeSerPromedio" id="CMeSerPromedio' + x + '" readonly class="form-control sinbordecon text-XX2 text-right"/><span class="custom help" style="color:black;"><img src="../../Imagenes/Help.png" alt="Help" height="48" width="48" /><em>Fórmula</em><label id="F_NmSerPromedio' + x + '" formula="=Promedio(Serie1_CoN·m:Serie10_CoN·m)">==Promedio(Serie1_CoN·m:Serie10_CoN·m)</label></span></a></th>';
            '</tr>';
        resultado5 += '<tr class="tabcertr">' +
            '<th class="text-right bg-gris text-XX2" id="CNmPorcencate' + x + '" ></th>' +
            '<td class="text-right bg-gris text-XX2" id="CNmLectura' + x + '"></td>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="CNmSerie1" id="CNmSerie1' + x + '" readonly class="form-control sinbordecon text-XX2 text-right"/><span class="custom help" style="color:black;"><img src="../../Imagenes/Help.png" alt="Help" height="48" width="48" /><em>Fórmula</em><label id="F_CNmSerie1' + x + '" formula="=a<sub>0</sub> + a<sub>1</sub>(Serie1_N·m) + a<sub>2</sub>(Serie1_N·m)<sup>2</sup> + a<sub>3</sub>(Serie1_N·m)<sup>3</sup>">=a<sub>0</sub> + a<sub>1</sub>(Serie1_N·m) + a<sub>2</sub>(Serie1_N·m)<sup>2</sup> + a<sub>3</sub>(Serie1_N·m)<sup>3</sup></label></span></a></th>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="CNmSerie2" id="CNmSerie2' + x + '" readonly class="form-control sinbordecon text-XX2 text-right"/><span class="custom help" style="color:black;"><img src="../../Imagenes/Help.png" alt="Help" height="48" width="48" /><em>Fórmula</em><label id="F_CNmSerie2' + x + '" formula="=a<sub>0</sub> + a<sub>1</sub>(Serie2_N·m) + a<sub>2</sub>(Serie2_N·m)<sup>2</sup> + a<sub>3</sub>(Serie2_N·m)<sup>3</sup>">=a<sub>0</sub> + a<sub>1</sub>(Serie2_N·m) + a<sub>2</sub>(Serie2_N·m)<sup>2</sup> + a<sub>3</sub>(Serie2_N·m)<sup>3</sup></label></span></a></th>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="CNmSerie3" id="CNmSerie3' + x + '" readonly class="form-control sinbordecon text-XX2 text-right"/><span class="custom help" style="color:black;"><img src="../../Imagenes/Help.png" alt="Help" height="48" width="48" /><em>Fórmula</em><label id="F_CNmSerie3' + x + '" formula="=a<sub>0</sub> + a<sub>1</sub>(Serie3_N·m) + a<sub>2</sub>(Serie3_N·m)<sup>2</sup> + a<sub>3</sub>(Serie3_N·m)<sup>3</sup>">=a<sub>0</sub> + a<sub>1</sub>(Serie3_N·m) + a<sub>2</sub>(Serie3_N·m)<sup>2</sup> + a<sub>3</sub>(Serie3_N·m)<sup>3</sup></label></span></a></th>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="CNmSerie4" id="CNmSerie4' + x + '" readonly class="form-control sinbordecon text-XX2 text-right"/><span class="custom help" style="color:black;"><img src="../../Imagenes/Help.png" alt="Help" height="48" width="48" /><em>Fórmula</em><label id="F_CNmSerie4' + x + '" formula="=a<sub>0</sub> + a<sub>1</sub>(Serie4_N·m) + a<sub>2</sub>(Serie4_N·m)<sup>2</sup> + a<sub>3</sub>(Serie4_N·m)<sup>3</sup>">=a<sub>0</sub> + a<sub>1</sub>(Serie4_N·m) + a<sub>2</sub>(Serie4_N·m)<sup>2</sup> + a<sub>3</sub>(Serie4_N·m)<sup>3</sup></label></span></a></th>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="CNmSerie5" id="CNmSerie5' + x + '" readonly class="form-control sinbordecon text-XX2 text-right"/><span class="custom help" style="color:black;"><img src="../../Imagenes/Help.png" alt="Help" height="48" width="48" /><em>Fórmula</em><label id="F_CNmSerie5' + x + '" formula="=a<sub>0</sub> + a<sub>1</sub>(Serie5_N·m) + a<sub>2</sub>(Serie5_N·m)<sup>2</sup> + a<sub>3</sub>(Serie5_N·m)<sup>3</sup>">=a<sub>0</sub> + a<sub>1</sub>(Serie5_N·m) + a<sub>2</sub>(Serie5_N·m)<sup>2</sup> + a<sub>3</sub>(Serie5_N·m)<sup>3</sup></label></span></a></th>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="CNmSerie6" id="CNmSerie6' + x + '" readonly class="form-control sinbordecon text-XX2 text-right punto10"/><span class="custom help" style="color:black;"><img src="../../Imagenes/Help.png" alt="Help" height="48" width="48" /><em>Fórmula</em><label id="F_CNmSerie6' + x + '" formula="=a<sub>0</sub> + a<sub>1</sub>(Serie6_N·m) + a<sub>2</sub>(Serie6_N·m)<sup>2</sup> + a<sub>3</sub>(Serie6_N·m)<sup>3</sup>">=a<sub>0</sub> + a<sub>1</sub>(Serie6_N·m) + a<sub>2</sub>(Serie6_N·m)<sup>2</sup> + a<sub>3</sub>(Serie6_N·m)<sup>3</sup></label></span></a></th>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="CNmSerie7" id="CNmSerie7' + x + '" readonly class="form-control sinbordecon text-XX2 text-right punto10"/><span class="custom help" style="color:black;"><img src="../../Imagenes/Help.png" alt="Help" height="48" width="48" /><em>Fórmula</em><label id="F_CNmSerie7' + x + '" formula="=a<sub>0</sub> + a<sub>1</sub>(Serie7_N·m) + a<sub>2</sub>(Serie7_N·m)<sup>2</sup> + a<sub>3</sub>(Serie7_N·m)<sup>3</sup>">=a<sub>0</sub> + a<sub>1</sub>(Serie7_N·m) + a<sub>2</sub>(Serie7_N·m)<sup>2</sup> + a<sub>3</sub>(Serie7_N·m)<sup>3</sup></label></span></a></th>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="CNmSerie8" id="CNmSerie8' + x + '" readonly class="form-control sinbordecon text-XX2 text-right punto10"/><span class="custom help" style="color:black;"><img src="../../Imagenes/Help.png" alt="Help" height="48" width="48" /><em>Fórmula</em><label id="F_CNmSerie8' + x + '" formula="=a<sub>0</sub> + a<sub>1</sub>(Serie8_N·m) + a<sub>2</sub>(Serie8_N·m)<sup>2</sup> + a<sub>3</sub>(Serie8_N·m)<sup>3</sup>">=a<sub>0</sub> + a<sub>1</sub>(Serie8_N·m) + a<sub>2</sub>(Serie8_N·m)<sup>2</sup> + a<sub>3</sub>(Serie8_N·m)<sup>3</sup></label></span></a></th>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="CNmSerie9" id="CNmSerie9' + x + '" readonly class="form-control sinbordecon text-XX2 text-right punto10"/><span class="custom help" style="color:black;"><img src="../../Imagenes/Help.png" alt="Help" height="48" width="48" /><em>Fórmula</em><label id="F_CNmSerie9' + x + '" formula="=a<sub>0</sub> + a<sub>1</sub>(Serie9_N·m) + a<sub>2</sub>(Serie9_N·m)<sup>2</sup> + a<sub>3</sub>(Serie9_N·m)<sup>3</sup>">=a<sub>0</sub> + a<sub>1</sub>(Serie9_N·m) + a<sub>2</sub>(Serie9_N·m)<sup>2</sup> + a<sub>3</sub>(Serie9_N·m)<sup>3</sup></label></span></a></th>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="CNmSerie10" id="CNmSerie10' + x + '" readonly class="form-control sinbordecon text-XX2 text-right punto10"/><span class="custom help" style="color:black;"><img src="../../Imagenes/Help.png" alt="Help" height="48" width="48" /><em>Fórmula</em><label id="F_CNmSerie10' + x + '" formula="=a<sub>0</sub> + a<sub>1</sub>(Serie10_N·m) + a<sub>2</sub>(Serie10_N·m)<sup>2</sup> + a<sub>3</sub>(Serie10_N·m)<sup>3</sup>">=a<sub>0</sub> + a<sub>1</sub>(Serie10_N·m) + a<sub>2</sub>(Serie10_N·m)<sup>2</sup> + a<sub>3</sub>(Serie10_N·m)<sup>3</sup></label></span></a></th>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="CNmSerPromedio" id="CNmSerPromedio' + x + '" readonly class="form-control sinbordecon text-XX2 text-right"/><span class="custom help" style="color:black;"><img src="../../Imagenes/Help.png" alt="Help" height="48" width="48" /><em>Fórmula</em><label id="F_CNmSerPromedio' + x + '" formula="=Promedio(Serie1_N·m:Serie10_N·m)">=Promedio(Serie1_N·m:Serie10_N·m)</label</span></a></th>';
            '</tr>';
        resultado6 += '<tr class="tabcertr">' +
            '<th class="text-right bg-gris text-XX2" id = "DePorcentaje' + x + '" >&nbsp;</th>' +
            '<td class="text-right bg-gris text-XX" id="DeLectura' + x + '"></td>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="DeSerie1" id="DeSerie1' + x + '" readonly class="form-control sinbordecon text-XX2 text-right"/><span class="custom help" style="color:black;"><img src="../../Imagenes/Help.png" alt="Help" height="48" width="48" /><em>Fórmula</em><label id="F_DeSerie1' + x + '" formula="=((InidicadorIBC_1 * Serie1_N·m) <br> * 100) / Serie1_N·m">=((InidicadorIBC_1 * Serie1_N·m) <br> * 100) / Serie1_N·m</label></span></a></th>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="DeSerie2" id="DeSerie2' + x + '" readonly class="form-control sinbordecon text-XX2 text-right"/><span class="custom help" style="color:black;"><img src="../../Imagenes/Help.png" alt="Help" height="48" width="48" /><em>Fórmula</em><label id="F_DeSerie2' + x + '" formula="=((InidicadorIBC_1 * Serie2_N·m) <br> * 100) / Serie2_N·m">=((InidicadorIBC_1 * Serie2_N·m) <br> * 100) / Serie2_N·m</label></span></a></th>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="DeSerie3" id="DeSerie3' + x + '" readonly class="form-control sinbordecon text-XX2 text-right"/><span class="custom help" style="color:black;"><img src="../../Imagenes/Help.png" alt="Help" height="48" width="48" /><em>Fórmula</em><label id="F_DeSerie3' + x + '" formula="=((InidicadorIBC_1 * Serie3_N·m) <br> * 100) / Serie3_N·m">=((InidicadorIBC_1 * Serie3_N·m) <br> * 100) / Serie3_N·m</label></span></a></th>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="DeSerie4" id="DeSerie4' + x + '" readonly class="form-control sinbordecon text-XX2 text-right"/><span class="custom help" style="color:black;"><img src="../../Imagenes/Help.png" alt="Help" height="48" width="48" /><em>Fórmula</em><label id="F_DeSerie4' + x + '" formula="=((InidicadorIBC_1 * Serie4_N·m) <br> * 100) / Serie4_N·m">=((InidicadorIBC_1 * Serie4_N·m) <br> * 100) / Serie4_N·m</label></span></a></th>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="DeSerie5" id="DeSerie5' + x + '" readonly class="form-control sinbordecon text-XX2 text-right"/><span class="custom help" style="color:black;"><img src="../../Imagenes/Help.png" alt="Help" height="48" width="48" /><em>Fórmula</em><label id="F_DeSerie5' + x + '" formula="=((InidicadorIBC_1 * Serie5_N·m) <br> * 100) / Serie5_N·m">=((InidicadorIBC_1 * Serie5_N·m) <br> * 100) / Serie5_N·m</label></span></a></th>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="DeSerie6" id="DeSerie6' + x + '" readonly class="form-control sinbordecon text-XX2 text-right punto10"/><span class="custom help" style="color:black;"><img src="../../Imagenes/Help.png" alt="Help" height="48" width="48" /><em>Fórmula</em><label id="F_DeSerie6' + x + '" formula="=((InidicadorIBC_1 * Serie6_N·m) <br> * 100) / Serie6_N·m">=((InidicadorIBC_1 * Serie6_N·m) <br> * 100) / Serie6_N·m</label></span></a></th>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="DeSerie7" id="DeSerie7' + x + '" readonly class="form-control sinbordecon text-XX2 text-right punto10"/><span class="custom help" style="color:black;"><img src="../../Imagenes/Help.png" alt="Help" height="48" width="48" /><em>Fórmula</em><label id="F_DeSerie7' + x + '" formula="=((InidicadorIBC_1 * Serie7_N·m) <br> * 100) / Serie7_N·m">=((InidicadorIBC_1 * Serie7_N·m) <br> * 100) / Serie7_N·m</label></span></a></th>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="DeSerie8" id="DeSerie8' + x + '" readonly class="form-control sinbordecon text-XX2 text-right punto10"/><span class="custom help" style="color:black;"><img src="../../Imagenes/Help.png" alt="Help" height="48" width="48" /><em>Fórmula</em><label id="F_DeSerie8' + x + '" formula="=((InidicadorIBC_1 * Serie8_N·m) <br> * 100) / Serie8_N·m">=((InidicadorIBC_1 * Serie8_N·m) <br> * 100) / Serie8_N·m</label></span></a></th>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="DeSerie9" id="DeSerie9' + x + '" readonly class="form-control sinbordecon text-XX2 text-right punto10"/><span class="custom help" style="color:black;"><img src="../../Imagenes/Help.png" alt="Help" height="48" width="48" /><em>Fórmula</em><label id="F_DeSerie9' + x + '" formula="=((InidicadorIBC_1 * Serie9_N·m) <br> * 100) / Serie9_N·m">=((InidicadorIBC_1 * Serie9_N·m) <br> * 100) / Serie9_N·m</label></span></a></th>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="DeSerie10" id="DeSerie10' + x + '" readonly class="form-control sinbordecon text-XX2 text-right punto10"/><span class="custom help" style="color:black;"><img src="../../Imagenes/Help.png" alt="Help" height="48" width="48" /><em>Fórmula</em><label id="F_DeSerie10' + x + '" formula="=((InidicadorIBC_1 * Serie10_N·m) <br> * 100) / Serie10_N·m">=((InidicadorIBC_1 * Serie10_N·m) <br> * 100) / Serie10_N·m</label</span></a></th>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="DeSerPromedio" id="DeSerPromedio' + x + '" readonly class="form-control sinbordecon text-XX2 text-right"/><span class="custom help" style="color:black;"><img src="../../Imagenes/Help.png" alt="Help" height="48" width="48" /><em>Fórmula</em><label id="F_CNmSerPromedio' + x + '" formula="=Promedio(Serie1_Des:Serie10_Des)">=Promedio(Serie1_Des:Serie10_Des)</label</span></a></th>';
            '</tr>';
        resultado7 += '<tr class="tabcertr">' +
            '<th class="text-right bg-gris text-XX2" id = "ReLectura' + x + '" >&nbsp;</th>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="ReResolucion" id="ReResolucion' + x + '" readonly class="form-control sinbordecon text-XX2 text-right"/><span class="custom help" style="color:black;"><img src="../../Imagenes/Help.png" alt="Help" height="48" width="48" /><em>Fórmula</em><label id="F_ReResolucion' + x + '" formula="=Resolucion*Factor">=Resolucion*Factor</label</span></a></th>' +
            '<td><a class="tooltip2" href="#"><input type="text" name="ReReproducibilidad" id="ReReproducibilidad' + x + '" readonly class="form-control sinbordecon text-XX2 text-right"/><span class="custom help" style="color:black;"><img src="../../Imagenes/Help.png" alt="Help" height="48" width="48" /><em>Fórmula</em><label id="F_ReReproducibilidad' + x + '" formula="=Max(Promedio_Repetibilidad)-Min(Promedio_Repetibilidad)">=Max(Promedio_Repetibilidad)-Min(Promedio_Repetibilidad)</label></span></a></th>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="ReGeoSalida" id="ReGeoSalida' + x + '" readonly class="form-control sinbordecon text-XX2 text-right"/><span class="custom help" style="color:black;"><img src="../../Imagenes/Help.png" alt="Help" height="48" width="48" /><em>Fórmula</em><label id="F_ReGeoSalida' + x + '" formula="=Max(Promedio_Geometria_Salida)-Min(Promedio_Geometria_Salida)">=Max(Promedio_Geometria_Salida)-Min(Promedio_Geometria_Salida)</label></span></a></th>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="ReGeoInterfaz" id="ReGeoInterfaz' + x + '" readonly class="form-control sinbordecon text-XX2 text-right"/><span class="custom help" style="color:black;"><img src="../../Imagenes/Help.png" alt="Help" height="48" width="48" /><em>Fórmula</em><label id="F_ReGeoInterfaz' + x + '" formula="=Max(Promedio_Geometria_Interfaz)-Min(Promedio_Geometria_Interfaz)">=Max(Promedio_Geometria_Interfaz)-Min(Promedio_Geometria_Interfaz)</label></span></a></th>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="ReLongitud" id="ReLongitud' + x + '" readonly class="form-control sinbordecon text-XX2 text-right"/><span class="custom help" style="color:black;"><img src="../../Imagenes/Help.png" alt="Help" height="48" width="48" /><em>Fórmula</em><label id="F_ReLongitud' + x + '" formula="Promedio_Geometria_Longitud1-Promedio_Geometria_Longitud1">=Promedio_Geometria_Longitud1-Promedio_Geometria_Longitud1</label></span></a></th>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="ReRepetibilidad" id="ReRepetibilidad' + x + '" readonly class="form-control sinbordecon text-XX2 text-right"/><span class="custom help" style="color:black;"><img src="../../Imagenes/Help.png" alt="Help" height="48" width="48" /><em>Fórmula</em><label id="F_ReRepetibilidad' + x + '" formula="DESVEST.M(Serie1_Des:Serie10_Des)">=DESVEST.M(Serie1_Des:Serie10_Des)</label></span></a></th>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="ReUPatron" id="ReUPatron' + x + '" readonly class="form-control sinbordecon text-XX2 text-right"/><span class="custom help" style="color:black;"><img src="../../Imagenes/Help.png" alt="Help" height="48" width="48" /><em>Fórmula</em><label id="F_ReUPatron' + x + '" formula="=IncertidumbreExpandidaPatron">=IncertidumbreExpandidaPatron</label></span></a></th>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="ReRPatron" id="ReRPatron' + x + '" readonly class="form-control sinbordecon text-XX2 text-right"/><span class="custom help" style="color:black;"><img src="../../Imagenes/Help.png" alt="Help" height="48" width="48" /><em>Fórmula</em><label id="F_ReRPatron' + x + '" formula="=ResolucionPatron">=ResolucionPatron</label></span></a></th>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="ReDPatron" id="ReDPatron' + x + '" readonly class="form-control sinbordecon text-XX2 text-right"/><span class="custom help" style="color:black;"><img src="../../Imagenes/Help.png" alt="Help" height="48" width="48" /><em>Fórmula</em><label id="F_ReDPatron' + x + '" formula="=DerivadaPatrón">=DerivadaPatrón</label></span></a></th>';
            '</tr>'; 

        resultado8 += '<tr class="tabcertr">' +
            '<th class="text-right bg-gris text-XX2" id = "CoPorcentaje' + x + '" >&nbsp;</th>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="CoResolucion" id="CoResolucion' + x + '" readonly class="form-control sinbordecon text-XX2 text-right"/><span class="custom help" style="color:black;"><img src="../../Imagenes/Help.png" alt="Help" height="48" width="48" /><em>Fórmula</em><label id="F_ReResolucion' + x + '" formula="=Resolucion*Factor">=Resolucion*Factor</label</span></a></th>' +
            '<td><a class="tooltip2" href="#"><input type="text" name="CoReproducibilidad" id="CoReproducibilidad' + x + '" readonly class="form-control sinbordecon text-XX2 text-right"/><span class="custom help" style="color:black;"><img src="../../Imagenes/Help.png" alt="Help" height="48" width="48" /><em>Fórmula</em><label id="F_ReReproducibilidad' + x + '" formula="=Max(Promedio_Repetibilidad)-Min(Promedio_Repetibilidad)">=Max(Promedio_Repetibilidad)-Min(Promedio_Repetibilidad)</label></span></a></th>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="CoGeoSalida" id="CoGeoSalida' + x + '" readonly class="form-control sinbordecon text-XX2 text-right"/></th>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="CoGeoInterfaz" id="CoGeoInterfaz' + x + '" readonly class="form-control sinbordecon text-XX2 text-right"/></th>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="CoLongitud" id="CoLongitud' + x + '" readonly class="form-control sinbordecon text-XX2 text-right"/></th>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="CoRepetibilidad" id="CoRepetibilidad' + x + '" readonly class="form-control sinbordecon text-XX2 text-right"/></th>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="CoUPatron" id="CoUPatron' + x + '" readonly class="form-control sinbordecon text-XX2 text-right"/></th>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="CoRPatron" id="CoRPatron' + x + '" readonly class="form-control sinbordecon text-XX2 text-right"/></th>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="CoDPatron" id="CoDPatron' + x + '" readonly class="form-control sinbordecon text-XX2 text-right"/></th>' +
            '</tr>'; 

        resultado9 += '<tr class="tabcertr">' +
            '<td class="text-right bg-gris text-XX2" id = "InPorcentaje' + x + '" >&nbsp;</td>' +
            '<td class="text-right bg-gris text-XX2" id="InLectura' + x + '">&nbsp;</td>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="InValorMed" id="InValorMed' + x + '" readonly class="form-control sinbordecon text-XX2 text-right"/></th>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="InValorMax" id="InValorMax' + x + '" readonly class="form-control sinbordecon text-XX2 text-right"/></th>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="InCombinada" id="InCombinada' + x + '" readonly class="form-control sinbordecon text-XX2 text-right"/></th>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="InFactor" id="InFactor' + x + '" readonly class="form-control sinbordecon text-XX2 text-right text-info"/></th>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="InExpandida" id="InExpandida' + x + '" readonly class="form-control sinbordecon text-XX2 text-right text-info"/></th>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="InNGrados" id="InNGrados' + x + '" readonly class="form-control sinbordecon text-XX2 text-right"/></th>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="InPromedio" id="InPromedio' + x + '" readonly class="form-control sinbordecon text-XX2 text-right"/></th>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="InDesPromedio" id="InDesPromedio' + x + '" readonly class="form-control sinbordecon text-XX2 text-right"/></th>' +
            '</tr>'; 

        resultado11 += '<tr class="tabcertr">' +
            '<td class="text-right bg-gris text-XX2" id="ReMeLecturaIBC' + x + '" ></td>' +
            '<td class="text-right bg-gris text-XX2" id="ReMeLecturapatron' + x + '"></td>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="ReMeErroMediMed" id="ReMeErroMediMed' + x + '" readonly class="form-control sinbordecon text-XX2 text-right"/></th>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="ReMeErroMediPor" id="ReMeErroMediPor' + x + '" readonly class="form-control sinbordecon text-XX2 text-right"/></th>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="ReMeIncExpMed" id="ReMeIncExpMed' + x + '" readonly class="form-control sinbordecon text-XX2 text-right"/></th>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="ReMeIncExpPor" id="ReMeIncExpPor' + x + '" readonly class="form-control sinbordecon text-XX2 text-right"/></th>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="ReMeFacCober" id="ReMeFacCober' + x + '" readonly class="form-control sinbordecon text-XX2 text-right"/></th>' +
            '</tr>';

        resultado12 += '<tr class="tabcertr">' +
            '<th class="text-right bg-gris text-XX2" id="ReMnLecturaIBC' + x + '" ></th>' +
            '<td class="text-right bg-gris text-XX2" id="ReMnLecturapatron' + x + '"></td>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="ReMnErroMediMed" id="ReMnErroMediMed' + x + '" readonly class="form-control sinbordecon text-XX2 text-right"/></th>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="ReMnErroMediPor" id="ReMnErroMediPor' + x + '" readonly class="form-control sinbordecon text-XX2 text-right"/></th>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="ReMnIncExpMed" id="ReMnIncExpMed' + x + '" readonly class="form-control sinbordecon text-XX2 text-right"/></th>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="ReMnIncExpPor" id="ReMnIncExpPor' + x + '" readonly class="form-control sinbordecon text-XX2 text-right"/></th>' +
            '<th><a class="tooltip2" href="#"><input type="text" name="ReMnFacCober" id="ReMnFacCober' + x + '" readonly class="form-control sinbordecon text-XX2 text-right"/></th>' +
            '</tr>';

        var por = Math.round(100 / can);

        resultado20 += '<th width="' + por + '" class="text-center bg-gris">Transductor' + x + '</th>';
        resultado21 += '<th width="' + por + '" class="text-info" id="PValor1' + x + '">&nbsp;</th>';
        resultado22 += '<th width="' + por + '" class="text-info" id="PValor2' + x + '">&nbsp;</th>';
        resultado23 += '<th width="' + por + '" class="text-info" id="PValor3' + x + '">&nbsp;</th>';
        resultado24 += '<th width="' + por + '" class="text-info" id="PValor4' + x + '">&nbsp;</th>';
        resultado25 += '<th width="' + por + '" class="text-info" id="PValor5' + x + '">&nbsp;</th>';
        resultado26 += '<th width="' + por + '" class="text-info" id="PValor6' + x + '">&nbsp;</th>';
        resultado27 += '<th width="' + por + '" class="text-info" id="PValor7' + x + '">&nbsp;</th>';
        resultado28 += '<th width="' + por + '" class="text-info" id="PValor8' + x + '">&nbsp;</th>';
        resultado29 += '<th width="' + por + '" class="text-info" id="PValor9' + x + '">&nbsp;</th>';
    }

    resultado20 += '</tr>';
    resultado21 += '</tr>';
    resultado22 += '</tr>';
    resultado23 += '</tr>';
    resultado24 += '</tr>';
    resultado25 += '</tr>';
    resultado26 += '</tr>';
    resultado27 += '</tr>';
    resultado28 += '</tr>';
    resultado29 += '</tr>';

    
    $("#TBpatron").html(resultado);
    $("#TBSerie").html(resultado2);
    $("#LecturaNm").html(resultado3);
    $("#LecturaCMe").html(resultado4);
    $("#LecturaCNm").html(resultado5);
    $("#TBDesviacion").html(resultado6);
    $("#TBRepetibilidad").html(resultado7);
    $("#TBContribucion").html(resultado8);
    $("#TBEstimacion0").html(resultado9);
    $("#TBCoeficiente").html(resultado10);
    $("#TBResultadoMedida").html(resultado11);
    $("#TBResultadoMedidaNm").html(resultado12);
    
    $("#TBodyPatron").html(resultado20 + resultado21 + resultado22 + resultado23 + resultado24 + resultado25 + resultado26 + resultado27 + resultado28 + resultado29);
    $('select').select2();

    if ($("#Ingreso").val() != "") {
        
        for (var x = 1; x <= can; x++) {

            if (can == 5) {
                switch (x) {
                    case 1:
                        porlectura = "20";
                        break;
                    case 2:
                        porlectura = "40";
                        break;
                    case 3:
                        porlectura = "60";
                        break;
                    case 4:
                        porlectura = "80";
                        break;
                    case 6:
                        porlectura = "100";
                        break;
                }

                var MejorPatron = LlamarAjax("Laboratorio/MejorPatron", "RangoHasta=" + RangoHasta + "&Factor=" + Factor + "&Porcentaje=" + porlectura + "&magnitud=6")*1;
                $("#Patron" + x).val(MejorPatron).trigger("change");
            }
        }
    }
}
    
function VerFotos() {
    if (Ingreso == 0)
        return false;
    CargarModalFoto(Ingreso, Foto,1);
}

$('select').select2();

if (document.location.hostname.match(/^(www\.)?chartjs\.org$/)) {
    (function (i, s, o, g, r, a, m) {
    i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
        (i[r].q = i[r].q || []).push(arguments)
    }, i[r].l = 1 * new Date(); a = s.createElement(o),
        m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
    ga('create', 'UA-28909194-3', 'auto');
    ga('send', 'pageview');
}

function CalcularTemperatura(guardar) {

    if (SensorLectura == "Excel") {
        $.jGrowl("Condiciones de temperatura y humedad obtenidas de forma manual", { life: 2000, theme: 'growl-success', header: '' });
    }

    if (window.myTemp != undefined) {
        window.myTemp.destroy();
    }

    if (window.myHume != undefined) {
        window.myHume.destroy();
    }

    TempMax = 0;
    TempMin = 100000000;
    HumeMax = 0;
    HumeMin = 100000000;
    TempVar = 0;
    HumeVar = 0;

    $("#TempMax").val("");
    $("#TempMin").val("");

    $("#HumeMax").val("");
    $("#HumeMin").val("");

    $("#ForMinTemp1").html("");
    $("#ForMinTemp2").html("");
    $("#ForMinTemp3").html("");

    $("#ForMaxTemp1").html("");
    $("#ForMaxTemp2").html("");
    $("#ForMaxTemp3").html("");

    $("#ForMinHume1").html("");
    $("#ForMinHume2").html("");
    $("#ForMinHume3").html("");

    $("#ForMaxHume1").html("");
    $("#ForMaxHume2").html("");
    $("#ForMaxHume3").html("");

    $("#CoTempMax").val("");
    $("#CoTempMin").val("");

    $("#CoTempMax").removeClass("bg-danger");
    $("#CoTempMin").removeClass("bg-danger");

    $("#CoHumeMax").removeClass("bg-danger");
    $("#CoHumeMin").removeClass("bg-danger");

    $("#CoHumeMax").val("");
    $("#CoHumeMin").val("");

    //********************************************************

    

    var fechaactual = "";
    var sensor = $("#Sensor").val() * 1;
    var variable = $("#Sensor");
    if (!variable)
        return false;
    if (sensor == 0 || HoraInicio == "" || (HoraFinal == "" && HoraCondicion == "") || String(sensor) == "NaN") {
        return false;
    }

    
    var datos = LlamarAjax("Laboratorio/DatosGraTemperatura", "sensor=" + sensor + "&horai=" + HoraInicio + "&horaf=" + (HoraFinal == "" ? HoraCondicion : HoraFinal) + "&fecha=" + (FechaCal == "" ? FechaActual : FechaCal));
        
    /*$.ajax({
        url: 'https://www.webstorage-service.com/data/viewcur.html?bsn=429A00BF&rsn=42BC0232',
        dataType: 'json'
    }).then((resultado) => {
        console(resultado);
    });*/

    if (datos != "[]") {
        var data = JSON.parse(datos);

        var colores = ['rgb(255, 99, 132)', 'rgb(255, 159, 64)', 'rgb(255, 205, 86)', 'rgb(75, 192, 192)', 'rgb(54, 162, 235)', 'rgb(153, 102, 255)', 'rgb(201, 203, 207)'];
        var puntos;
        var newDataset;

        fechaactual = data[0].fecha;

        var config = {
            type: 'scatter',
            data: {
                datasets: []
            },
            options: {
                responsive: true,
                title: {
                    display: true,
                    text: 'Gráfica de Temperatura ' + fechaactual
                },
                scales: {
                    xAxes: [{
                        display: true,
                        labelString: 'Par torsional nominal en lbf·ft'

                    }],
                    yAxes: [{
                        display: true,
                        labelString: 'Desviación ± U expandida en %'
                    }]
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
            }
        }
        var config2 = {
            type: 'scatter',
            data: {
                datasets: []
            },
            options: {
                responsive: true,
                title: {
                    display: true,
                    text: 'Gráfica de Humedad ' + fechaactual
                },
                scales: {
                    xAxes: [{
                        display: true,
                        labelString: 'Par torsional nominal en lbf·ft'

                    }],
                    yAxes: [{
                        display: true,
                        labelString: 'Desviación ± U expandida en %'
                    }]
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
            }
        }

        newDataset = {
            label: 'Temperatura',
            fill: false,
            borderColor: colores[1],
            backgroundColor: colores[1],
            data: [],
        };
        for (var i = 0; i < data.length; i++) {

            if (data[i].hora >= HoraInicio && data[i].hora <= (HoraFinal == "" ? HoraCondicion : HoraFinal)) {

                if (data[i].temperatura > TempMax * 1)
                    TempMax = data[i].temperatura;

                if (data[i].temperatura < TempMin * 1)
                    TempMin = data[i].temperatura;

                puntos = {
                    x: data[i].punto,
                    y: data[i].temperatura
                }
                newDataset.data.push(puntos);
            }
        }

        config.data.datasets.push(newDataset);
        if (TempMin == 100000000)
            TempMin = 0;

        $("#TempMax").html(TempMax);
        $("#TempMin").html(TempMin);

        newDataset = {
            label: 'Humedad',
            fill: false,
            borderColor: colores[4],
            backgroundColor: colores[4],
            data: [],
        };
        for (var i = 0; i < data.length; i++) {
            if (data[i].hora >= HoraInicio && data[i].hora <= (HoraFinal == "" ? HoraCondicion : HoraFinal)) {
                if (data[i].humedad > HumeMax * 1)
                    HumeMax = data[i].humedad;

                if (data[i].humedad < HumeMin * 1)
                    HumeMin = data[i].humedad;

                puntos = {
                    x: data[i].punto,
                    y: data[i].humedad
                }
                newDataset.data.push(puntos);
            }
        }

        if (HumeMin * 1 == 100000000)
            HumeMin = 0;


        $("#HumeMax").html(HumeMax);
        $("#HumeMin").html(HumeMin);

        config2.data.datasets.push(newDataset);

        window.myTemp = new Chart(gratemp, config);
        window.myTemp.update();

        window.myHume = new Chart(grahume, config2);
        window.myHume.update();

        ForMinTemp1 = TempMin * TempMin;
        ForMinTemp2 = TempMin;
        ForMinTemp3 = (parseFloat($("#MinTemp1").html()) * (TempMin * TempMin)) + (parseFloat($("#MinTemp2").html()) * TempMin) + parseFloat($("#MinTemp3").html());

        $("#ForMinTemp1").html(formato_numero(ForMinTemp1, 1, ".", ",", ""));
        $("#ForMinTemp2").html(formato_numero(ForMinTemp2, 1, ".", ",", ""));
        $("#ForMinTemp3").html(formato_numero(ForMinTemp3, 4, ".", ",", ""));

        ForMaxTemp1 = TempMax * TempMax;
        ForMaxTemp2 = TempMax;
        ForMaxTemp3 = (parseFloat($("#MaxTemp1").html()) * (TempMax * TempMax)) + (parseFloat($("#MaxTemp2").html()) * TempMax) + parseFloat($("#MaxTemp3").html());

        $("#ForMaxTemp1").html(formato_numero(ForMaxTemp1, 1, ".", ",", ""));
        $("#ForMaxTemp2").html(formato_numero(ForMaxTemp2, 1, ".", ",", ""));
        $("#ForMaxTemp3").html(formato_numero(ForMaxTemp3, 4, ".", ",", ""));

        ForMinHume1 = HumeMin * HumeMin;
        ForMinHume2 = HumeMin;
        ForMinHume3 = (parseFloat($("#MinHume1").html()) * (HumeMin * HumeMin)) + (parseFloat($("#MinHume2").html()) * HumeMin) + parseFloat($("#MinHume3").html());

        $("#ForMinHume1").html(formato_numero(ForMinHume1, 1, ".", ",", ""));
        $("#ForMinHume2").html(formato_numero(ForMinHume2, 1, ".", ",", ""));
        $("#ForMinHume3").html(formato_numero(ForMinHume3, 4, ".", ",", ""));

        ForMaxHume1 = HumeMax * HumeMax;
        ForMaxHume2 = HumeMax;
        ForMaxHume3 = (parseFloat($("#MaxHume1").html()) * (HumeMax * HumeMax)) + (parseFloat($("#MaxHume2").html()) * HumeMax) + parseFloat($("#MaxHume3").html());

        $("#ForMaxHume1").html(formato_numero(ForMaxHume1, 1, ".", ",", ""));
        $("#ForMaxHume2").html(formato_numero(ForMaxHume2, 1, ".", ",", ""));
        $("#ForMaxHume3").html(formato_numero(ForMaxHume3, 4, ".", ",", ""));

        $("#CoTempMax").val(formato_numero(TempMax + ForMaxTemp3, 1, ".", ",", "") + " °C");
        $("#CoTempMin").val(formato_numero(TempMin + ForMinTemp3, 1, ".", ",", "") + " °C");
        TempVar = Math.abs((TempMax + ForMaxTemp3) - (TempMin + ForMinTemp3));
        HumeVar = Math.abs((HumeMax + ForMaxHume3) - (HumeMin + ForMinHume3));
        if (TempVar >= 1) {
            $("#CoTempMax").addClass("bg-danger");
            $("#CoTempMin").addClass("bg-danger");
            ErrorTemp = 1;


        } else {
            ErrorTemp = 0;
            $("#CoTempMax").removeClass("bg-danger");
            $("#CoTempMin").removeClass("bg-danger");
            ErrorTemp = 0;
        }

        if (((HumeMax + ForMaxHume3) >= 75) || ((HumeMin + ForMinHume3) >= 75)) {
            $("#CoHumeMax").addClass("bg-danger");
            $("#CoHumeMin").addClass("bg-danger");
            ErrorHume = 1;
        } else {
            $("#CoHumeMax").removeClass("bg-danger");
            $("#CoHumeMin").removeClass("bg-danger");
            ErrorHume = 0;
        }

        $("#CoHumeMax").val(formato_numero(HumeMax + ForMaxHume3, 1, ".", ",", "") + " %HR");
        $("#CoHumeMin").val(formato_numero(HumeMin + ForMinHume3, 1, ".", ",", "") + " %HR");

        $("#VarTemp").html(formato_numero(TempVar, 1, ".", ",", ""));
        $("#VarHume").html(formato_numero(HumeVar, 1, ".", ",", ""));

        if (SensorLectura == "Excel") {

            TempVar = Math.abs(TempMax - TempMin);
            HumeVar = Math.abs(HumeMax - HumeMin);

            $("#CoHumeMax").val(formato_numero(HumeMax, 1, ".", ",", "") + " %HR");
            $("#CoHumeMin").val(formato_numero(HumeMin, 1, ".", ",", "") + " %HR");

            $("#CoTempMax").val(formato_numero(TempMax, 1, ".", ",", "") + " °C");
            $("#CoTempMin").val(formato_numero(TempMin, 1, ".", ",", "") + " °C");

            $("#VarTemp").html(formato_numero(TempVar, 1, ".", ",", ""));
            $("#VarHume").html(formato_numero(HumeVar, 1, ".", ",", ""));
        }

        if (guardar == 1) {
            var parametros = "tempmax=" + $("#CoTempMax").val().replace(".", ",") + "&tempmin=" + $("#CoTempMin").val().replace(".", ",") + "&tempvar=" + formato_numero(TempVar, 1, ",", ".", "") + " °C" +
                "&humemax=" + $("#CoHumeMax").val().replace(".", ",") + "&humemin=" + $("#CoHumeMin").val().replace(".", ",") + "&humevar=" + formato_numero(HumeVar, 1, ",", ".", "") + " %HR&ingreso=" + Ingreso + "&numero=" + NumCertificado + "&revision=" + Revision;

            LlamarAjax("Laboratorio/GuardarTemperatura", parametros);
        }

        if (Certificado == "") {
            if (ErrorTemp == 0 && ErrorHume == 0) {
                if (guardar == 1) {
                    EscucharMensaje("Condiciones de temperatura y humedad adecuadas para la calibración... Ya puede General el certificado");
                    $.jGrowl("Condiciones de temperatura y humedad adecuadas para la calibración... Ya puede General el certificado", { life: 5500, theme: 'growl-success', header: '' });
                } else {
                    $.jGrowl("Condiciones de temperatura y humedad adecuadas para la calibración", { life: 5500, theme: 'growl-success', header: '' });
                }

            } else {
                var mensaje = "";
                if (ErrorTemp == 1 && ErrorHume == 1)
                    mensaje = "temperatura y humedad";
                else {
                    if (ErrorTemp == 1)
                        mensaje = "temperatura";
                    else
                        mensaje = "humedad";
                }
                EscucharMensaje("Condiciones de " + mensaje + " no adecuadas, para la calibración. No podrá General el certificado");

                swal("Acción Cancelada", "Condiciones de " + mensaje + " no adecuadas, para la calibración... No podrá General el certificado", "warning");
                $("#documento").focus();
            }
        }
    }
}

function VerCondiciones() {
    $("#documento").focus();
}

function AbrirGrafica() {
    var direccion = $("#medidor").val();
    window.open(direccion);

}

function CargarTemperatura() {
    if (Ingreso == 0)
        return

    if (SensorLectura != "Excel") {
        swal("Acción Cancelada", "No se puede cargar archivo de temperatura con sensores de lectura automatizadas", "warning");
        return false;
    }
    
    if (Certificado != "") {
        swal("Acción Cancelada", "No se puede editar la temperatura de un certificado", "warning");
        return false;
    }
        
    var documento = $("#documento").val();
    if (documento == "") {
        $("#documento").focus();
        swal("Acción Cancelada", "Debe de seleccionar el archivo CSV de temperatura", "warning");
        return false;
    }

    d = new Date();
    HoraFinal = (d.getHours() * 1 < 10 ? "0" : "") + d.getHours() + ":" + (d.getMinutes() * 1 < 10 ? "0" : "") + d.getMinutes();
    $("#CaHoraFin").val(HoraFinal);

    ActivarLoad();
    setTimeout(function () {
        var parametros = "ingreso=" + Ingreso + "&numero=" + NumCertificado + "&revision=" + Revision + "&sensor=" + SensorTemperatura + "&HoraInicio=" + HoraInicio;
        var datos = LlamarAjax("Laboratorio/Temperatura", parametros);
        DesactivarLoad();
        datos = datos.split("|");
        if (datos[0] == "0") {
            swal("", ValidarTraduccion(datos[1]), "success");
            $("#documento").val("");
            CalcularTemperatura(1);
        } else {
            $("#documento").val("");
            swal("Acción Cancelada", ValidarTraduccion(datos[1]), "warning");
        }
    }, 15);
}

function ImprimirCertificado(tipo) {
    if (Ingreso == 0)
        return false;

    if (tipo == 2 && FirmaCertificado == "DIGITAL") {
        swal("Acción Cancelada", "No se puede imprimir un certificado con firma manual a este cliente", "warning");
        return false;
    }

    if (Certificado == "") {
        swal("Acción Cancelada", "Debe generar primero el certificado", "warning");
        return false;
    }


    ActivarLoad();
    setTimeout(function () {
        var parametros = "ingreso=" + Ingreso + "&numero=" + NumCertificado + "&revision=" + Revision + "&tipo=" + tipo+"&opcion=2";
        var datos = LlamarAjax("RpPlantillas/RpPlanillaPT1_V07", parametros);
        DesactivarLoad();
        datos = datos.split("|");
        if (datos[0] == "0") {
            window.open(url +  "DocumPDF/" + datos[1]);
        } else {
            if (datos[0] == "9")
                window.open(url + "Certificados/" + datos[1] + ".pdf");
            else
                swal("", ValidarTraduccion(datos[1]), "warning");
        }
    }, 15);
}

function ImprimirPrevia() {

    if (Ingreso == 0)
        return false;

    if (GuardaTemp == 1) {
        swal("Acción Cancelada", "Debe primero Guardar el informe", "warning");
        return false;
    }

    if (Certificado != "") {
        swal("Acción Cancelada", "No se puede ver la vista previa de un certificado generado", "warning");
        return false;
    }
    
    ActivarLoad();
    setTimeout(function () {
        var parametros = "ingreso=" + Ingreso + "&numero=" + NumCertificado + "&revision=" + Revision + "&tipo=0&opcion=1";
        var datos = LlamarAjax("RpPlantillas/RpPlanillaPT1_V07", parametros);
        DesactivarLoad();
        datos = datos.split("|");
        if (datos[0] == "0") {
            window.open(url + "DocumPDF/" + datos[1]);
        } else {
            swal("", datos[1], "warning");
        }
    }, 15);
    
}


function LlamarGenerarCertificado() {

    if (Ingreso == 0 || IdCalibracion == 0)
        return false;

    if (GuardaTemp == 1) {
        swal("Acción Cancelada", "Debe primero Guardar el informe", "warning");
        return false;
    }

    if (Certificado != "") {
        swal("Acción Cancelada", "Ya se generó el certificado de este ingreso", "warning");
        return false;
    }

    if (TempMax == 0 || TempMin == 100000000 || HumeMax == 0 || HumeMin == 100000000) {
        swal("Acción Cancelada", "Debe cargar el archivo de temperatura y humedad", "warning");
        return false;
    }

    var mensaje = "";
    if (TempVar > 1 || HumeMax >= 75) 
        mensaje = "temperatura y humedad";
    else {
        if (TempVar == 1)
            mensaje = "temperatura";
        if (HumeMax >= 75)
            mensaje = "humedad";
    }

    if (mensaje != "") {
        $("#documento").focus();
        swal("Acción Cancelada", "Condiciones de " + mensaje + " no adecuadas, para la calibración... No podrá General el certificado", "warning");
        EscucharMensaje("Condiciones de " + mensaje + " no adecuadas, para la calibración. No podrá General el certificado");
        return false;
    }

    $("#TipoAccesso").val("GENERAR CERTIFICADO CON FIRMA");
    $("#modalControlAcceso").modal("show");
    $("#AccClave").val("");
    $("#AccClave").focus();
    
}

function GuardarFirma(certificado) {
    var firma = canvafirma.toDataURL("image/png");
    $.ajax({
        type: 'POST',
        url: url + "Laboratorio/GuardarFirma",
        data: "firma=" + firma + "&certificado=" + certificado + "&usuario=" + localStorage.getItem("UsuarioCertificado"),
        success: function (msg) {
            respuesta = "si";
        }
    });
}


function GenerarCertificado() {

    var concepto = $("#Concepto").val() * 1;
        
    if (Ingreso == 0 || IdCalibracion == 0)
        return false;
    var ajuste = 1;
    for (var x = 1; x <= CantLecturas; x++) {
        if ($.trim($("#SerPromedio" + x).val()) == "")
            return false;
        if ($("#SerPromedio" + x).val() == "REQUIERE AJUSTE") {
            ajuste = 1;
        }
    }

    if (concepto != 1) {
        swal("Acción Cancelada", "Para poder generar el certificado el equipo debe de estar apto para calibrar", "warning");
        return false;
    }

    /*if (FechaCal != FechaActual) {
        swal("Acción Cancelada", "Debe primero Guardar el informe", "warning");
        return false;

    }*/

    if (GuardaTemp == 1) {
        swal("Acción Cancelada", "Debe primero Guardar el informe", "warning");
        return false;
    }

    if (Certificado != "") {
        swal("Acción Cancelada", "Ya se generó el certificado de este ingreso", "warning");
        return false;
    }

    if (TempMax == 0 || TempMin == 100000000 || HumeMax == 0 || HumeMin == 100000000) {
        swal("Acción Cancelada", "Debe cargar el archivo de temperatura y humedad", "warning");
        return false;
    }

    var mensaje = "";
    if (TempVar > 1 || HumeMax >= 75)
        mensaje = "temperatura y humedad";
    else {
        if (TempVar == 1)
            mensaje = "temperatura";
        if (HumeMax >= 75)
            mensaje = "humedad";
    }

    if (mensaje != "") {
        $("#documento").focus();
        swal("Acción Cancelada", "Condiciones de " + mensaje + " no adecuadas, para la calibración... No podrá General el certificado", "warning");
        EscucharMensaje("Condiciones de " + mensaje + " no adecuadas, para la calibración. No podrá General el certificado");
        return false;
    }
        
    if (localStorage.getItem("FirmaCertificado") == "") {
        swal("Acción Cancelada", "Debe primero firmar el certificado", "warning");
        return false;
    }

    Solicitante = Solicitante.replace(/&/g, '%26');
    Direccion = Direccion.replace(/&/g, '%26');
        
    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: ValidarTraduccion('¿Seguro que desea generar el certificado ' + (ajuste == 1 ? "con datos fuera del porcentaje de tolerancia" : "") + " del ingreso número " +  Ingreso + '?'),
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Si'),
        cancelButtonText: ValidarTraduccion('No'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                $.post("Laboratorio/GenerarCertificado", "ingreso=" + Ingreso + "&revision=" + Revision + "&numcertificado=" + NumCertificado + "&NombreCer=" + Solicitante + "&DireccionCer=" + Direccion +
                    "&usuario=" + localStorage.getItem("UsuarioCertificado") + "&version=" + IdVersion)
                    .done(function (data) {
                        var datos = data.split("|");

                        if (datos[0] == "0") {
                            Certificado = datos[2];
                            GuardarFirma(Certificado);
                            localStorage.setItem("FirmaCertificado", "");
                            clearCanvas(canvas, ctx);
                            $("#FirmarCertificadoPT").addClass("hidden");
                            $("#PrSerie1").prop("disabled", true);
                            $("#PrSerie2").prop("disabled", true);
                            $("#PrSerie3").prop("disabled", true);
                            $("#PrSerie4").prop("disabled", true);
                            $("#PrSerie5").prop("disabled", true);
                            $("#ProximaCali").prop("disabled", true);
                            $("#Observacion").prop("disabled", true);
                            $("#TiempoCali").prop("disabled", true);
                            for (var x = 1; x <= CantRegitro; x++) {
                                $("#Serie1" + x).prop("disabled", true);
                                $("#Serie2" + x).prop("disabled", true);
                                $("#Serie3" + x).prop("disabled", true);
                                $("#Serie4" + x).prop("disabled", true);
                                $("#Serie5" + x).prop("disabled", true);
                                $("#Serie6" + x).prop("disabled", true);
                                $("#Serie7" + x).prop("disabled", true);
                                $("#Serie8" + x).prop("disabled", true);
                                $("#Serie9" + x).prop("disabled", true);
                                $("#Serie10" + x).prop("disabled", true);
                                $("#PaPorLectura" + x).prop("disabled", true);
                                $("#Patron" + x).prop("disabled", true);
                            }
                            $("#Certificado").html(Certificado);
                            $("#FechaExp").html(FechaActual);
                            $.jGrowl(datos[1] + " " + datos[2], { life: 1500, theme: 'growl-success', header: '' });
                            EscucharMensaje(datos[1]);
                            resolve()
                        } else {
                            reject(datos[1]);
                        }
                    })
            })
        }
    }]);
}

TablaCMC();

function TiempoTrascurrido() {
    if (Ingreso == 0 || GuardaTemp == 0 || HoraInicio == "")
        return false;
    d = new Date();
    HoraCondicion = horaactual = (d.getHours() * 1 < 10 ? "0" : "") + d.getHours() + ":" + (d.getMinutes() * 1 < 10 ? "0" : "") + d.getMinutes();
    $("#CaTiempo").val(RestarHoras(HoraInicio, horaactual));
    CalcularTemperatura(1);
}


if (localStorage.getItem("CerIngreso") * 1 > 0) {
    LimpiarTodo();
    $("#Ingreso").val(localStorage.getItem("CerIngreso"));
    BuscarIngreso(localStorage.getItem("CerIngreso"), "", 0);
}

$(document).ready(function () {
    $("input").on('paste', function (e) {
        e.preventDefault();
        swal('Esta acción está prohibida');
    })

})

function pulsar(e) {
    tecla = (document.all) ? e.keyCode : e.which;
    return (tecla != 13);
}


function ModalCotizacion() {
    if (Ingreso == 0)
        return false;
    $("#modalcotizacion").modal("show");
}

function ModalActualizarInstr() {
    if (Ingreso == 0 || Certificado != "") {
        return false;
    }
    var datos = LlamarAjax("Laboratorio/BuscarInstrumentoPT", "ingreso=" + Ingreso);
    var data = JSON.parse(datos);
    $("#MResolucion").val(data[0].resolucion);
    $("#MTolerancia").val(data[0].tolerancia);
    $("#MInstrunmento").val(data[0].descripcion);
    $("#MIndicacion").val(data[0].indicacion).trigger("change");
    $("#MClasificacion").val(data[0].clasificacion).trigger("change");
    $("#MClase").val(data[0].clase).trigger("change");
    $("#ingresoinst").html(Ingreso);
    $("#modalInstrumento").modal("show");
}

function BuscarDescripcionEquipo() {
    var clase = $("#MClase").val();
    var tipo = $("#MClasificacion").val();
    $("#MInstrumento").val("");
    $("#Lecturas").val("");

    if (clase != "" && tipo != "") {
        var datos = LlamarAjax("Laboratorio/BuscarDescripcionEquipo", "tipo=" + tipo + "&clase=" + clase).split("||");
        if (datos != "XX") {
            $("#MInstrumento").val(datos[0]);
            $("#Lecturas").val(datos[1]);
        }
    }
}

function ActualizarInstrumento() {
    var resolucion = NumeroDecimal($("#MResolucion").val());
    var tolerancia = NumeroDecimal($("#MTolerancia").val());
    var instrumento = $.trim($("#MInstrumento").val())
    var indicacion = $("#MIndicacion").val();
    var clasificacion = $("#MClasificacion").val();
    var clase = $("#MClase").val();
    

    var mensaje = "";

    if (tolerancia == 0) {
        $("#MTolerancia").focus();
        mensaje += "Debe de ingresar la tolerancia del instrumento <br>";
    }
    if (resolucion == 0) {
        $("#MResolucion").focus();
        mensaje += "Debe de ingresar la resolucion del instrumento <br>";
    }
    if (clasificacion == "") {
        $("#MClasificacion").focus();
        mensaje += "Debe de seleccionar la clasificacion del instrumento <br>";
    }
    if (clase== "") {
        $("#MTipo").focus();
        mensaje += "Debe de seleccionar el tipo de instrumento <br>";
    }
    if (indicacion == "") {
        $("#MIndicacion").focus();
        mensaje += "Debe de seleccionar la indicacion del instrumento <br>";
    }

    if (mensaje != "") {
        swal("Acción Cancelada", mensaje, "warning");
        return false;
    }

    ActivarLoad();
    setTimeout(function () {
        var parametros = "ingreso=" + Ingreso + "&resolucion=" + resolucion + "&tolerancia=" + tolerancia +
            "&indicacion=" + indicacion + "&clase=" + clase + "&clasificacion=" + clasificacion + "&instrumento=" + instrumento;
        var datos = LlamarAjax("Laboratorio/EditarInstrumentoPT", parametros).split("|");
        if (datos[0] == "0") {
            $("#modalInstrumento").modal("hide");
            $("#Descripcion").html(instrumento);
            $("#Indicacion").html(indicacion);
            $("#Clasificacion").html(clasificacion);
            $("#Clase").html(clase);
            $("#Tolerancia").html(tolerancia);
            Tolerancia = tolerancia;
            Resolucion = resolucion;
            $("#Resolucion").html(Resolucion);
            TipoInst = clasificacion;
            CalcularTotal();
            DesactivarLoad();
            $("#FormPrevia").submit();
        } else
            swal("Acción Cancelada", datos[1], "warning");
    }, 15);
}

function CambioConcepto(concepto) {

    if (Certificado != "")
        return false;
    
    $("#Suministro").prop("disabled", true);
    $("#Ajuste").prop("disabled", true);
    $("#Conclusion").prop("disabled", true);
    $("#Observaciones").prop("disabled", false);

    $("#Observaciones").addClass("bg-amarillo");
    $("#Ajuste").removeClass("bg-amarillo");
    $("#Suminstro").removeClass("bg-amarillo");
    $("#Conclusion").removeClass("bg-amarillo");

    $("#Documento").removeClass("bg-amarillo");
    $("#Documento").prop("disabled", true);
    $("#Observaciones").val("La calibración se realizó en las instalaciones permanentes de Calibration Service S.A.S.");
    concepto = concepto * 1;

    switch (concepto) {
        case 1:
            $(".repetibilidad").each(function (index) {
                if ($(this).val() * 1 == 0)
                    $(this).val("");
            });
            $(".repetibilidad").prop("readonly", false);
            $(".repetibilidad").removeClass("bg-lineas-diagonales");
            break;
        case 2:
            $(".repetibilidad").val("0");
            $(".repetibilidad").prop("readonly", true);
            $(".repetibilidad").addClass("bg-lineas-diagonales");

            $("#Suministro").prop("disabled", false);
            $("#Ajuste").prop("disabled", false);
            $("#Observaciones").prop("disabled", true);
            $("#Conclusion").prop("disabled", true);

            $("#Observaciones").removeClass("bg-amarillo");
            $("#Ajuste").addClass("bg-amarillo");
            $("#Suministro").addClass("bg-amarillo");
            $("#Conclusion").removeClass("bg-amarillo");
            $("#Observaciones").val("");
            $("#Conclusion").val("");
            break;
        case 3:
            $(".repetibilidad").val("0");
            $(".repetibilidad").prop("readonly", true);
            $(".repetibilidad").addClass("bg-lineas-diagonales");

            $("#Observaciones").prop("disabled", false);
            $("#Conclusion").prop("disabled", false);

            $("#Observaciones").addClass("bg-amarillo");
            $("#Conclusion").addClass("bg-amarillo");
            $("#Ajuste").val("");
            $("#Observaciones").val("");
            $("#Suministro").val("");
            break;
    }
       
}

function RequerirAjuste() {
    if (AplicarTemporal == 1 && Certificado == "") {
        var ajuste = 0;
        GuardarTemporal();
        for (var x = 1; x <= CantLecturas; x++) {
            for (var y = 1; y <= 10; y++) {
                if ($.trim($("#Serie" + y + x).val()) == "")
                    return false;
            }
        }

        for (var x = 1; x <= CantLecturas; x++) {
            if ($.trim($("#SerPromedio" + x).val()) == "")
                return false;
            if ($("#SerPromedio"+ x).val() == "REQUIERE AJUSTE") {
                ajuste = 1;
            }
        }
                
        if (ajuste == 1) {
            $("#OptSinPosi").prop("disabled", false);
            $("#OptAjuste").prop("disabled", false);
            $("#Concepto").val(2).trigger("change");
            EscucharMensaje("El equipo requiere ajuste");
            $.jGrowl("El equipo requiere ajuste", { life: 6000, theme: 'growl-warning', header: '' });
        } else {
            $("#OptSinPosi").prop("disabled", true);
            $("#OptAjuste").prop("disabled", true);
            $("#Concepto").val(1).trigger("change");
            EscucharMensaje("El equipo está  apto para calibrar");
            $.jGrowl("El equipo está  apto para calibrar", { life: 6000, theme: 'growl-success', header: '' });
        }
    }

    
}

CambioConcepto(1, 1);

function EnviarReporte() {

    var concepto = $("#Concepto").val() * 1;
    
    if (IdCalibracion == 0)
        return false;
    if (GuardaTemp == 1 || IdCalibracion == 0) {
        swal("Acción Cancelada", "Debe primero guardar los datos", "warning");
        return false;
    }

    $("#eCliente").val($("#Solicitante").html());
    $("#eCorreo").val(CorreoEmpresa);
    
    if (concepto == 2) {
        $("#spcotizacion").html(Ingreso);
        $("#enviar_cotizacion").modal("show");
    } else {
        swal("Acción Cancelada", "Solo se podra enviar reportes de ajuste cuando el ingreso requiera ajuste o suministro", "warning");
    }

}

$("#formenviarcotizacion").submit(function (e) {

    e.preventDefault();
    var observacion = $.trim($("#ObserEnvio").val())
    var correo = $.trim($("#eCorreo").val());
    var plantilla = $("#Plantilla").val() * 1;
    a_correo = correo.split(";");

    if (ValidarCorreo(CorreoEmpresa) == 2) {
        swal(ValidarTraduccion("Acción Cancelada"), ValidarTraduccion("El correo electrónico del contacto no es válido ") + ":" + CorreoEmpresa, "warning");
        return false;
    }

    for (var x = 0; x < a_correo.length; x++) {
        if (ValidarCorreo($.trim(a_correo[x])) == 2) {
            $("#eCorreo").focus();
            swal(ValidarTraduccion("Acción Cancelada"), ValidarTraduccion("Formato de correo electrónico no válido") + ":" + a_correo[x], "warning");
            return false;
        }
    }


    ActivarLoad();
    setTimeout(function () {
        var parametros = "magnitud=6&ingreso=" + Ingreso + "&principal=" + CorreoEmpresa + "&e_email=" + correo + "&observacion=" + observacion + "&plantilla=" + NumCertificado + "&version=7" + "&revision=" + Revision;
        var datos = LlamarAjax("Laboratorio/EnvioCotizacion", parametros);
        DesactivarLoad();
        datos = datos.split("|");
        if (datos[0] == "0") {
            swal("", ValidarTraduccion(datos[1]) + " " + datos[2], "success");
            $("#enviar_cotizacion").modal("hide");
        } else
            swal("Acción Cancelada", datos[1], "warning");
    }, 15);

    return false;
});

function GenerarInforme() {

    var plantilla = 22;


    if (Ingreso == 0)
        return false;

    var informe = $("#InformeTecnico").val() * 1;
    if (informe > 0) {
        swal("Acción Cancelada", "Ya se generó el certificado de este ingreso", "warning");
        return false;
    }

    if (IdReporte == 0) {
        swal("Acción Cancelada", "Debe primero guardar el reporte", "warning");
        return false;
    }

    var concepto = $("#Concepto").val() * 1;
    if (concepto != 3) {
        swal("Acción Cancelada", "Solo se podrá generar informes cuando el ingreso no está apto para calibrar ó para mantenimiento y limpieza", "warning");
        return false;
    }

    var mensaje = '¿Seguro que desea generar el informe técnico del ingreso número ' + Ingreso + '?';
    
    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: mensaje,
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Si'),
        cancelButtonText: ValidarTraduccion('No'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                $.post("Laboratorio/InformeTecnicoDev", "ingreso=" + Ingreso + "&reporte=0&plantilla=" + plantilla)
                    .done(function (data) {
                        datos = data.split("|");
                        if (datos[0] == "0") {
                            $("#InformeTecnico").val(datos[2]);
                            ImprimirInforme();
                            resolve()
                        } else {
                            reject(datos[1]);
                        }
                    })
            })
        }
    }]);
};

function ImprimirInforme() {

    if (Ingreso == 0)
        return false;

    var concepto = $("#Concepto").val() * 1;
    var plantilla = 22;
    ActivarLoad();
    setTimeout(function () {
        var parametros = "ingreso=" + Ingreso + "&tipo=1&concepto=" + concepto + "&plantilla=" + plantilla;
        var datos = LlamarAjax("Laboratorio/RpInformeTecnico", parametros);
        DesactivarLoad();
        datos = datos.split("|");
        if (datos[0] == "0") {
            window.open("DocumPDF/" + datos[1]);
        } else {
            swal("", ValidarTraduccion(datos[1]), "warning");
        }

    }, 15);
}


$('select').select2();
DesactivarLoad();
