﻿d = new Date();
month = d.getMonth() + 1;
day = d.getDate();

var Anio = $.trim(d.getFullYear());
Anio = $.trim(Anio.substr(2, 2))

output = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-01';

output2 = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-' +
    (day < 10 ? '0' : '') + day;

$("#FechaDesde").val(output);
$("#FechaHasta").val(output2);
$("#Magnitud").html(CargarCombo(1, 1));


function Consultar() {
    var magnitud = $("#Magnitud").val() * 1;
    var ingreso = $("#Ingreso").val() * 1;
    var magnitud = $("#Magnitud").val() * 1;

    var fechad = $("#FechaDesde").val();
    var fechah = $("#FechaHasta").val();
    var certificado = $.trim($("#Certificado").val());

    if (fechad == "" && fechah == "") {
        swal("", ValidarTraduccion("debe de ingresar una fecha desde y una fecha hasta"), "warning");
        return false;
    }

    ActivarLoad();
    setTimeout(function () {

        var parametros = "fechaini=" + fechad + "&fechafin=" + fechah + "&ingreso=" + ingreso + "&certificado=" + certificado + "&magnitud=" + magnitud;
        var datos = LlamarAjax('Laboratorio/TablaCondiciones', parametros);
        var datasedjson = JSON.parse(datos);
        $('#TablaCondicion').DataTable({
            data: datasedjson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "fila" },
                { "data": "fecha" },
                { "data": "hora" },
                { "data": "temperatura" },
                { "data": "vartemperatura" },
                { "data": "humedad" },
                { "data": "varhumedad" }
            ],
            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy', 'colvis'
            ],
        });
        DesactivarLoad();
    }, 15);
}

$('select').select2();
DesactivarLoad();
