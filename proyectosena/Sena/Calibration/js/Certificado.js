﻿var Ingreso = 0;
var IdCertificado = 0;
var ErrorEnter = 0;
var Certificado = "";
var IdOT = 0;
var Foto = 0;
var TipoPlantilla = 0;
var TituloMensaje = "certificado";
var PeriodoMantenimiento = 0;

var AutoNombre = [];
var AutoDireccion = [];

localStorage.setItem("Ingreso", Ingreso);
var PermisioEspecial = PerGeneSistema("PERMISO ADMINISTRADOR LABORATORIO");

function LimpiarTodo() {
    LimpiarDatos();
    $("#Ingreso").val("");
    $("#Ingreso").focus();
}


function CambioDocumento(documento) {

    if (documento == 1) {
        $("#TipoDocumento").val("1").trigger("change");
        $("#DivDocumento").addClass("hidden");
        $("#AcreCertificado").prop("disabled", true);
    } else {
        $("#AcreCertificado").prop("disabled", false);
        $("#TipoDocumento").val("1").trigger("change");
        $("#DivDocumento").removeClass("hidden");
    }
    LimpiarDatos();
    ConsultarCertificados(0);
}

function CambioTipoDocumento(documento) {
    if (documento == 1) {
        $("#tituloDocumento").html("DATOS DEL CERTIFICADO");
        $("#AcreCertificado").prop("disabled", false);
        $("#ProCertificado").prop("disabled", false);
        $(".titulo").html("Certificado");
        TituloMensaje = "certificado";
    } else {
        $("#tituloDocumento").html("DATOS DEL INFORME TÉCNICO");
        $("#AcreCertificado").prop("disabled", true);
        $("#ProCertificado").prop("disabled", true);
        $(".titulo").html("Informe");
        TituloMensaje = "informe técnico";
    }
    LimpiarDatos();
}

function LimpiarDatos() {
    Ingreso = 0;
    IdCertificado = 0;
    Certificado = "";
    $("#Equipo").val("");
    $("#Marca").val("");
    $("#Modelo").val("");
    $("#Serie").val("");
    $("#ProCertificado").val("");
    $("#FecCertificado").val("");
    $("#AcreCertificado").val("").trigger("change");
    $("#Observacion").val("");
    $("#Observaciones").val("");
    $("#Certificado").val("");
    $("#Documento").val("");
    $("#registrocertificado").html("");
    $("#Remision").val("");
    $("#Rango").val("");
    $("#Magnitud").val("");

    $("#Sitio").val("");
    $("#Garantia").val("");
    $("#Accesorio").val("");

    $("#ObservRecep").val("");
    $("#Metodo").val("");
    $("#Punto").val("");
    $("#Servicio").val("");
    $("#ObservCerti").val("");
    $("#DireccionCe").val("");
    $("#NombreCe").val("");

    $("#DireccionCerti").val("");
    $("#NombreCerti").val("");

    localStorage.setItem("Ingreso", Ingreso);

    $("#Usuario").val("");
    $("#FechaIng").val("");
    $("#Tiempo").val("");
    $("#NumCertificado").val("1").trigger("change");
}

$("#TablaReportar > tbody").on("dblclick", "tr", function () {
    var row = $(this).parents("td").context.cells;
    var numero = row[1].innerText;
    $("#Ingreso").val(numero);
    BuscarIngreso(numero, 0, "0");
});


function LlamarFotoDet(ingreso, fotos) {
    if (fotos == 0)
        return false
    CargarModalFoto(ingreso, fotos, 1);
}

function ConsultarCertificados(tipo) {

    ActivarLoad();
    setTimeout(function () {
        var parametros = "tipo=" + $("#Tipo").val();
        var datos = LlamarAjax("Laboratorio/TablaCertificadoIngreso", parametros).split("||");
        DesactivarLoad();
        var datajson = JSON.parse(datos[0]);
        $('#TablaReportar').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "fila" },
                { "data": "ingreso" },
                { "data": "equipo" },
                { "data": "plantilla" },
                { "data": "fecha" },
                { "data": "tiempo" }
            ],

            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy', 'colvis'
            ]
        });
        if (tipo == 0)
            $("#NumCertificado").html(datos[1]);
    }, 15);
}

function DescargarExcel(id, tipo) {
    window.open(url + "PlantillasCertificados/" + id + tipo);
}

ConsultarCertificados(0);

function GuardarDocumento() {
    var documento = document.getElementById("Documento");
    if ($.trim(documento.value) != "") {

        var file = documento.files[0];
        var data = new FormData();
        data.append('documento', file);
        var resultado = '';
        var direccion = url + "Cotizacion/AdjuntarPDF";
        $.ajax({
            url: direccion,
            type: 'POST',
            contentType: false,
            data: data,
            processData: false,
            async: false,
            success: function (datos) {
                datos = datos.split("|");
                if (datos[0] != "0") {
                    swal(ValidarTraduccion("Error al cargar el archivo"), "", "error");
                } else {
                    if (datos[2] != "XX")
                        swal(ValidarTraduccion(datos[2]), "", "error");
                }
            }
        });
    }
}

function BuscarIngreso(numero, code, tipo) {
    var tipocert = $("#Tipo").val() * 1;
    if ((Ingreso != (numero * 1)) && ($("#Equipo").val() != ""))
        LimpiarDatos();
    if ((code.keyCode == 13 || tipo == "0") && ErrorEnter == 0) {
        if (numero * 1 > 0) {
            var datos = LlamarAjax("Laboratorio/BuscarIngresoCertificado", "ingreso=" + numero + "&tipo=" + tipocert).split("||");
            if (datos[0] == "1") {
                if (datos[1] * 1 == -1) {
                    $("#Ingreso").select();
                    $("#Ingreso").focus();
                    ErrorEnter = 1;
                    LimpiarTodo();
                    swal("Acción Cancelada", "El Ingreso número " + numero + " no está registrado", "warning");
                    return false;
                }
                if (datos[1] * 1 == 0) {
                    $("#Ingreso").select();
                    $("#Ingreso").focus();
                    ErrorEnter = 1;
                    LimpiarTodo();
                    swal("Acción Cancelada", "El Ingreso número " + numero + " no ha sido recibido en el laboratorio", "warning");
                    return false;
                }
            }
            if (datos[1] != "[]") {
                var data = JSON.parse(datos[1]);

                IdRemision = data[0].id * 1;
                Ingreso = data[0].ingreso * 1;
                Foto = data[0].fotos * 1;
                localStorage.setItem("Ingreso", Ingreso);

                $("#Equipo").val(data[0].equipo);
                $("#Marca").val(data[0].marca);
                $("#Modelo").val(data[0].modelo);
                $("#Serie").val(data[0].serie);
                $("#Observacion").val(data[0].observacion);
                $("#Magnitud").val(data[0].magnitud);
                $("#Remision").val(data[0].remision);
                $("#Rango").val(data[0].intervalo);
                PeriodoMantenimiento = data[0].periodo_mantenimiento * 1;

                $("#Sitio").val(data[0].sitio);
                $("#Garantia").val(data[0].garantia);
                $("#Accesorio").val(data[0].accesorio);

                $("#ObservRecep").val(data[0].observaesp);
                $("#Metodo").val((data[0].metodo ? data[0].metodo : ""));
                $("#Punto").val((data[0].punto ? data[0].punto : ""));
                $("#Servicio").val((data[0].servicio ? data[0].servicio : ""));
                $("#ObservCerti").val((data[0].observacot ? data[0].observacot : ""));
                $("#DireccionCe, #DireccionCerti").val(data[0].direccion);
                $("#NombreCe, #NombreCerti").val(data[0].nombreca);
                var cliente = data[0].cliente;
                var direccion = data[0].direccion2;

                $("#Usuario").val(data[0].usuarioing);
                $("#FechaIng").val(data[0].fechaing);
                $("#Tiempo").val(data[0].tiempo);

                AutoNombre.splice(0, AutoNombre.length);
                AutoDireccion.splice(0, AutoDireccion.length);

                AutoNombre.push(cliente);
                AutoDireccion.push(direccion);
                if (datos[3] != "[]") {
                    var data1 = JSON.parse(datos[3]);
                    for (var x = 0; x < data1.length; x++) {
                        AutoNombre.push(data1[x].nombrecer);
                    }
                }
                if (datos[4] != "[]") {
                    var data2 = JSON.parse(datos[4]);
                    for (var x = 0; x < data2.length; x++) {
                        AutoDireccion.push(data2[x].direccioncer);
                    }
                }

                if (tipocert == 1) {
                    $("#NumCertificado").html(datos[2]);
                    $("#NumCertificado").trigger("change");
                    $("#AcreCertificado").prop("readonly", true);
                } else {
                    $('#Certificado').html("");
                    $('#Certificado').select2({ tags: true });
                    $("#AcreCertificado").prop("readonly", false);
                }

                $("#Certificado").focus();
            }
        }
    } else
        ErrorEnter = 0;
}

function GuardarCertificado() {
    var certificado = $("#Certificado").val();
    var documento = $.trim($("#Documento").val());
    var tipodocumento = $("#TipoDocumento").val() * 1;
    var tipo = $("#Tipo").val() * 1;
    var numero = $.trim($("#NumCertificado").val());
    var observacion = $.trim($("#Observaciones").val());
    var fecha = $("#FechaIng").val();
    var proxima = $("#ProCertificado").val();
    var fechacer = $("#FecCertificado").val();
    var acreditado = $("#AcreCertificado").val();
    var direccion = $.trim($("#DireccionCerti").val());
    var nombre = $.trim($("#NombreCerti").val());
    if (Ingreso == 0)
        return false;
    var mensaje = "";

    if (documento == "") {
        $("#Documento").focus()
        mensaje = "<br> Debe de seleccionar un documento PDF " + mensaje;
    }
    if (certificado == "") {
        $("#Certificado").focus();
        mensaje = "<br> Debe de ingresar un número de " + TituloMensaje + " " + mensaje;
    }

    if (fechacer == "") {
        $("#FecCertificado").focus();
        mensaje = "<br> Debe de ingresar la fecha del " + TituloMensaje + " " + mensaje;
    }
    if (tipodocumento == 1) {
        if (acreditado == "") {
            $("#AcreCertificado").focus();
            mensaje = "<br> Debe de ingresar el tipo del " + TituloMensaje + " " + mensaje;
        }
        if (nombre == "") {
            $("#NombreCerti").focus();
            mensaje = "<br> Debe de ingresar a nombre de quien vá el " + TituloMensaje + " " + mensaje;
        }
        if (direccion == "") {
            $("#DireccionCerti").focus();
            mensaje = "<br> Debe de ingresar la dirección del " + TituloMensaje + " " + mensaje;
        }
    } else
        acreditado = 0;



    if (mensaje != "") {
        swal("Acción Cancelada", mensaje, "warning");
        return false;
    }

    var parametros = "ingreso=" + Ingreso + "&certificado=" + certificado + "&id=" + IdCertificado + "&observacion=" + observacion + "&fecha=" + fecha + "&numero=" + numero + "&acreditado=" + acreditado + "&proxima=" + proxima + "&fechacer=" + fechacer +
        "&tipodocumento=" + tipodocumento + "&nombre=" + nombre + "&direccion=" + direccion + "&tipo=" + tipo;
    var datos = LlamarAjax("Laboratorio/CertificadoIngreso", parametros).split("|");
    if (datos[0] == "0") {
        ConsultarCertificados(1);
        Certificado = certificado;
        IdCertificado = datos[2] * 1;
        $("#Documento").val("");
        $("#NumCertificado").focus();
        swal("", datos[1], "success");
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
}

function VerFotos() {
    if (Ingreso == 0)
        return false;
    CargarModalFoto(Ingreso, Foto, 1);
}

function CambioCertificado(numero) {

    if (Ingreso == 0)
        return false;

    Certificado = "";
    proxima = "";
    acreditado = "";
    nombreca = "";
    direccionca = "";
    fecha = "";
    IdCertificado = 0;
    $("#Observaciones").val("");
    $("#registrocertificado").html("");

    if (numero * 1 == 0)
        return false;

    var tipodocumento = $("#TipoDocumento").val() * 1;
    var tipo = $("#Tipo").val() * 1;
    var datos = LlamarAjax("Laboratorio/NumeroCertificado", "numero=" + numero + "&ingreso=" + Ingreso + "&tipodocumento=" + tipodocumento + "&tipo=" + tipo).split("||");

    if (datos[0] != "[]") {
        var data = JSON.parse(datos[0]);
        Certificado = data[0].numero;
        IdCertificado = data[0].id;
        proxima = data[0].proximacali;
        acreditado = data[0].acreditado;
        nombreca = data[0].nombrecer;
        direccionca = data[0].direccioncer;
        fecha = data[0].fechaexp;
        $("#Observaciones").val(data[0].observacion);
        $("#registrocertificado").html("<b>Registrado el día " + data[0].fecha + " por el técnico " + data[0].usuario + "</b>");
    } else {
        data = JSON.parse(datos[1]);
        if (tipo == 1) {
            if (data[0].recibidolab * 1 == 0) {
                ErrorEnter = 1;
                $("#Ingreso").select();
                $("#Ingreso").focus();
                swal("Acción Cancelada", "Este ingreso no ha sido recibido en el laboratorio", "warning");
                return false;
            }

            if (data[0].reportado * 1 == 0) {
                ErrorEnter = 1;
                $("#Ingreso").select();
                $("#Ingreso").focus();
                swal("Acción Cancelada", "Este ingreso no ha sido reportado por el técnico para certificar", "warning");
                return false;
            }

            if (data[0].calibracion * 1 == 2) {
                ErrorEnter = 1;
                $("#Ingreso").select();
                $("#Ingreso").focus();
                swal("Acción Cancelada", "Este ingreso no se le puede asignar un certificado porque es imposible calibrarlo según el tecnico", "warning");
                if (PermisioEspecial == 0) {
                    return false;
                }
            }

        } else {
            if (data[0].recibidocome * 1 == 0) {
                ErrorEnter = 1;
                $("#Ingreso").select();
                $("#Ingreso").focus();
                swal("Acción Cancelada", "Este ingreso no ha sido recibido en comercial para tercerizar", "warning");
                if (PermisioEspecial == 0) {
                    return false;
                }
            }
            if (data[0].recibidoterc * 1 == 0) {
                ErrorEnter = 1;
                $("#Ingreso").select();
                $("#Ingreso").focus();
                swal("Acción Cancelada", "Debe de realizar la salida del ingreso para poder subir el certificado", "warning");
                if (PermisioEspecial == 0) {
                    return false;
                }
            }
            if (data[0].salidaterce * 1 == 0) {
                ErrorEnter = 1;
                $("#Ingreso").select();
                $("#Ingreso").focus();
                swal("Acción Cancelada", "Debe de recibir la salida del ingreso para poder subir el certificado", "warning");
                if (PermisioEspecial == 0) {
                    return false;
                }
            }
        }

    }
    if (tipo == 1)
        $("#Certificado").val(Certificado);
    else {
        $("#Certificado").html("<option value='" + Certificado + "'>" + Certificado + "</option>");
    }
    $("#ProCertificado").val(proxima);
    $("#FecCertificado").val(fecha);
    $("#DireccionCerti").val(direccionca);
    $("#NombreCerti").val(nombreca);
    $("#AcreCertificado").val(acreditado).trigger("change");

    if (datos[0] == "[]")
        TipoAcreditacion(numero);

}

function VerCertificado() {
    var numero = $.trim($("#NumCertificado").val());
    var tipodocumento = $("#TipoDocumento").val() * 1;
    if (Certificado != "") {
        if (tipodocumento == 1)
            window.open(url + "Certificados/" + numero + "-" + $.trim(Certificado).replace('/', '-') + ".pdf");
        else
            window.open(url + "InformesTercerizados/" + numero + "-" + Certificado + ".pdf");
    }
}

function EliminarCertificado() {
    if (IdCertificado > 0) {
        var tipodocumento = $("#TipoDocumento").val() * 1;
        var tipo = $("#Tipo").val() * 1;
        var numero = $("#NumCertificado").val() * 1;
        swal.queue([{
            title: ValidarTraduccion('Advertencia'),
            text: ValidarTraduccion('¿Seguro que desea eliminar el ' + TituloMensaje + ' número ' + numero + ' del ingreso ' + Ingreso + ' ? '),
            type: 'question',
            showLoaderOnConfirm: true,
            showCancelButton: true,
            confirmButtonText: ValidarTraduccion('Si'),
            cancelButtonText: ValidarTraduccion('No'),
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            preConfirm: function () {
                return new Promise(function (resolve, reject) {
                    $.post("Laboratorio/EliminarCertificado", "id=" + IdCertificado + "&ingreso=" + Ingreso + "&numero=" + numero + "&tipodocumento=" + tipodocumento + "&tipo=" + tipo)
                        .done(function (data) {
                            datos = data.split("|");
                            if (datos[0] == "0") {
                                IdCertificado = 0;
                                $("#Observaciones").val("");
                                $("#registrocertificado").html("");
                                $("#Certificado").val("");
                                $("#ProCertificado").val("");
                                $("#FecCertificado").val("");
                                $("#AcreCertificado").val("").trigger("change");
                                $("#NombreCerti").val("");
                                $("#DireccionCerti").val("");
                                ConsultarCertificados(1);
                                resolve()
                            } else {
                                reject(datos[1]);
                            }
                        })
                })
            }
        }]);
    }
}

$("#DireccionCerti").autocomplete({ source: AutoDireccion, minLength: 0 }).focus(function () {
    $(this).data("uiAutocomplete").search($(this).val());
});

$("#NombreCerti").autocomplete({ source: AutoNombre, minLength: 0 }).focus(function () {
    $(this).data("uiAutocomplete").search($(this).val());
});

function TipoAcreditacion(plantilla) {
    $("#AcreCertificado").val("").trigger("change");
    var tipodocumento = $("#Tipo").val() * 1;
    plantilla = plantilla * 1;
    if (tipodocumento == 2)
        $("#AcreCertificado").val("1").trigger("change");
    if (plantilla == 0 || tipodocumento == 2) {
        return false;
    }
    var data = JSON.parse(LlamarAjax("Laboratorio/TipoAcreditacion", "plantilla=" + plantilla));
    $("#AcreCertificado").val(data[0].acreditado).trigger("change");
    
}

function ProximaCalibracion(fecha) {
    $("#ProCertificado").val("");
    var tipodocumento = $("#Tipo").val() * 1;
    if (TipoPlantilla == 1 || fecha == "" || tipodocumento == 2 || PeriodoMantenimiento == 0)
        return false;
    var a_fecha = fecha.split("-");
    var anio = a_fecha[0] * 1;
    var mes = (a_fecha[1] * 1);
    var dia = a_fecha[2] * 1;
    var proxima = new Date(anio + "/" + mes + "/" + dia);
    proxima.setDate(proxima.getDate() + (PeriodoMantenimiento * 30));
    fecha = proxima.getFullYear() + "-" + ((proxima.getMonth() + 1) * 1 < 10 ? "0" : "") + (proxima.getMonth() + 1) + "-" + (proxima.getDate() * 1 < 10 ? "0" : "") + proxima.getDate();
    $("#ProCertificado").val(fecha);
}

$('select').select2();