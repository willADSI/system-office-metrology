﻿
$("#Ser_IVA, #OSer_IVA").html(CargarCombo(35, 1));
$("#Pla_Magnitudes, #Pla_Magnitud").html(CargarCombo(1, 0));
$("#Ser_Estado, #OSer_Estado, #Met_Estado").html(CargarCombo(37, 1));
$("#Met_Equipos, #Pla_Equipos").html(CargarCombo(58, 0));
$("#OSer_CuentaContable, #Ser_CuentaContable").html(CargarCombo(83, 1));

tb = $('input');
$(tb).keypress(enter2tab);

tb = $('select');
$(tb).keypress(enter2tab);

function enter2tab(e) {
    if (e.keyCode == 13) {
        id = $(this).attr("id");

        cb = parseInt($(this).attr('tabindex'));

        if (id == "Documento") {
            if ($.trim($(this).val()) != "") {

                BuscarCliente($(this).val());
                $("#Sede").focus();
                return false;
            }
        }

        if ($(':input[tabindex=\'' + (cb + 1) + '\']') != null) {
            $(':input[tabindex=\'' + (cb + 1) + '\']').focus();
            $(':input[tabindex=\'' + (cb + 1) + '\']').select();
            e.preventDefault();

            return false;
        }

        if ($(':select[tabindex=\'' + (cb + 1) + '\']') != null) {
            console.log("1");
            $(':select[tabindex=\'' + (cb + 1) + '\']').focus();
            $(':select[tabindex=\'' + (cb + 1) + '\']').select();
            e.preventDefault();

            return false;
        }
    }
}

//****************SERVICIOS*****************************

function TablaServicios() {
    var datos = LlamarAjax('Configuracion','opcion=TablaConfiguracion&tipo=7&id=0');
    var datasedjson = JSON.parse(datos);
    $('#TablaServicios').DataTable({
        data: datasedjson,
        bProcessing: true,
        bDestroy: true,
        columns: [
            { "data": "id" },
            { "data": "codigo" },
            { "data": "nombre" },
            { "data": "acreditado" },
            { "data": "diaentrega" },
            { "data": "proveedor" },
            { "data": "sitio" },
            { "data": "express" },
            { "data": "otro" },
            { "data": "descuento" },
            { "data": "iva" },
            { "data": "cuenta" },
            { "data": "estado" },
            { "data": "eliminar" }
        ],
        "language": {
            "url": LenguajeDataTable
        }
    });
}

function LimpiarServicios() {
    $("#Ser_Id").val("0");
    $("#Ser_Codigo").val("0");
    $("#Ser_Descripcion").val("")
    $("#Ser_Dias").val("");
    $("#Ser_Acreditado").val("").trigger("change");
    $("#Ser_Proveedor").val("").trigger("change");
    $("#Ser_Sitio").val("").trigger("change");
    $("#Ser_Express").val("").trigger("change");
    $("#Ser_IVA").val("").trigger("change");
    $("#Ser_Otro").val("0").trigger("change");
    $("#Ser_Estado").val("1").trigger("change");
    $("#Ser_Descuento").val("").trigger("change");
    $("#Ser_CuentaContable").val("").trigger("change");
}

$("#TablaServicios > tbody").on("dblclick", "tr", function () {
    var row = $(this).parents("td").context.cells;
    var numero = row[0].innerText;
    var datos = LlamarAjax('Configuracion','opcion=TablaConfiguracion&tipo=7&id=' + numero);
    var data = JSON.parse(datos);
    $("#Ser_Id").val(data[0].id);
    $("#Ser_Codigo").val(data[0].codigo);
    $("#Ser_Descripcion").val(data[0].nombre)
    $("#Ser_Acreditado").val(data[0].acreditadoser).trigger("change");
    $("#Ser_Proveedor").val(data[0].proveedor).trigger("change");
    $("#Ser_Sitio").val(data[0].sitio).trigger("change");
    $("#Ser_Express").val(data[0].express).trigger("change");
    $("#Ser_Dias").val(data[0].diaentrega);
    $("#Ser_IVA").val(data[0].idiva).trigger("change");
    $("#Ser_Otro").val(data[0].otro).trigger("change");
    $("#Ser_Estado").val(data[0].estado).trigger("change");
    $("#Ser_Descuento").val(data[0].descuento).trigger("change");
    $("#Ser_CuentaContable").val(data[0].idcuecontable > 0 ? data[0].idcuecontable : "").trigger("change");
    $("#Ser_Descripcion").focus();
});


$("#formServicios").submit(function (e) {
    e.preventDefault();
    var parametros = $(this).serialize();
    var datos = LlamarAjax("Configuracion","opcion=GuardarServicio&" + parametros).split("|");
    if (datos[0] == "0") {
        TablaServicios();
        LimpiarServicios();
        swal("", datos[1], "success");
        $("#Met_Magnitudes").html(CargarCombo(1, 0));
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }

});


function EliminarServicio(id, servicio) {
    var accion = 1;
    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: ValidarTraduccion('¿Desea eliminar el servicio ' + servicio + '?'),
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Eliminar'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        allowOutsideClick: false,
        
        preConfirm: function (result) {
            return new Promise(function (resolve, reject) {
                $.post(url_servidor + "Configuracion", "opcion=EliminarServicio&Id=" + id)
                    .done(function (data) {
                        datos = data.split("|");
                        if (datos[0] == "0")
                            resolve()
                        else {
                            reject(datos[1]);
                        }
                            
                    })
            })
        }
    }]).then(function (data) {
        TablaServicios();
        $("#Met_Magnitudes").html(CargarCombo(1, 0));
        LimpiarServicios();
    });
}

//************ OTROS SERVICIOS ***********************************

function TablaOtrosServicios() {
    var datos = LlamarAjax('Configuracion','opcion=TablaConfiguracion&tipo=8&id=0');
    var datasedjson = JSON.parse(datos);
    $('#TablaOtroServicios').DataTable({
        data: datasedjson,
        bProcessing: true,
        bDestroy: true,
        columns: [
            { "data": "id" },
            { "data": "inicial" },
            { "data": "descripcion" },
            { "data": "porcentaje" },
            { "data": "precio" },
            { "data": "iva" },
            { "data": "estado" },
            { "data": "cuenta" },
            { "data": "eliminar" }
        ],
        "language": {
            "url": LenguajeDataTable
        }
    });
}

function LimpiarOtroServicios() {
    $("#OSer_Id").val("0");
    $("#OSer_Iniciales").val("");
    $("#OSer_Descripcion").val("")
    $("#OSer_Descuento").val("");
    $("#OSer_Precio").val("");
    $("#OSer_IVA").val("").trigger("change");
    $("#OSer_Estado").val("1").trigger("change");
    $("#OSer_CuentaContable").val("").trigger("change");
}

$("#TablaOtroServicios > tbody").on("dblclick", "tr", function () {
    var row = $(this).parents("td").context.cells;
    var numero = row[0].innerText;
    var datos = LlamarAjax('Configuracion','opcion=TablaConfiguracion&tipo=8&id=' + numero);
    var data = JSON.parse(datos);
    $("#OSer_Id").val(data[0].id);
    $("#OSer_Iniciales").val(data[0].inicial);
    $("#OSer_Descripcion").val(data[0].descripcion)
    $("#OSer_Descuento").val(data[0].porcentaje).trigger("change");
    $("#OSer_Precio").val(data[0].precio).trigger("change");
    $("#OSer_IVA").val(data[0].idiva).trigger("change");
    $("#OSer_Estado").val(data[0].estado).trigger("change");
    $("#OSer_CuentaContable").val(data[0].idcuecontable > 0 ? data[0].idcuecontable : "").trigger("change");
    $("#OSer_Descripcion").focus();
});


$("#formOtroServicios").submit(function (e) {
    e.preventDefault();
    var parametros = $(this).serialize();
    var datos = LlamarAjax("Configuracion","opcion=GuardarOtroServicio&" + parametros).split("|");
    if (datos[0] == "0") {
        TablaOtrosServicios();
        LimpiarOtroServicios();
        swal("", datos[1], "success");
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }

});


function EliminarOtroServicio(id, servicio) {
    var accion = 1;
    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: ValidarTraduccion('¿Desea eliminar el servicio ' + servicio + '?'),
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Eliminar'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        allowOutsideClick: false,

        preConfirm: function (result) {
            return new Promise(function (resolve, reject) {
                $.post(url_servidor + "Configuracion","opcion=EliminarOtroServicio&Id=" + id)
                    .done(function (data) {
                        datos = data.split("|");
                        if (datos[0] == "0")
                            resolve()
                        else {
                            reject(datos[1]);
                        }

                    })
            })
        }
    }]).then(function (data) {
        TablaOtrosServicios();
        LimpiarOtroServicios();
    });
}
//*******************************METODO****************************************
function TablaMetodos() {
    var datos = LlamarAjax('Configuracion','opcion=TablaConfiguracion&tipo=9&id=0');
    var datasedjson = JSON.parse(datos);
    $('#TablaMetodos').DataTable({
        data: datasedjson,
        bProcessing: true,
        bDestroy: true,
        columns: [
            { "data": "id" },
            { "data": "descripcion" },
            { "data": "equipos" },
            { "data": "estado" },
            { "data": "eliminar" }
        ],
        "language": {
            "url": LenguajeDataTable
        }
    });
}

function LimpiarMetodos() {
    $("#Met_Id").val("0");
    $("#Met_Descripcion").val("")
    $("#Met_Equipos").val("").trigger("change");
    $("#Met_Estado").val("1").trigger("change");
}

$("#TablaMetodos > tbody").on("dblclick", "tr", function () {
    var row = $(this).parents("td").context.cells;
    var numero = row[0].innerText;
    var datos = LlamarAjax('Configuracion','opcion=TablaConfiguracion&tipo=9&id=' + numero);
    var data = JSON.parse(datos);
    $("#Met_Id").val(data[0].id);
    $("#Met_Descripcion").val(data[0].descripcion);

    var equipos = data[0].equipos.split(",");
    for (var x = 0; x < equipos.length; x++) {
        $("#Met_Equipos option[value=" + equipos[x] + "]").prop("selected", true);
    }
    $("#Met_Equipos").trigger("change");
    $("#Met_Estado").val(data[0].estado).trigger("change");
    $("#Met_Descripcion").focus();
});


$("#formMetodos").submit(function (e) {
    e.preventDefault();
    var parametros = $(this).serialize();
    var datos = LlamarAjax("Configuracion","opcion=GuardarMetodo&" + parametros).split("|");
    if (datos[0] == "0") {
        TablaMetodos();
        LimpiarMetodos();
        swal("", datos[1], "success");
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
});

function EliminarMetodo(id, metodo) {
    var accion = 1;
    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: ValidarTraduccion('¿Desea eliminar el metodo ' + metodo + '?'),
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Eliminar'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        allowOutsideClick: false,

        preConfirm: function (result) {
            return new Promise(function (resolve, reject) {
                $.post(url_servidor + "Configuracion","opcion=EliminarMetodo&Id=" + id + "&IdReemplazo=0")
                    .done(function (data) {
                        datos = data.split("|");
                        if (datos[0] == "0")
                            resolve()
                        else {
                            accion = 0;
                            resolve();
                        }

                    })
            })
        }
    }]).then(function (data) {
        if (accion == 1) {
            TablaMetodos();
            LimpiarMetodos();
        } else {
            $("#TituloReemplazar").html("Reemplazar Método");
            $("#TextoReemplazo").html("Reemplazar método " + metodo);
            $("#OpcionReemplazo").html(CargarCombo(59,1));
            $("#NumeroReemplazo").val(3);
            $("#IdReemplazo").val(id);
            $("#modalReemplazoServicio").modal("show");
        }
    });
}




//*******************************Plantillas****************************************
function TablaPlantillas() {
    var datos = LlamarAjax('Configuracion','opcion=TablaConfiguracion&tipo=10&id=0');
    var datasedjson = JSON.parse(datos);
    $('#TablaPlantillas').DataTable({
        data: datasedjson,
        bProcessing: true,
        bDestroy: true,
        columns: [
            { "data": "id" },
            { "data": "codigo" },
            { "data": "descripcion" },
            { "data": "acreditada" },
            { "data": "equipos" },
            { "data": "estado" },
            { "data": "eliminar" }
        ],
        "language": {
            "url": LenguajeDataTable
        }
    });
}

function LimpiarPlantillas() {
    $("#Pla_Id").val("0");
    $("#Pla_Descripcion").val("")
    $("#Pla_Codigo").val("")
    $("#Pla_Equipos").val("").trigger("change");
    $("#Pla_Estado").val("1").trigger("change");
    $("#Pla_Acreditado").val("").trigger("change");
}

$("#TablaPlantillas > tbody").on("dblclick", "tr", function () {
    var row = $(this).parents("td").context.cells;
    var numero = row[0].innerText;
    var datos = LlamarAjax('Configuracion','opcion=TablaConfiguracion&tipo=10&id=' + numero);
    var data = JSON.parse(datos);
    $("#Pla_Id").val(data[0].id);
    $("#Pla_Descripcion").val(data[0].descripcion);
    $("#Pla_Codigo").val(data[0].codigo);
    $("#Pla_Acreditado").val(data[0].acreditado).trigger("change");

    var equipos = data[0].equipos.split(",");
    for (var x = 0; x < equipos.length; x++) {
        $("#Pla_Equipos option[value=" + equipos[x] + "]").prop("selected", true);
    }
    $("#Pla_Equipos").trigger("change");
    $("#Pla_Estado").val(data[0].estado).trigger("change");
    $("#Pla_Descripcion").focus();
});


$("#formPlantillas").submit(function (e) {
    e.preventDefault();
    var parametros = $(this).serialize();
    var datos = LlamarAjax("Configuracion","opcion=GuardarPlantillas&" + parametros).split("|");
    if (datos[0] == "0") {
        TablaPlantillas();
        LimpiarPlantillas();
        swal("", datos[1], "success");
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
});

function GuardarDocumento(id) {
    var documento = document.getElementById(id);
    if ($.trim(documento.value) != "") {
        var file = documento.files[0];
        var data = new FormData();
        data.append('documento', file);
        var resultado = '';
        var direccion = url + "Configuracion/AdjuntarExcel";
        $.ajax({
            url: direccion,
            type: 'POST',
            contentType: false,
            data: data,
            processData: false,
            async: false,
            success: function (datos) {
                datos = datos.split("|");
                if (datos[0] != "0") {
                    swal(ValidarTraduccion("Error al cargar el archivo"), "", "error");
                } else {
                    if (datos[2] != "XX")
                        swal(ValidarTraduccion(datos[2]), "", "error");
                }
            }
        });
    }
}

function DescargarExcel(id, tipo) {
    window.open(url + "PlantillasCertificados/" + id + tipo);
}


function EliminarPlantilla(id, plantilla) {
    var accion = 1;
    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: ValidarTraduccion('¿Desea eliminar la plantilla ' + plantilla + '?'),
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Eliminar'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        allowOutsideClick: false,

        preConfirm: function (result) {
            return new Promise(function (resolve, reject) {
                $.post(url_servidor + "Configuracion","opcion=EliminarPlantillas&Id=" + id + "&IdReemplazo=0")
                    .done(function (data) {
                        datos = data.split("|");
                        if (datos[0] == "0")
                            resolve()
                        else {
                            accion = 0;
                            resolve();
                        }

                    })
            })
        }
    }]).then(function (data) {
        if (accion == 1) {
            TablaPlantillas();
            LimpiarPlantillas();
        } else {
            $("#TituloReemplazar").html("Reemplazar Plantilla");
            $("#TextoReemplazo").html("Reemplazar plantilla " + metodo);
            $("#OpcionReemplazo").html(CargarCombo(59, 1));
            $("#NumeroReemplazo").val(4);
            $("#IdReemplazo").val(id);
            $("#modalReemplazoServicio").modal("show");
        }
    });
}


function ReemplazarServicio() {
    var numero = $("#NumeroReemplazo").val() * 1;
    var idreemplazo = $("#OpcionReemplazo").val() * 1;
    var id = $("#IdReemplazo").val() * 1;
    switch (numero) {
        case 3:
            datos = LlamarAjax("Configuracion","opcion=EliminarMetodo&Id=" + id + "&IdReemplazo=" + idreemplazo).split("|");
            if (datos[0] == "0") {
                TablaMetodos();
                LimpiarMetodos();
                swal("", datos[1], "success");
            } else {
                swal("Acción Cancelada", datos[1], "warning");
            }
            break;
        case 4:
            datos = LlamarAjax("Configuracion","opcion=EliminarPlantillas&Id=" + id + "&IdReemplazo=" + idreemplazo).split("|");
            if (datos[0] == "0") {
                TablaPlantillas();
                LimpiarPlantillas();
                swal("", datos[1], "success");
            } else {
                swal("Acción Cancelada", datos[1], "warning");
            }
            break;
    }
    $("#modalReemplazoServicio").modal("hide");

}

TablaServicios();
TablaOtrosServicios();
TablaMetodos();
TablaPlantillas()

$('select').select2();
DesactivarLoad();
