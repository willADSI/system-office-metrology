﻿var d = new Date();
var month = d.getMonth() + 1;
var day = d.getDate();
var TipoPersona = 1;
var ComboCliente = CargarCombo(9, 1);
var ComboProveedor = CargarCombo(7, 1);

var Anio = $.trim(d.getFullYear());
Anio = $.trim(Anio.substr(2, 2))
var IdVuelta = 0;



var Ingreso = 0;
var IdRemision = 0;
var Fotos = 0;
var Error = 0;
var url = $("#urlprincipal").val();

$("#Cliente").html(CargarCombo(9, 1));

function LimpiarTodo() {
    $("#Cliente").val("").trigger("change");
}

function CambiarFotoRecoger(numero) {
    $("#FotoIngreso").attr("src", url + "imagenes/FotoRecogida/" + TipoPersona + "/" + IdVuelta + "/" + numero + ".jpg");
}

function SeleccionarFilaRD(x,vuelta) {
    $(".Seleccionado").removeClass("bg-gris");
    if ($("#seleccionado" + x).prop("checked")) {
        $("#FilaRD-" + x).addClass("bg-gris");
        IdVuelta = vuelta;
        var fotos = LlamarAjax("Cotizacion/BuscarFotosVueltas", "vuelta=" + IdVuelta) * 1;
        CargarFotosRecoger(fotos)
    }
}

function BuscarVueltasPendientes() {
    var cliente = $("#Cliente").val() * 1;
    IdVuelta = 0;
    if (cliente == 0) {
        $("#TablaVueltas").html("");
        return false;
    }
    var pendiente = 0;
    if ($("#Pendientes").prop("checked"))
        pendiente = 1;
    Fotos = 0;
    var Resultado = "";
    if (cliente * 1 == 0) {
        contenedor = "";
        $("#AlbunFotosRecoger").html(contenedor);
    } else {
        var datos = LlamarAjax("Cotizacion/BuscarVueltasPendientes", "cliente=" + cliente + "&tipo=" + TipoPersona + "&pendiente=" + pendiente);

        if (datos != "[]") {
            var data = JSON.parse(datos);
            for (var x = 0; x < data.length; x++) {
                Resultado += "<tr id='FilaRD-" + x + "' class='Seleccionado'>" +
                    "<td><button class='btn btn-success' title='Ejecutar Vuelta' type='button' onclick='EjecutarVuelta(" + data[x].id + ")'><span data-icon='&#xe147;'></span></button></td>" +
                    "<td class='text-center'><input class='form-control' type='radio' value='" + data[x].id + "' name='seleccion[]' id='seleccionado" + x + "' onchange='SeleccionarFilaRD(" + x + "," + data[x].id + ")'></td>" +
                    "<td>" + data[x].fechaapro + "</td>" +
                    "<td>" + data[x].estado + "</td>" +
                    "<td>" + data[x].actividad + "</td></tr>";
            }
        }
        
    }

    $("#TablaVueltas").html(Resultado);
    
}

function CargarComboTipo() {
        
    if ($("#Tipo1").prop("checked")) {
        TipoPersona = 1;
        $("#Cliente").html(ComboCliente);
        $("#TipoPersona").html("Clientes");
    }
    else {
        TipoPersona = 2;
        $("#Cliente").html(ComboProveedor);
        $("#TipoPersona").html("Proveedores");
    }
    $("#TablaVueltas").html("");
    $("#AlbunFotosRecoger").html("");
}

function CargarFotosRecoger(fotos) {
    Fotos = fotos;
    var fecha = d.getFullYear() + '-' +
        (month < 10 ? '0' : '') + month + '-' +
        (day < 10 ? '0' : '') + day;
    var contenedor = "";
    contenedor = "<div class='row'><div class='col-xs-12 col-sm-12'>" +
        "<img id='FotoIngreso' " + (Fotos > 0 ? "src='" + url + "imagenes/FotoRecogida/" + TipoPersona + "/" + IdVuelta + "/1.jpg'" : "") + " width='90%'></div><div class='col-xs-12 col-sm-1'>";
    for (var x = 1; x <= Fotos; x++) {
        contenedor += "<button class='btn btn-primary' type='button' onclick=\"CambiarFotoRecoger(" + x + ")\">" + x + "</button>&nbsp;";
    }
    contenedor += "</div></div>";
    $("#AlbunFotosRecoger").html(contenedor);
}

Webcam.set({
    width: 290,
    height: 320,
    dest_width: 620,
    dest_height: 750,
    image_format: 'png',
    jpeg_quality: 100
});
Webcam.attach('#my_cameraing');

function TomarFoto() {

    var cliente = $("#Cliente").val() * 1;

    if (cliente > 0) {
        Webcam.snap(function (data_uri) {
            //var raw_image_data = data_uri.replace(/^data\:image\/\w+\;base64\,/, '');
            GuardarFoto(cliente, data_uri);
        });
    }

}

function GuardarFoto(cliente, archivo) {

    var fecha = d.getFullYear() + '-' +
        (month < 10 ? '0' : '') + month + '-' +
        (day < 10 ? '0' : '') + day;

    if (IdVuelta == 0) {
        swal("Acción Cancelada", "Debe de seleccionar una vuelta", "warning");
        return false;
    }
    
    ActivarLoad();
    setTimeout(function () {
        $.ajax({
            type: 'POST',
            url: url + "Cotizacion/GuardarFotoRecoger",
            data: "archivo=" + archivo + "&tipo=" + TipoPersona + "&vuelta=" + IdVuelta,
            async: true,
            success: function (data) {
                data = data.split("|");
                if (data[0] == "0") {
                    Fotos = data[1] * 1;
                    CargarFotosRecoger(Fotos);
                    CambiarFotoRecoger(Fotos);
                } else {
                    swal("Acción Cancelada", data[1], "warning");
                }
            }
        });
        DesactivarLoad();
    }, 15);
}

function EjecutarVuelta(id) {
    var cliente = $("#Cliente option:selected").text();
    var datos = LlamarAjax("Configuracion/DescripcionVuelta", "id=" + id).split("||");
    var vuelta = datos[0];
    var observacion = datos[1];

    $("#IdEjecutar").val(id);
    $("#EjeVuelta").val("").trigger("change");
    $("#DVuelta").val(vuelta);
    $("#DCliente").val(cliente);
    $("#OVuelta").val(observacion);

    $("#modalEjecutar").modal("show");

}

function GuaEjecVueltaFoto() {
    var id = $("#IdEjecutar").val() * 1;
    var ejecutada = $("#EjeVuelta").val();
    var observacion = $.trim($("#OVuelta").val());

    if (ejecutada == "") {
        $("#EjeVuelta").focus();
        swal("Acción Cancelada", "Debe seleccionar si la vuelta fue ejecutada", "warning");
        return false;
    } else {
        if (ejecutada == "NO" && observacion == "") {
            $("#OVuelta").focus();
            swal("Acción Cancelada", "Si la vuelta no fue ejecutada debe de ingresar una observación", "warning");
            return false;
        }
        if (ejecutada == "RECHAZADA" && observacion == "") {
            $("#OVuelta").focus();
            swal("Acción Cancelada", "Si la vuelta se rechaza debe de ingresar una observación", "warning");
            return false;
        }
    }

    var datos = LlamarAjax("Configuracion/OperacionVuelta", "id=" + id + "&operacion=" + 4 + "&fecha=2019-01-01 00:00&ejecutado=" + ejecutada + "&observacion=" + observacion).split("|");
    if (datos[0] == "0") {
        BuscarVueltasPendientes();
        $("#AlbunFotosRecoger").html("");
        $("#modalEjecutar").modal("hide");
        swal("", datos[1], "success");
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
}



$('select').select2();
DesactivarLoad();
