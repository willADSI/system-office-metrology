﻿var d = new Date();
month = d.getMonth() + 1;
day = d.getDate();

var HoraInicio = "";
var HoraCondicion = "";
var HoraFinal = "";
var CerImpreso = 0;
var ErrorHume = 0;
var ErrorTemp = 0;
var Calculado = 0;
var Sitio = "";
var FirmaCertificado = "";
var CoefSec = 0;

var Solicitante = "";
var Direccion = "";
var IdTomaDatos = "";
var SegundosToma = 0;
var HoraToma = "";
var IntervalContador = null;
var IntervaloTiempo = null;
var AlcanceDesde = 0;
var AlcanceHasta = 0;
var IdVersion = 8;
var TipoClase = "";

var RangoDesde = 0;
var RangoHasta = 0;
var RangoEquipo = 0;
var Alcance = 0;
var TipoLectura = "";
var TipoCalibracion = "";
var SensorTemperatura = 0;

var canvafirma = document.getElementById("padfirmar");
initPad(canvafirma);

var ctxgra = document.getElementById('myChart1').getContext('2d');
var gratemp = document.getElementById('GraTemperatura').getContext('2d');
var grahume = document.getElementById('GraHumedad').getContext('2d');
var ctdesviacion = document.getElementById('myCharDesviacion').getContext('2d');
var ctbarra = document.getElementById('myCharBarra').getContext('2d');

var datastuden = JSON.parse(LlamarAjax("Laboratorio/TStuden", ""));

var PermisioEspecial = PerGeneSistema("PERMISO ADMINISTRADOR LABORATORIO");


$("#NumCertificado").html(CargarCombo(72, 0, "", "'DID-159 (POSITIVA)','DID-159 (NEGATIVO)'"));

var COLORS = [
    '#4dc9f6',
    '#f67019',
    '#f53794',
    '#537bc4',
    '#acc236',
    '#166a8f',
    '#00a950',
    '#58595b',
    '#8549ba'
];

$("#RangoPatron").html(CargarCombo(20, 1, "", "2"));
$("#Sensor").html(CargarCombo(74, 0, "", "2"));

function CambioSensor(sensor) {
    SensorTemperatura = sensor;
}

$("#Sensor").trigger("change");

DatosTemperatura(6, SensorTemperatura);

DesactivarLoad();



var Aniofull = $.trim(d.getFullYear());
var Anio = $.trim(Aniofull.substr(2, 2))
var AprobarCotiza = "";

output = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-01';

var FechaCal = d.getFullYear() + "-" + (month < 10 ? '0' : '') + month + "-" + (day < 10 ? "0" : "") + day;
var FechaActual = d.getFullYear() + "-" + (month < 10 ? '0' : '') + month + "-" + (day < 10 ? "0" : "") + day;

var Ingreso = 0;
var IdReporte = 0;
var IdInstrumento = 0;
var ErrorEnter = 0;
var Foto = 0;
var NroReporte = 1;
var RangoHasta = 0;
var GuardaTemp = 0;
var CorreoEmpresa = "";
var AplicarTemporal = 0;
var InfoTecIngreso = 0;
var Factor = 0;
var FactorSI = 0;
var TipoInst = "";
var Descripcion = "";
var Resolucion = 0;
var CantRegitro = 3;
var DesPerm1 = 0;
var DesPerm2 = 0;
var Certificado = "";
var IdCalibracion = 0;
var NumCertificado = 21;
var Revision = 1;
var Foto = Foto;
var Clase = 0;
var AlturaPatron = 0;
var AlturaIBC = 0;
var UEstandarPSI = 0;
var CorAlturaPatron = 0;
var CorAlturaIBC = 0;
var CorAltU1 = 0;
var MayorAltura = 0;
var MenorAltura = 0;
var PUPatron = 0;
var TopeCero = "";
var DesMax = 0;
var DesMin = 0;

$("#NumCertificado").val("21").trigger("change");

var TempMax = 0;
var TempMin = 100000000;
var PresMax = 0;
var HumeMax = 0;
var HumeMin = 100000000;
var PresMin = 0;
var TempVar = 0;
var HumeVar = 0;
var PresVar = 0;

var UMetodo = 0.014953834;
var IncMetodo = 0;

$("#UMetodo").html(UMetodo);


function FormatoSalida2(Caja) {
    FormatoSalida(Caja, 3, NumCertificado);
}

function CambioCertificado(tipo) {
    NumCertificado = tipo;
    LimpiarTodo();
    if (tipo*1 == 2)
        $(".negativo").html("<h3>VALORES EN NEGATIVO CUANDO LA CALIBRACION ES EN PRESIÓN NEGATIVA</h3>");
    else
        $(".negativo").html("");
}

localStorage.setItem("Ingreso", "0");

function LimpiarTodo() {
    
    localStorage.removeItem(NumCertificado + "DID159" + Ingreso);
    localStorage.removeItem(NumCertificado + "DIDDATOS" + Ingreso);
    LimpiarDatos();
    $("#Ingreso").val("");
}

function LimpiarDatos() {

    FechaCal = d.getFullYear() + "-" + (month < 10 ? '0' : '') + month + "-" + (day < 10 ? "0" : "") + day;
    FechaActual = d.getFullYear() + "-" + (month < 10 ? '0' : '') + month + "-" + (day < 10 ? "0" : "") + day;

    GuardaTemp = 0;
    $(".serie3").addClass("hidden");
    IntervaloTiempo = null;
    IntervalContador = null;
    IdTomaDatos = "";
    IdCalibracion = 0;
    AplicarTemporal = 0;
    Ingreso = 0;
    Foto = 0
    HoraInicio = "";
    HoraCondicion = "";
    HoraFinal = "";
    CerImpreso = 0;
    ErrorHume = 0;
    ErrorTemp = 0;
    Revision = 1;
    Certificado = "";
    Clase = 0;
    Resolucion = 0;
    FactorSI = 0;
    Calculado = 0;

    AlturaPatron = 0;
    AlturaIBC = 0;
    CorAlturaPatron = 0;
    CorAlturaIBC = 0;
    MayorAltura = 0;
    MenorAltura = 0;
    
    TempMax = 0;
    TempMin = 100000000;
    HumeMax = 0;
    HumeMin = 100000000;
    TempVar = 0;
    HumeVar = 0;
    PresMax = 0;
    PresMin = 0;
    PresVar = 0;
    
    InfoTecIngreso = 0;
    $("#Solicitante").html("");
    $("#Clase").html("");
    $("#Certificado").html("");
    $("#Direccion").html("");
    $("#FechaIng").html("");
    $("#FechaCal").html("");
    $("#FechaExp").html("");
    $("#RangoMaximo").html("")
    $("#TIngreso").html("");
    $("#Medida").html("");
    $("#ValorCon").html("");
    $("#UnidadCon").html("");
    $("#UnidadCon").html("");
    $("#ProximaCali").val("");
    $("#Observacion").val("La calibración se realizó en las instalaciones permanentes de Calibration Service S.A.S.");

    $("#Sitio").html("")
    $(".medida").html("");
    $("#CertificadoAnulado").html("");
    $("#TiempoCali").val("0").trigger("change");
    $("#firmacertificado").html("");
    $("#ButCertFirMan").addClass("hidden");

    $("#Equipo").html("");
    $("#Marca").html("");
    $("#Modelo").html("");
    $("#Serie").html("");
    $("#Remision").val("");
    $("#Usuario").val("");
    $("#FechaRec").val("");
    $("#Tiempo").val("");

    $("#CMetodo").val("");
    $("#CPunto").val("");
    $("#CServicio").val("");
    $("#CObservCerti").val("");
    $("#CProxima").val("");
    $("#CEntrega").val("");
    $("#CAsesorComer").val("");

    $("#RangoHasta").prop("disabled", false);
    $("#RangoHasta").val("");

    $("#MedResolucion").html("");

    $("medida").html("");



    $("#Descripcion").html("");
    $("#UnidadConSI").html("");
    $("#Resolucion").html("");
    $("#Fluido").html("");
    $("#CoeSec").val("");
    $("#DivEscala").html("");
    $("#TopeCero").html("");
    $("#Gliserina").html("");

    $("#ValorConSI").html("");
    $("#Clase").html("");

    $("#AlturaPatron").html("");
    $("#AlturaIBC").html("");
    $("#CorAlturaPatron").html("");
    $("#CorAlturaIBC").html("");
    $("#PUIBC").html("");
    $("#PRelacion").html("");
    $("#Gliserina").html("");

    $("#CorAltValor1").html("");
    $("#CorAltU1").html("");
    $("#CorAltValor2").html("");
    $("#CorAltU2").html("");
    $("#CorAltValor3").html("");
    $("#CorAltU3").html("");
    $("#CorAltValor4").html("");
    $("#CorAltU4").html("");
    $("#CorAltValor5").html("");
    $("#CorAltValor6").html("");

    $("#IncAltTipica1").html("");
    $("#IncAltTipica2").html("");
    $("#IncAltTipica3").html("");
    $("#IncAltTipica4").html("");
    $("#IncAltCoeficiente1").html("");
    $("#IncAltCoeficiente2").html("");
    $("#IncAltCoeficiente3").html("");
    $("#IncAltCoeficiente4").html("");
    $("#IncAltUEstandar1").html("");
    $("#IncAltUEstandar2").html("");
    $("#IncAltUEstandar3").html("");
    $("#IncAltUEstandar4").html("");
    $("#IncAltUEstandarpa").html("");
    $("#IncAltUEstandarmed").html("");
      
    

    $("#CaHoraIni").val(""); 
    $("#CaHoraFin").val(""); 
    $("#CaTiempo").val(""); 

    $("#ProximaCali").prop("disabled", false);
    $("#Observacion").prop("disabled", false);
    $("#TiempoCali").prop("disabled", false);
    $("#Porcentaje_A13").prop("disabled", false);
    $("#Porcentaje_A14").prop("disabled", false);
    $("#Porcentaje_A15").prop("disabled", false);
    $("#PresMax").prop("disabled", false);
    $("#PresMin").prop("disabled", false);
    $("#RangoPatron").prop("disabled", false);
    
    for (var x = 1; x <= 6; x++) {
        $("#AltRegPat" + x).html("");
        $("#AltRegIBC" + x).html("");

        $("#ForAltRegPat" + x).html("");
        $("#ForAltRegIBC" + x).html("");
    }
    $("#ForAltRegPat7").html("");
    $("#ForAltRegIBC7").html("");
    
    $("#RangoPatron").val("").trigger("change"); 
    var valor = "";
    for (var x = 1; x <= 3; x++) {
        if (x == 3)
            valor = "0";
        for (var fila = 1; fila <= 6; fila++) {
            $("#LecturaIBCMed_A" + x + fila).val(valor);
            $("#LecturaIBCMed_D" + x + fila).val(valor);

            $("#CorrecionMed_A" + x + fila).removeClass("bg-danger");
            $("#CorrecionMed_A" + x+ fila).removeClass("bg-naranja");
            $("#CorrecionMed_A" + x + fila).addClass("bg-default");

            $("#CorrecionMed_D" + x + fila).removeClass("bg-danger");
            $("#CorrecionMed_D" + x + fila).removeClass("bg-naranja");
            $("#CorrecionMed_D" + x + fila).addClass("bg-default");

            $("#LecturaPatMed_A" + x + fila).prop("disabled", false);
            $("#LecturaIBCMed_A" + x + fila).prop("disabled", false);

            $("#LecturaPatMed_D" + x + fila).prop("disabled", false);
            $("#LecturaIBCMed_D" + x + fila).prop("disabled", false);

            $("#LecturaPatMed_A" + x + fila).val(valor);
            $("#LecturaPatMed_D" + x + fila).val(valor);
                        
            $("#Lectura_A" + x + fila).val(valor);
            $("#Lectura_D" + x + fila).val(valor);
            $("#LecturaPatCon_A" + x + fila).val(valor);
            $("#LecturaPatCon_D" + x + fila).val(valor);
            $("#ErrorEcuaCon_A" + x + fila).val(valor);
            $("#ErrorEcuaCon_D" + x + fila).val(valor);
            $("#ErrorEcuaMed_A" + x + fila).val(valor);
            $("#ErrorEcuaMed_D" + x + fila).val(valor);
            $("#LecturaPatCorMed_A" + x + fila).val(valor);
            $("#LecturaPatCorMed_D" + x + fila).val(valor);

            $("#CorrecionMed_A" + x + fila).val(valor);
            $("#CorrecionMed_D" + x + fila).val(valor);

        }
    }

    ConfigurarTablas(6);
        
   
        
    $("#PresMax").val("");
    $("#PresMin").val("");

    $("#CoPresMin").val("");
    $("#CoPresMax").val("");

    $("#VarTemp").html("");
    $("#VarHume").html("");
    $("#VarPres").html("");

    $("#ForMinPres1").html("");
    $("#ForMinPres2").html("");
    $("#ForMinPres3").html("");
    $("#ForMinPres4").html("");

    $("#ForMaxPres1").html("");
    $("#ForMaxPres2").html("");
    $("#ForMaxPres3").html("");
    $("#ForMaxPres4").html("");

    IdInstrumento = 0;
    Graficar();
    GraficarDesviacion();
    GraficarBarra();
    CalcularTemperatura(0);
    
}

function GuardarDocumento() {
    var documento = document.getElementById("documento");
    if ($.trim(documento.value) != "") {

        var file = documento.files[0];
        var data = new FormData();
        data.append('documento', file);
        var resultado = '';
        var direccion = url + "Cotizacion/AdjuntarPDF";
        $.ajax({
            url: direccion,
            type: 'POST',
            contentType: false,
            data: data,
            processData: false,
            async: false,
            success: function (datos) {
                datos = datos.split("|");
                if (datos[0] != "0") {
                    swal(ValidarTraduccion("Error al cargar el archivo"), "", "error");
                } else {
                    if (datos[2] != "XX")
                        swal(ValidarTraduccion(datos[2]), "", "error");
                }
            }
        });
    }
}

function CambioTiempoCali(tiempo) {
    if (tiempo * 1 == 0)
        $("#ProximaCali").val("");
    else {
        var fecha = (d.getFullYear()*1 + tiempo*1) + "-" + (month < 10 ? '0' : '') + month + "-" + (day < 10 ? "0" : "") + day;
        $("#ProximaCali").val(fecha);
    }
}

function GeneralPlantilla() {
    var concepto = $("#Concepto").val() * 1;
    if (Ingreso == 0)
        return false;

    if (GuardaTemp == 1 || IdReporte == 0) {
        swal("Acción Cancelada", "Debe primero guardar el reporte", "warning");
        return false;
    }

    if ((concepto != 2) && (concepto != 1)) {
        swal("Acción Cancelada", "No se puede generar la planilla del certificado cuando no está apto a calibrar o en subcontrataciones", "warning");
        return false;
    }

    ActivarLoad();
    setTimeout(function () {
        var datos = LlamarAjax("Laboratorio/GenerarPlantilla", "ingreso=" + Ingreso + "&magnitud=6").split("|");
        DesactivarLoad();
        if (datos[0] == "0")
            swal("", datos[1], "success");
        else
            swal("Acción Cancelada", datos[1], "warning");
    }, 15);
}

function EnviarCotizacion() {

    var concepto = $("#Concepto").val() * 1;


    if (Ingreso == 0)
        return false;
    if (GuardaTemp == 1 || IdReporte == 0) {
        swal("Acción Cancelada", "Debe primero guardar el reporte", "warning");
        return false;
    }

    $("#eCliente").val($("#Solicitante").html());
    $("#eCorreo").val(CorreoEmpresa);


    if (concepto == 2) {
        $("#spcotizacion").html(Ingreso);
        $("#enviar_cotizacion").modal("show");
    } else {
        swal("Acción Cancelada", "Solo se podra enviar cotizaciones cuando el ingreso requiera ajuste o suministro", "warning");
    }
    
}

function BuscarIngreso(numero, code, tipo) {
    if ((Ingreso != (numero * 1)) && ($("#Equipo").html() != "")) {
        LimpiarDatos();
    }
        
    if ((code.keyCode == 13 || tipo == "0") && ErrorEnter == 0) {
        if (numero * 1 > 0) {
            ActivarLoad();
            setTimeout(function () {
                var datos = LlamarAjax("Laboratorio/BuscarIngresoPlaCert", "magnitud=2&ingreso=" + numero + "&medida=psi" + "&numero=" + NumCertificado + "&version=" + IdVersion).split("|");
                
                if (datos[0] != "[]") {
                    var data = JSON.parse(datos[0]);

                    if (data[0].idmagnitud != "2") {
                        DesactivarLoad();
                        swal("Acción Cancelada", "El ingreso número " + numero + " no pertenece a la magnitud <br><b>PAR TORSIONAL</b>", "warning");
                        ErrorEnter = 1;
                        $("#Ingreso").select();
                        $("#Ingreso").focus();
                        return false;
                    }

                    if (NumCertificado == 2 && data[0].desde * 1 == 0) {
                        DesactivarLoad();
                        swal("Acción Cancelada", "Este ingreso no posee un intervalo para presión negativa", "warning");
                        ErrorEnter = 1;
                        $("#Ingreso").select();
                        $("#Ingreso").focus();
                        return false;
                    }

                    if (data[0].medidaconpa * 1 == 0) {
                        DesactivarLoad();
                        swal("Acción Cancelada", "Debe de configurar la conversion de Pa a " + data[0].medida, "warning");
                        ErrorEnter = 1;
                        $("#Ingreso").select();
                        $("#Ingreso").focus();
                        return false;
                    }

                    if (data[0].medidacon * 1 == 0) {
                        DesactivarLoad();
                        swal("Acción Cancelada", "Debe de configurar la conversion de psi a " + data[0].medida, "warning");
                        ErrorEnter = 1;
                        $("#Ingreso").select();
                        $("#Ingreso").focus();
                        return false;
                    }

                    if (data[0].medidaconkpa * 1 == 0) {
                        DesactivarLoad();
                        swal("Acción Cancelada", "Debe de configurar la conversion de kPa a " + data[0].medida, "warning");
                        ErrorEnter = 1;
                        $("#Ingreso").select();
                        $("#Ingreso").focus();
                        return false;
                    }

                    
                       
                    Ingreso = data[0].ingreso * 1;
                    localStorage.setItem("Ingreso", Ingreso);
                    Foto = data[0].fotos * 1;
                    Solicitante = data[0].cliente;
                    $("#Solicitante").html(data[0].cliente);
                    Direccion = data[0].direccion;
                    FirmaCertificado = data[0].firmacertificado;
                    $("#firmacertificado").html("FIRMA " + FirmaCertificado);
                    if (FirmaCertificado == "MANUAL")
                        $("#ButCertFirMan").removeClass("hidden");
                    $("#Direccion").html(data[0].direccion);
                    Sitio = data[0].sitio;
                    $("#FechaIng").html(data[0].fechaing);
                    $("#FechaCal").html(FechaCal);
                    CorreoEmpresa = data[0].email;
                    CantRegitro = data[0].puntos*1;
                    
                    $("#TIngreso").html(data[0].ingreso);
                    $("#Medida").html(data[0].medida);
                    $("#ValorCon").html(data[0].medidacon);
                    $("#ValorConSI").html(data[0].medidaconkpa);
                    Factor = data[0].medidacon;
                    FactorSI = data[0].medidaconkpa;
                    $("#UnidadCon").html("1 " + data[0].medida + " =");
                    $("#UnidadConSI").html("1 " + data[0].medida + " =");

                    $(".medida").html(data[0].medida);
                    IncMetodo = UMetodo;
                    if (data[0].medida == "bar")
                        IncMetodo = UMetodo / Factor;
                    else
                        IncMetodo = UMetodo * Factor;

                    $("#IncMetodo").html(IncMetodo);

                    $("#Equipo").html(data[0].equipo);
                    $("#Marca").html(data[0].marca);
                    $("#Modelo").html(data[0].modelo);
                    $("#Serie").html(data[0].serie);
                    $("#Remision").val(data[0].remision);
                    InfoTecIngreso = data[0].informetecnico;
                    AlcanceDesde = data[0].alcance_desde;
                    AlcanceHasta = data[0].alcance_hasta;
                    $("#RangoDesde").html(data[0].desde);
                    $("#RangoMedida").html(data[0].desde + "-" + data[0].hasta);
                    $("#MedResolucion").html(data[0].medida);
                    RangoDesde = data[0].desde;
                    RangoHasta = data[0].hasta;
                    RangoEquipo = data[0].hasta;
                    if (data[0].sitio == "SI") {
                        TipoCalibracion = "Sitio";
                        $("#Sitio").html("CALIBRACIÓN EN INSTALACIONES DEL CLIENTE");
                        $("#Observacion").val("La calibración se realizó en las instalaciones del cliente");

                    } else {
                        TipoCalibracion = "Instalaciones";
                        $("#Sitio").html("CALIBRACIÓN EN INSTALACIONES PERMANENTES");
                        $("#Observacion").val("La calibración se realizó en las instalaciones permanentes de Calibration Service S.A.S.");
                    }

                    $("#AlcanceAcreditado").html("(" + AlcanceDesde + " a " + AlcanceHasta + ") psi");
                    Revision = data[0].revision + 1;
                                        
                    $("#DivEscala").html(data[0].division_escala + " " + data[0].medida);
                    $("#AlturaPatron").html(data[0].altura_patron);
                    $("#CoeSec").html(data[0].coef_senc);
                    CoefSec = data[0].coef_senc * 1;
                    $("#Resolucion").html(formato_numero(data[0].resolucion, 5, ".", ",", "") + " " + data[0].medida);
                    $("#AlturaIBC").html(data[0].altura_ibc);
                    Clase = data[0].clase;
                    $("#Clase").html(Clase);
                    $("#TipoClase").html(data[0].delrango != "" ? "Clase (" + data[0].delrango + ")" : "");
                    TipoClase = data[0].delrango;
                    Resolucion = data[0].resolucion;

                    AlturaPatron = data[0].altura_patron*1;
                    AlturaIBC = data[0].altura_ibc * 1;
                    
                    ForAlturaPatron = 0;
                    ForAlturaIBC = 0;

                    CorAlturaIBC = 0;
                    CorAlturaPatron = 0;
                    
                    if (datos[8] != "[]") {

                        var datacoe = JSON.parse(datos[8]);
                        
                        for (var x = 0; x < datacoe.length; x++) {
                            if (datacoe[x].tipo == "Altura Patron") {
                                tipo = "AltRegPat";
                                $("#ForAltRegPat1").html(Math.pow(AlturaPatron, 5));
                                $("#ForAltRegPat2").html(Math.pow(AlturaPatron, 4));
                                $("#ForAltRegPat3").html(Math.pow(AlturaPatron, 3));
                                $("#ForAltRegPat4").html(Math.pow(AlturaPatron, 2));
                                $("#ForAltRegPat5").html(AlturaPatron, 1);

                                ForAlturaPatron = (Math.pow(AlturaPatron, 5) * datacoe[x].valor1) + (Math.pow(AlturaPatron, 4) * datacoe[x].valor2) + (Math.pow(AlturaPatron, 3) * datacoe[x].valor3) +
                                    (Math.pow(AlturaPatron, 2) * datacoe[x].valor4) + (AlturaPatron * datacoe[x].valor5) + datacoe[x].valor6;
                                CorAlturaPatron = ForAlturaPatron + AlturaPatron;
                                $("#ForAltRegPat6").html(ForAlturaPatron);
                                $("#ForAltRegPat7").html(CorAlturaPatron);
                                
                            } else {
                                tipo = "AltRegIBC";
                                $("#ForAltRegIBC1").html(Math.pow(AlturaIBC, 5));
                                $("#ForAltRegIBC2").html(Math.pow(AlturaIBC, 4));
                                $("#ForAltRegIBC3").html(Math.pow(AlturaIBC, 3));
                                $("#ForAltRegIBC4").html(Math.pow(AlturaIBC, 2));
                                $("#ForAltRegIBC5").html(AlturaIBC, 5);

                                ForAlturaIBC = (Math.pow(AlturaIBC, 5) * datacoe[x].valor1) + (Math.pow(AlturaIBC, 4) * datacoe[x].valor2) + (Math.pow(AlturaIBC, 3) * datacoe[x].valor3) +
                                    (Math.pow(AlturaIBC, 2) * datacoe[x].valor4) + (AlturaIBC * datacoe[x].valor5) + datacoe[x].valor6;
                                CorAlturaIBC = ForAlturaIBC + AlturaIBC;
                                $("#ForAltRegIBC6").html(ForAlturaIBC);
                                $("#ForAltRegIBC7").html(CorAlturaIBC);

                            }
                            $("#" + tipo + "1").html(datacoe[x].valor1);
                            $("#" + tipo + "2").html(datacoe[x].valor2);
                            $("#" + tipo + "3").html(datacoe[x].valor3);
                            $("#" + tipo + "4").html(datacoe[x].valor4);
                            $("#" + tipo + "5").html(datacoe[x].valor5);
                            $("#" + tipo + "6").html(datacoe[x].valor6);
                        }
                        
                        if (CorAlturaPatron > CorAlturaIBC) {
                            MayorAltura = CorAlturaPatron;
                            MenorAltura = CorAlturaIBC;
                        } else {
                            MayorAltura = CorAlturaIBC;
                            MenorAltura = CorAlturaPatron;
                        }

                        $("#CorAlturaPatron").html(formato_numero(CorAlturaPatron, 7, ".", ",", ""));
                        $("#CorAlturaIBC").html(formato_numero(CorAlturaIBC, 7, ".", ",", ""));

                        CorAltU1 = datacoe[0].maxutotal / 1000;
                        var CorAltValor1 = (MayorAltura - MenorAltura) / 1000;    
                        $("#CorAltValor1").html(formato_numero((MayorAltura - MenorAltura) / 1000,11,".",",",""));
                        $("#CorAltU1").html(CorAltU1);
                        $("#CorAltValor2").html(data[0].densidad);
                        $("#CorAltU2").html(data[0].derivada);
                        var fluidoaire = data[0].aire.split("!");
                        $("#CorAltValor3").html(fluidoaire[0]);
                        $("#CorAltU3").html(fluidoaire[1]);
                        $("#CorAltValor4").html("9.8");
                        $("#CorAltU4").html("0.05");

                        var corre = (data[0].densidad - fluidoaire[0]) * 9.8 * ((MayorAltura - MenorAltura) / 1000);
                        $("#CorAltValor5").html(formato_numero(corre,3,".",",","") + " Pa");
                        $("#CorAltValor6").html(formato_numero(corre / data[0].medidaconpa, 7, ".", ",", ""));
                        
                        var AltTipica1 = CorAltU1 / 2;
                        var AltCoeficiente1 = (data[0].densidad - fluidoaire[0]) * 9.8;
                        $("#IncAltTipica1").html(formato_numero(AltTipica1, 6, ".", ",", ""));
                        $("#IncAltCoeficiente1").html(formato_numero(AltCoeficiente1, 4, ".", ",", ""));
                        $("#IncAltUEstandar1").html(formato_numero(AltTipica1 * AltCoeficiente1, 5, ".", ",", ""));

                        var AltTipica2 = data[0].derivada / Math.sqrt(12);
                        var AltCoeficiente2 = ((MayorAltura - MenorAltura) / 1000) * 9.8;
                        $("#IncAltTipica2").html(formato_numero(AltTipica2, 4, ".", ",", ""));
                        $("#IncAltCoeficiente2").html(AltCoeficiente2);
                        $("#IncAltUEstandar2").html(formato_numero(AltTipica2 * AltCoeficiente2, 4, ".", ",", ""));

                        var AltTipica3 = fluidoaire[1] / Math.sqrt(12);
                        var AltCoeficiente3 = ((MayorAltura - MenorAltura) / 1000) * 9.8*-1;
                        $("#IncAltTipica3").html(formato_numero(AltTipica3, 4, ".", ",", ""));
                        $("#IncAltCoeficiente3").html(AltCoeficiente3);
                        $("#IncAltUEstandar3").html(formato_numero(AltTipica3 * AltCoeficiente3, 5, ".", ",", ""));

                        var AltTipica4 = 0.05/2;
                        var AltCoeficiente4 = (data[0].densidad - fluidoaire[0]) * CorAltValor1;
                        $("#IncAltTipica4").html(formato_numero(AltTipica4, 4, ".", ",", ""));
                        $("#IncAltCoeficiente4").html(AltCoeficiente4);
                        $("#IncAltUEstandar4").html(formato_numero(AltTipica4 * AltCoeficiente4, 5, ".", ",", ""));

                        var sumacuadrado = Math.pow(AltTipica1 * AltCoeficiente1, 2) + Math.pow(AltTipica2 * AltCoeficiente2, 2) +
                            Math.pow(AltTipica3 * AltCoeficiente3, 2) + Math.pow(AltTipica4 * AltCoeficiente4, 2);

                        $("#IncAltUEstandarpa").html(Math.sqrt(sumacuadrado));
                        UEstandarPSI = Math.sqrt(sumacuadrado) / data[0].medidaconpa;
                        $("#IncAltUEstandarmed").html(UEstandarPSI);
                        
                    } else {
                        ErrorEnter = 1;
                        DesactivarLoad();
                        swal("Acción Cancelada", "No se ha configurrado las correcciones de la regla graduada para la calibración", "warning");
                        $("#Ingreso").select();
                        $("#Ingreso").focus();
                        return false;
                    }

                    $("#AlturaPatron").html(AlturaPatron);
                    
                    $("#Fluido").html(data[0].fluido_usado);
                    TopeCero = data[0].tope_cero;

                    if (TopeCero == "NO") {
                        $("#Porcentaje_A11").val("0");
                        $("#Porcentaje_D16").val("0");
                        $("#Porcentaje_A21").val("0");
                        $("#Porcentaje_D26").val("0");
                        $("#Porcentaje_A31").val("0");
                        $("#Porcentaje_D36").val("0");
                    }
                    else {
                        $("#Porcentaje_A11").val("10");
                        $("#Porcentaje_D16").val("10");
                        $("#Porcentaje_A21").val("10");
                        $("#Porcentaje_D26").val("10");
                        $("#Porcentaje_A31").val("10");
                        $("#Porcentaje_D36").val("10");
                    }
                        

                    $("#TopeCero").html(TopeCero);
                    $("#Gliserina").html(data[0].glicerina);

                    $("#Usuario").val(data[0].usuarioing);
                    $("#FechaRec").val(data[0].fecharec);
                    $("#Tiempo").val(data[0].tiempo);

                    
                                        
                    if (datos[2] != "[]") {

                        var datacer = JSON.parse(datos[2]);
                        IdCalibracion = datacer[0].id * 1;
                        FechaCal = datacer[0].fecha;
                        $("#FechaCal").html(FechaCal);
                        $("#ProximaCali").val(datacer[0].proximacali);
                        $("#RangoHasta").val(datacer[0].rangohasta);
                        RangoHasta = datacer[0].rangohasta;
                        Revision = datacer[0].revision * 1;
                        $("#Observacion").val(datacer[0].observacion)
                        HoraInicio = datacer[0].horaini;
                        HoraFinal = datacer[0].horafin;
                        HoraCondicion = HoraFinal;
                        Calculado = 1;
                        if (datacer[0].clase * 1 > 0) {
                            Clase = datacer[0].clase * 1;

                        }


                        $("#CaHoraIni").val(HoraInicio);
                        $("#CaHoraFin").val(HoraFinal);
                        $("#CaTiempo").val(datacer[0].tiempotrans);
                        
                        CerImpreso = datacer[0].impreso * 1;
                                                                        
                    }

                    if (Clase > 0.25)
                        $(".serie3").addClass("hidden");
                    else
                        $(".serie3").removeClass("hidden");


                    if (TipoClase == "DEL RANGO") {
                        DesMax = Clase / 100 * RangoHasta;
                        DesMin = Clase / 100 * RangoHasta * -1;    
                    } 

                    

                    $("#Clase").val(formato_numero(Clase, 4, ".", ",", ""));

                    if (datos[1] != "[]") {
                        var dataser = JSON.parse(datos[1]);
                        $("#RangoPatron").val(dataser[0].rangopatron).trigger("change");
                    } else {
                        DesactivarLoad();
                        swal("Acción Cancelada", "El ingreso número " + Ingreso + " no tiene una operación previa", "warning");
                        LimpiarTodo();
                        return false;
                    }

                    if (datos[7] != "[]") {
                        var datacerpre = JSON.parse(datos[7]);
                        $("#PresMax").val(datacerpre[0].premax);
                        $("#PresMin").val(datacerpre[0].premin);
                        CalcularPresion("0", 1);
                        CalcularPresion("0", 2);
                    }

                    if (datos[9] != "[]") {
                        var dataco = JSON.parse(datos[9]);
                        for (var x = 0; x < dataco.length; x++) {
                            if (x == 0) {
                                $("#CMetodo").val(dataco[x].metodo);
                                $("#CPunto").val(dataco[x].punto);
                                $("#CServicio").val(dataco[x].servicio);
                                $("#CObservCerti").val(dataco[x].observacion);
                                $("#CProxima").val(dataco[x].proxima);
                                $("#CEntrega").val(dataco[x].entrega);
                                $("#CAsesorComer").val(dataco[x].asesor);
                            } else {
                                $("#CMetodo").val("<br>" + dataco[x].metodo);
                                $("#CPunto").val("<br>" + dataco[x].punto);
                                $("#CServicio").val("<br>" + dataco[x].servicio);
                                $("#CObservCerti").val("<br>" + dataco[x].observacion);
                                $("#CProxima").val("<br>" + dataco[x].proxima);
                                $("#CEntrega").val("<br>" + dataco[x].entrega);
                                $("#CAsesorComer").val("<br>" + dataco[x].asesor);
                            }
                        }
                    } else {
                        if (IdCalibracion == 0) {
                            ErrorEnter = 1;
                            DesactivarLoad();
                            swal("Acción Cancelada", "Este ingreso no ha sido cotizado por el asesor comercial ", "warning");
                        }
                    }
                    
                    if (IdCalibracion == 0) {
                        
                        TipoLectura = $.trim(data[0].tipolectura);
                        if (TipoLectura == "ANALOGO"){
                            for (var x = 1; x <= 3; x++) {
                                for (var fila = 1; fila <= 6; fila++) {
                                    var porcentaje = NumeroDecimal($("#Porcentaje_A" + x + fila).val());
                                    var porcentajed = NumeroDecimal($("#Porcentaje_D" + x + fila).val());
                                    var valora = RangoHasta * porcentaje / 100;
                                    var valord = RangoHasta * porcentajed / 100;
                                    $("#LecturaIBCMed_A" + x + fila).val(formato_numero(valora, 3, ".", ",", "", 3));
                                    $("#LecturaIBCMed_D" + x + fila).val(formato_numero(valord, 3, ".", ",", "", 3));

                                    /*if (!(x == 1 && fila == 1)) {
                                        $("#LecturaPatMed_A" + x + fila).prop("disabled", true);
                                        $("#LecturaPatMed_A" + x + fila).val("Bloqueado");

                                        $("#LecturaPatMed_D" + x + fila).prop("disabled", true);
                                        $("#LecturaPatMed_D" + x + fila).val("Bloqueado");
                                    } else {
                                        $("#LecturaPatMed_D" + x + fila).prop("disabled", true);
                                        $("#LecturaPatMed_D" + x + fila).val("Bloqueado");
                                    }*/

                                    $("#LecturaPatMed_A11").prop("disabled", false);

                                    $("#Lectura_A" + x + fila).val(formato_numero(valora, 4, ".", ",", "", 3));
                                    $("#Lectura_D" + x + fila).val(formato_numero(valord, 4, ".", ",", "", 3));

                                }
                            }
                        } else {
                            for (var x = 1; x <= 3; x++) {
                                for (var fila = 1; fila <= 6; fila++) {
                                    var porcentaje = NumeroDecimal($("#Porcentaje_A" + x + fila).val());
                                    var porcentajed = NumeroDecimal($("#Porcentaje_D" + x + fila).val());
                                    var valora = RangoHasta * porcentaje / 100;
                                    var valord = RangoHasta * porcentajed / 100;
                                    $("#LecturaPatMed_A" + x + fila).val(formato_numero(valora, 3, ".", ",", "", 3));
                                    $("#LecturaPatMed_D" + x + fila).val(formato_numero(valord, 3, ".", ",", "", 3));

                                    $("#Lectura_A" + x + fila).val(formato_numero(valora, 4, ".", ",", "", 3));
                                    $("#Lectura_D" + x + fila).val(formato_numero(valord, 4, ".", ",", "", 3));
                                }

                                /*if (!(x == 1 && fila == 1)) {
                                    $("#LecturaIBCMed_A" + x + fila).prop("disabled", true);
                                    $("#LecturaIBCMed_A" + x + fila).val("Bloqueado");

                                    $("#LecturaIBCMed_D" + x + fila).prop("disabled", true);
                                    $("#LecturaIBCMed_D" + x + fila).val("Bloqueado");
                                } else {
                                    $("#LecturaIBCMed_D" + x + fila).prop("disabled", true);
                                    $("#LecturaIBCMed_D" + x + fila).val("Bloqueado");
                                }*/

                                $("#LecturaPatMed_A11").prop("disabled", false);
                            }
                        }
                                                                        
                        if (localStorage.getItem(NumCertificado + "DID159" + Ingreso)) {
                            var datatemp = JSON.parse(localStorage.getItem(NumCertificado + "DID159" + Ingreso));
                            if (localStorage.getItem(NumCertificado + "DIDDATOS" + Ingreso)) {
                                var datas = JSON.parse(localStorage.getItem(NumCertificado + "DIDDATOS" + Ingreso));
                                $("#ProximaCali").val(datas[0].ProximaCali);
                                if (datas[0].AlcanceEquipo * 1 > 0) {
                                    $("#RangoHasta").val(datas[0].AlcanceEquipo);
                                    RangoHasta = datas[0].AlcanceEquipo;
                                }
                                $("#Clase").val(datas[0].Clase);
                                HoraInicio = datas[0].HoraInicio;
                                HoraCondicion = HoraInicio;
                                $("#FechaCal").html(FechaActual);
                                FechaCal = FechaActual;
                                LeerCondiciones(0);
                                IntervaloTiempo = null;
                                IntervaloTiempo = setInterval(TiempoTrascurrudi, 60000);
                                $("#CaHoraIni").val(HoraInicio);
                                if (HoraInicio != "") {
                                    d = new Date();
                                    horaactual = (d.getHours() * 1 < 10 ? "0" : "") + d.getHours() * 1 + ":" + (d.getMinutes() * 1 < 10 ? "0" : "") + d.getMinutes();
                                    $("#CaTiempo").val(RestarHoras(HoraInicio, horaactual));
                                }
                            }

                            var pos = 0;

                            for (var serie = 1; serie <= 3; serie++) {
                                for (var fila = 1; fila <= 6; fila++) {
                                    if (datatemp[pos].LecturaPatron_A != "Bloqueado") {
                                        $("#LecturaPatMed_A" + serie + fila).val(formato_numero(NumeroDecimal(datatemp[pos].LecturaPatron_A), 3, ".", ",", "", 3));
                                        $("#LecturaPatMed_A" + serie + fila).prop("disabled", false);
                                    }
                                    if (datatemp[pos].LecturaIBC_A != "Bloqueado") {
                                        $("#LecturaIBCMed_A" + serie + fila).val(formato_numero(NumeroDecimal(datatemp[pos].LecturaIBC_A, 3), 3, ".", ",", "", 3));
                                        $("#LecturaIBCMed_A" + serie + fila).prop("disabled", false);
                                    }
                                    if (datatemp[pos].LecturaPatron_D != "Bloqueado") {
                                        $("#LecturaPatMed_D" + serie + fila).val(formato_numero(NumeroDecimal(datatemp[pos].LecturaPatron_D), 3, ".", ",", "", 3));
                                        $("#LecturaPatMed_D" + serie + fila).prop("disabled", false);
                                    }
                                    if (datatemp[pos].LecturaIBC_D != "Bloqueado") {
                                        $("#LecturaIBCMed_D" + serie + fila).val(formato_numero(NumeroDecimal(datatemp[pos].LecturaIBC_D), 3, ".", ",", "", 3));
                                        $("#LecturaIBCMed_D" + serie + fila).prop("disabled", false);
                                    }
                                        
                                    pos++;
                                }
                            }

                            CalcularTotal();
                        }

                        

                    } else {
                        AplicarTemporal = 1; 
                    }

                    if (datos[4] != "[]") {
                        var datagen = JSON.parse(datos[4]);
                        var revisioncer = datagen[0].revision * 1;
                        var numcer = datagen[0].item * 1;
                        Certificado = datagen[0].numero;
                        $("#FechaExp").html(datagen[0].fechaexp);
                        $("#ProximaCali").prop("disabled", true);
                        $("#Clase").prop("disabled", true);
                        $("#Observacion").prop("disabled", true);
                        $("#TiempoCali").prop("disabled", true);
                        $("#RangoPatron").prop("disabled", true);
                        $("#Porcentaje_A13").prop("disabled", true);
                        $("#Porcentaje_A14").prop("disabled", true);
                        $("#Porcentaje_A15").prop("disabled", true);
                        $("#PresMax").prop("disabled", true);
                        $("#PresMin").prop("disabled", true);
                        if (revisioncer == Revision && NumCertificado == numcer) {
                            
                            if (datagen[0].idusuarioanula * 1 > 0) {
                                $("#CertificadoAnulado").html("Certificado anulado por: " + datagen[0].nombrecompleto + ", Fecha: " + datagen[0].fecha + ", Observación: " + datagen[0].observacionanula);
                            }

                            var puntoscer = JSON.parse(datos[6]);

                            for (var x = 0; x < puntoscer.length; x++) {
                                $("#LecturaPatMed" + puntoscer[x].tipo + puntoscer[x].fila).val(puntoscer[x].lecpatronmed);
                                $("#LecturaIBCMed" + puntoscer[x].tipo + puntoscer[x].fila).val(puntoscer[x].lecturaibc);

                                $("#LecturaPatMed" + puntoscer[x].tipo + puntoscer[x].fila).prop("disabled",true);
                                $("#LecturaIBCMed" + puntoscer[x].tipo + puntoscer[x].fila).prop("disabled", true);

                                $("#Porcentaje" + puntoscer[x].tipo + puntoscer[x].fila).val(puntoscer[x].prenominalpor);
                                $("#Lectura" + puntoscer[x].tipo + puntoscer[x].fila).val(puntoscer[x].prenominalmed);
                                $("#LecturaPatCon" + puntoscer[x].tipo + puntoscer[x].fila).val(puntoscer[x].lecpatroncon);
                                $("#ErrorEcuaCon" + puntoscer[x].tipo + puntoscer[x].fila).val(puntoscer[x].erroecuacioncon);
                                $("#ErrorEcuaMed" + puntoscer[x].tipo + puntoscer[x].fila).val(puntoscer[x].erroecuacionmed);
                                $("#LecturaPatCorMed" + puntoscer[x].tipo + puntoscer[x].fila).val(puntoscer[x].lecpatcorre);
                                $("#CorrecionMed" + puntoscer[x].tipo + puntoscer[x].fila).val(puntoscer[x].corrlectura);
                            }

                            var cerdatos = JSON.parse(datos[3]);

                            for (var x = 0; x < cerdatos.length; x++) {
                                $("#PaLecturaMed" + cerdatos[x].fila).val(cerdatos[x].palecturamed);
                                $("#PaLecturaCon" + cerdatos[x].fila).val(cerdatos[x].palecturacon);
                                $("#PaUPatronCon" + cerdatos[x].fila).val(cerdatos[x].paupatroncon);
                                $("#PaUPatronMed" + cerdatos[x].fila).val(cerdatos[x].paupatronmed);
                                $("#PaDerivadaCon" + cerdatos[x].fila).val(cerdatos[x].paderivadacon);
                                $("#PaDerivadaMed" + cerdatos[x].fila).val(cerdatos[x].paderivadamed);

                                $("#EstPresion" + cerdatos[x].fila).val(cerdatos[x].lectura);
                                $("#EstDesviacion" + cerdatos[x].fila).val(cerdatos[x].estdesviacion);
                                $("#EstHisteresis" + cerdatos[x].fila).val(cerdatos[x].esthisteresis);
                                $("#EstHisteresis1" + cerdatos[x].fila).val(cerdatos[x].esthisteresis1);
                                $("#EstHisteresis2" + cerdatos[x].fila).val(cerdatos[x].esthisteresis2);
                                $("#EstHisteresis3" + cerdatos[x].fila).val(cerdatos[x].esthisteresis3);
                                $("#EstResolucion" + cerdatos[x].fila).val(cerdatos[x].estresolucion);
                                $("#EstUPatron" + cerdatos[x].fila).val(cerdatos[x].estupatron);
                                $("#EstDerivada" + cerdatos[x].fila).val(cerdatos[x].estderivada);
                                $("#EstTempPatron" + cerdatos[x].fila).val(cerdatos[x].esttemppatron);
                                $("#EstTempIBC" + cerdatos[x].fila).val(cerdatos[x].esttempibc);
                                $("#EstErrorMaxPos" + cerdatos[x].fila).val(cerdatos[x].esterrormaxpos);
                                $("#EstErrorMaxNeg" + cerdatos[x].fila).val(cerdatos[x].esterrormaxneg);

                                DesMax = NumeroDecimal(cerdatos[x].esterrormaxpos);
                                DesMin = NumeroDecimal(cerdatos[x].esterrormaxneg);

                                $("#InsPresion" + cerdatos[x].fila).val(cerdatos[x].lectura);
                                $("#InsURepetibilidad" + cerdatos[x].fila).val(cerdatos[x].insurepetibilidad);
                                $("#InsUResolucion" + cerdatos[x].fila).val(cerdatos[x].insuresolucion);
                                $("#InsUHisteresis" + cerdatos[x].fila).val(cerdatos[x].insuhisteresis);
                                $("#InsUTempIBC" + cerdatos[x].fila).val(cerdatos[x].insutempibc);
                                $("#InsUNivel" + cerdatos[x].fila).val(cerdatos[x].insunivel);
                                $("#InsUCalPatron" + cerdatos[x].fila).val(cerdatos[x].insucalpatron);
                                $("#InsUDeriPatron" + cerdatos[x].fila).val(cerdatos[x].insuderipatron);
                                $("#InsUTempPat" + cerdatos[x].fila).val(cerdatos[x].insutemppat);
                                $("#InsUCombinada" + cerdatos[x].fila).val(cerdatos[x].insucombinada);

                                $("#ResGrado" + cerdatos[x].fila).val(cerdatos[x].resgrado);
                                $("#ResFactor" + cerdatos[x].fila).val(cerdatos[x].resfactor);
                                $("#ResUExpandida1" + cerdatos[x].fila).val(cerdatos[x].resuexpandida1);
                                $("#ResUExpandida2" + cerdatos[x].fila).val(cerdatos[x].resuexpandida2);
                                $("#ResUExpandida3" + cerdatos[x].fila).val(cerdatos[x].resuexpandida3);
                                $("#ResUExpandida4" + cerdatos[x].fila).val(cerdatos[x].resuexpandida4);

                                $("#RMUPrecionIBC" + cerdatos[x].fila).val(cerdatos[x].lectura);
                                $("#RMULecuraPatIBC" + cerdatos[x].fila).val(cerdatos[x].rmulecurapatibc);
                                $("#RMULecuraIBC" + cerdatos[x].fila).val(cerdatos[x].rmulecuraibc);
                                $("#RMUCorreccionIBC" + cerdatos[x].fila).val(cerdatos[x].rmucorreccionibc);
                                $("#RMUUExpandidaIBC" + cerdatos[x].fila).val(cerdatos[x].rmuuexpandidaibc);
                                $("#RMUPrecionSI" + cerdatos[x].fila).val(cerdatos[x].rmuprecionsi);
                                $("#RMULecuraPatSI" + cerdatos[x].fila).val(cerdatos[x].rmulecurapatsi);
                                $("#RMULecuraSI" + cerdatos[x].fila).val(cerdatos[x].rmulecurasi);
                                $("#RMUCorreccionSI" + cerdatos[x].fila).val(cerdatos[x].rmucorreccionsi);
                                $("#RMUUExpandidaSI" + cerdatos[x].fila).val(cerdatos[x].rmuuexpandidasi);
                                
                                $("#EvaURepetibilidad" + cerdatos[x].fila).val(cerdatos[x].evaurepetibilidad);
                                $("#EvaUHisteresis" + cerdatos[x].fila).val(cerdatos[x].evauhisteresis);
                                $("#EvaUResolucion" + cerdatos[x].fila).val(cerdatos[x].evauresolucion);
                                $("#EvaUTempIBC" + cerdatos[x].fila).val(cerdatos[x].evautempibc);
                                $("#EvaNivel" + cerdatos[x].fila).val(cerdatos[x].evanivel);
                                $("#EvaCalPatron" + cerdatos[x].fila).val(cerdatos[x].evacalpatron);
                                $("#EvaDeriPatron" + cerdatos[x].fila).val(cerdatos[x].evaderipatron);
                                $("#EvaTempPatron" + cerdatos[x].fila).val(cerdatos[x].evatemppatron);
                                $("#EvaUMaxima1" + cerdatos[x].fila).val(cerdatos[x].evaumaxima1);
                                $("#EvaUMaxima2" + cerdatos[x].fila).val(cerdatos[x].evaumaxima2);
                                $("#EvaDistribucion" + cerdatos[x].fila).val(cerdatos[x].evadistribucion);
                                $("#EvaEvaluador" + cerdatos[x].fila).val(cerdatos[x].evaevaluador);
                                $("#EvaDominante" + cerdatos[x].fila).val(cerdatos[x].evadominante);
                            }

                            Graficar();
                            GraficarDesviacion();
                            GraficarBarra();
                            CalcularTemperatura(0);
                            $("#RangoHasta").prop("disabled", true);
                            ErrorEnter = 1;
                            $("#Certificado").html(Certificado);
                            DesactivarLoad();
                            swal("Advertencia", "El certificado ya fue generado con el número " + Certificado, "warning")
                            EscucharMensaje("El certificado ya fue generado");


                        }
                    }

                    
                    if (ErrorTemp == 1 || ErrorHume == 1)
                        $("#documento").focus();

                    if (IdCalibracion > 0 && Certificado == "") {

                        var puntoscer = JSON.parse(datos[6]);

                        for (var x = 0; x < puntoscer.length; x++) {
                            $("#LecturaPatMed" + puntoscer[x].tipo + puntoscer[x].fila).val(puntoscer[x].lecpatronmed);
                            $("#LecturaIBCMed" + puntoscer[x].tipo + puntoscer[x].fila).val(puntoscer[x].lecturaibc);
                        }

                        CalcularTotal();
                        CalcularTemperatura(1);
                        
                        /*if (FechaCal != FechaActual) {
                            EliminarTemporal();
                            swal("Advertencia", "Temporal Elimado porque fue guardado un día diferente a la fecha actual", "warning");
                            EscucharMensaje("Temporal Elimado, porque fue guardado un día diferente a la fecha actual");
                        } else {*/
                        ErrorEnter = 1;
                        swal("Advertencia", "El certificado ya fue guardado el día " + datacer[0].fecha + ", pero no ha sido generado", "warning");
                        EscucharMensaje("El certificado ya fue guardado, pero no ha sido generado");
                        DesactivarLoad();
                        //}
                    }
                } else {
                    $("#Ingreso").select();
                    $("#Ingreso").focus();
                    ErrorEnter = 1;
                    DesactivarLoad();
                    swal("Acción Cancelada", "El Ingreso número " + numero + " no se ha recibido en el laboratorio", "warning");
                    return false;
                }
                DesactivarLoad();
            }, 15);
        }
    } else
        ErrorEnter = 0;
}

function EliminarTemporal() {
    if (IdCalibracion > 0 && Certificado == "") {
        var numero = Ingreso;

        swal.queue([{
            title: ValidarTraduccion('Advertencia'),
            text: ValidarTraduccion('¿Seguro que desea eliminar el temporal del certificado del ingreso número ' + Ingreso + '?'),
            type: 'question',
            showLoaderOnConfirm: true,
            showCancelButton: true,
            confirmButtonText: ValidarTraduccion('Si'),
            cancelButtonText: ValidarTraduccion('No'),
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            preConfirm: function () {
                return new Promise(function (resolve, reject) {
                    $.post("Laboratorio/EliminarCalibTemporal", "numcertificado=" + NumCertificado + "&revision=" + Revision + "&ingreso=" + Ingreso)
                        .done(function (data) {
                            datos = data.split("|");
                            if (datos[0] == "0") {
                                LimpiarTodo();
                                $("#Ingreso").val(numero);
                                BuscarIngreso(numero, "", "0");
                                swal("", datos[1], "");
                                resolve()
                            } else {
                                reject(datos[1]);
                            }
                        })
                })
            }
        }]);
    }
}

function CalcularSubTotal(Caja) {
    ValidarTexto(Caja, 3);
    CalcularTotal();
}



function CalcularTotal() {
    for (var serie = 1; serie <= 3; serie++) {
        for (var posicion = 1; posicion <= 6; posicion++) {
            CalcularSerie("", '_A' + serie + posicion, "");
            CalcularSerie("", '_D' + serie + posicion, "");
        }
    }
}

function ReCalcular() {
    if (Certificado == "" && Ingreso > 0) {
        ActivarLoad();
        setTimeout(function () {
            CalcularTotal();
            Calculado = 1;
            DesactivarLoad();
        }, 15);
    }
}

function GuardarTemporal() {

    if (Certificado != "")
        return false;
            
    if (Ingreso > 0) {

        Calculado = 0;
        
        
        var archivo = "[";

        for (var serie = 1; serie <= 3; serie++) {
            for (var fila = 1; fila <= 6; fila++) {
                if (archivo == "[")
                    archivo += '{"LecturaPatron_A":"' + $("#LecturaPatMed_A" + serie + fila).val() + '"';
                else
                    archivo += ',{"LecturaPatron_A":"' + $("#LecturaPatMed_A" + serie + fila).val() + '"';
                archivo += ',"LecturaIBC_A":"' + $("#LecturaIBCMed_A" + serie + fila).val() + '"';
                archivo += ',"LecturaPatron_D":"' + $("#LecturaPatMed_D" + serie + fila).val() + '"';
                archivo += ',"LecturaIBC_D":"' + $("#LecturaIBCMed_D" + serie + fila).val() + '"}';
            }
        }
                
        archivo += ']';
                                
        localStorage.setItem(NumCertificado + "DID159" + Ingreso, archivo);

        var archivo = "[{";
        archivo += '"HoraInicio":"' + HoraInicio + '","ProximaCali":"' + $("#ProximaCali").val() + '"';
        archivo += ',"AlcanceEquipo":"' + RangoHasta + '","Clase":"' + $("#Clase").val() + '"';
        archivo += '}]';

        localStorage.setItem(NumCertificado + "DIDDATOS" + Ingreso, archivo);

        $.jGrowl("Guardando Temporal", { life: 1500, theme: 'growl-success', header: '' });
        GuardaTemp = 1;
    } 
}

function SumarContador() {
    if (Ingreso == 0 || IdCalibracion > 0)
        return false;
    if ($(IdTomaDatos).prop("disabled")) {
        SegundosToma += 1;
        $(IdTomaDatos).val(SegundosToma + " Seg");
        if (SegundosToma == 30) {
            $(IdTomaDatos).prop("disabled", false);
            $(IdTomaDatos).val("");
            $(IdTomaDatos).focus();
            clearInterval(IntervalContador);
        }
    } else {
        clearInterval(IntervalContador);
    }
}

function CalcularTotalAlcance(Caja) {
    ActivarLoad();
    setTimeout(function () {
        if (NumeroDecimal(Caja.value)  > 0) {
            RangoHasta = NumeroDecimal(Caja.value);
            if (TipoLectura == "ANALOGO") {
                for (var x = 1; x <= 3; x++) {
                    for (var fila = 1; fila <= 6; fila++) {
                        var porcentaje = NumeroDecimal($("#Porcentaje_A" + x + fila).val());
                        var porcentajed = NumeroDecimal($("#Porcentaje_D" + x + fila).val());
                        var valora = RangoHasta * porcentaje / 100;
                        var valord = RangoHasta * porcentajed / 100;
                        $("#LecturaIBCMed_A" + x + fila).val(formato_numero(valora, 3, ".", ",", "", 3));
                        $("#LecturaIBCMed_D" + x + fila).val(formato_numero(valord, 3, ".", ",", "", 3));
                        $("#Lectura_A" + x + fila).val(formato_numero(valora, 4, ".", ",", "", 3));
                        $("#Lectura_D" + x + fila).val(formato_numero(valord, 4, ".", ",", "", 3));
                    }
                }
            } else {
                for (var x = 1; x <= 3; x++) {
                    for (var fila = 1; fila <= 6; fila++) {
                        var porcentaje = NumeroDecimal($("#Porcentaje_A" + x + fila).val());
                        var porcentajed = NumeroDecimal($("#Porcentaje_D" + x + fila).val());
                        var valora = RangoHasta * porcentaje / 100;
                        var valord = RangoHasta * porcentajed / 100;
                        $("#LecturaPatMed_A" + x + fila).val(formato_numero(valora, 3, ".", ",", "", 3));
                        $("#LecturaPatMed_D" + x + fila).val(formato_numero(valord, 3, ".", ",", "", 3));
                        $("#Lectura_A" + x + fila).val(formato_numero(valora, 4, ".", ",", "", 3));
                        $("#Lectura_D" + x + fila).val(formato_numero(valord, 4, ".", ",", "", 3));
                    }
                }
            }
            CalcularTotal();
            GuardarTemporal();
        }
        DesactivarLoad();
    }, 15);
}

function CalcularAlcance(Caja) {
    ValidarTexto(Caja, 3);
    var valor = Caja.value * Factor;
    if (valor * 1 != 0) {

        if (RangoEquipo < 0) {
            if (valor > 0) {
                $("#RangoHasta").focus();
                $("#RangoHasta").val("");
                RangoHasta = 0;
                swal("Acción Cancelada", "El alcance de equipo debe ser un valor negativo", "warning");
                return false;
            }

            if (valor * 1 < AlcanceDesde) {
                $("#RangoHasta").focus();
                $("#RangoHasta").val("");
                RangoHasta = 0;
                swal("Acción Cancelada", "El alcance de equipo supera el alcance acreditado", "warning");
                return false;
            }

            if (valor * 1 < RangoEquipo) {
                $("#RangoHasta").focus();
                $("#RangoHasta").val("");
                RangoHasta = 0;
                swal("Acción Cancelada", "El alcance que ingresó [" + valor + "] supera el alcance del equipo", "warning");
                return false;
            }

        } else {
            if (valor < 0) {
                $("#RangoHasta").focus();
                $("#RangoHasta").val("");
                RangoHasta = 0;
                swal("Acción Cancelada", "El alcance de equipo debe ser un valor positivo", "warning");
                return false;
            }

            if (valor * 1 > AlcanceHasta) {
                $("#RangoHasta").focus();
                $("#RangoHasta").val("");
                RangoHasta = 0;
                swal("Acción Cancelada", "El alcance de equipo supera el alcance acreditado", "warning");
                return false;
            }

            if (valor * 1 > RangoEquipo) {
                $("#RangoHasta").focus();
                $("#RangoHasta").val("");
                RangoHasta = 0;
                swal("Acción Cancelada", "El alcance que ingresó supera el alcance del equipo", "warning");
                return false;
            }

        }
    }
}




function CalcularSerie(Caja, fila, code) {
        
    if (Certificado != "")
        return false;



    if (code.keyCode == 40 || code.keyCode == 13) {
                
        var texto = $.trim(Caja.id).substr(0, 10) == "LecturaIBC" ? "#LecturaIBCMed_" : "#LecturaPatMed_";

        var siguiente = fila.substr(3, 1)*1;
        if (siguiente < 6) {
            $(texto + fila.substr(1, 2) + (siguiente + 1)).select();
            $(texto + fila.substr(1, 2) + (siguiente + 1)).focus();
        } else {
            var serie = fila.substr(2, 1) * 1;
            var final = Clase > 0.25 ? final = 2 : 3;
            if (serie < final || fila.substr(1, 1) == "A") {

                $(texto + (fila.substr(1, 1) == "A" ? "D" : "A") + (fila.substr(1, 1) == "A" ? serie : serie++) + "1").select();
                $(texto + (fila.substr(1, 1) == "A" ? "D" : "A") + (fila.substr(1, 1) == "A" ? serie : serie++) + "1").focus();
            }
        }
        return false;
    }

    if (code.keyCode == 38) {
        var texto = $.trim(Caja.id).substr(0, 10) == "LecturaIBC" ? "#LecturaIBCMed_" : "#LecturaPatMed_";

        var siguiente = fila.substr(3, 1) * 1;
        if (siguiente > 1) {
            $(texto + fila.substr(1, 2) + (siguiente - 1)).select();
            $(texto + fila.substr(1, 2) + (siguiente - 1)).focus();
        } else {
            var serie = fila.substr(2, 1) * 1;
            var final = Clase > 0.25 ? final = 2 : 3;
            if (serie > 1 || fila.substr(1, 1) == "D") {
                $(texto + (fila.substr(1, 1) == "A" ? "D" : "A") + (fila.substr(1, 1) == "D" ? serie : serie--) + "6").select();
                $(texto + (fila.substr(1, 1) == "A" ? "D" : "A") + (fila.substr(1, 1) == "D" ? serie : serie--) + "6").focus();
            }
        }
        return false;
    }
            
    if (Caja != "") {
        ValidarTexto(Caja, 3);
        if ($.trim(Caja.value) != "") {

            if (IdCalibracion == 0) {
                if (HoraInicio == "") {
                    d = new Date();
                    HoraInicio = (d.getHours() * 1 < 10 ? "0" : "") + d.getHours() + ":" + (d.getMinutes() * 1 < 10 ? "0" : "") + d.getMinutes();
                    HoraCondicion = HoraInicio;
                    $("#CaHoraIni").val(HoraInicio);
                    horaactual = (d.getHours() * 1 < 10 ? "0" : "") + d.getHours() + ":" + (d.getMinutes() * 1 < 10 ? "0" : "") + d.getMinutes();
                    $("#CaTiempo").val(RestarHoras(HoraInicio, horaactual));
                    IntervaloTiempo = null;
                    IntervaloTiempo = setInterval(TiempoTrascurrudi, 60000);
                }
            }

            var texto = $.trim(Caja.id).substr(0, 10) == "LecturaIBC" ? "#LecturaIBCMed_" : "#LecturaPatMed_";
            var id = "";
            var siguiente = fila.substr(3, 1) * 1;
            if (siguiente < 6) {
                id = texto + fila.substr(1, 2) + (siguiente + 1);
            } else {
                var serie = fila.substr(2, 1) * 1;
                var final = Clase > 0.25 ? final = 2 : 3;
                if (serie < final || fila.substr(1, 1) == "A") {
                    id = texto + (fila.substr(1, 1) == "A" ? "D" : "A") + (fila.substr(1, 1) == "A" ? serie : (serie+1)) + "1";
                }
            }
            if (IdTomaDatos != id) {
                SegundosToma = 0;
                IdTomaDatos = id;
                IntervalContador = null;
                IntervalContador = setInterval(SumarContador, 1000);

            }
        }
    }

    var lecturapat = NumeroDecimal($("#LecturaPatMed" + fila).val());
    var lecturaibc = NumeroDecimal($("#LecturaIBCMed" + fila).val());
        
    var pos = fila.substr(3, 1) * 1;
    var pos2 = 7 - pos;
    var tipo = fila.substr(1, 1);
    var CMC = 0;

    if (tipo == "D") {
        pos2 = pos;
        pos = 7 - pos2;
    }

    //$.jGrowl(pos + " " + pos2, { life: 1500, theme: 'growl-warning', header: '' });

    

    if (NumCertificado == 2) {
        lecturapat = Math.abs(lecturapat) * -1;
        lecturaibc = Math.abs(lecturaibc) * -1;
    }
        
    var porcentaje = NumeroDecimal($("#Porcentaje" + fila).val()) * 1;
    var lectura = RangoHasta * porcentaje / 100;

    if (TipoClase == "DE LECTURA") {
        DesMax = (Clase * RangoHasta) / porcentaje;
        DesMin = (Clase * RangoHasta) / porcentaje * -1;
    }

    $("#Lectura" + fila).val(formato_numero(lectura, 4,_CD,_CM,""));
    if (tipo == "A") {
        $("#EstPresion" + pos).val(formato_numero(lectura, 0,_CD,_CM,""));
        $("#RMUPrecionIBC" + pos).val(formato_numero(lectura, 0,_CD,_CM,""));
        $("#RMUPrecionSI" + pos).val(formato_numero(lectura * FactorSI, _DE,_CD,_CM,""));
        $("#InsPresion" + pos).val(formato_numero(lectura, 0,_CD,_CM,""));
    }
            
   var lecturacon = lecturapat * Factor;
    $("#LecturaPatCon" + fila).val(formato_numero(lecturacon, 4, ".", ",", ""));
    
    var patron = $("#RangoPatron").val() * 1;
    $("#ErrorEcuaCon" + fila).val("");
    $("#ErrorEcuaMed" + fila).val("");
    $("#LecturaPatCorMed" + fila).val("");
    $("#CorrecionMed" + fila).val("");

    var correccion = 0;


    if (patron > 0) {
        var parametros = "patron=" + patron + "&lectura=" + lecturacon + "&lecturamax=" + (RangoHasta * Factor);
            
        var datos = LlamarAjax("Laboratorio/ConvertirLecturaPresionErr", parametros).split("|");
        if (datos[0] == "0") {
            correccion = (lecturapat - (datos[1] / Factor)) - lecturaibc;
            $("#ErrorEcuaCon" + fila).val(formato_numero(datos[1], 4, ".", ",", ""));
            $("#ErrorEcuaMed" + fila).val(formato_numero(datos[1] / Factor, 4, ".", ",", ""));
            $("#LecturaPatCorMed" + fila).val(formato_numero(lecturapat - (datos[1] / Factor), 4, ".", ",", ""));
            $("#CorrecionMed" + fila).val(formato_numero(correccion, 4, ".", ",", ""));
        } else {
            $("#RangoPatron").focus();
            swal("Acción Cancelada", datos[1], "warning");
            $("#RangoPatron").val("").trigger("change");
            return false;
        }

        var corr1 = NumeroDecimal($("#CorrecionMed_A1" + pos).val());
        var corr2 = NumeroDecimal($("#CorrecionMed_D1" + pos2).val());
        var corr3 = NumeroDecimal($("#CorrecionMed_A2" + pos).val());
        var corr4 = NumeroDecimal($("#CorrecionMed_D2" + pos2).val());
        var corr5 = NumeroDecimal($("#CorrecionMed_A3" + pos).val());
        var corr6 = NumeroDecimal($("#CorrecionMed_D3" + pos2).val());

        var corrmed1 = NumeroDecimal($("#LecturaPatCorMed_A1" + pos).val());
        var corrmed2 = NumeroDecimal($("#LecturaPatCorMed_D1" + pos2).val());
        var corrmed3 = NumeroDecimal($("#LecturaPatCorMed_A2" + pos).val());
        var corrmed4 = NumeroDecimal($("#LecturaPatCorMed_D2" + pos2).val());
        var corrmed5 = NumeroDecimal($("#LecturaPatCorMed_A3" + pos).val());
        var corrmed6 = NumeroDecimal($("#LecturaPatCorMed_D3" + pos2).val());

        var lecibc1 = NumeroDecimal($("#LecturaIBCMed_A1" + pos).val());
        var lecibc2 = NumeroDecimal($("#LecturaIBCMed_D1" + pos2).val());
        var lecibc3 = NumeroDecimal($("#LecturaIBCMed_A2" + pos).val());
        var lecibc4 = NumeroDecimal($("#LecturaIBCMed_D2" + pos2).val());
        var lecibc5 = NumeroDecimal($("#LecturaIBCMed_A3" + pos).val());
        var lecibc6 = NumeroDecimal($("#LecturaIBCMed_D3" + pos2).val());

        
        
        var datos_m = 0;
        var media = 0;
        var datos_v = 0;
        var desviacion = 0;
        var rmulecturapac = 0;
        var rmulecturaibc = 0;
        var numlectura = 0;
        if (Clase > 0.25) {
            var datos_m = 0;
            datos_m = corr1 + corr3;
            //calcular media
            media = datos_m / 2;
            datos_v = 0;
            datos_v += Math.pow((corr1 - media), 2);
            datos_v += Math.pow((corr3 - media), 2);
            desviacion = datos_v / 1;
            desviacion = Math.sqrt(desviacion);
            $("#EstHisteresis3" + pos).val("-------");
            rmulecturapac = (corrmed1 + corrmed2 + corrmed3 + corrmed4) / 4;
            rmulecturaibc = (lecibc1 + lecibc2 + lecibc3 + lecibc4) / 4;
            numlectura = 4;
        }
        else {
            datos_m = 0;
            datos_m = corr1 + corr3 + corr5;
            //calcular media
            media = datos_m / 3;
            datos_v = 0;
            datos_v += Math.pow((corr1 - media), 2);
            datos_v += Math.pow((corr3 - media), 2);
            datos_v += Math.pow((corr5 - media), 2);
            desviacion = datos_v / 2;
            desviacion = Math.sqrt(desviacion);
            $("#EstHisteresis3" + pos).val(formato_numero(Math.abs(corr5 - corr6), 5, ".", ",", ""));
            rmulecturapac = (corrmed1 + corrmed2 + corrmed3 + corrmed4 + corrmed5 + corrmed6) / 6;
            rmulecturaibc = (lecibc1 + lecibc2 + lecibc3 + lecibc4 + lecibc5 + lecibc6) / 6;
            numlectura = 6;
        }

        
        var insrepetivilidad = desviacion / Math.sqrt(numlectura-1);
        var insresolucion = Resolucion / Math.sqrt(12);
        var histeresis = 0;
        var valor = 0; 

        

        if (isNaN(rmulecturapac * Factor)) {
            return false;
        }

        var datos = LlamarAjax("Laboratorio/CalculorCertiPresion", "patron=" + patron + "&lectura=" + (rmulecturapac * Factor)).split("|");
        var datapa = JSON.parse(datos[0]);
        CMC = datos[1] * 1;

        //TABLA Lectura Patrón
        $("#PaLecturaMed" + pos).val(formato_numero(rmulecturapac, _DE,_CD,_CM,""));
        $("#PaLecturaCon" + pos).val(formato_numero(rmulecturapac * Factor, _DE,_CD,_CM,""));
        $("#PaUPatronCon" + pos).val(formato_numero(datapa[0].uexpandida, 5, ".", ",", ""));
        $("#PaUPatronMed" + pos).val(formato_numero(datapa[0].uexpandida / Factor, 8, ".", ",", ""));
        $("#PaDerivadaCon" + pos).val(formato_numero(datapa[0].derivada, 8, ".", ",", ""));
        $("#PaDerivadaMed" + pos).val(formato_numero(datapa[0].derivada / Factor, 8, ".", ",", ""));

        //TABLA ESTIMACIONES
        var coesecpat = NumeroDecimal($("#CoefSencPat").val());
        var esttemppat = ((coesecpat * RangoHasta) / 100) * TempVar;
        var esttempibc = ((CoefSec * RangoHasta) / 100) * TempVar;

        $("#EstDesviacion" + pos).val(formato_numero(desviacion, 5, ".", ",", ""));
        $("#EstHisteresis1" + pos).val(formato_numero(Math.abs(corr1 - corr2), 5, ".", ",", ""));
        $("#EstHisteresis2" + pos).val(formato_numero(Math.abs(corr3 - corr4), 5, ".", ",", ""));
        $("#EstResolucion" + pos).val(formato_numero(Resolucion, 5, ".", ",", ""));
        $("#EstTempPatron" + pos).val(formato_numero(esttemppat, 5, ".", ",", ""));
        $("#EstTempIBC" + pos).val(formato_numero(esttempibc, 5, ".", ",", ""));
        $("#EstUPatron" + pos).val(formato_numero(datapa[0].uexpandida / Factor, 5, ".", ",", ""));
        $("#EstDerivada" + pos).val(formato_numero(datapa[0].derivada / Factor, 5, ".", ",", ""));
        
        $("#EstErrorMaxPos" + pos).val(formato_numero(DesMax, 5, ".", ",", ""));
        $("#EstErrorMaxNeg" + pos).val(formato_numero(DesMin, 5, ".", ",", ""));

        var uibc = 0;
        for (var x = 1; x <= 6; x++) {
            valor = NumeroDecimal($("#EstErrorMaxPos" + x).val());
            if (valor > uibc)
                uibc = valor;
        }
        $("#PUIBC").html(formato_numero(uibc, 5, ".", ",", ""));
        $("#PRelacion").html(formato_numero(uibc / PUPatron, 7, ".", ",", ""));

        valor = 0;
        if (numlectura == 4) {
            for (var x = 1; x <= 2; x++) {
                valor = NumeroDecimal($("#EstHisteresis" + x + pos).val());
                console.log(valor + " > " + histeresis + " " + pos + " " + fila);
                if (valor > histeresis)
                    histeresis = valor;
            }
        } else {
            for (var x = 1; x <= 3; x++) {
                valor = NumeroDecimal($("#EstHisteresis" + x + pos).val());
                if (valor > histeresis)
                    histeresis = valor;
            }
        }       
        $("#EstHisteresis" + pos).val(formato_numero(histeresis, 5, ".", ",", ""));

        
        //TABLA Resultados de la Medición en Unidades del IBC
        $("#RMULecuraPatIBC" + pos).val(formato_numero(rmulecturapac, 4, ".", ",", ""));
        $("#RMULecuraIBC" + pos).val(formato_numero(rmulecturaibc, _DE,_CD,_CM,""));
        $("#RMUCorreccionIBC" + pos).val(formato_numero(rmulecturapac - rmulecturaibc, _DE,_CD,_CM,""));

        //TABLA Resultados de la Medición en Unidades del SI
        $("#RMULecuraPatSI" + pos).val(formato_numero(rmulecturapac * FactorSI, 3, ".", ",", ""));
        $("#RMULecuraSI" + pos).val(formato_numero(rmulecturaibc * FactorSI, 3, ".", ",", ""));
        $("#RMUCorreccionSI" + pos).val(formato_numero((rmulecturapac - rmulecturaibc) * FactorSI, 3, ".", ",", ""));

        //TABLA ESTIMACIÓN DE INCERTIDUMBRES ESTÁNDAR

        var ucombinada = Math.pow(insrepetivilidad, 2) + Math.pow(insresolucion, 2) + Math.pow(histeresis / Math.sqrt(12), 2) + Math.pow(UEstandarPSI, 2) +
            Math.pow(datapa[0].uexpandida / 2, 2) + Math.pow(datapa[0].derivada / Math.sqrt(12), 2) + Math.pow(UMetodo, 2) + 
            Math.pow(esttempibc / Math.sqrt(12), 2) + Math.pow(esttemppat / Math.sqrt(12), 2);

        ucombinada = Math.sqrt(ucombinada);

        $("#InsURepetibilidad" + pos).val(formato_numero(insrepetivilidad, 5, ".", ",", ""));
        $("#InsUResolucion" + pos).val(formato_numero(insresolucion, 5, ".", ",", ""));
        $("#InsUHisteresis" + pos).val(formato_numero(histeresis / Math.sqrt(12), 5, ".", ", ", ""));
        $("#InsUNivel" + pos).val(formato_numero(UEstandarPSI, 9, ".", ", ", ""));
        $("#InsUTempIBC" + pos).val(formato_numero(esttempibc / Math.sqrt(12) , 5, ".", ",", ""));
        $("#InsUTempPat" + pos).val(formato_numero(esttemppat / Math.sqrt(12), 5, ".", ",", ""));
        $("#InsUCalPatron" + pos).val(formato_numero(datapa[0].uexpandida /  2, 7, ".", ", ", ""));
        $("#InsUDeriPatron" + pos).val(formato_numero(datapa[0].derivada / Math.sqrt(12), 7, ".", ", ", ""));
        $("#InsUCombinada" + pos).val(formato_numero(ucombinada, 4, ".", ", ", ""));
                
        //RESULTADOS DE LA MEDICIÓN

        var resgrado = Math.pow(ucombinada, 4) / ((Math.pow(insrepetivilidad, 4) / (numlectura - 1)) + (Math.pow(insresolucion, 4) / 200) +
            (Math.pow(histeresis / Math.sqrt(12), 4) / 200) + (Math.pow(UEstandarPSI, 4) / 200) + (Math.pow(datapa[0].uexpandida / 2, 4) / 200) +
            (Math.pow(datapa[0].derivada / Math.sqrt(12), 4) / 200) + 
            (Math.pow(esttempibc / Math.sqrt(12), 4) / 200) + (Math.pow(esttemppat / Math.sqrt(12), 4) / 200));
        TStuden = Calcular_TStudent(resgrado);
        expandida1 = ucombinada * TStuden;
        resexpandida = expandida1;
        if (expandida1 * Factor < CMC) {

            $.jGrowl("Expandida[" + expandida1 + "] menor a la de nuestro alcance se cambiará por [" + (CMC / Factor) + "]", { life: 2500, theme: 'growl-warning', header: '' });
            expandida1 = CMC / Factor;
        }

        expandida3 = expandida1 / rmulecturaibc * 100;

        $("#ResGrado" + pos).val(formato_numero(resgrado, 5, ".", ",", ""));
        $("#ResFactor" + pos).val(formato_numero(TStuden, _DE,_CD,_CM,""));
        $("#ResUExpandida1" + pos).val(formato_numero(expandida1, 5, ".", ",", ""));
        $("#ResUExpandida1" + pos).attr("title", formato_numero(resexpandida, 5, ".", ",", ""));
        $("#ResUExpandida2" + pos).val(formato_numero(expandida1, calculardecimal(expandida1), ".", ",", ""));
        $("#ResUExpandida3" + pos).val(formato_numero(expandida3, 5, ".", ",", ""));
        $("#ResUExpandida4" + pos).val(formato_numero(expandida3, calculardecimal(expandida3), ".", ",", ""));

        $("#RMUUExpandidaIBC" + pos).val(formato_numero(expandida1, calculardecimal(expandida1), ".", ",", ""));
        $("#RMUUExpandidaSI" + pos).val(formato_numero(expandida1 * FactorSI, calculardecimal(expandida1 * FactorSI), ".", ",", ""));

        //Evaluación del teorema del límite central
        $("#EvaURepetibilidad" + pos).val(formato_numero(insrepetivilidad, 5, ".", ",", ""));
        $("#EvaUHisteresis" + pos).val(formato_numero(histeresis / Math.sqrt(12), 5, ".", ",", ""));
        $("#EvaUResolucion" + pos).val(formato_numero(insresolucion, 5, ".", ",", ""));
        $("#EvaUTempIBC" + pos).val(formato_numero(esttempibc / Math.sqrt(12), 5, ".", ",", ""));
        $("#EvaNivel" + pos).val(formato_numero(UEstandarPSI, 5, ".", ",", ""));
        $("#EvaCalPatron" + pos).val(formato_numero(datapa[0].uexpandida / Factor / 2, 5, ".", ",", ""));
        $("#EvaDeriPatron" + pos).val(formato_numero(datapa[0].derivada / Factor / Math.sqrt(12), 5, ".", ",", ""));
        $("#EvaTempPatron" + pos).val(formato_numero(esttemppat / Math.sqrt(12), 5, ".", ",", ""));

        posmayor = 0;
        valormayor = 0;

        if (insrepetivilidad > valormayor) {
            posmayor = 1;
            valormayor = insrepetivilidad;
        } 
        if (histeresis / Math.sqrt(12) > valormayor) {
            posmayor = 2;
            valormayor = histeresis / Math.sqrt(12);
        }
        if (insresolucion > valormayor) {
            posmayor = 3;
            valormayor = insresolucion;
        }
        if (UEstandarPSI > valormayor) {
            posmayor = 5;
            valormayor = UEstandarPSI;
        }
        if (datapa[0].uexpandida / Factor / 2 > valormayor) {
            posmayor = 6;
            valormayor = datapa[0].uexpandida / Factor / 2;
        }
        if (datapa[0].derivada / Factor / Math.sqrt(12) > valormayor) {
            posmayor = 7;
            valormayor = datapa[0].derivada / Factor / Math.sqrt(12);
        }

        sumacuadrado = Math.pow(insrepetivilidad, 2) + Math.pow(histeresis / Math.sqrt(12), 2) + Math.pow(insresolucion, 2) +
            Math.pow(UEstandarPSI, 2) + Math.pow(datapa[0].uexpandida / Factor / 2, 2) + Math.pow(datapa[0].derivada / Factor / Math.sqrt(12), 2) - Math.pow(valormayor, 2);
        evaluador = Math.sqrt(sumacuadrado) / valormayor;
                    
        formulamax = $("#EvaFormula" + posmayor).html();
        figuramax = $("#EvaFigura" + posmayor).html();
        formulamax = $.trim(formulamax).replace('</sub>', '').replace('<sub>', '').replace('</i>', '').replace('<i>', '');
        dominante = (evaluador <= 0.3 ? "Si" : "No");
        
        $("#EvaUMaxima1" + pos).val(formato_numero(valormayor, 5, ".", ",", ""));
        $("#EvaUMaxima2" + pos).val(formulamax);
        $("#EvaDistribucion" + pos).val(figuramax);
        $("#EvaEvaluador" + pos).val(formato_numero(evaluador, 5, ".", ",", ""));
        $("#EvaDominante" + pos).val(dominante);
                        
    } else {
        if (Caja != "")
            $.jGrowl("Debe seleccionar un patrón", { life: 2500, theme: 'growl-warning', header: '' });
    }

    var porerror = RangoHasta * Clase / 100;
    var poradvertencia = porerror / 2;
            
    
    $("#CorrecionMed" + fila).removeClass("bg-danger");
    $("#CorrecionMed" + fila).removeClass("bg-naranja");
    $("#CorrecionMed" + fila).addClass("bg-default");

    if (porerror > 0) {
        if (Math.abs(correccion) >= Math.abs(poradvertencia) && Math.abs(correccion) < Math.abs(porerror)) {
            $("#CorrecionMed" + fila).addClass("bg-naranja");
            $("#CorrecionMed" + fila).removeClass("bg-danger");
            $("#CorrecionMed" + fila).removeClass("bg-default");
        } else {
            if (Math.abs(correccion) >= Math.abs(porerror)) {
                $("#CorrecionMed" + fila).addClass("bg-danger");
                $("#CorrecionMed" + fila).removeClass("bg-default");
                $("#CorrecionMed" + fila).removeClass("bg-naranja");
            } else {
                $("#CorrecionMed" + fila).removeClass("bg-danger");
                $("#CorrecionMed" + fila).addClass("bg-default");
                $("#CorrecionMed" + fila).removeClass("bg-naranja");
            }
        }
    }

    
    Graficar();
    GraficarDesviacion();
    GraficarBarra();

}


function calculardecimal(numero) {
    numero = $.trim(numero);
    var contador = 0;
    var punto = 0;
    decimal = 0;
    for (var i = 0; i < numero.length; i++) {
        if (punto > 0)
            decimal++;
        if (numero.charAt(i) != "." && numero.charAt(i) != ",") {
            if (numero.charAt(i) * 1 > 0 || contador > 0) {
                contador++;
                if (contador == 2)
                    break;
            }
        } else
            punto++;
    }
    
    return decimal;
}



function Graficar() {
    
    var colores = ['rgb(255, 99, 132)', 'rgb(255, 159, 64)', 'rgb(255, 205, 86)', 'rgb(75, 192, 192)', 'rgb(54, 162, 235)', 'rgb(153, 102, 255)', 'rgb(201, 203, 207)'];
    var varserie = 0;
    var TablaGrafica = "";

    var config = {
        type: 'scatter',
        data: {
            datasets: []
        },
        options: {
            responsive: true,
            title: {
                display: true,
                text: 'Corrección de la medición en (' + $("#Medida").html() + ') VS Lectura en (' + $("#Medida").html() + ')'
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            legend: {
                position: 'right',
                label: {
                    FontSize: '20px'
                }
            }
        },
        lineAtIndex: 2
    }

    TablaGrafica += '<tr class="tabcertr"><th class="bg-gris">EPM(+)</th>';
    $("#TituloLinea1").html('Corrección de la medición en (' + $("#Medida").html() + ') VS Lectura en (' + $("#Medida").html() + ')')
    
    var newDataset = {
        label: 'EPM(+)',
        fill: false,
        backgroundColor: colores[0],
        borderColor: colores[0],
        borderDash: [5, 5],
        data: [],
    };
    var punto;
    for (var i = 1; i <= 6; i++) {
        TablaGrafica += "<td>" + NumeroDecimal($("#Lectura_A1" + i).val()) + " ; " + DesMax + "</td>"; 
        punto = {
            x: NumeroDecimal($("#Lectura_A1" + i).val()),
            y: DesMax
        }
        newDataset.data.push(punto);
    }
    config.data.datasets.push(newDataset);

   

    var cantpuntos = Clase > 0.25 ? 2 : 3;

    TablaGraficaAsc = "";
    TablaGraficaDes = "";

    for (var p = 1; p <= cantpuntos; p++) {
        TablaGraficaAsc += '<tr class="tabcertr"><th class="bg-gris">Ascenso_' + p + '</th>';
        var newDataset = {
            label: 'Ascenso ' + p,
            fill: false,
            backgroundColor: colores[0+p],
            borderColor: colores[0+p],
            data: [],
        };
        var punto;

        for (var i = 1; i <= 6; i++) {

            TablaGraficaAsc += "<td>" + NumeroDecimal($("#Lectura_A" + p +  i).val()) + " ; " + NumeroDecimal($("#CorrecionMed_A" + p +  i).val()) + "</td>";
            punto = {
                x: NumeroDecimal($("#Lectura_A" + p +  i).val()),
                y: NumeroDecimal($("#CorrecionMed_A" + p +  i).val())
            }
            newDataset.data.push(punto);
        }
        config.data.datasets.push(newDataset);

        TablaGraficaDes += '<tr class="tabcertr"><th class="bg-gris">Descenso_' + p + '</th>';
        var newDataset = {
            label: 'Descenso ' + p,
            fill: false,
            backgroundColor: colores[3 + p],
            borderColor: colores[3 + p],
            data: [],
        };
        var punto;

        for (var i = 6; i >= 1; i--) {

            TablaGraficaDes += "<td>" + NumeroDecimal($("#Lectura_D" + p +  i).val()) + " ; " + NumeroDecimal($("#CorrecionMed_D" + p +  i).val()) + "</td>";
            punto = {
                x: NumeroDecimal($("#Lectura_D" + p + i).val()),
                y: NumeroDecimal($("#CorrecionMed_D" + p + i).val())
            }
            newDataset.data.push(punto);
        }
        config.data.datasets.push(newDataset);
        TablaGraficaAsc += "</tr>";
        TablaGraficaDes += "</tr>";
        TablaGrafica += TablaGraficaAsc + TablaGraficaDes;
    }
              

    TablaGrafica += '<tr class="tabcertr"><th class="bg-gris">EPM(-)</th>';
    
    newDataset = {
        label: 'EPM(-)',
        fill: false,
        borderColor: colores[0],
        backgroundColor: colores[0],
        borderDash: [5, 5],
        data: [],
    };
    for (var i = 1; i <= 6; i++) {
        TablaGrafica += "<td>" + NumeroDecimal($("#Lectura_A1" + i).val()) + " ; " + DesMin + "</td>"; 
        punto = {
            x: NumeroDecimal($("#Lectura_A1" + i).val()),
            y: DesMin
        }
        newDataset.data.push(punto);
    }
    TablaGrafica += "</tr>";
    config.data.datasets.push(newDataset);

    if (window.myLine != undefined) {
        window.myLine.destroy();
    }

    window.myLine = new Chart(ctxgra, config);   
    window.myLine.update();

    $("#TBGrafica1").html(TablaGrafica);

    

}

function GraficarBarra() {

    var colores = ['rgb(255, 99, 132)', 'rgb(255, 159, 64)', 'rgb(255, 205, 86)', 'rgb(75, 192, 192)', 'rgb(54, 162, 235)', 'rgb(153, 102, 255)', 'rgb(201, 203, 207)', 'rgb(104, 255, 51)', 'rgb(255, 60, 51)', 'rgb(8, 1, 0)'];
    var formula = ['', 'u(σ)', 'u(δRes)', 'u(δHis)', 'u(δTem)', 'u(ΔNR)', 'u(δCal)', 'u(δDer)', 'u(δTem)']
    var datos = ['', 'InsURepetibilidad', 'InsUResolucion', 'InsUHisteresis', 'InsUTempIBC', 'InsUNivel', 'InsUCalPatron', 'InsUDeriPatron', 'InsUTempPat']
    var lecturas = [];

    for (var i = 1; i <= 6; i++) {
        punto = NumeroDecimal($("#Lectura_A1" + i).val());
        lecturas.push(punto);
    }


    var config = {
        type: 'bar',
        data: {
            labels: lecturas,
            datasets: []
        },
        options: {
            responsive: true,
            title: {
                display: true,
                text: ''
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            legend: {
                position: 'right',
                label: {
                    FontSize: '20px'
                }
            }
        },
        lineAtIndex: 2
    }

        
    
    for (var p = 1; p <= 8; p++) {
        var newDataset = {
            label: formula[p],
            backgroundColor: colores[p],
            borderColor: colores[p],
            data: [],
        };
        var punto;

        for (var i = 1; i <= 6; i++) {
            punto = NumeroDecimal($("#" + datos[p] + i).val());
            newDataset.data.push(punto);
        }
        config.data.datasets.push(newDataset);
    }

    if (window.myBarra != undefined) {
        window.myBarra.destroy();
    }
        
    window.myBarra = new Chart(ctbarra, config);
    window.myBarra.update();


}

function GraficarDesviacion() {

    var colores = ['rgb(255, 99, 132)', 'rgb(255, 159, 64)', 'rgb(255, 205, 86)', 'rgb(75, 192, 192)', 'rgb(54, 162, 235)', 'rgb(153, 102, 255)', 'rgb(201, 203, 207)', 'rgb(156, 156, 156)'];
    var varserie = 0;
    var TablaGrafica;
    $("#TituloLinea1").html('Diagrama de calibración, Corrección ± Incertidumbre (' + $("#Medida").html() + ') VS Valor Nominal (' + $("#Medida").html() + ')');
    var config = {
        type: 'scatter',
        data: {
            datasets: []
        },
        options: {
            legendCallback: function (chart) {
                '<b>Prueba de leyenda en html</b>'
            },
            
            responsive: true,
            
            title: {
                display: true,
                text: 'Diagrama de calibración, Corrección ± Incertidumbre (' + $("#Medida").html() + ') VS Valor Nominal (' + $("#Medida").html() + ')'
            },
            scales: {
                xAxes: [{
                    display: true,
                    labelString: 'Par torsional nominal en lbf·ft'

                }],
                yAxes: [{
                    display: true,
                    labelString: 'Desviación ± U expandida en %'
                }]
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            legend: {
                position: 'right'
            }
        },
        lineAtIndex: 2
    }

    TablaGrafica += '<tr class="tabcertr"><th class="bg-gris">EPM(+)</th>';

    var newDataset = {
        label: 'EPM(+)',
        fill: false,
        backgroundColor: colores[0],
        borderColor: colores[0],
        borderDash: [5, 5],
        data: [],
    };
    var punto;
    for (var i = 1; i <= 6; i++) {
        TablaGrafica += "<td>" + NumeroDecimal($("#Lectura_A1" + i).val()) + " ; " + DesMax + "</td>";
        punto = {
            x: NumeroDecimal($("#Lectura_A1" + i).val()),
            y: DesMax
        }
        newDataset.data.push(punto);
    }
    config.data.datasets.push(newDataset);
    
    TablaGrafica += "</tr>";
    TablaGrafica += '<tr class="tabcertr"><th class="bg-gris">Corrección</th>';

    var newDataset = {
        label: 'Corrección',
        fill: false,
        backgroundColor: colores[4],
        borderColor: colores[4],
        data: [],
    };
    var punto;
    for (var i = 1; i <= 6; i++) {
        TablaGrafica += "<td>" + NumeroDecimal($("#Lectura_A1" + i).val()) + " ; " + NumeroDecimal($("#RMUCorreccionIBC" + i).val()) + "</td>"; 
        punto = {
            x: NumeroDecimal($("#Lectura_A1" + i).val()),
            y: NumeroDecimal($("#RMUCorreccionIBC" + i).val())
        }
        newDataset.data.push(punto);
    }
    config.data.datasets.push(newDataset);

    TablaGrafica += "</tr>";

    TablaGrafica += '<tr class="tabcertr"><th class="bg-gris">EPM(-)</th>';

    var newDataset = {
        label: 'EPM(+)',
        fill: false,
        backgroundColor: colores[0],
        borderColor: colores[0],
        borderDash: [5, 5],
        data: [],
    };
    var punto;
    for (var i = 1; i <= 6; i++) {
        TablaGrafica += "<td>" + NumeroDecimal($("#Lectura_A1" + i).val()) + " ; " + DesMin + "</td>";
        punto = {
            x: NumeroDecimal($("#Lectura_A1" + i).val()),
            y: DesMin
        }
        newDataset.data.push(punto);
    }
    config.data.datasets.push(newDataset);

    TablaGrafica += "</tr>";

    if (window.myDesviacion != undefined) {
        window.myDesviacion.destroy();
    }

    window.myDesviacion = new Chart(ctdesviacion, config);
    window.myDesviacion.update();

   
    for (var fila = 1; fila <= 6; fila++) {
        newDataset = {
            display: false,
            label:'Error',
            borderColor: colores[7],
            backgroundColor: colores[7],
            data: [],
        };
        varserie = NumeroDecimal($("#RMUCorreccionIBC" + fila).val()) - NumeroDecimal($("#ResUExpandida1" + fila).val());
        punto = {
            x: NumeroDecimal($("#Lectura_A1" + fila).val()),
            y: varserie
        }
        newDataset.data.push(punto);

        punto = {
            x: NumeroDecimal($("#Lectura_A1" + fila).val()),
            y: NumeroDecimal($("#RMUCorreccionIBC" + fila).val())
        }
        newDataset.data.push(punto);

        varserie = NumeroDecimal($("#RMUCorreccionIBC" + fila).val()) + NumeroDecimal($("#ResUExpandida1" + fila).val());
        punto = {
            x: NumeroDecimal($("#Lectura_A1" + fila).val()),
            y: varserie
        }
        newDataset.data.push(punto);

        config.data.datasets.push(newDataset);
    }
    

    if (window.myDesviacion != undefined) {
        window.myDesviacion.destroy();
    }

    window.myDesviacion = new Chart(ctdesviacion, config);
    window.myDesviacion.update();

    $("#TBGrafica2").html(TablaGrafica);
    

}


function CambioPatron(patron) {

    $("#PDescripcion").html("");
    $("#PMarca").html("");
    $("#PModelo").html("");
    $("#PCertificado").html("");
    $("#PFechaCali").html("");
    $("#PProxima").html("");
    $("#PSerie").html("");
    $("#PPrecision").html("");
    $("#PDerivada").html("");
    PUPatron = 0;
    $("#PUPatron").html("");

    $("#PatCoe0").html("");
    $("#PatCoe1").html("");
    $("#PatCoe2").html("");
    $("#PatCoe3").html("");
    $("#PatCoe4").html("");
    $("#PatCoe5").html("");

    if (patron * 1 == 0)
        return false;
    if (Ingreso == 0)
        return false;

    ActivarLoad();
    setTimeout(function () {
        var datos = LlamarAjax("Laboratorio/DatosPatron", "patron=" + patron);
        var data = JSON.parse(datos);
        if (data[0].alcance < RangoHasta * Factor) {
            $("#RangoPatron").focus();
            swal("Acción Cancelada", "La lectura máxima supera el alcance máximo del patrón", "warning");
            $("#RangoPatron").val("").trigger("change");
        }

        Alcance = data[0].alcance;


        $("#PDescripcion").html(data[0].descripcion);
        $("#PMarca").html(data[0].marca);
        $("#PModelo").html(data[0].modelo);
        $("#PCertificado").html(data[0].certificado);
        $("#PFechaCali").html(data[0].fechacertificado);
        $("#PProxima").html(data[0].proximacalibracion);
        $("#PSerie").html(data[0].serie);
        $("#PPrecision").html(data[0].precision);
        $("#PDerivada").html(data[0].derivada);
        PUPatron = data[0].maxuexpandida / Factor
        $("#PUPatron").html(formato_numero(PUPatron, 3, ".", ",", ""));

        $("#PatCoe0").html(data[0].a0);
        $("#PatCoe1").html(data[0].a1);
        $("#PatCoe2").html(data[0].a2);
        $("#PatCoe3").html(data[0].a3);
        $("#PatCoe4").html(data[0].a4);
        $("#PatCoe5").html(data[0].a5);

        CalcularTotal();
        DesactivarLoad();
    }, 15);

}


//$.connection.hub.start().done(function () {
$("#FormPrevia").submit(function (e) {
    e.preventDefault();

    if (Certificado != "") {
        swal("Acción Cancelada", "Ya se generó el certificado de este ingreso", "warning");
        return false;
    }

    if (Calculado == 0) {
        swal("Acción Cancelada", "Debe de recalcular los datos", "warning");
        return false;
    }

    if (PresMax == 0 || PresMin == 0) {
        $("#PresMax").focus();
        swal("Acción Cancelada", "Debe agregar la presión admoférica", "warning");
        return false;
    }

    
    LeerCondiciones(1);

    if (TempMax == 0 || TempMin == 100000000 || HumeMax == 0 || HumeMin == 100000000) {
        swal("Acción Cancelada", "No hay condiciones ambientales cargadas", "warning");
        return false;
    }
       

    var mensaje = "";
    if (TempVar > 1 || HumeMax >= 75)
        mensaje = "temperatura y humedad";
    else {
        if (TempVar == 1)
            mensaje = "temperatura";
        if (HumeMax >= 75)
            mensaje = "humedad";
    }

    /*if (mensaje != "") {
        $("#documento").focus();
        swal("Acción Cancelada", "Condiciones de " + mensaje + " no adecuadas, para la calibración... No podrá General el certificado", "warning");
        EscucharMensaje("Condiciones de " + mensaje + " no adecuadas, para la calibración. No podrá General el certificado");
        return false;
    }*/

           
    ActivarLoad();
    setTimeout(function () {

        d = new Date();
        HoraFinal = (d.getHours() * 1 < 10 ? "0" : "") + d.getHours() + ":" + (d.getMinutes() * 1 < 10 ? "0" : "") + d.getMinutes();
        
        tiempo = RestarHoras(HoraInicio, HoraFinal);
        var parametros = $("#FormPrevia").serialize() + "&id=" + IdCalibracion + "&HoraIni=" + HoraInicio + "&HoraFinal=" + HoraFinal + "&TiempoTrans=" + tiempo + "&magnitud=2" +
            "&FechaRec=" + $("#FechaIng").html() + "&Factor=" + Factor + "&revision=" + Revision + "&Clase=" + Clase + "&CoPresVar=" + $("#VarPres").html() + "hPa" + 
            "&CoPresMax=" + $("#CoPresMax").val() + "&CoPresMin=" + $("#CoPresMin").val() +
            "&PresMax=" + $("#PresMax").val() + "&PresMin=" + $("#PresMin").val() + "&RangoHasta=" + RangoHasta +
            "&co0=" + $("#PatCoe0").html() + "&co1=" + $("#PatCoe1").html() + "&co2=" + $("#PatCoe2").html() + "&co3=" + $("#PatCoe3").html() + "&co4=" + $("#PatCoe4").html() + "&co5=" + $("#PatCoe5").html() +
            "&Metodo=" + UMetodo + "&Incertidumbre=" + IncMetodo;
            

        var datos = LlamarAjax("Laboratorio/GuardaCertificadoPre1", parametros).split("|");
        DesactivarLoad();
        if (datos[0] == "0") {
            if (IdCalibracion == 0) {
                $("#Certificado").html(datos[3]);
                parametros = "tempmax=" + $("#CoTempMax").val().replace(".", ",") + "&tempmin=" + $("#CoTempMin").val().replace(".", ",") + "&tempvar=" + formato_numero(TempVar, 1, ",", ".", "") + " °C" +
                    "&humemax=" + $("#CoHumeMax").val().replace(".", ",") + "&humemin=" + $("#CoHumeMin").val().replace(".", ",") + "&humevar=" + formato_numero(HumeVar, 1, ",", ".", "") + " %HR&ingreso=" + Ingreso + "&numero=" + NumCertificado + "&revision=" + Revision;
                LlamarAjax("Laboratorio/GuardarTemperatura", parametros);
            }
                
            IdCalibracion = datos[2];
            clearInterval(IntervaloTiempo);
            $("#CaTiempo").val(tiempo);
            $("#CaHoraFin").val(HoraFinal);
            localStorage.removeItem(NumCertificado + "DID159" + Ingreso);
            localStorage.removeItem(NumCertificado + "DIDDATOS" + Ingreso);
            GuardaTemp = 0;
            

            swal("", datos[1], "success");
        } else {
            swal("Acción Cancelada", datos[1], "warning");
        }
    }, 15);
});


function TablaCMC() {
    var datos = LlamarAjax("Laboratorio/TablaCMC", "magnitud=2");
    var resultado = "";
    var data = JSON.parse(datos);
    for (var x = 0; x < data.length; x++) {
        resultado += "<tr>" +
            "<td>></td>" +
            "<td align='center'>" + data[x].desde2 + "<br>" + data[x].desde + "</td>" +
            "<td align='center'>" + data[x].hasta2 + "<br>" + data[x].hasta + "</td>" +
            "<td align='center'>" + data[x].medida2 + "<br>" + data[x].medida + "</td>" +
            "<td align='center'>" + data[x].valor2 + "<br>" + data[x].valor + "</td></tr>";
    }
    $("#TBPublicadaONAC").html(resultado);
}

function ConfigurarTablas(can) {
    var resultado = "";
    var resultado2 = "";
    var resultado3 = "";
    var resultado4 = "";
    var resultado5 = "";
    var resultado6 = "";
    var resultado7 = "";
    var resultado8 = "";
    var resultado9 = "";
    var resultado10 = "";

    for (var x = 1; x <= can; x++) {
        resultado += '<tr class="tabcertr">' +
            '<th class="text-center text-XX2"><input type="text" name="EstPresion" id="EstPresion' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-info text-XX2"> <input type="text" name="EstDesviacion" id="EstDesviacion' + x + '" readonly class="form-control sinbordecon text-info text-XX2 text-center" /></th>' +
            '<th class="text-center text-info text-XX2"> <input type="text" name="EstHisteresis" id="EstHisteresis' + x + '" readonly class="form-control sinbordecon text-info text-XX2 text-center" /></th > ' +
            '<th class="text-center text-info text-XX2"> <input type="text" name="EstHisteresis1" id="EstHisteresis1' + x + '" readonly class="form-control sinbordecon text-info text-XX2 text-center" /></th > ' +
            '<th class="text-center text-info text-XX2"> <input type="text" name="EstHisteresis2" id="EstHisteresis2' + x + '" readonly class="form-control sinbordecon text-info text-XX2 text-center" /></th > ' +
            '<th class="text-center text-info text-XX2"> <input type="text" name="EstHisteresis3" id="EstHisteresis3' + x + '" readonly class="form-control sinbordecon text-info text-XX2 text-center" /></th > ' +
            '<th class="text-center text-info text-XX2"> <input type="text" name="EstResolucion" id="EstResolucion' + x + '" readonly class="form-control sinbordecon text-info text-XX2 text-center" /></th > ' +
            '<th class="text-center text-info text-XX2"> <input type="text" name="EstUPatron" id="EstUPatron' + x + '" readonly class="form-control sinbordecon text-info text-XX2 text-center" /></th > ' +
            '<th class="text-center text-info text-XX2"> <input type="text" name="EstDerivada" id="EstDerivada' + x + '" readonly class="form-control sinbordecon text-info text-XX2 text-center" /></th > ' +
            '<th class="text-center text-info text-XX2"> <input type="text" name="EstTempPatron" id="EstTempPatron' + x + '" readonly class="form-control sinbordecon text-info text-XX2 text-center" /></th > ' +
            '<th class="text-center text-info text-XX2"> <input type="text" name="EstTempIBC" id="EstTempIBC' + x + '" readonly class="form-control sinbordecon text-info text-XX2 text-center" /></th > ' +
            '<th class="text-center text-info text-XX2"> <input type="text" name="EstErrorMaxPos" id="EstErrorMaxPos' + x + '" readonly class="form-control sinbordecon text-info text-XX2 text-center" /></th > ' +
            '<th class="text-center text-info text-XX2"><input type="text" name="EstErrorMaxNeg" id="EstErrorMaxNeg' + x + '" readonly class="form-control sinbordecon text-info text-XX2 text-center" /></th>' +            
            '</tr>';

        resultado2 += '<tr class="tabcertr">' +
            '<th class="text-center text-XX2"><input type="text" name="RMUPrecionIBC" id="RMUPrecionIBC' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-info text-XX2"> <input type="text" name="RMULecuraPatIBC" id="RMULecuraPatIBC' + x + '" readonly class="form-control sinbordecon text-XX2 text-center" /></th>' +
            '<th class="text-center text-info text-XX2"> <input type="text" name="RMULecuraIBC" id="RMULecuraIBC' + x + '" readonly class="form-control sinbordecon text-XX2 text-center" /></th > ' +
            '<th class="text-center text-info text-XX2"> <input type="text" name="RMUCorreccionIBC" id="RMUCorreccionIBC' + x + '" readonly class="form-control sinbordecon text-XX2 text-center" /></th > ' +
            '<th class="text-center text-info text-XX2"> <input type="text" name="RMUUExpandidaIBC" id="RMUUExpandidaIBC' + x + '" readonly class="form-control sinbordecon text-XX2 text-center" /></th > ' +
            '<th class="text-center text-XX2"><input type="text" name="RMUPrecionSI" id="RMUPrecionSI' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-info text-XX2"> <input type="text" name="RMULecuraPatSI" id="RMULecuraPatSI' + x + '" readonly class="form-control sinbordecon text-XX2 text-center" /></th>' +
            '<th class="text-center text-info text-XX2"> <input type="text" name="RMULecuraSI" id="RMULecuraSI' + x + '" readonly class="form-control sinbordecon text-XX2 text-center" /></th > ' +
            '<th class="text-center text-info text-XX2"> <input type="text" name="RMUCorreccionSI" id="RMUCorreccionSI' + x + '" readonly class="form-control sinbordecon text-XX2 text-center" /></th > ' +
            '<th class="text-center text-info text-XX2"> <input type="text" name="RMUUExpandidaSI" id="RMUUExpandidaSI' + x + '" readonly class="form-control sinbordecon text-XX2 text-center" /></th > ' +
            '</tr>';
        resultado3 += '<tr class="tabcertr">' +
            '<th class="text-center text-XX2"><input type="text" name="ResGrado" id="ResGrado' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"> <input type="text" name="ResFactor" id="ResFactor' + x + '" readonly class="form-control sinbordecon text-XX2 text-center" /></th>' +
            '<th class="text-center text-XX2"> <input type="text" name="ResUExpandida1" id="ResUExpandida1' + x + '" readonly class="form-control sinbordecon  text-XX2 text-center" /></th > ' +
            '<th class="text-center text-info text-XX2"> <input type="text" name="ResUExpandida2" id="ResUExpandida2' + x + '" readonly class="form-control sinbordecon text-info text-XX2 text-center" /></th > ' +
            '<th class="text-center text-XX2"> <input type="text" name="ResUExpandida3" id="ResUExpandida3' + x + '" readonly class="form-control sinbordecon text-XX2 text-center" /></th > ' +
            '<th class="text-center text-info text-XX2"> <input type="text" name="ResUExpandida4" id="ResUExpandida4' + x + '" readonly class="form-control sinbordecon text-info text-XX2 text-center" /></th > ' +
            '</tr>';
        resultado4 += '<tr class="tabcertr">' +
            '<th class="text-center text-XX2"><input type="text" name="InsPresion" id="InsPresion' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-info text-XX2"> <input type="text" name="InsURepetibilidad" id="InsURepetibilidad' + x + '" readonly class="form-control sinbordecon text-XX2 text-center" /></th>' +
            '<th class="text-center text-info text-XX2"> <input type="text" name="InsUResolucion" id="InsUResolucion' + x + '" readonly class="form-control sinbordecon text-XX2 text-center" /></th > ' +
            '<th class="text-center text-info text-XX2"> <input type="text" name="InsUHisteresis" id="InsUHisteresis' + x + '" readonly class="form-control sinbordecon text-XX2 text-center" /></th > ' +
            '<th class="text-center text-info text-XX2"> <input type="text" name="InsUTempIBC" id="InsUTempIBC' + x + '" readonly class="form-control sinbordecon text-XX2 text-center" /></th > ' +
            '<th class="text-center text-info text-XX2"> <input type="text" name="InsUNivel" id="InsUNivel' + x + '" readonly class="form-control sinbordecon text-XX2 text-center" /></th > ' +
            '<th class="text-center text-info text-XX2"> <input type="text" name="InsUCalPatron" id="InsUCalPatron' + x + '" readonly class="form-control sinbordecon text-XX2 text-center" /></th > ' +
            '<th class="text-center text-info text-XX2"> <input type="text" name="InsUDeriPatron" id="InsUDeriPatron' + x + '" readonly class="form-control sinbordecon text-XX2 text-center" /></th > ' +
            '<th class="text-center text-info text-XX2"> <input type="text" name="InsUTempPat" id="InsUTempPat' + x + '" readonly class="form-control sinbordecon text-XX2 text-center" /></th > ' +
            '<th class="text-center text-info text-XX2"> <input type="text" name="InsUCombinada" id="InsUCombinada' + x + '" readonly class="form-control sinbordecon text-XX2 text-center" /></th > ' +
            '</tr>';
        resultado5 += '<tr class="tabcertr">' +
            '<th class="text-center text-XX2"><input type="text" name="PaLecturaMed" id="PaLecturaMed' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="PaLecturaCon" id="PaLecturaCon' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"> <input type="text" name="PaUPatronCon" id="PaUPatronCon' + x + '" readonly class="form-control sinbordecon text-XX2 text-center" /></th>' +
            '<th class="text-center text-XX2"> <input type="text" name="PaUPatronMed" id="PaUPatronMed' + x + '" readonly class="form-control sinbordecon text-XX2 text-center" /></th > ' +
            '<th class="text-center text-XX2"> <input type="text" name="PaDerivadaCon" id="PaDerivadaCon' + x + '" readonly class="form-control sinbordecon text-XX2 text-center" /></th > ' +
            '<th class="text-center text-XX2"> <input type="text" name="PaDerivadaMed" id="PaDerivadaMed' + x + '" readonly class="form-control sinbordecon text-XX2 text-center" /></th > ' +
            '</tr>';
        resultado6 += '<tr class="tabcertr">' +
            '<th class="text-center text-XX2"><input type="text" name="EvaURepetibilidad" id="EvaURepetibilidad' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="EvaUHisteresis" id="EvaUHisteresis' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="EvaUResolucion" id="EvaUResolucion' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="EvaUTempIBC" id="EvaUTempIBC' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="EvaNivel" id="EvaNivel' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="EvaCalPatron" id="EvaCalPatron' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="EvaDeriPatron" id="EvaDeriPatron' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="EvaTempPatron" id="EvaTempPatron' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="EvaUMaxima1" id="EvaUMaxima1' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="EvaUMaxima2" id="EvaUMaxima2' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="EvaDistribucion" id="EvaDistribucion' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="EvaEvaluador" id="EvaEvaluador' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '<th class="text-center text-XX2"><input type="text" name="EvaDominante" id="EvaDominante' + x + '" readonly class="form-control sinbordecon text-XX2 text-center"/></th>' +
            '</tr>';
    }
    $("#TBEstimacion").html(resultado);
    $("#TBResultadoMU").html(resultado2);
    $("#TBResultado").html(resultado3);
    $("#TBInsertidumbre").html(resultado4);
    $("#TBPatron").html(resultado5);
    $("#TBEvaluacion").html(resultado6);
}
    
function VerFotos() {
    if (Ingreso == 0)
        return false;
    CargarModalFoto(Ingreso, Foto,1);
}


$('select').select2();

if (document.location.hostname.match(/^(www\.)?chartjs\.org$/)) {
    (function (i, s, o, g, r, a, m) {
    i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
        (i[r].q = i[r].q || []).push(arguments)
    }, i[r].l = 1 * new Date(); a = s.createElement(o),
        m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
    ga('create', 'UA-28909194-3', 'auto');
    ga('send', 'pageview');
}

function CalcularPresion(Caja, tipo) {
    if (Ingreso == 0 || Certificado != "") {
        $("#PresMax").val("");
        $("#PresMin").val("");
        return false;
    }
    if (Caja != "0")
        ValidarTexto(Caja, 3);
    if (tipo == 1) {
        premax = NumeroDecimal($("#PresMax").val());
        MaxPres1 = NumeroDecimal($("#MaxPres1").html());
        MaxPres2 = NumeroDecimal($("#MaxPres2").html());
        MaxPres3 = NumeroDecimal($("#MaxPres3").html());
        MaxPres4 = NumeroDecimal($("#MaxPres4").html());
        ForMaxPres1 = Math.pow(premax, 3);
        ForMaxPres2 = Math.pow(premax, 2);
        ForMaxPres3 = premax;
        ForMaxPres4 = (MaxPres1 * ForMaxPres1) + (MaxPres2 * ForMaxPres2) + (MaxPres3 * ForMaxPres3) + MaxPres4;
                
        $("#ForMaxPres1").html(formato_numero(ForMaxPres1, 3, ".", ",", ""));
        $("#ForMaxPres2").html(formato_numero(ForMaxPres2, 3, ".", ",", ""));
        $("#ForMaxPres3").html(formato_numero(ForMaxPres3, 3, ".", ",", ""));
        $("#ForMaxPres4").html(formato_numero(ForMaxPres4, 3, ".", ",", ""));

        PresMax = ForMaxPres4 + premax;

        $("#ForMaxPres4").html(formato_numero(ForMaxPres4, 3, ".", ",", ""));
        $("#CoPresMax").val(formato_numero(PresMax, 1, ".", ",", "") + " hPa");

        PresVar = Math.abs((PresMax) - (PresMin));
        $("#VarPres").html(formato_numero(PresVar, 3, ".", ", ", ""));
    } else {
        premin = NumeroDecimal($("#PresMin").val());
        MinPres1 = NumeroDecimal($("#MinPres1").html());
        MinPres2 = NumeroDecimal($("#MinPres2").html());
        MinPres3 = NumeroDecimal($("#MinPres3").html());
        MinPres4 = NumeroDecimal($("#MinPres4").html());
        ForMinPres1 = Math.pow(premin, 3);
        ForMinPres2 = Math.pow(premin, 2);
        ForMinPres3 = premin;
        ForMinPres4 = (MinPres1 * ForMinPres1) + (MinPres2 * ForMinPres2) + (MinPres3 * ForMinPres3) + MinPres4;
        
        $("#ForMinPres1").html(formato_numero(ForMinPres1, 3, ".", ",", ""));
        $("#ForMinPres2").html(formato_numero(ForMinPres2, 3, ".", ",", ""));
        $("#ForMinPres3").html(formato_numero(ForMinPres3, 3, ".", ",", ""));
        $("#ForMinPres4").html(formato_numero(ForMinPres4, 3, ".", ",", ""));

        PresMin = ForMinPres4 + premin;

        $("#ForMinPres4").html(formato_numero(ForMinPres4, 3, ".", ",", ""));
        $("#CoPresMin").val(formato_numero(PresMin, 1, ".", ",", "") + " hPa");

        PresVar = Math.abs((PresMax) - (PresMin));
        $("#VarPres").html(formato_numero(PresVar, 3, ".", ", ", ""));
    }
}

function CalcularTemperatura(guardar) {

        
    if (window.myTemp != undefined) {
        window.myTemp.destroy();
    }

    if (window.myHume != undefined) {
        window.myHume.destroy();
    }

    TempMax = 0;
    TempMin = 100000000;
    HumeMax = 0;
    HumeMin = 100000000;
    TempVar = 0;
    HumeVar = 0;
    
    $("#TempMax").val("");
    $("#TempMin").val("");

    $("#HumeMax").val("");
    $("#HumeMin").val("");
        
    $("#ForMinTemp1").html("");
    $("#ForMinTemp2").html("");
    $("#ForMinTemp3").html("");
        
    $("#ForMaxTemp1").html("");
    $("#ForMaxTemp2").html("");
    $("#ForMaxTemp3").html("");
        
    $("#ForMinHume1").html("");
    $("#ForMinHume2").html("");
    $("#ForMinHume3").html("");
        
    $("#ForMaxHume1").html("");
    $("#ForMaxHume2").html("");
    $("#ForMaxHume3").html("");

    $("#CoTempMax").val("");
    $("#CoTempMin").val("");

    $("#CoTempMax").removeClass("bg-danger");
    $("#CoTempMin").removeClass("bg-danger");
    
    $("#CoHumeMax").removeClass("bg-danger");
    $("#CoHumeMin").removeClass("bg-danger");
    
    $("#CoHumeMax").val("");
    $("#CoHumeMin").val("");
       
    //********************************************************

    
    var fechaactual = "";
    var datos = LlamarAjax("Laboratorio/DatosGraTemperatura", "sensor=" + sensor + "&horai=" + HoraInicio + "&horaf=" + (HoraFinal == "" ? HoraCondicion : HoraFinal) + "&fecha=" + (FechaCal == "" ? FechaActual : FechaCal));
    

    /*$.ajax({
        url: 'https://www.webstorage-service.com/data/viewcur.html?bsn=429A00BF&rsn=42BC0232',
        dataType: 'json'
    }).then((resultado) => {
        console(resultado);
    });*/

    if (datos != "[]") {
        var data = JSON.parse(datos);

        var colores = ['rgb(255, 99, 132)', 'rgb(255, 159, 64)', 'rgb(255, 205, 86)', 'rgb(75, 192, 192)', 'rgb(54, 162, 235)', 'rgb(153, 102, 255)', 'rgb(201, 203, 207)'];
        var puntos;
        var newDataset;

        fechaactual = data[0].fecha;

        var config = {
            type: 'scatter',
            data: {
                datasets: []
            },
            options: {
                responsive: true,
                title: {
                    display: true,
                    text: 'Gráfica de Temperatura ' + fechaactual
                },
                scales: {
                    xAxes: [{
                        display: true,
                        labelString: 'Par torsional nominal en lbf·ft'

                    }],
                    yAxes: [{
                        display: true,
                        labelString: 'Desviación ± U expandida en %'
                    }]
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
            }
        }
        var config2 = {
            type: 'scatter',
            data: {
                datasets: []
            },
            options: {
                responsive: true,
                title: {
                    display: true,
                    text: 'Gráfica de Humedad ' + fechaactual
                },
                scales: {
                    xAxes: [{
                        display: true,
                        labelString: 'Par torsional nominal en lbf·ft'

                    }],
                    yAxes: [{
                        display: true,
                        labelString: 'Desviación ± U expandida en %'
                    }]
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
            }
        }

        newDataset = {
            label: 'Temperatura',
            fill: false,
            borderColor: colores[1],
            backgroundColor: colores[1],
            data: [],
        };
        for (var i = 0; i < data.length; i++) {

            if (data[i].hora >= HoraInicio && data[i].hora <= (HoraFinal == "" ? HoraCondicion : HoraFinal)) {

                if (data[i].temperatura > TempMax * 1)
                    TempMax = data[i].temperatura;

                if (data[i].temperatura < TempMin * 1)
                    TempMin = data[i].temperatura;

                puntos = {
                    x: data[i].punto,
                    y: data[i].temperatura
                }
                newDataset.data.push(puntos);
            }
        }

        config.data.datasets.push(newDataset);
        if (TempMin == 100000000)
            TempMin = 0;

        $("#TempMax").html(TempMax);
        $("#TempMin").html(TempMin);

        newDataset = {
            label: 'Humedad',
            fill: false,
            borderColor: colores[4],
            backgroundColor: colores[4],
            data: [],
        };
        for (var i = 0; i < data.length; i++) {
            if (data[i].hora >= HoraInicio && data[i].hora <= (HoraFinal == "" ? HoraCondicion : HoraFinal)) {
                if (data[i].humedad > HumeMax * 1)
                    HumeMax = data[i].humedad;

                if (data[i].humedad < HumeMin * 1)
                    HumeMin = data[i].humedad;

                puntos = {
                    x: data[i].punto,
                    y: data[i].humedad
                }
                newDataset.data.push(puntos);
            }
        }

        if (HumeMin*1 == 100000000)
            HumeMin = 0;

        
        $("#HumeMax").html(HumeMax);
        $("#HumeMin").html(HumeMin);

        config2.data.datasets.push(newDataset);

        window.myTemp = new Chart(gratemp, config);
        window.myTemp.update();

        window.myHume = new Chart(grahume, config2);
        window.myHume.update();

        ForMinTemp1 = TempMin * TempMin;
        ForMinTemp2 = TempMin;
        ForMinTemp3 = (parseFloat($("#MinTemp1").html()) * (TempMin * TempMin)) + (parseFloat($("#MinTemp2").html()) * TempMin) + parseFloat($("#MinTemp3").html());

        $("#ForMinTemp1").html(formato_numero(ForMinTemp1, 1, ".", ",", ""));
        $("#ForMinTemp2").html(formato_numero(ForMinTemp2, 1, ".", ",", ""));
        $("#ForMinTemp3").html(formato_numero(ForMinTemp3, 4, ".", ",", ""));

        ForMaxTemp1 = TempMax * TempMax;
        ForMaxTemp2 = TempMax;
        ForMaxTemp3 = (parseFloat($("#MaxTemp1").html()) * (TempMax * TempMax)) + (parseFloat($("#MaxTemp2").html()) * TempMax) + parseFloat($("#MaxTemp3").html());

        $("#ForMaxTemp1").html(formato_numero(ForMaxTemp1, 1, ".", ",", ""));
        $("#ForMaxTemp2").html(formato_numero(ForMaxTemp2, 1, ".", ",", ""));
        $("#ForMaxTemp3").html(formato_numero(ForMaxTemp3, 4, ".", ",", ""));

        ForMinHume1 = HumeMin * HumeMin;
        ForMinHume2 = HumeMin;
        ForMinHume3 = (parseFloat($("#MinHume1").html()) * (HumeMin * HumeMin)) + (parseFloat($("#MinHume2").html()) * HumeMin) + parseFloat($("#MinHume3").html());

        $("#ForMinHume1").html(formato_numero(ForMinHume1, 1, ".", ",", ""));
        $("#ForMinHume2").html(formato_numero(ForMinHume2, 1, ".", ",", ""));
        $("#ForMinHume3").html(formato_numero(ForMinHume3, 4, ".", ",", ""));

        ForMaxHume1 = HumeMax * HumeMax;
        ForMaxHume2 = HumeMax;
        ForMaxHume3 = (parseFloat($("#MaxHume1").html()) * (HumeMax * HumeMax)) + (parseFloat($("#MaxHume2").html()) * HumeMax) + parseFloat($("#MaxHume3").html());

        $("#ForMaxHume1").html(formato_numero(ForMaxHume1, 1, ".", ",", ""));
        $("#ForMaxHume2").html(formato_numero(ForMaxHume2, 1, ".", ",", ""));
        $("#ForMaxHume3").html(formato_numero(ForMaxHume3, 4, ".", ",", ""));

        $("#CoTempMax").val(formato_numero(TempMax + ForMaxTemp3, 1, ".", ",", "") + " °C");
        $("#CoTempMin").val(formato_numero(TempMin + ForMinTemp3, 1, ".", ",", "") + " °C");
        TempVar = Math.abs((TempMax + ForMaxTemp3) - (TempMin + ForMinTemp3));
        HumeVar = Math.abs((HumeMax + ForMaxHume3) - (HumeMin + ForMinHume3));
        if (TempVar >= 1) {
            $("#CoTempMax").addClass("bg-danger");
            $("#CoTempMin").addClass("bg-danger");
            ErrorTemp = 1;
                       

        } else {
            ErrorTemp = 0;
            $("#CoTempMax").removeClass("bg-danger");
            $("#CoTempMin").removeClass("bg-danger");
            ErrorTemp = 0;
        }

        if (((HumeMax + ForMaxHume3) >= 75) || ((HumeMin + ForMinHume3) >= 75)) {
            $("#CoHumeMax").addClass("bg-danger");
            $("#CoHumeMin").addClass("bg-danger");
            ErrorHume = 1;
        } else {
            $("#CoHumeMax").removeClass("bg-danger");
            $("#CoHumeMin").removeClass("bg-danger");
            ErrorHume = 0;
        }

        $("#CoHumeMax").val(formato_numero(HumeMax + ForMaxHume3, 1, ".", ",", "") + " %HR");
        $("#CoHumeMin").val(formato_numero(HumeMin + ForMinHume3, 1, ".", ",", "") + " %HR");
        
        $("#VarTemp").html(formato_numero(TempVar, 1, ".", ",", ""));
        $("#VarHume").html(formato_numero(HumeVar, 1, ".", ",", ""));
        
        if (guardar == 1) {
            var parametros = "tempmax=" + $("#CoTempMax").val().replace(".", ",") + "&tempmin=" + $("#CoTempMin").val().replace(".", ",") + "&tempvar=" + formato_numero(TempVar, 1, ",", ".", "") + " °C" +
                "&humemax=" + $("#CoHumeMax").val().replace(".", ",") + "&humemin=" + $("#CoHumeMin").val().replace(".", ",") + "&humevar=" + formato_numero(HumeVar, 1, ",", ".", "") + " %HR&ingreso=" + Ingreso + "&numero=" + NumCertificado + "&revision=" + Revision;

            LlamarAjax("Laboratorio/GuardarTemperatura", parametros);
        }
                
        /*if (Certificado == "") {
            if (ErrorTemp == 0 && ErrorHume == 0) {
                /*if (guardar == 1) {
                    EscucharMensaje("Condiciones de temperatura y humedad adecuadas para la calibración... Ya puede General el certificado");
                    $.jGrowl("Condiciones de temperatura y humedad adecuadas para la calibración... Ya puede General el certificado", { life: 5500, theme: 'growl-success', header: '' });
                } else {
                    $.jGrowl("Condiciones de temperatura y humedad adecuadas para la calibración", { life: 5500, theme: 'growl-success', header: '' });
                }
                
            } else {
                var mensaje = "";
                if (ErrorTemp == 1 && ErrorHume == 1)
                    mensaje = "temperatura y humedad";
                else {
                    if (ErrorTemp == 1)
                        mensaje = "temperatura";
                    else
                        mensaje = "humedad";
                }
                EscucharMensaje("Condiciones de " + mensaje + " no adecuadas, para la calibración. No podrá General el certificado");

                swal("Acción Cancelada", "Condiciones de " + mensaje + " no adecuadas, para la calibración... No podrá General el certificado","warning");
                $("#documento").focus();
            }
        }*/
    }
}

function VerCondiciones() {
    $("#documento").focus();
}

function AbrirGrafica() {
    var direccion = $("#medidor").val();
    window.open(direccion);

}

function CargarTemperatura() {
    if (Ingreso == 0)
        return
    /*if (GuardaTemp == 1) {
        swal("Acción Cancelada", "Debe primero guardar la calibración", "warning");
        return false;
    }*/

    if (Certificado != "") {
        swal("Acción Cancelada", "No se puede editar la temperatura de un certificado", "warning");
        return false;
    }
        
    var documento = $("#documento").val();
    if (documento == "") {
        $("#documento").focus();
        swal("Acción Cancelada", "Debe de seleccionar el archivo CSV de temperatura", "warning");
        return false;
    }

    d = new Date();
    HoraFinal = (d.getHours() * 1 < 10 ? "0" : "") + d.getHours() + ":" + (d.getMinutes() * 1 < 10 ? "0" : "") + d.getMinutes();
    $("#CaHoraFin").val(HoraFinal);

    ActivarLoad();
    setTimeout(function () {
        var parametros = "ingreso=" + Ingreso + "&numero=" + NumCertificado + "&revision=" + Revision;
        var datos = LlamarAjax("Laboratorio/Temperatura", parametros);
        DesactivarLoad();
        datos = datos.split("|");
        if (datos[0] == "0") {
            swal("", ValidarTraduccion(datos[1]), "success");
            $("#documento").val("");
            CalcularTemperatura(1);
        } else {
            $("#documento").val("");
            swal("Acción Cancelada", ValidarTraduccion(datos[1]), "warning");
        }
    }, 15);
}

function ImprimirCertificado(tipo) {
    if (Ingreso == 0)
        return false;

    if (tipo == 2 && FirmaCertificado == "DIGITAL") {
        swal("Acción Cancelada", "No se puede imprimir un certificado con firma manual a este cliente", "warning");
        return false;
    }

    if (Certificado == "") {
        swal("Acción Cancelada", "Debe generar primero el certificado", "warning");
        return false;
    }


    ActivarLoad();
    setTimeout(function () {
        var parametros = "ingreso=" + Ingreso + "&numero=" + NumCertificado + "&revision=" + Revision + "&tipo=" + tipo;
        var datos = LlamarAjax("RpPlantillas/RpPlanillaPresion1", parametros);
        DesactivarLoad();
        datos = datos.split("|");
        if (datos[0] == "0") {
            window.open(url + "DocumPDF/" + datos[1]);
        } else {
            if (datos[0] == "9")
                window.open(url + "Certificados/" + datos[1] + ".pdf");
            else
                swal("", ValidarTraduccion(datos[1]), "warning");
        }
    }, 15);
}

function ImprimirPrevia() {

    if (Ingreso == 0)
        return false;

    if (GuardaTemp == 1) {
        swal("Acción Cancelada", "Debe primero Guardar el informe", "warning");
        return false;
    }

    if (Certificado != "") {
        swal("Acción Cancelada", "No se puede ver la vista previa de un certificado generado", "warning");
        return false;
    }
    

    ActivarLoad();
    setTimeout(function () {
        var parametros = "ingreso=" + Ingreso + "&numero=" + NumCertificado + "&revision=" + Revision;
        var datos = LlamarAjax("RpPlantillas/RpVPPlanillaPresion1", parametros);
        DesactivarLoad();
        datos = datos.split("|");
        if (datos[0] == "0") {
            window.open(url + "DocumPDF/" + datos[1]);
        } else {
            swal("", ValidarTraduccion(datos[1]), "warning");
        }
    }, 15);
    
}

function LlamarGenerarCertificado() {

    var presmax = NumeroDecimal($("#PresMax").val());
    var presmin = NumeroDecimal($("#PresMin").val());

    if (Ingreso == 0 || IdCalibracion == 0)
        return false;

    if (GuardaTemp == 1) {
        swal("Acción Cancelada", "Debe primero Guardar el informe", "warning");
        return false;
    }

    if (Certificado != "") {
        swal("Acción Cancelada", "Ya se generó el certificado de este ingreso", "warning");
        return false;
    }

    if (TempMax == 0 || TempMin == 100000000 || HumeMax == 0 || HumeMin == 100000000) {
        swal("Acción Cancelada", "Debe cargar el archivo de temperatura y humedad", "warning");
        return false;
    }

    if (presmax == 0 || presmin == 0) {
        $("#PresMax").focus();
        swal("Acción Cancelada", "Debe agregar la presión admoférica", "warning");
        return false;
    }

    var mensaje = "";
    if (TempVar > 1 || HumeMax >= 75)
        mensaje = "temperatura y humedad";
    else {
        if (TempVar == 1)
            mensaje = "temperatura";
        if (HumeMax >= 75)
            mensaje = "humedad";
    }

    /*if (mensaje != "") {
        $("#documento").focus();
        swal("Acción Cancelada", "Condiciones de " + mensaje + " no adecuadas, para la calibración... No podrá General el certificado", "warning");
        EscucharMensaje("Condiciones de " + mensaje + " no adecuadas, para la calibración. No podrá General el certificado");
        return false;
    }*/

    $("#TipoAccesso").val("GENERAR CERTIFICADO CON FIRMA");
    $("#modalControlAcceso").modal("show");
    $("#AccClave").val("");
    $("#AccClave").focus();

}

function GuardarFirma(certificado) {
    var firma = canvafirma.toDataURL("image/png");
    $.ajax({
        type: 'POST',
        url: url + "Laboratorio/GuardarFirma",
        data: "firma=" + firma + "&certificado=" + certificado + "&usuario=" + localStorage.getItem("UsuarioCertificado"),
        success: function (msg) {
            respuesta = "si";
        }
    });
}

function CalcularClase(Caja) {
    if (Certificado != "" || Ingreso == 0)
        return false
    ValidarTexto(Caja, 1);
    if ((Caja.value * 1) == 0)
        return false
    if (IdCalibracion == 0) {
        $("#Clase").val(formato_numero(Clase, 4, ".", ",", ""));
        swal("Acción Cancelada", "Debe primero guardar el certificado", "warning");
        return false;
    }
    Clase = Caja.value * 1;
    DesMax = Clase / 100 * RangoHasta;
    DesMin = Clase / 100 * RangoHasta * -1;
    ActivarLoad();
    setTimeout(function () {
        CalcularTotal();
        Graficar();
        GraficarBarra();
        GraficarDesviacion();
        $("#FormPrevia").submit();
    }, 15);
}
    


function GenerarCertificado() {

    if (Ingreso == 0 || IdCalibracion == 0)
        return false;

    if (GuardaTemp == 1) {
        swal("Acción Cancelada", "Debe primero Guardar el informe", "warning");
        return false;
    }
        
    if (Certificado != "") {
        swal("Acción Cancelada", "Ya se generó el certificado de este ingreso", "warning");
        return false;
    }

    if (TempMax == 0 || TempMin == 100000000 || HumeMax == 0 || HumeMin == 100000000) {
        swal("Acción Cancelada", "Debe cargar el archivo de temperatura y presión", "warning");
        return false;
    }

    if (PresMax == 0 || PresMin == 0) {
        $("#PresMax").focus();
        swal("Acción Cancelada", "Debe agregar la presión admoférica", "warning");
        return false;
    }

    var mensaje = "";
    if (TempVar > 1 || HumeMax >= 75)
        mensaje = "temperatura y humedad";
    else {
        if (TempVar == 1)
            mensaje = "temperatura";
        if (HumeMax >= 75)
            mensaje = "humedad";
    }

    /*if (mensaje != "") {
        $("#documento").focus();
        swal("Acción Cancelada", "Condiciones de " + mensaje + " no adecuadas, para la calibración... No podrá General el certificado", "warning");
        EscucharMensaje("Condiciones de " + mensaje + " no adecuadas, para la calibración. No podrá General el certificado");
        return false;
    }*/

    if (localStorage.getItem("FirmaCertificado") == "") {
        swal("Acción Cancelada", "Debe primero firmar el certificado", "warning");
        return false;
    }

    Solicitante = Solicitante.replace(/&/g, '%26');
    Direccion = Direccion.replace(/&/g, '%26');

    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: ValidarTraduccion('¿Seguro que desea generar el certificado del ingreso número ' + Ingreso + '?'),
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Si'),
        cancelButtonText: ValidarTraduccion('No'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                $.post("Laboratorio/GenerarCertificadoPre", "ingreso=" + Ingreso + "&revision=" + Revision + "&numcertificado=" + NumCertificado + "&NombreCer=" + Solicitante + "&DireccionCer=" + Direccion + "&CoPresMax=" + $("#CoPresMax").val() + "&CoPresMin=" + $("#CoPresMin").val() + "&CoPresVar=" + $("#VarPres").html() + "hPa" + 
                    "&CoPresMax=" + $("#CoPresMax").val() + "&CoPresMin=" + $("#CoPresMin").val() + "&PresMax=" + $("#PresMax").val() + "&PresMin=" + $("#PresMin").val() + 
                    "&usuario=" + localStorage.getItem("UsuarioCertificado"))
                    .done(function (data) {
                        var datos = data.split("|");
                        if (datos[0] == "0") {
                            Certificado = datos[2];
                            GuardarFirma(Certificado);
                            localStorage.setItem("FirmaCertificado", "");
                            clearCanvas(canvas, ctxgra);
                            $("#FirmarCertificadoPT").addClass("hidden");
                            for (var x = 1; x <= 3; x++) {
                                for (var fila = 1; fila <= 6; fila++) {
                                    $("#LecturaPatMed_A" + x + fila).prop("disabled",true);
                                    $("#LecturaPatMed_D" + x + fila).prop("disabled", true);

                                    $("#LecturaIBCMed_A" + x + fila).prop("disabled", true);
                                    $("#LecturaIBCMed_D" + x + fila).prop("disabled", true);
                                }
                            }
                            $("#Porcentaje_A13").prop("disabled", true);
                            $("#Porcentaje_A14").prop("disabled", true);
                            $("#Porcentaje_A15").prop("disabled", true);
                            $("#Observacion").prop("disabled", true);
                            $("#PresMax").prop("disabled", true);
                            $("#PresMin").prop("disabled", true);
                            $("#Certificado").html(Certificado);
                            $("#FechaExp").html(FechaActual);
                            $.jGrowl(datos[1] + " " + datos[2], { life: 1500, theme: 'growl-success', header: '' });
                            EscucharMensaje(datos[1]);
                            resolve()
                        } else {
                            reject(datos[1]);
                        }
                    })
            })
        }
    }]);
}

TablaCMC();

function TiempoTrascurrudi() {
    if (Ingreso == 0 || IdCalibracion > 0 || HoraInicio == "")
        return false;
    d = new Date();
    horaactual = (d.getHours() * 1 < 10 ? "0" : "") + d.getHours() + ":" + (d.getMinutes() * 1 < 10 ? "0" : "") + d.getMinutes();
    tiempo = RestarHoras(HoraInicio, horaactual);
    $("#CaTiempo").val(tiempo);
    LeerCondiciones(0);

}

if (localStorage.getItem("CerIngreso") * 1 > 0) {
    LimpiarTodo();
    $("#Ingreso").val(localStorage.getItem("CerIngreso"));
    BuscarIngreso(localStorage.getItem("CerIngreso"), "", 0);
}

$(document).ready(function () {
    $("input").on('paste', function (e) {
        e.preventDefault();
        swal('Esta acción está prohibida');
    })
    
})

function pulsar(e) {
    tecla = (document.all) ? e.keyCode : e.which;
    //console.log(e.path[0].id)
    return (tecla != 13 || e.path[0].id == "Observacion");
}

function LeerCondiciones(tipo) {
    return false;
    if (TipoCalibracion == "Sitio")
        return false;

    if (Ingreso == 0 || Certificado != "")
        return false;
    if (tipo == 0 && HoraFinal != "") {
        return false;
    }
    if (tipo == 1 && HoraFinal != "")
        HoraCondicion = HoraFinal;

    var datos = LlamarAjax("Laboratorio/LeerArchivoTemperatura", "magnitud=2&fecha=" + FechaActual + "&hora=" + HoraCondicion + "&ingreso=" + Ingreso + "&numero=" + NumCertificado + "&revision=" + Revision).split("|");
    if (datos[0] == "0") {
        HoraCondicion = datos[1];
        CalcularTemperatura(tipo);
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
    
}

function ModalCotizacion() {
    if (Ingreso == 0)
        return false;
    $("#modalcotizacion").modal("show");
}

$('select').select2();
DesactivarLoad();
