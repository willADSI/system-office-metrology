﻿d = new Date();
month = d.getMonth() + 1;
day = d.getDate();

var Anio = $.trim(d.getFullYear());
Anio = $.trim(Anio.substr(2, 2))
var PermisioEspecial = PerGeneSistema("PERMISO ADMINISTRADOR LABORATORIO");

output = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-01';

var FechaCal = d.getFullYear() + "-" + (month < 10 ? '0' : '') + month + "-" + (day < 10 ? "0" : "") + day;

var Ingreso = 0;
var ErrorCalculo = 0;
var IdReporte = 0;
var IdInstrumento = 0;
var ErrorEnter = 0;
var Fotos = 0;
var IdVersion = 6;
var NroReporte = 1;
var RangoHasta = 0;
var RangoDesde = 0;
var GuardaTemp = 0;
var CorreoEmpresa = "";
var AplicarTemporal = 0;
var InfoTecIngreso = 0;
var Factor = 0;

var asesor = "";
var Habitual = "NO";
var Sitio = "";
var Garantia = "NO";
var AprobarCotizacion = "SI";
var FechaAproCoti = "";


/*$("#Patron1").html(CargarCombo(20, 1, "", "6"));
$("#Patron2").html($("#Patron1").html());
$("#Patron3").html($("#Patron1").html());*/

localStorage.setItem("Ingreso", "0");

function GenerarCertificado() {
    localStorage.setItem("CerIngreso", Ingreso);
    LlamarOpcionMenu('Laboratorio/PlaCertiParTor', 3, '');

}

function LimpiarTodo() {
    GuardaTemp = 0;
    Ingreso = 0;
    ErrorEnter = 0;
    localStorage.removeItem("Pre" + Ingreso);
    localStorage.removeItem("Table" + Ingreso)
    localStorage.removeItem("Puntos" + Ingreso)
    LimpiarDatos();
    $("#IngresoParTor").val("");
    GuardaTemp = 1;
}

function LimpiarPlantilla() {
    GuardaTemp = 0;
    localStorage.removeItem("Pre" + Ingreso);
    localStorage.removeItem("Table" + Ingreso)
    localStorage.removeItem("Puntos" + Ingreso)
    LimpiarDatos();
    $("#IngresoParTor").val("");
    GuardaTemp = 1;
}

function LimpiarDatos() {
    ErrorCalculo = 0;
    IdRemision = 0;
    AplicarTemporal = 0;
    Ingreso = 0;
    localStorage.setItem("Ingreso", Ingreso);
    Fotos = 0
    InfoTecIngreso = 0;
    NroReporte = 1;
    $("#Solicitante").html("");
    $("#Certificado").html("");
    $("#Direccion").html("");
    $("#FechaIng").html("");
    $("#FechaCal").html("");

    $("#TIngreso").html("");
    $("#Medida").html("");
    $("#ValorCon").html("");
    $("#UnidadCon").html("");

    $(".medida").html("");

    $("#Equipo").html("");
    $("#Marca").html("");
    $("#Modelo").html("");
    $("#Serie").html("");
    $("#Remision").val("");
    $("#Usuario").val("");
    $("#FechaRec").val("");
    $("#Tiempo").val("");
    $("#Proxima").html("");

    $("#CMetodo").val("");
    $("#CPunto").val("");
    $("#CServicio").val("");
    $("#CObservCerti").val("");
    $("#CProxima").val("");
    $("#CEntrega").val("");
    $("#CAsesorComer").val("");

    $("#RangoMedida").html("");
    $("#MedResolucion").html("");
    RangoHasta = 0;

    $("#Descripcion").val("");
    $("#Tolerancia").val("");
    $("#Resolucion").val("");
    $("#Indicacion").val("").trigger("change");
    $("#Clase").val("").trigger("change");
    $("#Clasificacion").val("").trigger("change");
    $("#ProximaCali").val("");

    $("#PorLectura1").val("20");
    $("#Serie11").val("");
    $("#Serie21").val("");
    $("#Serie31").val("");

    $("#PorLectura2").val("60");
    $("#Serie12").val("");
    $("#Serie22").val("");
    $("#Serie32").val("");

    $("#PorLectura3").val("100");
    $("#Serie13").val("");
    $("#Serie23").val("");
    $("#Serie33").val("");

    $("#Promedio1").html("");
    $("#Lectura1").html("");
    $("#Desviacion1").html("");
    $("#Porcentaje1").html("");

    $("#Promedio2").html("");
    $("#Lectura2").html("");
    $("#Desviacion2").html("");
    $("#Porcentaje2").html("");

    $("#Promedio3").html("");
    $("#Lectura3").html("");
    $("#Desviacion3").html("");
    $("#Porcentaje3").html("");

    $("#Usuario").val("");
    $("#FechaRec").val("");
    $("#Tiempo").val("");

    $("#Concepto").val("1").trigger("change");
    $("#Ajuste").val("");
    $("#Suministro").val("");
    $("#Observacion").val("");
    $("#Conclusion").val("");

    $("#tbodycondicion").html("");
    $("#TBSerie").html("");
    $("#TBCalculo").html("");
    $("#ValorCero").html("0");

    $("#Puntos").val("3").trigger("change");
           
    IdInstrumento = 0;

    for (var x = 1; x <= 5; x++) {
        $("#IdReporte" + x).val("0");
        $("#Observaciones" + x).val("");
        $("#Conclusion" + x).val("");
        $("#Ajuste" + x).val("");
        $("#Suministro" + x).val("");
        $("#registroingreso" + x).html("");
        $("#Concepto" + x).val("").trigger("change");
    }
}


function GeneralCertificado() {
    var plantilla = $("#Plantilla").val(); 
    if (GuardaTemp == 1 || IdReporte == 0) {
        swal("Acción Cancelada", "Debe primero guardar el reporte", "warning");
        return false;
    }

    if ((concepto != 2) && (concepto != 1)) {
        swal("Acción Cancelada", "No se puede generar certificado cuando no está apto a calibrar o en subcontrataciones", "warning");
        return false;
    }

    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: '¿Seguro que desea generar el informe técnico del ingreso número ' + Ingreso + '?',
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Si'),
        cancelButtonText: ValidarTraduccion('No'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                $.post("Laboratorio/InformeTecnicoDev", "ingreso=" + Ingreso + "&reporte=0&plantilla=" + plantilla)
                    .done(function (data) {
                        datos = data.split("|");
                        if (datos[0] == "0") {
                            $("#NroInforme" + reporte).val(datos[2]);
                            ImprimirInforme();
                            resolve()
                        } else {
                            reject(datos[1]);
                        }
                    })
            })
        }
    }]);
}

function EnviarCotizacion() {

    var concepto = $("#Concepto").val() * 1;


    if (Ingreso == 0)
        return false;
    if (GuardaTemp == 1 || IdReporte == 0) {
        swal("Acción Cancelada", "Debe primero guardar el reporte", "warning");
        return false;
    }

    $("#eCliente").val($("#Solicitante").html());
    $("#eCorreo").val(CorreoEmpresa);


    if (concepto == 2) {
        $("#spcotizacion").html(Ingreso);
        $("#enviar_cotizacion").modal("show");
    } else {
        swal("Acción Cancelada", "Solo se podra enviar cotizaciones cuando el ingreso requiera ajuste o suministro", "warning");
    }
    
}

$("#formenviarcotizacion").submit(function (e) {

    e.preventDefault();
    var observacion = $.trim($("#ObserEnvio").val())
    var correo = $.trim($("#eCorreo").val());
    var plantilla = $("#Plantilla").val() * 1;
    a_correo = correo.split(";");

    if (ValidarCorreo(CorreoEmpresa) == 2) {
        swal(ValidarTraduccion("Acción Cancelada"), ValidarTraduccion("El correo electrónico del contacto no es válido ") + ":" + CorreoEmpresa, "warning");
        //return false;
    }

    for (var x = 0; x < a_correo.length; x++) {
        if (ValidarCorreo($.trim(a_correo[x])) == 2) {
            $("#eCorreo").focus();
            swal(ValidarTraduccion("Acción Cancelada"), ValidarTraduccion("Formato de correo electrónico no válido") + ":" + a_correo[x], "warning");
            return false;
        }
    }

    
    ActivarLoad();
    setTimeout(function () {
        var parametros = "magnitud=6&ingreso=" + Ingreso + "&principal=" + CorreoEmpresa + "&e_email=" + correo + "&observacion=" + observacion + "&plantilla=" + plantilla + "&version=6&revision=1";
        var datos = LlamarAjax("Laboratorio/EnvioCotizacion", parametros);
        DesactivarLoad();
        datos = datos.split("|");
        if (datos[0] == "0") {
            swal("", ValidarTraduccion(datos[1]) + " " + datos[2], "success");
            $("#enviar_cotizacion").modal("hide");
        } else
            swal("Acción Cancelada", datos[1], "warning");
    }, 15);

    return false;
});





function ConsularReportesIngresos() {
            
    ActivarLoad();
    setTimeout(function () {
        var cliente = $("#ClienteIngreso").val() * 1;
        var aprobadas = 0;
        if ($("#Aprobadas").prop("checked"))
            aprobadas = 1
        var parametros = "magnitud=6&cliente=" + cliente + "&aprobadas=" + aprobadas;
        var datos = LlamarAjax("Laboratorio/TablaReportarIngreso", parametros);
        DesactivarLoad();
        var datajson = JSON.parse(datos);
        $('#TablaReportar').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "fila" },
                { "data": "ingreso" },
                { "data": "equipo" },
                { "data": "fecha" },
                { "data": "tiempo" }
            ],

            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy','colvis'
            ]
        });
    }, 15);
}


function CambioCondicion(tipo) {

    var plantilla = $("#Plantilla").val();
    var resultado = "";
    if ($.trim(tipo) != "") {
        if (tipo == "TIPO I")
            tipo = "/1/";
        else
            tipo = "/2/";

        var datos = LlamarAjax("Laboratorio/CambioCondicion", "ingreso=" + Ingreso + "&magnitud=6" + "&tipo=" + tipo + "&plantilla=" + plantilla);

        if (datos != "[]") {
            var datacon = JSON.parse(datos);
            var valores = "";
            var opcion = 0;
            var radio = "";
            for (var x = 0; x < datacon.length; x++) {
                valores = datacon[x].opcion.split("!!");
                opcion = valores[0] * 1;
                switch (opcion) {
                    case 0:
                        radio = "<td><input type='radio' onchange='GuardarTemporal(1)' required class='form-control' name='Opcion" + x + "' id ='CSi" + x + "'></td>" +
                            "<td><input type='radio' onchange='GuardarTemporal(1)' required class='form-control' name='Opcion" + x + "' id ='CNo" + x + "'></td>" +
                            "<td><input type='radio' onchange='GuardarTemporal(1)' required class='form-control' name='Opcion" + x + "' id ='CNA" + x + "'></td>";
                        break;
                    case 1:
                        radio = "<td><input type='radio' onchange='GuardarTemporal(1)' required checked class='form-control' name='Opcion" + x + "' id ='CSi" + x + "'></td>" +
                            "<td><input type='radio' onchange='GuardarTemporal(1)' required class='form-control' name='Opcion" + x + "' id ='CNo" + x + "'></td>" +
                            "<td><input type='radio' onchange='GuardarTemporal(1)' required class='form-control' name='Opcion" + x + "' id ='CNA" + x + "'></td>";
                        break;
                    case 2:
                        radio = "<td><input type='radio' onchange='GuardarTemporal(1)' required class='form-control' name='Opcion" + x + "' id ='CSi" + x + "'></td>" +
                            "<td><input type='radio' onchange='GuardarTemporal(1)' required checked class='form-control' name='Opcion" + x + "' id ='CNo" + x + "'></td>" +
                            "<td><input type='radio' onchange='GuardarTemporal(1)' required class='form-control' name='Opcion" + x + "' id ='CNA" + x + "'></td>";
                        break;
                    case 3:
                        radio = "<td><input type='radio' onchange='GuardarTemporal(1)' required class='form-control' name='Opcion" + x + "' id ='CSi" + x + "'></td>" +
                            "<td><input type='radio' onchange='GuardarTemporal(1)' required class='form-control' name='Opcion" + x + "' id ='CNo" + x + "'></td>" +
                            "<td><input type='radio' onchange='GuardarTemporal(1)' required checked class='form-control' name='Opcion" + x + "' id ='CNA" + x + "'></td>";
                        break;
                }
                resultado += "<tr>" +
                    "<td class='bg-gris'>" + datacon[x].descripcion + "</td>" + radio +
                    "<td><input class='form-control sinbordecon bg-amarillo' name='CObservacion[]' value='" + valores[1] + "'>" +
                    "<input type='hidden' value='" + datacon[x].id + "' name='Ids[]'><input type='hidden' value='" + datacon[x].descripcion + "' name='Descripciones[]'></td></tr>";
            }
        }
    }
            
    $("#tbodycondicion").html(resultado);
    BuscarDescripcionEquipo();
    GuardarTemporal(1);
}


ConsularReportesIngresos();

$.connection.hub.start().done(function () {

    $('#IngresoParTor').keyup(function (event) {
        numero = this.value*1;
        if (numero == -1)
            numero = Ingreso;

        
        if ((Ingreso != (numero * 1)) && ($("#Equipo").html() != "")) {
            GuardaTemp = 1;
            LimpiarDatos();
            Ingreso = 0;
            ErrorEnter = 0;
        }
        if (event.which == 13) {
            event.preventDefault();

            if (Ingreso == numero)
                return false;
            BuscarIngreso(numero);
        }
    });
});

function BuscarIngreso(numero) {
    var plantilla = $("#Plantilla").val() * 1;
    var revision = $("#Revision").val() * 1
    if (ErrorEnter == 0) {
        if (numero * 1 > 0) {
            GuardaTemp = 0;
            var datos = LlamarAjax("Laboratorio/BuscarIngresoOperacion", "magnitud=6&ingreso=" + numero + "&medida=N·m&plantilla=" + plantilla + "&revision=" + revision + "&version=" + IdVersion).split("|");
            if (datos[0] != "[]") {
                var data = JSON.parse(datos[0]);

                if (data[0].idmagnitud != "6") {
                    swal("Acción Cancelada", "El ingreso número " + numero + " no pertenece a la magnitud <br><b>PAR TORSIONAL</b>", "warning");
                    ErrorEnter = 1;
                    $("#IngresoParTor").select();
                    $("#IngresoParTor").focus();
                    return false;
                }

                IdReporte = 0;
                NumReporte = 0;
                if (datos[1] != "[]") {
                    var datarep = JSON.parse(datos[1]);

                    for (var x = 0; x < datarep.length; x++) {
                        NumReporte = datarep[x].nroreporte;
                        if (datarep[x].nroreporte * 1 == 0) {
                            GuardaTemp = 0;
                            IdReporte = datarep[x].id;
                            Ingreso = numero;
                            $("#Concepto").val(datarep[x].idconcepto).trigger("change");
                            $("#Ajuste").val(datarep[x].ajuste);
                            $("#Suministro").val(datarep[x].suministro);
                            $("#Observacion").val(datarep[x].observaciones);
                            $("#Conclusion").val(datarep[x].conclusion);
                            $("#Plantilla").val(datarep[x].plantilla).trigger("change");
                            $("#ValorCero").val(datarep[x].valorcero);
                            $("#Puntos").val(datarep[x].puntos).trigger("change");

                            $("#registroingreso").html("<h3>Registrado el día " + datarep[x].fecha + " por el técnico " + datarep[x].usuario + "</h3>" + datarep[x].aprobado);
                        }
                        if (datarep[x].idconcepto == 3) {
                            InfoTecIngreso = 1;
                            break;
                        }
                    }

                    if (IdReporte != 0) {
                        if (NumReporte != 0) {
                            if (tipo != "3")
                                swal("Advertencia", "Reporte realizado en reporte de ingreso " + tipo, "warning");
                        } else {
                            GuardaTemp = 0;
                            var dataser = JSON.parse(datos[2]);

                            for (var x = 0; x < dataser.length; x++) {
                                $("#PorLectura" + dataser[x].numero).val(dataser[x].porlectura);
                                $("#CoPorLectura" + dataser[x].numero).val(dataser[x].porlectura);
                                $("#Serie1" + dataser[x].numero).val(formato_numero(dataser[x].serie1, 3, ".", ",", ""));
                                $("#Serie2" + dataser[x].numero).val(formato_numero(dataser[x].serie2, 3, ".", ",", ""));
                                $("#Serie3" + dataser[x].numero).val(formato_numero(dataser[x].serie3, 3, ".", ",", ""));
                                $("#Patron" + dataser[x].numero).val(dataser[x].patron).trigger("change");
                            }
                            swal("Acción Cancelada", "El ingreso número " + numero + " ya ha sido reportado", "warning");
                        }
                    }
                    ErrorEnter = 1;
                    $("#IngresoParTor").select();
                    $("#IngresoParTor").focus();
                }

                IdRemision = data[0].id * 1;
                Ingreso = data[0].ingreso * 1;
                localStorage.setItem("Ingreso", Ingreso);
                Fotos = data[0].fotos * 1;
                asesor = "/" + data[0].asesor + "/";
                Sitio = data[0].sitio;
                Garantia = data[0].garantia;
                Habitual = data[0].habitual;
                FechaAproCoti = $.trim(data[0].fechaaprocoti);
                AprobarCotizacion = data[0].aprobar_cotizacion;
                $("#Solicitante").html(data[0].cliente);
                $("#Certificado").html("PT-" + data[0].contador + "-" + Anio);
                $("#Direccion").html(data[0].direccion);
                $("#FechaIng").html(data[0].fechaing);
                $("#FechaCal").html(FechaCal);
                CorreoEmpresa = data[0].email;

                $("#TIngreso").html(data[0].ingreso);
                $("#Medida").html(data[0].medida);
                $("#ValorCon").html(data[0].medidacon);
                Factor = data[0].medidacon;
                $("#UnidadCon").html("1 " + data[0].medida + " =");

                $(".medida").html(data[0].medida);

                $("#Equipo").html(data[0].equipo);
                $("#Marca").html(data[0].marca);
                $("#Modelo").html(data[0].modelo);
                $("#Proxima").html(data[0].proxima);
                $("#Serie").html(data[0].serie);
                $("#Remision").val(data[0].remision);
                InfoTecIngreso = data[0].informetecnico;


                $("#RangoMedida").html("(" + data[0].desde + " a " + data[0].hasta + ") " + data[0].medida);
                $("#MedResolucion").html(data[0].medida);
                RangoHasta = data[0].hasta * 1;
                RangoDesde = data[0].desde * 1;

                $("#Descripcion").val((data[0].descripcion ? data[0].descripcion : ""));
                $("#Tolerancia").val((data[0].tolerancia ? data[0].tolerancia : "4"));
                $("#Resolucion").val((data[0].resolucion ? data[0].resolucion : ""));
                $("#Indicacion").val((data[0].indicacion ? data[0].indicacion : "")).trigger("change");
                $("#Clase").val((data[0].clase ? data[0].clase : "")).trigger("change");
                $("#Clasificacion").val((data[0].clasificacion ? data[0].clasificacion : "")).trigger("change");
                IdInstrumento = (data[0].idinstrumento ? data[0].idinstrumento : 0);

                $("#Usuario").val(data[0].usuarioing);
                $("#FechaRec").val(data[0].fecharec);
                $("#Tiempo").val(data[0].tiempo);

                //MODAL DE COTIZACIONES

                if (datos[3] != "[]") {
                    var dataco = JSON.parse(datos[3]);
                    if (IdReporte == 0) {
                        if (Habitual == "NO" && AprobarCotizacion == "SI" && Sitio == "NO" && Garantia == "NO" && FechaAproCoti == "") {
                            if (dataco[0].estado != "Aprobado" && dataco[0].estado != "POR REEMPLAZAR") {
                                ErrorEnter = 1;
                                swal("Acción Cancelada", "La cotizacíon número " + dataco[0].cotizacion + " no ha sido aprobada por el cliente", "warning");
                                chat.server.send(usuariochat, "Buen días amigo. se requiere la aprobación de la cotización número " + dataco[0].cotizacion + " para proceder a la calibración", '', asesor);
                                if (PermisioEspecial == 0) {
                                    LimpiarDatos();
                                    return false;
                                }
                            }
                        }
                    }
                    for (var x = 0; x < dataco.length; x++) {
                        if (x == 0) {
                            $("#CMetodo").val(dataco[x].metodo);
                            $("#CPunto").val(dataco[x].punto);
                            $("#CServicio").val(dataco[x].servicio);
                            $("#CObservCerti").val(dataco[x].observacion);
                            $("#CProxima").val(dataco[x].proxima);
                            $("#CEntrega").val(dataco[x].entrega);
                            $("#CAsesorComer").val(dataco[x].asesor);
                        } else {
                            $("#CMetodo").val("<br>" + dataco[x].metodo);
                            $("#CPunto").val("<br>" + dataco[x].punto);
                            $("#CServicio").val("<br>" + dataco[x].servicio);
                            $("#CObservCerti").val("<br>" + dataco[x].observacion);
                            $("#CProxima").val("<br>" + dataco[x].proxima);
                            $("#CEntrega").val("<br>" + dataco[x].entrega);
                            $("#CAsesorComer").val("<br>" + dataco[x].asesor);
                        }
                    }
                } else {
                    if (IdReporte == 0) {
                        if (Habitual == "NO" && AprobarCotizacion == "SI" && Sitio == "NO" && Garantia == "NO" && FechaAproCoti == "") {
                            ErrorEnter = 1;
                            swal("Acción Cancelada", "Este ingreso no ha sido cotizado por el asesor comercial", "warning");
                            chat.server.send(usuariochat, "Buen días amigo. se requiere realizar la cotizacion y aprobación del ingreso número " + Ingreso + " para proceder a la calibración", '', asesor);
                            if (PermisioEspecial == 0) {
                                LimpiarDatos();
                                return false;
                            }
                        }
                    }
                }

                if (IdReporte == 0) {
                    if (localStorage.getItem("Pre" + Ingreso)) {
                        var data = JSON.parse(localStorage.getItem("Pre" + Ingreso));
                        $("#Descripcion").val(data[0].descripcion);
                        $("#Tolerancia").val(data[0].tolerancia);
                        $("#Resolucion").val(data[0].resolucion);
                        $("#Indicacion").val(data[0].indicacion).trigger("change");
                        $("#Clase").val(data[0].clase).trigger("change");
                        $("#Clasificacion").val(data[0].clasificacion).trigger("change");
                        $("#Puntos").val(data[0].puntos).trigger("change");
                        $("#ValorCero").val(data[0].valorcero);
                        $("#Planilla").val(data[0].planilla).trigger("change");
                        console.log(localStorage.getItem("Puntos" + Ingreso));
                        var datapunto = JSON.parse(localStorage.getItem("Puntos" + Ingreso));
                        for (var x = 0; x < datapunto.length; x++) {
                            $("#PorLectura" + (x + 1)).val(datapunto[x].PorLectura);
                            $("#Patron" + (x + 1)).val(datapunto[x].patron).trigger("change");
                            $("#Serie1" + (x + 1)).val(datapunto[x].Serie1);
                            $("#Serie2" + (x + 1)).val(datapunto[x].Serie2);
                            $("#Serie3" + (x + 1)).val(datapunto[x].Serie3);
                        }

                        $("#Concepto").val(data[0].concepto).trigger("change");
                        $("#Ajuste").val(data[0].ajuste);
                        $("#Suministro").val(data[0].suministro);
                        $("#Conclusion").val(data[0].conclusion);
                        $("#Observacion").val(data[0].observacion);
                    }

                    if (localStorage.getItem("Table" + Ingreso))
                        $("#tbodycondicion").html(localStorage.getItem("Table" + Ingreso));
                    GuardaTemp = 1;
                    CalcularTotal();
                } else {
                    CalcularTotal();
                    //$("#Concepto").val(datarep[x].idconcepto).trigger("change");
                    AplicarTemporal = 1;
                }



            } else {
                $("#IngresoParTor").select();
                $("#IngresoParTor").focus();
                ErrorEnter = 1;
                swal("Acción Cancelada", "El Ingreso número " + numero + " no se ha recibido en el laboratorio", "warning");
                return false;
            }
        }
    } else
        ErrorEnter = 0;
}
  

function CalcularSubTotal(Caja) {
    ValidarTexto(Caja, 3);
    CalcularTotal();
}

function CalcularTotal() {
    CalcularSerie("", 1,1);
    CalcularSerie("", 2,1);
    CalcularSerie("", 3,1);

    RequerirAjuste();

}

function BuscarDescripcionEquipo() {
    var clase = $("#Clase").val();
    var tipo = $("#Clasificacion").val();
    $("#Descripcion").html("");
    if (clase != "" && tipo != "") {
        var datos = LlamarAjax("Laboratorio/BuscarDescripcionEquipo", "tipo=" + tipo + "&clase=" + clase);
        if (datos != "XX") {
            datos = datos.split("||");
            $("#Descripcion").html(datos[0]);
        }
    }
    GuardarTemporal();
}

function GuardarTemporal(tabla) {
    var puntos = $("#Puntos").val() * 1;
    if (Ingreso > 0 && GuardaTemp == 1) {
        var archivo = "[";
        for (var x = 1; x <= puntos; x++) {
            if (x > 1)
                archivo += ',';
            archivo += '{"PorLectura":"' + $("#PorLectura" + x).val() + '","Lectura":"' + $("#Lectura" + x).val() + '","patron":"' + $("#Patron" + x).val() + '",' + 
                '"Serie1":"' + $("#Serie1" + x).val() + '","Serie2":"' + $("#Serie2" + x).val() + '","Serie3":"' + $("#Serie3" + x).val() + '"}';
        }
        archivo += "]";

        localStorage.setItem("Puntos" + Ingreso, archivo);

        var archivo = "[{";
        archivo += '"tolerancia":"' + $("#Tolerancia").val() + '","resolucion":"' + $("#Resolucion").val() + '",' +
            '"descripcion":"' + $("#Descripcion").val() + '","clase":"' + $("#Clase").val() + '","indicacion":"' + $("#Indicacion").val() + '",' +
            '"clasificacion":"' + $("#Clasificacion").val() + '",' +
            '"concepto":"' + $("#Concepto").val() + '","ajuste":"' + $("#Ajuste").val() + '","suministro":"' + $("#Suministro").val() +
            '","observacion":"' + $("#Observacion").val() + '","conclusion":"' + $("#Conclusion").val() + '","idinstrumento":"' + IdInstrumento +
            '","id":"' + IdReporte + '","ingreso":"' + Ingreso + '","puntos":"' + puntos + '","planilla":"' + $("#Plantilla").val() + '","valorcero":"' + $("#ValorCero").val() + '"';
        archivo += '}]';

        if (tabla) {
            var id = document.getElementsByName("Ids[]");
            var observacion = document.getElementsByName("CObservacion[]");
            var descripcion = document.getElementsByName("Descripciones[]");

            var opcion = 0;
            var resultado = "";
            var radio = "";

            for (var x = 0; x < id.length; x++) {
                opcion = 0;
                if ($("#CSi" + x).prop("checked"))
                    opcion = 1;
                else {
                    if ($("#CNo" + x).prop("checked"))
                        opcion = 2;
                    else {
                        if ($("#CNA" + x).prop("checked"))
                            opcion = 3;
                    }
                }

                switch (opcion) {
                    case 0:
                        radio = "<td><input type='radio' onchange='GuardarTemporal(1)' required class='form-control' name='Opcion" + x + "' id ='CSi" + x + "'></td>" +
                            "<td><input type='radio' onchange='GuardarTemporal(1)' required class='form-control' name='Opcion" + x + "' id ='CNo" + x + "'></td>" +
                            "<td><input type='radio' onchange='GuardarTemporal(1)' required class='form-control' name='Opcion" + x + "' id ='CNA" + x + "'></td>";
                        break;
                    case 1:
                        radio = "<td><input type='radio' onchange='GuardarTemporal(1)' required checked class='form-control' name='Opcion" + x + "' id ='CSi" + x + "'></td>" +
                            "<td><input type='radio' onchange='GuardarTemporal(1)' required class='form-control' name='Opcion" + x + "' id ='CNo" + x + "'></td>" +
                            "<td><input type='radio' onchange='GuardarTemporal(1)' required class='form-control' name='Opcion" + x + "' id ='CNA" + x + "'></td>";
                        break;
                    case 2:
                        radio = "<td><input type='radio' onchange='GuardarTemporal(1)' required class='form-control' name='Opcion" + x + "' id ='CSi" + x + "'></td>" +
                            "<td><input type='radio' onchange='GuardarTemporal(1)' required checked class='form-control' name='Opcion" + x + "' id ='CNo" + x + "'></td>" +
                            "<td><input type='radio' onchange='GuardarTemporal(1)' required class='form-control' name='Opcion" + x + "' id ='CNA" + x + "'></td>";
                        break;
                    case 3:
                        radio = "<td><input type='radio' onchange='GuardarTemporal(1)' required class='form-control' name='Opcion" + x + "' id ='CSi" + x + "'></td>" +
                            "<td><input type='radio' onchange='GuardarTemporal(1)' required class='form-control' name='Opcion" + x + "' id ='CNo" + x + "'></td>" +
                            "<td><input type='radio' onchange='GuardarTemporal(1)' required checked class='form-control' name='Opcion" + x + "' id ='CNA" + x + "'></td>";
                        break;
                }
                resultado += "<tr>" +
                    "<td class='bg-gris'>" + descripcion[x].value + "</td>" + radio +
                    "<td><input class='form-control sinbordecon bg-amarillo' name='CObservacion[]' value='" + observacion[x].value + "'>" +
                    "<input type='hidden' value='" + id[x].value + "' name='Ids[]'><input type='hidden' value='" + descripcion[x].value + "' name='Descripciones[]'></td></tr>";
            }
            localStorage.setItem("Table" + Ingreso, resultado);
        }


        localStorage.setItem("Pre" + Ingreso, archivo);

        $.jGrowl("Guardando Temporal", { life: 1500, theme: 'growl-warning', header: '' });
    } else {
        if (AplicarTemporal == 1)
            GuardaTemp = 1;
    }
}


function CalcularSerie(Caja, fila, tipo) {
    if (ErrorCalculo == 1) {
        ErrorCalculo = 0;
        return false;
    }
    if (Caja != "")
        ValidarTexto(Caja, 3);
    var serie1 = NumeroDecimal($("#Serie1" + fila).val());
    var serie2 = NumeroDecimal($("#Serie2" + fila).val());
    var serie3 = NumeroDecimal($("#Serie3" + fila).val());
    var tolerancia = NumeroDecimal($("#Tolerancia").val());
    var patron = $("#Patron" + fila).val() * 1;
    var despatron = $("#Patron" + fila + " option:selected").text();


    if (tolerancia == 0)
        return false;

    var porlectura = 0;
    var lectura = 0;
    if (tipo == 1) {
        porlectura = NumeroDecimal($("#PorLectura" + fila).val()) * 1;
        lectura = RangoHasta * porlectura / 100;
    } else{
        lectura = NumeroDecimal($("#Lectura" + fila).val()) * 1;
        if (lectura > RangoHasta) {
            ErrorCalculo = 1;
            $("#Lectura" + fila).select();
            $("#Lectura" + fila).focus();
            swal("Acción Cancelada", "El punto de lectura no puede ser mayor a " + RangoHasta, "warning");
            return false;
        }
        porlectura = lectura * 100 / RangoHasta;
        /*if (porlectura > 20)       
            lectura = RangoHasta * porlectura / 100;*/
        $("#PorLectura" + fila).val(formato_numero(porlectura,4,".",",",""));
    }

    
    var promedio = (serie1 + serie2 + serie3) / 3;
    var desviacion = promedio - lectura;
    var porcentaje = desviacion * 100 / lectura;

    $("#Porcentaje" + fila).removeClass("bg-gris");
                
    if (porcentaje >= ((tolerancia * -1) + 1) && porcentaje <= (tolerancia - 1)) {
       $("#Porcentaje" + fila).removeClass("bg-naranja");
       $("#Porcentaje" + fila).removeClass("bg-danger");
       $("#Porcentaje" + fila).addClass("bg-default");
    } else {
        if (porcentaje >= (tolerancia * -1) && porcentaje <= tolerancia) {
            $("#Porcentaje" + fila).removeClass("bg-danger");
            $("#Porcentaje" + fila).removeClass("bg-default");
            $("#Porcentaje" + fila).addClass("bg-naranja");
       } else {
           if (porcentaje >= -1000 && porcentaje <= 1000) {
               $("#Porcentaje" + fila).addClass("bg-danger");
               $("#Porcentaje" + fila).removeClass("bg-default");
               $("#Porcentaje" + fila).removeClass("bg-naranja");
           }
       }
    }
               
    $("#Promedio" + fila).html(formato_numero(promedio, 4, ".", ",", ""));
    $("#Lectura" + fila).val(formato_numero(lectura, 4, ".", ",", ""));
    $("#Desviacion" + fila).html(formato_numero(desviacion, 4, ".", ",", ""));
    $("#Porcentaje" + fila).html(formato_numero(porcentaje, 4, ".", ",", ""));

    //tabla corregida
    var Copromedio = 0;
    if (patron > 0) {
        var parametros = "patron=" + patron + "&factor=" + Factor + "&serie1=" + serie1 + "&serie2=" + serie2 + "&serie3=" + serie3 + "&lectura=" + (lectura * Factor);
        var datos = LlamarAjax("Laboratorio/ConvertirSerieErr", parametros).split("|");
        if (datos[0] == ["0"]) {
            $("#CoSerie1" + fila).html(formato_numero(datos[1], 4, ".", ",", ""));
            $("#CoSerie2" + fila).html(formato_numero(datos[2], 4, ".", ",", ""));
            $("#CoSerie3" + fila).html(formato_numero(datos[3], 4, ".", ",", ""));
            Copromedio = ((datos[1] * 1) + (datos[2] * 1) + (datos[3] * 1)) / 3;
        } else {
            swal("Acción Cancelada", datos[1], "warning");
            despatron = "";
            $("#Patron" + fila).val("").trigger("change");
            
        }
    } else {
        $("#CoSerie1" + fila).html(formato_numero(serie1 * Factor, 4, ".", ",", ""));
        $("#CoSerie2" + fila).html(formato_numero(serie2 * Factor, 4, ".", ",", ""));
        $("#CoSerie3" + fila).html(formato_numero(serie3 * Factor, 4, ".", ",", ""));
        Copromedio = ((serie1 * Factor) + (serie2 * Factor) + (serie3 * Factor)) / 3;
    }

    $("#CoPatron" + fila).html(despatron);
    $("#CoPorLectura" + fila).html(formato_numero(porlectura,4,".",",",""));
    $("#CoLectura" + fila).html(formato_numero(lectura * Factor, 4, ".", ",", ""));

    var Codesviacion = Copromedio - (lectura * Factor);
    var Coporcentaje = Codesviacion * 100 / (lectura * Factor);

    $("#CoPorcentaje" + fila).removeClass("bg-gris");

    if (Coporcentaje >= ((tolerancia * -1) + 1) && Coporcentaje <= (tolerancia - 1)) {
        $("#CoPorcentaje" + fila).removeClass("bg-naranja");
        $("#CoPorcentaje" + fila).removeClass("bg-danger");
        $("#CoPorcentaje" + fila).addClass("bg-default");
    } else {
        if (Coporcentaje >= (tolerancia * -1) && Coporcentaje <= tolerancia) {
            $("#CoPorcentaje" + fila).removeClass("bg-danger");
            $("#CoPorcentaje" + fila).removeClass("bg-default");
            $("#CoPorcentaje" + fila).addClass("bg-naranja");
        } else {
            if (Coporcentaje >= -1000 && Coporcentaje <= 1000) {
                $("#CoPorcentaje" + fila).addClass("bg-danger");
                $("#CoPorcentaje" + fila).removeClass("bg-default");
                $("#CoPorcentaje" + fila).removeClass("bg-naranja");
            }
        }
    }

    $("#CoPromedio" + fila).html(formato_numero(Copromedio, 4, ".", ",", ""));
    $("#CoDesviacion" + fila).html(formato_numero(Codesviacion, 4, ".", ",", ""));
    $("#CoPorcentaje" + fila).val(formato_numero(Coporcentaje, 4, ".", ",", ""));

    
}

function CambioPatron(fila) {
    CalcularSerie("", fila,1);
    GuardarTemporal(0);
}

function RequerirAjuste() {
    GuardarTemporal();
    var tolerancia = NumeroDecimal($("#Tolerancia").val());
    if (tolerancia == 0)
        return false;
    if ($("#Serie11").val() == "" || $("#Serie21").val() == "" || $("#Serie31").val() == "")
        return false;
    if ($("#Serie12").val() == "" || $("#Serie22").val() == "" || $("#Serie32").val() == "")
        return false;
    if ($("#Serie13").val() == "" || $("#Serie23").val() == "" || $("#Serie33").val() == "")
        return false;

    var por1 = Math.abs(NumeroDecimal($("#CoPorcentaje1").val()));
    var por2 = Math.abs(NumeroDecimal($("#CoPorcentaje2").val()));
    var por3 = Math.abs(NumeroDecimal($("#CoPorcentaje3").val()));

    if (GuardaTemp == 1) {
        if (por1 > tolerancia || por2 > tolerancia || por3 > tolerancia) {
            $("#OptCalibrar").prop("disabled", true);
            $("#OptSinPosi").prop("disabled", false);
            $("#OptAjuste").prop("disabled", false);
            $("#Concepto").val(2).trigger("change");
            EscucharMensaje("El equipo requiere ajuste");
            $.jGrowl("El equipo requiere ajuste", { life: 6000, theme: 'growl-warning', header: '' });
        } else {
            $("#OptCalibrar").prop("disabled", false);
            $("#OptSinPosi").prop("disabled", true);
            $("#OptAjuste").prop("disabled", true);
            $("#Concepto").val(1).trigger("change");
            EscucharMensaje("El equipo está  apto para calibrar");
            $.jGrowl("El equipo está  apto para calibrar", { life: 6000, theme: 'growl-success', header: '' });
        }
    } else {
        if (por1 > tolerancia || por2 > tolerancia || por3 > tolerancia) {
            EscucharMensaje("El equipo requiere ajuste");
            $.jGrowl("El equipo requiere ajuste", { life: 6000, theme: 'growl-warning', header: '' });
        } else {
            EscucharMensaje("El equipo está  apto para calibrar");
            $.jGrowl("El equipo está  apto para calibrar", { life: 6000, theme: 'growl-success', header: '' });
        }
    }
}

function CambioConcepto(concepto, tipo) {
    var prefijo = (tipo == 0 ? "" : tipo);

    $("#Suministro" + prefijo).prop("disabled", true);
    $("#Ajuste" + prefijo).prop("disabled", true);
    $("#Conclusion" + prefijo).prop("disabled", true);
    $("#Observacion" + prefijo).prop("disabled", false);

    $("#Observacion" + prefijo).addClass("bg-amarillo");
    $("#Ajuste" + prefijo).removeClass("bg-amarillo");
    $("#Suminstro" + prefijo).removeClass("bg-amarillo");
    $("#Conclusion" + prefijo).removeClass("bg-amarillo");

    $("#Documento" + prefijo).removeClass("bg-amarillo");
    $("#NumFoto" + prefijo).removeClass("bg-amarillo");
    $("#Documento" + prefijo).prop("disabled", true);
    $("#NumFoto" + prefijo).prop("disabled", true);

    if (concepto == "2") {
        $("#Suministro" + prefijo).prop("disabled", false);
        $("#Ajuste" + prefijo).prop("disabled", false);
        $("#Observacion" + prefijo).prop("disabled", true);
        $("#Conclusion" + prefijo).prop("disabled", true);

        $("#Observacion" + prefijo).removeClass("bg-amarillo");
        $("#Ajuste" + prefijo).addClass("bg-amarillo");
        $("#Suministro" + prefijo).addClass("bg-amarillo");
        $("#Conclusion" + prefijo).removeClass("bg-amarillo");

    } else {
        if (concepto == "3" || concepto == "5") {
            $("#Suministro" + prefijo).prop("disabled", true);
            $("#Ajuste" + prefijo).prop("disabled", true);
            $("#Observacion" + prefijo).prop("disabled", false);
            $("#Conclusion" + prefijo).prop("disabled", false);

            $("#Observacion" + prefijo).addClass("bg-amarillo");
            $("#Ajuste" + prefijo).removeClass("bg-amarillo");
            $("#Suministro" + prefijo).removeClass("bg-amarillo");
            $("#Conclusion" + prefijo).addClass("bg-amarillo");
        
            $("#Documento" + prefijo).addClass("bg-amarillo");
            $("#NumFoto" + prefijo).removeClass("bg-amarillo");
            $("#Documento" + prefijo).prop("disabled", false);
            $("#NumFoto" + prefijo).prop("disabled", false);
        }
    }

    if (tipo == 0)
        GuardarTemporal();

}

CambioConcepto(1,1);

//$.connection.hub.start().done(function () {
$("#FormPrevia").submit(function (e) {
    e.preventDefault();

    var id = document.getElementsByName("Ids[]");
    var observacion = document.getElementsByName("CObservacion[]");
    var concepto = $("#Concepto").val() * 1;
    var suministro = $.trim($("#Suministro").val());
    var ajuste = $.trim($("#Ajuste").val());

    var porcentaje1 = $.trim($("#CoPorcentaje1").html());
    var porcentaje2 = $.trim($("#CoPorcentaje2").html());
    var porcentaje3 = $.trim($("#CoPorcentaje3").html());
        
    if (concepto == 2) {
        if (suministro == "" && ajuste == "") {
            $("#Ajuste").focus();
            swal("Acción Cancelada", "Debe ingresar el ajusto ó suministro", "warning");
            return false;
        }
    }

    var a_id = "";
    var a_observacion = "";
    var a_opcion = "";
    var opcion = 0;

    for (var x = 0; x < id.length; x++) {
        if ($("#CSi" + x).prop("checked"))
            opcion = 1;
        else {
            if ($("#CNo" + x).prop("checked"))
                opcion = 2
            else {
                if ($("#CNA" + x).prop("checked"))
                    opcion = 3
            }
        }
        if (a_id == "") {
            a_id += id[x].value;
            a_observacion = observacion[x].value + "";
            a_opcion += opcion;
        } else {
            a_id += "|" + id[x].value;
            a_observacion += "|" + observacion[x].value + "";
            a_opcion += "|" + opcion;
        }
    }
    var parametros = $("#FormPrevia").serialize().replace(/\%2C/g, "") + "&a_id=" + a_id + "&a_observacion=" + a_observacion + "&a_opcion=" + a_opcion + "&idinstrumento=" + IdInstrumento +
        "&id=" + IdReporte + "&concepto=" + $("#Concepto option:selected").text() + "&Descripcion=" + $("#Descripcion").html() + "&Version=" + IdVersion;
    var datos = LlamarAjax("Laboratorio/GuarOperParTor", parametros);
    console.log(datos);

    if (datos[0] == "0") {
        IdReporte = datos[2];
        IdInstrumento = datos[3];
        GuardaTemp = 0;
        localStorage.removeItem("Pre" + Ingreso);
        localStorage.removeItem("Table" + Ingreso)
        swal("", datos[1], "success");
        ConsularReportesIngresos();
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
});

function VistaInforme(reporte) {

    if (Ingreso == 0)
        return false;

    var concepto = 0;
    if (reporte > 0) {
        concepto = $("#Concepto" + reporte).val() * 1;
    }

    ActivarLoad();
    setTimeout(function () {
        var parametros = "ingreso=" + Ingreso + "&tipo=2&concepto=" + concepto;
        var datos = LlamarAjax("Laboratorio/RpInformeTecnico", parametros);
        DesactivarLoad();
        datos = datos.split("|");
        if (datos[0] == "0") {
            window.open(url + "DocumPDF/" + datos[1]);
        } else {
            swal("", ValidarTraduccion(datos[1]), "warning");
        }

    }, 15);
}

function ImprimirInforme(reporte) {
        
    if (Ingreso == 0)
        return false;

    var concepto = 0;
    if (reporte > 0) {
        concepto = $("#Concepto" + reporte).val() * 1;
    }

    ActivarLoad();
    setTimeout(function () {
        var parametros = "ingreso=" + Ingreso + "&tipo=1&concepto=" + concepto;
        var datos = LlamarAjax("Laboratorio/RpInformeTecnico", parametros);
        DesactivarLoad();
        datos = datos.split("|");
        if (datos[0] == "0") {
            window.open("DocumPDF/" + datos[1]);
        } else {
            swal("", ValidarTraduccion(datos[1]), "warning");
        }

    }, 15);
}

function InformeTecnico(tipo) {

    var concepto = $("#Concepto").val() * 1;
    var reporte = 0;
    var plantilla = $("#Plantilla").val() * 1;
    var id = IdReporte;
    var temporal = GuardarTemporal;
    if (tipo > 0) {
        concepto = $("#Concepto" + tipo).val() * 1;
        plantilla = 1;
        id = $("#IdReporte" + tipo).val() * 1;
        temporal = 0;
        reporte = NroReporte;
    }  
    if (Ingreso == 0)
        return false;

    if (temporal == 1 || id == 0) {
        swal("Acción Cancelada", "Debe primero guardar el reporte", "warning");
        return false;
    }
        
    if (concepto != 3 && concepto != 5) {
        swal("Acción Cancelada", "Solo se podrá generar informes cuando el ingreso no está apto para calibrar ó para mantenimiento y limpieza", "warning");
        return false;
    }

    if (InfoTecIngreso == 1) {

        ImprimirInforme(0);
        return false;
    }

    var mensaje = '¿Seguro que desea generar el informe técnico del ingreso número ' + Ingreso + '?';
    if (concepto == 5)
        mensaje = '¿Seguro que desea generar el informe de mantenimiento y limpieza del ingreso número ' + Ingreso + '?';

    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: mensaje,
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Si'),
        cancelButtonText: ValidarTraduccion('No'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                $.post("Laboratorio/InformeTecnicoDev", "ingreso=" + Ingreso + "&reporte=" + reporte + "&plantilla=" + plantilla)
                    .done(function (data) {
                        datos = data.split("|");
                        if (datos[0] == "0") {
                            $("#NroInforme" + reporte).val(datos[2]);
                            ImprimirInforme();
                            resolve()
                        } else {
                            reject(datos[1]);
                        }
                    })
            })
        }
    }]);
};
    
function VerFotos() {
    if (Ingreso == 0)
        return false;
    CargarModalFoto(Ingreso, Fotos,1);
}

function ConfigurarTablas(can) {
    var resultado = "";
    var resultado2 = "";
    var porlectura = "";

    
    var patrones = CargarCombo(20, 1, "", "6");
    for (var x = 1; x <= can; x++) {

        if (can == 3) {
            switch (x) {
                case 1:
                    porlectura = "20";
                    break;
                case 2:
                    porlectura = "60";
                    break;
                case 3:
                    porlectura = "100";
                    break;
            }
        }
        

        resultado += '<tr class="tabcertr">' +
            '<th class="text-center"><input type="text" id="PorLectura' + x + '" required onfocus="FormatoEntrada(this, 1)" onblur="FormatoSalida(this,4,1)" onchange="RequerirAjuste()" onkeyup="CalcularSerie(this,' + x + ',1)" class="form-control sinbordecon text-XX text-center bg-amarillo" value="' + porlectura + '" name="PorLectura" /></th>' +
            '<th class="text-center bg-gris text-XX"><input type="text"  id="Lectura' + x + '" required onfocus="FormatoEntrada(this, 1)" onblur="FormatoSalida(this,4,1)" onchange="RequerirAjuste()" onkeyup="CalcularSerie(this,' + x + ',2)" class="form-control sinbordecon text-XX text-center bg-amarillo" name="Lecturas"/></th>' +
            '<th>' +
            ' <select style="width:100%" id="Patron' + x + '" required name="Patron" onchange="CambioPatron(' + x + ')">' +
            patrones +
            '    </select>' +
            '</th>' +
            '<th class="text-center"><input type="text" id="Serie1' + x + '" required onfocus="FormatoEntrada(this, 1)" onblur="FormatoSalida(this,4,1)" onchange="RequerirAjuste()" onkeyup="CalcularSerie(this,' + x + ',1)" class="form-control sinbordecon text-XX text-center bg-amarillo" Name="Serie1" /></th>' +
            '<th class="text-center"><input type="text" id="Serie2' + x + '" required onfocus="FormatoEntrada(this, 1)" onblur="FormatoSalida(this,4,1)" onchange="RequerirAjuste()" onkeyup="CalcularSerie(this,' + x + ',1)" class="form-control sinbordecon text-XX text-center bg-amarillo" Name="Serie2" /></th>' +
            ' <th class="text-center"><input type="text"id="Serie3' + x + '" required onfocus="FormatoEntrada(this, 1)" onblur="FormatoSalida(this,4,1)" onchange="RequerirAjuste()" onkeyup="CalcularSerie(this,' + x + ',1)" class="form-control sinbordecon text-XX text-center bg-amarillo" Name="Serie3" /></th>' +
            ' <th class="text-center bg-gris text-XX" id="Promedio' + x + '">&nbsp;</th>' +
            '<th class="text-center bg-gris text-XX" id="Desviacion' + x + '">&nbsp;</th>' +
            '<th class="text-center bg-gris text-XX" id="Porcentaje' + x + '">&nbsp;</th>' +
            '</tr>';
        

        resultado2 += '<tr class="tabcertr">' +
            '<th class="text-center" id = "CoPorLectura' + x + '" >&nbsp;</th>' +
            '   <th class="text-center bg-gris text-XX" id="CoLectura' + x + '">&nbsp;</th>' +
            '   <th id="CoPatron' + x + '">&nbsp;</th>' +
            '   <th class="text-center" id="CoSerie1' + x + '">&nbsp;</th>' +
            '   <th class="text-center" id="CoSerie2' + x + '">&nbsp;</th>' +
            '   <th class="text-center" id="CoSerie3' + x + '">&nbsp;</th>' +
            '   <th class="text-center bg-gris text-XX" id="CoPromedio' + x + '">&nbsp;</th>' +
            '   <th class="text-center bg-gris text-XX" id="CoDesviacion' + x + '">&nbsp;</th>' +
            '  <th class="text-center bg-gris"><input type="text" name="Porcentaje" id="CoPorcentaje' + x + '" readonly class="form-control sinbordecon text-XX text-center"/></th>' +
            '</tr>';
    }

    $("#TBSerie").html(resultado);
    $("#TBCalculo").html(resultado2);
    $('select').select2();
}

function ReportarEquipo() {
    if (Ingreso == 0)
        return false;
    var datos = LlamarAjax("Laboratorio/BuscarReportesLab", "ingreso=" + Ingreso);
    if (datos != "[]") {
        var datarep = JSON.parse(datos);
        for (var x = 0; x < datarep.length; x++) {
            IdReporte = datarep[x].nroreporte * 1;
            $("#IdReporte" + IdReporte).val(datarep[x].id);
            $("#Concepto" + IdReporte).val(datarep[x].idconcepto).trigger("change");
            $("#Ajuste" + IdReporte).val(datarep[x].ajuste);
            $("#Suministro" + IdReporte).val(datarep[x].suministro);
            $("#Observacion" + IdReporte).val(datarep[x].observaciones);
            $("#Conclusion" + IdReporte).val(datarep[x].conclusion);
            $("#NroInforme" + IdReporte).val(datarep[0].informetecnico)
            $("#registroingreso" + IdReporte).html("<b>Registrado el día " + datarep[x].fecha + " por el técnico " + datarep[x].usuario + "</b>" + datarep[x].aprobado);
            if (datarep[x].idconcepto * 1 == 5 || datarep[x].idconcepto * 1 == 3 )
                MostrarFoto(IdReporte, 1);
        }
    }
    $('#tabreportaringreso a[href="#Reporte1"]').tab('show')
    NroReporte = 1;
    $("#ReportarEquipo").modal("show");
    $("#Concepto1").focus();
}

$("#formreportar").submit(function (e) {
    e.preventDefault();
    var idconcepto = $("#RConcepto").val() * 1;
    var concepto = $("#RConcepto option:selected").text();
    var ajuste = $.trim($("#RAjuste").val());
    var suministro = $.trim($("#RSuministro").val());

    if (idconcepto == 2 && ajuste == "" && suministro == "") {
        $("#RAjuste").focus();
        swal("Acción Cancelada", "Debe ingresar la descripción del ajuste y/ó suministro", "warning");
        return false;
    }

    var parametros = $("#formreportar").serialize() + "&concepto=" + concepto + "&idconcepto=" + idconcepto + "&ingreso=" + Ingreso + "&fecha=" + $("#FechaRec").val();
    var datos = LlamarAjax("Laboratorio/ReportarIngresoOpera", parametros).split("|");
    if (datos[0] == "0") {
        swal("", datos[1], "success");
        $("#idreporte").val(datos[2]);
        var numero = Ingreso;
        Ingreso = 0;
        ConsularReportesIngresos();
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
});

function ModalCotizacion() {
    if (Ingreso == 0)
        return false;
    $("#modalcotizacion").modal("show");
}

function EnviarReporte(reporte) {

    var id = $("#IdReporte" + reporte).val() * 1;
    if (Ingreso == 0 || id == 0)
        return false;



    if (id == 0) {
        swal("Acción Cancelada", "Debe primero guardar el reporte", "warning");
        return false;
    }

    var concepto = $("#Concepto").val() * 1;
    if (reporte > 0)
        concepto = $("#Concepto" + reporte).val() * 1;
    if (concepto != 2) {
        swal("Acción Cancelada", "Solo se podrá enviar reportes técnicos cuando el ingreso requiere ajuste o suministro", "warning");
        return false;
    }

    var datos = LlamarAjax('Cotizacion/ModalEmpresa', "ingreso=" + Ingreso);
    var data = JSON.parse(datos);

    CorreoEmpresa = data[0].email;
    Empresa = data[0].cliente;

    if (ValidarCorreo(CorreoEmpresa) == 2) {
        swal("Acción Cancela", "el contacto " + data[0].contacto + " no posee correo válido registrado <br>Llame a comercial para registrar el correo del contacto", "warning");
        return false;
    }
    $("#rCliente").val(Empresa);
    $("#rCorreo").val(CorreoEmpresa);

    $("#rObserEnvio").val("");
    $("#rspreporte").html(Ingreso);
    $("#rReporte").val(reporte);
    $("#enviar_reporte").modal("show");
}

$.connection.hub.start().done(function () {

    $(".guardarreporte").click(function () {
        var reporte = NroReporte;
        var IdReporte = $("#IdReporte" + reporte).val() * 1;
        var idconcepto = $("#Concepto" + reporte).val() * 1;
        var ajuste = $.trim($("#Ajuste" + reporte).val());
        var suministro = $.trim($("#Suministro" + reporte).val());
        var observacion = $.trim($("#Observacion" + reporte).val());
        var conclusion = $.trim($("#Conclusion" + reporte).val());
        var concepto = $("#Concepto" + reporte + " option:selected").text();
        var fecha = $("#FechaRec").val();
        if (Ingreso == 0)
            return false;
        var mensaje = "";

        if ((idconcepto == 3 || idconcepto == 4) && observacion == "") {
            $("#Observacion" + reporte).focus();
            mensaje = "<br> Debe de ingresar la observación del por qué no se puede calibrar";
        }
        if (idconcepto == 3 && conclusion == "") {
            $("#Conclusion" + reporte).focus();
            mensaje = "<br> Debe de ingresar la conclusión del por qué no se puede calibrar" + mensaje;
        }

        if (idconcepto == 5 && conclusion == "") {
            $("#Conclusion" + reporte).focus();
            mensaje = "<br> Debe de ingresar la conclusión del informe de mantenimiento" + mensaje;
        }

        if (idconcepto == 2 && ajuste == "" && suministro == "") {
            $("#Ajuste" + reporte).focus();
            mensaje = "<br> Debe de ingresar la observación del juste o suministro" + mensaje;
        }
        if (idconcepto == 0) {
            $("#Concepto" + reporte).focus()
            mensaje = "<br> Debe de seleccionar un concepto " + mensaje;
        }

        if (mensaje != "") {
            swal("Acción Cancelada", mensaje, "warning");
            return false;
        }

        var parametros = "ingreso=" + Ingreso + "&idconcepto=" + idconcepto + "&concepto=" + concepto + "&ajuste=" + ajuste + "&suministro=" + suministro + "&id=" + IdReporte + "&observacion=" + observacion + "&reporte=" + reporte + "&fecha=" + fecha + "&conclusion=" + conclusion;
        var datos = LlamarAjax("Laboratorio/ReportarIngreso", parametros).split("|");
        if (datos[0] == "0") {
            notireportados.server.send(Ingreso);
            ConsularReportesIngresos();
            if (idconcepto == 1 || idconcepto == 4) {
                LimpiarTodo();
                $("#IngresoParTor").focus();
         
            } else {
                $("#IdReporte" + reporte).val(datos[2]);
            }
            swal("", datos[1], "success");
        } else {
            swal("Acción Cancelada", datos[1], "warning");
        }
    });

});

$("#formenviarreporte").submit(function (e) {

    e.preventDefault();
    var observacion = $.trim($("#rObserEnvio").val())
    var correo = $.trim($("#rCorreo").val());
    a_correo = correo.split(";");
    var reporte = $("#rReporte").val() * 1;

    for (var x = 0; x < a_correo.length; x++) {
        if (ValidarCorreo($.trim(a_correo[x])) == 2) {
            $("#rCorreo").focus();
            swal(ValidarTraduccion("Acción Cancelada"), ValidarTraduccion("Formato de correo inválido") + ":" + a_correo[x], "warning");
            return false;
        }
    }

    ActivarLoad();
    setTimeout(function () {
        var parametros = "ingreso=" + Ingreso + "&reporte=" + reporte + "&principal=" + CorreoEmpresa + " &e_email=" + correo + " &cliente=" + Empresa + " &observacion=" + observacion;
        var datos = LlamarAjax("Laboratorio/EnvioReporte", parametros);
        DesactivarLoad();
        datos = datos.split("|");
        if (datos[0] == "0") {
            swal("", ValidarTraduccion(datos[1]) + " " + datos[2], "success");
            $("#enviar_reporte").modal("hide");
        } else
            swal("Acción Cancelada", datos[1], "warning");
    }, 15);

    return false;
});

function MostrarFoto(reporte, numero) {
    var informe = $("#NroInforme" + reporte).val() * 1;
    $("#FotoMan" + reporte).attr("src", url + "imagenes/informemantenimineto/" + informe + "/" + numero + ".jpg");
}


function GuardarDocumento(reporte) {
    var documento = document.getElementById("Documento" + reporte);
    var informe = $("#NroInforme" + reporte).val() * 1;
    var numero = $("#NumFoto" + reporte).val() * 1;


    if ($.trim(documento.value) != "") {

        if (informe == 0) {
            swal("Acción Cancelada", "Debe generar primero el informe técnico", "warning");
            return false;
        }

        var file = documento.files[0];
        var data = new FormData();
        data.append('documento', file);
        var direccion = url + "Cotizacion/AdjuntarFoto";
        $.ajax({
            url: direccion,
            type: 'POST',
            contentType: false,
            data: data,
            processData: false,
            async: false,
            success: function (datos) {
                datos = datos.split("|");
                if (datos[0] == "0") {
                    datos = LlamarAjax("Laboratorio/SubirFoto", "ingreso=" + Ingreso + "&informe=" + informe + "&numero=" + numero + "&foto=" + datos[1]).split("|");
                    swal(datos[1]);
                    if (datos[0] == "0")
                        MostrarFoto(reporte, numero)
                } else {
                    swal(ValidarTraduccion(datos[1]), "", "error");
                }
            }
        });
    }
}

$('select').select2();

$("#Concepto").select2('destroy'); 