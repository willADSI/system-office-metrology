﻿d = new Date();
month = d.getMonth() + 1;
day = d.getDate();

var Anio = $.trim(d.getFullYear());
Anio = $.trim(Anio.substr(2, 2))


output = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-01';

output2 = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-' +
    (day < 10 ? '0' : '') + day;

$("#CFechaDesde").val(output);
$("#CFechaHasta").val(output2);

$("#AFechaEnt").val(output2);
var PermisioEspecial = PerGeneSistema("ENTREGAR CON SALDO PENDIENTE");

var datos = LlamarAjax("Configuracion","opcion=CargaComboInicial&tipo=1").split("||");

$("#CMagnitud").html(datos[0]);
$("#CEquipo").html(datos[1]);
$("#CMarca").html(datos[2]);
$("#CCliente").html(datos[3]);
$("#CModelo").html(datos[4]);
$("#CIntervalo").html(datos[5]);
$("#CUsuario").html(datos[6]);
$("#AEntrega").html(datos[6] + "<option value='1000'>CLIENTE</option><option value='1000'>MOVIL-CLIENTE</option><option value='1000'>ENVIO</option><option value='1000'>CORREO CERTIFICADO</option>");
$("#AEmpresaEnv").html(datos[7]);

var IdCliente = 0;
var totales = 0;
var Orden = 0;
var DocumentoCli = "";
var Ingresos = "";
var Devolucion = 0;
var IdDevolucion = 0;
var RetirarSinFac = "";
var ArchivosPDF = "";
var Entregado = 0;
var IdSede = 0;
var IdContacto = 0;
var CorreoEmpresa = "";
var IdEmpresaEnv = 0;
var a_Certificados = "";
var FotoDev = 0;
var FotoEntrega = 0;
var Enviar_Certificado = "NO";
tb = $('input');
$(tb).keypress(enter2tab);

tb = $('select');
$(tb).keypress(enter2tab);

function enter2tab(e) {
    if (e.keyCode == 13) {
        id = $(this).attr("id");

        cb = parseInt($(this).attr('tabindex'));

        if (id == "Cliente") {
            if ($.trim($(this).val()) != "") {

                BuscarCliente($(this).val());
                $("#Sede").focus();
                return false;
            }
        }

        if ($(':input[tabindex=\'' + (cb + 1) + '\']') != null) {
            $(':input[tabindex=\'' + (cb + 1) + '\']').focus();
            $(':input[tabindex=\'' + (cb + 1) + '\']').select();
            e.preventDefault();

            return false;
        }

        if ($(':select[tabindex=\'' + (cb + 1) + '\']') != null) {
            console.log("1");
            $(':select[tabindex=\'' + (cb + 1) + '\']').focus();
            $(':select[tabindex=\'' + (cb + 1) + '\']').select();
            e.preventDefault();

            return false;
        }
    }
}




function LimpiarTodo() {
    LimpiarDatos();
    $("#Cliente").val("");
    $("#Cliente").focus();
    $("#Sitio").val("NO").trigger("change");
}

LimpiarTodo();

function DatosContacto(contacto) {
    $("#Email").val("");
    $("#Telefonos").val("");
    if (contacto * 1 == 0)
        return false;
    var datos = LlamarAjax("Cotizacion","opcion=DatosContacto&id=" + contacto);
    if (datos != "[]") {
        var data = JSON.parse(datos);
        $("#Email").val(data[0].email);
        $("#Telefonos").val(data[0].telefonos);
        CorreoEmpresa = data[0].email;
    }
}

function LimpiarDatos() {

    IdCliente = 0;
    DocumentoCli = "";
    TipoCliente = 0;
    IdDevolucion = 0;
    PlazoPago = 0;
    TipoPrecio = 0;
    Devolucion = 0; 
    totales = 0;
    TotalSeleccion = 0;
    IdEmpresaEnv = 0;
    Orden = 0;
    RetirarSinFac = "";
    CorreoEmpresa = ""
    $("#seleccionatodo").prop("checked", "");
    $("#SaldoClienteDev").html("");
    $("#divAnulaciones").html("");
    $("#Sitio").attr("disabled", false);
    
    $("#NombreCliente").val("");
    $("#Devolucion").val("");
    $("#Direccion").val("");

    $("#RPersona").val("");
    $("#RObservacion").val("");
    $("#RFecha").val("");

    $("#Sede").html("");
    $("#Contacto").html("");
    $("#FechaReg").val("");
    $("#UsuarioReg").val("");
    $("#Observaciones").val("");

    $("#Devolucion").attr("disabled", false);

    $("#Ciudad").val("");
    $("#Departamento").val("");

    $("#PlazoPago").val("0");
    $("#Totales").val("0");

    $("#bodyIngreso").html("");

    $("#Email").val("");
    $("#Telefonos").val("");
    $("#TipoCliente").val("");
    $("#REmpresa").val("");
    $("#RGuia").val("");

}

function ValidarCliente(documento) {
    if ($.trim(DocumentoCli) != "") {
        if (DocumentoCli != documento) {
            DocumentoCli = "";
            LimpiarDatos();
        }
    }
}

function BuscarDevolucion(numero) {
    if (Devolucion == numero)
        return false;
    LimpiarTodo();
    if ((numero * 1) == 0)
        return false;



    var parametros = "devolucion=" + numero;
    var datos = LlamarAjax('Cotizacion','opcion=BuscarDevolucion&'+parametros);
    var Anulacion = "";

    if (datos != "[]") {
        var data = JSON.parse(datos);
        IdDevolucion = data[0].id;
        Devolucion = data[0].devolucion;
        Entregado = data[0].entregado;
        IdSede = data[0].idsede;
        IdEmpresaEnv = data[0].idempresa;
        FotoDev = data[0].fotos * 1;
        FotoEntrega = data[0].fotoentrega*1;

        if (data[0].fechaanulacion) {
            Anulacion = "<span class='text-XX text-danger'>Devolución Anulada el día: " + data[0].fechaanulacion + ", por el <b>Asesor</b>: " + data[0].usuarioanula + ", <b>Observación:</b> " + data[0].observacionanula + "</span>";
            $("#divAnulaciones").html(Anulacion);
        }
       
                
        $("#RPersona").val((data[0].nombreentrega ? data[0].nombreentrega : ""));
        $("#RObservacion").val((data[0].observacionentrega ? data[0].observacionentrega : ""));
        $("#RFecha").val((data[0].fechaentrega ? data[0].fechaentrega : ""));
        $("#RGuia").val((data[0].fechaentrega ? data[0].guia : ""));
        $("#REmpresa").val((data[0].fechaentrega ? data[0].empresa_envia : ""));

        IdContacto = data[0].idcontacto;
        $("#Cliente").val(data[0].documento);
        BuscarCliente(data[0].documento);
        $("#Sede").val(IdSede).trigger("change");
        $("#Contacto").val(IdContacto).trigger("change");
        $("#Sitio").val(data[0].sitio).trigger("change");
        $("#Devolucion").val(Devolucion);
        $("#FechaReg").val(data[0].fecha);
        $("#UsuarioReg").val(data[0].usuario);
        $("#Observaciones").val(data[0].observacion);
        $("#seleccionatodo").prop("checked", "checked");
        $("#Devolucion").attr("disabled", true);
        
        SeleccionarTodo();

        SeguimientoCartera(numero);

    } else {
        $("#Devolucion").focus();
        swal("Acción Cancelada", "Devolución número " + numero + " no registrada", "warning");
    }
}


function BuscarCliente(documento) {

    if ($.trim(documento) == "")
        return false;

    if (DocumentoCli == documento)
        return false;

    DocumentoCli = documento;

    var parametros = "documento=" + documento;
    var datos = LlamarAjax('Cotizacion','opcion=BuscarCliente&'+parametros);
    datos = datos.split("|");
    if (datos[0] == "0") {
        var data = JSON.parse(datos[1]);
        DesactivarLoad();
        IdCliente = data[0].id;
        PlazoPago = data[0].plazopago;
        TipoCliente = data[0].tipocliente;
        RetirarSinFac = data[0].retirarsinfac;
        Enviar_Certificado = data[0].enviar_certificado;
        CorreoEmpresa = data[0].email;
        $("#TipoCliente").val(TipoCliente);
        $("#NombreCliente").val(data[0].nombrecompleto);
        $("#Ciudad").val(data[0].departamento + "/" + data[0].ciudad);
        $("#Contacto").html(datos[2]);
        $("#Sede").html(datos[3]);
        $("#Sede").focus();
        $("#Sitio").attr("disabled", true);
        TablaTemporalDevolucion(IdCliente);
        if (totales == 0) {
            $("#Cliente").select();
            $("#Cliente").focus();
            swal("Acción Cancelada", "Este cliente no posee ingresos recibido en logística para realizar devoluciones", "warning")
        } else {
            SaldoActual(IdCliente, 1);
        }
        $("#SaldoClienteDev").html(SaldoTotal(IdCliente));
        
    } else {
        DocumentoCli = "";
        $("#Cliente").val("");
        $("#Cliente").focus();
        swal("Acción Cancelada", "El documento " + documento + ", no se encuentra registrado como cliente", "warning");
    }
}

function AnularDevolucion() {

    if (IdDevolucion == 0)
        return false;

    var resultado = "";
    swal({
        title: 'Advertencia',
        text: '¿' + ValidarTraduccion('Desea Anular la Devolición') + ' ' + Devolucion + ' ' + ValidarTraduccion('del cliente') + ' ' + $("#NombreCliente").val(),
        input: 'text',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Si'),
        cancelButtonText: ValidarTraduccion('No'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',

        inputValidator: function (value) {
            return new Promise(function (resolve, reject) {
                if (value) {
                    var datos = LlamarAjax("Cotizacion","opcion=AnularDevolucion&id=" + IdDevolucion + "&observaciones=" + value)
                    datos = datos.split("|");
                    if (datos[0] == "0") {
                        resultado = Devolucion;
                        resolve()
                    } else {
                        reject(datos[1]);
                    }

                } else {
                    reject(ValidarTraduccion('Debe de ingresar un correo electrónico'));
                }
            })
        }
    }).then(function (result) {
        LimpiarTodo();
        IdCliente = 0;
        $("#Cliente").val("");
        $("#Cliente").focus();
        swal({
            type: 'success',
            html: 'Devolución Nro ' + resultado + ' anulada con éxito'
        })
    })

}

function TablaTemporalDevolucion(cliente) {
    $("#bodyIngreso").html("");
    var sitio = $("#Sitio").val();
    var datos = LlamarAjax("Cotizacion","opcion=TemporalDevolucion&cliente=" + cliente + "&devolucion=" + Devolucion + "&sitio=" + sitio);
    var resultado = "";
    totales = 0;
    var estado = "";
    if (datos != "[]") {
        eval("data=" + datos);
        for (var x = 0; x < data.length; x++) {
            resultado += "<tr id='fila-" + x + "'>" +
                "<td class='text-center'><b>" + (x + 1) + "</b><br> <input class='form-control' type='checkbox' value='" + data[x].ingreso + "' name='seleccion[]' id='seleccionado" + x + "' onchange='SeleccionarFila(" + x + ")'></td>" +
                "<td><b>" + data[x].ingreso + "</b><br>" + data[x].fechaing + "<br>" + data[x].usuario + "</td>" +
                "<td><b>" + data[x].magnitud + "</b><br>" + data[x].equipo + "<br>" + data[x].intervalo + "</td>" +
                "<td><b>" + data[x].marca + "</b><br>" + data[x].modelo + "</td>" +
                "<td>" + data[x].serie + "</td>" +
                "<td align='center' class='text-XX'>" + data[x].ubicacion + "</td>" +
                "<td class='text-center'><input type='text' name='Certificados[]' " + ($.trim(data[x].certificado) != "" || $.trim(data[x].informetecnico) != "" ? "readonly" : "") + " class='text-center text-XX sinborde' style='width:99%' value = '" + (IdDevolucion > 0 ? data[x].certificadodevo : data[x].certificado) + "'>" +
                ($.trim(data[x].certificado) != "" ? "<br><a href='javascript:VerCertificados(" + data[x].ingreso + ")'>Ver Detalle</a>" : ($.trim(data[x].informetecnico) != "" ? "<br><a href='javascript:VerInforme(" + data[x].informetecnico + ")'>Ver Informe " + data[x].informetecnico + "</a>" : "")) + "</td>" +
                "<td>" + data[x].observacion + "</td>" +
                "<td><textarea name='Observaciones[]' class='form-control sinborde' rows='4'>" + data[x].observadev + "</textarea><input type='hidden' name='Informes[]' value='" + data[x].informetecnico + "'><input type='hidden' name='Certificadospdf[]' value='" + data[x].certificadospdf + "'></td>" +
                "<td align='center'><button class='verfotos btn btn-success' title='Ver Fotos' type='button' onclick='CargarModalFoto(" + data[x].ingreso + "," + data[x].fotos + ",1)'><span data-icon='&#xe2c7;'></span></button></td></tr>";
            totales += data[x].cantidad;

        }
    }

    $("#bodyIngreso").html(resultado);
    $("#Totales").val(totales);

}

function VerCertificados(ingreso) {
    $("#titulodetalle").html("CERTIFICADO DEL INGRESO NRO " + ingreso);
    
    var datos = LlamarAjax("Laboratorio","opcion=DetalleIngreso&opciones=6&ingreso=" + ingreso);
    $("#detallemovimiento").html(datos);

    $("#modalDetalleDevo").modal("show");
    
}

function ImprimirCertificado(numero) {
    window.open(url_cliente + "Certificados/" + numero + ".pdf?id=" + NumeroAleatorio());
}

function SeleccionarTodo() {

    if ($("#seleccionatodo").prop("checked")) {
        
        for (var x = 0; x < totales; x++) {
            if ($("#seleccionado" + x).prop("disabled") == false) {
                $("#fila-" + x).addClass("bg-gris");
                $("#seleccionado" + x).prop("checked", "checked");
            }
        }
    } else {
        for (var x = 0; x < totales; x++) {
            $("#fila-" + x).removeClass("bg-gris");
            $("#seleccionado" + x).prop("checked", "");
        }
    }
}

function SeleccionarFila(x) {
    
    if ($("#seleccionado" + x).prop("checked")) {
        $("#fila-" + x).addClass("bg-gris");
    } else {
        $("#fila-" + x).removeClass("bg-gris");
    }

}


function VerCertificado(certificado) {
    window.open(url_cliente + "Certificados/" + certificado + ".pdf?id=" + NumeroAleatorio());
}



$("#tablamodalclientedev > tbody").on("click", "tr", function (e) {
    var row = $(this).parents("td").context.cells;
    var numero = row[1].innerText;
    $("#modalClientesDev").modal("hide");
    $("#Cliente").val(numero);
    BuscarCliente(numero);
});



function GuardarDevolucion() {
    var sede = $("#Sede").val() * 1;
    var contacto = $("#Contacto").val() * 1;
    var mensaje = "";

    if (IdCliente == 0)
        return false;

    if (contacto == 0) {
        $("#Contacto").focus();
        mensaje = "<br> Debe de seleccionar un contacto del cliente"
    }
    if (sede == 0) {
        $("#Sede").focus();
        mensaje = "<br> Debe de seleccionar una sede del cliente " + mensaje;
    }

    if (mensaje != "") {
        swal("Acción Cancelada", mensaje, "warning");
        return false;
    }

    var seleccionado = 0;
    var observaciones = $.trim($("#Observaciones").val());
    var ingresos = document.getElementsByName("seleccion[]");
    var observacion = document.getElementsByName("Observaciones[]");
    var certificado = document.getElementsByName("Certificados[]");
    var informe = document.getElementsByName("Informes[]");
    var sitio = $("#Sitio").val();
    var a_ingreso = "array[";
    var a_observacion = "array[";
    var a_certificado = "array[";
    for (var x = 0; x < ingresos.length; x++) {
        if (ingresos[x].checked) {
            if ($.trim(observacion[x].value) == "") {
                swal("Acción Cancelada", "Debe de ingresar la observación del ingreso número " + ingresos[x].value, "warning");
                return false;
            }
            if ($.trim(certificado[x].value) == "" && $.trim(informe[x].value) == "") {
                swal("Acción Cancelada", "Debe de ingresar el certificado ó informe del ingreso número " + ingresos[x].value, "warning");
                return false;
            }
            if (a_ingreso == "array[") {
                a_ingreso += ingresos[x].value;
                a_observacion += "'" + $.trim(observacion[x].value) + "'";
                a_certificado += "'" + $.trim(certificado[x].value) + "'"; 
            } else {
                a_ingreso += "," + ingresos[x].value;
                a_observacion += ", '" + $.trim(observacion[x].value) + "'"; 
                a_certificado += ",'" + $.trim(certificado[x].value) + "'"; 
            }
            seleccionado += 1;
        }
    }
    a_ingreso += "]";
    a_observacion += "]";
    a_certificado += "]";

    if (seleccionado == 0) {
        swal("Acción Cancelada", "Debe de seleccionar por lo mínimo un ingreso", "warning");
        return false;
    }
    var mensaje = "";
    var parametros = "id=" + IdDevolucion + "&cliente=" + IdCliente + "&a_ingreso=" + a_ingreso + "&a_observacion=" + a_observacion + "&observacion=" + observaciones +
        "&sede=" + sede + "&contacto=" + contacto + "&a_certificados=" + a_certificado + "&sitio=" + sitio;
    var datos = LlamarAjax("Cotizacion", "opcion=GuardarDevolucion&"+parametros).split("|");
    if (datos[0] == 0) {
        Devolucion = datos[2] * 1;
        ImprimirDevolucion(datos[2], 0);
        if (Enviar_Certificado == "SI") {
            EnviarCertificado();
            mensaje = "<br><b>Debe de enviar el certificado por correo</b>";
        }
        swal("", datos[1] + " " + datos[2] + mensaje, "success");
        LimpiarTodo();
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
}

function ImprimirDevolucion(numero, tipo) {
    if (numero * 1 == 0)
        numero = Devolucion;
    if (numero * 1 != 0) {
        ActivarLoad();
        setTimeout(function () {
            var datos = LlamarAjax("Configuracion", "opcion=GenerarPDF&tipo=Devolucion&documento=" + numero).split("||");
            DesactivarLoad();
            if (datos[0] == "0") {
                if (tipo == 0)
                    window.open(url_cliente + "DocumPDF/" + datos[1]);
                else {
                    $("#resultadopdfdevolucion").attr("src", url_cliente + "DocumPDF/" + datos[1]);
                    Pdf_Remision = datos[1];
                }
            } else {
                swal("Acción Cancelada", ValidarTraduccion(datos[1]), "warning");
            }
        }, 15);
    }
}

function ImprimirEtiqueta() {
    if (Devolucion * 1 != 0) {
        ActivarLoad();
        setTimeout(function () {
            var datos = LlamarAjax("Configuracion", "opcion=GenerarPDF&tipo=EtiquetaDevolucion&documento=" + Devolucion).split("||");
            DesactivarLoad();
            if (datos[0] == "0") {
                window.open(url_cliente + "DocumPDF/" + datos[1]);
            } else {
                swal("Acción Cancelada", ValidarTraduccion(datos[1]), "warning");
            }
        }, 15);
    }
}

function EntregarDevolucion() {
    var saldo = SaldoActual(IdCliente, 0);
    var gestion = $("#IdMenSeguiCart").val() * 1;
    if (saldo > 0 && RetirarSinFac == "NO" && PermisioEspecial == 0 && gestion == 0) {
        swal("Acción Cancelada", "No se puede entregar los equipos porque el Cliente posee un saldo de <b>" + formato_numero(saldo, 0,_CD,_CM,"") + "</b> por facturas vencidas... Diríjase a cartera para gestionar el pago", "warning");
        return false
    }

    $("#ACliente").val($("#NombreCliente").val());
    $("#ASede").val($("#Sede option:selected").text());
    $("#AContacto").val($("#Contacto option:selected").text());
    $("#ADevolucion").val(Devolucion);
    $("#AAsesor").val($("#UsuarioReg").val());
    $("#AFecha").val($("#FechaReg").val());

    $("#AEntrega").val("");
    $("#AObservacion").val("");

    $("#entregar_devolucion").modal("show");
}

function GuardarEntDevolucion() {
    var persona = $("#AEntrega option:selected").text();
    var fecha = $("#AFechaEnt").val();
    var archivo = $("#AArchivo").val();
    var empresa = $("#AEmpresaEnv").val() * 1;
    var guia = $.trim($("#AGuia").val());
    
    var observacion = $("#AObservacion").val();

    if (persona == "--Seleccione--") {
        $("#AEntrega").focus();
        swal("Acción Cancelada", "Debe de ingresar la persona que se el entrega la devolución", "warning");
        return false;
    }

    if (persona == "ENVIO") {
        if (empresa == "" && guia == "") {
            $("#AEmpresaEnv").focus();
            swal("Acción Cancelada", "Debe de ingresar la empresa y guía de envío", "warning");
            return false;
        }
    }

    if (fecha == "") {
        $("#AFechaEnt").focus();
        swal("Acción Cancelada", "Debe de ingresar la fecha de entrega", "warning");
        return false;
    }

    if (archivo == "") {
        $("#AArchivo").focus();
        swal("Acción Cancelada", "Debe de examinar la devolución firmada", "warning");
        return false;
    }

    var datos = LlamarAjax("Cotizacion","opcion=EntregarDevolucion&devolucion=" + Devolucion + "&persona=" + persona + "&observacion=" + observacion + "&fecha=" + fecha +
        "&empresa=" + empresa + "&guia=" + guia).split("|");
    if (datos[0] == "0") {
        $("#entregar_devolucion").modal("hide");
        $("#RPersona").val(persona);
        $("#RObservacion").val(observacion);
        $("#RFecha").val(datos[2]);
        $("#RGuia").val(guia);
        IdEmpresaEnv = empresa;
        $("#REmpresa").val($("#AEmpresaEnv option:selected").text());
        swal("", datos[1], "success");
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
}

function GuardarDocumento() {
    var documento = document.getElementById("AArchivo");
    if ($.trim(documento.value) != "") {

        var file = documento.files[0];
        var data = new FormData();
        data.append('documento', file);
        var resultado = '';
        var direccion = url + "Cotizacion/AdjuntarPDF";
        $.ajax({
            url: direccion,
            type: 'POST',
            contentType: false,
            data: data,
            processData: false,
            async: false,
            success: function (datos) {
                datos = datos.split("|");
                if (datos[0] != "0") {
                    swal(ValidarTraduccion("Error al cargar el archivo"), "", "error");
                } else {
                    if (datos[2] != "XX")
                        swal(ValidarTraduccion(datos[2]), "", "error");
                }
            }
        });
    }
}

function EnviarDevolucion() {

    if (Devolucion == 0)
        return false;

    
    if (CorreoEmpresa == "") {
        swal("Acción Cancelada", "Cliente no posee ningún correo principal registrado", "warning");
        return false;
    }

    $("#eCliente").val($("#NombreCliente").val());
    $("#eCorreo").val($("#Email").val());
    ImprimirDevolucion(Devolucion, 1);

    $("#spremision").html(Devolucion);
    $("#enviar_devolucion").modal("show");
}

function EnviarCertificado() {
    if (Devolucion == 0)
        return false;
    var saldo = SaldoActual(IdCliente, 0);
    var gestion = $("#IdMenSeguiCart").val() * 1;
    if (saldo > 0 && RetirarSinFac == "NO" && gestion == 0) {
        swal("Acción Cancelada", "No se puede entregar los equipos porque el Cliente posee un saldo de <b>" + formato_numero(saldo, 0,_CD,_CM,"") + "</b> por facturas vencidas... Diríjase a cartera para gestionar el pago", "warning");
        return false
    }
    ArchivosPDF = "";
    var cantidad = 0;
    var resultado = '<div class="tabbable page-tabs">' +
        '<ul class="nav nav-tabs">';
    var resultado2 = "";

    if (CorreoEmpresa == "") {
        swal("Acción Cancelada", "Cliente no posee ningún correo principal registrado", "warning");
        return false;
    }

    var ingresos = document.getElementsByName("seleccion[]");
    var informe = document.getElementsByName("Informes[]");
    var certificadopdf = document.getElementsByName("Certificadospdf[]");
    var certificado = document.getElementsByName("Certificados[]");
    a_Certificados = "";
    var valor = "";
    var valor2 = "";

    var datos;
    var datacer;
    var impreso;
    var numcertificado;
    
    for (var x = 0; x < ingresos.length; x++) {
        if (ingresos[x].checked) {
            if (informe[x].value * 1 == 0) {
                arreglo = $.trim(certificadopdf[x].value) + "//";
                arreglo = arreglo.split("//");
                arreglo2 = $.trim(certificado[x].value) + "/";
                arreglo2 = arreglo2.split("/");
                for (var y = 0; y < arreglo.length; y++) {
                    valor = $.trim(arreglo[y]);
                    valor2 = $.trim(arreglo2[y]);
                    if (valor2 != "") {
                        datos = LlamarAjax("Cotizacion","opcion=TipoCertificado&Certificado=" + valor2);
                        if (datos != "[]") {
                            datacer = JSON.parse(datos);
                            impreso = datacer[0].impreso * 1;
                            numcertificado = datacer[0].numero;
                            if (impreso == 1)
                                valor = numcertificado;
                            if (cantidad == 0) {

                                resultado += '<li class="active"><a href="#tabcertificado' + x + '" data-toggle="tab"><i class="icon-checkbox-partial"></i>' + valor2 + '</a></li>'
                                resultado2 += '<div class="tab-pane active fade in" id="tabcertificado' + x + '">'
                                ArchivosPDF = valor;
                                a_Certificados = valor2 + ".pdf";
                            } else {
                                resultado += '<li><a href="#tabcertificado' + x + '" data-toggle="tab"><i class="icon-checkmark3"></i>' + valor2 + '</a></li>';
                                resultado2 += '<div class="tab-pane fade in" id="tabcertificado' + x + '">'
                                ArchivosPDF += "," + valor;
                                a_Certificados += "," + valor2 + ".pdf";
                            }
                            cantidad++;
                            resultado2 += '<iframe src="' + url_cliente + 'Certificados/' + valor + '.pdf?id=' + NumeroAleatorio() + '" width="100%" height="350px"></iframe>'
                            resultado2 += "</div>";
                        }
                    }
                }
            }
        }
    }

    if (cantidad == 0) {
        swal("Acción Cancelada", "Debe de seleccionar por lo mínimo un certificado", "warning");
        return false;
    }

    resultado = resultado + '</ul><div class="tab-content">' + resultado2 + '</div></div>';

    $("#detcertificados").html(resultado);

    $("#CeCliente").val($("#NombreCliente").val());
    $("#CeCorreo").val($("#Email").val());
    $("#CObservacion").val("")
    
    $("#cespremision").html(Devolucion);
    $("#enviar_certificado").modal("show");
}


$("#formenviardevolucion").submit(function (e) {

    e.preventDefault();

    var correo = $.trim($("#eCorreo").val());
    a_correo = correo.split(";");
    var observacion = $("#ObserEnvio").val();

    if (Devolucion == 0)
        return false;

    for (var x = 0; x < a_correo.length; x++) {
        if (ValidarCorreo($.trim(a_correo[x])) == 2) {
            $("#eCorreo").focus();
            swal(ValidarTraduccion("Acción Cancelada"), ValidarTraduccion("Formato de correo onválido") + ":" + a_correo[x], "warning");
            return false;
        }
    }

    ActivarLoad();
    setTimeout(function () {
        var parametros = "devolucion=" + Devolucion + "&principal=" + CorreoEmpresa + "&e_email=" + correo + " &archivo=" + Pdf_Remision + "&observacion=" + observacion;
        var datos = LlamarAjax("Cotizacion/EnvioDevolucion", parametros);
        DesactivarLoad();
        datos = datos.split("|");
        if (datos[0] == "0") {
            swal("", ValidarTraduccion(datos[1]) + " " + CorreoEmpresa + " " + correo, "success");
            $("#enviar_devolucion").modal("hide");
        } else
            swal("Error", datos[1], "error");
    }, 15);

    return false;
})

$("#formenviarcertificado").submit(function (e) {

    e.preventDefault();

    var correo = $.trim($("#CeCorreo").val());
    a_correo = correo.split(";");
    var observacion = $("#CObserEnvio").val();

    if (Devolucion == 0)
        return false;

    for (var x = 0; x < a_correo.length; x++) {
        if (ValidarCorreo($.trim(a_correo[x])) == 2) {
            $("#CeCorreo").focus();
            swal(ValidarTraduccion("Acción Cancelada"), ValidarTraduccion("Formato de correo onválido") + ":" + a_correo[x], "warning");
            return false;
        }
    }

    ActivarLoad();
    setTimeout(function () {
        var parametros = "devolucion=" + Devolucion + "&principal=" + CorreoEmpresa + "&e_email=" + correo + " &archivos=" + ArchivosPDF + "&pdf=" + a_Certificados + "&observacion=" + observacion;
        var datos = LlamarAjax("Cotizacion/EnvioCertificado", parametros);
        DesactivarLoad();
        datos = datos.split("|");
        if (datos[0] == "0") {
            swal("", ValidarTraduccion(datos[1]) + " " + CorreoEmpresa + " " + correo, "success");
            $("#enviar_devolucion").modal("hide");
        } else
            swal("Error", datos[1], "error");
    }, 15);

    return false;
})

function ConsularDevoluciones() {


    var magnitud = $("#CMagnitud").val() * 1;
    var equipo = $("#CEquipo").val();
    var modelo = $("#CModelo").val();
    var marca = $("#CMarca").val() * 1;
    var intervalo = $("#CIntervalo").val();
    var ingreso = $("#CIngreso").val() * 1;
    var serie = $.trim($("#CSerie").val());
    var entregado = $("#CEntregado").val();
        
    var devolucion = $("#CDevolucion").val() * 1;
    var cliente = $("#CCliente").val() * 1;
    var usuario = $("#CUsuario").val() * 1;

    var fechad = $("#CFechaDesde").val();
    var fechah = $("#CFechaHasta").val();

    if (fechad == "" && fechah == "") {
        swal("", ValidarTraduccion("debe de ingresar una fecha o números de días"), "error");
        return false;
    }

    ActivarLoad();
    setTimeout(function () {
        var parametros = "devolucion=" + devolucion + "&cliente=" + cliente + "&ingreso=" + ingreso +
            "&fechad=" + fechad + "&fechah=" + fechah + "&magnitud=" + magnitud + "&equipo=" + equipo + "&marca=" + marca + "&modelo=" + modelo +
            "&intervalo=" + intervalo + "&serie=" + serie + "&usuario=" + usuario + "&entregado=" + entregado;
        var datos = LlamarAjax("Cotizacion","opcion=ConsultarDevoluciones&"+ parametros);
        DesactivarLoad();

        var datajson = JSON.parse(datos);
        $('#TablaDevoluciones').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "imprimir" },
                { "data": "devolucion", "className": "text-XX" },
                { "data": "documento" },
                { "data": "cliente" },
                { "data": "sede" },
                { "data": "contacto" },
                { "data": "departamento" },
                { "data": "ciudad" },
                { "data": "telefonos" },
                { "data": "email" },
                { "data": "totales", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "fecha" },
                { "data": "entregado" },
                { "data": "usuario" },
                { "data": "anulado" }
            ],

            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy','colvis'
            ]
        });
    }, 15);
}

function VerFotosEntrega() {

    if (Devolucion == 0)
        return false;

    if ($("#RPersona").val() == "") {
        return false;
    }

    if (FotoEntrega == 0) {
        swal("Acción Cancelada", "Esta devolución no fue entregada por la móvil", "warning");
        return false;
    }

        
    var contenedor = "";
    contenedor = "<div class='row'><div class='col-xs-12 col-sm-12'>";
    for (var x = 1; x <= FotoEntrega; x++) {
        contenedor += "<a class='btn btn-primary' href='javascript:CambiarFotoEnt(" + x + ")'>" + x + "</a>&nbsp;";
    }
    contenedor += "</div>" +
        "<div class='col-xs-12 col-sm-12'><img id='FotoEntregas' " + (FotoEntrega > 0 ? "src='" + url_cliente + "imagenes/FotoEntrega/" + Devolucion + "/1.jpg?id=" + NumeroAleatorio() + "'" : "") + " width='90%'></div></div>";
    $("#AlbunFotosEntrega").html(contenedor);

    $("#springresoent").html(Devolucion);
    $("#modalFotosEntrega").modal("show");

}

function VerDevolucionFirma() {
    if ($("#RPersona").val() == "") {
        return false;
    }

    
    if (FotoEntrega > 0) {
        swal("Acción Cancelada", "Esta devolución fue entregada por la móvil", "warning");
        return false;
    }

    window.open(url_cliente + "imagenes/DevoFirma/" + Devolucion + ".pdf?id=" + NumeroAleatorio());
}

function VerGuia() {
    var guia = $("#RGuia").val();
    if (guia == "") {
        return false;
    }
    var pagina = LlamarAjax("Cotizacion/PagEmpresaEnvia", "id=" + IdEmpresaEnv);
    setTimeout(function () {
        window.open(pagina + guia);
    }, 20);
}


$("#TablaDevoluciones > tbody").on("dblclick", "tr", function () {
    var row = $(this).parents("td").context.cells;
    var numero = row[1].innerText;
    $('#tabdevolucion a[href="#tabcrear"]').tab('show')
    BuscarDevolucion(numero);
});


function VerEstadoCuenta() {
    var cliente = $("#NombreCliente").val();
    EstadoCuenta(IdCliente, cliente, CorreoEmpresa, TipoCliente, PlazoPago, CorreoEmpresa);
}

function VerFotosDev() {
	CargarModalFoto(Devolucion, 0, 2);
}
        
function InicializarCamaraDev() {
    if (Devolucion == 0)
        return false;

    if ($("#RPersona").val() != "") {
        swal("Acción Cancelada", "Esta devolución ya fue entregada", "warning");
        return false;
    }

    CargarFotos();
    $("#FotoNumDevolucion").html(Devolucion);
    $("#modal_camara_web").modal("show");
}

function CargarFotos() {
    var fotos = LlamarAjax("Cotizacion","opcion=CantidadFotoDev&devolucion=" + Devolucion) * 1;
    var contenedor = "";
    if (fotos > 0) {
        contenedor = "<div class='row'><div class='col-xs-12 col-sm-12'><label>Número de Foto</label><select id='ComboFoto' class='form-control text-center' style='width:120px' onchange='CambiarFoto2(this.value)'> ";
        for (var x = 1; x <= fotos; x++) {
            contenedor += "<option value='" + x + "'>" + x + "</option>";
        }
        contenedor += "</select></div><hr/><br><div class='col-xs-12 col-sm-12 text-center'>" +
            "<img id='FotoDevolucion2' src='" + url_cliente + "imagenes/Devoluciones/" + Devolucion + "/1.jpg?id=" + NumeroAleatorio() + "' width='90%'></div></div>";
    }
    $("#fotocamdevo").html(contenedor);
}

function EliminarFotoDev() {
    
	ActivarLoad();
	setTimeout(function () {
		datos = LlamarAjax("Cotizacion","opcion=EliminarFotoDev&devolucion=" + Devolucion).split("|");
		if (datos[1] != "X") {
			Fotos = datos[1]*1;
			CargarFotos();
			//CambiarFoto2(datos[1]);
			$("#ComboFoto").val(datos[1]).trigger("change");
			DesactivarLoad();
		}else{
			swal("Acción Cancelada",datos[0],"warning");
			DesactivarLoad();
		}
	},15);
    
}


function CambiarFoto2(numero) {
    $("#FotoDevolucion2").attr("src", url_cliente + "imagenes/Devoluciones/" + Devolucion + "/" + numero + ".jpg?id=" + NumeroAleatorio());
}


Webcam.attach('#camaraweb');    

function TomarFotoDev() {
    Webcam.snap(function (data_uri) {
        //var raw_image_data = data_uri.replace(/^data\:image\/\w+\;base64\,/, '');
        GuardarFotoDevo(data_uri);
    });
}

function GuardarFotoDevo(archivo) {
    ActivarLoad();
	setTimeout(function () {
		var parametros = "opcion=SubirFoto&archivo=" + archivo + "&devolucion=" + Devolucion + "&tipo_imagen=3";
		var data = LlamarAjax("Configuracion",parametros).split("|");
		if (data[0] == "0"){
			CargarFotos();
			$("#ComboFoto").val(data[3]).trigger("change");
			DesactivarLoad();
		}else{
			DesactivarLoad();
			swal("Acción Cancelada",data[1],"warning");
		}
	},15);
}

    
$('select').select2();
DesactivarLoad();
