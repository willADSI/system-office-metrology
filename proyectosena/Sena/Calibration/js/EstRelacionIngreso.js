﻿d = new Date();
month = d.getMonth() + 1;
day = d.getDate();

var Anio = $.trim(d.getFullYear());
Anio = $.trim(Anio.substr(2, 2));


output = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-01';

output2 = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-' +
    (day < 10 ? '0' : '') + day;

$("#CFechaDesde").val(output);
$("#CFechaHasta").val(output2);

var DataPunto = null;
var Parametros = "";

var movil = 0;
if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
    movil = 1;
}


var ctbarracant = document.getElementById('GraficaCantidad').getContext('2d');
var ctbarratotal = document.getElementById('GraficaTotal').getContext('2d');

var datos = LlamarAjax("Base/CargaComboInicial", "tipo=4").split("||");

$("#CMagnitud").html(datos[0]);
$("#CEquipo").html(datos[1]);
$("#CMarca").html(datos[2]);
$("#CCliente").html(datos[3]);
$("#CProveedor").html(datos[4]);
$("#CModelo").html(datos[5]);
$("#CIntervalo").html(datos[6]);
$("#CUsuarioFac, #CUsuarioCom, #CUsuarioLog").html(datos[7]);

function BuscarPunto(magnitud, fecha, tabla, tipo) {
    var punto = 0;
    var data = null;
    switch (tabla) {
        case 0:
            if (tipo == 1)
                data = DataPunto.Table;
            else
                data = DataPunto.Table1;
            break;
        case 1:
            if (tipo == 1)
                data = DataPunto.Table2;
            else
                data = DataPunto.Table3;
            break;
        case 2:
            if (tipo == 1)
                data = DataPunto.Table4;
            else
                data = DataPunto.Table5;
            break;
        case 3:
            if (tipo == 1)
                data = DataPunto.Table6;
            else
                data = DataPunto.Table7;
            break;
        case 4:
            if (tipo == 1)
                data = DataPunto.Table8;
            else
                data = DataPunto.Table9;
            break;
        case 5:
            if (tipo == 1)
                data = DataPunto.Table10;
            else
                data = DataPunto.Table11;
            break;
    }
    var punto = 0;
    for (var x = 0; x < data.length; x++) {
        if (data[x].magnitud == magnitud && data[x].fecha == fecha) {
            punto += data[x].valor*1;
        }
    }
    return punto;
}

function BuscarPuntoImp(fecha, tabla, tipo) {
    var punto = 0;
    var data = null;
    if (tabla != 2)
        return 0;
    switch (tabla) {
        case 2:
            if (tipo == 1)
                data = DataPunto.Table13;
            else
                data = DataPunto.Table14;
            break;
    }
    var punto = 0;
    for (var x = 0; x < data.length; x++) {
        if (data[x].fecha == fecha) {
            punto += data[x].valor * 1;
        }
    }
    return punto;
}


function ConsularIngresos() {

    var magnitud = $("#CMagnitud").val() * 1;
    var equipo = $("#CEquipo").val();
    var modelo = $("#CModelo").val();
    var marca = $("#CMarca").val() * 1;
    var intervalo = $("#CIntervalo").val();
    var usuariolog = $("#CUsuarioLog").val() * 1;
    var usuariocom = $("#CUsuarioCom").val() * 1;
    var usuariofac = $("#CUsuarioFac").val() * 1;
    var cliente = $("#CCliente").val() * 1;
    var proveedor = $("#CProveedor").val() * 1;
    var sitio = $("#CSitio").val();
    var garantia = $("#CGarantia").val();
    var convenio = $("#CConvenio").val();
        
    var fechad = $("#CFechaDesde").val();
    var fechah = $("#CFechaHasta").val();

    if (fechad == "" && fechah == "") {
        swal("Acción Cancelada", ValidarTraduccion("debe de ingresar un rango de fecha"), "warning");
        return false;
    }

    ActivarLoad();
    setTimeout(function () {
        Parametros = "usuariolog=" + usuariolog + "&cliente=" + cliente + "&proveedor=" + proveedor + "&usuariocom= " + usuariocom + " &convenio=" + convenio +
            "&fechad=" + fechad + "&fechah=" + fechah + "&magnitud=" + magnitud + "&equipo=" + equipo + "&marca=" + marca + "&modelo=" + modelo +
            "&intervalo=" + intervalo + "&usuariofac=" + usuariofac + "&sitio=" + sitio + "&garantia=" + garantia;
        var datosesta = LlamarAjax("Estadisticas/RelacionIngreso", Parametros);
        DesactivarLoad();
        var colores = ['rgb(255, 99, 132)', 'rgb(255, 159, 64)', 'rgb(255, 205, 86)', 'rgb(75, 192, 192)', 'rgb(54, 162, 235)', 'rgb(153, 102, 255)', 'rgb(201, 203, 207)', 'rgb(104, 255, 51)', 'rgb(255, 60, 51)', 'rgb(8, 1, 0)', 'rgb(8, 89, 57)', 'rgb(105, 9, 102)'];
        var magnitudes = [];
        var fechas = [];
        var opciones = ['Cotizados', 'Ingresados', 'Facturados', 'Ajustes', 'Suministros', 'Tercerizados'];
        var totales = [0, 0, 0, 0, 0, 0];
        var cantidades = [0, 0, 0, 0, 0, 0];
        var datos = LlamarAjax("Laboratorio/MagnitudesAbrevia", "magnitud=" + magnitud);
        var data = JSON.parse(datos);
        for (x = 0; x < data.length; x++)
            magnitudes.push(data[x].descripcion);
        DataPunto = JSON.parse(datosesta);
        for (x = 0; x < DataPunto.Table12.length; x++)
            fechas.push(DataPunto.Table12[x].fecha);

        var config = {
            type: 'bar',
            data: {
                labels: fechas,
                datasets: []
            },
            options: {
                responsive: true,
                title: {
                    display: true,
                    text: ''
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                legend: {
                    position: 'top',
                    label: {
                        FontSize: '20px'
                    }
                }
            },
            lineAtIndex: 2
        }

        var config2 = {
            type: 'bar',
            data: {
                labels: fechas,
                datasets: []
            },
            options: {
                responsive: true,
                title: {
                    display: true,
                    text: ''
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                legend: {
                    position: 'top',
                    label: {
                        FontSize: '20px'
                    }
                }
            },
            lineAtIndex: 2
        }

        for (var i = 0; i < magnitudes.length; i++) {
            for (var x = 0; x < opciones.length; x++) {
                var newDataset = {
                    label: magnitudes[i] + "-" + opciones[x],
                    backgroundColor: colores[x],
                    borderColor: colores[i],
                    borderSkipped: 'bottom',
                    borderWidth: 2,
                    data: [],
                };

                var newDataset2 = {
                    label: magnitudes[i] + "-" + opciones[x],
                    backgroundColor: colores[x],
                    borderColor: colores[i],
                    borderSkipped: 'bottom',
                    borderWidth: 2,
                    data: [],
                };
                for (var p = 0; p < fechas.length; p++) {
                    
                    punto = BuscarPunto(magnitudes[i], fechas[p], x, 1)
                    punto2 = BuscarPunto(magnitudes[i], fechas[p], x, 2)
                    newDataset.data.push(punto);
                    newDataset2.data.push(punto2);
                }
                config.data.datasets.push(newDataset);
                config2.data.datasets.push(newDataset2);
            }
        }

        
        if (window.myBarracant != undefined) {
            window.myBarracant.destroy();
        }

        window.myBarracant = new Chart(ctbarracant, config2);
        window.myBarracant.update();

        if (window.myBarratotal != undefined) {
            window.myBarratotal.destroy();
        }

        window.myBarratotal = new Chart(ctbarratotal, config);
        window.myBarratotal.update();

        var resultado = "";

        magnitudes.push("--");

        var valor = 0;
        var cantidad = 0;
        

        for (var p = 0; p < fechas.length; p++) {
            
            for (var i = 0; i < magnitudes.length; i++) {
                resultado += "<tr " + (magnitudes[i] ==  "--" ? "class='bg-warning'" : "") + ">" +
                    "<td>" + fechas[p] + "</td>" + 
                    "<td>" + magnitudes[i] + "</td>";
                for (var x = 0; x < opciones.length; x++) {
                    valor = BuscarPunto(magnitudes[i], fechas[p], x, 1);
                    cantidad = BuscarPunto(magnitudes[i], fechas[p], x, 2);
                    if (magnitudes[i] == "--") {
                        valor = valor*1 - BuscarPuntoImp(fechas[p], x, 1)*1;
                        cantidad = cantidad - BuscarPuntoImp(fechas[p], x, 2) * 1;
                    }
                    totales[x] += valor;
                    cantidades[x] += cantidad;

                    resultado += "<td align='right' class='text-XX'><a href=\"javascript:VerDetalleRI('" + opciones[x] + "','" + magnitudes[i] + "','" + fechas[p] + "')\">" + formato_numero(cantidad, 0,_CD,_CM,"") + "</a></td>";
                    resultado += "<td align='right'>" + formato_numero(valor, 0,_CD,_CM,"") + "</td>";
                }
                resultado += "</tr>"
            }
            
        }

        resultado += "<tr class='bg-primary text-XX'><td colspan='2'>TOTALES</td>";
        for (var x = 0; x < cantidades.length; x++) {
            resultado += "<td align='right'>" + formato_numero(cantidades[x], 0,_CD,_CM,"") + "</td>" +
                "<td align='right'>" + formato_numero(totales[x], 0,_CD,_CM,"") + "</td>";
        }
        resultado += "</tr>";
            
        $("#TablaTotal").html(resultado);

    }, 15);
}

function ExportarExcel() {
    //Creamos un Elemento Temporal en forma de enlace
    var tmpElemento = document.createElement('a');
    // obtenemos la información desde el div que lo contiene en el html
    // Obtenemos la información de la tabla
    var data_type = 'data:application/vnd.ms-excel';
    var tabla_div = document.getElementById('TablaRelaIngreso');
    var tabla_html = tabla_div.outerHTML.replace(/ /g, '%20');
    tmpElemento.href = data_type + ', ' + tabla_html;
    //Asignamos el nombre a nuestro EXCEL
    tmpElemento.download = 'RelacionIngreso.xls';
    // Simulamos el click al elemento creado para descargarlo
    tmpElemento.click();
}

function VerDetalleRI(opcion, magnitudes, fecha) {
    var mensaje = "";
    var valores = 0;
    var cantidades = 0;
    switch (opcion) {
        case "Cotizados":
            $("#RelaOperacion").html("Cotización");
            mensaje = "Detalles de las cotizaciones de la magnitud " + magnitudes;
            break;
        case "Ingresados":
            $("#RelaOperacion").html("Remisión");
            mensaje = "Detalles de las remisiones de la magnitud " + magnitudes;
            break;
        case "Facturados":
            $("#RelaOperacion").html("Factura");
            mensaje = "Detalles de las facturas de la magnitud " + magnitudes;
            break;
        case "Ajustes":
            $("#RelaOperacion").html("Factura");
            mensaje = "Detalles de los ajustes de la magnitud " + magnitudes;
            break;
        case "Suministro":
            $("#RelaOperacion").html("Factura");
            mensaje = "Detalles de los suministros de la magnitud " + magnitudes;
            break;
        case "Tercerizado":
            $("#RelaOperacion").html("Orden <br>de Compra");
            mensaje = "Detalles de las órdenes de compra a proveedor de la magnitud " + magnitudes;
            break;
    }
    $("#RelacionDetalle").html(mensaje);
    var parametros =  Parametros + "&opcion=" + opcion + "&magnitudes=" + magnitudes+ "&fecha=" + fecha;
    var datos = LlamarAjax("Estadisticas/DetRelacionIngreso", parametros);
    
    var datajson = JSON.parse(datos);
    var table = $('#TablaDetRelacion').DataTable({
        data: datajson,
        bProcessing: true,
        bDestroy: true,
        paging: true,
        ordering: true,
        columns: [
            { "data": "fecha" },
            { "data": "magnitud" },
            { "data": "control", "className": "text-XX" },
            { "data": "ingreso" },
            { "data": "cliente" },
            { "data": "equipo" },
            { "data": "cantidad", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
            { "data": "valor", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') }
        ],
        "lengthMenu": [[8], [8]],
        "language": {
            "url": LenguajeDataTable
        },
        dom: 'Bfrtip',
        buttons: [
            'excel', 'csv', 'copy'
        ],
    });

    table.rows().data().each(function (datos, index) {
        cantidades += datos.cantidad * 1;
        valores += datos.valor * 1;
    });

    $("#TotalRelacion").html("<h3>Cantidades: " + formato_numero(cantidades, 0,_CD,_CM,"") + ", Valores $: " + formato_numero(valores, 0,_CD,_CM,""));
    $("#ModalRelaDetalle").modal("show");
}

function ImprimirCotizacion(cotizacion) {
    ActivarLoad();
    setTimeout(function () {
        var parametros = "cotizacion=" + cotizacion;
        var datos = LlamarAjax("Cotizacion/RpCotizacion", parametros);
        DesactivarLoad();
        datos = datos.split("|");
        if (datos[0] == "0") {
            window.open("DocumPDF/" + datos[1]);
        } else {
            swal("", ValidarTraduccion(datos[1]), "warning");
        }

    }, 15);
}

function ImprimirRemision(remision) {
    ActivarLoad();
    setTimeout(function () {
        var parametros = "remision=" + remision;
        var datos = LlamarAjax("Cotizacion/RpRemision", parametros);
        DesactivarLoad();
        datos = datos.split("|");
        if (datos[0] == "0") {
            window.open("DocumPDF/" + datos[1]);
        } else {
            swal("", ValidarTraduccion(datos[1]), "warning");
        }
    }, 15);
}

function ImprimirFactura(factura) {

    ActivarLoad();
    setTimeout(function () {
        var parametros = "factura=" + factura;
        var datos = LlamarAjax("Facturacion/RpFactura", parametros);
        DesactivarLoad();
        datos = datos.split("|");
        if (datos[0] == "0") {
            window.open("DocumPDF/" + datos[1]);
        } else {
            swal("", ValidarTraduccion(datos[1]), "warning");
        }
    }, 15);
}


$('select').select2();
DesactivarLoad();
