﻿
d = new Date();
month = d.getMonth() + 1;
day = d.getDate();

output = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-' +
    (day < 10 ? '0' : '') + day;

$("#Fecha").val(output);

var tabfactura = ActivarDataTable("tablafactura", 5);
var tabremision = ActivarDataTable("tablaremision", 5);


$("#Almacen").html(CargarCombo(1, 1));
$("#Almacen").val($("#menu_idalmacen").val());
$("#Almacen").focus();
var nfilas = 0;
var OpcionReporte = 1;

$("form").submit(function (e) {
    e.preventDefault();
})

function Consultar() {
    
    var almacen = $("#Almacen").val();
    var fecha = $("#Fecha").val();
    
    $("#resultadopdf").removeAttr("src");

    document.getElementById('preloader').style.display = 'block';
    setTimeout(function () {
        var parametros = "fecha=" + fecha + "&Almacen=" + almacen;
        var datos = LlamarAjax(url + "Arqueo/AuditoriaFactura", parametros);
        document.getElementById('preloader').style.display = 'none';
        datos = datos.split("|");
        tabfactura.rows().remove().draw(); 
        tabremision.rows().remove().draw(); 
        if (datos[0] != "[]") {
            var data = JSON.parse(datos[0]);
            for (var x = 0; x < data.length; x++) {
                tabfactura.row.add([
                    formato_numero(data[x].Numero, "0", ".", ",", ""),
                    data[x].FvnCodCliente,
                    formato_numero(data[x].Valor, "0", ".", ",", ""),
                    data[x].FvnEstado,
                    formato_numero(data[x].Abono, "0", ".", ",", ""),
                    formato_numero(data[x].Saldo, "0", ".", ",", ""),
                    data[x].Fecha,
                    data[x].FechaAnula,
                    data[x].Producto
                ]).draw(false);
            }
        }
        if (datos[1] != "[]") {

        }
    }, 15);
}


tb = $('input');
$(tb).keypress(enter2tab);

tb = $('select');
$(tb).keypress(enter2tab);

function enter2tab(e) {
    id = $(this).attr("id");
    if ((e.keyCode == 13) && ((id != "Codigo") || (id != "CodigoB"))) {
        cb = parseInt($(this).attr('tabindex'));
        if ($(':input[tabindex=\'' + (cb + 1) + '\']') != null) {
            $(':input[tabindex=\'' + (cb + 1) + '\']').focus();
            $(':input[tabindex=\'' + (cb + 1) + '\']').select();
            e.preventDefault();
            return false;
        }
    }
}
$('body').removeClass('nav-md');
$('body').addClass('nav-sm');

$('select').select2();