﻿angular.module('menucaja', ['ngRoute'])
/*var url = "http://localhost:49926/";

alert(url);*/

.config(['$routeProvider', function ($routeProvider) {
    $routeProvider
    when('/RCaja', {
        templateUrl: '/RCaja.html',
        controller: 'IndexCtrl'
    }).
    when('/about', {
        templateUrl: 'embedded.about.html',
        controller: 'Vista1Ctrl'
    }).
    otherwise({
        redirectTo: '/home'
    });
}])

.controller('IndexCtrl', ['$scope', function ($scope) {
    $scope.texto = "Esto es el inicio";
    $scope.suma = 2 + 2;
}])

.controller('Vista1Ctrl', ['$scope', function ($scope) {
    $scope.texto = "Esto es la vista 1";
    $scope.suma = 1 + 2;
}])

.controller('Vista2Ctrl', ['$scope', function ($scope) {
    $scope.texto = "Esto es la vista 2";
    $scope.suma = 2 + 6;
}])