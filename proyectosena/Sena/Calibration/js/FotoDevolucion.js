﻿var Devolucion = 0;
var Fotos = 0;
var ErrorDevolucion = 0;
var url = $("#urlprincipal").val();
var Entregado = 0;

function LimpiarTodo() {
    LimpiarDatos();
    $("#Devolucion").val("");
}

function CambiarFotoDev2(numero) {
    $("#FotoDevolucion").attr("src", url_cliente + "Adjunto/imagenes/Devoluciones/" + Devolucion + "/" + numero + ".jpg?id=" + NumeroAleatorio());
}

function CargarFotosDev() {
    var contenedor = "";
    var resultado = '<div class="row">';
    var fotos = LlamarAjax("Cotizacion","opcion=CantidadFotoDev&devolucion=" + Devolucion) * 1;
    contenedor = "<div class='row'><div class='col-xs-12 col-sm-12'>";
    for (var x = 1; x <= fotos; x++) {
        contenedor += "<a class='text-XX2' href='javascript:CambiarFotoDev2(" + x + ")'>" + x + "</a>&nbsp;";
        resultado += '<div class="col-lg-3 col-md-4 col-sm-4"><div class="block">' +
				'<div class="thumbnail">' +
				'   <a href="' + url_cliente + 'Adjunto/imagenes/Devoluciones/' + Devolucion + '/' + x + '_copia.jpg?id=' + NumeroAleatorio() + '" class="thumb-zoom lightbox" title="Devolución número ' + Devolucion + "/" + x + '">' +
				'       <img src="' + url_cliente + 'Adjunto/imagenes/Devoluciones/' + Devolucion + '/' + x + '.jpg?id=' + NumeroAleatorio()  + '" alt="">' +
				'						</a>' +
				'        <div class="caption text-center">' +
				'            <h6>Foto Número <small>' +  x + '</small></h6>' +
				'            <div class="icons-group">' +
				"                <a href=\"javascript:EliminarFotoDevIndividual(" + Devolucion + "," + x + ")\" title='Eliminar' class='tip'><span data-icon='&#xe0d8;'></span></a>" +
				'            </div>' +
				'        </div>' +
				'</div> ' +
				'</div></div>';
    }
    if (fotos > 0){
		contenedor += "</div>" + 
            "<div class='col-xs-12 col-sm-12'><img id='FotoDevolucion' src='" + url_cliente + "Adjunto/imagenes/Devoluciones/" + Devolucion + "/1.jpg?id=" + NumeroAleatorio() + " width='90%'></div></div>";
	}
    resultado += "</div>";
    $("#AlbunFotosDev").html(contenedor);
    $("#Albun_DetalladoDev").html(resultado);
}

function LimpiarDatos() {
    Devolucion = 0;
    $("#Cliente").val("");
    $("#Contacto").val("");
    $("#Direccion").val("");
    $("#Cantidad").val("");
    $("#Fecha").val("");
    $("#Asesor").val("");
    $("#Observacion").val("");
    $("#AlbunFotosDev").html("");
    $("#Albun_DetalladoDev").html("");

}

function BuscarDevolucion(numero, code, tipo) {
    if (Devolucion == (numero * 1))
        return false;
    if ((Devolucion != (numero * 1)) && ($("#Equipo").val() != ""))
        LimpiarDatos();
    if (numero * 1 == 0)
        return false;
    if ((code.keyCode == 13 || tipo == 1) && ErrorDevolucion == 0) {
		ActivarLoad();
		setTimeout(function () {
			if (numero * 1 > 0) {
				var datos = LlamarAjax("Cotizacion","opcion=BuscarDevolucion&devolucion=" + numero);
				if (datos != "[]") {
					var data = JSON.parse(datos);
					Entregado = data[0].entregado * 1;
					Devolucion = data[0].devolucion * 1;
					$("#Cliente").val(data[0].cliente);
					$("#Contacto").val(data[0].contacto);
					$("#Direccion").val(data[0].sede);
					$("#Cantidad").val(data[0].cantidad);
					$("#Fecha").val(data[0].fecha);
					$("#Asesor").val(data[0].usuario);
					$("#Observacion").val(data[0].observacion);
					 
					CargarFotosDev();
					DesactivarLoad();
					 
				} else {
					$("#Devolucion").select();
					$("#Devolucion").focus();
					ErrorDevolucion = 1;
					DesactivarLoad();
					swal("Acción Cancelada", "El Devolucion número " + numero + " no se encuentra registrado", "warning");
					return false;
				}
			}
		},15);
    } else
        ErrorDevolucion = 0;
}
    

function TomarFotoDev() {

    if (Devolucion > 0) {
        Webcam.snap(function (data_uri) {
            //var raw_image_data = data_uri.replace(/^data\:image\/\w+\;base64\,/, '');
            GuardarFotoDev(Devolucion, data_uri);
        });
    }
    
}

function EliminarFotoDev() {
    if (Devolucion > 0) {
		ActivarLoad();
		setTimeout(function () {
			datos = LlamarAjax("Cotizacion","opcion=EliminarFotoDev&devolucion=" + Devolucion).split("|");
			if (datos[1] != "X") {
				Fotos = datos[1]*1;
				CargarFotosDev(Devolucion, datos[1]);
				CambiarFotoDev2(datos[1], Devolucion);
				DesactivarLoad();
			}else{
				swal("Acción Cancelada",datos[0],"warning");
				DesactivarLoad();
			}
		},15);
    }
}

function EliminarFotoDevIndividual(devolucion, numero){
	if (devolucion > 0){
		ActivarLoad();
		setTimeout(function () {
			datos = LlamarAjax("Cotizacion", "opcion=EliminarFotoDevIndividual&devolucion=" + devolucion + "&numero=" + numero).split("|");
			if (datos[0] == "0") {
				CargarFotosDev();
				DesactivarLoad();
			}else{
				DesactivarLoad();
				swal("Acción Cancelada",datos[1],"success");
			}
		},15);
	}
}

function GuardarFotoDev(devolucion, archivo) {
	ActivarLoad();
	setTimeout(function () {
		var parametros = "opcion=SubirFoto&archivo=" + archivo + "&devolucion=" + devolucion + "&tipo_imagen=3";
		var data = LlamarAjax("Configuracion",parametros).split("|");
		if (data[0] == "0"){
			CargarFotosDev();
			CambiarFotoDev2(data[3], devolucion);
			DesactivarLoad();
		}else{
			DesactivarLoad();
			swal("Acción Cancelada",data[1],"warning");
		}
	},15);
}


function SubirFotoDev() {
    if (Devolucion > 0) {
		var resultado = "";
        swal.queue([{
            html: "<h3>" + ValidarTraduccion("Seleccione las fotos que deseas subir de la devolucion número " + Devolucion) + "</h3><input type='file' class='form-control' id='documenfotodevol' multiple onchange=\"GuardarDocumentoAdjunto(this.id,4,'')\" name='documento' accept='image/*' required />",
            showLoaderOnConfirm: true,
            showCancelButton: true,
            confirmButtonText: ValidarTraduccion('Guardar Fotos'),
            cancelButtonText: ValidarTraduccion('Cancelar'),
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            preConfirm: function () {
				return new Promise(function (resolve, reject) {
					if ($("#documenfotoregistro").val() == ""){
						reject("Debe de examinar por lo mínimo un archivo de imagen");
					}else{
						$.post(url_servidor + "Configuracion","opcion=SubirFotoMultiple&tipo_imagen=3&valor=" + Devolucion)
							.done(function (data) {
								datos = data.split("|");
								if (datos[0] == "0") {
									resultado = "<b>Fotos Subidas:</b><br>" + datos[1] + "<br><b>Fotos con error:</b><br>" + datos[2];
									resolve();
								} else {
									reject(datos[1]);
								}
							})
					}
				})
			}
        }]).then(function (data) {
			swal("",resultado,"success");
			CargarFotosDev();
        });
    }
}

Webcam.attach('#my_cameradeb');

function VerDevolucion(){
    if (Devolucion > 0) {
        ActivarLoad();
        setTimeout(function () {
            var datos = LlamarAjax("Configuracion", "opcion=GenerarPDF&tipo=Devolucion&documento=" + Devolucion).split("||");
            DesactivarLoad();
            if (datos[0] == "0") {
				window.open(url_cliente + "DocumPDF/" + datos[1]);
            } else {
                swal("Acción Cancelada", ValidarTraduccion(datos[1]), "warning");
            }
        }, 15);
    }
}


$('select').select2();
DesactivarLoad();
