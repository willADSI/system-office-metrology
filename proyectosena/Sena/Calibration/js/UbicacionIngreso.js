﻿var Ingreso = 0;
var IdRecibido = 0;
var Fotos = 0;
var ErrorIng = 0;

var bcliente = 0;
var bingreso = 0;
var bequipo = "";
var bentregado = -1;

var idusuario = localStorage.getItem('idusuario');

d = new Date();
month = d.getMonth() + 1;
day = d.getDate();

var Anio = $.trim(d.getFullYear());
Anio = $.trim(Anio.substr(2, 2))


output = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-' +
    (day < 10 ? '0' : '') + day;

$("#CCliente").html(CargarCombo(9, 1));
$("#EUbicacion").html(ComboUbicacion());

CargarTablaUbicacion(0, 0, "", 0);

function LimpiarDatos() {
    Fotos = 0;
    $("#EEquipo").val("");
    $("#EMarca").val("");
    $("#EModelo").val("");
    $("#ESerie").val("");
    $("#EMagnitud").val("");
    $("#ERango").val("");
    $("#AlbunFotos").html("");
}

$("#TablaUbicacion > tbody").on("dblclick", "tr", function () {
    var row = $(this).parents("td").context.cells;
    var fotos = row[6].innerText * 1;
    var ingreso = row[1].innerText * 1;
    if (fotos > 0)
        CargarModalFoto(ingreso, fotos,1);
});


function ConsultarUbicacion() {

    bcliente = $("#CCliente").val() * 1;
    bingreso = $("#CIngreso").val() * 1;
    bequipo = $.trim($("#CEquipo").val());
    bentregado = $("#CEntregado").val();
            
    ActivarLoad();
    setTimeout(function () {
        var parametros = "numero=" + bingreso + "&cliente=" + bcliente + "&equipo=" + bequipo + "&entregado=" + bentregado;
        var datos = LlamarAjax("Ubicacion","TablaUbicacionIngreso&"+parametros);
        DesactivarLoad();

        var datajson = JSON.parse(datos);
        $('#TablaUbicacion').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "ubicacion" },
                { "data": "ingreso" },
                { "data": "equipo" },
                { "data": "cliente" },
                { "data": "fecha" },
                { "data": "entregado" },
                { "data": "fotos" }
            ],

            "lengthMenu": [[5], [5]],
                       
            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy','colvis'
            ]
        });

        CargarTablaUbicacion(bcliente, bingreso, bequipo, bentregado);

    }, 15);
}

function BuscarIngreso(numero, code, tipo) {
    if ((Ingreso != (numero * 1)) && ($("#EEquipo").val() != ""))
        LimpiarDatos();
    if ((code.keyCode == 13 || tipo == 1) && ErrorIng == 0) {
        if (numero * 1 > 0) {
            var datos = LlamarAjax("Ubicacion","opcion=BuscarIngreso&numero=" + numero);
            if (datos != "[]") {
                var data = JSON.parse(datos);
                Ingreso = data[0].ingreso * 1;
                Fotos = data[0].fotos * 1;
                $("#EEquipo").val(data[0].equipo);
                $("#EMarca").val(data[0].marca);
                $("#EModelo").val(data[0].modelo);
                $("#ESerie").val(data[0].serie);
                $("#ERango").val(data[0].medida);
                $("#EMagnitud").val(data[0].magnitud);
                if ($.trim(data[0].ubicacion) != "")
                    $("#EUbicacion").val(data[0].ubicacion).trigger("change");

                CargarFotos();
            } else {
                $("#EIngreso").select();
                $("#EIngreso").focus();
                ErrorIng = 1;
                swal("Acción Cancelada", "El Ingreso número " + numero + " no se encuentra registrado", "warning");
                return false;
            }
        }
    } else
        ErrorIng = 0;
}

function CambiarFoto2(numero) {
    $("#FotoIngresodet").attr("src", url_cliente + "imagenes/ingresos/" + Ingreso + "/" + numero + ".jpg?id=" + NumeroAleatorio());
}

function CargarFotos() {
	var fotos = LlamarAjax("Cotizacion","opcion=CantidadFoto&ingreso=" + Ingreso) * 1;
    var contenedor = "";
    contenedor = "<div class='row'><div class='col-xs-12 col-sm-11'>" +
        "<img id='FotoIngresodet' " + (fotos > 0 ? "src='" + url_cliente + "imagenes/ingresos/" + Ingreso + "/1.jpgid=" + NumeroAleatorio() + "'" : "") + " width='70%'></div><div class='col-xs-12 col-sm-1'>";
    for (var x = 1; x <= fotos; x++) {
        contenedor += "<button class='btn btn-primary' type='button' onclick='CambiarFoto2(" + x + ")'>" + x + "</button>&nbsp;";
    }
    contenedor += "</div></div>";
    $("#AlbunFotosdet").html(contenedor);
}

function DetalleUbicacion(x, y, ingreso, cliente, entregado, equipo) {
    ActivarLoad();
    setTimeout(function () {
        var parametros = "x=" + x + "&y=" + y + "&numero=" + ingreso + "&cliente=" + cliente + "&equipo=" + equipo + "&entregado=" + entregado;
        var datos = LlamarAjax("Ubicacion","opcion=DetalleUbicacionIngreso&" + parametros);
        DesactivarLoad();

        var datajson = JSON.parse(datos);
        $('#TablaDetallado').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "ubicacion" },
                { "data": "ingreso" },
                { "data": "equipo" },
                { "data": "cliente" },
                { "data": "fecha" },
                { "data": "devolucion" },
                { "data": "entregado" },
                { "data": "fotos" },
                { "data": "editar" },
                { "data": "eliminar" }

            ],

            "lengthMenu": [[3], [3]],

            columnDefs: [
                {
                    "targets": [7],
                    "visible": false,
                    "searchable": false
                }
            ],
            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy','colvis'
            ]
        });
        var data = JSON.parse(datos);
        var resultado = '<div class="row">';
        for (var i = 0; i < data.length; i++) {
            if (data[i].fotos * 1 > 0) {
                resultado += '<div class="col-lg-3 col-md-4 col-sm-4"><div class="block">' +
                    '<div class="thumbnail">' +
                    '   <a href="' + url_cliente + 'imagenes/ingresos/' + data[i].ingreso + '/1.jpg?id=' + NumeroAleatorio() + '" class="thumb-zoom lightbox" title="Ingreso número ' + data[i].ingreso + '">' +
                    '       <img src="' + url_cliente + 'imagenes/ingresos/' + data[i].ingreso + '/1.jpg?id=' + NumeroAleatorio() + '" alt="">' +
                    '						</a>' +
                    '        <div class="caption text-center">' +
                    '            <h6>' + data[i].equipos.substr(0, 19) + '<small>' +  data[i].modelo + '</small></h6>' +
                    '            <div class="icons-group">' +
                    "                <a href=\"javascript:EditarUbicacion(" + data[i].ingreso + "," + data[i].entregado + ",'" + data[i].x + "','" + data[i].y + "')\" title='Editar' class='tip'><span data-icon='&#xe064;'></span></a>" +
                    "                <a href=\"javascript:EliminarUbicacion(" + data[i].ingreso + "," + data[i].entregado + ",'" + data[i].equipos + " " + data[i].modelo + "','" +  data[i].x + "','" + data[i].y + "')\" title='Editar' class='tip'><span data-icon='&#xe0d8;'></span></a>" +
                    '            </div>' +
                    '        </div>' +
                    '</div> ' +
                    '</div></div>';
            }
        }

        resultado += "</div>";
        $("#DetalleFoto").html(resultado);

    }, 15);

    $("#EUbicacion").val(y + x).trigger("change");
        
    $("#dtubicacion").html(y+x);
    $("#modalDetalleUbicacion").modal("show");
}


function CargarTablaUbicacion(cliente, ingreso, equipo, entregado) {
    var resultado = "";
    var letras = ["A", "B", "C", "D", "E", "F", "G"]
    var datos = "";
    var parametros = "&numero=" + ingreso + "&cliente=" + cliente + "&equipo=" + equipo + "&entregado=" + entregado
    for (var x = 2; x <= 5; x++) {
        resultado += '<tr class="tabcertr">';
        for (var y = 0; y <=6; y++) {
            if (y == 0) {
                resultado += "<td height='80px' class='text-XX text-center'>" +x + "</td>";
            } else {
                if (y == 1) {
                    resultado += "<td height='80px' class='bg-danger'>&nbsp;</td>";
                } else {
                    datos = LlamarAjax("Ubicacion","opcion=BusquedaUbicacionIngreso&x=" + x + "&y=" + letras[y] + parametros);
                    resultado += "<td onclick=\"DetalleUbicacion('" + x + "','" + letras[y] + "'," + ingreso + "," + cliente + "," + entregado + ",'" + equipo + "')\" height='40px' class='bg-default text-XX2'>" + datos + "</td>";
                }
            }
        }
        resultado += '</tr>';
    }
    $("#TBArchivo").html(resultado);
}

function AgregarUbicacion() {
    LimpiarDatos();
    $("#EIngreso").val("");
    $("#modalEditar").modal("show");
}

function EditarUbicacion(ingreso, estado, x, y) {

    if (estado != "0") {
        swal("Acción Cancelada", "No se puede editar una ubicación entregada", "warning");
        return false;
    }

    AgregarUbicacion();
    $("#EIngreso").val(ingreso);
    BuscarIngreso(ingreso, "", 1);
    $("CUbicacion").val(y + x).trigger("change");
    $("CUbicacion").focus();
}

function EliminarUbicacion(ingreso, estado, equipo, x, y) {
    if (estado != "0") {
        swal("Acción Cancelada", "No se puede editar una ubicación entregada", "warning");
        return false;
    }

    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: ValidarTraduccion('¿Seguro que desea eliminar la ubicacion del ingreso número ' + ingreso + ' del equipo ' + equipo + '?'),
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Si'),
        cancelButtonText: ValidarTraduccion('No'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                $.post(url_servidor+ "Ubicacion","opcion=Operaciones&numero=" + ingreso + "&ubicacion=xx&operacion=2")
                    .done(function (data) {
                        datos = data.split("|");
                        if (datos[0] == "0") {
                            ConsultarUbicacion();
                            DetalleUbicacion(x, y, bingreso, bcliente, bentregado, bequipo);
                            resolve();
                        } else {
                            reject(datos[1]);
                        }
                    })
            })
        }
    }]);
}

function GuardarUbicacion() {
    var ingreso = $("#EIngreso").val() * 1;
    var ubicacion = $("#EUbicacion").val();

    if ($("#EEquipo").val() == "") {
        $("#EIngreso").focus();
        swal("Acción Cancelada", "Debe de ingresar un equipo", "warning");
        return false;
    }

    if (ubicacion == "") {
        $("#EUbicacion").focus();
        swal("Acción Cancelada", "Debe de seleccionar la ubicación del equipo", "warning");
        return false;
    }

    var parametros = "numero=" + ingreso + "&ubicacion=" + ubicacion + "&operacion=1";
    var datos = LlamarAjax("Ubicacion","opcion=Operaciones&"+ parametros).split("|");
    if (datos[0] == "0") {
        ConsultarUbicacion();

        var y = ubicacion.substring(0, 1);
        var x = ubicacion.substring(1, 2);
        DetalleUbicacion(x, y, bingreso, bcliente, bentregado, bequipo);

        $("#EIngreso").focus();
        swal("", datos[1], "success");
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
    
}

$('select').select2();
DesactivarLoad();
