﻿var Ingreso = 0;
var IdRecibido = 0;
var Foto = 0;
var Error = 0;

var idusuario = localStorage.getItem('idusuario');

d = new Date();
month = d.getMonth() + 1;
day = d.getDate();

var Anio = $.trim(d.getFullYear());
Anio = $.trim(Anio.substr(2, 2))


output = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-' +
    (day < 10 ? '0' : '') + day;

$("#FechaInicio").val(output);
$("#FechaFin").val(output);

$("#Especialista").html(CargarCombo(19, 0));
$("#Especialista").val(idusuario).trigger("change");

$("#Asesor").html(CargarCombo(13, 1,"","1"));
localStorage.setItem("Ingreso", "0");

function LimpiarTodo() {
    LimpiarDatos();
    $("#Ingreso").val("");
}

function LimpiarDatos() {
    Ingreso = 0;
    IdRecibido = 0;
    Foto = 0;
    localStorage.setItem("Ingreso", "0");
    $("#btnguardar").removeClass("hidden");
    $("#Equipo").val("");
    $("#Marca").val("");
    $("#Modelo").val("");
    $("#Serie").val("");
    $("#Magnitud").val("");
    $("#Observacion").val("");
    $("#Observaciones").val("");
    $("#Observaesp").val("");
    $("#Remision").val("");
    $("#Rango").val("");
    $("#Asesor").val("").trigger("change");
    $("#registrorecibiring").html("");

    $("#Usuario").val("");
    $("#FechaIng").val("");
    $("#Tiempo").val("");

    $("#Sitio").val("");
    $("#Garantia").val("");
    $("#Accesorio").val("");
    
}


function ConsultarRecibidoIng() {

    var fecini = $("#FechaInicio").val();
    var fecfin = $("#FechaFin").val();
    var especialista = $("#Especialista").val() * 1;

    if (fecini == "" || fecfin == "") {
        $("#FechaInicio").focus();
        swal("Acción Cancelada", "Debeb de seleccionar fecha inicio y fecha final", "warning");
        return false;
    }
                
    ActivarLoad();
    setTimeout(function () {
        var parametros = "fecini=" + fecini + "&fecfin=" + fecfin + "&especialista=" + especialista;
        var datos = LlamarAjax("Laboratorio/TablaRecibidoCome", parametros);
        DesactivarLoad();
        var datajson = JSON.parse(datos);
        $('#TablaReportar').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "fila" },
                { "data": "ingreso" },
                { "data": "equipo" },
                { "data": "observaciones" },
                { "data": "usuario" },
                { "data": "fecha" }
            ],

            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy','colvis'
            ]
        });
    }, 15);
}

ConsultarRecibidoIng();


function BuscarIngreso(numero, code, tipo) {
    var mensaje = "";
    if ((Ingreso != (numero*1)) && ($("#Equipo").val() != ""))
        LimpiarDatos();
    if ((code.keyCode == 13 || tipo == "0") && Error == 0) {
        if (numero * 1 > 0) {
            var datos = LlamarAjax("Laboratorio/BuscarIngresoRecbCome", "ingreso=" + numero).split("|");
            if (datos != "[]") {
                var data = JSON.parse(datos);
                IdRemision = data[0].id * 1;
                Ingreso = data[0].ingreso * 1;
                Foto = data[0].fotos * 1;
                localStorage.setItem("Ingreso", Ingreso);
                $("#Equipo").val(data[0].equipo);
                $("#Marca").val(data[0].marca);
                $("#Modelo").val(data[0].modelo);
                $("#Serie").val(data[0].serie);
                $("#Rango").val(data[0].intervalo);
                $("#Remision").val(data[0].remision);
                $("#Magnitud").val(data[0].magnitud);
                $("#Observacion").val(data[0].observacion);
                $("#Observaesp").val((data[0].observaespeci ? data[0].observaespeci : ""));

                $("#Sitio").val(data[0].sitio);
                $("#Garantia").val(data[0].garantia);
                $("#Accesorio").val(data[0].accesorio);

                $("#Usuario").val(data[0].usuarioing);
                $("#FechaIng").val(data[0].fechaing);
                $("#Tiempo").val(data[0].tiempo);

                if (data[0].id) {

                    swal("Acción Cancelada", "Este ingreso ya fue recibido", "warning");
                    Error = 1;
                    IdRecibido = data[0].id;
                    Certificado = data[0].numero;
                    $("#Asesor").val(data[0].idusuarioent).trigger("change");
                    $("#Observaciones").val(data[0].observacioning);
                    $("#registrorecibiring").html("<b>Recibido el día " + data[0].fecha + " por el asesor " + data[0].usuariorecing + "</b>");
                    $("#btnguardar").addClass("hidden");

                } else {
                    $("#Asesor").focus();
                }
                
            } else {
                $("#Ingreso").select();
                $("#Ingreso").focus();
                Error = 1;
                swal("Acción Cancelada", "El Ingreso número " + numero + " no se encuentra Registrado/Calibrado", "warning");
                return false;
            }
        }
    } else
        Error = 0;
}


function Guardar() {
    var asesor = $("#Asesor").val()*1;
    var observacion = $.trim($("#Observaciones").val());
    var fecha = $("#FechaIng").val();
    if (Ingreso == 0)
        return false;
    var mensaje = "";
    if (observacion == "") {
        $("#Observaciones").focus()
        mensaje = "<br> Debe de ingresar la observación para la devolución del equipo" + mensaje;
    }
    if (asesor == 0) {
        $("#Asesor").focus();
        mensaje = "<br> Debe de seleccionar el especialista que entrega el equipo" + mensaje;
    }
    
    if (mensaje != "") {
        swal("Acción Cancelada", mensaje, "warning");
        return false;
    }

    var parametros = "ingreso=" + Ingreso + "&usuario=" + asesor + "&observacion=" + observacion + "&fecha=" + fecha;
    var datos = LlamarAjax("Laboratorio/RecibirCome", parametros).split("|");
    if (datos[0] == "0") {
        LimpiarTodo();
        ConsultarRecibidoIng();
        $("#Ingreso").focus();
        swal("", datos[1], "success");
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
}

function EliminarRecIngresoCom() {
    if (Ingreso == 0)
        return false;
    var mensaje = "";

    swal({
        title: 'Advertencia',
        text: '¿Desea eliminar el recibido del ingreso número ' + Ingreso + '?',
        input: 'text',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Eliminar'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',

        inputValidator: function (value) {
            return new Promise(function (resolve, reject) {
                if (value) {
                    var datos = LlamarAjax("Laboratorio/EliminarRecibido", "opcion=Recibir Comercial&observacion=" + value + "&ingreso=" + Ingreso)
                    datos = datos.split("|");
                    if (datos[0] == "0") {
                        mensaje = datos[1];
                        resolve()
                    } else {
                        reject(datos[1]);
                    }

                } else {
                    reject(ValidarTraduccion('Debe de ingresar un observación'));
                }
            })
        }
    }).then(function (result) {
        LimpiarTodo();
        $("#Ingreso").focus();
        swal({
            type: 'success',
            html: mensaje
        })
    });
}

function VerFotos() {
    if (Ingreso == 0)
        return false;
    CargarModalFoto(Ingreso, Foto,1);
}

$('select').select2();    

