﻿var a_Nro;
var a_Cuenta;
var a_Concepto;
var a_Beneficiario;
var a_Debito;
var a_Credito;
var a_Centro;
var a_IdDetalle;
var CboTerceros = CargarCombo(84, 7);
var CboCentroCostos = CargarCombo(61, 7);
var CboCuentas = CargarCombo(83, 1);
var Estado = "Temporal";

var TotalFila = 0;
var Contador = 0;

var IdComprobante = 0;

var AutoConcepto = [];

d = new Date();
month = d.getMonth() + 1;
day = d.getDate();

var Anio = $.trim(d.getFullYear());
Anio = $.trim(Anio.substr(2, 2))

output = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-01';

var fechaactual = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-' +
    (day < 10 ? '0' : '') + day;

$("#BHasta").val(fechaactual);
$("#BDesde").val(output);

$("#BPrefijo").html(CargarCombo(45, 1));

function AutoCompletar() {
    var datos = LlamarAjax("Contabilidad/Autocompletar", "");
    AutoConcepto.splice(0, AutoConcepto.length);
    var data = JSON.parse(datos);
    for (var x = 0; x < data.length; x++) {
        AutoConcepto.push(data[x].concepto);
    }
}

AutoCompletar();

function ModalComprobante() {
    $("#modalComprobantes").modal("show");
}

function CargarModalComprobante(temporal) {
    var prefijo = $("#BPrefijo").val();
    var numero = $("#BNumero").val() * 1;
    var descripcion = $.trim($("#BDescripcion").val());
    var estado = $("#BEstado").val();
    var desde = $("#BDesde").val();
    var hasta = $("#BHasta").val();

    var parametros = "&Prefijo=" + prefijo + "&Numero=" + numero + "&Descripcion=" + descripcion + "&Estado=" + estado +
        "&Desde=" + desde + "&Hasta=" + hasta;
    var datos = LlamarAjax("Contabilidad/BuscarComprobante", parametros);
    var datajson = JSON.parse(datos);
    $('#TablaBusCompro').DataTable({
        data: datajson,
        bProcessing: true,
        bDestroy: true,
        columns: [
            { "data": "prefijo" },
            { "data": "numero" },
            { "data": "descripcion" },
            { "data": "tdebito", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
            { "data": "tcredito", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
            { "data": "estado" },
            { "data": "fecha" },
            { "data": "usuario" }
        ],
        "lengthMenu": [[5], [5]],   
        "language": {
            "url": LenguajeDataTable
        }
    });
}


$("#TablaBusCompro > tbody").on("dblclick", "tr", function (e) {
    var row = $(this).parents("td").context.cells;
    ActivarLoad();
    setTimeout(function () {
        LimpiarDatos();
        var numero = row[1].innerText;
        var datos = LlamarAjax("Contabilidad/DetalleComprobante", "Numero=" + numero);
        var data = JSON.parse(datos);

        IdComprobante = data[0].idcomprobante * 1;
        $("#Tipo").val(data[0].prefijo);
        $("#Numero").val(data[0].numero);
        $("#Fecha").val(data[0].fecha);
        $("#Descripcion").val(data[0].descripcion);
        Estado = data[0].estado;
        $("#Estado").val(data[0].estado);

        for (var x = 0; x < data.length; x++) {
            $("#Cuenta" + x).val(data[x].idcuenta).trigger("change");
            $("#Concepto" + x).val(data[x].concepto);
            $("#Beneficiario" + x).val(data[x].idbeneficiario).trigger("change");
            $("#Debito" + x).val(formato_numero(data[x].debito, _DE, _CD, _CM, ""));
            $("#Credito" + x).val(formato_numero(data[x].credito, _DE, _CD, _CM, ""));
            $("#Centro" + x).val(data[x].idcentrocosto).trigger("change");
            AddNuevaFila((x + 1));
        }
        CalcularDiferencia();
        $('select').select2();
        DesactivarLoad();
        $("#modalComprobantes").modal("hide");
    }, 15);
        
});

function AddNuevaFila(x) {
              
    var NuevaFila = '<tr id="trfila' + x + '">' +
        '<td><input class="sinbordemed text-right text-XX" name="Item[]" id="Item' + x + '" value="' + (x + 1) + '" disabled="disabled" readonly="readonly" /></td>' +
        '<td><select type="text" class="form-control" name="Cuenta[]" style="width:250px;height:250px;" id="Cuenta' + x + '">' + CboCuentas + '</select></td> ' +
        '<td><input class="sinbordemed atconcepto" name="Concepto[]" id="Concepto' + x + '" onkeyup="PasarConcepto(this.value,' + x + ', event)"></td>' +
        '<td><select type="text" class="form-control" name="Beneficiario[]" id="Beneficiario' + x + '" style="width:300px;height:300px;">' + CboTerceros + '</select></td> ' +
        '<td><input type="text" class="sinbordemed text-right text-XX" name="Debito[]" onfocus="FormatoEntrada(this, 1)" onblur="FormatoSalida(this, 0)" onkeyup="CalcularTotal(this, ' + x + ',event)" id="Debito' + x + '"  /></td>' +
        '<td><input type="text" class="sinbordemed text-right text-XX" name="Credito[]" onfocus="FormatoEntrada(this, 1)" onblur="FormatoSalida(this, 0)" onkeyup="CalcularTotal(this, ' + x + ',event)" id="Credito' + x + '"  /></td>' +
        '<td><select type="text" class="form-control" name="Centro[]" id="Centro' + x + '" style="width:100%">' + CboCentroCostos + '</select><input type="hidden" name="IdDetalle[]" id="IdDetalle' + x + '"></td></tr>';

    $("#TablaComprobante").append(NuevaFila);

    a_Nro = document.getElementsByName("Item[]");
    a_Cuenta = document.getElementsByName("Cuenta[]");
    a_Concepto = document.getElementsByName("Concepto[]");
    a_Beneficiario = document.getElementsByName("Beneficiario[]");
    a_Credito = document.getElementsByName("Credito[]");
    a_Debito = document.getElementsByName("Debito[]");
    a_Centro = document.getElementsByName("Centro[]");
    a_IdDetalle = document.getElementsByName("IdDetalle[]");
                        
    TotalFila++;
      
}

function PasarConcepto(concepto, fila, tecla) {

    if (tecla.keyCode == 13) {
        concepto = $.trim(concepto);
        if (concepto == "") {
            $.jGrowl("Debe de ingresar un concepto", { life: 2000, theme: 'growl-warning', header: '' });
        } else {
            GuardarTemporal(fila);
            $("#Beneficiario" + fila).focus();
        }
    }
}



function LimpiarDatos() {
    TotalFila = 0;
    $("#TablaComprobante tbody tr").remove();
    IdComprobante = 0;
    AddNuevaFila(0);
    Contador = LlamarAjax("Contabilidad/Contador", "") * 1;
    $("#Numero").val(Contador);
    $("#Fecha").val(fechaactual);
    $("#Tipo").val("CC");
    $("#Descripcion").val("");
    Estado = "Temporal"
    $("#Estado").val(Estado);

    ActivarAutocomplete();
    $('select').select2();
    CalcularDiferencia();
}

function GuardarTemporal(fila) {

    var idcomprobante = $("#IdComprobante").val() * 1;
    var fecha = $("#Fecha").val();
    var descripcion = $.trim($("#Descripcion").val());
    var iddetalle = $("#IdDetalle" + fila).val() * 1;
    var cuenta = $("#Cuenta" + fila).val() * 1;
    var concepto = $.trim($("#Concepto" + fila).val());
    var beneficiario = $.trim($("#Beneficiario" + fila).val());
    var debito = NumeroDecimal($("#Debito" + fila).val())*1;
    var credito = NumeroDecimal($("#Credito" + fila).val())*1;

    var parametros = "IdComprobante=" + idcomprobante + "&IdDetalle=" + iddetalle + "&Cuenta=" + cuenta + "&Concepto=" + concepto + "&Beneficiario=" + beneficiario +
        "&Fecha=" + fecha + "&Descripcion=" + descripcion + "&Debito=" + debito + " &Credito=" + credito;
    var datos = LlamarAjax("Contabilidad/GuardartmpComprobante", parametros).split("|");
    if (datos[0] == "0") {
        $("#Numero").val(datos[1]);
        $("#IdComprobante").val(datos[2]);
        $("#IdDetalle" + fila).val(datos[3]);
        $.jGrowl("Temporal guardado", { life: 2500, theme: 'growl-success', header: '' });
    } else
        $.jGrowl(datos[1], { life: 2500, theme: 'growl-warning', header: '' });
    
}

LimpiarDatos();

function CalcularDiferencia() {
    var debito = 0;
    var credito = 0;
    for (var x = 0; x < a_Debito.length; x++) {
        debito += NumeroDecimal(a_Debito[x].value);
        credito += NumeroDecimal(a_Credito[x].value);
    }
    var diferencia = debito - credito;
    $("#TDebito").html(formato_numero(debito, _DE, _CD, _CM, ""));
    $("#TCredito").html(formato_numero(credito, _DE,_CD,_CM,""));
    $("#TDiferencia").html(formato_numero(diferencia, _DE,_CD,_CM,""));
}

function CalcularTotal(Caja, fila, tecla) {
    ValidarTexto(Caja, 1);
    CalcularDiferencia();
    if (Caja.name == "Debito[]") {
        if (NumeroDecimal(Caja.value) > 0) {
            $("#Credito" + fila).prop("disabled", true);
            $("#Credito" + fila).val("0");
        } else {
            $("#Credito" + fila).prop("disabled", false);
            $("#Credito" + fila).val("");
        }
            
    } else {
        if (NumeroDecimal(Caja.value) > 0) {
            $("#Debito" + fila).prop("disabled", true);
            $("#Debito" + fila).val("0");
        } else {
            $("#Debito" + fila).prop("disabled", false);
            $("#Debito" + fila).val("");
        }
    }
    if (tecla.keyCode == 13) {
        if ((fila + 1) == TotalFila) {
            AddNuevaFila(fila + 1);
            ActivarAutocomplete();
        }
        if (Caja.name == "Debito[]") {
            if (NumeroDecimal(Caja.value) == 0) {
                $("#Credito" + fila).focus();
            } else {
                $("#Cuenta" + (fila + 1)).focus();
            }

        } else {
            if (NumeroDecimal(Caja.value) == 0) {
                $("#Debito" + fila).focus();
            } else {
                $("#Cuenta" + (fila + 1)).focus();
            }
        }
        GuardarTemporal(fila);
    }
}

function BuscarDatos(fila) {
    $("#TCuenta").html($("#DesCuenta" + fila).val());

}

function ActivarAutocomplete() {
        
    $(".atconcepto").autocomplete({ source: AutoConcepto, minLength: 1 }).focus(function () {
        $(this).data("uiAutocomplete").search($(this).val());
    });
}

$('select').select2();
DesactivarLoad();
