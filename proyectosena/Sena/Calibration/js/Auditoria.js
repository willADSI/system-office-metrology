﻿d = new Date();
month = d.getMonth() + 1;
day = d.getDate();

var Anio = $.trim(d.getFullYear());
Anio = $.trim(Anio.substr(2, 2))


output = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-01';

output2 = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-' +
    (day < 10 ? '0' : '') + day;

$("#FechaDesde").val(output);
$("#FechaHasta").val(output2);

$("#Usuario").html(CargarCombo(13, 1));

function TablaAuditoria() {

    var usuario = $("#Usuario").val()*1;
    var modulo = $.trim($("#Modulo").val());
    var accion = $.trim($("#Accion").val());
    var consulta = $.trim($("#Consulta").val());

    var fechaini = $("#FechaDesde").val();
    var fechafin = $("#FechaHasta").val();
    
    if (fechaini == "" || fechafin == "") {
        $("#fechaini").focus();
        swal("Acción Cancelada", "Debe seleccionar la fecha inicial y fecha final", "warning");
        return false;
    }
            
    ActivarLoad();
    setTimeout(function () {
        var parametros = "usuario=" + usuario + "&modulo=" + modulo + "&accion=" + accion + "&consulta=" + consulta + "&fechad=" + fechaini + "&fechah=" + fechafin;
        var datos = LlamarAjax("Seguridad/TablaAuditoria", parametros);
        DesactivarLoad();
        var datajson = JSON.parse(datos);
        $('#TablaAuditoria').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "fecha" },
                { "data": "usuario" },
                { "data": "modulo" },
                { "data": "accion" },
                { "data": "ip" },
                { "data": "consulta" }
            ],

            "language": {
                "url": LenguajeDataTable
            }
        });
    }, 15);
}


$('select').select2();
DesactivarLoad();
