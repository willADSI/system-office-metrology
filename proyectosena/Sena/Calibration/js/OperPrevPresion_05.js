﻿d = new Date();
month = d.getMonth() + 1;
day = d.getDate();

var Anio = $.trim(d.getFullYear());
Anio = $.trim(Anio.substr(2, 2))
var PermisioEspecial = PerGeneSistema("PERMISO ADMINISTRADOR LABORATORIO");

output = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-01';

var FechaCal = d.getFullYear() + "-" + (month < 10 ? '0' : '') + month + "-" + (day < 10 ? "0" : "") + day;

var Ingreso = 0;
var PlaIngreso = 0;
var ErrorCalculo = 0;
var IdReporte = 0;
var IdInstrumento = 0;
var ErrorEnter = 0;
var Fotos = 0;
var NroReporte = 1;
var IdVersion = 5;
var RangoHasta = 0;
var RangoDesde = 0;
var GuardaTemp = 0;
var CorreoEmpresa = "";
var AplicarTemporal = 0;
var InfoTecIngreso = 0;
var Factor = 0;
var NumCertificado = 1;
var asesor = "";
var Habitual = "NO";
var AprobarCotizacion = "SI";
var Sitio;
var Garantia;
var FechaAproCoti = "";

$("#RangoPatron").html(CargarCombo(20, 1, "", "2"));

localStorage.setItem("Ingreso", "0");

function GenerarCertificado() {
    localStorage.setItem("CerIngreso", Ingreso);
    LlamarOpcionMenu('Laboratorio/PlaCertiPresion', 3, '');

}

function LimpiarTodo() {
    GuardaTemp = 0;
    localStorage.removeItem("Pre" + NumCertificado + "-" + Ingreso);
    localStorage.removeItem("Table" + NumCertificado + "-" + Ingreso)
    localStorage.removeItem("Puntos" + NumCertificado + "-" + Ingreso)
    LimpiarDatos();
    $("#IngresoPresion").val("");
    GuardaTemp = 1;
}

function LimpiarDatos() {
    
    PlaIngreso = 0;
    ErrorCalculo = 0;
    IdRemision = 0;
    AplicarTemporal = 0;
    Ingreso = 0;
    localStorage.setItem("Ingreso", Ingreso);
    Fotos = 0
    InfoTecIngreso = 0;
    NroReporte = 1;
    $("#Solicitante").html("");
    $("#Certificado").html("");
    $("#Direccion").html("");
    $("#FechaIng").html("");
    $("#FechaCal").html("");

    $("#TIngreso").html("");
    $("#Medida").html("");
    $("#ValorCon").html("");
    $("#UnidadCon").html("");

    $(".medida").html("");

    $("#Equipo").html("");
    $("#Marca").html("");
    $("#Modelo").html("");
    $("#Serie").html("");
    $("#Remision").val("");
    $("#Usuario").val("");
    $("#FechaRec").val("");
    $("#Tiempo").val("");
    $("#Proxima").html("");

    $("#CMetodo").val("");
    $("#CPunto").val("");
    $("#CServicio").val("");
    $("#CObservCerti").val("");
    $("#CProxima").val("");
    $("#CEntrega").val("");
    $("#CAsesorComer").val("");

    $("#RangoMedida").html("");
    $("#MedResolucion").html("");
    RangoHasta = 0;

    $("#DivEscala").val("");
    $("#AlturaPatron").val("");
    $("#Resolucion").val("");
    $("#AlturaIBC").val("").trigger("change");
    $("#Clase").val("");
    $("#RangoPatron").val("").trigger("change");
    $("#Fluido").val("ACEITE").trigger("change");
    $("#CoeSec").val("");
    $("#TopeCero").val("").trigger("change");
    $("#Gliserina").val("").trigger("change");
    $("#ProximaCali").val("");
    $("#TipoLectura").val("").trigger("change");

    for (var x = 1; x <= 3; x++) {
        $("#PrSerie" + x).val("");
        $("#LecturaPatMed" + x).val("");
        $("#LecturaIBCMed" + x).val("");
    }

    
    $("#Usuario").val("");
    $("#FechaRec").val("");
    $("#Tiempo").val("");

    $("#Concepto").val("1").trigger("change");
    $("#Ajuste").val("");
    $("#Suministro").val("");
    $("#Observacion").val("");
    $("#Conclusion").val("");

    $("#tbodycondicion").html("");
           
    IdInstrumento = 0;
    $("#PrLectura").val("");
        
    for (var x = 1; x <= 5; x++) {
        $("#IdReporte" + x).val("0");
        $("#Observaciones" + x).val("");
        $("#Conclusion" + x).val("");
        $("#Ajuste" + x).val("");
        $("#Suministro" + x).val("");
        $("#registroingreso" + x).html("");
        $("#Concepto" + x).val("").trigger("change");
    }

    CalcularTotal();
}

$("#TablaReportar > tbody").on("dblclick", "tr", function () {
    var row = $(this).parents("td").context.cells;
    var numero = row[1].innerText;
    $("#IngresoPresion").val(numero);
    BuscarIngreso(numero,0,"0");
});

function GeneralCertificado() {
    
    if (GuardaTemp == 1 || IdReporte == 0) {
        swal("Acción Cancelada", "Debe primero guardar el reporte", "warning");
        return false;
    }

    if ((concepto != 2) && (concepto != 1)) {
        swal("Acción Cancelada", "No se puede generar certificado cuando no está apto a calibrar o en subcontrataciones", "warning");
        return false;
    }

    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: '¿Seguro que desea generar el informe técnico del ingreso número ' + Ingreso + '?',
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Si'),
        cancelButtonText: ValidarTraduccion('No'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                $.post("Laboratorio/InformeTecnicoDev", "ingreso=" + Ingreso + "&reporte=0&plantilla=" + NumCertificado)
                    .done(function (data) {
                        datos = data.split("|");
                        if (datos[0] == "0") {
                            $("#NroInforme" + reporte).val(datos[2]);
                            ImprimirInforme();
                            resolve()
                        } else {
                            reject(datos[1]);
                        }
                    })
            })
        }
    }]);
}

function EnviarCotizacion() {

    var concepto = $("#Concepto").val() * 1;


    if (Ingreso == 0)
        return false;
    if (GuardaTemp == 1 || IdReporte == 0) {
        swal("Acción Cancelada", "Debe primero guardar el reporte", "warning");
        return false;
    }

    $("#eCliente").val($("#Solicitante").html());
    $("#eCorreo").val(CorreoEmpresa);


    if (concepto == 2) {
        $("#spcotizacion").html(Ingreso);
        $("#enviar_cotizacion").modal("show");
    } else {
        swal("Acción Cancelada", "Solo se podra enviar cotizaciones cuando el ingreso requiera ajuste o suministro", "warning");
    }
    
}

$("#formenviarcotizacion").submit(function (e) {

    e.preventDefault();
    var observacion = $.trim($("#ObserEnvio").val())
    var correo = $.trim($("#eCorreo").val());
    a_correo = correo.split(";");

    if (ValidarCorreo(CorreoEmpresa) == 2) {
        swal(ValidarTraduccion("Acción Cancelada"), ValidarTraduccion("El correo electrónico del contacto no es válido ") + ":" + CorreoEmpresa, "warning");
        //return false;
    }

    for (var x = 0; x < a_correo.length; x++) {
        if (ValidarCorreo($.trim(a_correo[x])) == 2) {
            $("#eCorreo").focus();
            swal(ValidarTraduccion("Acción Cancelada"), ValidarTraduccion("Formato de correo electrónico no válido") + ":" + a_correo[x], "warning");
            return false;
        }
    }

    
    ActivarLoad();
    setTimeout(function () {
        var parametros = "magnitud=2&ingreso=" + Ingreso + "&principal=" + CorreoEmpresa + "&e_email=" + correo + "&observacion=" + observacion + "&plantilla=" + NumCertificado;
        var datos = LlamarAjax("Laboratorio/EnvioCotizacion", parametros);
        DesactivarLoad();
        datos = datos.split("|");
        if (datos[0] == "0") {
            swal("", ValidarTraduccion(datos[1]) + " " + datos[2], "success");
            $("#enviar_cotizacion").modal("hide");
        } else
            swal("Acción Cancelada", datos[1], "warning");
    }, 15);

    return false;
});





function ConsularReportesIngresos() {
            
    ActivarLoad();
    setTimeout(function () {
        var cliente = $("#ClienteIngreso").val() * 1;
        var aprobadas = 0;
        if ($("#Aprobadas").prop("checked"))
            aprobadas = 1
        var parametros = "magnitud=2&cliente=" + cliente + "&aprobadas=" + aprobadas;
        var datos = LlamarAjax("Laboratorio/TablaReportarIngreso", parametros);
        DesactivarLoad();
        var datajson = JSON.parse(datos);
        $('#TablaReportar').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "fila" },
                { "data": "ingreso" },
                { "data": "equipo" },
                { "data": "fecha" },
                { "data": "tiempo" }
            ],

            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy','colvis'
            ]
        });
    }, 15);
}


function CondicionExterna() {

    var resultado = "";
    var datos = LlamarAjax("Laboratorio/CambioCondicion", "ingreso=" + Ingreso + "&magnitud=2&tipo=0&plantilla=" + NumCertificado);
    if (datos != "[]") {
        var datacon = JSON.parse(datos);
        var valores = "";
        var opcion = 0;
        var radio = "";
        for (var x = 0; x < datacon.length; x++) {
            valores = datacon[x].opcion.split("!!");
            opcion = valores[0] * 1;
            switch (opcion) {
                case 0:
                    radio = "<td><input type='radio' onchange='GuardarTemporal(1)' required class='form-control' name='Opcion" + x + "' id ='CSi" + x + "'></td>" +
                        "<td><input type='radio' onchange='GuardarTemporal(1)' required class='form-control' name='Opcion" + x + "' id ='CNo" + x + "'></td>";
                    break;
                case 1:
                    radio = "<td><input type='radio' onchange='GuardarTemporal(1)' required checked class='form-control' name='Opcion" + x + "' id ='CSi" + x + "'></td>" +
                        "<td><input type='radio' onchange='GuardarTemporal(1)' required class='form-control' name='Opcion" + x + "' id ='CNo" + x + "'></td>";
                    break;
                case 2:
                    radio = "<td><input type='radio' onchange='GuardarTemporal(1)' required class='form-control' name='Opcion" + x + "' id ='CSi" + x + "'></td>" +
                        "<td><input type='radio' onchange='GuardarTemporal(1)' required checked class='form-control' name='Opcion" + x + "' id ='CNo" + x + "'></td>";
                    break;
            }
            resultado += "<tr>" +
                "<td class='bg-gris'>" + datacon[x].descripcion + "</td>" + radio +
                "<td><input class='form-control sinbordecon bg-amarillo' name='CObservacion[]' value='" + valores[1] + "'>" +
                "<input type='hidden' value='" + datacon[x].id + "' name='Ids[]'><input type='hidden' value='" + datacon[x].descripcion + "' name='Descripciones[]'></td></tr>";
        }
    }
    $("#tbodycondicion").html(resultado);
}


ConsularReportesIngresos();

$.connection.hub.start().done(function () {

    $('#IngresoPresion').keyup(function (event) {
        numero = this.value * 1;
        if (numero == -1)
            numero = Ingreso;


        if ((Ingreso != (numero * 1)) && ($("#Equipo").html() != "")) {
            GuardaTemp = 1;
            LimpiarDatos();
            Ingreso = 0;
            ErrorEnter = 0;
        }
        if (event.which == 13) {
            event.preventDefault();

            if (Ingreso == numero)
                return false;
            BuscarIngreso(numero);
        }
    });
});

function BuscarIngreso(numero) {

    NumCertificado = $("#Plantilla").val() * 1;
    var revision = $("#Revision").val() * 1;
        
    numero = numero * 1;
        
    PlaIngreso = NumCertificado;
                
    if (ErrorEnter == 0) {
        if (numero * 1 > 0) {
            GuardaTemp = 0;
            var datos = LlamarAjax("Laboratorio/BuscarIngresoOperacion", "magnitud=2&ingreso=" + numero + "&medida=psi&plantilla=" + NumCertificado + "&revision=" + revision + "&version=" + IdVersion).split("|");
            if (datos[0] != "[]") {
                var data = JSON.parse(datos[0]);

                if (data[0].idmagnitud != "2") {
                    swal("Acción Cancelada", "El ingreso número " + numero + " no pertenece a la magnitud <br><b>PRESIÓN</b>", "warning");
                    ErrorEnter = 1;
                    $("#IngresoPresion").select();
                    $("#IngresoPresion").focus();
                    return false;
                }

                if (revision > 1) {
                    if (revision * 1 != (data[0].revision + 1)) {
                        swal("Acción Cancelada", "Debe de realizar primero la revisón número " + (data[0].revision + 1), "warning");
                        LimpiarDatos();
                        Ingreso = numero;
                        return false;
                    }
                }

                IdReporte = 0;
                NumReporte = 0;
                if (datos[1] != "[]") {
                    var datarep = JSON.parse(datos[1]);

                    for (var x = 0; x < datarep.length; x++) {
                        NumReporte = datarep[x].nroreporte;
                        if (datarep[x].nroreporte * 1 == 0) {
                            GuardaTemp = 0;
                            IdReporte = datarep[x].id;
                            $("#Concepto").val(datarep[x].idconcepto).trigger("change");
                            $("#Ajuste").val(datarep[x].ajuste);
                            $("#Suministro").val(datarep[x].suministro);
                            $("#Observacion").val(datarep[x].observaciones);
                            $("#Conclusion").val(datarep[x].conclusion);

                            $("#registroingreso").html("<h3>Registrado el día " + datarep[x].fecha + " por el técnico " + datarep[x].usuario + "</h3>" + datarep[x].aprobado);
                        }
                        if (datarep[x].idconcepto == 3) {
                            InfoTecIngreso = 1;
                            break;
                        }
                    }

                    if (IdReporte != 0) {
                        if (NumReporte != 0) {
                            if (tipo != "3")
                                swal("Advertencia", "Reporte realizado en reporte de ingreso " + tipo, "warning");
                        } else {
                            GuardaTemp = 0;
                            var dataser = JSON.parse(datos[2]);
                            var porerror = data[0].hasta * data[0].clase / 100;
                            var poradvertencia = porerror / 2;
                            $("#RangoPatron").val(dataser[0].rangopatron).trigger("change");
                            for (var x = 0; x < dataser.length; x++) {
                                $("#PrSerie" + dataser[x].numero).val(formato_numero(dataser[x].prserie, 0,_CD,_CM,""));
                                $("#Porcentaje" + dataser[x].numero).val(formato_numero(dataser[x].porcentaje, 0,_CD,_CM,""));
                                $("#Lectura" + dataser[x].numero).val(formato_numero(dataser[x].lectura, _DE,_CD,_CM,""));
                                $("#LecturaPatMed" + dataser[x].numero).val(formato_numero(dataser[x].lecturapatmed, _DE,_CD,_CM,""));
                                $("#LecturaPatCon" + dataser[x].numero).val(formato_numero(dataser[x].lecturapatcon, 4, ".", ",", ""));
                                $("#ErrorEcuaCon" + dataser[x].numero).val(formato_numero(dataser[x].errorecuacon, 4, ".", ",", ""));
                                $("#ErrorEcuaMed" + dataser[x].numero).val(formato_numero(dataser[x].errorecuamed, 4, ".", ",", ""));
                                $("#LecturaPatCor" + dataser[x].numero).val(formato_numero(dataser[x].lecturapatcor, 4, ".", ",", ""));
                                $("#LecturaIBCMed" + dataser[x].numero).val(formato_numero(dataser[x].lecturaibcmed, _DE,_CD,_CM,""));
                                $("#CorreccionMed" + dataser[x].numero).val(formato_numero(dataser[x].correccionmed, 4, ".", ",", ""));

                                $("#CorreccionMed" + dataser[x].numero).removeClass("bg-danger");
                                $("#CorreccionMed" + dataser[x].numero).removeClass("bg-naranja");
                                $("#CorreccionMed" + dataser[x].numero).addClass("bg-default");

                                if (porerror > 0) {
                                    if (Math.abs(dataser[x].correccionmed) >= Math.abs(poradvertencia) && Math.abs(dataser[x].correccionmed) < Math.abs(porerror)) {
                                        $("#CorreccionMed" + dataser[x].numero).addClass("bg-naranja");
                                        $("#CorreccionMed" + dataser[x].numero).removeClass("bg-danger");
                                        $("#CorreccionMed" + dataser[x].numero).removeClass("bg-default");
                                    } else {
                                        if (Math.abs(dataser[x].correccionmed) >= Math.abs(porerror)) {
                                            $("#CorreccionMed" + dataser[x].numero).addClass("bg-danger");
                                            $("#CorreccionMed" + dataser[x].numero).removeClass("bg-default");
                                            $("#CorreccionMed" + dataser[x].numero).removeClass("bg-naranja");
                                        } else {
                                            $("#CorreccionMed" + dataser[x].numero).removeClass("bg-danger");
                                            $("#CorreccionMed" + dataser[x].numero).addClass("bg-default");
                                            $("#CorreccionMed" + dataser[x].numero).removeClass("bg-naranja");
                                        }
                                    }
                                }

                            }
                            swal("Acción Cancelada", "El ingreso número " + numero + " ya ha sido reportado", "warning");
                        }
                    }
                    ErrorEnter = 1;
                    $("#IngresoPresion").select();
                    $("#IngresoPresion").focus();
                }

                IdRemision = data[0].id * 1;
                Ingreso = data[0].ingreso * 1;
                localStorage.setItem("Ingreso", Ingreso);
                Fotos = data[0].fotos * 1;
                asesor = "/" + data[0].asesor + "/";
                Sitio = data[0].sitio;
                Garantia = data[0].garantia;
                Habitual = data[0].habitual;
                AprobarCotizacion = data[0].aprobar_cotizacion;
                FechaAproCoti = $.trim(data[0].fechaaprocoti);
                $("#Solicitante").html(data[0].cliente);
                $("#Certificado").html("PT-" + data[0].contador + "-" + Anio);
                $("#Direccion").html(data[0].direccion);
                $("#FechaIng").html(data[0].fechaing);
                $("#FechaCal").html(FechaCal);
                CorreoEmpresa = data[0].email;
                
                $("#TIngreso").html(data[0].ingreso);
                $("#Medida").html(data[0].medida);
                $("#ValorCon").html(data[0].medidacon);
                Factor = data[0].medidacon;
                $("#UnidadCon").html("1 " + data[0].medida + " =");

                $(".medida").html(data[0].medida);

                $("#Equipo").html(data[0].equipo);
                $("#Marca").html(data[0].marca);
                $("#Modelo").html(data[0].modelo);
                $("#Proxima").html(data[0].proxima);
                $("#Serie").html(data[0].serie);
                $("#Remision").val(data[0].remision);
                InfoTecIngreso = data[0].informetecnico;

                CondicionExterna();
                                
                
                $("#RangoMedida").html("(" + data[0].desde + " a " + data[0].hasta + ") " + data[0].medida);
                $("#MedResolucion").html(data[0].medida);
                RangoHasta = data[0].hasta*1;
                RangoDesde = data[0].desde * 1;

                $("#PrLectura").val(formato_numero(RangoHasta,_DE,_CD,_CM,""));

                $("#DivEscala").val((data[0].division_escala ? data[0].division_escala : ""));
                $("#AlturaPatron").val((data[0].altura_patron ? data[0].altura_patron : ""));
                $("#Resolucion").val((data[0].resolucion ? data[0].resolucion : ""));
                $("#AlturaIBC").val((data[0].altura_ibc ? data[0].altura_ibc : ""));
                $("#Clase").val((data[0].clase ? data[0].clase : ""));
                $("#Fluido").val((data[0].fluido_usado ? data[0].fluido_usado : "ACEITE")).trigger("change");
                $("#CoeSec").val(data[0].coef_senc);
                $("#TopeCero").val((data[0].tope_cero ? data[0].tope_cero : "")).trigger("change");
                $("#Gliserina").val((data[0].glicerina ? data[0].glicerina : "")).trigger("change");
                $("#TipoLectura").val((data[0].tipolectura ? data[0].tipolectura : "")).trigger("change");
                IdInstrumento = (data[0].idinstrumento ? data[0].idinstrumento : 0);
                                                                
                $("#Usuario").val(data[0].usuarioing);
                $("#FechaRec").val(data[0].fecharec);
                $("#Tiempo").val(data[0].tiempo);

                //MODAL DE COTIZACIONES

                if (datos[3] != "[]") {
                    var dataco = JSON.parse(datos[3]);
                    if (IdReporte == 0) {
                        if (Habitual == "NO" && AprobarCotizacion == "SI" && Sitio == "NO" && Garantia == "NO" && FechaAproCoti == "") {
                            if (dataco[0].estado != "Aprobado" && dataco[0].estado != "POR REEMPLAZAR") {
                                ErrorEnter = 1;
                                chat.server.send(usuariochat, "Buen días amigo. se requiere la aprobación de la cotización número " + dataco[0].cotizacion + " para proceder a la calibración", '', asesor);
                                swal("Acción Cancelada", "La cotizacíon número " + dataco[0].cotizacion + " no ha sido aprobada por el cliente", "warning");
                                if (PermisioEspecial == 0) {
                                    LimpiarDatos();
                                    return false;
                                }
                            }
                        }
                    }
                    for (var x = 0; x < dataco.length; x++) {
                        if (x == 0) {
                            $("#CMetodo").val(dataco[x].metodo);
                            $("#CPunto").val(dataco[x].punto);
                            $("#CServicio").val(dataco[x].servicio);
                            $("#CObservCerti").val(dataco[x].observacion);
                            $("#CProxima").val(dataco[x].proxima);
                            $("#CEntrega").val(dataco[x].entrega);
                            $("#CAsesorComer").val(dataco[x].asesor);
                        } else {
                            $("#CMetodo").val("<br>" + dataco[x].metodo);
                            $("#CPunto").val("<br>" + dataco[x].punto);
                            $("#CServicio").val("<br>" + dataco[x].servicio);
                            $("#CObservCerti").val("<br>" + dataco[x].observacion);
                            $("#CProxima").val("<br>" + dataco[x].proxima);
                            $("#CEntrega").val("<br>" + dataco[x].entrega);
                            $("#CAsesorComer").val("<br>" + dataco[x].asesor);
                        }
                    }
                } else {
                    if (IdReporte == 0) {
                        if (Habitual == "NO" && AprobarCotizacion == "SI" && Sitio == "NO" && Garantia == "NO" && FechaAproCoti == "") {
                            ErrorEnter = 1;
                            swal("Acción Cancelada", "Este ingreso no ha sido cotizado por el asesor comercial", "warning");
                            chat.server.send(usuariochat, "Buen días amigo. se requiere realizar la cotizacion y aprobación del ingreso número " + Ingreso + " para proceder a la calibración", '', asesor);
                            if (PermisioEspecial == 0) {
                                LimpiarDatos();
                                return false;
                            }
                        }
                    }
                }
                
                if (IdReporte == 0) {
                    if (localStorage.getItem("Pre" + NumCertificado + "-" + Ingreso)) {
                        var data = JSON.parse(localStorage.getItem("Pre" + NumCertificado + "-" + Ingreso));

                        $("#DivEscala").val(data[0].divescala);
                        $("#AlturaPatron").val(data[0].alturapatron);
                        $("#Resolucion").val(data[0].resolucion);
                        $("#AlturaIBC").val(data[0].alturaibc);
                        $("#Clase").val(data[0].clase);
                        $("#Fluido").val(data[0].fluido).trigger("change");
                        $("#TopeCero").val(data[0].topecero).trigger("change");
                        $("#Gliserina").val(data[0].gliserina).trigger("change");
                        $("#RangoPatron").val(data[0].rangopatron).trigger("change");
                        IdInstrumento = data[0].idinstrumento;

                        $("#PrSerie1").val(data[0].prserie1);
                        $("#PrSerie2").val(data[0].prserie2);
                        $("#PrSerie3").val(data[0].prserie3);

                        var datapunto = JSON.parse(localStorage.getItem("Puntos" + NumCertificado + "-" + Ingreso));
                        for (var x = 0; x <= 2; x++) {
                            $("#LecturaPatMed" + (x + 1)).val(datapunto[x].LecturaPatMed);
                            $("#LecturaIBCMed" + (x + 1)).val(datapunto[x].LecturaIBCMed);
                        }
                        
                        $("#Concepto").val(data[0].concepto).trigger("change");
                        $("#Ajuste").val(data[0].ajuste);
                        $("#Suministro").val(data[0].suministro);
                        $("#Conclusion").val(data[0].conclusion);
                        $("#Observacion").val(data[0].observacion);
                    }

                    if (localStorage.getItem("Table" + NumCertificado + "-" + Ingreso))
                        $("#tbodycondicion").html(localStorage.getItem("Table" + NumCertificado + "-" + Ingreso));
                    GuardaTemp = 1;
                    CalcularTotal();
                } else {
                    //CalcularTotal();
                    //$("#Concepto").val(datarep[x].idconcepto).trigger("change");
                    AplicarTemporal = 1;
                }

                RequerirAjuste();
                                
            } else {
                $("#IngresoPresion").select();
                $("#IngresoPresion").focus();
                ErrorEnter = 1;
                swal("Acción Cancelada", "El Ingreso número " + numero + " no se ha recibido en el laboratorio", "warning");
                return false;
            }
        }
    } else
        ErrorEnter = 0;
}

function CalcularSubTotal(Caja) {
    if (Ingreso == 0)
        return false;
    ValidarTexto(Caja, 3);
    CalcularTotal();
}

function CalcularTotal() {
    
    CalcularSerie("", 1,1);
    CalcularSerie("", 2,1);
    CalcularSerie("", 3,1);
        
}

function BuscarDescripcionEquipo() {
    var clase = $("#Clase").val();
    var tipo = $("#Clasificacion").val();
    $("#Descripcion").html("");
    if (clase != "" && tipo != "") {
        var datos = LlamarAjax("Laboratorio/BuscarDescripcionEquipo", "tipo=" + tipo + "&clase=" + clase);
        if (datos != "XX")
            $("#Descripcion").html(datos);
    }
    GuardarTemporal();
}

function GuardarTemporal(tabla) {
    if (Ingreso > 0 && GuardaTemp == 1) {
        var archivo = "[";
        for (var x = 1; x <= 3; x++) {
            if (x > 1)
                archivo += ',';
            archivo += '{"LecturaPatMed":"' + $("#LecturaPatMed" + x).val() + '","LecturaIBCMed":"' + $("#LecturaIBCMed" + x).val() + '"}';
        }
        archivo += "]";

        
        localStorage.setItem("Puntos" + NumCertificado + "-" + Ingreso, archivo);

        var archivo = "[{";
        archivo += '"prserie1":"' + $("#PrSerie1").val() + '","prserie2":"' + $("#PrSerie2").val() + '","prserie3":"' + $("#PrSerie3").val() + '",';
        archivo += '"divescala":"' + $("#DivEscala").val() + '","resolucion":"' + $("#Resolucion").val() + '","rangopatron":"' + $("#RangoPatron").val() + '",' +
            '"fluido":"' + $("#Fluido").val() + '","clase":"' + $("#Clase").val() + '","topecero":"' + $("#TopeCero").val() + '",' +
            '"gliserina":"' + $("#Gliserina").val() + '","alturapatron":"' + $("#AlturaPatron").val() + '","alturaibc":"' + $("#AlturaIBC").val() + '",' +
            '"concepto":"' + $("#Concepto").val() + '","ajuste":"' + $("#Ajuste").val() + '","suministro":"' + $("#Suministro").val() +
            '","observacion":"' + $("#Observacion").val() + '","conclusion":"' + $("#Conclusion").val() + '","idinstrumento":"' + IdInstrumento +
            '","id":"' + IdReporte + '","ingreso":"' + Ingreso + '","planilla":"' + NumCertificado + '"';
        archivo += '}]';

        if (tabla) {
            var id = document.getElementsByName("Ids[]");
            var observacion = document.getElementsByName("CObservacion[]");
            var descripcion = document.getElementsByName("Descripciones[]");

            var opcion = 0;
            var resultado = "";
            var radio = "";

            for (var x = 0; x < id.length; x++) {
                opcion = 0;
                if ($("#CSi" + x).prop("checked"))
                    opcion = 1;
                else {
                    if ($("#CNo" + x).prop("checked"))
                        opcion = 2;
                }

                switch (opcion) {
                    case 0:
                        radio = "<td><input type='radio' onchange='GuardarTemporal(1)' required class='form-control' name='Opcion" + x + "' id ='CSi" + x + "'></td>" +
                            "<td><input type='radio' onchange='GuardarTemporal(1)' required class='form-control' name='Opcion" + x + "' id ='CNo" + x + "'></td>";
                        break;
                    case 1:
                        radio = "<td><input type='radio' onchange='GuardarTemporal(1)' required checked class='form-control' name='Opcion" + x + "' id ='CSi" + x + "'></td>" +
                            "<td><input type='radio' onchange='GuardarTemporal(1)' required class='form-control' name='Opcion" + x + "' id ='CNo" + x + "'></td>";
                        break;
                    case 2:
                        radio = "<td><input type='radio' onchange='GuardarTemporal(1)' required class='form-control' name='Opcion" + x + "' id ='CSi" + x + "'></td>" +
                            "<td><input type='radio' onchange='GuardarTemporal(1)' required checked class='form-control' name='Opcion" + x + "' id ='CNo" + x + "'></td>";
                            
                        break;
                }
                resultado += "<tr>" +
                    "<td class='bg-gris'>" + descripcion[x].value + "</td>" + radio +
                    "<td><input class='form-control sinbordecon bg-amarillo' name='CObservacion[]' value='" + observacion[x].value + "'>" +
                    "<input type='hidden' value='" + id[x].value + "' name='Ids[]'><input type='hidden' value='" + descripcion[x].value + "' name='Descripciones[]'></td></tr>";
            }
            localStorage.setItem("Table" + NumCertificado + "-" + Ingreso, resultado);
        }


        localStorage.setItem("Pre" + NumCertificado + "-" + Ingreso, archivo);

        $.jGrowl("Guardando Temporal", { life: 1500, theme: 'growl-warning', header: '' });
    } else {
        if (AplicarTemporal == 1)
            GuardaTemp = 1;
    }
}

function FormatoSalida2(Caja) {
    FormatoSalida(Caja, 4, NumCertificado);
}

function CalcularSerie(Caja, fila, tipo) {

    
    if (ErrorCalculo == 1) {
        ErrorCalculo = 0;
        return false;
    }
    if (Caja != "")
        ValidarTexto(Caja, 3);

    var lecturapat = NumeroDecimal($("#LecturaPatMed" + fila).val())
    var lecturaibc = NumeroDecimal($("#LecturaIBCMed" + fila).val());
    
    if (NumCertificado == 2) {
        lecturapat = Math.abs(lecturapat) * -1;
        lecturaibc = Math.abs(lecturaibc) * -1;
    }
                
    var porlectura = NumeroDecimal($("#Porcentaje" + fila).val()) * 1;
    var lectura = RangoHasta * porlectura / 100;
    $("#Lectura" + fila).val(formato_numero(lectura, _DE,_CD,_CM,""));
    var lecturacon = lecturapat * Factor;
    $("#LecturaPatCon" + fila).val(formato_numero(lecturacon, 4, ".", ",", ""));

    var patron = $("#RangoPatron").val() * 1;
    $("#ErrorEcuaCon" + fila).val("");
    $("#ErrorEcuaMed" + fila).val("");
    $("#LecturaPatCor" + fila).val("");
    $("#CorreccionMed" + fila).val("");
    
        
    //tabla corregida
    var correccion = 0;
    
    if (patron > 0) {
        var parametros = "patron=" + patron + "&lectura=" + lecturacon + "&lecturamax=" + RangoHasta * Factor;
        var datos = LlamarAjax("Laboratorio/ConvertirLecturaPresionErr", parametros).split("|");
        if (datos[0] == "0") {
                        
            correccion = (lecturapat - (datos[1] / Factor)) - lecturaibc;
            $("#ErrorEcuaCon" + fila).val(formato_numero(datos[1], 4, ".", ",", ""));
            $("#ErrorEcuaMed" + fila).val(formato_numero(datos[1] / Factor, 4, ".", ",", ""));
            $("#LecturaPatCor" + fila).val(formato_numero(lecturapat - (datos[1] / Factor), 4, ".", ",", ""));
            $("#CorreccionMed" + fila).val(formato_numero(correccion, 4, ".", ",", ""));
        } else {
            $("#RangoPatron").focus();
            swal("Acción Cancelada", datos[1], "warning");
            $("#RangoPatron").val("").trigger("change");
        }
    } 

    var clase = NumeroDecimal($("#Clase").val());
    var porerror = RangoHasta * clase / 100;
    var poradvertencia = porerror/2;

    $("#CorreccionMed" + fila).removeClass("bg-danger");
    $("#CorreccionMed" + fila).removeClass("bg-naranja");
    $("#CorreccionMed" + fila).addClass("bg-default");

    if (porerror > 0) {
        if (Math.abs(correccion) >= Math.abs(poradvertencia) && Math.abs(correccion) < Math.abs(porerror)) {
            $("#CorreccionMed" + fila).addClass("bg-naranja");
            $("#CorreccionMed" + fila).removeClass("bg-danger");
            $("#CorreccionMed" + fila).removeClass("bg-default");
        } else {
            if (Math.abs(correccion) >= Math.abs(porerror)) {
                $("#CorreccionMed" + fila).addClass("bg-danger");
                $("#CorreccionMed" + fila).removeClass("bg-default");
                $("#CorreccionMed" + fila).removeClass("bg-naranja");
            } else {
                $("#CorreccionMed" + fila).removeClass("bg-danger");
                $("#CorreccionMed" + fila).addClass("bg-default");
                $("#CorreccionMed" + fila).removeClass("bg-naranja");
            }
        }
    }
}

function CambioPatron(fila) {
    CalcularSerie("", fila,1);
    GuardarTemporal(0);
}

function RequerirAjuste() {
    if (GuardaTemp == 1)
        GuardarTemporal();    
    var clase = NumeroDecimal($("#Clase").val());
    var porerror = RangoHasta * clase / 100;
    
    if (porerror == 0)
        return false;
    if (NumeroDecimal($("#LecturaPatMed1").val()) == 0 || NumeroDecimal($("#LecturaPatMed2").val()) == 0 || NumeroDecimal($("#LecturaPatMed3").val()) == 0)
        return false;
    if (NumeroDecimal($("#LecturaIBCMed1").val()) == 0 || NumeroDecimal($("#LecturaIBCMed2").val()) == 0 || NumeroDecimal($("#LecturaIBCMed3").val()) == 0)
        return false;
        
    var correccion1 = Math.abs(NumeroDecimal($("#CorreccionMed1").val()));
    var correccion2 = Math.abs(NumeroDecimal($("#CorreccionMed2").val()));
    var correccion3 = Math.abs(NumeroDecimal($("#CorreccionMed3").val()));

    
    if (GuardaTemp == 1) {
        if (correccion1 > porerror || correccion2 > porerror || correccion3 > porerror) {
            $("#OptCalibrar").prop("disabled", true);
            $("#OptSinPosi").prop("disabled", false);
            $("#OptAjuste").prop("disabled", false);
            $("#Concepto").val(2).trigger("change");
            EscucharMensaje("El equipo requiere ajuste");
            $.jGrowl("El equipo requiere ajuste", { life: 6000, theme: 'growl-warning', header: '' });
        } else {
            $("#OptCalibrar").prop("disabled", false);
            $("#OptSinPosi").prop("disabled", true);
            $("#OptAjuste").prop("disabled", true);
            $("#Concepto").val(1).trigger("change");
            EscucharMensaje("El equipo está  apto para calibrar");
            $.jGrowl("El equipo está  apto para calibrar", { life: 6000, theme: 'growl-success', header: '' });
        }
    } else {
        if (correccion1 > porerror || correccion2 > porerror || correccion3 > porerror) {
            EscucharMensaje("El equipo requiere ajuste");
            $.jGrowl("El equipo requiere ajuste", { life: 6000, theme: 'growl-warning', header: '' });
        } else {
            EscucharMensaje("El equipo está  apto para calibrar");
            $.jGrowl("El equipo está  apto para calibrar", { life: 6000, theme: 'growl-success', header: '' });
        }
    }
}

function CambioConcepto(concepto, tipo) {
    var prefijo = (tipo == 0 ? "" : tipo);

    $("#Suministro" + prefijo).prop("disabled", true);
    $("#Ajuste" + prefijo).prop("disabled", true);
    $("#Conclusion" + prefijo).prop("disabled", true);
    $("#Observacion" + prefijo).prop("disabled", false);

    $("#Observacion" + prefijo).addClass("bg-amarillo");
    $("#Ajuste" + prefijo).removeClass("bg-amarillo");
    $("#Suminstro" + prefijo).removeClass("bg-amarillo");
    $("#Conclusion" + prefijo).removeClass("bg-amarillo");

    $("#Documento" + prefijo).removeClass("bg-amarillo");
    $("#NumFoto" + prefijo).removeClass("bg-amarillo");
    $("#Documento" + prefijo).prop("disabled", true);
    $("#NumFoto" + prefijo).prop("disabled", true);

    if (concepto == "2") {
        $("#Suministro" + prefijo).prop("disabled", false);
        $("#Ajuste" + prefijo).prop("disabled", false);
        $("#Observacion" + prefijo).prop("disabled", true);
        $("#Conclusion" + prefijo).prop("disabled", true);

        $("#Observacion" + prefijo).removeClass("bg-amarillo");
        $("#Ajuste" + prefijo).addClass("bg-amarillo");
        $("#Suministro" + prefijo).addClass("bg-amarillo");
        $("#Conclusion" + prefijo).removeClass("bg-amarillo");

    } else {
        if (concepto == "3" || concepto == "5") {
            $("#Suministro" + prefijo).prop("disabled", true);
            $("#Ajuste" + prefijo).prop("disabled", true);
            $("#Observacion" + prefijo).prop("disabled", false);
            $("#Conclusion" + prefijo).prop("disabled", false);

            $("#Observacion" + prefijo).addClass("bg-amarillo");
            $("#Ajuste" + prefijo).removeClass("bg-amarillo");
            $("#Suministro" + prefijo).removeClass("bg-amarillo");
            $("#Conclusion" + prefijo).addClass("bg-amarillo");
        
            $("#Documento" + prefijo).addClass("bg-amarillo");
            $("#NumFoto" + prefijo).removeClass("bg-amarillo");
            $("#Documento" + prefijo).prop("disabled", false);
            $("#NumFoto" + prefijo).prop("disabled", false);
        }
    }

    if (tipo == 0)
        GuardarTemporal();

}

CambioConcepto(1,1);

//$.connection.hub.start().done(function () {
$("#FormPrevia").submit(function (e) {
    e.preventDefault();

    var id = document.getElementsByName("Ids[]");
    var observacion = document.getElementsByName("CObservacion[]");
    var concepto = $("#Concepto").val() * 1;
    var suministro = $.trim($("#Suministro").val());
    var ajuste = $.trim($("#Ajuste").val());
                
    if (concepto == 2) {
        if (suministro == "" && ajuste == "") {
            $("#Ajuste").focus();
            swal("Acción Cancelada", "Debe ingresar el ajusto ó suministro", "warning");
            return false;
        }
    }

    var a_id = "";
    var a_observacion = "";
    var a_opcion = "";
    var opcion = 0;

    for (var x = 0; x < id.length; x++) {
        if ($("#CSi" + x).prop("checked"))
            opcion = 1;
        else {
            if ($("#CNo" + x).prop("checked"))
                opcion = 2
            else {
                if ($("#CNA" + x).prop("checked"))
                    opcion = 3
            }
        }
        if (a_id == "") {
            a_id += id[x].value;
            a_observacion = observacion[x].value + "";
            a_opcion += opcion;
        } else {
            a_id += "|" + id[x].value;
            a_observacion += "|" + observacion[x].value + "";
            a_opcion += "|" + opcion;
        }
    }
    var parametros = $("#FormPrevia").serialize().replace(/\%2C/g, "") + "&a_id=" + a_id + "&a_observacion=" + a_observacion + "&a_opcion=" + a_opcion + "&idinstrumento=" + IdInstrumento +
        "&id=" + IdReporte + "&concepto=" + $("#Concepto option:selected").text() + "&version=" + IdVersion;
    var datos = LlamarAjax("Laboratorio/GuarOperPresion", parametros).split("|");
    if (datos[0] == "0") {
        IdReporte = datos[2];
        IdInstrumento = datos[3];
        GuardaTemp = 0;
        localStorage.removeItem("Pre" + NumCertificado + "-" + Ingreso);
        localStorage.removeItem("Table" + NumCertificado + "-" + Ingreso)
        localStorage.removeItem("Puntos" + NumCertificado + "-" + Ingreso)
        swal("", datos[1], "success");
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
});

function VistaInforme(reporte) {

    if (Ingreso == 0)
        return false;

    var concepto = $("#Concepto").val() * 1
    if (reporte > 0) {
        concepto = $("#Concepto" + reporte).val() * 1;
    }

    ActivarLoad();
    setTimeout(function () {
        var parametros = "ingreso=" + Ingreso + "&tipo=2&concepto=" + concepto;
        var datos = LlamarAjax("Laboratorio/RpInformeTecnico", parametros);
        DesactivarLoad();
        datos = datos.split("|");
        if (datos[0] == "0") {
            window.open("DocumPDF/" + datos[1]);
        } else {
            swal("", ValidarTraduccion(datos[1]), "warning");
        }

    }, 15);
}

function ImprimirInforme(reporte) {
        
    if (Ingreso == 0)
        return false;

    var concepto = 0;
    if (reporte > 0) {
        concepto = $("#Concepto" + reporte).val() * 1;
    }

    ActivarLoad();
    setTimeout(function () {
        var parametros = "ingreso=" + Ingreso + "&tipo=1&concepto=" + concepto;
        var datos = LlamarAjax("Laboratorio/RpInformeTecnico", parametros);
        DesactivarLoad();
        datos = datos.split("|");
        if (datos[0] == "0") {
            window.open("DocumPDF/" + datos[1]);
        } else {
            swal("", ValidarTraduccion(datos[1]), "warning");
        }

    }, 15);
}

function InformeTecnico(tipo) {

    var concepto = $("#Concepto").val() * 1;
    var reporte = 0;
    
    var id = IdReporte;
    var temporal = GuardarTemporal;
    if (tipo > 0) {
        concepto = $("#Concepto" + tipo).val() * 1;
        
        id = $("#IdReporte" + tipo).val() * 1;
        temporal = 0;
        reporte = NroReporte;
    }  
    if (Ingreso == 0)
        return false;

    if (temporal == 1 || id == 0) {
        swal("Acción Cancelada", "Debe primero guardar el reporte", "warning");
        return false;
    }
        
    if (concepto != 3 && concepto != 5) {
        swal("Acción Cancelada", "Solo se podrá generar informes cuando el ingreso no está apto para calibrar ó para mantenimiento y limpieza", "warning");
        return false;
    }

    if (InfoTecIngreso == 1) {

        ImprimirInforme(0);
        return false;
    }

    var mensaje = '¿Seguro que desea generar el informe técnico del ingreso número ' + Ingreso + '?';
    if (concepto == 5)
        mensaje = '¿Seguro que desea generar el informe de mantenimiento y limpieza del ingreso número ' + Ingreso + '?';

    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: mensaje,
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Si'),
        cancelButtonText: ValidarTraduccion('No'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                $.post("Laboratorio/InformeTecnicoDev", "ingreso=" + Ingreso + "&reporte=" + reporte + "&plantilla=" + NumCertificado)
                    .done(function (data) {
                        datos = data.split("|");
                        if (datos[0] == "0") {
                            $("#NroInforme" + reporte).val(datos[2]);
                            ImprimirInforme();
                            resolve()
                        } else {
                            reject(datos[1]);
                        }
                    })
            })
        }
    }]);
};
    
function VerFotos() {
    if (Ingreso == 0)
        return false;
    CargarModalFoto(Ingreso, Fotos,1);
}

function ReportarEquipo() {
    if (Ingreso == 0)
        return false;
    var datos = LlamarAjax("Laboratorio/BuscarReportesLab", "ingreso=" + Ingreso);
    if (datos != "[]") {
        var datarep = JSON.parse(datos);
        for (var x = 0; x < datarep.length; x++) {
            IdReporte = datarep[x].nroreporte * 1;
            $("#IdReporte" + IdReporte).val(datarep[x].id);
            $("#Concepto" + IdReporte).val(datarep[x].idconcepto).trigger("change");
            $("#Ajuste" + IdReporte).val(datarep[x].ajuste);
            $("#Suministro" + IdReporte).val(datarep[x].suministro);
            $("#Observacion" + IdReporte).val(datarep[x].observaciones);
            $("#Conclusion" + IdReporte).val(datarep[x].conclusion);
            $("#NroInforme" + IdReporte).val(datarep[0].informetecnico)
            $("#registroingreso" + IdReporte).html("<b>Registrado el día " + datarep[x].fecha + " por el técnico " + datarep[x].usuario + "</b>" + datarep[x].aprobado);
            if (datarep[x].idconcepto * 1 == 5 || datarep[x].idconcepto * 1 == 3 )
                MostrarFoto(IdReporte, 1);
        }
    }
    $('#tabreportaringreso a[href="#Reporte1"]').tab('show')
    NroReporte = 1;
    $("#ReportarEquipo").modal("show");
    $("#Concepto1").focus();
}

$("#formreportar").submit(function (e) {
    e.preventDefault();
    var idconcepto = $("#RConcepto").val() * 1;
    var concepto = $("#RConcepto option:selected").text();
    var ajuste = $.trim($("#RAjuste").val());
    var suministro = $.trim($("#RSuministro").val());

    if (idconcepto == 2 && ajuste == "" && suministro == "") {
        $("#RAjuste").focus();
        swal("Acción Cancelada", "Debe ingresar la descripción del ajuste y/ó suministro", "warning");
        return false;
    }

    var parametros = $("#formreportar").serialize() + "&concepto=" + concepto + "&idconcepto=" + idconcepto + "&ingreso=" + Ingreso + "&fecha=" + $("#FechaRec").val();
    var datos = LlamarAjax("Laboratorio/ReportarIngresoOpera", parametros).split("|");
    if (datos[0] == "0") {
        swal("", datos[1], "success");
        $("#idreporte").val(datos[2]);
        var numero = Ingreso;
        Ingreso = 0;
        BuscarIngreso(numero, "", 3)
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
});

/*function BuscarReporte(reporte) {
    var datos = LlamarAjax("Laboratorio/BuscarReporteOpera", "ingreso=" + Ingreso + "&reporte=" + reporte);
    if (datos != "[]") {
        var data = JSON.parse(datos);
        $("#RegistroReporte").html("<h3>Reporte realizado por " + data[0].usuario + " fecha " + data[0].fecha + data[0].aprobado);
        $("#idreporte").val(data[0].id);
        $("#RConcepto").val(data[0].idconcepto).trigger("change");
        $("#RObservacion").val(data[0].observacion);
        $("#RConclusion").val(data[0].conclusion);
        $("#RAjuste").val(data[0].ajuste);
        $("#RSuministro").val(data[0].suministro);
    } else {
        $("#RegistroReporte").html("");
        $("#idreporte").val("0");
        $("#RConcepto").val("2").trigger("change");
        $("#RObservacion").val("");
        $("#RConclusion").val("");
        $("#RAjuste").val("");
        $("#RSuministro").val("");
    }
}*/

function ModalCotizacion() {
    if (Ingreso == 0)
        return false;
    $("#modalcotizacion").modal("show");
}

function EnviarReporte(reporte) {

    var id = $("#IdReporte" + reporte).val() * 1;
    if (Ingreso == 0 || id == 0)
        return false;



    if (id == 0) {
        swal("Acción Cancelada", "Debe primero guardar el reporte", "warning");
        return false;
    }

    var concepto = $("#Concepto").val() * 1;
    if (reporte > 0)
        concepto = $("#Concepto" + reporte).val() * 1;
    if (concepto != 2) {
        swal("Acción Cancelada", "Solo se podrá enviar reportes técnicos cuando el ingreso requiere ajuste o suministro", "warning");
        return false;
    }

    var datos = LlamarAjax('Cotizacion/ModalEmpresa', "ingreso=" + Ingreso);
    var data = JSON.parse(datos);

    CorreoEmpresa = data[0].email;
    Empresa = data[0].cliente;

    if (ValidarCorreo(CorreoEmpresa) == 2) {
        swal("Acción Cancela", "el contacto " + data[0].contacto + " no posee correo válido registrado <br>Llame a comercial para registrar el correo del contacto", "warning");
        return false;
    }
    $("#rCliente").val(Empresa);
    $("#rCorreo").val(CorreoEmpresa);

    $("#rObserEnvio").val("");
    $("#rspreporte").html(Ingreso);
    $("#rReporte").val(reporte);
    $("#enviar_reporte").modal("show");
}

$.connection.hub.start().done(function () {

    $(".guardarreporte").click(function () {
        var reporte = NroReporte;
        var IdReporte = $("#IdReporte" + reporte).val() * 1;
        var idconcepto = $("#Concepto" + reporte).val() * 1;
        var ajuste = $.trim($("#Ajuste" + reporte).val());
        var suministro = $.trim($("#Suministro" + reporte).val());
        var observacion = $.trim($("#Observacion" + reporte).val());
        var conclusion = $.trim($("#Conclusion" + reporte).val());
        var concepto = $("#Concepto" + reporte + " option:selected").text();
        var fecha = $("#FechaRec").val();
        if (Ingreso == 0)
            return false;
        var mensaje = "";

        if ((idconcepto == 3 || idconcepto == 4) && observacion == "") {
            $("#Observacion" + reporte).focus();
            mensaje = "<br> Debe de ingresar la observación del por qué no se puede calibrar";
        }
        if (idconcepto == 3 && conclusion == "") {
            $("#Conclusion" + reporte).focus();
            mensaje = "<br> Debe de ingresar la conclusión del por qué no se puede calibrar" + mensaje;
        }

        if (idconcepto == 5 && conclusion == "") {
            $("#Conclusion" + reporte).focus();
            mensaje = "<br> Debe de ingresar la conclusión del informe de mantenimiento" + mensaje;
        }

        if (idconcepto == 2 && ajuste == "" && suministro == "") {
            $("#Ajuste" + reporte).focus();
            mensaje = "<br> Debe de ingresar la observación del juste o suministro" + mensaje;
        }
        if (idconcepto == 0) {
            $("#Concepto" + reporte).focus()
            mensaje = "<br> Debe de seleccionar un concepto " + mensaje;
        }

        if (mensaje != "") {
            swal("Acción Cancelada", mensaje, "warning");
            return false;
        }

        var parametros = "ingreso=" + Ingreso + "&idconcepto=" + idconcepto + "&concepto=" + concepto + "&ajuste=" + ajuste + "&suministro=" + suministro + "&id=" + IdReporte + "&observacion=" + observacion + "&reporte=" + reporte + "&fecha=" + fecha + "&conclusion=" + conclusion;
        var datos = LlamarAjax("Laboratorio/ReportarIngreso", parametros).split("|");
        if (datos[0] == "0") {
            notireportados.server.send(Ingreso);
            ConsularReportesIngresos();
            if (idconcepto == 1 || idconcepto == 4) {
                LimpiarTodo();
                $("#IngresoPresion").focus();
         
            } else {
                $("#IdReporte" + reporte).val(datos[2]);
            }
            swal("", datos[1], "success");
        } else {
            swal("Acción Cancelada", datos[1], "warning");
        }
    });

});

$("#formenviarreporte").submit(function (e) {

    e.preventDefault();
    var observacion = $.trim($("#rObserEnvio").val())
    var correo = $.trim($("#rCorreo").val());
    a_correo = correo.split(";");
    var reporte = $("#rReporte").val() * 1;

    for (var x = 0; x < a_correo.length; x++) {
        if (ValidarCorreo($.trim(a_correo[x])) == 2) {
            $("#rCorreo").focus();
            swal(ValidarTraduccion("Acción Cancelada"), ValidarTraduccion("Formato de correo inválido") + ":" + a_correo[x], "warning");
            return false;
        }
    }

    ActivarLoad();
    setTimeout(function () {
        var parametros = "ingreso=" + Ingreso + "&reporte=" + reporte + "&principal=" + CorreoEmpresa + " &e_email=" + correo + " &cliente=" + Empresa + " &observacion=" + observacion;
        var datos = LlamarAjax("Laboratorio/EnvioReporte", parametros);
        DesactivarLoad();
        datos = datos.split("|");
        if (datos[0] == "0") {
            swal("", ValidarTraduccion(datos[1]) + " " + datos[2], "success");
            $("#enviar_reporte").modal("hide");
        } else
            swal("Acción Cancelada", datos[1], "warning");
    }, 15);

    return false;
});

function MostrarFoto(reporte, numero) {
    var informe = $("#NroInforme" + reporte).val() * 1;
    $("#FotoMan" + reporte).attr("src", url + "imagenes/informemantenimineto/" + informe + "/" + numero + ".jpg");
}


function GuardarDocumento(reporte) {
    var documento = document.getElementById("Documento" + reporte);
    var informe = $("#NroInforme" + reporte).val() * 1;
    var numero = $("#NumFoto" + reporte).val() * 1;


    if ($.trim(documento.value) != "") {

        if (informe == 0) {
            swal("Acción Cancelada", "Debe generar primero el informe técnico", "warning");
            return false;
        }

        var file = documento.files[0];
        var data = new FormData();
        data.append('documento', file);
        var direccion = url + "Cotizacion/AdjuntarFoto";
        $.ajax({
            url: direccion,
            type: 'POST',
            contentType: false,
            data: data,
            processData: false,
            async: false,
            success: function (datos) {
                datos = datos.split("|");
                if (datos[0] == "0") {
                    datos = LlamarAjax("Laboratorio/SubirFoto", "ingreso=" + Ingreso + "&informe=" + informe + "&numero=" + numero + "&foto=" + datos[1]).split("|");
                    swal(datos[1]);
                    if (datos[0] == "0")
                        MostrarFoto(reporte, numero)
                } else {
                    swal(ValidarTraduccion(datos[1]), "", "error");
                }
            }
        });
    }
}

$('select').select2();

$("#Concepto").select2('destroy'); 