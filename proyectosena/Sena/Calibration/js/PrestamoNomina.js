﻿var IdEmpleado = 0;
var IdPrestamo = 0;
var DocumentoEmp = "";
var CanCuotas = 0;

tb = $('input');
$(tb).keypress(enter2tab);

tb = $('select');
$(tb).keypress(enter2tab);

d = new Date();
month = d.getMonth() + 1;
day = d.getDate();

var Anio = $.trim(d.getFullYear());
Anio = $.trim(Anio.substr(2, 2))


output = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-01';

var output2 = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-' +
    (day < 10 ? '0' : '') + day;

$("#FechaIniCuota").val(output2);

$("#CCargo").html(CargarCombo(26, 1));
$("#CEmpleado").html(CargarCombo(29, 1));

function enter2tab(e) {
    if (e.keyCode == 13) {
        id = $(this).attr("id");

        cb = parseInt($(this).attr('tabindex'));

        if (id == "DocumentoEmp") {
            if ($.trim($(this).val()) != "") {

                BuscarEmpleados($(this).val());
                return false;
            }
        }

        if ($(':input[tabindex=\'' + (cb + 1) + '\']') != null) {
            $(':input[tabindex=\'' + (cb + 1) + '\']').focus();
            $(':input[tabindex=\'' + (cb + 1) + '\']').select();
            e.preventDefault();

            return false;
        }

        if ($(':select[tabindex=\'' + (cb + 1) + '\']') != null) {
            console.log("1");
            $(':select[tabindex=\'' + (cb + 1) + '\']').focus();
            $(':select[tabindex=\'' + (cb + 1) + '\']').select();
            e.preventDefault();

            return false;
        }
    }
}

function LimpiarTodo() {
    $("#Documento").val("");
    DocumentoEmp = "";
    LimpiarDatos();
}

function LimpiarDatos() {
    IdEmpleado = 0;
    IdPrestamo = 0;
    DocumentoEmp = "";
    TipoDocumento = "";
    $("#TipoDocumento").val("");
    $("#Nombres").val("");
    $("#Cargo").val("").trigger("change");
    $("#Banco").val("").trigger("change");
    $("#TipoCuenta").val("").trigger("change");
    $("#Telefonos").val("");
    $("#Celular").val("");
    $("#IdEmpleado").val("0");
    $("#Sueldo").val("");
    $("#Cuenta").val("");
    $("#Descripcion").val("");
    $("#Monto").val("");
    $("#Saldo").val("");
    $("#TipoPrestamo").val("VALE").trigger("change");
    $("#PagoPrestamo").val("EFECTIVO").trigger("change");
    $("#FechaIniCuota").val(output2);
}

$("#TipoPrestamo").val("VALE").trigger("change");

function ValidarEmpleado(documento) {
    if ($.trim(DocumentoEmp) != "") {
        if (DocumentoEmp != documento) {
            DocumentoEmp = "";
            LimpiarDatos();
        }
    }
}

function ModalEmpleados() {
    $("#modalEmpleados").modal("show");
}

function BuscarEmpleado(documento) {

    if ($.trim(documento) == "")
        return false;

    if (DocumentoEmp == documento)
        return false;

    LimpiarDatos();
    DocumentoEmp = documento;
    var parametros = "documento=" + documento;
    var datos = LlamarAjax('Recursohumano/BuscarEmpleados', parametros);
    if (datos != "[]") {
        var data = JSON.parse(datos);
        IdEmpleado = data[0].id;
        $("#TipoDocumento").val(data[0].tipodocumento);
        $("#Cargo").val(data[0].cargo);
        $("#Banco").val(data[0].banco);
        $("#Sueldo").val(formato_numero(data[0].sueldo,0,".",",",""));
        $("#Cuenta").val(data[0].cuenta);
        $("#TipoCuenta").val(data[0].tipocuenta);
        $("#Nombres").val(data[0].nombrecompleto);
        $("#FechaInicio").val(data[0].fechainicio);
        $("#FechaFinal").val(data[0].fechafin);
        $("#Telefonos").val(data[0].telefono);
        $("#Celular").val(data[0].celular);
        $("#IdEmpleado").val(data[0].id);
        $("#Correo").val(data[0].email);
    }
}

function CalTipoPrestamo(prestamo) {
    var formapago = $("#FormaPago").val();
    var opcion = "";
    switch (prestamo) {
        case "VALE":
            CanCuotas = 1;
            $("#FormaPago").html("<option value='QUINCENAL'>QUINCENAL</option>");
            $("#Prima").val("0");
            $("#Prima").prop("readonly", true);
            break;
        case "PRESTAMO":
            CanCuotas = 6;
            $("#FormaPago").html("<option value='QUINCENAL'>QUINCENAL</option><option value='MENSUAL'>MENSUAL</option>");
            $("#Prima").val("0");
            $("#Prima").prop("readonly", false);
            break;
        case "CONVENIO":
            CanCuotas = 50;
            $("#FormaPago").html("<option value='QUINCENAL'>QUINCENAL</option><option value='MENSUAL'>MENSUAL</option>");
            $("#Prima").val("0");
            $("#Prima").prop("readonly", false);
            break;
    }
    for (var x = 1; x <= CanCuotas; x++) {
        opcion += "<option value='" + x + "'>" + x + "</option>";  
    }
    $("#NCuota").html(opcion);
}

function CambioFormaPago(forma) {
    var cuota = CanCuotas;
    var opcion = "";
    if (forma == "MENSUAL")
        cuota = CanCuotas / 2;
    for (var x = 1; x <= cuota; x++) {
        opcion += "<option value='" + x + "'>" + x + "</option>";
    }
    $("#NCuota").html(opcion);
    $("#NCuota").val(1).trigger("change");
    
}

function CalcularCuota(Caja) {
    if (Caja != "")
        ValidarTexto(Caja, 1);
    var prestamo = NumeroDecimal($("#Monto").val());
    var cuota = $("#NCuota").val();
    var prima = NumeroDecimal($("#Prima").val());
    var formapago = $("#FormaPago").val();

    if (prima > prestamo) {
        $("#Prima").val("0");
        prima = 0;
    }
    
    var monto = prestamo - prima;
    cuota = monto / cuota;
    $("#ValorCuota").val(formato_numero(cuota, 0,_CD,_CM,""));
}

$("#tablamodalempleados > tbody").on("dblclick", "tr", function (e) {
    var row = $(this).parents("td").context.cells;
    var documento = row[1].innerText;
    $("#Documento").val(documento);
    BuscarEmpleado(documento);
    $("#modalEmpleados").modal("hide");
});

function GuardarPrestamo() {
    var idempleado = $("#IdEmpleado").val() * 1;
    var idprestamo = $("#IdPrestamo").val() * 1;
    var sueldo = NumeroDecimal($("#Sueldo").val());
    var prestamo = NumeroDecimal($("#Monto").val());
    var prima = NumeroDecimal($("#Prima").val());
    var cuota = NumeroDecimal($("#NCuoto").val());
    var tipo = $("#TipoPrestamo").val();
    var formapago = $("#FormaPago").val();
    var pago = $("#PagoPrestamo").val();


    if (idempleado == 0) {
        $("#Documento").focus();
        swal("Acción Cancelada", "Debe de seleccionar un empleado", "success");
        return false;
    }

    if (prestamo == 0) {
        $("#Monto").focus();
        swal("Acción Cancelada", "Debe de ingresar el monto del préstamo", "success");
        return false;
    }

    if (formapago == "QUINCENAL")
        sueldo = sueldo / 2;

    if (prestamo == 0) {
        $("#Monto").focus();
        swal("Acción Cancelada", "Debe de ingresar el monto del préstamo", "success");
        return false;
    }

}

$("#formPrestamo").submit(function (e) {
    e.preventDefault();
    var parametros = $("#formPrestamo").serialize().replace(/\%2C/g, "");
    var datos = LlamarAjax("Recursohumano/GuardarPrestamo", parametros).split("|");
    if (datos["0"] == "0") {
        IdPrestamo = datos[2] * 1;
        $("#IdPrestamo").val(IdPrestamo);
        swal("", datos[1], "success");
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
});

function ConsultarPrestamos() {


    var empleado = $("#CEmpleado").val() * 1;
    var cargo = $("#CCargo").val() * 1;
    var tipo = $.trim($("#CTipo").val());
    var estado = $("#CEstado").val() * 1;
    
    ActivarLoad();
    setTimeout(function () {
        var parametros = "empleado=" + empleado + "&cargo=" + cargo + "&tipo=" + tipo + "&estado=" + estado;
        var datos = LlamarAjax("Recursohumano/ConsultarPrestamos", parametros);
        DesactivarLoad();

        var datajson = JSON.parse(datos);
        $('#TablaPrestamos').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "estado_cuenta" },
                { "data": "id" },
                { "data": "empleado" },
                { "data": "cargo" },
                { "data": "sueldo", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "fechaini" },
                { "data": "tipo" },
                { "data": "descripcion" },
                { "data": "monto", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "cuotas" },
                { "data": "valorcuota", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "prima", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "saldo", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "pagado", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "registro" }
            ],

            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy', 'colvis'
            ]
        });
    }, 15);
}



$('select').select2();
DesactivarLoad();


