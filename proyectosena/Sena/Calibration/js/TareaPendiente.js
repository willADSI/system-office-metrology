﻿
$("#FechaDesde").val("");
$("#FechaHasta").val("");

$("#Magnitud").html(CargarCombo(1, 1));
$("#Equipo").html(CargarCombo(2, 1));
$("#Marca").html(CargarCombo(3, 1));
$("#Cliente").html(CargarCombo(9, 1));
$("#Modelo").html(CargarCombo(5, 1, "", "-1"));
$("#Intervalo").html(CargarCombo(6, 1, "", "-1"));

$("#Asesor, #EspecialistaPro").html(CargarCombo(13, 1));

var TablaProgramacion;

d = new Date();
month = d.getMonth() + 1;
day = d.getDate();

var Anio = $.trim(d.getFullYear());
Anio = $.trim(Anio.substr(2, 2))

output2 = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-' +
    (day < 10 ? '0' : '') + day;

var FechaDia = d.getFullYear() + '/' +
    (month < 10 ? '0' : '') + month + '/' +
    (day < 10 ? '0' : '') + day;


function BuscarTareas() {
    var magnitud = $("#Magnitud").val() * 1;
    var equipo = $("#Equipo").val();
    var modelo = $("#Modelo").val();
    var marca = $("#Marca").val() * 1;
    var intervalo = $("#Intervalo").val();
    var ingreso = $("#Ingreso").val() * 1;
    var cotizacion = $("#Cotizacion").val() * 1;
    var remision = $("#Remision").val() * 1;
    var serie = $.trim($("#CSerie").val());
    var cliente = $("#Cliente").val() * 1;
    var asesor = $("#Asesor").val() * 1;

    var fechad = $("#FechaDesde").val();
    var fechah = $("#FechaHasta").val();

    var habitual = $("#Habitual").val();
    var cotizado = $("#Cotizado").val();
    var aprobado = $("#Aprobado").val();
    var recibidolab = $("#RecibidoLab").val();
    var reportado = $("#Reportado").val();
    var ajuste = $("#Ajuste").val();
    var certificado = $("#Certificado").val();
    var informe = $("#Informe").val();
    var noatorizado = $("#NoAtorizado").val();
    var convenio = $("#Convenio").val();
    var recibidoing = $("#RecibidoLog").val();

    if (TablaProgramacion != null)
        TablaProgramacion.colReorder.reset();

    ActivarLoad();
    setTimeout(function () {
        var parametros = "cliente=" + cliente + "&ingreso=" + ingreso + "&cotizacion=" + cotizacion + "&remision=" + remision + "&fechad=" + fechad +
            "&fechah=" + fechah + "&magnitud=" + magnitud + "&equipo=" + equipo + "&marca=" + marca + "&modelo=" + modelo +
            "&intervalo=" + intervalo + "&serie=" + serie + "&asesor=" + asesor + "&habitual=" + habitual +
            "&cotizado=" + cotizado + "&aprobado=" + aprobado + "&recibidolab=" + recibidolab + "&reportado=" + reportado +
            "&certificado=" + certificado + "&informe=" + informe + "&noautorizado=" + noatorizado + "&convenio=" + convenio +
            "&recibidoing=" + recibidoing + "&ajuste=" + ajuste;
        var datos = LlamarAjax("Laboratorio/TareasPendientes", parametros);
        DesactivarLoad();
        var datajson = JSON.parse(datos);
        TablaProgramacion = $('#TablaTareasPendiente').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "opciones" },
                { "data": "imagen" },
                { "data": "ingreso", "className": "text-XX" },
                { "data": "plantilla" },
                { "data": "equipo" },
                { "data": "rango" },
                { "data": "garantia" },
                { "data": "cliente" },
                { "data": "fechaing" },
                { "data": "tiempolab" },
                { "data": "cotizado" },
                { "data": "fechaaprocoti" },
                { "data": "fechaaproajus" },
                { "data": "tiempoapro" },
                { "data": "diaapro" },
                { "data": "fechaentrega" },
                { "data": "programacion" },
                { "data": "recibidolab" },
                { "data": "reportado" },
                { "data": "certificado" },
                { "data": "informe" },
                { "data": "noautorizado" },
                { "data": "recingreso" },
                { "data": "id" },
                { "data": "codigo" },
            ],

            "fnRowCallback": function (nRow, aData) {
                if (aData.programacion) {
                    $('td', nRow).addClass("bg-verde-claro");
                }
            },

            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy', 'colvis'
            ],
            "colReorder": true
        });
    }, 15);
}

function LlamarFotoDet(ingreso, fotos) {
    if (fotos == 0)
        return false
    CargarModalFoto(ingreso, fotos, 1);
}

$('#TablaTareasPendiente tbody').on('click', 'tr', function () {
    $(this).toggleClass('selected');
});


function Programar() {
    var datos = TablaProgramacion.rows('.selected').data();
    console.log(datos);
    var ingresos = "";
    proingreso = "";
    for (var x = 0; x < datos.length; x++) {
        if (datos[x].reportado == "NO" && $.trim(datos[x].programacion) == "") {
            if (proingreso == "")
                proingreso = datos[x].id;
            else
                proingreso += "," + datos[x].id;
            ingresos += "<option value='" + datos[x].id + "' title='" + datos[x].plantilla + "' >" + datos[x].ingreso + " " + datos[x].codigo + "</option>";
        }
    }
    if (datos.length == 0 || proingreso == "") {
        swal("Acción cancelada", "Debe de seleccionar por lo menos un ingreso sin estado en calibración y sin fecha de programación", "warning");
        return false;
    }

    var fecha = new Date(FechaDia);
    fecha.setDate(fecha.getDate() + 8);

    fecha = fecha.getFullYear() + '-' +
        ((fecha.getMonth() + 1) < 10 ? '0' : '') + (fecha.getMonth() + 1) + '-' +
        (fecha.getDate() < 10 ? '0' : '') + fecha.getDate();

    $("#FechaPro").val(fecha);
    $("#IdPro").val("0");
    $("#HoraPro").val("08:00");
    $("#EspecialistaPro").val("").trigger("change");
    $("#ObservacionPro").val("");
    $("#AlbunFotosPro").html("");
    $("#DetalleIngreso").html("");
    $("#NINgresos").html(ingresos);
    $("#ModalProgramacion").modal({ backdrop: 'static', keyboard: false }, 'show');
}

function CerrarProgramacion() {
    var image = $("#FotoIngresoPro");
    $.removeData(image, 'elevateZoom');
    $('.zoomContainer').remove();// remove zoom container from DOM
    $(".ligthbox").remove();
    $("#ModalProgramacion").modal("hide");
}

function CambioIngresoPro(id) {
    var datos = LlamarAjax("Laboratorio/DetalleIngresoPro", "id=" + id);
    var data = JSON.parse(datos);
    $("#DetalleIngreso").html(data[0].equipo + "<br>" + data[0].cliente);
    CargarFotosPro(data[0].ingreso, data[0].fotos);
}


function ProgramarIngreso() {

    var fecha = $("#FechaPro").val();
    var id = $("#IdPro").val() * 1;
    var hora = $("#HoraPro").val();
    var asesor = $("#EspecialistaPro").val() * 1;
    var observacion = $.trim($("#ObservacionPro").val());
    if (fecha == "") {
        $("#FechaPro").focus();
        swal("Acción Cancelada", "Debe de ingresar la fecha de programación", "warning");
        return false;
    } else {
        if (fecha < output2) {
            $("#FechaPro").focus();
            swal("Acción Cancelada", "La fecha de programación debe ser mayor o igual a la fecha actual", "warning");
            return false;
        }
    }
    if (hora == "") {
        $("#HoraPro").focus();
        swal("Acción Cancelada", "Debe de ingresar la hora de programación", "warning");
        return false;
    }
    var parametros = "id=" + id + "&idplantilla=" + proingreso + "&fecha=" + fecha + "&hora=" + hora + "&asesor=" + asesor + "&observacion=" + observacion;
    var datos = LlamarAjax("Laboratorio/ProgramarIngresoPro", parametros).split("|");
    if (datos[0] == "0") {
        BuscarTareas();
        swal("", datos[1], "success");
        $("#ModalProgramacion").modal("hide");
    } else
        swal("Acción Cancelada", datos[1], "warning");

}

function EditarProgramacion(id) {
    var datos = LlamarAjax("Laboratorio/DatosProgramacionPro", "id=" + id);
    var data = JSON.parse(datos);

    proingreso = id;
    var ingresos = "<option value='" + id + "' title='" + data[0].plantilla + "' >" + data[0].ingreso + " " + data[0].codigo + "</option>";

    $("#FechaPro").val(data[0].fecha);
    $("#IdPro").val(data[0].id);
    $("#HoraPro").val(data[0].hora);
    $("#EspecialistaPro").val((data[0].tecnico * 1 == 0 ? "" : data[0].tecnico)).trigger("change");
    $("#ObservacionPro").val(data[0].observacion);

    $("#NINgresos").html(ingresos);
    $("#NINgresos").val(id).trigger("change");
    $("#ModalProgramacion").modal("show");

}

function CargarFotosPro(ingreso, fotos) {

    if (fotos == 0)
        return false;
    ruta = "imagenes/ingresos/" + ingreso + "/";
    var contenedor = "";
    contenedor = "<div class='row'><div class='col-xs-12 col-sm-12'>" +
        "<img id='FotoIngresoPro' class='zoom-img' src='" + ruta + "1.jpg' height='300px' data-zoom-image='" + ruta + "1.jpg'></div></div><hr>";
    contenedor += "<div class='row'><div class='col-xs-12 col-sm-12 scrolling-wrapper' id='my_images'>";
    for (var x = 1; x <= fotos; x++) {
        contenedor += "<a href='#' title='Foto número " + x + "' data-image='" + ruta + x + ".jpg' data-zoom-image='" + ruta + x + ".jpg'><img id='FotoIngreso' src='" + ruta + x + ".jpg' height='80px'></a>&nbsp;";
    }
    contenedor += "</div></div>";

    $("#AlbunFotosPro").html(contenedor);

    setTimeout(function () {

        $('#FotoIngresoPro').elevateZoom({
            scrollZoom: true,
            cursor: "crosshair",
            zoomWindowFadeIn: 500,
            zoomWindowFadeOut: 750,
            borderSize: 1,
            zoomLevel: 0.8,
            gallery: "my_images"
        });

    }, 300);
    $("#AlbunFotosPro").html(contenedor);
}

$('select').select2();
DesactivarLoad();

$("#NINgresos").select2('destroy'); 
