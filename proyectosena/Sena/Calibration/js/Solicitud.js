﻿var DocumentoCli = "";
var IdServicio = 0;
var Solicitud = 0;
var IdSolicitud = 0;
var canfila = 0;
var Estado = "Temporal";
var OpcionEquipo = 0;

var AutoContacto = [];
var AutoDireccion = [];
var AutoTelefono = [];
var AutoCorreo = [];

d = new Date();
month = d.getMonth() + 1;
day = d.getDate();

var Anio = $.trim(d.getFullYear());
Anio = $.trim(Anio.substr(2, 2))


output = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-01';

output2 = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-' +
    (day < 10 ? '0' : '') + day;

$("#CFechaDesde").val(output);
$("#CFechaHasta").val(output2);

var datos = LlamarAjax("Configuracion", "opcion=CargaComboInicial&tipo=3").split("||");

$("#CCliente").html(datos[3]);
$("#Magnitud, #CMagnitud").html(datos[0]);
$("#CEquipo").html(datos[1]);
$("#CMarca, #Marca").html(datos[2]);
$("#CModelo").html(datos[4]);
$("#CIntervalo").html(datos[5]);
$("#CUsuario").html(datos[6]);
$("#IMedida").html(datos[7]);

function LimpiarTodo() {
    LimpiarDatos();
    $("#Cliente").val("");
}

LimpiarTodo();

function LimpiarDatos() {

    IdIngreso = 0;
    DescuentoCli = 0;
    IdCliente = 0;
    DocumentoCli = "";
    TipoCliente = 0;
    PlazoPago = 0;
    TipoPrecio = 0;
    OpcionEquipo = 0;
    IdSolicitud = 0;
    Solicitud = 0;
    Cotizacion = 0;
    canfila = 0;
    CorreoEmpresa = "";
    Estado = "Temporal";

    $("#NombreCliente").val("");
    $("#Solicitud").val("");
    $("#Remision").val("");

    $("#Solicitud").attr("disabled", false);
    $("#Remision").prop("disabled", false);
    $("#Solicitud").prop("disabled", false);
    $("#Recepcion").val("").trigger("change");
    $("#Estado").val(Estado);
    $("#Sede").html("");
    $("#Contacto").html("");
    $("#FechaReg").val("");
    $("#UsuarioReg").val("");
    $("#OArchivo").val("");

    $("#Magnitud").prop("disabled", false);
    $("#Intervalo").prop("disabled", false);
    $("#Equipo").prop("disabled", false);
    $("#Marca").prop("disabled", false);
    $("#Modelo").prop("disabled", false);

    $("#CheRecoger").prop("checked", "").trigger("change");
    $("#CheEntregar").prop("checked", "").trigger("change");
    $("#SaldoClienteDev").html("");
    $("#divAnulaciones").html("");

    $("#PlazoPago").val("0");
    $("#Totales").val("0");

    $("#bodyIngreso").html("");

    $("#Email").val("");
    $("#Telefonos").val("");
    $("#TipoCliente").val("");
    $("#Observacion").val("");

    $("#TBSolicitudDet").html("");

    $("#Observacion").val("");

    $("#MRDireccion").val("");
    $("#MRContacto").val("");
    $("#MRTelefono").val("");
    $("#MRCorreo").val("");
    $("#MEDireccion").val("");
    $("#MEContacto").val("");
    $("#METelefono").val("");
    $("#MECorreo").val("");

}

function ValidarCliente(documento) {
    if ($.trim(DocumentoCli) != "") {
        if (DocumentoCli != documento) {
            DocumentoCli = "";
            LimpiarDatos();
        }
    }
}

function BuscarRemisiom(remision) {
    remision = remision * 1;
    if (remision == 0) {
        if (Remision > 0)
            LimpiarDatos();
        return false;
    }
    if (remision != Remision && $("#NombreCliente").val() != "")
        LimpiarDatos();

    var datos = LlamarAjax("Solicitud","opcion=BuscarRemision&remision=" + remision).split("|");
    if (datos[0] == "0") {
        var data = JSON.parse(datos[1]);
        var sede = data[0].idsede;
        var contacto = data[0].idcontacto;
        var documento = data[0].documento;
        $("#Cliente").val(documento);
        DocumentoCli = "";
        Remision = remision;
        BuscarCliente(documento, 1);
        $("Sede").val(sede).trigger("change");
        $("Contacto").val(contacto).trigger("change");
        swal("", datos[2], "success");
    } else {
        $("#Remision").select();
        $("#Remision").focus();
        swal("Acción Cancelada", datos[1], "warning");
    }

}

function CargarModelos(marca) {
    $("#Modelo").html(CargarCombo(5, 1, "", marca));
}

function CargarIntervalos(magnitud) {
    $("#Intervalo").html(CargarCombo(6, 3, "", magnitud));
    if (OpcionEquipo == 0) {
        if (magnitud * 1 > 0)
            $("#Equipo").html(CargarCombo(2, 1, "", magnitud));
        else
            $("#Equipo").html(CargarCombo(58, 1));
    }
 
}

function MagnitudEquipo(equipo) {
    equipo = equipo * 1;
    $("#Metodo").html(CargarCombo(17, 1, "", equipo));
    var datos = LlamarAjax("Cotizacion","opcion=MagnitudEquipo&id=" + equipo);
    if (datos != "[]") {
        var data = JSON.parse(datos);
        OpcionEquipo = 1;
        if (data[0].idmagnitud * 1 != $("#Magnitud").val() * 1)
            $("#Magnitud").val(data[0].idmagnitud).trigger("change");
        OpcionEquipo = 0;
    }
}

function BuscarCliente(documento, tipo) {

    if ($.trim(documento) == "")
        return false;

    if (DocumentoCli == documento)
        return false;

    if (tipo == 0)
        LimpiarDatos();

    DocumentoCli = documento;

    var parametros = "documento=" + $.trim(documento);
    var datos = LlamarAjax("Cotizacion","opcion=BuscarCliente&" + parametros);
    datos = datos.split("|");
    if (datos[0] == "0") {
        var data = JSON.parse(datos[1]);
        IdCliente = data[0].id;
        TablaPrecio = data[0].tablaprecio;
        DescuentoCli = data[0].descuento;
        PlazoPago = data[0].plazopago;
        TipoCliente = data[0].tipocliente;
        CorreoEmpresa = data[0].email;
        $("#TipoCliente").val(TipoCliente);
        $("#Descuento").val(DescuentoCli);
        $("#NombreCliente").val(data[0].nombrecompleto);
        $("#Contacto").html(datos[2]);
        $("#Sede").html(datos[3]);
        $("#Sede").focus();
        $("#Estado").val(data[0].estusu);
        $("#Estado").val(data[0].supusu);
        $("#Remision").prop("disabled", true);
        $("#Solicitud").prop("disabled", true);

        SaldoActual(IdCliente, 1);
        $("#SaldoClienteDev").html(SaldoTotal(IdCliente));

        TablaDetSolicitud();
        AutoCompletar();
        
        DesactivarLoad();

    } else {
        DocumentoCli = "";
        $("#Cliente").val("");
        $("#Cliente").focus();
        swal("Acción Cancelada", "El documento " + documento + ", no se encuentra registrado como cliente", "warning");
    }

}

function DatosContacto(contacto) {
    $("#Email").val("");
    $("#Telefonos").val("");
    if (contacto * 1 == 0)
        return false;
    var datos = LlamarAjax("Cotizacion","opcion=DatosContacto&id=" + contacto);
    if (datos != "[]") {
        var data = JSON.parse(datos);
        $("#Email").val(data[0].email);
        $("#Telefonos").val(data[0].telefonos);
    }
}

function LimpiarServicio() {
    $("#Ingreso").val("0");
    $("#Magnitud").val("").trigger("change");
    $("#Marca").val("").trigger("change");
    $("#Cantidad").val("1");
    $("#NombreCa").val($("#NombreCliente").val());
    $("#Serie").val("");
    $("#Acreditacion").val("SI").trigger("change");
    $("#Sitio").val("NO").trigger("change");
    $("#Express").val("NO").trigger("change");
    $("#SubContratado").val("NO").trigger("change");
    $("#Punto").val("N/A");
    $("#Metodo").val("LABORATORIO");
    $("#Declaracion").val("N/A");
    $("#Observacion").val("N/A");
    $("#Calibracion").val("N/A");
    $("#Certificado").val("").trigger("change");
    $("#Proxima").val("");

    $("#Magnitud").prop("disabled", false);
    $("#Intervalo").prop("disabled", false);
    $("#Equipo").prop("disabled", false);
    $("#Marca").prop("disabled", false);
    $("#Modelo").prop("disabled", false);

}

function BuscarServicio() {
    if (IdCliente > 0) {
        LimpiarServicio();
        IdServicio = 0;
        $("#ModalArticulos").modal("show");
    }
}

$("#tablamodalclientesol > tbody").on("click", "tr", function (e) {
    var row = $(this).parents("td").context.cells;
    var numero = row[1].innerText;
    $("#modalClientesSol").modal("hide");
    $("#Cliente").val(numero);
    BuscarCliente(numero, 0);
});

function CambioTiempo() {
    d = new Date();
    month = d.getMonth() + 1;
    day = d.getDate();
    var fecha = (d.getFullYear() * 1 + 1) + "-" + (month < 10 ? '0' : '') + month + "-" + (day < 10 ? "0" : "") + day;
    $("#Proxima").val(fecha);
}

$("#FormArticuloSol").submit(function (e) {
    e.preventDefault();
    if ($("#Intervalo").prop("disabled") == true) {
        $("#Magnitud").prop("disabled", false);
        $("#Intervalo").prop("disabled", false);
        $("#Equipo").prop("disabled", false);
        $("#Marca").prop("disabled", false);
        $("#Modelo").prop("disabled", false);
    }
    
    var parametros = $(this).serialize() + "&Id=" + IdServicio + "&Cliente=" + IdCliente + "&Solicitud=" + Solicitud;

    var datos = LlamarAjax("Solicitud","opcion=AgregarItemCotizacion&" + parametros).split("|");
    if (datos[0] == "0") {
        $("#Magnitud").focus();
        swal("", datos[1], "success");
        LimpiarServicio();
        TablaDetSolicitud();
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }

    return false;

});

function SeleccionarFila(id) {

    ActivarLoad();
    setTimeout(function () {

        var datos = LlamarAjax("Solicitud","opcion=TablaSolicitudDetalle&solicitud=0&cliente=" + IdCliente + "&id=" + id);
        var data = JSON.parse(datos);
        IdServicio = data[0].id * 1;
        if (data[0].ingreso * 1 > 0) {
            $("#Magnitud").prop("disabled", true);
            $("#Intervalo").prop("disabled", true);
            $("#Equipo").prop("disabled", true);
            $("#Marca").prop("disabled", true);
            $("#Modelo").prop("disabled", true);
        } else {
            $("#Magnitud").prop("disabled", false);
            $("#Intervalo").prop("disabled", false);
            $("#Equipo").prop("disabled", false);
            $("#Marca").prop("disabled", false);
            $("#Modelo").prop("disabled", false);
        }
        $("#Ingreso").val(data[0].ingreso);
        $("#Magnitud").val(data[0].idmagnitud).trigger("change");
        $("#Intervalo").val(data[0].idintervalo).trigger("change");
        $("#Equipo").val(data[0].idequipo).trigger("change");
        $("#Marca").val(data[0].idmarca).trigger("change");
        $("#Modelo").val(data[0].idmodelo).trigger("change");
        $("#Cantidad").val(data[0].cantidad);
        $("#NombreCa").val(data[0].nombreca);
        $("#Serie").val(data[0].serie);
        $("#Acreditacion").val(data[0].acreditado).trigger("change");
        $("#Sitio").val(data[0].sitio).trigger("change");
        $("#Express").val(data[0].express).trigger("change");
        $("#SubContratado").val(data[0].subcontratado).trigger("change");
        $("#Punto").val(data[0].punto);
        $("#Metodo").val(data[0].metodo);
        $("#Declaracion").val(data[0].declaracion);
        $("#Observacion").val(data[0].observacion);
        $("#Calibracion").val(data[0].proxima);
        $("#Certificado").val(data[0].certificado).trigger("change");
        $("#ModalArticulos").modal("show");
        DesactivarLoad();
    }, 15);
}

function TablaDetSolicitud() {
    var datos = LlamarAjax("Solicitud","opcion=TablaSolicitudDetalle&cliente=" + IdCliente + "&id=0&solicitud=" + Solicitud)
    var data = JSON.parse(datos);
    canfila = 0;
    var resultado = "";
    for (var x = 0; x < data.length; x++) {
        resultado += "<tr ondblclick='SeleccionarFila(" + data[x].id + ")'>" +
            "<td>" + (x+1) + "</td>" +
            "<td>" + data[x].ingreso + "</td>" +
            "<td>" + data[x].equipo + "</td>" +
            "<td aling='center'>" + data[x].cantidad + "</td>" +
            "<td>" + data[x].servicio + "</td>" +
            "<td>" + data[x].metodo + "</td>" +
            "<td>" + data[x].direccion + "</td>" +
            "<td>" + data[x].declaracion + "</td>" +
            "<td>" + data[x].observacion + "</td>" +
            "<td>" + data[x].certificado + "</td>" + 
            "<td>" + data[x].opcion + "</td></tr>";
        canfila += data[x].cantidad*1;
    }
    $("#Totales").val(canfila)
    $("#TBSolicitudDet").html(resultado);
}

function CambioContratacion() {
    var sitio = $("#Sitio").val();
    var express = $("#Express").val();
}



function EliminarItem(id, equipo) {
    if (Estado != "Temporal" && Estado != "Pendiente") {
        swal("Acción cancelada", "No se puede editar una solicitud en estado " + Estado, "warning");
        return false;
    }
    swal.queue([{
        title: 'Advertencia',
        text: '¿Seguro que desea eliminar el item del equipo ' + equipo + '?',
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: 'Si',
        cancelButtonText: 'No',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                $.post(url_servidor + "Solicitud","opcion=EliminarItemSolicitud&id=" + id + "&Solicitud=" + Solicitud)
                    .done(function (data) {
                        datos = data.split("|");
                        if (datos[0] == "0") {
                            TablaDetSolicitud()
                            $.jGrowl(datos[1], { life: 1500, theme: 'growl-success', header: '' });
                            resolve();
                        } else {
                            reject(datos[1]);
                        }
                    })
            })
        }
    }]);
}

function EliminarSolicitud() {
    if (IdCliente == 0)
        return false;
    if (Estado != "Temporal") {
        swal("Acción cancelada", "No se puede editar una solicitud en estado " + Estado, "warning");
        return false;
    }
    swal.queue([{
        title: 'Advertencia',
        text: '¿Seguro que desea eliminar todos los items de la solicitud de cotización?',
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: 'Si',
        cancelButtonText: 'No',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                $.post(url_servidor + "Solicitud","opcion=EliminarSolicitud&cliente=" + IdCliente + "&Solicitud=" + Solicitud)
                    .done(function (data) {
                        datos = data.split("|");
                        if (datos[0] == "0") {
                            TablaDetSolicitud()
                            $.jGrowl(datos[1], { life: 1500, theme: 'growl-success', header: '' });
                            resolve();
                        } else {
                            reject(datos[1]);
                        }
                    })
            })
        }
    }]);
}

$("#formsolicitud").submit(function (e) {
    e.preventDefault();
    if (Estado != "Temporal" && Estado != "Pendiente") {
        swal("Acción cancelada", "No se puede editar una solicitud en estado " + Estado, "warning");
        return false;
    }
    if (canfila == 0) {
        swal("Acción Cancelada", "Debe de ingresar por lo mínimo un instrumento o equipo para cotizar", "warning");
        return false;
    }

    var parametros = $("#formsolicitud").serialize() + "&IdSolicitud=" + IdSolicitud + "&Cliente=" + IdCliente;
    ActivarLoad();
    setTimeout(function () {
        var datos = LlamarAjax("Solicitud","opcion=GenerarSolicitud&" + parametros).split("|");
        DesactivarLoad();
        if (datos[0] == "0") {
            if (IdSolicitud == 0) {
                Solicitud = datos[2] * 1;
                $("#Solicitud").val(Solicitud);
                IdSolicitud = datos[3] * 1;
            }
            swal("", datos[1] + " " + Solicitud, "success");
        } else {
            swal("Acción Cancelada", datos[1], "warning");
        }
    }, 15);
    
    return false;
});

function AgregarIntervalo(numero) {

    if ($("#Intervalo").prop("disabled") == true)
        return false;

    var magnitud = $("#Magnitud").val() * 1;
    if (magnitud == 0) {
        $("#Magnitud").focus();
        swal("Acción Cancelada", "Debe de seleccionar una magnitud", "warning");
        return false;
    }

    $("#IDesde").val("");
    $("#IHasta").val("");
    $("#IMedida").val("").trigger("change");
    $("#NumIntervalo").val(numero);
    $("#modalIntervalo").modal("show");

}

function AgregarOpcion(tipo, mensaje, id) {

    var magnitud = $("#Magnitud").val() * 1;
    var marca = $("#Marca").val() * 1

    switch (tipo) {

        case 1:
            if (magnitud == 0) {
                $("#Magnitud").focus();
                swal("Acción Cancelada", "Debe de seleccionar una magnitud", "warning");
                return false;
            }
            break;
        case 3:
            if (marca == 0) {
                $("#Marca").focus();
                swal("Acción Cancelada", "Debe de seleccionar una marca", "warning");
                return false;
            }
            break;
    }

    swal({
        title: 'Agregar Opción',
        text: mensaje,
        input: 'text',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Si'),
        cancelButtonText: ValidarTraduccion('No'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',

        inputValidator: function (value) {
            return new Promise(function (resolve, reject) {
                if ($.trim(value)) {
                    var datos = LlamarAjax("Cotizacion","opcion=GuardarOpcion&tipo=" + tipo + "&magnitud=" + magnitud + "&marca=" + marca + "&descripcion=" + value + "&tipoconcepto=0");
                    datos = datos.split("|");
                    if (datos[0] != "XX") {
                        $("#" + id).html(datos[1]);
                        $("#" + id).val(datos[0]).trigger("change");
                        resolve();
                    } else {
                        reject("Descripcíon ya existe como " + mensaje);
                    }

                } else {
                    reject(ValidarTraduccion('Debe de ingresar la descripción de ' + mensaje));
                }
            })
        }
    }).then(function (result) {
        $("#" + id).focus();
    })

}
   
$("#CerrarCotizacion").submit(function (e) {
    
    e.preventDefault();
    var parametros = $(this).serialize() + "&Cliente=" + IdCliente;
    var datos = LlamarAjax("Solicitud","opcion=GenerarSolicitud&" + parametros).split("|");
    if (datos[0] == "0") {
        LimpiarCotizacion();
        TablaCotizacion();
        $("#MDireccion").val("");
        $("#MContacto").val("");
        $("#MTelefono").val("");
        $("#MEDireccion").val("");
        $("#MEContacto").val("");
        $("#METelefono").val("");
        $("#MECorreo").val("");
        $("#MRDireccion").val("");
        $("#MRContacto").val("");
        $("#MRTelefono").val("");
        $("#MRCorreo").val("");
        $("#modalGenerarCotizacion").modal("hide");
        $("#Magnitud").focus();
        swal("", datos[1] + " " + datos[2], "success");
        ImprimirSolicitud(datos[2]);
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
    return false;
});

function CargarOIngreso() {
    if (IdCliente == 0)
        return false;
    if (Estado != "Temporal" && Estado != "Pendiente") {
        swal("Acción cancelada", "No se puede editar una solicitud en estado " + Estado, "warning");
        return false;
    }
    localStorage.setItem("idcliente", IdCliente);
    CargarModalIngreso(6,"sol");
    $("#modalIngresoSol").modal("show");
}

$("#tablamodalingresosol > tbody").on("dblclick", "tr", function (e) {

    var row = $(this).parents("td").context.cells;
    var numero = row[1].innerText;
    var parametros = "ingreso=" + numero + "&idcliente=" + IdCliente + "&remision=0&Solicitud=" + Solicitud;
    var datos = LlamarAjax("Solicitud","opcion=AgregarIngreso&" + parametros).split("|");
    if (datos[0] == "0") {
        TablaDetSolicitud();
        CargarOIngreso();
        swal("", datos[1], "success");
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
});

function ActivarRecogida(activo) {
    if (activo.checked) {
        $("#MRDireccion").prop("disabled", false);
        $("#MRContacto").prop("disabled", false);
        $("#MRTelefono").prop("disabled", false);
        $("#MRCorreo").prop("disabled", false);
    } else {
        $("#MRDireccion").prop("disabled", true);
        $("#MRContacto").prop("disabled", true);
        $("#MRTelefono").prop("disabled", true);
        $("#MRCorreo").prop("disabled", true);
    }
}

function ActivarEntrega(activo) {
    if (activo.checked) {
        $("#MEDireccion").prop("disabled", false);
        $("#MEContacto").prop("disabled", false);
        $("#METelefono").prop("disabled", false);
        $("#MECorreo").prop("disabled", false);
    } else {
        $("#MEDireccion").prop("disabled", true);
        $("#MEContacto").prop("disabled", true);
        $("#METelefono").prop("disabled", true);
        $("#MECorreo").prop("disabled", true);
    }
}

function AutoCompletar() {
    var datos = LlamarAjax("Solicitud","opcion=DatosClientesCompletar&cliente=" + IdCliente).split("|");

    AutoContacto.splice(0, AutoContacto.length);
    AutoDireccion.splice(0, AutoDireccion.length);
    AutoCorreo.splice(0, AutoCorreo.length);
    AutoTelefono.splice(0, AutoTelefono.length);
    var data = JSON.parse(datos[0]);
    for (var x = 0; x < data.length; x++) {
        AutoContacto.push(data[x].nombres);
    }
    var data1 = JSON.parse(datos[1]);
    for (var x = 0; x < data1.length; x++) {
        AutoDireccion.push(data1[x].direccion);
    }
    var data2 = JSON.parse(datos[2]);
    for (var x = 0; x < data2.length; x++) {
        AutoCorreo.push(data2[x].email);
    }
    var data3 = JSON.parse(datos[3]);
    for (var x = 0; x < data3.length; x++) {
        AutoTelefono.push(data3[x].telefono);
    }
}

$("#MRDireccion, #MEDireccion").autocomplete({ source: AutoDireccion, minLength: 0 }).focus(function () {
    $(this).data("uiAutocomplete").search($(this).val());
});

$("#MRContacto, #MEContacto").autocomplete({ source: AutoContacto, minLength: 0 }).focus(function () {
    $(this).data("uiAutocomplete").search($(this).val());
});

$("#MRTelefono, #METelefono").autocomplete({ source: AutoTelefono, minLength: 0 }).focus(function () {
    $(this).data("uiAutocomplete").search($(this).val());
});

$("#MRCorreo, #MECorreo").autocomplete({ source: AutoCorreo, minLength: 0 }).focus(function () {
    $(this).data("uiAutocomplete").search($(this).val());
});


function ImprimirSolicitud(solicitud) {
    if (solicitud == 0)
        solicitud = Solicitud;
    if (solicitud == 0)
        return false;
    var parametros = "solicitud=" + solicitud;
    ActivarLoad();
    setTimeout(function () {
        var datos = LlamarAjax("Configuracion", "opcion=GenerarPDF&tipo=Solicitud&documento=" + solicitud).split("||");
        DesactivarLoad();
        if (datos[0] == "0") {
            window.open(url_cliente + "DocumPDF/" + datos[1]);
        } else {
            swal("Acción Cancelada", datos[1], "warning");
        }
    }, 15);
}

function ConsularSolicitud() {


    var magnitud = $("#CMagnitud").val() * 1;
    var equipo = $("#CEquipo").val();
    var modelo = $("#CModelo").val();
    var marca = $("#CMarca").val() * 1;
    var intervalo = $("#CIntervalo").val();
    var cotizacion = $("#CCotizacion").val() * 1;
    var cliente = $("#CCliente").val() * 1;

    var solicitud = $("#CSolicitud").val()*1;
    var remision = $("#CRemision").val() * 1;
    var usuario = $("#CUsuario").val() * 1;
    var estado = $("#CEstado").val();

    var fechad = $("#CFechaDesde").val();
    var fechah = $("#CFechaHasta").val();
        
    if (fechad == "" && fechah == "") {
        swal("Acción Cancelada", ValidarTraduccion("debe de ingresar una fecha o números de días"), "warning");
        return false;
    }

    ActivarLoad();
    setTimeout(function () {
        var parametros = "remision=" + remision + "&cotizacion=" + cotizacion + "&cliente=" + cliente + "&solicitud=" + solicitud + "&estado=" + estado +
            "&fechad=" + fechad + "&fechah=" + fechah + "&magnitud=" + magnitud + "&equipo=" + equipo + "&marca=" + marca + "&modelo=" + modelo +
            "&intervalo=" + intervalo + "&usuario=" + usuario;
        var datos = LlamarAjax("Solicitud","opcion=ConsultarSolicitud&" + parametros);
        DesactivarLoad();

        var datajson = JSON.parse(datos);
        $('#TablaSolicitudes').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "opcion" },
                { "data": "solicitud", "className": "text-XX" },
                { "data": "fecha" },
                { "data": "estado" },
                { "data": "cliente" },
                { "data": "direccioncli" },
                { "data": "telefonos" },
                { "data": "email" },
                { "data": "observacion" },
                { "data": "cantidad" },
                { "data": "cotizado" },
                { "data": "asesor" },
                { "data": "anulado" }
            ],

            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy','colvis'
            ]
        });
    }, 15);
}

function BuscarSolicitud(numero) {
    if (Solicitud == numero)
        return false;
    LimpiarTodo();
    if ((numero * 1) == 0)
        return false;
    var parametros = "solicitud=" + numero;
    var datos = LlamarAjax("Solicitud","opcion=BuscarSolicitud&" + parametros);
    if (datos != "[]") {
        var data = JSON.parse(datos);
        IdSolicitud = data[0].id;
        Solicitud = data[0].solicitud;
        Estado = data[0].estado;
        $("#Cliente").val(data[0].documento);
        BuscarCliente(data[0].documento, 1);
        $("#Solicitud").val(Solicitud);
        $("#Estado").val(Estado);
        $("#Sede").val(data[0].idsede).trigger("change");
        $("#Contacto").val(data[0].idcontacto).trigger("change");
        $("#FechaReg").val(data[0].fecha);
        $("#UsuarioReg").val(data[0].usuario);
        $("#Observacion").val(data[0].observacion);

        if (data[0].fechaanulado) {
            Anulacion = "<span class='text-XX text-danger'>Solicitud Anulada el día: " + data[0].fechaanulado + ", por el <b>Asesor</b>: " + data[0].usuarioanula + ", <b>Observación:</b> " + data[0].observacionanula + "</span>";
            $("#divAnulaciones").html(Anulacion);
        }

        if ($.trim(data[0].rdireccion) == "")
            $("#CheRecoger").prop("checked", "").trigger("change");
        else {
            $("#CheRecoger").prop("checked", "checked").trigger("change");
            $("#MRDireccion").val(data[0].rdireccion);
            $("#MRContacto").val(data[0].rcontacto);
            $("#MRTelefono").val(data[0].rtelefonos);
            $("#MRCorreo").val(data[0].rcorreo);
        }
        if ($.trim(data[0].edireccion) == "")
            $("#CheEntregar").prop("checked", "").trigger("change");
        else {
            $("#CheEntregar").prop("checked", "checked").trigger("change");
            $("#MEDireccion").val(data[0].edireccion);
            $("#MEContacto").val(data[0].econtacto);
            $("#METelefono").val(data[0].etelefonos);
            $("#MECorreo").val(data[0].ecorreo);
        }
        
        
        
    } else {
        $("#Solicitud").focus();
        swal("Acción Cancelada", "Solicitud número " + numero + " no registrada", "warning");
    }
}


function AnularSolicitud() {

    if (Estado != "Temporal" && Estado != "Pendiente") {
        swal("Acción Cancelada", "No se puede anular una solicitud en estado " + Estado, "warning");
        return false;
    }

    swal({
        title: 'Advertencia',
        text: '¿Desea Anular la solicitud número ' + Solicitud + '?',
        input: 'text',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: 'Si',
        cancelButtonText: 'No',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',

        inputValidator: function (value) {
            return new Promise(function (resolve, reject) {
                if (value) {
                    var datos = LlamarAjax("Solicitud","opcion=AnularSolicitud&solicitud=" + Solicitud + "&observacion=" + value)
                    datos = datos.split("|");
                    if (datos[0] == "0") {
                        Estado = "Anulado";
                        $("#Estado").val(Estado);
                        resolve()
                    } else {
                        reject(datos[1]);
                    }

                } else {
                    reject('Debe de ingresar una observación');
                }
            })
        }
    }).then(function (result) {
        TablaListarCotizacion("");
        swal({
            type: 'success',
            html: 'Solicitud número ' + Solicitud + ' anulada con éxito'
        })
    })
}

function GuardarIntervalo() {
    var magnitud = $("#Magnitud").val() * 1;
    var medida = $("#IMedida option:selected").text();
    var desde = $.trim($("#IDesde").val());
    var hasta = $.trim($("#IHasta").val());
    
    var mensaje = "";

    if ($.trim($("#IHasta").val()) == "") {
        $("#IHasta").focus();
        mensaje = "<br> *Ingrese el rango hasta" + mensaje;
    }
    if ($.trim($("#IDesde").val()) == "") {
        $("#IDesde").focus();
        mensaje = "<br> *Ingrese el rango desde" + mensaje;
    }

    if (medida == "--Seleccione--") {
        $("#IMedida").focus();
        mensaje = "<br> *Seleccione una unidad de medida" + mensaje;
    }

    if (mensaje != "") {
        swal("Verifique los siguientes campos", mensaje, "warning");
        return false;
    }

    var parametros = "magnitud=" + magnitud + "&medida=" + medida + "&desde=" + desde + "&hasta=" + hasta;
    var datos = LlamarAjax("Cotizacion","opcion=GuardarIntervalo&" + parametros);
    if (datos * 1 > 0) {
        $("#Intervalo").html(CargarCombo(6, 1, "", magnitud));
        $("#Intervalo").val(datos).trigger("change");
        $("#modalIntervalo").modal("hide");
    } else {
        swal("Acción Cancelada", "Ya existe un rango de intervalo con estas descripciones", "warning");
    }
}

function VerEstadoCuenta() {
    var cliente = $("#NombreCliente").val();
    EstadoCuenta(IdCliente, cliente, CorreoEmpresa, TipoCliente, PlazoPago, CorreoEmpresa);
}

$("#TablaSolicitudes > tbody").on("dblclick", "tr", function () {
    var row = $(this).parents("td").context.cells;
    var numero = row[1].innerText;
    $('#tabsolicitudes a[href="#tabcrear"]').tab('show')
    BuscarSolicitud(numero);
});

function ActualizarIngresoSol() {
    if (Estado != "Temporal" && Estado != "Pendiente") {
        swal("Acción Cancelada", "No se puede actualizar un ingreso con la solicitud en estado " + Estado, "warning");
        return false;
    }

    var ingreso = $("#Ingreso").val() * 1;

    if (ingreso == 0) {
        swal("Acción cancelada", "Este item no posee un ingreso asociado", "warning");
        return false;
    }


    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: ValidarTraduccion('¿Seguro que desea actualizar el ingreso número ' + ingreso + '?'),
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Actualizar'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                $.post(url_servidor + "Solicitud","opcion=ActualizarIngresoSol&ingreso=" + ingreso + "&solicitud=" + Solicitud)
                    .done(function (data) {
                        datos = data.split("|");
                        if (datos[0] == "0") {
                            var data = JSON.parse(datos[2]);
                            $("#Magnitud").val(data[0].idmagnitud).trigger("change");
                            $("#Equipo").val(data[0].idequipo).trigger("change");
                            $("#Marca").val(data[0].idmarca).trigger("change");
                            $("#Modelo").val(data[0].idmodelo).trigger("change");
                            $("#Intervalo").val(data[0].idintervalo).trigger("change");
                            $("#Serie").val(data[0].serie);                            
                            resolve()
                        } else {
                            reject(datos[1]);
                        }
                    })
            })
        }
    }]);
}


$('select').select2();
DesactivarLoad();
