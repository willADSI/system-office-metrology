﻿d = new Date();
month = d.getMonth() + 1;
day = d.getDate();

var Anio = $.trim(d.getFullYear());
Anio = $.trim(Anio.substr(2, 2))


output = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-01';

output2 = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-' +
    (day < 10 ? '0' : '') + day;

$("#CFechaDesde").val(output);
$("#CFechaHasta").val(output2);

var IdAutorizacion = 0;
var OpcionEquipo = 0;

$("#Empleado, #CEmpleado").html(CargarCombo(29, 1, "", "-1"));
$("#Magnitud, #AreaCalibracion, #AreaMantenimiento, #CMagnitud, #CAreaCalibracion, #CAreaMantenimiento").html(CargarCombo(1, 1));

$("#Equipo, #CEquipo").html(CargarCombo(2, 1));
$("#CIntervalo").html(CargarCombo(6, 1, "", "-1"));
$("#CProcedimiento").html(CargarCombo(52, 1));
$("#CCargo").html(CargarCombo(26, 1));
$("#Pais").html(CargarCombo(18, 1));
$("#Pais").val(1).trigger("change");

tb = $('input');
$(tb).keypress(enter2tab);

tb = $('select');
$(tb).keypress(enter2tab);

function enter2tab(e) {
    if (e.keyCode == 13) {
        id = $(this).attr("id");

        cb = parseInt($(this).attr('tabindex'));

        if (id == "Documento") {
            if ($.trim($(this).val()) != "") {

                BuscarProveedor($(this).val());
                $("#Sede").focus();
                return false;
            }
        }

        if ($(':input[tabindex=\'' + (cb + 1) + '\']') != null) {
            $(':input[tabindex=\'' + (cb + 1) + '\']').focus();
            $(':input[tabindex=\'' + (cb + 1) + '\']').select();
            e.preventDefault();

            return false;
        }

        if ($(':select[tabindex=\'' + (cb + 1) + '\']') != null) {
            console.log("1");
            $(':select[tabindex=\'' + (cb + 1) + '\']').focus();
            $(':select[tabindex=\'' + (cb + 1) + '\']').select();
            e.preventDefault();

            return false;
        }
    }
}

$("#formAutorizacion").submit(function (e) {
    e.preventDefault();
    var parametros = $("#formAutorizacion").serialize().replace(/\%2C/g, "");
    var datos = LlamarAjax("Recursohumano","opcion=GuardarAutorizacion&"+ parametros).split("|");
    if (datos["0"] == "0") {
        if (IdAutorizacion == 0) {
            IdAutorizacion = datos[1];
            $("#IdAutorizacion").val(IdAutorizacion);
            $("#Usuario").val(datos[2]);
            $("#Fecha").val(datos[3]);
            $("#Estado").val("Autorizado");
            swal("", "Autorización guardada con éxito", "success");
        } else
            swal("", "Autorización actualizada con éxito", "success");
        return false;
    } else {
        swal("Acción Cancelada", datos[1], "warning");
        return false;
    }
});

function LimpiarTodo() {
    $("#IdAutorizacion").val("0");
    $("#Usuario").val("");
    $("#Fecha").val("");
    $("#FechaSus").val("");
    $("#Estado").val("Temporal");
    $("#Empleado").val("").trigger("change");
    $("#Magnitud").val("").trigger("change");
    $("#Procedimiento").val("").trigger("change");
    $("#Pais").val("1").trigger("change");
    $("#Ciudad").html("<option value''>--Seleccione--</option>");
}

function CargarCargo() {
    var texto = $("#Empleado option:selected").text();
    var arreglo = texto.split("|");
    $("#Cargo").val(arreglo[3]);
}

function CargarEquipos(magnitud) {
    $("#Intervalo").html(CargarCombo(6, 3, "", magnitud));
    $("#Procedimiento").html(CargarCombo(51, 1, "", magnitud));
    if (OpcionEquipo == 0)
        $("#Equipo").html(CargarCombo(2, 1, "", magnitud));
    $("#AreaCalibracion, #AreaMantenimiento").val(magnitud).trigger("change");

}

function MagnitudEquipo(equipo) {
    equipo = equipo * 1;
    var datos = LlamarAjax("Cotizacion","opcion=MagnitudEquipo&id=" + equipo);
    if (datos != "[]") {
        var data = JSON.parse(datos);
        OpcionEquipo = 1;
        if (data[0].idmagnitud * 1 != $("#Magnitud").val() * 1)
            $("#Magnitud").val(data[0].idmagnitud).trigger("change");
        OpcionEquipo = 0;
    }
}


$("#TablaAutorizaciones > tbody").on("dblclick", "tr", function () {
    var row = $(this).parents("td").context.cells;
    var id = row[1].innerText;
    var datos = LlamarAjax("Recursohumano","opcion=BuscarAutorizacion&id=" + id);
    var data = JSON.parse(datos);
    $("#IdAutorizacion").val(id);
    $("#Usuario").val(data[0].usuario);
    $("#Fecha").val(data[0].fecha);
    $("#FechaSus").val(data[0].fechasus);
    $("#Estado").val(data[0].estado);
    $("#Empleado").val(data[0].idempleado).trigger("change");
    $("#Cargo").val(data[0].cargo);
    $("#Equipo").val(data[0].idequipo).trigger("change");
    $("#Procedimiento").val(data[0].idprocedimiento).trigger("change");
    $("#Pais").val(data[0].idpais).trigger("change");
    $("#Departamento").val(data[0].iddepartamento).trigger("change");
    $("#Ciudad").val(data[0].idciudad).trigger("change");
    $("#AreaCalibracion").val(data[0].idarecalibracion).trigger("change");
    $("#AreaMantenimiento").val(data[0].idaremantenimiento).trigger("change");
    
    $('#tab-autorizacion a[href="#tabcrear"]').tab('show')
});


function ConsultarAutorizacion() {
    
    var empleado =$("#CEmpleado").val()*1;
    var cargo = $("#CCargo").val() * 1;
    var estado = $.trim($("#CEstado").val());
    var procedimiento = $("#CProcedimiento").val()*1;

    var magnitud = $("#CMagnitud").val() * 1;
    var equipo = $("#CEquipo").val() * 1;
    var intervalo = $("#CIntervalo").val() * 1;
    var areamant= $("#CAreaMantenimiento").val() * 1;
    var areacalib = $("#CAreaCalibracion").val() * 1;

    var fechad = $("#CFechaDesde").val();
    var fechah = $("#CFechaHasta").val();

    if (fechad == "" && fechah == "") {
        swal("Acción Cancelada", ValidarTraduccion("debe de ingresar la fecha de búsqueda"), "warning");
        return false;
    }
               
    ActivarLoad();
    setTimeout(function () {
        var parametros = "empleado=" + empleado + "&cargo=" + cargo + "&procedimiento=" + procedimiento + "&estado=" + estado + "&magnitud=" +
            magnitud + "&equipo=" + equipo + "&intervalo=" + intervalo + "&areamant=" + areamant + "&areacalib=" + areacalib + "&fechad=" + fechad + "&fechah=" + fechah;
        var datos = LlamarAjax("Recursohumano","opcion=ConsultarAutorizacion&"+parametros);
        DesactivarLoad();
        var datajson = JSON.parse(datos);
        $('#TablaAutorizaciones').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "imprimir" },
                { "data": "id" },
                { "data": "empleado" },
                { "data": "cargo" },
                { "data": "magnitud" },
                { "data": "equipo" },
                { "data": "intervalo" },
                { "data": "procedimiento" },
                { "data": "ciudad" },
                { "data": "areacalibracion" },
                { "data": "areamantenimiento" },
                { "data": "fecha" },
                { "data": "estado" },
                { "data": "usuario" },
                { "data": "actualizado" },
                { "data": "suspendido" },
                { "data": "observacionsus" }
            ],

            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy','colvis'
            ]
        });
    }, 15);
}

function CambioDepartamento(pais) {
    $("#Departamento").html(CargarCombo(11, 1, "", pais));
    $("#Ciudad").html("");
}

function CambioCiudad(departamento) {
    $("#Ciudad").html(CargarCombo(12, 1, "", departamento));
}

function Suspender() {
    var id = $("#IdAutorizacion").val() * 1;
    var estado = $("#Estado").val();

    var texto = $("#Empleado option:selected").text();
    var arreglo = texto.split("|");
    var empleado = arreglo[0];

    var equipo = $("#Equipo option:selected").text();

    if (id == 0) {
        swal("Acción Cancelada", "Debe de seleccionar una autorización registrada", "warning");
        return false;
    }
    if (estado != "Autorizado") {
        swal("Acción Cancelada", "Esta autorización ya fue suspendida", "warning");
        return false;
    }

    swal({
        title: 'Advertencia',
        text: '¿Desea suspender la autorización del empleado ' + empleado + ' de la magnitud y equipo ' + equipo + '?',
        input: 'text',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Suspender'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',

        inputValidator: function (value) {
            return new Promise(function (resolve, reject) {
                if (value) {
                    var datos = LlamarAjax("Recursohumano","opcion=SuspenderAutorizacion&id=" + id + "&observacion=" + value);
                    datos = datos.split("|");
                    if (datos[0] == "0") {
                        $("#Estado").val("Suspendido");
                        $("#FechaSus").val(datos[1]);
                        resolve()
                    } else {
                        reject(datos[1]);
                    }

                } else {
                    reject(ValidarTraduccion('Debe de ingresar el motivo de la suspención'));
                }
            })
        }
    }).then(function (result) {
        swal({
            type: 'success',
            html: 'Suspención realizada con éxito'
        })
    });
}

$('select').select2();
DesactivarLoad();


