﻿
$('select').select2();
DesactivarLoad();
$("#NumCertificado").html(CargarCombo(72, 0, "", "'DID-150'"));
var ErrorEnter = 0;
var numero;
var Ingreso;
var IdVersion = 9;
var datosParalelismo = [];
var horaIni, horaFin, horaTrans;
var NumCertificado = 13;

var isMarch = false;
var acumularTime = 0;

var plenitud = [0.32, (0.32 / 25.4)];
var plenExtError = [];  //todos los valores asignados para la tabla de Planitud Exteriores
var paralelismo = [];  // todos los valores asignados para la tabla de paralelismo
var paralInputs = [];
var tipoMedicion = [];
var errorPlanExt = [];
var errorAbbe = [];


var mandSerie = [];
var bloquesMandSerie = [];
var idbloquesMandSerie = [];
var datosMandExt = [];
var repPunt = [];
var datosPatron;
var datos;
var datosEnc;
var datosEnc2;
var derivaBloquesPatron = [];
var incertidumbreBloquesPatron = [];



var incertMandExte = [];

/****************/
var calLongitudExt = [];
/**************/




/**Nuevos metodos y variables***/
var dat_matrizPatrones = [];
var dat_matrizInfoCalib = [];
var dat_matrizInstrCalib = [];
var dat_matrizEMP = [];
var dat_intervalosAbbe = [];
var selectIntErrorAbbe = "<option value=''>--Seleccione--</option>";
var selectDAbbe = "<option value=''>--Seleccione--</option>";
var selectParalelaje = "<option value=''>--Seleccione--</option>";
var dat_matrizAbbe = [];
var dat_matrices = [];
var dat_matrices_incert = [];

var camposActualizar = []; // campos para el modal de actualiuzar el intrumento
var matrizIntrumentoIBC = []; // aqui los valores para la matriz de calibracion de IBC 
var informes = [];
var matrizIntervalos = [];
var matrizParalelismo = [];
var matruizPlanitudExt = [];
var matrizErrorAbbe = [];
var matrizParalaje = [];
var matrizTemp = [];
var matrizMed = [];
var matrizLineError = [];

var matrizErrorK = [];


var matrizPatrones = [];
var matrizPatronesInt = [];
var matrizPatronesSP = [];
var matrizPatronesCalP = [];

var matrizIncertidumbreMandExt = []; 
var matrizIncertidumbreMandInt = [];
var matrizIncertidumbreMandSP = [];
var matrizIncertidumbreMandCalP = [];
var matrizIncertidumbreMandExtra = [];

var matrizBloques = [];
var matrizBloquesInteriores = [];
var matrizBloquesProfundidad = [];
var matrizBloquesPaso = [];

var matrizPromMandExt = [];
var matrizPromMandInt = [];
var matrizPromMandSP = [];
var matrizPromMandCalP = [];

var datastuden = JSON.parse(LlamarAjax("Laboratorio/TStuden", ""));
/***Variables de respaldo boorrar una vez se tengan lo datos que son*/
var registroF14 = 1.07156;
/********************************/

$(document).ready(function () {
    //start();
    tipoMedicion["MDI"] = 1;
    matrizIncertidumbreMandExt['"5,0"'] = 0;
    matrizIncertidumbreMandInt['"5,0"'] = 0;
    matrizIncertidumbreMandSP['"5,0"'] = 0;
    matrizIncertidumbreMandCalP['"5,0"'] = 0;
    matrizIncertidumbreMandExtra['"5,0"'] = 0;
});

function LeerCondiciones() {
    var datos = LlamarAjax("Laboratorio/LeerArchivoTemperatura", "magnitud=2&fecha=2019-01-21&hora=15:00&ingreso=3450&numero=1&revision=1");
}

$.connection.hub.start().done(function () {
    $('#IngresoLong').keyup(function (event) {
        numero = this.value * 1;
        if (numero == -1)
            numero = Ingreso;
        if ((Ingreso != (numero * 1)) && ($("#Equipo").html() != "")) {
            GuardaTemp = 1;
            //LimpiarDatos(); habiliatr para limpiar los datos del los formularios
            Ingreso = 0;
            ErrorEnter = 0;
        }
        if (event.which == 13) {
            event.preventDefault();
            if (Ingreso == numero)
                return false;
            BuscarIngreso(numero);
        }
    });
});

function BuscarIngreso(numero) {
    //var revision = $("#Revision").val() * 1;
    //NumCertificado = $("#NumCertificado").val() * 1;//modifque esto LW
    if (ErrorEnter == 0) {
        if (numero * 1 > 0) {
            
            GuardaTemp = 0;
            datos = LlamarAjax("Laboratorio/BuscarIngresoOperacionNueva", "magnitud=5&ingreso=" + numero + "&medida=mm&revision=" + 1 + "&version=" + IdVersion).split("|");            
            console.log(datos);
            datosEnc = JSON.parse(datos[0]);
            datosEnc2 = JSON.parse(datos[2]);
            datosPatron = JSON.parse(datos[7]);

            dat_matrizInstrCalib = JSON.parse(datos[0]);
            dat_matrizInfoCalib = JSON.parse(datos[2]);
            dat_matrizPatrones = JSON.parse(datos[7]);
            dat_matrizEMP = JSON.parse(datos[8]);
            dat_intervalosAbbe = JSON.parse(datos[9]);
            dat_matrizAbbe = JSON.parse(datos[10]);
            dat_matrices = JSON.parse(datos[11]);
            dat_matrices_incert = JSON.parse(datos[12]);            

            habilitarPuntos(['', 3], 'puntos');
            console.log("--dat_matrizInstrCalib", dat_matrizInstrCalib);
            console.log("-- dat_matrizInfoCalib", dat_matrizInfoCalib);
            console.log("--dat_matrizPatrones", dat_matrizPatrones);
            console.log("--dat_matrizEMP", dat_matrizEMP);
            console.log("--dat_matrizAbbe", dat_matrizAbbe);
            console.log("--dat_intervalosAbbe", dat_intervalosAbbe);
            console.log("--dat_matrices", dat_matrices);

            /********************Primer encabezado*****************************************/
            $('#Remision').val(dat_matrizInstrCalib[0].remision);
            $('#Usuario').val(dat_matrizInstrCalib[0].usuarioing);
            /******************************************************************************/
            /*****************INFORMACIÓN DE LA CALIBRACIÓN********************************/
            $('#FechaIng').html(dat_matrizInstrCalib[0].fecharec);
            $('#Direccion').html(dat_matrizInstrCalib[0].direccion);
            $('#FechaCal').html(new Date((dat_matrizInfoCalib[0].fecha).replace('T', ' ')).toLocaleDateString());
            $('#TIngreso').html(dat_matrizInstrCalib[0].ingreso);
            $('#medida, #medida1, #medida2, #medida3, #uniCalInst').html(datosEnc[0].medida);

            /******************************************************************************/ 
            /****************Cargar Intervalos matriz abbe*****************************************************************/
            var contMatrizMandExt = 0, contMatrizMandInt = 0, contMatrizMandSP = 0, contMatrizMandP = 0;
            var selectActualizar = '<option value="null">Seleccione</option>'
            for (var i = 0; i < dat_matrizAbbe.length; i++) {
                if (dat_matrizAbbe[i].tpo_calculo == null){
                    selectDAbbe += '<option value="' + dat_matrizAbbe[i].id + '">' + dat_matrizAbbe[i].rango_intervalo + '</option>';
                }
                if (dat_matrizAbbe[i].tpo_calculo == 'INTERVALO MAXIMO') {
                    selectIntErrorAbbe += '<option value="' + dat_matrizAbbe[i].id + '">' + dat_matrizAbbe[i].rango_intervalo + '</option>';
                }
                if (dat_matrizAbbe[i].tpo_calculo == 'ERROR DE PARALELAJE') {
                    selectParalelaje += '<option value="' + formato_numero(dat_matrizAbbe[i].rango_intervalo, 3, ".", ",", "") + '">' + formato_numero(dat_matrizAbbe[i].rango_intervalo, 3, ".", ",", "") + '</option>';
                }
                if (dat_matrizAbbe[i].tpo_calculo == 'LONGITUD NOMINAL MANDIBULAS EXTERORES') {                   
                    matrizBloques['"' + 0 + ',' + contMatrizMandExt + '"'] = (dat_matrizAbbe[i].g / dat_matrizAbbe[i].h <= 1 ? dat_matrizAbbe[i].g / dat_matrizAbbe[i].h * 100 : dat_matrizAbbe[i].g / dat_matrizAbbe[i].h);
                    matrizBloques['"' + 1 + ',' + contMatrizMandExt + '"'] = formato_numero(((matrizBloques['"' + 0 + ',' + contMatrizMandExt + '"']) * (Number(dat_matrizInstrCalib[0].desde) - Number(dat_matrizInstrCalib[0].hasta))) / 100, 3, ".", ",", "");
                    $("#inLongN0" + contMatrizMandExt).val(formato_numero(matrizBloques['"' + 0 + ',' + contMatrizMandExt + '"'], 0, ".", ",", "") + ' %');
                    $("#longNomi1" + contMatrizMandExt).val(formato_numero(Math.abs(matrizBloques['"' + 1 + ',' + contMatrizMandExt + '"']), 3, ".", ",", ""));
                    contMatrizMandExt++;
                }

                if (dat_matrizAbbe[i].tpo_calculo == 'LONGITUD NOMINAL MANDIBULAS INTERIORES') {
                    matrizBloquesInteriores['"' + 0 + ',' + contMatrizMandInt + '"'] = ((dat_matrizAbbe[i].g / dat_matrizAbbe[i].h <= 1) ? dat_matrizAbbe[i].g / dat_matrizAbbe[i].h * 100 : dat_matrizAbbe[i].g / dat_matrizAbbe[i].h);
                    matrizBloquesInteriores['"' + 1 + ',' + contMatrizMandInt + '"'] = formato_numero(((matrizBloquesInteriores['"' + 0 + ',' + contMatrizMandInt + '"']) * (Number(dat_matrizInstrCalib[0].desde) - Number(dat_matrizInstrCalib[0].hasta))) / 100, 3, ".", ",", "");
                    $("#porcInt0" + contMatrizMandInt).val(formato_numero(matrizBloquesInteriores['"' + 0 + ',' + contMatrizMandInt + '"'], 0, ".", ",", "") + ' %');
                    if (Number(matrizBloquesInteriores['"' + 1 + ',' + 0 + '"']) == 0) {
                        matrizBloquesInteriores['"' + 1 + ',' + 0 + '"'] = Number(20);
                    }
                    $("#porcInt1" + contMatrizMandInt).val(formato_numero(Math.abs(matrizBloquesInteriores['"' + 1 + ',' + contMatrizMandInt + '"']), 3, ".", ",", ""));
                    contMatrizMandInt++;
                }

                if (dat_matrizAbbe[i].tpo_calculo == 'LONGITUD NOMINAL PROFUNDIDAD') {
                    matrizBloquesProfundidad['"' + 0 + ',' + contMatrizMandSP + '"'] = (dat_matrizAbbe[i].g / dat_matrizAbbe[i].h <= 1 ? dat_matrizAbbe[i].g / dat_matrizAbbe[i].h * 100 : dat_matrizAbbe[i].g / dat_matrizAbbe[i].h);
                    matrizBloquesProfundidad['"' + 1 + ',' + contMatrizMandSP + '"'] = formato_numero(((matrizBloquesProfundidad['"' + 0 + ',' + contMatrizMandSP + '"']) * (Number(dat_matrizInstrCalib[0].desde) - Number(dat_matrizInstrCalib[0].hasta))) / 100, 3, ".", ",", "");
                    $("#inLongSP0" + contMatrizMandSP).val(formato_numero(matrizBloquesProfundidad['"' + 0 + ',' + contMatrizMandSP + '"'], 0, ".", ",", "") + ' %');
                    $("#inLongSP1" + contMatrizMandSP).val(formato_numero(Math.abs(matrizBloquesProfundidad['"' + 1 + ',' + contMatrizMandSP + '"']), 3, ".", ",", ""));
                    contMatrizMandSP++;
                }

                if (dat_matrizAbbe[i].tpo_calculo == 'LONGITUD NOMINAL PASO') {
                    matrizBloquesPaso['"' + 0 + ',' + contMatrizMandP + '"'] = (dat_matrizAbbe[i].g / dat_matrizAbbe[i].h <= 1 ? dat_matrizAbbe[i].g / dat_matrizAbbe[i].h * 100 : dat_matrizAbbe[i].g / dat_matrizAbbe[i].h);
                    matrizBloquesPaso['"' + 1 + ',' + contMatrizMandP + '"'] = formato_numero(((matrizBloquesPaso['"' + 0 + ',' + contMatrizMandP + '"']) * (Number(dat_matrizInstrCalib[0].desde) - Number(dat_matrizInstrCalib[0].hasta))) / 100, 3, ".", ",", "");
                    $("#inLongCalP0" + contMatrizMandP).val(formato_numero(matrizBloquesPaso['"' + 0 + ',' + contMatrizMandP + '"'], 0, ".", ",", "") + ' %');
                    $("#inLongCalP1" + contMatrizMandP).val(formato_numero(Math.abs(matrizBloquesPaso['"' + 1 + ',' + contMatrizMandP + '"']), 3, ".", ",", ""));
                    contMatrizMandP++;
                }

                if (dat_matrizAbbe[i].tpo_calculo == 'INCERTIDUMBRE TIPICA RESOLUCCION') {
                    matrizIncertidumbreMandExt['"0,1"'] = dat_matrizAbbe[i].h;
                }
                if (dat_matrizAbbe[i].tpo_calculo == 'INCERTIDUMBRE TIPICA REPETIBILIDAD') {
                    matrizIncertidumbreMandExt['"1,1"'] = dat_matrizAbbe[i].h;
                }
                if (dat_matrizAbbe[i].tpo_calculo == 'INCERTIDUMBRE TIPICA ERROR DE PARALELAJE') {
                    matrizIncertidumbreMandExt['"2,1"'] = dat_matrizAbbe[i].h;
                }
                if (dat_matrizAbbe[i].tpo_calculo == 'INCERTIDUMBRE TIPICA ERROR ABBE') {
                    matrizIncertidumbreMandExt['"3,1"'] = dat_matrizAbbe[i].h;
                }
                if (dat_matrizAbbe[i].tpo_calculo == 'INCERTIDUMBRE TIPICA RESOLUCCION ERROR DE LINEALIDAD') {
                    matrizIncertidumbreMandExt['"4,1"'] = dat_matrizAbbe[i].h;
                }
                if (dat_matrizAbbe[i].tpo_calculo == 'INCERTIDUMBRE TIPICA RESOLUCCION INCERTIDUMBRE PATRON') {
                    matrizIncertidumbreMandExt['"5,1"'] = dat_matrizAbbe[i].h;
                }


                if (dat_matrizAbbe[i].tpo_calculo == 'INCERTIDUMBRE TIPICA RESOLUCCION INTERIORES') {
                    matrizIncertidumbreMandInt['"0,1"'] = dat_matrizAbbe[i].h;
                }

                if (dat_matrizAbbe[i].tpo_calculo == 'INCERTIDUMBRE TIPICA REPETIBILIDAD INTERIORES') {
                    matrizIncertidumbreMandInt['"1,1"'] = dat_matrizAbbe[i].h;
                }

                if (dat_matrizAbbe[i].tpo_calculo == 'INCERTIDUMBRE TIPICA ERROR DE PARALELAJE INTERIORES') {
                    matrizIncertidumbreMandInt['"2,1"'] = dat_matrizAbbe[i].h;
                }

                if (dat_matrizAbbe[i].tpo_calculo == 'INCERTIDUMBRE TIPICA ERROR ABBE INTERIORES') {
                    matrizIncertidumbreMandInt['"3,1"'] = dat_matrizAbbe[i].h;
                }

                if (dat_matrizAbbe[i].tpo_calculo == 'INCERTIDUMBRE TIPICA RESOLUCCION ERROR DE LINEALIDAD INTERIORES') {
                    matrizIncertidumbreMandInt['"4,1"'] = dat_matrizAbbe[i].h;
                }

                if (dat_matrizAbbe[i].tpo_calculo == 'INCERTIDUMBRE TIPICA RESOLUCCION INCERTIDUMBRE PATRON INTERIORES') {
                    matrizIncertidumbreMandInt['"5,1"'] = dat_matrizAbbe[i].h;
                }

                if (dat_matrizAbbe[i].tpo_calculo == 'INCERTIDUMBRE TIPICA RESOLUCCION PROFUNDIDAD') {
                    matrizIncertidumbreMandSP['"0,1"'] = dat_matrizAbbe[i].h;
                    matrizIncertidumbreMandCalP['"0,1"'] = dat_matrizAbbe[i].h;
                    matrizIncertidumbreMandExtra['"0,1"'] = dat_matrizAbbe[i].h;
                }

                if (dat_matrizAbbe[i].tpo_calculo == 'INCERTIDUMBRE TIPICA REPETIBILIDAD PROFUNDIDAD') {
                    matrizIncertidumbreMandSP['"1,1"'] = dat_matrizAbbe[i].h;
                    matrizIncertidumbreMandCalP['"1,1"'] = dat_matrizAbbe[i].h;
                    matrizIncertidumbreMandExtra['"1,1"'] = dat_matrizAbbe[i].h;
                }

                if (dat_matrizAbbe[i].tpo_calculo == 'INCERTIDUMBRE TIPICA ERROR DE PARALELAJE PROFUNDIDAD') {
                    matrizIncertidumbreMandSP['"2,1"'] = dat_matrizAbbe[i].h;
                    matrizIncertidumbreMandCalP['"2,1"'] = dat_matrizAbbe[i].h;
                    matrizIncertidumbreMandExtra['"2,1"'] = dat_matrizAbbe[i].h;
                }

                if (dat_matrizAbbe[i].tpo_calculo == 'INCERTIDUMBRE TIPICA RESOLUCCION INCERTIDUMBRE PATRON PROFUNDIDAD') {
                    matrizIncertidumbreMandSP['"5,1"'] = dat_matrizAbbe[i].h;
                    matrizIncertidumbreMandCalP['"5,1"'] = dat_matrizAbbe[i].h;
                    matrizIncertidumbreMandExtra['"5,1"'] = dat_matrizAbbe[i].h;
                }

                if (dat_matrizAbbe[i].tpo_calculo == 'DIVISION ESCALA') {
                    if (Number(dat_matrizAbbe[i].h) == Number(dat_matrizInstrCalib[0].divisionescala)) {
                        selectActualizar += '<option selected value="' + dat_matrizAbbe[i].id + '">' + dat_matrizAbbe[i].h + '</option>';
                        camposActualizar['DS'] = Number(dat_matrizAbbe[i].h);
                    } else {
                        selectActualizar += '<option value="' + dat_matrizAbbe[i].id + '">' + dat_matrizAbbe[i].h + '</option>';
                    }                    
                }
            }           
            /*Modal Campos*/
            camposActualizar['TPO'] = Number(dat_matrizInstrCalib[0].tipo);
            camposActualizar['RSLN'] = Number(dat_matrizInstrCalib[0].resolucion);
            camposActualizar['UC'] = String(datosEnc[0].medida);
            camposActualizar['IDSD'] = Number(dat_matrizInstrCalib[0].desde);
            camposActualizar['IHST'] = Number(dat_matrizInstrCalib[0].hasta);
            $("#tpoInst").html('<option value="null">Seleccione</option><option ' + (Number(camposActualizar['TPO']) == 0 ? 'selected' : '') + ' value="0">Digital</option><option ' + (Number(camposActualizar['TPO']) == 1 ? 'selected' : '') + ' value="1">Analógico</option>');
            $("#UnidC").html('<option value="null">Seleccione</option><option ' + (camposActualizar['UC'].toLowerCase() == 'mm' ? 'selected' : '') + ' value="mm">mm</option><option ' + (camposActualizar['UC'].toLowerCase() == 'in' ? 'selected' : '') + ' value="in">in</option>');
            $("#divEscala").html(selectActualizar);
            $("#resolAct").val(camposActualizar['RSLN']);
            $("#intervD").val(camposActualizar['IDSD']);
            $("#intervH").val(camposActualizar['IHST']);
            /*****************DATOS DEL INSTRUMENTO BAJO CALIBRACIÓN (IBC)*****************/
            matrizIntrumentoIBC['"0,0"'] = dat_matrizInstrCalib[0].equipo;
            matrizIntrumentoIBC['"1,0"'] = dat_matrizInstrCalib[0].marca;//fabricante
            matrizIntrumentoIBC['"2,0"'] = dat_matrizInstrCalib[0].modelo;
            matrizIntrumentoIBC['"3,0"'] = dat_matrizInstrCalib[0].serie;
            matrizIntrumentoIBC['"4,0"'] = dat_matrizInstrCalib[0].tipo == 0 ? 'Digital' : 'Analógico';//falta setear el tipo
            $('#Equipo').html(matrizIntrumentoIBC['"0,0"']);
            $('#Marca').html(matrizIntrumentoIBC['"1,0"']);
            $('#Modelo').html(matrizIntrumentoIBC['"2,0"']);
            $('#Serie').html(matrizIntrumentoIBC['"3,0"']);
            $('#tpoInstCal').html(matrizIntrumentoIBC['"4,0"']);
            /*Rango de medida divido en 2*/
            matrizIntrumentoIBC['"0,1"'] = Number(dat_matrizInstrCalib[0].desde);
            matrizIntrumentoIBC['"0,2"'] = Number(dat_matrizInstrCalib[0].hasta);
            $('#RangoMedida').html(matrizIntrumentoIBC['"0,1"'] + ' ' + camposActualizar['UC']);
            $('#RangoMedidaHST').html(matrizIntrumentoIBC['"0,2"'] + ' ' + camposActualizar['UC']);
            /*Division de escala normal y conversion*/
            matrizIntrumentoIBC['"1,1"'] = Number(dat_matrizInstrCalib[0].divisionescala);
            matrizIntrumentoIBC['"1,2"'] = (camposActualizar['UC'] == 'mm' ? Number(dat_matrizInstrCalib[0].divisionescala) : Number(dat_matrizInstrCalib[0].divisionescala) * 25.4);
            $('#DivEscala').html(matrizIntrumentoIBC['"1,1"'] + ' ' + camposActualizar['UC']);
            $('#DivEscalaConv').html(matrizIntrumentoIBC['"1,2"'] + ' ' + 'mm');            
            /*Resolucion y conersion*/
            matrizIntrumentoIBC['"2,1"'] = dat_matrizInstrCalib[0].resolucion;
            matrizIntrumentoIBC['"2,2"'] = (camposActualizar['UC'] == 'mm' ? Number(dat_matrizInstrCalib[0].resolucion) : Number(dat_matrizInstrCalib[0].resolucion) * 25.4);
            $('#Resolucion').html(matrizIntrumentoIBC['"2,1"'] + ' ' + camposActualizar['UC']);
            $('#ResolucionConv').html(matrizIntrumentoIBC['"2,2"'] + ' ' + 'mm');
            /*Error maximo permitido positivo y negativo*/
            matrizIntrumentoIBC['"3,1"'] = (Number(dat_matrizInstrCalib[0].desde));
            matrizIntrumentoIBC['"3,2"'] = (Number(dat_matrizInstrCalib[0].hasta));
            $('#ErrroMaxP').html((camposActualizar['UC'] == 'mm' ? Number(matrizIntrumentoIBC['"3,1"']) : Number(matrizIntrumentoIBC['"3,1"']) * 25.4) + ' ' + camposActualizar['UC']);
            $('#ErrroMaxN').html((camposActualizar['UC'] == 'mm' ? Number(matrizIntrumentoIBC['"3,2"']) : Number(matrizIntrumentoIBC['"3,2"']) * 25.4) + ' ' + camposActualizar['UC']);
            /******************Cargar los intervalos**************************************************/
            matrizIntervalos['"' + 0 + ',' + 0 + '"'] = Number(matrizIntrumentoIBC['"0,1"']);
            matrizIntervalos['"' + 0 + ',' + 1 + '"'] = dat_matrizInstrCalib[0].medida;
            matrizIntervalos['"' + 0 + ',' + 2 + '"'] = Number(matrizIntrumentoIBC['"0,1"']);
            matrizIntervalos['"' + 0 + ',' + 3 + '"'] = dat_matrizInstrCalib[0].medida;

            matrizIntervalos['"' + 1 + ',' + 0 + '"'] = Number(matrizIntrumentoIBC['"0,2"']);
            matrizIntervalos['"' + 1 + ',' + 1 + '"'] = dat_matrizInstrCalib[0].medida;
            matrizIntervalos['"' + 1 + ',' + 2 + '"'] = Number(matrizIntrumentoIBC['"0,2"']);
            matrizIntervalos['"' + 1 + ',' + 3 + '"'] = dat_matrizInstrCalib[0].medida;
            cargarMatriz(["intMin", "intMax"], [0, 1], [0, 1, 2, 3], matrizIntervalos, 0);
            /**********************************************************************************************************/
            /*********************************************************************************************************/
            $("#errAbbe00").html(selectIntErrorAbbe);
            $("#errAbbe10").html("<option value=''>--Seleccione--</option><option value='M'>M</option><option value='cm'>CM</option>");
            $("#errAbbe03").html(selectDAbbe);
            $("#errPar03").html(selectParalelaje);
            //cargarMatriz(["inLongN"], [0], [0, 1, 2, 3, 4, 5, 6, 7, 8, 9], matrizBloques, 1);
            //cargarMatriz(["longNomi"], ['',1], [0, 1, 2, 3, 4, 5, 6, 7, 8, 9], matrizBloques, 0);
            /**********************************************************************************************************/
            /****************Cargar patrones de medida*****************************************************************/
            if (dat_matrizPatrones.length > 0) {
                var opcion = "<option value='No' title='No' selected>No</option>";
                for (var i = 0; i < dat_matrizPatrones.length; i++) {
                    opcion += "<option value='" + dat_matrizPatrones[i].id + "' title='" + dat_matrizPatrones[i].marca + " - " + dat_matrizPatrones[i].serie + "' " + (dat_matrizPatrones[i].estado * 1 == 1 ? '' : "disabled") + ">" + dat_matrizPatrones[i].valornominal + "</option>";
                }
                $(".patron").html(opcion);
            }  
            /*********************************************************************************************************/
            /****************Cargar datos de temperaturas*****************************************************************/
            matrizTemp['"' + 0 + ',' + 1 + '"'] = (11.5 * Math.pow(10, -6));
            matrizTemp['"' + 1 + ',' + 1 + '"'] = (11.5 * Math.pow(10, -6));

            matrizTemp['"' + 0 + ',' + 4 + '"'] = (matrizTemp['"' + 0 + ',' + 1 + '"'] * 0.1);
            matrizTemp['"' + 1 + ',' + 4 + '"'] = (matrizTemp['"' + 1 + ',' + 1 + '"'] * 0.1);

            cargarMatriz(["temp"], [0, 1], ['',1,'','', 4], matrizTemp, 1);
            /*********************************************************************************************************/
            /****************Cargar datos Error de la linea de contacto  *********************************************/
            for (var i = 0; i < 13; i++) {
                var valor = 0;
                var enc = ''
                if (i < 10) {
                    enc = i+1;
                    valor = 20;
                    /*Error k Matriz de extras*/
                    matrizErrorK['"' + 0 + ',' + i + '"'] = enc;
                    matrizErrorK['"' + 1 + ',' + i + '"'] = valor;
                }
                if (i == 10) {
                    enc = 'MAX';
                    valor = 20;
                    /*Error k Matriz de extras*/
                    matrizErrorK['"' + 0 + ',' + i + '"'] = "Promedio";
                    matrizErrorK['"' + 1 + ',' + i + '"'] = 20;
                }
                if (i == 11) {
                    enc = 'MIN';
                    valor = 20;
                    /*Error k Matriz de extras*/
                    matrizErrorK['"' + 0 + ',' + i + '"'] = "Desvest";
                    matrizErrorK['"' + 1 + ',' + i + '"'] = 0;
                }
                if (i == 12) {
                    enc = 'DIFERENCIA';
                    valor = 0;
                    /*Error k Matriz de extras*/
                    matrizErrorK['"' + 0 + ',' + i + '"'] = "Error";
                    matrizErrorK['"' + 1 + ',' + i + '"'] = 20;      

                    matrizErrorK['"' + 0 + ',' + 12 + '"'] = "urep";
                    matrizErrorK['"' + 1 + ',' + 12 + '"'] = Number(matrizErrorK['"' + 1 + ',' + 11 + '"']) / Math.sqrt(9);
                    $("#medEXTRA0" + (i + 1)).val(matrizErrorK['"' + 0 + ',' + 12 + '"']);
                    $("#medEXTRA1" + (i + 1)).val(formato_numero(matrizErrorK['"' + 1 + ',' + 12 + '"'], 2, ".", ",", ""));
                }
                /*Para exteriores*/
                matrizLineError['"' + 0 + ',' + i + '"'] = enc;
                matrizLineError['"' + 1 + ',' + i + '"'] = valor;
                $("#errLin0" + i).val(matrizLineError['"' + 0 + ',' + i + '"']);
                $("#errLin1" + i).val(formato_numero(matrizLineError['"' + 1 + ',' + i + '"'], 2, ".", ",", ""));
                /*Para Error de paralelismo*/
                matrizLineError['"' + 2 + ',' + i + '"'] = enc;
                matrizLineError['"' + 3 + ',' + i + '"'] = valor;
                $("#errParInt0" + i + "#errParInt0" + i).val(matrizLineError['"' + 2 + ',' + i + '"']);
                $("#errParInt1" + i + "#errParInt1" + i).val(formato_numero(matrizLineError['"' + 3 + ',' + i + '"'], 2, ".", ",", ""));    
                /*Para MEDICIÓN DE  Lc & NO*/
                matrizLineError['"' + 4 + ',' + i + '"'] = (i > 9 ? 'PROMEDIO' : enc);
                matrizLineError['"' + 5 + ',' + i + '"'] = 0; 
                $("#medLC0" + i).val(matrizLineError['"' + 4 + ',' + i + '"']);
                $("#medLC1" + i).val(formato_numero(matrizLineError['"' + 5 + ',' + i + '"'], 2, ".", ",", ""));
                /*Error k Matriz de extras*/
                $("#medEXTRA0" + i).val(matrizErrorK['"' + 0 + ',' + i + '"']);
                $("#medEXTRA1" + i).val(formato_numero(matrizErrorK['"' + 1 + ',' + i + '"'], 2, ".", ",", ""));
            }            
            /*********************************************************************************************************/
            /****************Cargar datos Calibración de las mandíbulas de de medición de interiores******************/
                        
            /*********************************************************************************************************/
            $('#Tiempo').val(dat_matrizInstrCalib[0].tiempo);            
           // $('#medidaCond1, #medidaCond2, #medidaCond3, #medidaCond4, #medidaCond5, #medidaCond6, #medidaCond7, #medidaCond8').html((dat_matrizInstrCalib[0].medida == "mm" ? 'µm' : 'mil'));            
            $('#medidaCondPlen1, #medidaCondPlen2').html((dat_matrizInstrCalib[0].medida == "mm" ? plenitud[0] : formato_numero(plenitud[1], 2, ".", ",", "")));
            //$('#paralCond1, #paralCond2, #paralCond3, #paralCond4, #paralCond5, #paralCond6, #paralCond7, #paralCond8').html((dat_matrizInstrCalib[0].medida == "mm" ? 'µm' : 'mil'));
            $("#errorAbbeRes").html(Number(matrizIntrumentoIBC['"2,1"']));
            $("#errorAbbeResUM, #errorAbbeLonBocUM").html(dat_matrizInstrCalib[0].medida);                     
            /********************************************************************************************************/
        }
    }
    
    llenarIncentidumbreExt();
    tablaIncertidumbreInterior();
    tablaIncertidumbreSP();
    tablaIncertidumbreSP();
    if (dat_matrices == [] || dat_matrices == '[]' || dat_matrices.length == 0) {
        BuscarLocal();
    } else {
        cargarCerificado();
    }
}

function cargarMatriz(id, columnas, filas, values, tipo) {
    if (columnas.length > 0 && filas.length > 0) {
        for (var i = 0; i < columnas.length; i++) {
            for (var j = 0; j < filas.length; j++) {
                if (tipo == 0) {
                    if (id.length > 1) {
                        $("#" + id[i] + columnas[i] + filas[j]).html(values['"' + i + ',' + j + '"']);
                    } else {
                        $("#" + id + columnas[i] + filas[j]).html(values['"' + i + ',' + j + '"']);
                    }                        
                } else {
                    if (id.length > 1) {
                        $("#" + id[i] + columnas[i] + filas[j]).val(values['"' + i + ',' + j + '"']);
                    } else {
                        $("#" + id + columnas[i] + filas[j]).val(values['"' + i + ',' + j + '"']);
                    }  
                }
            }
        }
    }

}

function cargarErrorAbbe() {
    try {
        if (dat_matrices == [] || dat_matrices == '[]' || dat_matrices.length == 0) {
            errorAbbe['"' + 0 + ',' + 0 + '"'] = $("#errAbbe00").val();
            errorAbbe['"' + 1 + ',' + 0 + '"'] = $("#errAbbe10").val();
            errorAbbe['"' + 0 + ',' + 3 + '"'] = $("#errAbbe03").val();
            errorAbbe['"' + 0 + ',' + 6 + '"'] = $("#errAbbe06").val();
            if (errorAbbe['"' + 0 + ',' + 3 + '"'] !== undefined && String(errorAbbe['"' + 0 + ',' + 3 + '"']) !== 'NaN') {
                for (var i = 0; i < dat_matrizAbbe.length; i++) {
                    if (dat_matrizAbbe[i].id == errorAbbe['"' + 0 + ',' + 3 + '"']) {
                        var calc = 0, calc2 = 0;
                        calc = (dat_matrizAbbe[i].it_7 + dat_matrizAbbe[i].h);
                        calc2 = (dat_matrizAbbe[i].g - dat_matrizAbbe[i].it_6);
                        errorAbbe['"' + 1 + ',' + 3 + '"'] = (calc - calc2);
                    }

                    if (dat_matrizAbbe[i].id == errorAbbe['"' + 0 + ',' + 0 + '"']) {
                        errorAbbe['"' + 2 + ',' + 0 + '"'] = ((errorAbbe['"' + 1 + ',' + 0 + '"']).toLowerCase() === 'cm' ? dat_matrizAbbe[i].h : dat_matrizAbbe[i].g);
                    }
                }
                if (errorAbbe['"' + 0 + ',' + 6 + '"'] !== undefined && String(errorAbbe['"' + 0 + ',' + 6 + '"']) !== 'NaN') {
                    errorAbbe['"' + 1 + ',' + 6 + '"'] = (errorAbbe['"' + 2 + ',' + 0 + '"'] * errorAbbe['"' + 1 + ',' + 3 + '"']) / errorAbbe['"' + 0 + ',' + 6 + '"'];
                }
                $("#errAbbe13").html(formato_numero(errorAbbe['"' + 1 + ',' + 3 + '"'], 3, ".", ",", ""));
                $("#errAbbe20").html(errorAbbe['"' + 2 + ',' + 0 + '"']);
                $("#errAbbe16").html(formato_numero(errorAbbe['"' + 1 + ',' + 6 + '"'], 4, ".", ",", ""));
            }
            llenarIncentidumbreExt();
        }
    } catch (error) {
        console.error("Ha ocurrido un error [cargarErrorAbbe]--> ", error);
    }
}  

function calcularErrorParalaje(value, elemento) {
    try {    
        if (dat_matrices == [] || dat_matrices == '[]' || dat_matrices.length == 0) {
            var acum = 0;
            if (value !== undefined && String(value) !== 'NaN' && value !== '') {
                matrizParalaje['"' + elemento[0] + ',' + elemento[1] + '"'] = (value);
                matrizParalaje['"' + 2 + ',' + 0 + '"'] = 0;
                if (elemento[2] == 'select') {
                    for (var i = 0; i < dat_matrizAbbe.length; i++) {
                        if (dat_matrizAbbe[i].id == value) {
                            matrizParalaje['"' + elemento[0] + ',' + elemento[1] + '"'] = dat_matrizAbbe[i].rango_intervalo;
                        }
                    }
                }
                if (matrizParalaje['"' + 0 + ',' + 0 + '"'] !== undefined && String(matrizParalaje['"' + 0 + ',' + 0 + '"']) !== 'NaN' && matrizParalaje['"' + 0 + ',' + 3 + '"'] !== undefined && String(matrizParalaje['"' + 0 + ',' + 3 + '"']) !== 'NaN' && matrizParalaje['"' + 1 + ',' + 3 + '"'] !== undefined && String(matrizParalaje['"' + 1 + ',' + 3 + '"']) !== 'NaN') {
                    matrizParalaje['"' + 2 + ',' + 1 + '"'] = (matrizParalaje['"' + 0 + ',' + 0 + '"'] * matrizParalaje['"' + 0 + ',' + 3 + '"'] * matrizParalaje['"' + 1 + ',' + 3 + '"']);
                }
                if (matrizParalaje['"' + 1 + ',' + 0 + '"'] !== undefined && String(matrizParalaje['"' + 1 + ',' + 0 + '"']) !== 'NaN' && matrizParalaje['"' + 1 + ',' + 5 + '"'] !== undefined && String(matrizParalaje['"' + 1 + ',' + 5 + '"']) !== 'NaN' && matrizParalaje['"' + 0 + ',' + 5 + '"'] !== undefined && String(matrizParalaje['"' + 0 + ',' + 5 + '"']) !== 'NaN') {
                    matrizParalaje['"' + 2 + ',' + 1 + '"'] = matrizParalaje['"' + 2 + ',' + 1 + '"'] / (matrizParalaje['"' + 1 + ',' + 0 + '"'] * (matrizParalaje['"' + 1 + ',' + 5 + '"']) / (matrizParalaje['"' + 0 + ',' + 5 + '"']));
                }
                $("#errPar20").html(formato_numero(matrizParalaje['"' + 2 + ',' + 1 + '"'], 3, ".", ",", ""));
                llenarIncentidumbreExt();
            }
            activarCalibracion();
        }
    } catch (error) {
        console.error("Ha ocurrido un error [calcularErrorParalaje]--> ", error);
    }
}

function calcularTemperatura(value, elemento) {    
    if (dat_matrices == [] || dat_matrices == '[]' || dat_matrices.length == 0) {
        matrizTemp['"' + elemento[0] + ',' + elemento[1] + '"'] = (value);
        matrizTemp['"' + elemento[0] + ',' + 4 + '"'] = (matrizTemp['"' + elemento[0] + ',' + elemento[1] + '"'] * 0.1);
        $("#temp" + elemento[0] + '' + 4).val(matrizTemp['"' + elemento[0] + ',' + 4 + '"']);
        activarCalibracion();
    }
}

function matrisLinError(value, elemento, matriz) { 
    try {
        if (dat_matrices == [] || dat_matrices == '[]' || dat_matrices.length == 0) {
            mayor = 0;
            if (matriz == undefined) {
                matrizLineError['"' + 1 + ',' + elemento[1] + '"'] = value;
                for (var i = 0; i < 10; i++) {
                    if (matrizLineError['"' + 1 + ',' + 10 + '"'] < matrizLineError['"' + 1 + ',' + i + '"']) {
                        matrizLineError['"' + 1 + ',' + 10 + '"'] = matrizLineError['"' + 1 + ',' + i + '"'];
                    }
                    if (matrizLineError['"' + 1 + ',' + 11 + '"'] > matrizLineError['"' + 1 + ',' + i + '"']) {
                        matrizLineError['"' + 1 + ',' + 11 + '"'] = matrizLineError['"' + 1 + ',' + i + '"'];
                    }
                    matrizLineError['"' + 1 + ',' + 12 + '"'] = matrizLineError['"' + 1 + ',' + 10 + '"'] - matrizLineError['"' + 1 + ',' + 11 + '"'];
                }
                $("#errLin110").val(formato_numero(matrizLineError['"' + 1 + ',' + 10 + '"'], 2, ".", ",", ""));
                $("#errLin111").val(formato_numero(matrizLineError['"' + 1 + ',' + 11 + '"'], 2, ".", ",", ""));
                $("#errLin112").val(formato_numero(matrizLineError['"' + 1 + ',' + 12 + '"'], 2, ".", ",", ""));
            }

            if (matriz == 'EP') {
                matrizLineError['"' + 3 + ',' + elemento[1] + '"'] = value;
                for (var i = 0; i < 10; i++) {
                    if (matrizLineError['"' + 3 + ',' + 10 + '"'] < matrizLineError['"' + 3 + ',' + i + '"']) {
                        matrizLineError['"' + 3 + ',' + 10 + '"'] = matrizLineError['"' + 3 + ',' + i + '"'];
                    }
                    if (matrizLineError['"' + 3 + ',' + 11 + '"'] > matrizLineError['"' + 3 + ',' + i + '"']) {
                        matrizLineError['"' + 3 + ',' + 11 + '"'] = matrizLineError['"' + 3 + ',' + i + '"'];
                    }
                    matrizLineError['"' + 3 + ',' + 12 + '"'] = Number(matrizLineError['"' + 3 + ',' + 10 + '"']) - Number(matrizLineError['"' + 3 + ',' + 11 + '"']);
                }
                $("#errPar110").val(formato_numero(matrizLineError['"' + 3 + ',' + 10 + '"'], 2, ".", ",", ""));
                $("#errPar111").val(formato_numero(matrizLineError['"' + 3 + ',' + 11 + '"'], 2, ".", ",", ""));
                $("#errPar112").val(formato_numero(matrizLineError['"' + 3 + ',' + 12 + '"'], 2, ".", ",", ""));
            }
            if (matriz == 'MLC') {
                matrizLineError['"' + 5 + ',' + elemento[1] + '"'] = value;
                matrizLineError['"' + 5 + ',' + 10 + '"'] = 0;
                var cont = 0;
                for (var i = 0; i < 10; i++) {
                    if (matrizLineError['"' + 5 + ',' + i + '"'] !== undefined && String(matrizLineError['"' + 5 + ',' + i + '"']) !== 'NaN') {
                        matrizLineError['"' + 5 + ',' + 10 + '"'] += Number(matrizLineError['"' + 5 + ',' + i + '"']);
                        cont++;
                    }
                }
                matrizLineError['"' + 5 + ',' + 10 + '"'] = Number(matrizLineError['"' + 5 + ',' + 10 + '"']) / (cont);
                $("#medLC110").val(formato_numero(matrizLineError['"' + 5 + ',' + 10 + '"'], 2, ".", ",", ""));
            }
            activarCalibracion();
        }
    } catch (error) {
        console.error("Ha ocurrido un error [matrisLinError]--> ", error);
    }
}

function habilitarPuntos(value, elementos) {
    try {
            var campo;
            var fila;
            if (value[1] !== '') {
                campo = (String(value[1]).startsWith("#") ? value[1] : "#" + value[1]);
                fila = (String(value[1]).substring(value[1].length - 1, value[1].length)) * 1;
            }
            if (value[1] == '') {
                fila = value[1] * 1;
            }
            for (var i = 0; i < 10; i++) {
                for (var j = 0; j < 10; j++) {
                    /*Repetibilidad*/
                    if (elementos == 'repetibilidad' && value[1].includes('ckeckPuntosExt')) {
                        if (value[0] == true && j == fila && i < 8) {
                            //si el check de repetibilidad esta seleccionado
                            $("#lecturaIBC" + (i + 3) + "" + j).val("").addClass('bg-amarillo').prop('readonly', false);
                        }
                        if (value[0] == false && j == fila && i < 8) {
                            //si el check de repetibilidad no esta seleccionado
                            $("#lecturaIBC" + (i + 3) + "" + j).val("").removeClass('bg-amarillo').prop('readonly', true);
                        }
                    }

                    if (elementos == 'repetibilidad' && value[1].includes('ckeckPuntosInt')) {
                        if (value[0] == true && j == fila && i < 8) {
                            //si el check de repetibilidad esta seleccionado
                            $("#lectInt" + (i + 3) + "" + j).val("").addClass('bg-amarillo').prop('readonly', false);
                        }
                        if (value[0] == false && j == fila && i < 8) {
                            //si el check de repetibilidad no esta seleccionado
                            $("#lectInt" + (i + 3) + "" + j).val("").removeClass('bg-amarillo').prop('readonly', true);
                        }
                    }

                    if (elementos == 'repetibilidad' && value[1].includes('ckeckPuntosSP')) {
                        if (value[0] == true && j == fila && i < 8) {
                            //si el check de repetibilidad esta seleccionado
                            $("#lectSP" + (i + 3) + "" + j).val("").addClass('bg-amarillo').prop('readonly', false);
                        }
                        if (value[0] == false && j == fila && i < 8) {
                            //si el check de repetibilidad no esta seleccionado
                            $("#lectSP" + (i + 3) + "" + j).val("").removeClass('bg-amarillo').prop('readonly', true);
                        }
                    }
                    if (elementos == 'repetibilidad' && value[1].includes('ckeckPuntosCalP')) {
                        if (value[0] == true && j == fila && i < 8) {
                            //si el check de repetibilidad esta seleccionado
                            $("#lectCalP" + (i + 3) + "" + j).val("").addClass('bg-amarillo').prop('readonly', false);
                        }
                        if (value[0] == false && j == fila && i < 8) {
                            //si el check de repetibilidad no esta seleccionado
                            $("#lectCalP" + (i + 3) + "" + j).val("").removeClass('bg-amarillo').prop('readonly', true);
                        }
                    }
                    /*Puntos*/
                    if (elementos == 'puntos') {
                        campo = "#ckeckPuntos" + j;
                        if ($(campo).prop('checked') !== true) {
                            if (i < fila && i < 10) {
                                $("#lecturaIBC" + i + "" + j + ", #lectInt" + i + "" + j + ", #lectSP" + i + "" + j + ", #lectCalP" + i + "" + j).val("").addClass('bg-amarillo').prop('readonly', false);
                            } else if (i < 10) {
                                $("#lecturaIBC" + i + "" + j + ", #lectInt" + i + "" + j + ", #lectSP" + i + "" + j + ", #lectCalP" + i + "" + j).val("").removeClass('bg-amarillo').prop('readonly', true);
                            }
                        }
                    }
                }
            }
            activarCalibracion();
       
    } catch (error) {
        console.error("Ha ocurrido un error [habilitarPuntos]--> ", error);
    }
}

function calcularPiesMandExt(value, elemento, select) {
    try {
        if (dat_matrices == [] || dat_matrices == '[]' || dat_matrices.length == 0) {
            var cont = 0, acum = 0, acumLP = 0, acumU = [], acumDer = [];
            var arregloDesvi = [];
            var ErrorPromedio = 0, ciclo = 0;
            if (select == 3) {
                matrizBloques['"' + 0 + ',' + elemento[1] + '"'] = (Number(value));
                matrizBloques['"' + 1 + ',' + elemento[1] + '"'] = (Number(matrizBloques['"' + 0 + ',' + elemento[1] + '"']) * (Number(matrizIntrumentoIBC['"3,1"']) - Number(matrizIntrumentoIBC['"3,2"']))) / 100;
                $("#longNomi1" + elemento[1]).val(formato_numero(Math.abs(matrizBloques['"' + 1 + ',' + elemento[1] + '"']), 3, ".", ",", ""));
            }
            if (select == 4) {
                matrizBloques['"' + 1 + ',' + elemento[1] + '"'] = (Number(value));
                matrizBloques['"' + 0 + ',' + elemento[1] + '"'] = (Number(matrizBloques['"' + 1 + ',' + elemento[1] + '"']) / (Number(matrizIntrumentoIBC['"0,1"']) - Number(matrizIntrumentoIBC['"0,2"']))) * 100;
                $("#inLongN0" + elemento[1]).val(formato_numero(Math.abs(matrizBloques['"' + 0 + ',' + elemento[1] + '"']), 0, ".", ",", "") + " %");
            }
            if (select == undefined) {
                matrizPromMandExt['"' + elemento[0] + ',' + elemento[1] + '"'] = value;
                for (var i = 0; i < 10; i++) {
                    if (matrizPromMandExt['"' + i + ',' + elemento[1] + '"'] !== undefined && String(matrizPromMandExt['"' + i + ',' + elemento[1] + '"']) !== 'NaN') {
                        cont++;
                        acum += (matrizPromMandExt['"' + i + ',' + elemento[1] + '"'] * 1);
                        arregloDesvi.push((matrizPromMandExt['"' + i + ',' + elemento[1] + '"'] * 1));
                    }
                }
                matrizPromMandExt['"' + 10 + ',' + elemento[1] + '"'] = (acum == 0 && cont == 0 ? 0 : acum / cont);
                matrizPromMandExt['"' + 11 + ',' + elemento[1] + '"'] = (String(desviacionStandar(arregloDesvi)) == 'NaN' || desviacionStandar(arregloDesvi) == undefined ? 0 : desviacionStandar(arregloDesvi));
                matrizPromMandExt['"' + 12 + ',' + elemento[1] + '"'] = (matrizPromMandExt['"' + 11 + ',' + elemento[1] + '"'] / Math.sqrt(cont));
                matrizPromMandExt['"' + 13 + ',' + elemento[1] + '"'] = cont;
                $("#promedio" + elemento[1]).html(formato_numero(matrizPromMandExt['"' + 10 + ',' + elemento[1] + '"'], 4, ".", ",", ""));
                $("#desvStand" + elemento[1]).html(formato_numero(matrizPromMandExt['"' + 11 + ',' + elemento[1] + '"'], 4, ".", ",", ""));
                $("#repet" + elemento[1]).html(formato_numero(matrizPromMandExt['"' + 12 + ',' + elemento[1] + '"'], 4, ".", ",", ""));
                $("#nroLect" + elemento[1]).html(formato_numero(matrizPromMandExt['"' + 13 + ',' + elemento[1] + '"'], 2, ".", ",", ""));
            }
            if (select !== undefined && select == 1 && value !== 'No') {
                var existe = false, columna = 0;
                for (var i = 0; i < 6; i++) {
                    if (value == matrizPatrones['"' + i + ',' + elemento[1] + '"']) {
                        columna = i;
                        existe = true;
                        break;
                    }
                }
                if (existe == false) {
                    matrizPatrones['"' + elemento[0] + ',' + elemento[1] + '"'] = value;
                    for (var j = 0; j < 6; j++) {
                        if (matrizPatrones['"' + j + ',' + elemento[1] + '"'] !== undefined && String(matrizPatrones['"' + j + ',' + elemento[1] + '"']) !== 'NaN') {
                            for (var i = 0; i < dat_matrizPatrones.length; i++) {
                                if ((matrizPatrones['"' + j + ',' + elemento[1] + '"'] * 1) == dat_matrizPatrones[i].id) {
                                    acumLP += Math.abs(dat_matrizPatrones[i].valornominal);
                                    acum += dat_matrizPatrones[i].desviacionlongitud;
                                    acumU.push(dat_matrizPatrones[i].upatron / 2);
                                    acumDer.push(dat_matrizPatrones[i].derivada / dat_matrizPatrones[i].denominadorderivada);
                                }
                            }
                        }
                    }
                    if (acumLP > Math.abs(formato_numero(Number(matrizBloques['"' + 1 + ',' + elemento[1] + '"']), 3, ".", ",", ""))) {
                        matrizPatrones['"' + elemento[0] + ',' + elemento[1] + '"'] = undefined;
                        $('#bloque' + elemento[0] + '' + elemento[1]).val('No');
                        $('#bloque' + elemento[0] + '' + elemento[1]).change();
                        swal("Acción Cancelada", "El bloque que intenta seleccionar supera la longitud nominal, por favor seleccione otro", "warning");
                    } else {
                        matrizPatrones['"' + 6 + ',' + elemento[1] + '"'] = acumLP;
                        matrizPatrones['"' + 7 + ',' + elemento[1] + '"'] = acum;
                        matrizPatrones['"' + 8 + ',' + elemento[1] + '"'] = ((matrizPatrones['"' + 6 + ',' + elemento[1] + '"']) + (matrizPatrones['"' + 7 + ',' + elemento[1] + '"'] / 1000));
                        $("#longPatr" + elemento[1]).html(formato_numero(matrizPatrones['"' + 6 + ',' + elemento[1] + '"'], 4, ".", ",", ""));
                        /*Calcular valores de fondo para los bloques*/
                        if (acumU.length == 1) {
                            matrizPatrones['"' + 9 + ',' + elemento[1] + '"'] = acumU[0];
                        } else {
                            var acumu = 0;
                            for (var i = 0; i < acumU.length; i++) {
                                acumu += Math.pow(Number(acumU[i]), 2);
                            }
                            matrizPatrones['"' + 9 + ',' + elemento[1] + '"'] = Math.sqrt(acumu);
                        }

                        if (acumDer.length == 1) {
                            matrizPatrones['"' + 10 + ',' + elemento[1] + '"'] = acumDer[0];
                        } else {
                            var acumu = 0;
                            for (var i = 0; i < acumDer.length; i++) {
                                acumu += Math.pow(Number(acumDer[i]), 2);
                            }
                            matrizPatrones['"' + 10 + ',' + elemento[1] + '"'] = Math.sqrt(acumu);
                        }
                        matrizPatrones['"' + 11 + ',' + elemento[1] + '"'] = Math.pow(matrizPatrones['"' + 9 + ',' + elemento[1] + '"'], 2) + Math.pow(matrizPatrones['"' + 10 + ',' + elemento[1] + '"'], 2);
                        matrizPatrones['"' + 11 + ',' + elemento[1] + '"'] = Math.sqrt(matrizPatrones['"' + 11 + ',' + elemento[1] + '"']);
                        if (Number(matrizPatrones['"' + 11 + ',' + elemento[1] + '"']) > Number(matrizIncertidumbreMandExt['"5,0"'])) {
                            matrizIncertidumbreMandExt['"5,0"'] = (matrizPatrones['"' + 11 + ',' + elemento[1] + '"']);
                        }
                        /*Calcular valores de fondo para los bloques*/
                    }
                } else {
                    $('#bloque' + elemento[0] + '' + elemento[1]).val('No');
                    $('#bloque' + elemento[0] + '' + elemento[1]).change();
                    swal("Acción Cancelada", "El bloque que intenta seleccionar ya lo ha utilizado anteriormente en la columna " + columna + ", por favor seleccione otro bloque para continuar", "warning");
                }
            }
            if (matrizPromMandExt['"' + 10 + ',' + elemento[1] + '"'] !== undefined && String(matrizPromMandExt['"' + 10 + ',' + elemento[1] + '"']) !== 'NaN' && matrizPatrones['"' + 8 + ',' + elemento[1] + '"'] !== undefined && String(matrizPatrones['"' + 8 + ',' + elemento[1] + '"']) !== 'NaN') {
                matrizPromMandExt['"' + 14 + ',' + elemento[1] + '"'] = matrizPromMandExt['"' + 10 + ',' + elemento[1] + '"'] - matrizPatrones['"' + 8 + ',' + elemento[1] + '"'];
                $("#error" + elemento[1]).html(formato_numero(matrizPromMandExt['"' + 14 + ',' + elemento[1] + '"'], 4, ".", ",", ""));
            }
            llenarIncentidumbreExt();
            activarCalibracion();
        }
    } catch (error) {
        console.error("Ha ocurrido un error [calcularPiesMandExt]--> ", error);
    }
}

function llenarIncentidumbreExt() {
    if (dat_matrices == [] || dat_matrices == '[]' || dat_matrices.length == 0) {
        //Resolucion
        matrizIncertidumbreMandExt['"0,0"'] = (Number(matrizIntrumentoIBC['"2,2"']) * 1) / (Math.sqrt(12));
        matrizIncertidumbreMandExt['"0,3"'] = (Number(matrizIncertidumbreMandExt['"0,0"']) * Number(matrizIncertidumbreMandExt['"0,1"']));
        $("#resolIncExt").val(formato_numero(matrizIncertidumbreMandExt['"0,0"'], 4, ".", ",", ""));
        $("#resolIncExtCoef").val(formato_numero(matrizIncertidumbreMandExt['"0,1"'], 4, ".", ",", ""));
        $("#resolIncExtContri").val(formato_numero(matrizIncertidumbreMandExt['"0,3"'], 4, ".", ",", ""));
        //Repetibilidad    
        if (matrizIncertidumbreMandExt['"1,0"'] !== undefined && String(matrizIncertidumbreMandExt['"1,0"']) !== 'NaN') {
            matrizIncertidumbreMandExt['"1,3"'] = Number(matrizIncertidumbreMandExt['"1,1"']) * Number(matrizIncertidumbreMandExt['"1,0"']);
        } else {
            matrizIncertidumbreMandExt['"1,0"'] = 0;
            matrizIncertidumbreMandExt['"1,3"'] = Number(matrizIncertidumbreMandExt['"1,1"']) * Number(matrizIncertidumbreMandExt['"1,0"']);
        }
        $("#repetIncExtCoef").val(formato_numero(matrizIncertidumbreMandExt['"1,1"'], 4, ".", ",", ""));
        $("#repetIncExtContri").val(formato_numero(matrizIncertidumbreMandExt['"1,3"'], 4, ".", ",", ""));
        //Error de paralaje
        matrizIncertidumbreMandExt['"2,0"'] = (dat_matrizInstrCalib[0].tipo == 0 ? 0 : (Number(matrizParalaje['"' + 2 + ',' + 1 + '"']) / (Math.sqrt(3))));
        matrizIncertidumbreMandExt['"2,3"'] = (Number(matrizIncertidumbreMandExt['"2,0"']) * Number(matrizIncertidumbreMandExt['"2,1"']));
        $("#errParaIncExt").val(formato_numero(matrizIncertidumbreMandExt['"2,0"'], 4, ".", ",", ""));
        $("#errParaIncExtCoef").val(formato_numero(matrizIncertidumbreMandExt['"2,1"'], 4, ".", ",", ""));
        $("#errParaIncExtContri").val(formato_numero(matrizIncertidumbreMandExt['"2,3"'], 4, ".", ",", ""));
        //Error Abbe
        matrizIncertidumbreMandExt['"3,0"'] = (Number(errorAbbe['"' + 1 + ',' + 6 + '"']) / (Math.sqrt(12)));
        matrizIncertidumbreMandExt['"3,3"'] = (Number(matrizIncertidumbreMandExt['"3,0"']) * Number(matrizIncertidumbreMandExt['"3,1"']));
        $("#errAbbExt").val(formato_numero(matrizIncertidumbreMandExt['"3,0"'], 4, ".", ",", ""));
        $("#errAbbExtCoef").val(formato_numero(matrizIncertidumbreMandExt['"3,1"'], 4, ".", ",", ""));
        $("#errAbbExtContri").val(formato_numero(matrizIncertidumbreMandExt['"3,3"'], 4, ".", ",", ""));
        //Error de linealidad
        matrizIncertidumbreMandExt['"4,0"'] = (Number(matrizLineError['"' + 1 + ',' + 12 + '"']) / (Math.sqrt(12)));
        matrizIncertidumbreMandExt['"4,3"'] = (Number(matrizIncertidumbreMandExt['"4,0"']) * Number(matrizIncertidumbreMandExt['"4,1"']));
        $("#errLinExt").val(formato_numero(matrizIncertidumbreMandExt['"4,0"'], 4, ".", ",", ""));
        $("#errLinExtCoef").val(formato_numero(matrizIncertidumbreMandExt['"4,1"'], 4, ".", ",", ""));
        $("#errLinExtContri").val(formato_numero(matrizIncertidumbreMandExt['"4,3"'], 4, ".", ",", ""));
        //Incertidumbre Patrón    
        matrizIncertidumbreMandExt['"5,3"'] = ((Number(matrizIncertidumbreMandExt['"5,0"']) / 1000) * Number(matrizIncertidumbreMandExt['"5,1"']));
        $("#incertPatrExt").val(formato_numero((Number(matrizIncertidumbreMandExt['"5,0"']) / 1000), 4, ".", ",", ""));
        $("#incertPatrExtCoef").val(formato_numero(matrizIncertidumbreMandExt['"5,1"'], 4, ".", ",", ""));
        $("#incertPatrExtContri").val(formato_numero(matrizIncertidumbreMandExt['"5,3"'], 4, ".", ",", ""));
        //Coeficiente de dilatación
        var acum = 0, acumu = 0;
        acumu = Math.pow((Number(matrizTemp['"' + 0 + ',' + 4 + '"'])) / (Math.sqrt(3)), 2);
        acum = Math.pow(Number(matrizTemp['"' + 1 + ',' + 4 + '"']) / (Math.sqrt(3)), 2);
        matrizIncertidumbreMandExt['"6,0"'] = Math.sqrt(acum + acumu);
        matrizIncertidumbreMandExt['"6,1"'] = Number(matrizIntrumentoIBC['"3,2"']) * registroF14;
        matrizIncertidumbreMandExt['"6,3"'] = (Number(matrizIncertidumbreMandExt['"6,0"']) * Number(matrizIncertidumbreMandExt['"6,1"']));
        $("#coeDilExt").val(formato_numero(Number(matrizIncertidumbreMandExt['"6,0"']), 4, ".", ",", ""));
        $("#coeDilExtCoef").val(formato_numero(Number(matrizIncertidumbreMandExt['"6,1"']), 4, ".", ",", ""));
        $("#coeDilExtContri").val(formato_numero(Number(matrizIncertidumbreMandExt['"6,3"']), 4, ".", ",", ""));
        //Diferencia de temperatura
        matrizIncertidumbreMandExt['"7,0"'] = registroF14;
        matrizIncertidumbreMandExt['"7,1"'] = (Number(matrizIntrumentoIBC['"3,2"']) * (Number(matrizTemp['"' + 0 + ',' + 1 + '"']) > Number(matrizTemp['"' + 1 + ',' + 1 + '"']) ? Number(matrizTemp['"' + 0 + ',' + 1 + '"']) : Number(matrizTemp['"' + 1 + ',' + 1 + '"'])));
        matrizIncertidumbreMandExt['"7,3"'] = (Number(matrizIncertidumbreMandExt['"7,0"']) * Number(matrizIncertidumbreMandExt['"7,1"']));
        $("#difTempExt").val(formato_numero(Number(matrizIncertidumbreMandExt['"7,0"']), 4, ".", ",", ""));
        $("#difTempExtCoef").val(formato_numero(Number(matrizIncertidumbreMandExt['"7,1"']), 4, ".", ",", ""));
        $("#difTempExtContri").val(formato_numero(Number(matrizIncertidumbreMandExt['"7,3"']), 4, ".", ",", ""));
        //Incertidumbre combinada
        matrizIncertidumbreMandExt['"8,0"'] = (
            Math.pow(Number(matrizIncertidumbreMandExt['"0,3"']), 2) +
            Math.pow(Number(matrizIncertidumbreMandExt['"1,3"']), 2) +
            Math.pow(Number(matrizIncertidumbreMandExt['"2,3"']), 2) +
            Math.pow(Number(matrizIncertidumbreMandExt['"3,3"']), 2) +
            Math.pow(Number(matrizIncertidumbreMandExt['"4,3"']), 2) +
            Math.pow(Number(matrizIncertidumbreMandExt['"5,3"']), 2) +
            Math.pow(Number(matrizIncertidumbreMandExt['"6,3"']), 2));
        matrizIncertidumbreMandExt['"8,0"'] = Math.sqrt(Number(matrizIncertidumbreMandExt['"8,0"']));
        $("#incertCombExt").val(formato_numero(Number(matrizIncertidumbreMandExt['"8,0"']), 4, ".", ",", ""));
        //Grados efectivos de libertad
        matrizIncertidumbreMandExt['"9,0"'] = (Math.pow(Number(matrizIncertidumbreMandExt['"8,0"']), 4)) / (
            (Math.pow(Number(matrizIncertidumbreMandExt['"0,3"']), 4) / 200) +
            (Math.pow(Number(matrizIncertidumbreMandExt['"1,3"']), 4) / 9) +
            (Math.pow(Number(matrizIncertidumbreMandExt['"2,3"']), 4) / 200) +
            (Math.pow(Number(matrizIncertidumbreMandExt['"3,3"']), 4) / 200) +
            (Math.pow(Number(matrizIncertidumbreMandExt['"4,3"']), 4) / 200) +
            (Math.pow(Number(matrizIncertidumbreMandExt['"5,3"']), 4) / 200) +
            (Math.pow(Number(matrizIncertidumbreMandExt['"6,3"']), 4) / 200) +
            (Math.pow(Number(matrizIncertidumbreMandExt['"7,3"']), 4) / 200));
        $("#graEfecExt").val(formato_numero(Number(matrizIncertidumbreMandExt['"9,0"']), 4, ".", ",", ""));
        //Factor de cobertura
        matrizIncertidumbreMandExt['"10,0"'] = Calcular_TStudent(Number(matrizIncertidumbreMandExt['"9,0"']));
        $("#factCoberExt").val(formato_numero(Number(matrizIncertidumbreMandExt['"10,0"']), 4, ".", ",", ""));
        //U exp
        matrizIncertidumbreMandExt['"11,0"'] = (Number(matrizIncertidumbreMandExt['"8,0"'])) * (Number(matrizIncertidumbreMandExt['"10,0"']));
        $("#uExpExt").val(formato_numero(Number(matrizIncertidumbreMandExt['"11,0"']), 4, ".", ",", ""));
    }
}

function calcularRepetibilidad(value, campo) {
    try {
        if (dat_matrices == [] || dat_matrices == '[]' || dat_matrices.length == 0) {
            if (campo == undefined) {
                if (value !== undefined && String(value) !== 'NaN') {
                    matrizIncertidumbreMandExt['"1,0"'] = value;
                    matrizIncertidumbreMandExt['"1,3"'] = Number(matrizIncertidumbreMandExt['"1,1"']) * Number(value);
                    $("#repetIncExtContri").val(formato_numero(matrizIncertidumbreMandExt['"1,3"'], 4, ".", ",", ""));
                }
                llenarIncentidumbreExt();
            }
            if (campo == 'INT') {
                if (value !== undefined && String(value) !== 'NaN') {
                    matrizIncertidumbreMandInt['"1,0"'] = value;
                    matrizIncertidumbreMandInt['"1,3"'] = Number(matrizIncertidumbreMandInt['"1,1"']) * Number(value);
                    $("#repetIncSPContri").val(formato_numero(matrizIncertidumbreMandInt['"1,3"'], 4, ".", ",", ""));
                }
                tablaIncertidumbreInterior();
            }
            if (campo == 'SP') {
                if (value !== undefined && String(value) !== 'NaN') {
                    matrizIncertidumbreMandSP['"1,0"'] = value;
                    matrizIncertidumbreMandSP['"1,3"'] = Number(matrizIncertidumbreMandSP['"1,1"']) * Number(value);
                    $("#repetIncSPContri").val(formato_numero(matrizIncertidumbreMandSP['"1,3"'], 4, ".", ",", ""));
                }
                tablaIncertidumbreSP();
            }
            if (campo == 'CP') {
                if (value !== undefined && String(value) !== 'NaN') {
                    matrizIncertidumbreMandCalP['"1,0"'] = matrizErrorK['"' + 1 + ',' + 12 + '"'];
                    matrizIncertidumbreMandCalP['"1,3"'] = Number(matrizIncertidumbreMandCalP['"1,1"']) * Number(value);
                    $("#repetIncSPContri").val(formato_numero(matrizIncertidumbreMandCalP['"1,3"'], 4, ".", ",", ""));
                }
                tablaIncertidumbreCalP();
            }
            activarCalibracion();
        }
    } catch (error) {
        console.error("Ha ocurrido un error [calcularRepetibilidad]--> ", error);
    }
}

function Calcular_TStudent(grado) {
    valor = $.trim(grado).split(".");
    grado = valor[0] * 1;
    var factor = 0;
    if (grado <= 29)
        factor = datastuden[(grado - 1)].por45;
    else {
        if (grado >= 30 && grado < 35)
            factor = datastuden[29].por45;
        if (grado >= 35 && grado < 40)
            factor = datastuden[30].por45;
        if (grado >= 40 && grado < 45)
            factor = datastuden[31].por45;
        if (grado >= 45 && grado < 50)
            factor = datastuden[32].por45;
        if (grado >= 50 && grado < 60)
            factor = datastuden[33].por45;
        if (grado >= 60 && grado < 80)
            factor = datastuden[34].por45;
        if (grado >= 80 && grado < 100)
            factor = datastuden[35].por45;
        if (grado >= 100 && grado < 159)
            factor = datastuden[36].por45;
        if (grado >= 159 && grado < 473)
            factor = datastuden[37].por45;
        if (grado >= 473)
            factor = datastuden[38].por5;
    }
    return factor;
}

/*******Calculos para madibulas interiores******/
function capturarPatrones(value, elemento, select) {
    try {
        if (dat_matrices == [] || dat_matrices == '[]' || dat_matrices.length == 0) {
            var cont = 0, acum = 0, acumLP = 0, acumU = [], acumDer = [];
            var arregloDesvi = [];
            var ErrorPromedio = 0, ciclo = 0;

            if (select == 3) {
                matrizBloquesInteriores['"' + 0 + ',' + elemento[1] + '"'] = (Number(value));
                matrizBloquesInteriores['"' + 1 + ',' + elemento[1] + '"'] = (Number(matrizBloquesInteriores['"' + 0 + ',' + elemento[1] + '"']) * (Number(matrizIntrumentoIBC['"3,1"']) - Number(matrizIntrumentoIBC['"3,2"']))) / 100;
                $("#porcInt1" + elemento[1]).val(formato_numero(Math.abs(matrizBloquesInteriores['"' + 1 + ',' + elemento[1] + '"']), 3, ".", ",", ""));
            }
            if (select == 4) {
                matrizBloquesInteriores['"' + 1 + ',' + elemento[1] + '"'] = (Number(value));
                matrizBloquesInteriores['"' + 0 + ',' + elemento[1] + '"'] = (Number(matrizBloquesInteriores['"' + 1 + ',' + elemento[1] + '"']) / (Number(matrizIntrumentoIBC['"3,1"']) - Number(matrizIntrumentoIBC['"3,2"']))) * 100;
                $("#porcInt0" + elemento[1]).val(formato_numero(Math.abs(matrizBloquesInteriores['"' + 0 + ',' + elemento[1] + '"']), 2, ".", ",", "") + " %");
            }
            if (select == undefined) {
                matrizPromMandInt['"' + elemento[0] + ',' + elemento[1] + '"'] = Number(value);
                for (var i = 0; i < 10; i++) {
                    if (matrizPromMandInt['"' + i + ',' + elemento[1] + '"'] !== undefined && String(matrizPromMandInt['"' + i + ',' + elemento[1] + '"']) !== 'NaN') {
                        cont++;
                        acum += (matrizPromMandInt['"' + i + ',' + elemento[1] + '"'] * 1);
                        arregloDesvi.push((matrizPromMandInt['"' + i + ',' + elemento[1] + '"'] * 1));
                    }
                }
                matrizPromMandInt['"' + 10 + ',' + elemento[1] + '"'] = (acum == 0 && cont == 0 ? 0 : acum / cont);
                matrizPromMandInt['"' + 11 + ',' + elemento[1] + '"'] = (String(desviacionStandar(arregloDesvi)) == 'NaN' || desviacionStandar(arregloDesvi) == undefined ? 0 : desviacionStandar(arregloDesvi));
                matrizPromMandInt['"' + 12 + ',' + elemento[1] + '"'] = (matrizPromMandInt['"' + 11 + ',' + elemento[1] + '"'] / Math.sqrt(cont));
                matrizPromMandInt['"' + 13 + ',' + elemento[1] + '"'] = cont;
                $("#promInt" + elemento[1]).val(formato_numero(matrizPromMandInt['"' + 10 + ',' + elemento[1] + '"'], 4, ".", ",", ""));
                $("#DesvStanInt" + elemento[1]).val(formato_numero(matrizPromMandInt['"' + 11 + ',' + elemento[1] + '"'], 4, ".", ",", ""));
                $("#repetInt" + elemento[1]).html(formato_numero(matrizPromMandInt['"' + 12 + ',' + elemento[1] + '"'], 4, ".", ",", ""));
                $("#nroLectInt" + elemento[1]).val(formato_numero(matrizPromMandInt['"' + 13 + ',' + elemento[1] + '"'], 2, ".", ",", ""));
            }
            if (select !== undefined && select == 1 && value !== 'No') {
                var existe = false, columna = 0;
                for (var i = 0; i < 6; i++) {
                    if (Number(value) == Number(matrizPatronesInt['"' + i + ',' + elemento[1] + '"'])) {
                        columna = i;
                        existe = true;
                        break;
                    }
                }
                if (existe == false) {
                    matrizPatronesInt['"' + elemento[0] + ',' + elemento[1] + '"'] = Number(value);
                    for (var j = 0; j < 6; j++) {
                        if (matrizPatronesInt['"' + j + ',' + elemento[1] + '"'] !== undefined && String(matrizPatronesInt['"' + j + ',' + elemento[1] + '"']) !== 'NaN') {
                            for (var i = 0; i < dat_matrizPatrones.length; i++) {
                                if ((matrizPatronesInt['"' + j + ',' + elemento[1] + '"'] * 1) == dat_matrizPatrones[i].id) {
                                    acumLP += Math.abs(dat_matrizPatrones[i].valornominal);
                                    acum += dat_matrizPatrones[i].desviacionlongitud;
                                    acumU.push(dat_matrizPatrones[i].upatron / 2);
                                    acumDer.push(dat_matrizPatrones[i].derivada / dat_matrizPatrones[i].denominadorderivada);
                                }
                            }
                        }
                    }
                    if (acumLP > Math.abs(Number(matrizBloquesInteriores['"' + 1 + ',' + elemento[1] + '"']))) {
                        matrizPatronesInt['"' + elemento[0] + ',' + elemento[1] + '"'] = undefined;
                        $('#patrInt' + elemento[0] + '' + elemento[1]).val('No');
                        $('#patrInt' + elemento[0] + '' + elemento[1]).change();
                        swal("Acción Cancelada", "El bloque que intenta seleccionar supera la longitud nominal, por favor seleccione otro", "warning");
                    } else {
                        matrizPatronesInt['"' + 6 + ',' + elemento[1] + '"'] = acumLP;
                        matrizPatronesInt['"' + 7 + ',' + elemento[1] + '"'] = acum;
                        matrizPatronesInt['"' + 8 + ',' + elemento[1] + '"'] = (Number(matrizPatronesInt['"' + 6 + ',' + elemento[1] + '"']) + (Number(matrizPatronesInt['"' + 7 + ',' + elemento[1] + '"']) / 1000));
                        $("#longPatInt" + elemento[1]).val(matrizPatronesInt['"' + 6 + ',' + elemento[1] + '"']);
                        /*Calcular valores de fondo para los bloques*/
                        if (acumU.length == 1) {
                            matrizPatronesInt['"' + 9 + ',' + elemento[1] + '"'] = acumU[0];
                        } else {
                            var acumu = 0;
                            for (var i = 0; i < acumU.length; i++) {
                                acumu += Math.pow(Number(acumU[i]), 2);
                            }
                            matrizPatronesInt['"' + 9 + ',' + elemento[1] + '"'] = Math.sqrt(acumu);
                        }

                        if (acumDer.length == 1) {
                            matrizPatronesInt['"' + 10 + ',' + elemento[1] + '"'] = acumDer[0];
                        } else {
                            var acumu = 0;
                            for (var i = 0; i < acumDer.length; i++) {
                                acumu += Math.pow(Number(acumDer[i]), 2);
                            }
                            matrizPatronesInt['"' + 10 + ',' + elemento[1] + '"'] = Math.sqrt(acumu);
                        }
                        matrizPatronesInt['"' + 11 + ',' + elemento[1] + '"'] = Math.pow(matrizPatronesInt['"' + 9 + ',' + elemento[1] + '"'], 2) + Math.pow(matrizPatronesInt['"' + 10 + ',' + elemento[1] + '"'], 2);
                        matrizPatronesInt['"' + 11 + ',' + elemento[1] + '"'] = Math.sqrt(matrizPatronesInt['"' + 11 + ',' + elemento[1] + '"']);
                        if (Number(matrizPatronesInt['"' + 10 + ',' + elemento[1] + '"']) > Number(matrizIncertidumbreMandInt['"5,0"'])) {
                            matrizIncertidumbreMandInt['"5,0"'] = (matrizPatronesInt['"' + 10 + ',' + elemento[1] + '"']);
                        }
                        /*Calcular valores de fondo para los bloques*/
                    }

                } else {
                    $('#patrInt' + elemento[0] + '' + elemento[1]).val('No');
                    $('#patrInt' + elemento[0] + '' + elemento[1]).change();
                    swal("Acción Cancelada", "El bloque que intenta seleccionar ya lo ha utilizado anteriormente en la columna " + columna + ", por favor seleccione otro bloque para continuar", "warning");
                }
            }

            if (matrizPromMandInt['"' + 10 + ',' + elemento[1] + '"'] !== undefined && String(matrizPromMandInt['"' + 10 + ',' + elemento[1] + '"']) !== 'NaN' && matrizPatronesInt['"' + 8 + ',' + elemento[1] + '"'] !== undefined && String(matrizPatronesInt['"' + 8 + ',' + elemento[1] + '"']) !== 'NaN') {
                matrizPromMandInt['"' + 14 + ',' + elemento[1] + '"'] = matrizPromMandInt['"' + 10 + ',' + elemento[1] + '"'] - matrizPatronesInt['"' + 8 + ',' + elemento[1] + '"'];
                $("#errInt" + elemento[1]).val(formato_numero(matrizPromMandInt['"' + 14 + ',' + elemento[1] + '"'], 2, ".", ",", ""));
            }
            tablaIncertidumbreInterior();
            activarCalibracion();
        }
    } catch (error) {
        console.error("Ha ocurrido un error [capturarPatrones]--> ", error);
    }
}

function tablaIncertidumbreInterior() {
    if (dat_matrices == [] || dat_matrices == '[]' || dat_matrices.length == 0) {
        //Resolucion
        matrizIncertidumbreMandInt['"0,0"'] = (Number(matrizIntrumentoIBC['"2,2"']) * 1) / (Math.sqrt(12));
        matrizIncertidumbreMandInt['"0,3"'] = (Number(matrizIncertidumbreMandInt['"0,0"']) * Number(matrizIncertidumbreMandInt['"0,1"']));
        $("#resolIncInt").val(formato_numero(matrizIncertidumbreMandInt['"0,0"'], 4, ".", ",", ""));
        $("#resolIncIntCoef").val(formato_numero(matrizIncertidumbreMandInt['"0,1"'], 4, ".", ",", ""));
        $("#resolIncIntContri").val(formato_numero(matrizIncertidumbreMandInt['"0,3"'], 4, ".", ",", ""));
        //Repetibilidad
        matrizIncertidumbreMandInt['"1,3"'] = Number(matrizIncertidumbreMandInt['"1,1"']) * Number(matrizIncertidumbreMandInt['"1,0"']);
        $("#repetIncInt").val(formato_numero(matrizIncertidumbreMandInt['"1,0"'], 4, ".", ",", ""));
        $("#repetIncIntCoef").val(formato_numero(matrizIncertidumbreMandInt['"1,1"'], 4, ".", ",", ""));
        $("#repetIncIntContri").val(formato_numero(matrizIncertidumbreMandInt['"1,3"'], 4, ".", ",", ""));
        //Error de paralaje
        matrizIncertidumbreMandInt['"2,0"'] = (dat_matrizInstrCalib[0].tipo == 0 ? 0 : (Number(matrizParalaje['"' + 2 + ',' + 1 + '"']) / (Math.sqrt(3))));
        matrizIncertidumbreMandInt['"2,3"'] = (Number(matrizIncertidumbreMandInt['"2,0"']) * Number(matrizIncertidumbreMandInt['"2,1"']));
        $("#errParaIncInt").val(formato_numero(matrizIncertidumbreMandInt['"2,0"'], 4, ".", ",", ""));
        $("#errParaIncIntCoef").val(formato_numero(matrizIncertidumbreMandInt['"2,1"'], 4, ".", ",", ""));
        $("#errParaIncIntContri").val(formato_numero(matrizIncertidumbreMandInt['"2,3"'], 4, ".", ",", ""));
        //Error Abbe
        matrizIncertidumbreMandInt['"3,0"'] = (Number(errorAbbe['"' + 1 + ',' + 6 + '"']) / (Math.sqrt(12)));
        matrizIncertidumbreMandInt['"3,3"'] = (Number(matrizIncertidumbreMandInt['"3,0"']) * Number(matrizIncertidumbreMandInt['"3,1"']));
        $("#errAbbInt").val(formato_numero(matrizIncertidumbreMandInt['"3,0"'], 4, ".", ",", ""));
        $("#errAbbIntCoef").val(formato_numero(matrizIncertidumbreMandInt['"3,1"'], 4, ".", ",", ""));
        $("#errAbbIntContri").val(formato_numero(matrizIncertidumbreMandInt['"3,3"'], 4, ".", ",", ""));
        //Error de linealidad
        matrizIncertidumbreMandInt['"4,0"'] = (Number(matrizLineError['"' + 3 + ',' + 12 + '"']) / (Math.sqrt(12)));
        matrizIncertidumbreMandInt['"4,3"'] = (Number(matrizIncertidumbreMandInt['"4,0"']) * Number(matrizIncertidumbreMandInt['"4,1"']));
        $("#errLinInt").val(formato_numero(matrizIncertidumbreMandInt['"4,0"'], 4, ".", ",", ""));
        $("#errLinIntCoef").val(formato_numero(matrizIncertidumbreMandInt['"4,1"'], 4, ".", ",", ""));
        $("#errLinIntContri").val(formato_numero(matrizIncertidumbreMandInt['"4,3"'], 4, ".", ",", ""));
        //Incertidumbre Patrón    
        matrizIncertidumbreMandInt['"5,3"'] = ((Number(matrizIncertidumbreMandInt['"5,0"']) / 1000) * Number(matrizIncertidumbreMandInt['"5,1"']));
        $("#incertPatrInt").val(formato_numero((Number(matrizIncertidumbreMandInt['"5,0"']) / 1000), 4, ".", ",", ""));
        $("#incertPatrIntCoef").val(formato_numero(matrizIncertidumbreMandInt['"5,1"'], 4, ".", ",", ""));
        $("#incertPatrIntContri").val(formato_numero(matrizIncertidumbreMandInt['"5,3"'], 4, ".", ",", ""));
        //Coeficiente de dilatación
        var acum = 0, acumu = 0;
        acumu = Math.pow((Number(matrizTemp['"' + 0 + ',' + 4 + '"'])) / (Math.sqrt(3)), 2);
        acum = Math.pow(Number(matrizTemp['"' + 1 + ',' + 4 + '"']) / (Math.sqrt(3)), 2);
        matrizIncertidumbreMandInt['"6,0"'] = Math.sqrt(acum + acumu);
        matrizIncertidumbreMandInt['"6,1"'] = Number(matrizIntrumentoIBC['"3,2"']) * registroF14;
        matrizIncertidumbreMandInt['"6,3"'] = (Number(matrizIncertidumbreMandInt['"6,0"']) * Number(matrizIncertidumbreMandInt['"6,1"']));
        $("#coeDilInt").val(formato_numero(Number(matrizIncertidumbreMandInt['"6,0"']), 4, ".", ",", ""));
        $("#coeDilIntCoef").val(formato_numero(Number(matrizIncertidumbreMandInt['"6,1"']), 4, ".", ",", ""));
        $("#coeDilIntContri").val(formato_numero(Number(matrizIncertidumbreMandInt['"6,3"']), 4, ".", ",", ""));
        //Diferencia de temperatura
        matrizIncertidumbreMandInt['"7,0"'] = registroF14;
        matrizIncertidumbreMandInt['"7,1"'] = (Number(matrizIntrumentoIBC['"3,2"']) * (Number(matrizTemp['"' + 0 + ',' + 1 + '"']) > Number(matrizTemp['"' + 1 + ',' + 1 + '"']) ? Number(matrizTemp['"' + 0 + ',' + 1 + '"']) : Number(matrizTemp['"' + 1 + ',' + 1 + '"'])));
        matrizIncertidumbreMandInt['"7,3"'] = (Number(matrizIncertidumbreMandInt['"7,0"']) * Number(matrizIncertidumbreMandInt['"7,1"']));
        $("#difTempInt").val(formato_numero(Number(matrizIncertidumbreMandInt['"7,0"']), 4, ".", ",", ""));
        $("#difTempIntCoef").val(formato_numero(Number(matrizIncertidumbreMandInt['"7,1"']), 4, ".", ",", ""));
        $("#difTempIntContri").val(formato_numero(Number(matrizIncertidumbreMandInt['"7,3"']), 4, ".", ",", ""));
        //Incertidumbre combinada
        matrizIncertidumbreMandInt['"8,0"'] = (
            Math.pow(Number(matrizIncertidumbreMandInt['"0,3"']), 2) +
            Math.pow(Number(matrizIncertidumbreMandInt['"1,3"']), 2) +
            Math.pow(Number(matrizIncertidumbreMandInt['"2,3"']), 2) +
            Math.pow(Number(matrizIncertidumbreMandInt['"3,3"']), 2) +
            Math.pow(Number(matrizIncertidumbreMandInt['"4,3"']), 2) +
            Math.pow(Number(matrizIncertidumbreMandInt['"5,3"']), 2) +
            Math.pow(Number(matrizIncertidumbreMandInt['"6,3"']), 2));
        matrizIncertidumbreMandInt['"8,0"'] = Math.sqrt(Number(matrizIncertidumbreMandInt['"8,0"']));
        $("#incertCombInt").val(formato_numero(Number(matrizIncertidumbreMandInt['"8,0"']), 4, ".", ",", ""));
        //Grados efectivos de libertad
        matrizIncertidumbreMandInt['"9,0"'] = (Math.pow(Number(matrizIncertidumbreMandInt['"8,0"']), 4)) / (
            (Math.pow(Number(matrizIncertidumbreMandInt['"0,3"']), 4) / 200) +
            (Math.pow(Number(matrizIncertidumbreMandInt['"1,3"']), 4) / 9) +
            (Math.pow(Number(matrizIncertidumbreMandInt['"2,3"']), 4) / 200) +
            (Math.pow(Number(matrizIncertidumbreMandInt['"3,3"']), 4) / 200) +
            (Math.pow(Number(matrizIncertidumbreMandInt['"4,3"']), 4) / 200) +
            (Math.pow(Number(matrizIncertidumbreMandInt['"5,3"']), 4) / 200) +
            (Math.pow(Number(matrizIncertidumbreMandInt['"6,3"']), 4) / 200) +
            (Math.pow(Number(matrizIncertidumbreMandInt['"7,3"']), 4) / 200));
        $("#graEfecInt").val(formato_numero(Number(matrizIncertidumbreMandInt['"9,0"']), 4, ".", ",", ""));
        //Factor de cobertura
        matrizIncertidumbreMandInt['"10,0"'] = Calcular_TStudent(matrizIncertidumbreMandInt['"9,0"']);
        $("#factCoberInt").val(formato_numero(Number(matrizIncertidumbreMandInt['"10,0"']), 4, ".", ",", ""));
        //U exp
        matrizIncertidumbreMandInt['"11,0"'] = (Number(matrizIncertidumbreMandInt['"8,0"'])) * (Number(matrizIncertidumbreMandInt['"10,0"']));
        $("#uExpInt").val(formato_numero(Number(matrizIncertidumbreMandInt['"11,0"']), 4, ".", ",", ""));
    }
}

/*******Calculos para Calibración de profundidad******/

function capturarPatronesSP(value, elemento, select) {
    try {
        if (dat_matrices == [] || dat_matrices == '[]' || dat_matrices.length == 0) {
            var cont = 0, acum = 0, acumLP = 0, acumU = [], acumDer = [];
            var arregloDesvi = [];
            var ErrorPromedio = 0, ciclo = 0;
            if (select == 3) {
                matrizBloquesProfundidad['"' + 0 + ',' + elemento[1] + '"'] = (Number(value));
                matrizBloquesProfundidad['"' + 1 + ',' + elemento[1] + '"'] = (Number(matrizBloquesProfundidad['"' + 0 + ',' + elemento[1] + '"']) * (Number(matrizIntrumentoIBC['"3,1"']) - Number(matrizIntrumentoIBC['"3,2"']))) / 100;
                $("#inLongSP1" + elemento[1]).val(formato_numero(Math.abs(matrizBloquesProfundidad['"' + 1 + ',' + elemento[1] + '"']), 3, ".", ",", ""));
            }
            if (select == 4) {
                matrizBloquesProfundidad['"' + 1 + ',' + elemento[1] + '"'] = (Number(value));
                matrizBloquesProfundidad['"' + 0 + ',' + elemento[1] + '"'] = (Number(matrizBloquesProfundidad['"' + 1 + ',' + elemento[1] + '"']) / (Number(matrizIntrumentoIBC['"3,1"']) - Number(matrizIntrumentoIBC['"3,2"']))) * 100;
                $("#inLongSP0" + elemento[1]).val(formato_numero(Math.abs(matrizBloquesProfundidad['"' + 0 + ',' + elemento[1] + '"']), 0, ".", ",", "") + " %");
            }
            if (select == undefined) {
                matrizPromMandSP['"' + elemento[0] + ',' + elemento[1] + '"'] = Number(value);
                for (var i = 0; i < 10; i++) {
                    if (matrizPromMandSP['"' + i + ',' + elemento[1] + '"'] !== undefined && String(matrizPromMandSP['"' + i + ',' + elemento[1] + '"']) !== 'NaN') {
                        cont++;
                        acum += (matrizPromMandSP['"' + i + ',' + elemento[1] + '"'] * 1);
                        arregloDesvi.push((matrizPromMandSP['"' + i + ',' + elemento[1] + '"'] * 1));
                    }
                }
                matrizPromMandSP['"' + 10 + ',' + elemento[1] + '"'] = (acum == 0 && cont == 0 ? 0 : acum / cont);
                matrizPromMandSP['"' + 11 + ',' + elemento[1] + '"'] = (String(desviacionStandar(arregloDesvi)) == 'NaN' || desviacionStandar(arregloDesvi) == undefined ? 0 : desviacionStandar(arregloDesvi));
                matrizPromMandSP['"' + 12 + ',' + elemento[1] + '"'] = (matrizPromMandSP['"' + 11 + ',' + elemento[1] + '"'] / Math.sqrt(cont));
                matrizPromMandSP['"' + 13 + ',' + elemento[1] + '"'] = cont;
                $("#promSP" + elemento[1]).val(formato_numero(matrizPromMandSP['"' + 10 + ',' + elemento[1] + '"'], 2, ".", ",", ""));
                $("#DesvStanSP" + elemento[1]).val(formato_numero(matrizPromMandSP['"' + 11 + ',' + elemento[1] + '"'], 2, ".", ",", ""));
                $("#repetSP" + elemento[1]).html(formato_numero(matrizPromMandSP['"' + 12 + ',' + elemento[1] + '"'], 2, ".", ",", ""));
                $("#nrolectSP" + elemento[1]).val(formato_numero(matrizPromMandSP['"' + 13 + ',' + elemento[1] + '"'], 2, ".", ",", ""));
            }
            if (select !== undefined && select == 1 && value !== 'No') {
                var existe = false, columna = 0;
                for (var i = 0; i < 6; i++) {
                    if (value == matrizPatronesSP['"' + i + ',' + elemento[1] + '"']) {
                        columna = i;
                        existe = true;
                        break;
                    }
                }
                if (existe == false) {
                    matrizPatronesSP['"' + elemento[0] + ',' + elemento[1] + '"'] = Number(value);
                    for (var j = 0; j < 6; j++) {
                        if (matrizPatronesSP['"' + j + ',' + elemento[1] + '"'] !== undefined && String(matrizPatronesSP['"' + j + ',' + elemento[1] + '"']) !== 'NaN') {
                            for (var i = 0; i < dat_matrizPatrones.length; i++) {
                                if ((matrizPatronesSP['"' + j + ',' + elemento[1] + '"'] * 1) == dat_matrizPatrones[i].id) {
                                    acumLP += Math.abs(dat_matrizPatrones[i].valornominal);
                                    acum += dat_matrizPatrones[i].desviacionlongitud;
                                    acumU.push(dat_matrizPatrones[i].upatron / 2);
                                    acumDer.push(dat_matrizPatrones[i].derivada / dat_matrizPatrones[i].denominadorderivada);
                                }
                            }
                        }
                    }
                    if (acumLP > Math.abs(Number(matrizBloquesProfundidad['"' + 1 + ',' + elemento[1] + '"']))) {
                        matrizPatronesSP['"' + elemento[0] + ',' + elemento[1] + '"'] = undefined;
                        $('#bloqueSP' + elemento[0] + '' + elemento[1]).val('No');
                        $('#bloqueSP' + elemento[0] + '' + elemento[1]).change();
                        swal("Acción Cancelada", "El bloque que intenta seleccionar supera la longitud nominal, por favor seleccione otro", "warning");
                    } else {
                        matrizPatronesSP['"' + 6 + ',' + elemento[1] + '"'] = acumLP;
                        matrizPatronesSP['"' + 7 + ',' + elemento[1] + '"'] = acum;
                        matrizPatronesSP['"' + 8 + ',' + elemento[1] + '"'] = (Number(matrizPatronesSP['"' + 6 + ',' + elemento[1] + '"']) + (Number(matrizPatronesSP['"' + 7 + ',' + elemento[1] + '"']) / 1000));
                        $("#longPatSP" + elemento[1]).val(matrizPatronesSP['"' + 6 + ',' + elemento[1] + '"']);
                        /*Calcular valores de fondo para los bloques*/
                        if (acumU.length == 1) {
                            matrizPatronesSP['"' + 9 + ',' + elemento[1] + '"'] = acumU[0];
                        } else {
                            var acumu = 0;
                            for (var i = 0; i < acumU.length; i++) {
                                acumu += Math.pow(Number(acumU[i]), 2);
                            }
                            matrizPatronesSP['"' + 9 + ',' + elemento[1] + '"'] = Math.sqrt(acumu);
                        }

                        if (acumDer.length == 1) {
                            matrizPatronesSP['"' + 10 + ',' + elemento[1] + '"'] = acumDer[0];
                        } else {
                            var acumu = 0;
                            for (var i = 0; i < acumDer.length; i++) {
                                acumu += Math.pow(Number(acumDer[i]), 2);
                            }
                            matrizPatronesSP['"' + 10 + ',' + elemento[1] + '"'] = Math.sqrt(acumu);
                        }
                        matrizPatronesSP['"' + 11 + ',' + elemento[1] + '"'] = Math.pow(matrizPatronesSP['"' + 9 + ',' + elemento[1] + '"'], 2) + Math.pow(matrizPatronesSP['"' + 10 + ',' + elemento[1] + '"'], 2);
                        matrizPatronesSP['"' + 11 + ',' + elemento[1] + '"'] = Math.sqrt(matrizPatronesSP['"' + 11 + ',' + elemento[1] + '"']);
                        if (Number(matrizPatronesSP['"' + 10 + ',' + elemento[1] + '"']) > Number(matrizIncertidumbreMandSP['"5,0"'])) {
                            matrizIncertidumbreMandSP['"5,0"'] = (matrizPatronesSP['"' + 10 + ',' + elemento[1] + '"']);
                        }
                        /*Calcular valores de fondo para los bloques*/
                    }

                } else {
                    $('#bloqueSP' + elemento[0] + '' + elemento[1]).val('No');
                    $('#bloqueSP' + elemento[0] + '' + elemento[1]).change();
                    swal("Acción Cancelada", "El bloque que intenta seleccionar ya lo ha utilizado anteriormente en la columna " + columna + ", por favor seleccione otro bloque para continuar", "warning");
                }
            }
            if (matrizPromMandSP['"' + 10 + ',' + elemento[1] + '"'] !== undefined && String(matrizPromMandSP['"' + 10 + ',' + elemento[1] + '"']) !== 'NaN' && matrizPatronesSP['"' + 8 + ',' + elemento[1] + '"'] !== undefined && String(matrizPatronesSP['"' + 8 + ',' + elemento[1] + '"']) !== 'NaN') {
                matrizPromMandSP['"' + 14 + ',' + elemento[1] + '"'] = matrizPromMandSP['"' + 10 + ',' + elemento[1] + '"'] - matrizPatronesSP['"' + 8 + ',' + elemento[1] + '"'];
                $("#errSP" + elemento[1]).val(formato_numero(matrizPromMandSP['"' + 14 + ',' + elemento[1] + '"'], 2, ".", ",", ""));
            }
            tablaIncertidumbreSP();
            activarCalibracion();
        }
    } catch (error) {
        console.error("Ha ocurrido un error [capturarPatronesSP]--> ", error);
    }
}

function tablaIncertidumbreSP() {
    if (dat_matrices == [] || dat_matrices == '[]' || dat_matrices.length == 0) {
        //Resolucion
        matrizIncertidumbreMandSP['"0,0"'] = (Number(matrizIntrumentoIBC['"2,2"']) * 1) / (Math.sqrt(12));
        matrizIncertidumbreMandSP['"0,3"'] = (Number(matrizIncertidumbreMandSP['"0,0"']) * Number(matrizIncertidumbreMandSP['"0,1"']));
        $("#resolIncSP").val(formato_numero(matrizIncertidumbreMandSP['"0,0"'], 4, ".", ",", ""));
        $("#resolIncSPCoef").val(formato_numero(matrizIncertidumbreMandSP['"0,1"'], 4, ".", ",", ""));
        $("#resolIncSPContri").val(formato_numero(matrizIncertidumbreMandSP['"0,3"'], 4, ".", ",", ""));
        //Repetibilidad       
        matrizIncertidumbreMandSP['"1,3"'] = Number(matrizIncertidumbreMandSP['"1,1"']) * Number(matrizIncertidumbreMandSP['"1,0"']);
        $("#repetIncSP").val(formato_numero(matrizIncertidumbreMandSP['"1,0"'], 4, ".", ",", ""));
        $("#repetIncSPCoef").val(formato_numero(matrizIncertidumbreMandSP['"1,1"'], 4, ".", ",", ""));
        $("#repetIncSPContri").val(formato_numero(matrizIncertidumbreMandSP['"1,3"'], 4, ".", ",", ""));
        //Error de paralaje
        matrizIncertidumbreMandSP['"2,0"'] = (dat_matrizInstrCalib[0].tipo == 0 ? 0 : (Number(matrizParalaje['"' + 2 + ',' + 1 + '"']) / (Math.sqrt(3))));
        matrizIncertidumbreMandSP['"2,3"'] = (Number(matrizIncertidumbreMandSP['"2,0"']) * Number(matrizIncertidumbreMandSP['"2,1"']));
        $("#errParaIncSP").val(formato_numero(matrizIncertidumbreMandSP['"2,0"'], 4, ".", ",", ""));
        $("#errParaIncSPCoef").val(formato_numero(matrizIncertidumbreMandSP['"2,1"'], 4, ".", ",", ""));
        $("#errParaIncSPContri").val(formato_numero(matrizIncertidumbreMandSP['"2,3"'], 4, ".", ",", ""));
        //Incertidumbre Patrón    
        matrizIncertidumbreMandSP['"5,3"'] = ((Number(matrizIncertidumbreMandSP['"5,0"']) / 1000));
        $("#incertPatrSP").val(formato_numero((Number(matrizIncertidumbreMandSP['"5,0"']) / 1000), 4, ".", ",", ""));
        $("#incertPatrSPCoef").val(formato_numero(matrizIncertidumbreMandSP['"5,1"'], 4, ".", ",", ""));
        $("#incertPatrSPContri").val(formato_numero(matrizIncertidumbreMandSP['"5,3"'], 4, ".", ",", ""));
        //Coeficiente de dilatación
        var acum = 0, acumu = 0;
        acumu = Math.pow((Number(matrizTemp['"' + 0 + ',' + 4 + '"'])) / (Math.sqrt(3)), 2);
        acum = Math.pow(Number(matrizTemp['"' + 1 + ',' + 4 + '"']) / (Math.sqrt(3)), 2);
        matrizIncertidumbreMandSP['"6,0"'] = Math.sqrt(acum + acumu);
        matrizIncertidumbreMandSP['"6,1"'] = Number(matrizIntrumentoIBC['"3,2"']) * registroF14;
        matrizIncertidumbreMandSP['"6,3"'] = (Number(matrizIncertidumbreMandSP['"6,0"']) * Number(matrizIncertidumbreMandSP['"6,1"']));
        $("#coeDilSP").val(formato_numero(Number(matrizIncertidumbreMandSP['"6,0"']), 4, ".", ",", ""));
        $("#coeDilSPCoef").val(formato_numero(Number(matrizIncertidumbreMandSP['"6,1"']), 4, ".", ",", ""));
        $("#coeDilSPContri").val(formato_numero(Number(matrizIncertidumbreMandSP['"6,3"']), 4, ".", ",", ""));
        //Diferencia de temperatura
        matrizIncertidumbreMandSP['"7,0"'] = registroF14;
        matrizIncertidumbreMandSP['"7,1"'] = Number(matrizIntrumentoIBC['"3,2"']) * 0;
        matrizIncertidumbreMandSP['"7,3"'] = (Number(matrizIncertidumbreMandSP['"7,0"']) * Number(matrizIncertidumbreMandSP['"7,1"']));
        $("#difTempSP").val(formato_numero(Number(matrizIncertidumbreMandSP['"7,0"']), 4, ".", ",", ""));
        $("#difTempSPCoef").val(formato_numero(Number(matrizIncertidumbreMandSP['"7,1"']), 4, ".", ",", ""));
        $("#difTempSPContri").val(formato_numero(Number(matrizIncertidumbreMandSP['"7,3"']), 4, ".", ",", ""));
        //Incertidumbre combinada
        matrizIncertidumbreMandSP['"8,0"'] = (
            Math.pow(Number(matrizIncertidumbreMandSP['"0,3"']), 2) +
            Math.pow(Number(matrizIncertidumbreMandSP['"1,3"']), 2) +
            Math.pow(Number(matrizIncertidumbreMandSP['"2,3"']), 2) +
            Math.pow(Number(matrizIncertidumbreMandSP['"5,3"']), 2) +
            Math.pow(Number(matrizIncertidumbreMandSP['"6,3"']), 2));
        matrizIncertidumbreMandSP['"8,0"'] = Math.sqrt(Number(matrizIncertidumbreMandSP['"8,0"']));
        $("#incertCombSP").val(formato_numero(Number(matrizIncertidumbreMandSP['"8,0"']), 4, ".", ",", ""));
        //Grados efectivos de libertad
        matrizIncertidumbreMandSP['"9,0"'] = (Math.pow(Number(matrizIncertidumbreMandSP['"8,0"']), 4)) / (
            (Math.pow(Number(matrizIncertidumbreMandSP['"0,3"']), 4) / 200) +
            (Math.pow(Number(matrizIncertidumbreMandSP['"1,3"']), 4) / 9) +
            (Math.pow(Number(matrizIncertidumbreMandSP['"2,3"']), 4) / 200) +
            (Math.pow(Number(matrizIncertidumbreMandSP['"5,3"']), 4) / 200) +
            (Math.pow(Number(matrizIncertidumbreMandSP['"6,3"']), 4) / 200) +
            (Math.pow(Number(matrizIncertidumbreMandSP['"7,3"']), 4) / 200));
        $("#graEfecSP").val(formato_numero(Number(matrizIncertidumbreMandSP['"9,0"']), 4, ".", ",", ""));
        //Factor de cobertura
        matrizIncertidumbreMandSP['"10,0"'] = Calcular_TStudent(matrizIncertidumbreMandSP['"9,0"']);
        $("#factCoberSP").val(formato_numero(Number(matrizIncertidumbreMandSP['"10,0"']), 4, ".", ",", ""));
        //U exp
        matrizIncertidumbreMandSP['"11,0"'] = (Number(matrizIncertidumbreMandSP['"8,0"'])) * (Number(matrizIncertidumbreMandSP['"10,0"']));
        $("#uExpSP").val(formato_numero(Number(matrizIncertidumbreMandSP['"11,0"']), 4, ".", ",", ""));
    }
}

/*Calculos para calibracion de paso*/

function capturarPatronesCalP(value, elemento, select) {
    try {
        if (dat_matrices == [] || dat_matrices == '[]' || dat_matrices.length == 0) {
            var cont = 0, acum = 0, acumLP = 0, acumU = [], acumDer = [];
            var arregloDesvi = [];
            var ErrorPromedio = 0, ciclo = 0;

            if (select == 3) {
                matrizBloquesProfundidad['"' + 0 + ',' + elemento[1] + '"'] = (Number(value));
                matrizBloquesProfundidad['"' + 1 + ',' + elemento[1] + '"'] = (Number(matrizBloquesProfundidad['"' + 0 + ',' + elemento[1] + '"']) * (Number(matrizIntrumentoIBC['"3,1"']) - Number(matrizIntrumentoIBC['"3,2"']))) / 100;
                $("#inLongCalP1" + elemento[1]).val(formato_numero(Math.abs(matrizBloquesProfundidad['"' + 1 + ',' + elemento[1] + '"']), 3, ".", ",", ""));
            }
            if (select == 4) {
                matrizBloquesProfundidad['"' + 1 + ',' + elemento[1] + '"'] = (Number(value));
                matrizBloquesProfundidad['"' + 0 + ',' + elemento[1] + '"'] = (Number(matrizBloquesProfundidad['"' + 1 + ',' + elemento[1] + '"']) / (Number(matrizIntrumentoIBC['"3,1"']) - Number(matrizIntrumentoIBC['"3,2"']))) * 100;
                $("#inLongCalP0" + elemento[1]).val(formato_numero(Math.abs(matrizBloquesProfundidad['"' + 0 + ',' + elemento[1] + '"']), 0, ".", ",", "") + " %");
            }

            if (select == undefined) {
                matrizPromMandCalP['"' + elemento[0] + ',' + elemento[1] + '"'] = Number(value);
                for (var i = 0; i < 10; i++) {
                    if (matrizPromMandCalP['"' + i + ',' + elemento[1] + '"'] !== undefined && String(matrizPromMandCalP['"' + i + ',' + elemento[1] + '"']) !== 'NaN') {
                        cont++;
                        acum += (matrizPromMandCalP['"' + i + ',' + elemento[1] + '"'] * 1);
                        arregloDesvi.push((matrizPromMandCalP['"' + i + ',' + elemento[1] + '"'] * 1));
                    }
                }
                matrizPromMandCalP['"' + 10 + ',' + elemento[1] + '"'] = (acum == 0 && cont == 0 ? 0 : acum / cont);
                matrizPromMandCalP['"' + 11 + ',' + elemento[1] + '"'] = (String(desviacionStandar(arregloDesvi)) == 'NaN' || desviacionStandar(arregloDesvi) == undefined ? 0 : desviacionStandar(arregloDesvi));
                matrizPromMandCalP['"' + 12 + ',' + elemento[1] + '"'] = (matrizPromMandCalP['"' + 11 + ',' + elemento[1] + '"'] / Math.sqrt(cont));
                matrizPromMandCalP['"' + 13 + ',' + elemento[1] + '"'] = cont;
                $("#promCalP" + elemento[1]).val(formato_numero(matrizPromMandCalP['"' + 10 + ',' + elemento[1] + '"'], 2, ".", ",", ""));
                $("#DesvStanCalP" + elemento[1]).val(formato_numero(matrizPromMandCalP['"' + 11 + ',' + elemento[1] + '"'], 2, ".", ",", ""));
                $("#repetCalP" + elemento[1]).html(formato_numero(matrizPromMandCalP['"' + 12 + ',' + elemento[1] + '"'], 2, ".", ",", ""));
                $("#nrolectCalP" + elemento[1]).val(formato_numero(matrizPromMandCalP['"' + 13 + ',' + elemento[1] + '"'], 2, ".", ",", ""));
            }
            if (select !== undefined && select == 1 && value !== 'No') {
                var existe = false, columna = 0;
                for (var i = 0; i < 6; i++) {
                    if (value == matrizPatronesCalP['"' + i + ',' + elemento[1] + '"']) {
                        columna = i;
                        existe = true;
                        break;
                    }
                }
                if (existe == false) {
                    matrizPatronesCalP['"' + elemento[0] + ',' + elemento[1] + '"'] = Number(value);
                    for (var j = 0; j < 6; j++) {
                        if (matrizPatronesCalP['"' + j + ',' + elemento[1] + '"'] !== undefined && String(matrizPatronesCalP['"' + j + ',' + elemento[1] + '"']) !== 'NaN') {
                            for (var i = 0; i < dat_matrizPatrones.length; i++) {
                                if ((matrizPatronesCalP['"' + j + ',' + elemento[1] + '"'] * 1) == dat_matrizPatrones[i].id) {
                                    acumLP += Math.abs(dat_matrizPatrones[i].valornominal);
                                    acum += dat_matrizPatrones[i].desviacionlongitud;
                                    acumU.push(dat_matrizPatrones[i].upatron / 2);
                                    acumDer.push(dat_matrizPatrones[i].derivada / dat_matrizPatrones[i].denominadorderivada);
                                }
                            }
                        }
                    }
                    if (acumLP > Math.abs(formato_numero(Number(matrizBloquesPaso['"' + 1 + ',' + elemento[1] + '"'])), 3, ".", ",", "")) {
                        matrizPatronesCalP['"' + elemento[0] + ',' + elemento[1] + '"'] = undefined;
                        $('#bloqueCalP' + elemento[0] + '' + elemento[1]).val('No');
                        $('#bloqueCalP' + elemento[0] + '' + elemento[1]).change();
                        swal("Acción Cancelada", "El bloque que intenta seleccionar supera la longitud nominal, por favor seleccione otro", "warning");
                    } else {
                        matrizPatronesCalP['"' + 6 + ',' + elemento[1] + '"'] = acumLP;
                        matrizPatronesCalP['"' + 7 + ',' + elemento[1] + '"'] = acum;
                        matrizPatronesCalP['"' + 8 + ',' + elemento[1] + '"'] = (Number(matrizPatronesCalP['"' + 6 + ',' + elemento[1] + '"']) + (Number(matrizPatronesCalP['"' + 7 + ',' + elemento[1] + '"']) / 1000));
                        $("#longPatCalP" + elemento[1]).val(matrizPatronesCalP['"' + 6 + ',' + elemento[1] + '"']);
                        /*Calcular valores de fondo para los bloques*/
                        if (acumU.length == 1) {
                            matrizPatronesCalP['"' + 9 + ',' + elemento[1] + '"'] = acumU[0];
                        } else {
                            var acumu = 0;
                            for (var i = 0; i < acumU.length; i++) {
                                acumu += Math.pow(Number(acumU[i]), 2);
                            }
                            matrizPatronesCalP['"' + 9 + ',' + elemento[1] + '"'] = Math.sqrt(acumu);
                        }

                        if (acumDer.length == 1) {
                            matrizPatronesCalP['"' + 10 + ',' + elemento[1] + '"'] = acumDer[0];
                        } else {
                            var acumu = 0;
                            for (var i = 0; i < acumDer.length; i++) {
                                acumu += Math.pow(Number(acumDer[i]), 2);
                            }
                            matrizPatronesCalP['"' + 10 + ',' + elemento[1] + '"'] = Math.sqrt(acumu);
                        }
                        matrizPatronesCalP['"' + 11 + ',' + elemento[1] + '"'] = Math.pow(matrizPatronesCalP['"' + 9 + ',' + elemento[1] + '"'], 2) + Math.pow(matrizPatronesCalP['"' + 10 + ',' + elemento[1] + '"'], 2);
                        matrizPatronesCalP['"' + 11 + ',' + elemento[1] + '"'] = Math.sqrt(matrizPatronesCalP['"' + 11 + ',' + elemento[1] + '"']);
                        if (Number(matrizPatronesCalP['"' + 10 + ',' + elemento[1] + '"']) > Number(matrizIncertidumbreMandCalP['"5,0"'])) {
                            matrizIncertidumbreMandCalP['"5,0"'] = (matrizPatronesCalP['"' + 10 + ',' + elemento[1] + '"']);
                        }
                        /*Calcular valores de fondo para los bloques*/
                    }
                } else {
                    $('#bloqueCalP' + elemento[0] + '' + elemento[1]).val('No');
                    $('#bloqueCalP' + elemento[0] + '' + elemento[1]).change();
                    swal("Acción Cancelada", "El bloque que intenta seleccionar ya lo ha utilizado anteriormente en la columna " + columna + ", por favor seleccione otro bloque para continuar", "warning");
                }
            }
            if (matrizPromMandCalP['"' + 10 + ',' + elemento[1] + '"'] !== undefined && String(matrizPromMandCalP['"' + 10 + ',' + elemento[1] + '"']) !== 'NaN' && matrizPatronesCalP['"' + 8 + ',' + elemento[1] + '"'] !== undefined && String(matrizPatronesCalP['"' + 8 + ',' + elemento[1] + '"']) !== 'NaN') {
                matrizPromMandCalP['"' + 14 + ',' + elemento[1] + '"'] = matrizPromMandCalP['"' + 10 + ',' + elemento[1] + '"'] - matrizPatronesCalP['"' + 8 + ',' + elemento[1] + '"'];
                $("#errCalP" + elemento[1]).val(formato_numero(matrizPromMandCalP['"' + 14 + ',' + elemento[1] + '"'], 2, ".", ",", ""));
            }
            tablaIncertidumbreCalP();
            activarCalibracion();
        }
    } catch (error) {
        console.error("Ha ocurrido un error [capturarPatronesCalP]--> ", error);
    }
}

function tablaIncertidumbreCalP() {
    if (dat_matrices == [] || dat_matrices == '[]' || dat_matrices.length == 0) {
        //Resolucion
        matrizIncertidumbreMandCalP['"0,0"'] = (Number(matrizIntrumentoIBC['"2,2"']) * 1) / (Math.sqrt(12));
        matrizIncertidumbreMandCalP['"0,3"'] = (Number(matrizIncertidumbreMandCalP['"0,0"']) * Number(matrizIncertidumbreMandCalP['"0,1"']));
        $("#resolIncCalP").val(formato_numero(matrizIncertidumbreMandCalP['"0,0"'], 4, ".", ",", ""));
        $("#resolIncCalPCoef").val(formato_numero(matrizIncertidumbreMandCalP['"0,1"'], 4, ".", ",", ""));
        $("#resolIncCalPContri").val(formato_numero(matrizIncertidumbreMandCalP['"0,3"'], 4, ".", ",", ""));
        //Repetibilidad   
        matrizIncertidumbreMandCalP['"1,3"'] = Number(matrizIncertidumbreMandCalP['"1,1"']) * Number(matrizIncertidumbreMandCalP['"1,0"']);
        $("#repetIncCalP").val(formato_numero(matrizIncertidumbreMandCalP['"1,0"'], 4, ".", ",", ""));
        $("#repetIncCalPCoef").val(formato_numero(matrizIncertidumbreMandCalP['"1,1"'], 4, ".", ",", ""));
        $("#repetIncCalPContri").val(formato_numero(matrizIncertidumbreMandCalP['"1,3"'], 4, ".", ",", ""));
        //Error de paralaje
        matrizIncertidumbreMandCalP['"2,0"'] = (dat_matrizInstrCalib[0].tipo == 0 ? 0 : (Number(matrizParalaje['"' + 2 + ',' + 1 + '"']) / (Math.sqrt(3))));
        matrizIncertidumbreMandCalP['"2,3"'] = (Number(matrizIncertidumbreMandCalP['"2,0"']) * Number(matrizIncertidumbreMandCalP['"2,1"']));
        $("#errParaIncCalP").val(formato_numero(matrizIncertidumbreMandCalP['"2,0"'], 4, ".", ",", ""));
        $("#errParaIncCalPCoef").val(formato_numero(matrizIncertidumbreMandCalP['"2,1"'], 4, ".", ",", ""));
        $("#errParaIncCalPContri").val(formato_numero(matrizIncertidumbreMandCalP['"2,3"'], 4, ".", ",", ""));
        //Incertidumbre Patrón    
        matrizIncertidumbreMandCalP['"5,3"'] = ((Number(matrizIncertidumbreMandCalP['"5,0"']) / 1000) * Number(matrizIncertidumbreMandCalP['"5,1"']));
        $("#incertPatrCalP").val(formato_numero((Number(matrizIncertidumbreMandCalP['"5,0"']) / 1000), 4, ".", ",", ""));
        $("#incertPatrCalPCoef").val(formato_numero(matrizIncertidumbreMandCalP['"5,1"'], 4, ".", ",", ""));
        $("#incertPatrCalPContri").val(formato_numero(matrizIncertidumbreMandCalP['"5,3"'], 4, ".", ",", ""));
        //Coeficiente de dilatación
        var acum = 0, acumu = 0;
        acumu = Math.pow((Number(matrizTemp['"' + 0 + ',' + 4 + '"'])) / (Math.sqrt(3)), 2);
        acum = Math.pow(Number(matrizTemp['"' + 1 + ',' + 4 + '"']) / (Math.sqrt(3)), 2);
        matrizIncertidumbreMandCalP['"6,0"'] = Math.sqrt(acum + acumu);
        matrizIncertidumbreMandCalP['"6,1"'] = Number(matrizIntrumentoIBC['"3,2"']) * registroF14;
        matrizIncertidumbreMandCalP['"6,3"'] = (Number(matrizIncertidumbreMandCalP['"6,0"']) * Number(matrizIncertidumbreMandCalP['"6,1"']));
        $("#coeDilCalP").val(formato_numero(Number(matrizIncertidumbreMandCalP['"6,0"']), 4, ".", ",", ""));
        $("#coeDilCalPCoef").val(formato_numero(Number(matrizIncertidumbreMandCalP['"6,1"']), 4, ".", ",", ""));
        $("#coeDilCalPContri").val(formato_numero(Number(matrizIncertidumbreMandCalP['"6,3"']), 4, ".", ",", ""));
        //Diferencia de temperatura
        matrizIncertidumbreMandCalP['"7,0"'] = registroF14;
        matrizIncertidumbreMandCalP['"7,1"'] = Number(matrizIntrumentoIBC['"3,2"']) * 0;
        matrizIncertidumbreMandCalP['"7,3"'] = (Number(matrizIncertidumbreMandCalP['"7,0"']) * Number(matrizIncertidumbreMandCalP['"7,1"']));
        $("#difTempCalP").val(formato_numero(Number(matrizIncertidumbreMandCalP['"7,0"']), 4, ".", ",", ""));
        $("#difTempCalPCoef").val(formato_numero(Number(matrizIncertidumbreMandCalP['"7,1"']), 4, ".", ",", ""));
        $("#difTempCalPContri").val(formato_numero(Number(matrizIncertidumbreMandCalP['"7,3"']), 4, ".", ",", ""));
        //Incertidumbre combinada
        matrizIncertidumbreMandCalP['"8,0"'] = (
            Math.pow(Number(matrizIncertidumbreMandCalP['"0,3"']), 2) +
            Math.pow(Number(matrizIncertidumbreMandCalP['"1,3"']), 2) +
            Math.pow(Number(matrizIncertidumbreMandCalP['"2,3"'] !== undefined && String(matrizIncertidumbreMandCalP['"2,3"']) !== 'NaN' ? matrizIncertidumbreMandCalP['"2,3"'] : 0), 2) +
            Math.pow(Number(matrizIncertidumbreMandCalP['"5,3"']), 2) +
            Math.pow(Number(matrizIncertidumbreMandCalP['"6,3"']), 2));
        matrizIncertidumbreMandCalP['"8,0"'] = Math.sqrt(Number(matrizIncertidumbreMandCalP['"8,0"']));
        $("#incertCombCalP").val(formato_numero(Number(matrizIncertidumbreMandCalP['"8,0"']), 4, ".", ",", ""));
        //Grados efectivos de libertad
        matrizIncertidumbreMandCalP['"9,0"'] = (Math.pow(Number(matrizIncertidumbreMandCalP['"8,0"']), 4)) / (
            (Math.pow(Number(matrizIncertidumbreMandCalP['"0,3"']), 4) / 200) +
            (Math.pow(Number(matrizIncertidumbreMandCalP['"1,3"']), 4) / 9) +
            (Math.pow(Number(matrizIncertidumbreMandCalP['"2,3"']), 4) / 200) +
            (Math.pow(Number(matrizIncertidumbreMandCalP['"5,3"']), 4) / 200) +
            (Math.pow(Number(matrizIncertidumbreMandCalP['"6,3"']), 4) / 200) +
            (Math.pow(Number(matrizIncertidumbreMandCalP['"7,3"']), 4) / 200));
        $("#graEfecCalP").val(formato_numero(Number(matrizIncertidumbreMandCalP['"9,0"']), 4, ".", ",", ""));
        //Factor de cobertura
        matrizIncertidumbreMandCalP['"10,0"'] = Calcular_TStudent(matrizIncertidumbreMandCalP['"9,0"']);
        $("#factCoberCalP").val(formato_numero(Number(matrizIncertidumbreMandCalP['"10,0"']), 4, ".", ",", ""));
        //U exp
        matrizIncertidumbreMandCalP['"11,0"'] = (Number(matrizIncertidumbreMandCalP['"8,0"'])) * (Number(matrizIncertidumbreMandCalP['"10,0"']));
        $("#uExpCalP").val(formato_numero(Number(matrizIncertidumbreMandCalP['"11,0"']), 4, ".", ",", ""));
    }
}

/*Matriz extra**/
function matrizErrK(value, elemento, matriz) {
    try { 
        if (dat_matrices == [] || dat_matrices == '[]' || dat_matrices.length == 0) {
            var vectorEK = [];
            var cont = 0;
            var acum = 0;
            if (value !== undefined && String(value) !== 'NaN') {
                matrizErrorK['"' + elemento[0] + ',' + elemento[1] + '"'] = Number(value);
                for (var i = 0; i < 10; i++) {
                    if (matrizErrorK['"' + 1 + ',' + i + '"'] !== undefined && String(matrizErrorK['"' + 1 + ',' + i + '"']) !== 'NaN') {
                        vectorEK.push(matrizErrorK['"' + 1 + ',' + i + '"']);
                        cont++;
                        acum += Number(matrizErrorK['"' + 1 + ',' + i + '"']);
                    }
                }
                matrizErrorK['"' + 1 + ',' + 10 + '"'] = acum / cont;
                matrizErrorK['"' + 1 + ',' + 11 + '"'] = desviacionStandar(vectorEK);
                matrizErrorK['"' + 1 + ',' + 12 + '"'] = Number(matrizErrorK['"' + 1 + ',' + 10 + '"']);
                matrizErrorK['"' + 1 + ',' + 13 + '"'] = Number(matrizErrorK['"' + 1 + ',' + 11 + '"']) / Math.sqrt(9);


                $("#medEXTRA1" + 10).val(formato_numero(matrizErrorK['"' + 1 + ',' + 10 + '"'], 2, ".", ",", ""));
                $("#medEXTRA1" + 11).val(formato_numero(matrizErrorK['"' + 1 + ',' + 11 + '"'], 2, ".", ",", ""));
                $("#medEXTRA1" + 12).val(formato_numero(matrizErrorK['"' + 1 + ',' + 12 + '"'], 2, ".", ",", ""));
                $("#medEXTRA1" + 13).val(formato_numero(matrizErrorK['"' + 1 + ',' + 13 + '"'], 2, ".", ",", ""));
                matrizIncertidumbreMandExtra['"1,0"'] = Number(matrizErrorK['"' + 1 + ',' + 13 + '"']);
                matrizExtra();
            }
            activarCalibracion();
        }
    } catch (error) {
        console.error("Ha ocurrido un error [matrizErrK]--> ", error);
    }
}

function matrizExtra() {
    if (dat_matrices == [] || dat_matrices == '[]' || dat_matrices.length == 0) {
        //Resolucion
        matrizIncertidumbreMandExtra['"0,0"'] = (Number(matrizIntrumentoIBC['"2,2"']) * 1) / (Math.sqrt(12));
        matrizIncertidumbreMandExtra['"0,3"'] = (Number(matrizIncertidumbreMandExtra['"0,0"']) * Number(matrizIncertidumbreMandExtra['"0,1"']));
        $("#resolIncExtra").val(formato_numero(matrizIncertidumbreMandExtra['"0,0"'], 4, ".", ",", ""));
        $("#resolIncExtraCoef").val(formato_numero(matrizIncertidumbreMandExtra['"0,1"'], 4, ".", ",", ""));
        $("#resolIncExtraContri").val(formato_numero(matrizIncertidumbreMandExtra['"0,3"'], 4, ".", ",", ""));
        //Repetibilidad
        matrizIncertidumbreMandExtra['"1,3"'] = Number(matrizIncertidumbreMandExtra['"1,1"']) * Number(matrizIncertidumbreMandExtra['"1,0"']);
        $("#repetIncExtra").val(formato_numero(matrizIncertidumbreMandExtra['"1,0"'], 4, ".", ",", ""));
        $("#repetIncExtraCoef").val(formato_numero(matrizIncertidumbreMandExtra['"1,1"'], 4, ".", ",", ""));
        $("#repetIncExtraContri").val(formato_numero(matrizIncertidumbreMandExtra['"1,3"'], 4, ".", ",", ""));
        //Error de paralaje
        matrizIncertidumbreMandExtra['"2,0"'] = (dat_matrizInstrCalib[0].tipo !== 0 ? 0 : (Number(matrizParalaje['"' + 2 + ',' + 1 + '"']) / (Math.sqrt(3))));
        matrizIncertidumbreMandExtra['"2,3"'] = (Number(matrizIncertidumbreMandExtra['"2,0"']) * Number(matrizIncertidumbreMandExtra['"2,1"']));
        $("#errParaIncExtra").val(formato_numero(matrizIncertidumbreMandExtra['"2,0"'], 4, ".", ",", ""));
        $("#errParaIncExtraCoef").val(formato_numero(matrizIncertidumbreMandExtra['"2,1"'], 4, ".", ",", ""));
        $("#errParaIncExtraContri").val(formato_numero(matrizIncertidumbreMandExtra['"2,3"'], 4, ".", ",", ""));
        //Incertidumbre Patrón    
        matrizIncertidumbreMandExtra['"5,3"'] = ((Number(matrizIncertidumbreMandExtra['"5,0"']) / 1000) * Number(matrizIncertidumbreMandExtra['"5,1"']));
        $("#incertPatrExtra").val(formato_numero((Number(matrizIncertidumbreMandExtra['"5,0"']) / 1000), 4, ".", ",", ""));
        $("#incertPatrExtraCoef").val(formato_numero(matrizIncertidumbreMandExtra['"5,1"'], 4, ".", ",", ""));
        $("#incertPatrExtraContri").val(formato_numero(matrizIncertidumbreMandExtra['"5,3"'], 4, ".", ",", ""));
        //Coeficiente de dilatación
        var acum = 0, acumu = 0;
        acumu = Math.pow((Number(matrizTemp['"' + 0 + ',' + 4 + '"'])) / (Math.sqrt(3)), 2);
        acum = Math.pow(Number(matrizTemp['"' + 1 + ',' + 4 + '"']) / (Math.sqrt(3)), 2);
        matrizIncertidumbreMandExtra['"6,0"'] = Math.sqrt(acum + acumu);
        matrizIncertidumbreMandExtra['"6,1"'] = Number(matrizIntrumentoIBC['"3,2"']) * registroF14;
        matrizIncertidumbreMandExtra['"6,3"'] = (Number(matrizIncertidumbreMandExtra['"6,0"']) * Number(matrizIncertidumbreMandExtra['"6,1"']));
        $("#coeDilExtra").val(formato_numero(Number(matrizIncertidumbreMandExtra['"6,0"']), 4, ".", ",", ""));
        $("#coeDilExtraCoef").val(formato_numero(Number(matrizIncertidumbreMandExtra['"6,1"']), 4, ".", ",", ""));
        $("#coeDilExtraContri").val(formato_numero(Number(matrizIncertidumbreMandExtra['"6,3"']), 4, ".", ",", ""));
        //Diferencia de temperatura
        matrizIncertidumbreMandExtra['"7,0"'] = registroF14;
        matrizIncertidumbreMandExtra['"7,1"'] = Number(matrizIntrumentoIBC['"3,2"']) * 0;
        matrizIncertidumbreMandExtra['"7,3"'] = (Number(matrizIncertidumbreMandExtra['"7,0"']) * Number(matrizIncertidumbreMandExtra['"7,1"']));
        $("#difTempExtra").val(formato_numero(Number(matrizIncertidumbreMandExtra['"7,0"']), 4, ".", ",", ""));
        $("#difTempExtraCoef").val(formato_numero(Number(matrizIncertidumbreMandExtra['"7,1"']), 4, ".", ",", ""));
        $("#difTempExtraContri").val(formato_numero(Number(matrizIncertidumbreMandExtra['"7,3"']), 4, ".", ",", ""));
        //Incertidumbre combinada
        matrizIncertidumbreMandExtra['"8,0"'] = (
            Math.pow(Number(matrizIncertidumbreMandExtra['"0,3"']), 2) +
            Math.pow(Number(matrizIncertidumbreMandExtra['"1,3"']), 2) +
            Math.pow(Number(matrizIncertidumbreMandExtra['"2,3"']), 2) +
            Math.pow(Number(matrizIncertidumbreMandExtra['"5,3"']), 2) +
            Math.pow(Number(matrizIncertidumbreMandExtra['"6,3"']), 2));
        matrizIncertidumbreMandExtra['"8,0"'] = Math.sqrt(Number(matrizIncertidumbreMandExtra['"8,0"']));
        $("#incertCombExtra").val(formato_numero(Number(matrizIncertidumbreMandExtra['"8,0"']), 4, ".", ",", ""));
        //Grados efectivos de libertad
        matrizIncertidumbreMandExtra['"9,0"'] = (Math.pow(Number(matrizIncertidumbreMandExtra['"8,0"']), 4)) / (
            (Math.pow(Number(matrizIncertidumbreMandExtra['"0,3"']), 4) / 200) +
            (Math.pow(Number(matrizIncertidumbreMandExtra['"1,3"']), 4) / 9) +
            (Math.pow(Number(matrizIncertidumbreMandExtra['"2,3"']), 4) / 200) +
            (Math.pow(Number(matrizIncertidumbreMandExtra['"5,3"']), 4) / 200) +
            (Math.pow(Number(matrizIncertidumbreMandExtra['"6,3"']), 4) / 200));
        $("#graEfecExtra").val(formato_numero(Number(matrizIncertidumbreMandExtra['"9,0"']), 4, ".", ",", ""));
        //Factor de cobertura
        matrizIncertidumbreMandExtra['"10,0"'] = Calcular_TStudent(matrizIncertidumbreMandExtra['"9,0"']);
        $("#factCoberExtra").val(formato_numero(Number(matrizIncertidumbreMandExtra['"10,0"']), 4, ".", ",", ""));
        //U exp
        matrizIncertidumbreMandExtra['"11,0"'] = (Number(matrizIncertidumbreMandExtra['"8,0"'])) * (Number(matrizIncertidumbreMandExtra['"10,0"']));
        $("#uExpExtra").val(formato_numero(Number(matrizIncertidumbreMandExtra['"11,0"']), 4, ".", ",", ""));
    }
}

function asignarRepetibilidad(value) {
    if (dat_matrices == [] || dat_matrices == '[]' || dat_matrices.length == 0) {
        var elemento = value.substring(0, value.length - 1);
        var columna = value.substring(value.length - 1, value.length);
        if (elemento !== undefined && String(elemento) !== 'NaN' && columna !== undefined && String(columna) !== 'NaN') {
            if (elemento == 'repet' && matrizPromMandExt['"' + 12 + ',' + columna + '"'] !== undefined && String(matrizPromMandExt['"' + 12 + ',' + columna + '"']) !== 'NaN') {
                matrizIncertidumbreMandExt['"1,0"'] = Number(matrizPromMandExt['"' + 12 + ',' + columna + '"']);
                calcularRepetibilidad(Number(matrizIncertidumbreMandExt['"1,0"']));
                $("#repetIncExt").val(formato_numero(matrizIncertidumbreMandExt['"1,0"'], 4, ".", ",", ""));
            }
            if (elemento == 'repetInt' && matrizPromMandInt['"' + 12 + ',' + columna + '"'] !== undefined && String(matrizPromMandInt['"' + 12 + ',' + columna + '"']) !== 'NaN') {
                matrizIncertidumbreMandInt['"1,0"'] = Number(matrizPromMandInt['"' + 12 + ',' + columna + '"']);
                calcularRepetibilidad(Number(matrizIncertidumbreMandInt['"1,0"']), 'INT');
                $("#repetIncInt").val(formato_numero(matrizIncertidumbreMandInt['"1,0"'], 4, ".", ",", ""));
            }
            if (elemento == 'repetSP' && matrizPromMandSP['"' + 12 + ',' + columna + '"'] !== undefined && String(matrizPromMandSP['"' + 12 + ',' + columna + '"']) !== 'NaN') {
                matrizIncertidumbreMandSP['"1,0"'] = Number(matrizPromMandSP['"' + 12 + ',' + columna + '"']);
                calcularRepetibilidad(Number(matrizIncertidumbreMandSP['"1,0"']), 'SP');
                $("#repetIncSP").val(formato_numero(matrizIncertidumbreMandSP['"1,0"'], 4, ".", ",", ""));
            }
            if (elemento == 'repetCalP' && matrizPromMandCalP['"' + 12 + ',' + columna + '"'] !== undefined && String(matrizPromMandCalP['"' + 12 + ',' + columna + '"']) !== 'NaN') {
                matrizIncertidumbreMandCalP['"1,0"'] = Number(matrizPromMandCalP['"' + 12 + ',' + columna + '"']);
                calcularRepetibilidad(Number(matrizIncertidumbreMandCalP['"1,0"']), 'CP');
                $("#repetIncCalP").val(formato_numero(matrizIncertidumbreMandCalP['"1,0"'], 4, ".", ",", ""));
            }
        }
        activarCalibracion();
    }    
}

function modificarOperacionesPrevias() {
    $("#ingresoinst").html($('#IngresoLong').val());
    $("#modalInstrumentoLONG").modal("show");
}

function mostrarCotizacion() {   
    /*Servicio de Calibración*/
    $("#CServicio").val();
    /*Método*/
    $("#CMetodo").val();
    /*Observación de Calibración*/
    $("#CObservCerti").val();
    /*Proxima Calibración*/
    $("#CProxima").val();
    /*Días de Entrega*/
    $("#CEntrega").val();
    /*Asesor Comercial*/
    $("#CAsesorComer").val();
    /*Mostrar modal*/
    $("#modalcotizacion").modal("show");
}

function actualizarDesdeMoodal(value, elemento) {
    try {   
        if (dat_matrices == [] || dat_matrices == '[]' || dat_matrices.length == 0) {
            camposActualizar[elemento] = (elemento == 'UC' ? String(value) : Number(value));
            if (elemento == 'UC') {
                $('#medida, #medida1, #medida2, #medida3, #uniCalInst').html(camposActualizar[elemento]);
                /*Intervalo de medida*/
                $('#RangoMedida').html(matrizIntrumentoIBC['"0,1"'] + ' ' + camposActualizar['UC']);
                $('#RangoMedidaHST').html(matrizIntrumentoIBC['"0,2"'] + ' ' + camposActualizar['UC']);
                /*Division de escala normal y conversion*/
                matrizIntrumentoIBC['"1,1"'] = Number(matrizIntrumentoIBC['"1,1"']);
                matrizIntrumentoIBC['"1,2"'] = (camposActualizar['UC'] == 'mm' ? Number(matrizIntrumentoIBC['"1,1"']) : Number(matrizIntrumentoIBC['"1,1"']) * 25.4);
                $('#DivEscala').html(matrizIntrumentoIBC['"1,1"'] + ' ' + camposActualizar['UC']);
                $('#DivEscalaConv').html(matrizIntrumentoIBC['"1,2"'] + ' mm');
                /*Resolucion y conersion*/
                matrizIntrumentoIBC['"2,1"'] = Number(matrizIntrumentoIBC['"2,1"']);
                matrizIntrumentoIBC['"2,2"'] = (camposActualizar['UC'] == 'mm' ? Number(matrizIntrumentoIBC['"2,1"']) : Number(matrizIntrumentoIBC['"2,1"']) * 25.4);
                $('#Resolucion').html(matrizIntrumentoIBC['"2,1"'] + ' ' + camposActualizar['UC']);
                $('#ResolucionConv').html(matrizIntrumentoIBC['"2,2"'] + ' mm');
                /*Error maximo permitido positivo y negativo*/
                matrizIntrumentoIBC['"3,1"'] = (camposActualizar['UC'] == 'mm' ? Number(matrizIntrumentoIBC['"0,1"']) : Number(matrizIntrumentoIBC['"0,1"']) * 25.4);
                matrizIntrumentoIBC['"3,2"'] = (camposActualizar['UC'] == 'mm' ? Number(matrizIntrumentoIBC['"0,2"']) : Number(matrizIntrumentoIBC['"0,2"']) * 25.4);
                $('#ErrroMaxP').html(matrizIntrumentoIBC['"3,1"'] + ' mm');
                $('#ErrroMaxN').html(matrizIntrumentoIBC['"3,2"'] + ' mm');
                /*Intervalo desde*/
                matrizIntervalos['"' + 0 + ',' + 0 + '"'] = Number(matrizIntrumentoIBC['"0,1"']);
                matrizIntervalos['"' + 0 + ',' + 1 + '"'] = camposActualizar['UC'];
                matrizIntervalos['"' + 0 + ',' + 2 + '"'] = Number(matrizIntrumentoIBC['"3,1"']);
                matrizIntervalos['"' + 0 + ',' + 3 + '"'] = 'mm';
                cargarMatriz(["intMin", "intMax"], [0, 1], [0, 1, 2, 3], matrizIntervalos, 0);
                /*Intervalo hasta*/
                matrizIntervalos['"' + 1 + ',' + 0 + '"'] = Number(matrizIntrumentoIBC['"0,2"']);
                matrizIntervalos['"' + 1 + ',' + 1 + '"'] = camposActualizar['UC'];
                matrizIntervalos['"' + 1 + ',' + 2 + '"'] = Number(matrizIntrumentoIBC['"3,2"']);
                matrizIntervalos['"' + 1 + ',' + 3 + '"'] = 'mm';
                cargarMatriz(["intMin", "intMax"], [0, 1], [0, 1, 2, 3], matrizIntervalos, 0);

                var contMatrizMandExt = 0, contMatrizMandInt = 0, contMatrizMandSP = 0, contMatrizMandP = 0;
                for (var i = 0; i < 10; i++) {
                    calcularPiesMandExt(matrizBloques['"' + 0 + ',' + i + '"'], [0, i], 3);
                    capturarPatrones(matrizBloquesInteriores['"' + 0 + ',' + i + '"'], [0, i], 3);
                    capturarPatronesSP(matrizBloquesProfundidad['"' + 0 + ',' + i + '"'], [0, i], 3);
                    capturarPatronesCalP(matrizBloquesPaso['"' + 0 + ',' + i + '"'], [0, i], 3);
                }
            }
            if (elemento == 'TPO') {
                matrizIntrumentoIBC['"4,0"'] = camposActualizar[elemento] == 1 ? 'Analógico' : 'Digital';
                $('#tpoInstCal').html(matrizIntrumentoIBC['"4,0"']);
            }
            if (elemento == 'DS') {
                /*Division de escala normal y conversion*/
                matrizIntrumentoIBC['"1,1"'] = Number(camposActualizar[elemento]);
                matrizIntrumentoIBC['"1,2"'] = (camposActualizar['UC'] == 'mm' ? Number(matrizIntrumentoIBC['"1,1"']) / 25.4 : Number(matrizIntrumentoIBC['"1,1"']) * 25.4);
                $('#DivEscala').html(matrizIntrumentoIBC['"1,1"'] + ' ' + camposActualizar['UC']);
                $('#DivEscalaConv').html(matrizIntrumentoIBC['"1,2"'] + ' mm');
            }
            if (elemento == 'RSLN') {
                /*Resolucion y conersion*/
                matrizIntrumentoIBC['"2,1"'] = Number(camposActualizar[elemento]);
                matrizIntrumentoIBC['"2,2"'] = (camposActualizar['UC'] == 'mm' ? Number(matrizIntrumentoIBC['"2,1"']) / 25.4 : Number(matrizIntrumentoIBC['"2,1"']) * 25.4);
                $('#Resolucion').html(matrizIntrumentoIBC['"2,1"'] + ' ' + camposActualizar['UC']);
                $('#ResolucionConv').html(matrizIntrumentoIBC['"2,2"'] + ' mm');
            }
            if (elemento == 'IDSD') {
            }
            if (elemento == 'IHST') {
            }
            /*Realizar correciones**/
            llenarIncentidumbreExt();
            tablaIncertidumbreInterior();
            tablaIncertidumbreSP();
            tablaIncertidumbreCalP();
            matrizExtra();
            activarCalibracion();
        }
    } catch (error) {
        console.error("Ha ocurrido un error [actualizarDesdeMoodal]--> ", error);
    }
}

function asignarInformes(value, elemento) {
    try {
        if (dat_matrices == [] || dat_matrices == '[]' || dat_matrices.length == 0) {
            var elementoNum = (elemento.endsWith("Ext") ? 0 : (elemento.endsWith("Int") ? 1 : (elemento.endsWith("SP") ? 2 : (elemento.endsWith("CalP") ? 3 : 4))));
            if (elementoNum !== undefined && String(elementoNum) !== 'NaN') {
                if (elemento.startsWith("Concepto")) {
                    informes['"' + elementoNum + ',' + 0 + '"'] = value;
                }
                if (elemento.startsWith("Ajuste")) {
                    informes['"' + elementoNum + ',' + 1 + '"'] = value;
                }
                if (elemento.startsWith("Suministro")) {
                    informes['"' + elementoNum + ',' + 2 + '"'] = value;
                }
                if (elemento.startsWith("Observaciones")) {
                    informes['"' + elementoNum + ',' + 3 + '"'] = value;
                }
            }
        }
    } catch (error) {
        console.log("Ha aocurrido un error en [asignarInformes]: " + error);
    }
}

$("#FormPrevia").submit(function (e) {
    e.preventDefault();
    guardarCalculos();
})

function guardarCalculos() {
    var instruccion = "pendiente por asignar";
    var timefinal = new Date();
    horaFin = (timefinal.getHours() * 1 < 10 ? "0" : "") + timefinal.getHours() + ":" + (timefinal.getMinutes() * 1 < 10 ? "0" : "") + timefinal.getMinutes() + ":" + (timefinal.getSeconds() * 1 < 10 ? "0" : "") + timefinal.getSeconds();
    var matrizExteriores = '', matrizInteriores = '', matrizProfundidad = '', matrizPaso = '', matricesCal='', consulta = '';
    var incertidumbreExterior = '', incertidumbreInterior = '', incertidumbreProfundidad = '', incertidumbrePaso = '', incertidumbreExtra = '', observacionesPartes='';
    var contador = 0;    
    /*Estructura de mandibulas de exteriores*/
    for (var i = 0; i < 2; i++) {
        matrizExteriores += '(' + i +', ' ;
        for (var j = 0; j < 10; j++) {
            if (matrizBloques['"' + i + ',' + j + '"'] !== undefined && String(matrizBloques['"' + i + ',' + j + '"']) !== 'NaN') {                
                matrizExteriores += "'" + matrizBloques['"' + i + ',' + j + '"'] + "'" + (j == 9 ? '' : ',');                           
            } else {
                matrizExteriores += 'null' + (j == 9 ? '' : ',');
            }            
        }
        matrizExteriores += ", 'LNEXT', * ),\n";
    }
    for (var i = 0; i < 6; i++) {
        matrizExteriores += '(' + i + ', ';
        for (var j = 0; j < 10; j++) {
            if (matrizPatrones['"' + i + ',' + j + '"'] !== undefined && String(matrizPatrones['"' + i + ',' + j + '"']) !== 'NaN') {
                matrizExteriores += "'" + matrizPatrones['"' + i + ',' + j + '"'] + "'" + (j == 9 ? '' : ',');
            } else {
                matrizExteriores += 'null' + (j == 9 ? '' : ',');
            }
        }
        matrizExteriores += ", 'BQEXT', * ),\n";
    }
    for (var i = 0; i < 15; i++) {
        matrizExteriores += '(' + i + ', ';
        for (var j = 0; j < 10; j++) {
            if (matrizPromMandExt['"' + i + ',' + j + '"'] !== undefined && String(matrizPromMandExt['"' + i + ',' + j + '"']) !== 'NaN') {
                matrizExteriores += "'" + matrizPromMandExt['"' + i + ',' + j + '"'] + "'" + (j == 9 ? '' : ',');
            } else {
                matrizExteriores += 'null' + (j == 9 ? '' : ',');
            }
        }
        matrizExteriores += ", 'LCTEXT', * ),\n";
    }
    /*estructura de presupuesto de incertidumbres para exteriores*/
    for (var i = 0; i < 12; i++) {
        incertidumbreExterior += '(' + i + ', ';
        for (j = 0; j < 4; j++) {
            if (matrizIncertidumbreMandExt['"' + i + ',' + j + '"'] !== undefined && String(matrizIncertidumbreMandExt['"' + i + ',' + j + '"']) != 'NaN') {
                incertidumbreExterior += "'" + matrizIncertidumbreMandExt['"' + i + ',' + j + '"'] + "'" + (j == 3 ? '' : ',');
            } else {
                incertidumbreExterior += 'null' + (j == 3 ? '' : ',');
            }
        }
        incertidumbreExterior += ", 'PINCRTEXT', * ),\n";
    }
    /*Estructura de mandibulas interiores*/
    for (var i = 0; i < 2; i++) {
        matrizInteriores += '(' + i + ', ';
        for (var j = 0; j < 10; j++) {
            if (matrizBloquesInteriores['"' + i + ',' + j + '"'] !== undefined && String(matrizBloquesInteriores['"' + i + ',' + j + '"']) !== 'NaN') {
                matrizInteriores += matrizBloquesInteriores['"' + i + ',' + j + '"'] + (j == 9 ? '' : ',');
            } else {
                matrizInteriores += 'null' + (j == 9 ? '' : ',');
            }
        }
        matrizInteriores += ", 'LNINT', * ),\n";
    }
    for (var i = 0; i < 6; i++) {
        matrizInteriores += '(' + i + ', ';
        for (var j = 0; j < 10; j++) {
            if (matrizPatronesInt['"' + i + ',' + j + '"'] !== undefined && String(matrizPatronesInt['"' + i + ',' + j + '"']) !== 'NaN') {
                matrizInteriores += matrizPatronesInt['"' + i + ',' + j + '"'] + (j == 9 ? '' : ',');
            } else {
                matrizInteriores += 'null' + (j == 9 ? '' : ',');
            }
        }
        matrizInteriores += ", 'BQINT', * ),\n";
    }
    for (var i = 0; i < 15; i++) {
        matrizInteriores += '(' + i + ', ';
        for (var j = 0; j < 10; j++) {
            if (matrizPromMandInt['"' + i + ',' + j + '"'] !== undefined && String(matrizPromMandInt['"' + i + ',' + j + '"']) !== 'NaN') {
                matrizInteriores += matrizPromMandInt['"' + i + ',' + j + '"'] + (j == 9 ? '' : ',');
            } else {
                matrizInteriores += 'null' + (j == 9 ? '' : ',');
            }
        }
        matrizInteriores += ", 'LCTINT', * ),\n";
    }
    /*estructura de presupuesto de incertidumbres para interiores*/
    for (var i = 0; i < 12; i++) {
        incertidumbreInterior += '(' + i + ', ';
        for (j = 0; j < 4; j++) {
            if (matrizIncertidumbreMandInt['"' + i + ',' + j + '"'] !== undefined && String(matrizIncertidumbreMandInt['"' + i + ',' + j + '"']) != 'NaN') {
                incertidumbreInterior += "'" + matrizIncertidumbreMandInt['"' + i + ',' + j + '"'] + "'" + (j == 3 ? '' : ',');
            } else {
                incertidumbreInterior += 'null' + (j == 3 ? '' : ',');
            }
        }
        incertidumbreInterior += ", 'PINCRTINT', * ),\n";
    }
    /*Estructura de profundidad*/
    for (var i = 0; i < 2; i++) {
        matrizProfundidad += '(' + i + ', ';
        for (var j = 0; j < 10; j++) {
            if (matrizBloquesProfundidad['"' + i + ',' + j + '"'] !== undefined && String(matrizBloquesProfundidad['"' + i + ',' + j + '"']) !== 'NaN') {
                matrizProfundidad += matrizBloquesProfundidad['"' + i + ',' + j + '"'] + (j == 9 ? '' : ',');
            } else {
                matrizProfundidad += 'null' + (j == 9 ? '' : ',');
            }
        }
        matrizProfundidad += ", 'LNPFD', * ),\n";
    }
    for (var i = 0; i < 6; i++) {
        matrizProfundidad += '(' + i + ', ';
        for (var j = 0; j < 10; j++) {
            if (matrizPatronesSP['"' + i + ',' + j + '"'] !== undefined && String(matrizPatronesSP['"' + i + ',' + j + '"']) !== 'NaN') {
                matrizProfundidad += matrizPatronesSP['"' + i + ',' + j + '"'] + (j == 9 ? '' : ',');
            } else {
                matrizProfundidad += 'null' + (j == 9 ? '' : ',');
            }
        }
        matrizProfundidad += ", 'BQPFD', * ),\n";
    }
    for (var i = 0; i < 15; i++) {
        matrizProfundidad += '(' + i + ', ';
        for (var j = 0; j < 10; j++) {
            if (matrizPromMandSP['"' + i + ',' + j + '"'] !== undefined && String(matrizPromMandSP['"' + i + ',' + j + '"']) !== 'NaN') {
                matrizProfundidad += matrizPromMandSP['"' + i + ',' + j + '"'] + (j == 9 ? '' : ',');
            } else {
                matrizProfundidad += 'null' + (j == 9 ? '' : ',');
            }
        }
        matrizProfundidad += ", 'LCTPFD', * ),\n";
    }
    /*estructura de presupuesto de incertidumbres para profundidad*/
    for (var i = 0; i < 12; i++) {
        incertidumbreProfundidad += '(' + i + ', ';
        for (j = 0; j < 4; j++) {
            if (matrizIncertidumbreMandSP['"' + i + ',' + j + '"'] !== undefined && String(matrizIncertidumbreMandSP['"' + i + ',' + j + '"']) != 'NaN') {
                incertidumbreProfundidad += "'" + matrizIncertidumbreMandSP['"' + i + ',' + j + '"'] + "'" + (j == 3 ? '' : ',');
            } else {
                incertidumbreProfundidad += 'null' + (j == 3 ? '' : ',');
            }
        }
        incertidumbreProfundidad += ", 'PINCRTPFD', * ),\n";
    }
    /*Estructura de paso*/
    for (var i = 0; i < 2; i++) {
        matrizPaso += '(' + i + ', ';
        for (var j = 0; j < 10; j++) {
            if (matrizBloquesPaso['"' + i + ',' + j + '"'] !== undefined && String(matrizBloquesPaso['"' + i + ',' + j + '"']) !== 'NaN') {
                matrizPaso += matrizBloquesPaso['"' + i + ',' + j + '"'] + (j == 9 ? '' : ',');
            } else {
                matrizPaso += 'null' + (j == 9 ? '' : ',');
            }
        }
        matrizPaso += ",'LNPSO', * ),\n";
    }
    for (var i = 0; i < 6; i++) {
        matrizPaso += '(' + i + ', ';
        for (var j = 0; j < 10; j++) {
            if (matrizPatronesCalP['"' + i + ',' + j + '"'] !== undefined && String(matrizPatronesCalP['"' + i + ',' + j + '"']) !== 'NaN') {
                matrizPaso += matrizPatronesCalP['"' + i + ',' + j + '"'] + (j == 9 ? '' : ',');
            } else {
                matrizPaso += 'null' + (j == 9 ? '' : ',');
            }
        }
        matrizPaso += ", 'BQPSO', * ),\n";
    }
    for (var i = 0; i < 15; i++) {
        matrizPaso += '(' + i + ', ';
        for (var j = 0; j < 10; j++) {
            if (matrizPromMandCalP['"' + i + ',' + j + '"'] !== undefined && String(matrizPromMandCalP['"' + i + ',' + j + '"']) !== 'NaN') {
                matrizPaso += matrizPromMandCalP['"' + i + ',' + j + '"'] + (j == 9 ? '' : ',');
            } else {
                matrizPaso += 'null' + (j == 9 ? '' : ',');
            }
        }
        matrizPaso += ", 'LCTPSO', * ),\n";
    }
    /*estructura de presupuesto de incertidumbres para paso*/
    for (var i = 0; i < 12; i++) {
        incertidumbrePaso += '(' + i + ', ';
        for (j = 0; j < 4; j++) {
            if (matrizIncertidumbreMandCalP['"' + i + ',' + j + '"'] !== undefined && String(matrizIncertidumbreMandCalP['"' + i + ',' + j + '"']) != 'NaN') {
                incertidumbrePaso += "'" + matrizIncertidumbreMandCalP['"' + i + ',' + j + '"'] + "'" + (j == 3 ? '' : ',');
            } else {
                incertidumbrePaso += 'null' + (j == 3 ? '' : ',');
            }
        }
        incertidumbrePaso += ", 'PINCRTPSO', * ),\n";
    }
    /*Estructura de presupuesto de incertidumbres para extras*/
    for (var i = 0; i < 12; i++) {
        incertidumbreExtra += '(' + i + ', ';
        for (j = 0; j < 4; j++) {
            if (matrizIncertidumbreMandExtra['"' + i + ',' + j + '"'] !== undefined && String(matrizIncertidumbreMandExtra['"' + i + ',' + j + '"']) != 'NaN') {
                incertidumbreExtra += "'" + matrizIncertidumbreMandExtra['"' + i + ',' + j + '"'] + "'" + (j == 3 ? '' : ',');
            } else {
                incertidumbreExtra += 'null' + (j == 3 ? '' : ',');
            }
        }
        incertidumbreExtra += ", 'PINCRTXTR', * ),\n";
    }
    /*Estructura para las observaciones*/
    for (var i = 0; i < 5; i++) {
        observacionesPartes += '(' + i + ', ';
        for (j = 0; j < 4; j++) {
            if (informes['"' + i + ',' + j + '"'] !== undefined && String(informes['"' + i + ',' + j + '"']) != 'NaN') {
                observacionesPartes += "'" + informes['"' + i + ',' + j + '"'] + "'" + (j == 3 ? '' : ',');
            } else {
                observacionesPartes += 'null' + (j == 3 ? '' : ',');
            }
        }
        observacionesPartes += ", * )" + (i == 3 ? "$": ",") + "\n";
    }
    /*Validaciones matrices extras*/
    matricesCal += '(0,';
    matricesCal += "'" + errorAbbe['"' + 0 + ',' + 0 + '"'] + "', '"
        + errorAbbe['"' + 0 + ',' + 3 + '"'] + "', '"
        + errorAbbe['"' + 0 + ',' + 6 + '"'] + "', '"
        + errorAbbe['"' + 1 + ',' + 0 + '"'] + "', '"
        + errorAbbe['"' + 1 + ',' + 3 + '"'] + "', '"
        + errorAbbe['"' + 1 + ',' + 6 + '"'] + "', '"
        + errorAbbe['"' + 2 + ',' + 0 + '"'] + "'";
    matricesCal += ", null, null, null, null, null, null, 'ERRABB', *),\n(0,";

    matricesCal += "'" + matrizParalaje['"' + 0 + ',' + 0 + '"'] + "', '"
                + matrizParalaje['"' + 0 + ',' + 3 + '"'] + "', '"
        + matrizParalaje['"' + 0 + ',' + 5 + '"'] + "', '"
        + matrizParalaje['"' + 1 + ',' + 0 + '"'] + "', '"
        + matrizParalaje['"' + 1 + ',' + 3 + '"'] + "', '"
        + matrizParalaje['"' + 1 + ',' + 5 + '"'] + "', '"
        + matrizParalaje['"' + 2 + ',' + 1 + '"'] + "'";
    matricesCal += ",null, null, null, null, null, null, 'ERRPARJ', *),\n(0,";

    for (var i = 0; i < 13; i++) {
        matricesCal += "'" + matrizLineError['"' + 1 + ',' + i + '"'] + "'" + (i==12? ", 'ERRLC', *" : ', ');
    }
    matricesCal += '),\n';
    matricesCal += '(0,';
    for (var i = 0; i < 13; i++) {
        matricesCal += "'" + matrizLineError['"' + 3 + ',' + i + '"'] + "'" + (i == 12 ? ", 'ERRPAR', *" : ', ');
    }
    matricesCal += '),\n';
    matricesCal += '(0,';
    for (var i = 0; i < 13; i++) {
        matricesCal += "'" + matrizErrorK['"' + 1 + ',' + i + '"'] + "'" + (i == 12 ? ", 'ERRK', *" : ', ');
    }
    matricesCal += '),\n';
    matricesCal += '(0,';
    for (var i = 0; i < 11; i++) {
        matricesCal += "'" + matrizLineError['"' + 5 + ',' + i + '"'] + "'" + (i == 10 ? ", null, null, 'MEDLC', *" : ', ');
    }
    matricesCal += ')';
    /*Validaciones del QUERY*/
    if (matrizExteriores.endsWith(',\n')) {
        matrizExteriores = matrizExteriores.substring(0, matrizExteriores.length - 2);
    }
    if (matrizInteriores.endsWith(',\n')) {
        matrizInteriores = matrizInteriores.substring(0, matrizInteriores.length - 2);
    }
    if (matrizProfundidad.endsWith(',\n')) {
        matrizProfundidad = matrizProfundidad.substring(0, matrizProfundidad.length - 2);
    }
    if (matrizPaso.endsWith(',\n')) {
        matrizPaso = matrizPaso.substring(0, matrizPaso.length - 2);
    }    
    if (incertidumbreExterior.endsWith(',\n')) {
        incertidumbreExterior = incertidumbreExterior.substring(0, incertidumbreExterior.length - 2);
    }
    if (incertidumbreInterior.endsWith(',\n')) {
        incertidumbreInterior = incertidumbreInterior.substring(0, incertidumbreInterior.length - 2);
    }
    if (incertidumbreProfundidad.endsWith(',\n')) {
        incertidumbreProfundidad = incertidumbreProfundidad.substring(0, incertidumbreProfundidad.length - 2);
    }
    if (incertidumbrePaso.endsWith(',\n')) {
        incertidumbrePaso = incertidumbrePaso.substring(0, incertidumbrePaso.length - 2);
    }
    if (incertidumbreExtra.endsWith(',\n')) {
        incertidumbreExtra = incertidumbreExtra.substring(0, incertidumbreExtra.length - 2);
    }
    if (matricesCal.endsWith(',\n')) {
        matricesCal = matricesCal.substring(0, matricesCal.length - 2);
    }
    stop();
    consulta = LlamarAjax("Laboratorio/GuardarCertificadoLongitud", "instruccion=" + instruccion
        + "&numero=" + $('#IngresoLong').val()
        + "&horaini=" + horaIni
        + "&horafin=" + horaFin
        + "&tiempotrans=" + horaTrans
        + "&plantilla=" + $('#NumCertificado').val()
        + "&prxcal=" + $('#ProximaCali').val()
        + "&revision=" + $('#Revision').val()
        + "&observacion=" + $("#Observacion").val()
        + "&exteriores=" + matrizExteriores
        + "&interiores=" + matrizInteriores
        + "&profundidad=" + matrizProfundidad
        + "&paso=" + matrizPaso
        + "&incertidumbreExterior=" + incertidumbreExterior
        + "&incertidumbreInterior=" + incertidumbreInterior
        + "&incertidumbreProfundidad=" + incertidumbreProfundidad
        + "&incertidumbrePaso=" + incertidumbrePaso
        + "&incertidumbreExtra=" + incertidumbreExtra
        + "&observaciones=" + observacionesPartes
        + "&matricesCal=" + matricesCal).split("|");
    console.log(consulta);
    swal((Number(consulta[0]) == 0 ? "" : "Acción Cancelada"), consulta[1], (Number(consulta[0]) == 0 ? "success" : "warning")); 
    //guardarLocal();
}

function activarCalibracion() {
    if (isMarch == false) {
        start();
    }
}

function start() {
    if (isMarch == false) {
        timeInicial = new Date();
        horaIni = (timeInicial.getHours() * 1 < 10 ? "0" : "") + timeInicial.getHours() + ":" + (timeInicial.getMinutes() * 1 < 10 ? "0" : "") + timeInicial.getMinutes() + ":" + (timeInicial.getSeconds() * 1 < 10 ? "0" : "") + timeInicial.getSeconds();
        $("#CaHoraIni").val(horaIni);
        
        control = setInterval(cronometro, 10);
        isMarch = true;
    }
}

function cronometro() {
    timeActual = new Date();
    acumularTime = timeActual - timeInicial;
    acumularTime2 = new Date();
    acumularTime2.setTime(acumularTime);
    cc = Math.round(acumularTime2.getMilliseconds() / 10);
    ss = acumularTime2.getSeconds();
    mm = acumularTime2.getMinutes();
    hh = acumularTime2.getHours() - 18;
    if (cc < 10) { cc = "0" + cc; }
    if (ss < 10) { ss = "0" + ss; }
    if (mm < 10) { mm = "0" + mm; }
    if (hh < 10) { hh = "0" + (hh - 1); }
    horaTrans = hh + " : " + mm + " : " + ss;
    $("#CaTiempo").val(horaTrans);
}

function stop() {
    if (isMarch == true) {
        clearInterval(control);
        isMarch = false;
    }
}

function resume() {
    if (isMarch == false) {
        timeActu2 = new Date();
        timeActu2 = timeActu2.getTime();
        acumularResume = timeActu2 - acumularTime;

        timeInicial.setTime(acumularResume);
        control = setInterval(cronometro, 10);
        isMarch = true;
    }
}

function guardarLocal() {
    try {
        if (dat_matrices == [] || dat_matrices == '[]' || dat_matrices.length == 0) {
            if (Number($('#IngresoLong').val()) > 0) {
                /*if (IdCalibracion == 0) {
                    if (HoraInicio == "") {
                        d = new Date();
                        HoraInicio = (d.getHours() * 1 < 10 ? "0" : "") + d.getHours() + ":" + (d.getMinutes() * 1 < 10 ? "0" : "") + d.getMinutes();
                        $("#CaHoraIni").val(HoraInicio);
                        HoraCondicion = HoraInicio;
                        horaactual = (d.getHours() * 1 < 10 ? "0" : "") + d.getHours() + ":" + (d.getMinutes() * 1 < 10 ? "0" : "") + d.getMinutes();
                        $("#CaTiempo").val(RestarHoras(HoraInicio, horaactual));
                        IntervaloTiempo = null;
                        IntervaloTiempo = setInterval(TiempoTrascurrudi, 60000);
                    }
                }*/
                var archivo = "[{";
                /*Estructura de mandibulas de exteriores*/
                for (var i = 0; i < 2; i++) {
                    for (var j = 0; j < 10; j++) {
                        if (matrizBloques['"' + i + ',' + j + '"'] !== undefined && String(matrizBloques['"' + i + ',' + j + '"']) !== 'NaN') {
                            archivo += '"matrizBloques[' + i + ',' + j + ']": "' + matrizBloques['"' + i + ',' + j + '"'] + '",';
                        }
                    }
                }
                for (var i = 0; i < 6; i++) {
                    for (var j = 0; j < 10; j++) {
                        if (matrizPatrones['"' + i + ',' + j + '"'] !== undefined && String(matrizPatrones['"' + i + ',' + j + '"']) !== 'NaN') {
                            archivo += '"matrizPatrones[' + i + ',' + j + ']": "' + matrizPatrones['"' + i + ',' + j + '"'] + '",';
                        }
                    }
                }
                for (var i = 0; i < 15; i++) {
                    for (var j = 0; j < 10; j++) {
                        if (matrizPromMandExt['"' + i + ',' + j + '"'] !== undefined && String(matrizPromMandExt['"' + i + ',' + j + '"']) !== 'NaN') {
                            archivo += '"matrizPromMandExt[' + i + ',' + j + ']": "' + matrizPromMandExt['"' + i + ',' + j + '"'] + '",';
                        }
                    }
                }
                /*estructura de presupuesto de incertidumbres para exteriores*/
                for (var i = 0; i < 12; i++) {
                    for (j = 0; j < 4; j++) {
                        if (matrizIncertidumbreMandExt['"' + i + ',' + j + '"'] !== undefined && String(matrizIncertidumbreMandExt['"' + i + ',' + j + '"']) != 'NaN') {
                            archivo += '"matrizIncertidumbreMandExt[' + i + ',' + j + ']": "' + matrizIncertidumbreMandExt['"' + i + ',' + j + '",'] + '", ';
                        }
                    }
                }
                /*Estructura de mandibulas interiores*/
                for (var i = 0; i < 2; i++) {
                    for (var j = 0; j < 10; j++) {
                        if (matrizBloquesInteriores['"' + i + ',' + j + '"'] !== undefined && String(matrizBloquesInteriores['"' + i + ',' + j + '"']) !== 'NaN') {
                            archivo += '"matrizBloquesInteriores[' + i + ',' + j + ']": "' + matrizBloquesInteriores['"' + i + ',' + j + '"'] + '",';
                        }
                    }
                }
                for (var i = 0; i < 6; i++) {
                    for (var j = 0; j < 10; j++) {
                        if (matrizPatronesInt['"' + i + ',' + j + '"'] !== undefined && String(matrizPatronesInt['"' + i + ',' + j + '"']) !== 'NaN') {
                            archivo += '"matrizPatronesInt[' + i + ',' + j + ']": "' + matrizPatronesInt['"' + i + ',' + j + '"'] + '",';
                        }
                    }
                }
                for (var i = 0; i < 15; i++) {
                    for (var j = 0; j < 10; j++) {
                        if (matrizPromMandInt['"' + i + ',' + j + '"'] !== undefined && String(matrizPromMandInt['"' + i + ',' + j + '"']) !== 'NaN') {
                            archivo += '"matrizPromMandInt[' + i + ',' + j + ']": "' + matrizPromMandInt['"' + i + ',' + j + '"'] + '",';
                        }
                    }
                }
                /*estructura de presupuesto de incertidumbres para interiores*/
                for (var i = 0; i < 12; i++) {
                    for (j = 0; j < 4; j++) {
                        if (matrizIncertidumbreMandInt['"' + i + ',' + j + '"'] !== undefined && String(matrizIncertidumbreMandInt['"' + i + ',' + j + '"']) != 'NaN') {
                            archivo += '"matrizIncertidumbreMandInt[' + i + ',' + j + ']": "' + matrizIncertidumbreMandInt['"' + i + ',' + j + '"'] + '",';
                        }
                    }
                }
                /*Estructura de profundidad*/
                for (var i = 0; i < 2; i++) {
                    for (var j = 0; j < 10; j++) {
                        if (matrizBloquesProfundidad['"' + i + ',' + j + '"'] !== undefined && String(matrizBloquesProfundidad['"' + i + ',' + j + '"']) !== 'NaN') {
                            archivo += '"matrizBloquesProfundidad[' + i + ',' + j + ']": "' + matrizBloquesProfundidad['"' + i + ',' + j + '"'] + '",';
                        }
                    }
                }
                for (var i = 0; i < 6; i++) {
                    for (var j = 0; j < 10; j++) {
                        if (matrizPatronesSP['"' + i + ',' + j + '"'] !== undefined && String(matrizPatronesSP['"' + i + ',' + j + '"']) !== 'NaN') {
                            archivo += '"matrizPatronesSP[' + i + ',' + j + ']": "' + matrizPatronesSP['"' + i + ',' + j + '"'] + '",';
                        }
                    }
                }
                for (var i = 0; i < 15; i++) {
                    for (var j = 0; j < 10; j++) {
                        if (matrizPromMandSP['"' + i + ',' + j + '"'] !== undefined && String(matrizPromMandSP['"' + i + ',' + j + '"']) !== 'NaN') {
                            archivo += '"matrizPromMandSP[' + i + ',' + j + ']": "' + matrizPromMandSP['"' + i + ',' + j + '"'] + '",';
                        }
                    }
                }
                /*estructura de presupuesto de incertidumbres para profundidad*/
                for (var i = 0; i < 12; i++) {
                    for (j = 0; j < 4; j++) {
                        if (matrizIncertidumbreMandSP['"' + i + ',' + j + '"'] !== undefined && String(matrizIncertidumbreMandSP['"' + i + ',' + j + '"']) != 'NaN') {
                            archivo += '"matrizIncertidumbreMandSP[' + i + ',' + j + ']": "' + matrizIncertidumbreMandSP['"' + i + ',' + j + '"'] + '",';
                        }
                    }
                }
                /*Estructura de paso*/
                for (var i = 0; i < 2; i++) {
                    for (var j = 0; j < 10; j++) {
                        if (matrizBloquesPaso['"' + i + ',' + j + '"'] !== undefined && String(matrizBloquesPaso['"' + i + ',' + j + '"']) !== 'NaN') {
                            archivo += '"matrizBloquesPaso[' + i + ',' + j + ']": "' + matrizBloquesPaso['"' + i + ',' + j + '"'] + '",';
                        }
                    }
                }
                for (var i = 0; i < 15; i++) {
                    for (var j = 0; j < 10; j++) {
                        if (matrizPatronesCalP['"' + i + ',' + j + '"'] !== undefined && String(matrizPatronesCalP['"' + i + ',' + j + '"']) !== 'NaN') {
                            archivo += '"matrizPatronesCalP[' + i + ',' + j + ']": "' + matrizPatronesCalP['"' + i + ',' + j + '"'] + '", ';
                        }
                    }
                }
                for (var i = 0; i < 15; i++) {
                    for (var j = 0; j < 10; j++) {
                        if (matrizPromMandCalP['"' + i + ',' + j + '"'] !== undefined && String(matrizPromMandCalP['"' + i + ',' + j + '"']) !== 'NaN') {
                            archivo += '"matrizPromMandCalP[' + i + ',' + j + ']": "' + matrizPromMandCalP['"' + i + ',' + j + '"'] + '",';
                        }
                    }
                }
                /*estructura de presupuesto de incertidumbres para paso*/
                for (var i = 0; i < 12; i++) {
                    for (j = 0; j < 4; j++) {
                        if (matrizIncertidumbreMandCalP['"' + i + ',' + j + '"'] !== undefined && String(matrizIncertidumbreMandCalP['"' + i + ',' + j + '"']) != 'NaN') {
                            archivo += '"matrizIncertidumbreMandCalP[' + i + ',' + j + ']": "' + matrizIncertidumbreMandCalP['"' + i + ',' + j + '"'] + '",';
                        }
                    }
                }
                /*Estructura de presupuesto de incertidumbres para extras*/
                for (var i = 0; i < 12; i++) {
                    for (j = 0; j < 4; j++) {
                        if (matrizIncertidumbreMandExtra['"' + i + ',' + j + '"'] !== undefined && String(matrizIncertidumbreMandExtra['"' + i + ',' + j + '"']) != 'NaN') {
                            archivo += '"matrizIncertidumbreMandExtra[ ' + i + ',' + j + ']": "' + matrizIncertidumbreMandExtra['"' + i + ',' + j + '"'] + '",';
                        }
                    }
                }
                /*Estructura para las observaciones*/
                for (var i = 0; i < 5; i++) {
                    for (j = 0; j < 4; j++) {
                        if (informes['"' + i + ',' + j + '"'] !== undefined && String(informes['"' + i + ',' + j + '"']) != 'NaN') {
                            archivo += '"informes[' + i + ',' + j + ']": "' + informes['"' + i + ',' + j + '"'] + '",';
                        }
                    }
                }
                /*Validaciones matrices extras*/
                archivo += '"errorAbbe[0,0]": "' + errorAbbe['"' + 0 + ',' + 0 + '"'] + '", ' +
                    '"errorAbbe[0,3]": "' + errorAbbe['"' + 0 + ',' + 3 + '"'] + '", ' +
                    '"errorAbbe[0,6]": "' + errorAbbe['"' + 0 + ',' + 6 + '"'] + '", ' +
                    '"errorAbbe[1,0]": "' + errorAbbe['"' + 1 + ',' + 0 + '"'] + '", ' +
                    '"errorAbbe[1,3]": "' + errorAbbe['"' + 1 + ',' + 3 + '"'] + '", ' +
                    '"errorAbbe[1,6]": "' + errorAbbe['"' + 1 + ',' + 6 + '"'] + '", '
                '"errorAbbe[2,0]": "' + errorAbbe['"' + 2 + ',' + 0 + '"'] + '", ';

                archivo += '"matrizParalaje[0,0]": "' + matrizParalaje['"' + 0 + ',' + 0 + '"'] + '", ' +
                    '"matrizParalaje[0,3]": "' + matrizParalaje['"' + 0 + ',' + 3 + '"'] + '", ' +
                    '"matrizParalaje[0,5]": "' + matrizParalaje['"' + 0 + ',' + 5 + '"'] + '", ' +
                    '"matrizParalaje[1,0]": "' + matrizParalaje['"' + 1 + ',' + 0 + '"'] + '", ' +
                    '"matrizParalaje[1,3]": "' + matrizParalaje['"' + 1 + ',' + 3 + '"'] + '", ' +
                    '"matrizParalaje[1,5]": "' + matrizParalaje['"' + 1 + ',' + 5 + '"'] + '", ' +
                    '"matrizParalaje[2,1]": "' + matrizParalaje['"' + 2 + ',' + 1 + '"'] + '", ';

                for (var i = 0; i < 13; i++) {
                    if (matrizLineError['"1,' + i + '"'] !== undefined && String(matrizLineError['"1,' + i + '"']) !== 'NaN') {
                        archivo += '"matrizLineError[1,' + i + ']": "' + matrizLineError['"' + 1 + ',' + i + '"'] + '", ';
                    }

                }
                for (var i = 0; i < 13; i++) {
                    if (matrizLineError['"3,' + i + '"'] !== undefined && String(matrizLineError['"3,' + i + '"']) !== 'NaN') {
                        archivo += '"matrizLineError[3,' + i + ']": "' + matrizLineError['"' + 3 + ',' + i + '"'] + '", ';
                    }
                }
                for (var i = 0; i < 13; i++) {
                    if (matrizErrorK['"1,' + i + '"'] !== undefined && String(matrizErrorK['"1,' + i + '"']) !== 'NaN') {
                        archivo += '"matrizErrorK[1,' + i + ']": "' + matrizErrorK['"' + 1 + ',' + i + '"'] + '", ';
                    }
                }
                for (var i = 0; i < 11; i++) {
                    if (matrizLineError['"5,' + i + '"'] !== undefined && String(matrizLineError['"5,' + i + '"']) !== 'NaN') {
                        archivo += '"matrizLineError[5,' + i + ']": "' + matrizLineError['"' + 5 + ',' + i + '"'] + '", ';
                    }
                }
                /*Validaciones del QUERY*/
                if (archivo.endsWith(', ')) {
                    archivo = archivo.substring(0, archivo.length - 2);
                }
                archivo += '}]';
                localStorage.setItem(NumCertificado + "-" + $('#IngresoLong').val() + "-" + $('#Revision').val(), archivo);
                $.jGrowl("Guardando Temporal", { life: 1500, theme: 'growl-warning', header: '' });
                console.log("Guardar Local: ", JSON.parse(localStorage.getItem(NumCertificado + "-" + $('#IngresoLong').val() + "-" + $('#Revision').val())))
            }
        }
    } catch (error) {
        console.log("Error en la funcion [guardarLocal]:  " + error);
    }
}

function BuscarLocal() {
    if (dat_matrices == [] || dat_matrices == '[]' || dat_matrices.length == 0) {
        var archivo = localStorage.getItem(NumCertificado + "-" + $('#IngresoLong').val() + "-" + $('#Revision').val());
        if (archivo !== undefined && String(archivo) !== 'NaN') {
            archivo = JSON.parse(archivo);
            $.each(archivo[0], function (index, item) {
                var campo = (index).split('[');
                campo[1] = campo[1].replace("]", "");
                /*Externo*/
                if (item !== "undefined" && item !== "NaN") {

                    if (campo[0] === "matrizBloques") {
                        $("#longNomi" + (campo[1]).replace(',', '')).val(formato_numero(Math.abs(Number(item)), 2, ".", ",", "")).change();
                    }
                    if (campo[0] === "matrizPatrones") {
                        $("#bloque" + (campo[1]).replace(',', '')).val(Number(item)).change();
                    }
                    if (campo[0] === "matrizPromMandExt") {
                        $("#lecturaIBC" + (campo[1]).replace(',', '')).val(formato_numero((Number(item)), 2, ".", ",", "")).change();
                    }
                    if (campo[0] === "matrizIncertidumbreMandExt") {
                        matrizIncertidumbreMandExt['"' + campo[1] + '"'] = Number(item);
                    }
                    /*Internos*/
                    if (campo[0] === "matrizBloquesInteriores") {
                        $("#porcInt" + (campo[1]).replace(',', '')).val(formato_numero(Math.abs(Number(item)), 2, ".", ",", "")).change();
                    }
                    if (campo[0] === "matrizPatronesInt") {
                        $("#patrInt" + (campo[1]).replace(',', '')).val((Number(item))).change();
                    }
                    if (campo[0] === "matrizPromMandInt") {
                        $("#lectInt" + (campo[1]).replace(',', '')).val(Number(item)).change();
                    }
                    if (campo[0] === "matrizIncertidumbreMandInt") {
                        matrizIncertidumbreMandInt['"' + campo[1] + '"'] = Number(item);
                    }
                    /*Profundidad*/
                    if (campo[0] === "matrizBloquesProfundidad") {
                        $("#inLongSP" + (campo[1]).replace(',', '')).val(formato_numero(Math.abs(Number(item)), 2, ".", ",", "")).change()
                    }
                    if (campo[0] === "matrizPatronesSP") {
                        $("#bloqueSP" + (campo[1]).replace(',', '')).val(Number(item)).change();
                    }
                    if (campo[0] === "matrizPromMandSP") {
                        $("#lectSP" + (campo[1]).replace(',', '')).val(Number(item)).change();
                    }
                    if (campo[0] === "matrizIncertidumbreMandSP") {
                        matrizIncertidumbreMandSP['"' + campo[1] + '"'] = Number(item);
                    }
                    /*Paso*/
                    if (campo[0] === "matrizBloquesPaso") {
                        $("#porcInt" + (campo[1]).replace(',', '')).val(formato_numero(Math.abs(Number(item)), 2, ".", ",", "")).change();
                    }
                    if (campo[0] === "matrizPatronesCalP") {
                        $("#bloqueCalP" + (campo[1]).replace(',', '')).val(Number(item)).change();
                    }
                    if (campo[0] === "matrizPromMandCalP") {
                        $("#lectCalP" + (campo[1]).replace(',', '')).val(Number(item)).change();
                    }
                    if (campo[0] === "matrizIncertidumbreMandCalP") {
                        matrizIncertidumbreMandCalP['"' + campo[1] + '"'] = Number(item);
                    }
                    /*Extra*/
                    if (campo[0] === "matrizIncertidumbreMandExtra") {
                        matrizIncertidumbreMandExtra['"' + campo[1] + '"'] = Number(item);
                    }
                    /*Otras matrices*/
                    if (campo[0] === "informes") {
                        if (campo[1] == '0,0') {
                            $("#ConceptoExt").val(item).change();
                        }
                        if (campo[1] == '1,0') {
                            $("#ConceptoInt").val(item).change();
                        }
                        if (campo[1] == '2,0') {
                            $("#ConceptoSP").val(item).change();
                        }
                        if (campo[1] == '3,0') {
                            $("#ConceptoCalP").val(item).change();
                        }
                        if (campo[1] == '4,0') {
                            $("#ConceptoGral").val(item).change();
                        }


                        if (campo[1] == '0,1') {
                            $("#AjusteExt").val(item);
                        }
                        if (campo[1] == '1,1') {
                            $("#AjusteInt").val(item);
                        }
                        if (campo[1] == '2,1') {
                            $("#AjusteSP").val(item);
                        }
                        if (campo[1] == '3,1') {
                            $("#AjusteCalP").val(item);
                        }
                        if (campo[1] == '4,1') {
                            $("#AjusteGral").val(item);
                        }


                        if (campo[1] == '0,2') {
                            $("#SuministroExt").val(item);
                        }
                        if (campo[1] == '1,2') {
                            $("#SuministroInt").val(item);
                        }
                        if (campo[1] == '2,2') {
                            $("#SuministroSP").val(item);
                        }
                        if (campo[1] == '3,2') {
                            $("#SuministroCalP").val(item);
                        }
                        if (campo[1] == '4,2') {
                            $("#SuministroGral").val(item);
                        }

                        if (campo[1] == '0,3') {
                            $("#ObservacionesExt").val(item);
                        }
                        if (campo[1] == '1,3') {
                            $("#ObservacionesInt").val(item);
                        }
                        if (campo[1] == '2,3') {
                            $("#ObservacionesSP").val(item);
                        }
                        if (campo[1] == '3,3') {
                            $("#ObservacionesCalP").val(item);
                        }
                        if (campo[1] == '4,3') {
                            $("#ObservacionesGral").val(item);
                        }
                    }

                    if (campo[0] === "errorAbbe") {
                        $("#errAbbe" + (campo[1]).replace(',', '')).val((item)).change();
                    }

                    if (campo[0] === "matrizParalaje") {
                        $("#errPar" + (campo[1]).replace(',', '')).val((item)).change();
                    }

                    if (campo[0] === "matrizLineError") {
                        if (Number(campo[1].split(',')[0]) == 1) {
                            $("#errLin1" + (campo[1].split(',')[1])).val(formato_numero((Number(item)), 2, ".", ",", "")).change();
                        }
                        if (Number(campo[1].split(',')[0]) == 3) {
                            $("#errParInt1" + (campo[1].split(',')[1])).val(formato_numero((Number(item)), 2, ".", ",", "")).change();
                        }
                        if (Number(campo[1].split(',')[0]) == 5) {
                            $("#medLC1" + (campo[1].split(',')[1])).val(formato_numero((Number(item)), 2, ".", ",", "")).change();
                        }
                    }

                    if (campo[0] === "matrizErrorK") {
                        if (Number(campo[1].split(',')[1]) < 10) {
                            $("#medEXTRA" + (campo[1]).replace(',', '')).val(formato_numero((Number(item)), 2, ".", ",", "")).change();
                        }
                    }
                }
            });
        }
    }
}

function cargarCerificado() {
    try {
        if (dat_matrices.length > 0) {
            $.each(dat_matrices, function (index, item) {
                $.each(item, function (indexHijo, itemHijo) {
                    if (itemHijo !== null && String(itemHijo) !== 'null' && item.indicador == 'LNEXT') {
                        $("#inLongN" + item.fila + "" + (Number((indexHijo).replace('col', '')) - 1)).val(formato_numero(Math.abs(Number(itemHijo)), 0, ".", ",", "") + ((item.fila == 0)? ' %' : '') ).change();
                    }
                    if (itemHijo !== null && String(itemHijo) !== 'null' && item.indicador == 'BQEXT') {
                        $("#bloque" + item.fila + "" + (Number((indexHijo).replace('col', '')) - 1)).val(formato_numero(Math.abs(Number(itemHijo)), 0, ".", ",", "")).change();
                    }
                    /*Falta la sumatoria de la longitud*/
                    if (itemHijo !== null && String(itemHijo) !== 'null' && item.indicador == 'LCTEXT') {
                        var dec = ((item.fila == 13 || item.fila < 10) ? 2 : 4); 
                        var campHTML = (item.fila < 10 ? "lecturaIBC" + item.fila + "" + (Number((indexHijo).replace('col', '')) - 1) :
                            (item.fila == 10) ? "promedio" + (Number((indexHijo).replace('col', '')) - 1) : 
                                (item.fila == 11) ? "desvStand" + (Number((indexHijo).replace('col', '')) - 1) :
                                    (item.fila == 12) ? "repet" + (Number((indexHijo).replace('col', '')) - 1) :
                                        (item.fila == 13) ? "nroLect" + (Number((indexHijo).replace('col', '')) - 1) :
                                            (item.fila == 14) ? "error" + (Number((indexHijo).replace('col', '')) - 1) : "");
                        if ((item.fila < 10)) {
                            $("#" + campHTML).val(formato_numero(Math.abs(Number(itemHijo)), dec, ".", ",", "")).change();
                        } else {
                            $("#" + campHTML).html(formato_numero(Math.abs(Number(itemHijo)), dec, ".", ",", ""));
                        }
                        
                    }
                    /*Matriz de exteriores*/
                    if (itemHijo !== null && String(itemHijo) !== 'null' && item.indicador == 'LNINT') {
                        $("#porcInt" + item.fila + "" + (Number((indexHijo).replace('col', '')) - 1)).val(formato_numero(Math.abs(Number(itemHijo)), ((item.fila == 0) ? 2 : 3), ".", ",", "") + ((item.fila == 0) ? ' %' : '')).change();
                    }
                    if (itemHijo !== null && String(itemHijo) !== 'null' && item.indicador == 'BQINT') {
                        $("#patrInt" + item.fila + "" + (Number((indexHijo).replace('col', '')) - 1)).val(formato_numero(Math.abs(Number(itemHijo)), 0, ".", ",", "")).change();
                    }
                    /*Falta la sumatoria de la longitud*/
                    if (itemHijo !== null && String(itemHijo) !== 'null' && item.indicador == 'LCTINT') {
                        var dec = ((item.fila >= 13 || item.fila < 10) ? 2 : 4);
                        var campHTML = (item.fila < 10 ? "lectInt" + item.fila + "" + (Number((indexHijo).replace('col', '')) - 1) :
                            (item.fila == 10) ? "promInt" + (Number((indexHijo).replace('col', '')) - 1) :
                                (item.fila == 11) ? "DesvStanInt" + (Number((indexHijo).replace('col', '')) - 1) :
                                    (item.fila == 12) ? "repetInt" + (Number((indexHijo).replace('col', '')) - 1) :
                                        (item.fila == 13) ? "nroLectInt" + (Number((indexHijo).replace('col', '')) - 1) :
                                            (item.fila == 14) ? "errInt" + (Number((indexHijo).replace('col', '')) - 1) : "");
                        if ((item.fila == 12)) {
                            $("#" + campHTML).html(formato_numero(Math.abs(Number(itemHijo)), dec, ".", ",", ""));
                        } else {
                            $("#" + campHTML).val(formato_numero(Math.abs(Number(itemHijo)), dec, ".", ",", "")).change();
                        }
                    }
                    /*Matriz de Sonda de profundidad*/
                    if (itemHijo !== null && String(itemHijo) !== 'null' && item.indicador == 'LNPFD') {
                        $("#inLongSP" + item.fila + "" + (Number((indexHijo).replace('col', '')) - 1)).val(formato_numero(Math.abs(Number(itemHijo)), ((item.fila == 0) ? 2 : 3), ".", ",", "") + ((item.fila == 0) ? ' %' : '')).change();
                    }
                    if (itemHijo !== null && String(itemHijo) !== 'null' && item.indicador == 'BQPFD') {
                        $("#bloqueSP" + item.fila + "" + (Number((indexHijo).replace('col', '')) - 1)).val(formato_numero(Math.abs(Number(itemHijo)), 0, ".", ",", "")).change();
                    }
                    /*Falta la sumatoria de la longitud*/
                    if (itemHijo !== null && String(itemHijo) !== 'null' && item.indicador == 'LCTPFD') {
                        var campHTML = (item.fila < 10 ? "lectSP" + item.fila + "" + (Number((indexHijo).replace('col', '')) - 1) :
                            (item.fila == 10) ? "promSP" + (Number((indexHijo).replace('col', '')) - 1) :
                                (item.fila == 11) ? "DesvStanSP" + (Number((indexHijo).replace('col', '')) - 1) :
                                    (item.fila == 12) ? "repetSP" + (Number((indexHijo).replace('col', '')) - 1) :
                                        (item.fila == 13) ? "nrolectSP" + (Number((indexHijo).replace('col', '')) - 1) :
                                            (item.fila == 14) ? "errSP" + (Number((indexHijo).replace('col', '')) - 1) : "");
                        if ((item.fila == 12)) {
                            $("#" + campHTML).html(formato_numero(Math.abs(Number(itemHijo)), 2, ".", ",", ""));
                        } else {
                            $("#" + campHTML).val(formato_numero(Math.abs(Number(itemHijo)), 2, ".", ",", "")).change();
                        }
                    }
                    /*Matriz de Paso*/
                    if (itemHijo !== null && String(itemHijo) !== 'null' && item.indicador == 'LNPSO') {
                        $("#inLongCalP" + item.fila + "" + (Number((indexHijo).replace('col', '')) - 1)).val(formato_numero(Math.abs(Number(itemHijo)), ((item.fila == 0) ? 2 : 3), ".", ",", "") + ((item.fila == 0) ? ' %' : '')).change();
                    }
                    if (itemHijo !== null && String(itemHijo) !== 'null' && item.indicador == 'BQPSO') {
                        $("#bloqueCalP" + item.fila + "" + (Number((indexHijo).replace('col', '')) - 1)).val(formato_numero(Math.abs(Number(itemHijo)), 0, ".", ",", "")).change();
                    }
                    /*Falta la sumatoria de la longitud*/
                    if (itemHijo !== null && String(itemHijo) !== 'null' && item.indicador == 'LCTPSO') {
                        var campHTML = (item.fila < 10 ? "lectCalP" + item.fila + "" + (Number((indexHijo).replace('col', '')) - 1) :
                            (item.fila == 10) ? "promCalP" + (Number((indexHijo).replace('col', '')) - 1) :
                                (item.fila == 11) ? "DesvStanCalP" + (Number((indexHijo).replace('col', '')) - 1) :
                                    (item.fila == 12) ? "repetCalP" + (Number((indexHijo).replace('col', '')) - 1) :
                                        (item.fila == 13) ? "nrolectCalP" + (Number((indexHijo).replace('col', '')) - 1) :
                                            (item.fila == 14) ? "errCalP" + (Number((indexHijo).replace('col', '')) - 1) : "");
                        if ((item.fila == 12)) {
                            $("#" + campHTML).html(formato_numero(Math.abs(Number(itemHijo)), 2, ".", ",", ""));
                        } else {
                            $("#" + campHTML).val(formato_numero(Math.abs(Number(itemHijo)), 2, ".", ",", "")).change();
                        }
                    }

                    if (itemHijo !== null && String(itemHijo) !== 'null' && item.indicador == 'ERRABB') {
                        if (indexHijo == 'col1') {
                            $("#errAbbe00").val(Number(itemHijo)).change();
                        }
                        if (indexHijo == 'col2') {
                            $("#errAbbe03").val(Number(itemHijo)).change();
                        }
                        if (indexHijo == 'col3') {
                            $("#errAbbe06").val(Number(itemHijo)).change();
                        }                        
                        if (indexHijo == 'col4') {
                            $("#errAbbe10").val((itemHijo)).change();
                        }                        
                        if (indexHijo == 'col5') {
                            $("#errAbbe13").html(Number(itemHijo));
                        }
                        if (indexHijo == 'col6') {
                            $("#errAbbe16").html(Number(itemHijo));
                        }
                        if (indexHijo == 'col7') {
                            $("#errAbbe20").html(Number(itemHijo));
                        }                        
                    }

                    if (itemHijo !== null && String(itemHijo) !== 'null' && item.indicador == 'ERRPARJ') {
                        if (indexHijo == 'col1') {
                            $("#errPar00").val(Number(itemHijo)).change();
                        }
                        if (indexHijo == 'col2') {
                            $("#errPar03").val((itemHijo)).change();
                        }
                        if (indexHijo == 'col3') {
                            $("#errPar13").val(Number(itemHijo)).change();
                        }
                        if (indexHijo == 'col4') {
                            $("#errPar10").val(Number(itemHijo)).change();
                        }
                        if (indexHijo == 'col5') {
                            $("#errPar05").val(Number(itemHijo));
                        }
                        if (indexHijo == 'col6') {
                            $("#errPar15").val(Number(itemHijo));
                        }
                        if (indexHijo == 'col7') {
                            $("#errPar20").html(Number(itemHijo));
                        }
                    }
                    
                    if (itemHijo !== null && String(itemHijo) !== 'null' && item.indicador == 'ERRLC') {
                        $("#errLin1" + (Number((indexHijo).replace('col', '')) - 1)).val(formato_numero(Math.abs(Number(itemHijo)), 2, ".", ",", "")).change();
                    }
                    if (itemHijo !== null && String(itemHijo) !== 'null' && item.indicador == 'ERRPAR') {
                        $("#errParInt1" + (Number((indexHijo).replace('col', '')) - 1)).val(formato_numero(Math.abs(Number(itemHijo)), 2, ".", ",", "")).change();
                    }
                    if (itemHijo !== null && String(itemHijo) !== 'null' && item.indicador == 'ERRLC') {
                        $("#medLC1" + (Number((indexHijo).replace('col', '')) - 1)).val(formato_numero(Math.abs(Number(itemHijo)), 2, ".", ",", "")).change();
                    }
                    if (itemHijo !== null && String(itemHijo) !== 'null' && item.indicador == 'ERRK') {
                        $("#medEXTRA1" + (Number((indexHijo).replace('col', '')) - 1)).val(formato_numero(Math.abs(Number(itemHijo)), 2, ".", ",", "")).change();
                    }                    
                });               
            });



        }
        cargarIncertidumbre();
    } catch (error) {
        console.log("Ha ocurrido un error en [cargarCerificado]: " + error);
    }
}

function cargarIncertidumbre() {
    console.log("Data incertidumbre: ", dat_matrices_incert);
    try {
        $.each(dat_matrices_incert, function (index, item) {
            $.each(item, function (indexHijo, itemHijo) {
                var extencion = (item.indicador == 'PINCRTEXT' ? 'Ext' : (item.indicador == 'PINCRTINT') ? 'Int' : (item.indicador == 'PINCRTPSO') ? 'CalP' : (item.indicador == 'PINCRTPFD')? 'SP': 'Extra');
                if (itemHijo !== null && String(itemHijo) !== 'null' && item.indicador == 'PINCRTEXT' && item.fila == 0) {
                    if (Number((indexHijo).replace('col', '')) == 1) {
                        $("#resolInc" + extencion).val(formato_numero(Math.abs(Number(itemHijo)), 4, ".", ",", ""));
                    }
                    if (Number((indexHijo).replace('col', '')) == 2) {
                        $("#resolInc" + extencion + "Coef").val(formato_numero(Math.abs(Number(itemHijo)), 4, ".", ",", ""));                        
                    }
                    if (Number((indexHijo).replace('col', '')) == 4) {
                        $("#resolInc" + extencion + "Contri").val(formato_numero(Math.abs(Number(itemHijo)), 4, ".", ",", ""));
                    }
                }

                if (itemHijo !== null && String(itemHijo) !== 'null' && item.fila == 1) {
                    if (Number((indexHijo).replace('col', '')) == 1) {
                        $("#repetInc" + extencion).val(formato_numero(Math.abs(Number(itemHijo)), 4, ".", ",", ""));
                    }
                    if (Number((indexHijo).replace('col', '')) == 2) {
                        $("#repetInc" + extencion + "Coef").val(formato_numero(Math.abs(Number(itemHijo)), 4, ".", ",", ""));
                    }
                    if (Number((indexHijo).replace('col', '')) == 4) {
                        $("#repetInc" + extencion + "Contri").val(formato_numero(Math.abs(Number(itemHijo)), 4, ".", ",", ""));
                    }
                }

                if (itemHijo !== null && String(itemHijo) !== 'null' && item.fila == 2) {
                    if (Number((indexHijo).replace('col', '')) == 1) {
                        $("#errParaInc" + extencion).val(formato_numero(Math.abs(Number(itemHijo)), 4, ".", ",", ""));
                    }
                    if (Number((indexHijo).replace('col', '')) == 2) {
                        $("#errParaInc" + extencion + "Coef").val(formato_numero(Math.abs(Number(itemHijo)), 4, ".", ",", ""));
                    }
                    if (Number((indexHijo).replace('col', '')) == 4) {
                        $("#errParaInc" + extencion + "Contri").val(formato_numero(Math.abs(Number(itemHijo)), 4, ".", ",", ""));
                    }
                }

                if (itemHijo !== null && String(itemHijo) !== 'null' && item.fila == 3) {
                    if (Number((indexHijo).replace('col', '')) == 1) {
                        $("#errAbb" + extencion).val(formato_numero(Math.abs(Number(itemHijo)), 4, ".", ",", ""));
                    }
                    if (Number((indexHijo).replace('col', '')) == 2) {
                        $("#errAbb" + extencion + "Coef").val(formato_numero(Math.abs(Number(itemHijo)), 4, ".", ",", ""));
                    }
                    if (Number((indexHijo).replace('col', '')) == 4) {
                        $("#errAbb" + extencion + "Contri").val(formato_numero(Math.abs(Number(itemHijo)), 4, ".", ",", ""));
                    }
                }

                if (itemHijo !== null && String(itemHijo) !== 'null' && item.fila == 4) {
                    if (Number((indexHijo).replace('col', '')) == 1) {
                        $("#errLin"+ extencion).val(formato_numero(Math.abs(Number(itemHijo)), 4, ".", ",", ""));
                    }
                    if (Number((indexHijo).replace('col', '')) == 2) {
                        $("#errLin" + extencion + "Coef").val(formato_numero(Math.abs(Number(itemHijo)), 4, ".", ",", ""));
                    }
                    if (Number((indexHijo).replace('col', '')) == 4) {
                        $("#errLin" + extencion + "Contri").val(formato_numero(Math.abs(Number(itemHijo)), 4, ".", ",", ""));
                    }
                }

                if (itemHijo !== null && String(itemHijo) !== 'null' && item.fila == 5) {
                    if (Number((indexHijo).replace('col', '')) == 1) {
                        $("#incertPatr" + extencion).val(formato_numero(Math.abs(Number(itemHijo)), 4, ".", ",", ""));
                    }
                    if (Number((indexHijo).replace('col', '')) == 2) {
                        $("#incertPatr" + extencion + "Coef").val(formato_numero((Number(itemHijo)), 4, ".", ",", ""));
                    }
                    if (Number((indexHijo).replace('col', '')) == 4) {
                        $("#incertPatr" + extencion + "Contri").val(formato_numero((Number(itemHijo)), 4, ".", ",", ""));
                    }
                }

                if (itemHijo !== null && String(itemHijo) !== 'null' && item.fila == 6) {
                    if (Number((indexHijo).replace('col', '')) == 1) {
                        $("#coeDil" + extencion).val(formato_numero(Math.abs(Number(itemHijo)), 4, ".", ",", ""));
                    }
                    if (Number((indexHijo).replace('col', '')) == 2) {
                        $("#coeDil" + extencion + "Coef").val(formato_numero(Math.abs(Number(itemHijo)), 4, ".", ",", ""));
                    }
                    if (Number((indexHijo).replace('col', '')) == 4) {
                        $("#coeDil" + extencion + "Contri").val(formato_numero(Math.abs(Number(itemHijo)), 4, ".", ",", ""));
                    }
                }

                if (itemHijo !== null && String(itemHijo) !== 'null' && item.fila == 7) {
                    if (Number((indexHijo).replace('col', '')) == 1) {
                        $("#difTemp" + extencion).val(formato_numero(Math.abs(Number(itemHijo)), 4, ".", ",", ""));
                    }
                    if (Number((indexHijo).replace('col', '')) == 2) {
                        $("#difTemp" + extencion + "Coef").val(formato_numero(Math.abs(Number(itemHijo)), 4, ".", ",", ""));
                    }
                    if (Number((indexHijo).replace('col', '')) == 4) {
                        $("#difTemp" + extencion + "Contri").val(formato_numero(Math.abs(Number(itemHijo)), 4, ".", ",", ""));
                    }
                }

                if (itemHijo !== null && String(itemHijo) !== 'null' && item.fila == 8) {
                    if (Number((indexHijo).replace('col', '')) == 1) {
                        $("#incertComb" + extencion).val(formato_numero(Math.abs(Number(itemHijo)), 4, ".", ",", ""));
                    }
                }

                if (itemHijo !== null && String(itemHijo) !== 'null' && item.fila == 9) {
                    if (Number((indexHijo).replace('col', '')) == 1) {
                        $("#graEfec" + extencion).val(formato_numero(Math.abs(Number(itemHijo)), 4, ".", ",", ""));
                    }
                }

                if (itemHijo !== null && String(itemHijo) !== 'null' && item.fila == 10) {
                    if (Number((indexHijo).replace('col', '')) == 1) {
                        $("#factCober" + extencion).val(formato_numero(Math.abs(Number(itemHijo)), 4, ".", ",", ""));
                    }
                }

                if (itemHijo !== null && String(itemHijo) !== 'null' && item.fila == 11) {
                    if (Number((indexHijo).replace('col', '')) == 1) {
                        $("#uExp" + extencion).val(formato_numero(Math.abs(Number(itemHijo)), 4, ".", ",", ""));
                    }
                }
            });
        });
    } catch (error) {
        console.log("Ha ocurrido un error en [cargarIncertidumbre]: "+ error);
    }
}

function EliminarTemporal() {
    localStorage.setItem(NumCertificado + "-" + $('#IngresoLong').val() + "-" + $('#Revision').val(), []);
}