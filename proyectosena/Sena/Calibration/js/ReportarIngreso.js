﻿var Ingreso = 0;
var IdReporte = 0;
var ErrorEnter = 0;
var Fotos = 0;
var NroReporte = 1;
var CorreoEmpresa = "";
var Empresa = "";
var InformeTecnico = 0;

var Asesor = "";
var Habitual = "NO";
var AprobarCotizacion = "SI";
var Sitio;
var Convenio = 0;
var Garantia;
var FechaAproCoti = "";
var Cotizacion = 0;
var EstadoCotizacion = "";

$("#ClienteIngreso").html(CargarCombo(9, 1));

localStorage.setItem("Ingreso", "0");
var PermisioEspecial = PerGeneSistema("PERMISO ADMINISTRADOR LABORATORIO");

function LimpiarTodo() {
    LimpiarDatos();
    $("#IngresoReporte").val("");
}

function DescargarExcel(id, tipo) {
    window.open(url_cliente + "PlantillasCertificados/" + id + tipo);
}

function LimpiarDatos() {
    Ingreso = 0;
    IdReporte = 0;
    Foto = 0;
    $("#Equipo").val("");
    $("#Marca").val("");
    $("#Modelo").val("");
    $("#Serie").val("");
    $("#Magnitud").val("");
    $("#Observacion").val("");
    $("#Remision").val("");
    $("#Rango").val("");
    localStorage.setItem("Ingreso", "0");
    $("#Plantilla").html("");
    $(".tabbable").addClass("hidden");
    ErrorEnter = 0;

    Cotizacion = 0;
    EstadoCotizacion = "";

    $("#Usuario").val("");
    $("#FechaIng").val("");
    $("#Tiempo").val("");

    $("#Sitio").val("");
    $("#Garantia").val("")
    $("#Accesorio").val("");

    $("#ObservRecep").val("");
    $("#Metodo").val("");
    $("#Punto").val("");
    $("#Servicio").val("");
    $("#ObservCerti").val("");

    $("#Proxima").val("");
    $("#Entrega").val("");
    $("#AsesorComer").val("");

    for (var x = 1; x <= 5; x++) {
        $("#IdReporte" + x).val("0");
        $("#Observaciones" + x).val("");
        $("#Conclusion" + x).val("");
        $("#Ajuste" + x).val("");
        $("#Suministro" + x).val("");
        $("#registroingreso" + x).html("");
        $("#Concepto" + x).val("").trigger("change");
    }
}

/*$("#TablaReportar > tbody").on("dblclick", "tr", function () {
    var row = $(this).parents("td").context.cells;
    var numero = row[1].innerText;
    $("#IngresoReporte").val(numero);
    BuscarIngreso(numero, 0, "0");
});*/

function LlamarFotoDet(ingreso, fotos) {
    if (fotos == 0)
        return false
    CargarModalFoto(ingreso, fotos, 1);
}



function ConsularReportesIngresos() {

    var cliente = $("#ClienteIngreso").val() * 1;
    var tipo = $("#TipoTabla").val() * 1;

    ActivarLoad();
    setTimeout(function () {
        var parametros = "cliente=" + cliente + "&tipobusqueda=" + tipo;
        var datos = LlamarAjax("Laboratorio","opcion=TablaReportarIngreso&" + parametros);
        DesactivarLoad();
        var datajson = JSON.parse(datos);
        $('#TablaReportar').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "fila" },
                { "data": "ingreso" },
                { "data": "equipo" },
                { "data": "plantilla" },
                { "data": "fecha" },
                { "data": "tiempo" }
            ],

            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy', 'colvis'
            ]
        });
    }, 15);
}

ConsularReportesIngresos();

setTimeout(function () {
    ConsularReportesIngresos();
}, 400000);



$('#IngresoReporte').keyup(function (event) {
    numero = this.value * 1;
    if (numero == -1)
        numero = Ingreso;


    if ((Ingreso != (numero * 1)) && ($("#Equipo").val() != "")) {
        LimpiarDatos();
        Ingreso = 0;
        ErrorEnter = 0;
    }
    if (event.which == 13) {
        event.preventDefault();

        if (Ingreso == numero)
            return false;
        BuscarIngreso(numero);
    }
});



function BuscarIngreso(numero) {
    numero = numero * 1;
    if (ErrorEnter == 0) {
        if (numero != 0) {
            var datos = LlamarAjax("Laboratorio","opcion=BuscarIngresoReport&ingreso=" + numero).split("|");
            if (datos[0] != "[]") {
                var data = JSON.parse(datos[0]);
                IdRemision = data[0].id * 1;
                if (data[0].idmagnitud * 1 == 6) {
                    $("#IngresoReporte").select();
                    $("#IngresoReporte").focus();
                    ErrorEnter = 1;
                    swal("Acción Cancelada", "Debe reportar los ingresos por la opción <br><b>OPERACIONES PREVIAS <br>PAR TORSIONAL</b>", "warning");
                    return false;
                }
                /*if (data[0].idmagnitud * 1 == 2) {
                    $("#IngresoReporte").select();
                    $("#IngresoReporte").focus();
                    ErrorEnter = 1;
                    swal("Acción Cancelada", "Debe reportar los ingresos por la opción <br><b>OPERACIONES PREVIAS <br>PRESION</b>", "warning");
                    return false;
                }*/
                Ingreso = data[0].ingreso * 1;
                localStorage.setItem("Ingreso", Ingreso);
                Fotos = data[0].fotos * 1;

                Asesor = "/" + data[0].asesor + "/";
                Sitio = data[0].sitio;
                Convenio = data[0].convenio * 1;
                Garantia = data[0].garantia;
                Habitual = data[0].habitual;
                FechaAproCoti = $.trim(data[0].fechaaprocoti);
                AprobarCotizacion = data[0].aprobar_cotizacion;
                InformeTecnico = data[0].informe;

                $("#Equipo").val(data[0].equipo);
                $("#Marca").val(data[0].marca);
                $("#Modelo").val(data[0].modelo);
                $("#Serie").val(data[0].serie);
                $("#Observacion").val(data[0].observacion);
                $("#Rango").val(data[0].intervalo);
                $("#Remision").val(data[0].remision);
                $("#Magnitud").val(data[0].magnitud);

                $("#ObservRecep").val(data[0].observaesp);

                $("#Sitio").val(data[0].sitio);
                $("#Garantia").val(data[0].garantia);
                $("#Accesorio").val(data[0].accesorio);

                $("#Usuario").val(data[0].usuarioing);
                $("#FechaIng").val(data[0].fechaing);
                $("#Tiempo").val(data[0].tiempo);

                if (datos[1] != "[]") {
                    var dataco = JSON.parse(datos[1]);
                    Cotizacion = dataco[0].cotizacion * 1;
                    EstadoCotizacion = dataco[0].estado;
                    for (var x = 0; x < dataco.length; x++) {
                        if (x == 0) {
                            $("#Metodo").val(dataco[x].metodo);
                            $("#Punto").val(dataco[x].punto);
                            $("#Servicio").val(dataco[x].servicio);
                            $("#ObservCerti").val(dataco[x].observacion);
                            $("#Proxima").val(dataco[x].proxima);
                            $("#Entrega").val(dataco[x].entrega);
                            $("#AsesorComer").val(dataco[x].asesor);
                        } else {
                            $("#Metodo").val($("#Metodo").val() + "\n" + dataco[x].metodo);
                            $("#Punto").val($("#Punto").val() + "\n" + dataco[x].punto);
                            $("#Servicio").val($("#Servicio").val() + "\n" + dataco[x].servicio);
                            $("#ObservCerti").val($("#ObservCerti").val() + "\n" + dataco[x].observacion);
                            $("#Proxima").val($("#Proxima").val() + "\n" + dataco[x].proxima);
                            $("#Entrega").val($("#Entrega").val() + "\n" + dataco[x].entrega);
                            $("#AsesorComer").val($("#AsesorComer").val() + "\n" + dataco[x].asesor);
                        }
                    }
                }

                $('#tabreportaringreso a[href="#Reporte1"]').tab('show')
                NroReporte = 1;

                $("#Plantilla").html(datos[2]);
                $("#Plantilla").focus();

            } else {
                $("#IngresoReporte").select();
                $("#IngresoReporte").focus();
                ErrorEnter = 1;
                swal("Acción Cancelada", "El Ingreso número " + numero + " no se ha recibido en el laboratorio", "warning");
                return false;
            }
        }
    } else
        ErrorEnter = 0;
}

$("#Plantilla").change(function (event) {
	if (Ingreso == 0)
		return false;
	CambioPlantilla(this.value);
});


function CambioPlantilla(plantilla) {
    if (Ingreso == 0)
        return false;

    $(".tabbable").addClass("hidden");

    for (var x = 1; x <= 5; x++) {
        $("#IdReporte" + x).val("0");
        $("#Observaciones" + x).val("");
        $("#Conclusion" + x).val("");
        $("#Ajuste" + x).val("");
        $("#Suministro" + x).val("");
        $("#registroingreso" + x).html("");
        $("#Concepto" + x).val("").trigger("change");
        $("#NroInforme" + x).val("");
    }

    if (plantilla * 1 == 0)
        return false;


    var datos = LlamarAjax("Laboratorio","opcion=PlantillaReportes&ingreso=" + Ingreso + "&plantilla=" + plantilla).split("||");

    if (datos[1] != "[]") {
        var data = JSON.parse(datos[1]);

        var tecnico = data[0].tecnico * 1;
        var usuario = data[0].usuario * 1;
        var cantidad = data[0].cantidad * 1;

        if (tecnico != usuario && tecnico != -1) {
            $("#Plantilla").focus();
            $("#Plantilla").val("").trigger("change");
            swal("Acción Cancelada", "Usted no tiene asignado este ingreso", "warning");
            return false;
        }

        if (tecnico == -1 && cantidad > 0) {
            $("#Plantilla").focus();
            $("#Plantilla").val("").trigger("change");
            swal("Acción Cancelada", "Usted tiene " + cantidad + " ingreso(s) pendiente(s) por reportar", "warning");
            return false;
        }

    }

    $(".tabbable").removeClass("hidden");


    if (datos[0] != "[]") {
        var datarep = JSON.parse(datos[0]);
        for (var x = 0; x < datarep.length; x++) {
            IdReporte = datarep[x].nroreporte * 1;

            $("#IdReporte" + IdReporte).val(datarep[x].id);
            $("#Concepto" + IdReporte).val(datarep[x].idconcepto).trigger("change");
            $("#Ajuste" + IdReporte).val(datarep[x].ajuste);
            $("#Suministro" + IdReporte).val(datarep[x].suministro);
            $("#Observaciones" + IdReporte).val(datarep[x].observaciones);
            $("#Conclusion" + IdReporte).val(datarep[x].conclusion);
            $("#NroInforme" + IdReporte).val(InformeTecnico);
            $("#registroingreso" + IdReporte).html("<b>Registrado el día " + datarep[x].fecha + " por el técnico " + datarep[x].usuario + "</b>" + datarep[x].aprobado);
            if (datarep[x].idconcepto == 5 || datarep[x].idconcepto == 3)
                MostrarFoto(IdReporte, 1)
        }
        ErrorEnter = 1;
        $("#Plantilla").focus();
        swal("Acción Cancelada", "El ingreso número " + Ingreso + " ya ha sido reportado", "warning");
    } else {
        if (Habitual == "NO" && AprobarCotizacion == "SI" && Sitio == "NO" && Garantia == "NO" && Convenio == 0 && FechaAproCoti == "") {
            if (Cotizacion > 0) {
                if (EstadoCotizacion != "Aprobado" && EstadoCotizacion != "POR REEMPLAZAR") {
                    ErrorEnter = 1;
                    $("#Plantilla").val("").trigger("change");
                    $("#Plantilla").focus();
                    sendMessage(3, "Buen días amigo. se requiere la aprobación de la cotización número " + Cotizacion + " para proceder a la calibración", Asesor);
                    swal("Acción Cancelada", "La cotizacíon número " + Cotizacion + " no ha sido aprobada por el cliente", "warning");
                    if (PermisioEspecial == 0) {
                        LimpiarDatos();
                        return false;
                    }
                }
            } else {
                $("#Plantilla").val("").trigger("change");
                $("#Plantilla").focus();
                ErrorEnter = 1;
                sendMessage(3, "Buen días amigo. se requiere la cotización y aprobación del ingreso número " + Ingreso + " para proceder a la calibración", Asesor);
                swal("Acción Cancelada", "El Ingreso número " + numero + " no ha sido cotizado en comercial", "warning");
                if (PermisioEspecial == 0) {
                    LimpiarDatos();
                    return false;
                }
            }
        }
    }
    $('#tabreportaringreso a[href="#Reporte1"]').tab('show')
    NroReporte = 1;
    $("#Concepto1").focus();
}

function EnviarReporte(reporte) {

    var idreporte = $("#IdReporte" + reporte).val() * 1;

    if (Ingreso == 0)
        return false;

    if (idreporte == 0) {
        swal("Acción Cancelada", "Debe primero guardar el reporte", "warning");
        return false;
    }

    var concepto = $("#Concepto" + reporte).val() * 1;
    if (concepto != 2) {
        swal("Acción Cancelada", "Solo se podrá enviar reportes técnicos cuando el ingreso requiere ajuste o suministro", "warning");
        return false;
    }

    var datos = LlamarAjax('Cotizacion','opcion=ModalEmpresa&ingreso=' + Ingreso);
    var data = JSON.parse(datos);

    CorreoEmpresa = data[0].email;
    Empresa = data[0].cliente;

    if (ValidarCorreo(CorreoEmpresa) == 2) {
        swal("Acción Cancela", "el contacto " + data[0].contacto + " no posee correo válido registrado <br>Llame a comercial para registrar el correo del contacto", "warning");
        return false;
    }
    $("#eCliente").val(Empresa);
    $("#eCorreo").val(CorreoEmpresa);

    $("#ObserEnvio").val("");
    $("#spreporte").html(Ingreso);
    $("#eReporte").val(reporte);
    $("#enviar_reporte").modal("show");
}

$("#formenviarreporte").submit(function (e) {

    e.preventDefault();
    var observacion = $.trim($("#ObserEnvio").val())
    var correo = $.trim($("#eCorreo").val());
    a_correo = correo.split(";");
    var reporte = $("#eReporte").val() * 1;
    var plantilla = $("#Plantilla").val() * 1;

    for (var x = 0; x < a_correo.length; x++) {
        if (ValidarCorreo($.trim(a_correo[x])) == 2) {
            $("#eCorreo").focus();
            swal(ValidarTraduccion("Acción Cancelada"), ValidarTraduccion("Formato de correo inválido") + ":" + a_correo[x], "warning");
            return false;
        }
    }

    ActivarLoad();
    setTimeout(function () {
        var parametros = "ingreso=" + Ingreso + "&reporte=" + reporte + "&principal=" + CorreoEmpresa + " &e_email=" + correo + " &cliente=" + Empresa + " &observacion=" + observacion + "&plantilla=" + plantilla;
        var datos = LlamarAjax("Laboratorio","opcion=EnvioReporte&" + parametros);
        DesactivarLoad();
        datos = datos.split("|");
        if (datos[0] == "0") {
            swal("", ValidarTraduccion(datos[1]) + " " + datos[2], "success");
            $("#enviar_reporte").modal("hide");
        } else
            swal("Acción Cancelada", datos[1], "warning");
    }, 15);

    return false;
});

function EliminarReporte(reporte){
	var idreporte = $("#IdReporte" + reporte).val() * 1;
    var plantilla = $("#Plantilla").val() * 1;
    var fecha = $("#FechaIng").val();

    if (Ingreso == 0)
        return false;

    if (idreporte == 0) {
        return false;
    }

    var mensaje = '¿Seguro que desea eliminar el reporte número ' + reporte + ' del ingreso número ' +  Ingreso + '?';
    swal({
		title: 'Advertencia',
		text: mensaje,
		input: 'text',
		showLoaderOnConfirm: true,
		showCancelButton: true,
		confirmButtonText: ValidarTraduccion('Eliminar'),
		cancelButtonText: ValidarTraduccion('Cancelar'),
		confirmButtonClass: 'btn btn-success',
		cancelButtonClass: 'btn btn-danger',

		inputValidator: function (value) {
			return new Promise(function (resolve, reject) {
				if (value) {
					var datos = LlamarAjax("Laboratorio","opcion=ElimarReporteIngreso&ingreso=" + Ingreso + "&observaciones=" + value + "&reporte=" + reporte + "&plantilla=" + plantilla + "&fecha=" + fecha + "&idreporte=" + idreporte)
					datos = datos.split("|");
					if (datos[0] == "0") {
						resultado = datos[1];
						resolve()
					} else {
						reject(datos[1]);
					}

				} else {
					reject(ValidarTraduccion('Debe de ingresar un observación'));
				}
			})
		}
	}).then(function (result) {
					
		swal({
			type: 'success',
			html: 'Reporte número ' + reporte + ' eliminado'
		});
		$("#Concepto" + reporte).val("").trigger("change");
		$("#Suministro" + reporte).prop("readonly", true);
		$("#Ajuste" + reporte).prop("readonly", true);
		$("#Observaciones" + reporte).prop("readonly", false);
		$("#Conclusion" + reporte).prop("readonly", true);

		$("#Observaciones" + reporte).addClass("bg-amarillo");
		$("#Ajuste" + reporte).removeClass("bg-amarillo");
		$("#Suministro" + reporte).removeClass("bg-amarillo");
		$("#Conclusion" + reporte).removeClass("bg-amarillo");
		
		$("#Observaciones" + reporte).val("");
		$("#Ajuste" + reporte).val("");
		$("#Suministro" + reporte).val("");
		$("#Conclusion" + reporte).val("");
		
	});
}


function GenerarInforme(reporte) {

    var idreporte = $("#IdReporte" + reporte).val() * 1;
    var plantilla = $("#Plantilla").val() * 1;

    if (Ingreso == 0)
        return false;

    if (idreporte == 0) {
        swal("Acción Cancelada", "Debe primero guardar el reporte", "warning");
        return false;
    }

    var concepto = $("#Concepto" + reporte).val() * 1;
    if (concepto != 3 && concepto != 5) {
        swal("Acción Cancelada", "Solo se podrá generar informes cuando el ingreso no está apto para calibrar ó para mantenimiento y limpieza", "warning");
        return false;
    }

    var mensaje = '¿Seguro que desea generar el informe técnico del ingreso número ' + Ingreso + '?';
    if (concepto == 5)
        mensaje = '¿Seguro que desea generar el informe de mantenimiento y limpieza del ingreso número ' + Ingreso + '?';


    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: mensaje,
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Generar Informe'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                $.post(url_servidor + "Laboratorio","opcion=InformeTecnicoDev&ingreso=" + Ingreso + "&reporte=" + reporte + "&plantilla=" + plantilla)
                    .done(function (data) {
                        datos = data.split("|");
                        if (datos[0] == "0") {
                            $("#NroInforme" + reporte).val(datos[2]);
                            ImprimirInforme(reporte);
                            resolve()
                        } else {
                            reject(datos[1]);
                        }
                    })
            })
        }
    }]);
};

function VistaInforme(reporte) {

    if (Ingreso == 0)
        return false;
	ImprimirInforme(reporte);
}

function ImprimirInforme(reporte) {

    if (Ingreso == 0)
        return false;
        
	var plantilla = $("#Plantilla").val() * 1;
    
    ActivarLoad();
	setTimeout(function () {
		var datos = LlamarAjax("Configuracion", "opcion=GenerarPDF&tipo=InformeTecnico&documento=0&ingreso=" + Ingreso + "&plantilla=" + plantilla).split("||");
		DesactivarLoad();
		if (datos[0] == "0") {
			window.open(url_cliente + "DocumPDF/" + datos[1]);
		} else {
			swal("Acción Cancelada", ValidarTraduccion(datos[1]), "warning");
		}
		
	}, 15);
}


function CambioConcepto(concepto, reporte) {
    $("#Suministro" + reporte).prop("readonly", true);
    $("#Ajuste" + reporte).prop("readonly", true);
    $("#Observaciones" + reporte).prop("readonly", false);
    $("#Conclusion" + reporte).prop("readonly", true);

    $("#Observaciones" + reporte).addClass("bg-amarillo");
    $("#Ajuste" + reporte).removeClass("bg-amarillo");
    $("#Suministro" + reporte).removeClass("bg-amarillo");
    $("#Conclusion" + reporte).removeClass("bg-amarillo");

    $("#Documento" + reporte).removeClass("bg-amarillo");
    $("#NumFoto" + reporte).removeClass("bg-amarillo");
    $("#Documento" + reporte).prop("disabled", true);
    $("#NumFoto" + reporte).prop("disabled", true);

    switch (concepto){
		case "2":
			$("#Suministro" + reporte).prop("readonly", false);
			$("#Ajuste" + reporte).prop("readonly", false);
			$("#Observaciones" + reporte).prop("readonly", true);
			$("#Conclusion" + reporte).prop("readonly", true);

			$("#Ajuste" + reporte).addClass("bg-amarillo");
			$("#Suministro" + reporte).addClass("bg-amarillo");
			$("#Conclusion" + reporte).removeClass("bg-amarillo");
			$("#Observaciones" + reporte).removeClass("bg-amarillo");
			break;
		case "3":
		case "5":
            $("#Observaciones" + reporte).prop("readonly", false);
            $("#Conclusion" + reporte).prop("readonly", false);

            $("#Observaciones" + reporte).addClass("bg-amarillo");
            $("#Conclusion" + reporte).addClass("bg-amarillo");

            $("#Documento" + reporte).addClass("bg-amarillo");
            $("#NumFoto" + reporte).removeClass("bg-amarillo");
            $("#Documento" + reporte).prop("disabled", false);
            $("#NumFoto" + reporte).prop("disabled", false);
			break;
		case "8":
			$("#Suministro" + reporte).prop("readonly", true);
			$("#Ajuste" + reporte).prop("readonly", false);
			$("#Observaciones" + reporte).prop("readonly", true);
			$("#Conclusion" + reporte).prop("readonly", true);

			$("#Ajuste" + reporte).addClass("bg-amarillo");
			$("#Suministro" + reporte).removeClass("bg-amarillo");
			$("#Conclusion" + reporte).removeClass("bg-amarillo");
			$("#Observaciones" + reporte).removeClass("bg-amarillo");
			break;
		case "7":
			$("#Suministro" + reporte).prop("readonly", false);
			$("#Ajuste" + reporte).prop("readonly", true);
			$("#Observaciones" + reporte).prop("readonly", true);
			$("#Conclusion" + reporte).prop("readonly", true);

			$("#Ajuste" + reporte).removeClass("bg-amarillo");
			$("#Suministro" + reporte).addClass("bg-amarillo");
			$("#Conclusion" + reporte).removeClass("bg-amarillo");
			$("#Observaciones" + reporte).removeClass("bg-amarillo");
			break;
		
    }
}


$(".guardarreporte").click(function () {
	var reporte = NroReporte;
	var plantilla = $("#Plantilla").val() * 1;
	var IdReporte = $("#IdReporte" + reporte).val() * 1;
	var idconcepto = $("#Concepto" + reporte).val() * 1;
	var ajuste = $.trim($("#Ajuste" + reporte).val());
	var suministro = $.trim($("#Suministro" + reporte).val());
	var observacion = $.trim($("#Observaciones" + reporte).val());
	var conclusion = $.trim($("#Conclusion" + reporte).val());
	var concepto = $("#Concepto" + reporte + " option:selected").text();
	var fecha = $("#FechaIng").val();
	if (Ingreso == 0)
		return false;
	var mensaje = "";

	if ((idconcepto == 3 || idconcepto == 4) && observacion == "") {
		$("#Observaciones" + reporte).focus();
		mensaje = "<br> Debe de ingresar la observación del por qué no se puede calibrar";
	}
	if (idconcepto == 3 && conclusion == "") {
		$("#Conclusion" + reporte).focus();
		mensaje = "<br> Debe de ingresar la conclusión del por qué no se puede calibrar" + mensaje;
	}
	if (idconcepto == 5 && conclusion == "") {
		$("#Conclusion" + reporte).focus();
		mensaje = "<br> Debe de ingresar la conclusión del informe de mantenimiento" + mensaje;
	}
	if (idconcepto == 2 && ajuste == "" && suministro == "") {
		$("#Ajuste" + reporte).focus();
		mensaje = "<br> Debe de ingresar la observación del juste o suministro" + mensaje;
	}
	if (idconcepto == 0) {
		$("#Concepto" + reporte).focus()
		mensaje = "<br> Debe de seleccionar un concepto " + mensaje;
	}

	if (mensaje != "") {
		swal("Acción Cancelada", mensaje, "warning");
		return false;
	}

	var parametros = "plantilla=" + plantilla + "&ingreso=" + Ingreso + "&idconcepto=" + idconcepto + "&concepto=" + concepto + "&ajuste=" + ajuste +
		"&suministro=" + suministro + "&id=" + IdReporte + "&observacion=" + observacion + "&reporte=" + reporte + "&fecha=" + fecha + "&conclusion=" + conclusion;
	var datos = LlamarAjax("Laboratorio","opcion=ReportarIngreso&" + parametros).split("|");
	if (datos[0] == "0") {
		
		ConsularReportesIngresos();
		$("#IdReporte" + reporte).val(datos[2]);
		swal("", datos[1], "success");
		localStorage.setItem("IngresoSocker",Ingreso);
		mensaje = "El ingreso número " + Ingreso + " ha sido reportado en el laboratorio en las siguientes condiciones: <b>" + concepto + "</b>." + 
				 (ajuste != "" ? "<br><b>Ajuste: </b>" + ajuste + "." : "") +  
				 (suministro != "" ? "<br><b>Suministro: </b>" + suministro + "." : "") +  
				 (observacion != "" ? "<br><b>observacion/Informe: </b>" + observacion + "." : "") +  
				 (conclusion != "" ? "<br><b>Conclusiones:</b>" + conclusion + "." : "") +  
				 "<br><b>Cliente: </b>" + datos[3] + "." + 
				 "<br><b>Asesor Comercial: </b>" + datos[4] + "." +  
				 "<br><b>Cliente Habitual: </b>" + datos[5];
		sendMessage(4,mensaje);
	} else {
		swal("Acción Cancelada", datos[1], "warning");
	}
});


function VerFotos() {
    if (Ingreso == 0)
        return false;
    CargarModalFoto(Ingreso, Fotos, 1);
}

function MostrarFoto(reporte, numero) {
    var informe = $("#NroInforme" + reporte).val() * 1;
    $("#FotoMan" + reporte).attr("src", url_cliente + "imagenes/informemantenimineto/" + informe + "/" + numero + ".jpg");
}


function GuardarDocumento(reporte, id) {
    var documento = document.getElementById("Documento" + reporte);
    var informe = $("#NroInforme" + reporte).val() * 1;
    var numero = $("#NumFoto" + reporte).val() * 1;


    if ($.trim(documento.value) != "") {

        if (informe == 0) {
            swal("Acción Cancelada", "Debe generar primero el informe técnico", "warning");
            return false;
        }
        
        var archivo = GuardarDocumentoAdjunto(id,4,'');
        

        var file = documento.files[0];
        var data = new FormData();
        data.append('documento', file);
        var resultado = '';
        var direccion = url + "Cotizacion/AdjuntarFoto";
        $.ajax({
            url: direccion,
            type: 'POST',
            contentType: false,
            data: data,
            processData: false,
            async: false,
            success: function (datos) {
                datos = datos.split("|");
                if (datos[0] == "0") {
                    datos = LlamarAjax("Laboratorio/SubirFoto", "ingreso=" + Ingreso + "&informe=" + informe + "&numero=" + numero + "&foto=" + datos[1]).split("|");
                    swal(datos[1]);
                    if (datos[0] == "0")
                        MostrarFoto(reporte, numero)
                } else {
                    swal(ValidarTraduccion(datos[1]), "", "error");
                }
            }
        });
    }
}

function HistorialReporteElimando() {
	
	if (Ingreso == 0)
		return false;
    
    ActivarLoad();
    setTimeout(function () {
        var datos = LlamarAjax("Laboratorio","opcion=TablaReporIngEliminados&ingreso=" + Ingreso);
        DesactivarLoad();
        
        if (datos == "[]"){
			swal("Acción Cancelada","No hay historial de reportes eliminados para este ingreso","warning");
			return false;
		}

        var datajson = JSON.parse(datos);
        $('#TablaReporteEliminado').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "reporte", "className": "text-XX" },
                { "data": "plantilla" },
                { "data": "concepto" },
                { "data": "ajuste" },
                { "data": "suministro"},
                { "data": "observacion"},
                { "data": "conclusion"},
                { "data": "reportado"},
                { "data": "eliminado"},
                { "data": "observacion_eliminacion"}
            ],


            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy', 'colvis'
            ],
            "lengthMenu": [[7], [7]],
        });
        $("#ModalReportesEliminados").modal("show");
    }, 15);

}




$('select').select2();
