﻿var dias = ['Domingo','Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'];
var meses = ['--', 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];

Date.prototype.getWeekNumber = function () {
    var d = new Date(+this);  //Creamos un nuevo Date con la Fecha de "this".
    d.setHours(0, 0, 0, 0);   //Nos aseguramos de limpiar la hora.
    d.setDate(d.getDate() + 4 - (d.getDay() || 7)); // Recorremos los días para asegurarnos de estar "dentro de la semana"
    //Finalmente, calculamos redondeando y ajustando por la naturaleza de los números en JS:
    return Math.ceil((((d - new Date(d.getFullYear(), 0, 1)) / 8.64e7) + 1) / 7);
};

var fecha_h = new Date();
var month_h = fecha_h.getMonth() + 1;
var day_h = fecha_h.getDate();
var Anio_h = fecha_h.getFullYear();
var DatosCalendarios = null;

var FechaActual = Anio_h + '-' +
    (month_h < 10 ? '0' : '') + month_h + '-' +
    (day_h < 10 ? '0' : '') + day_h;


$("#Vistas").html(CargarCombo(13, 4, "", "1"));
$("#Cliente, #ClienteAgenda, #CoCliente").html(CargarCombo(9, 7));

var Fecha = new Date();
Fecha.setHours(0, 0, 0, 0);
var month = Fecha.getMonth() + 1;
var day = Fecha.getDate();
var Anio = Fecha.getFullYear();
var TipoAgenda = 1;
var Semana = Fecha.getWeekNumber();
var SemanaDI = 0;
var SemanaDF = 0;
var FechaCab = null;
var FechaIniSem = null;

var FechaBDesde = null;
var FechaBHasta = null;

function CambiarMes(tipo) {

    switch (TipoAgenda) {
        case 1:
            if (tipo == 1) {
                if (month == 1) {
                    month = 12;
                    Anio = Anio - 1;
                } else
                    month = month - 1;
            } else {
                if (month == 12) {
                    month = 1;
                    Anio = Anio + 1;
                } else
                    month = month + 1;
            }

            if (Anio == Anio_h && month == month_h)
                day = day_h;
            else
                day = 1;

            Fecha = new Date(Anio + "/" + month + "/" + day);
            break;
        case 2:
            
            if (tipo == 1) {
                if (FechaIniSem != null) {
                    Fecha = new Date(FechaIniSem.getFullYear() + "/" + (FechaIniSem.getMonth() + 1) + "/" + FechaIniSem.getDate());
                    Fecha.setDate(Fecha.getDate() - 1);
                    month = Fecha.getMonth() + 1;
                    day = Fecha.getDate();
                    Anio = Fecha.getFullYear();
                }
            } else {
                if (FechaCab != null) {
                    Fecha = new Date(FechaCab.getFullYear() + "/" + (FechaCab.getMonth() + 1) + "/" + FechaCab.getDate());
                    Fecha.setDate(Fecha.getDate() + 1);
                    month = Fecha.getMonth() + 1;
                    day = Fecha.getDate();
                    Anio = Fecha.getFullYear();
                }
            }
            break;
        case 3:

            if (tipo == 1) {
                Fecha.setDate(Fecha.getDate() - 1);
                month = Fecha.getMonth() + 1;
                day = Fecha.getDate();
                Anio = Fecha.getFullYear();
            } else {
                Fecha.setDate(Fecha.getDate() + 1);
                month = Fecha.getMonth() + 1;
                day = Fecha.getDate();
                Anio = Fecha.getFullYear();
            }
            break;
            
    }
        
   
    CargarAgenda(TipoAgenda);

}

function Obtenerdatos() {
    var parametros = "fechad=" + FechaBDesde + "&fechah=" + FechaBHasta + "&cliente=" + ($("#ClienteAgenda").val()*1);
    DatosCalendarios = LlamarAjax("Configuracion","opcion=EventosCalendarios&" + parametros).split("||");
}

function BuscarDatos(fecha, hora) {
    var resultado = '<div class="fc-event-inner">';
    var span = "";
    var data = null;
    var fechac = null;
        
    if (hora == "") {
        if ($("#CCumpleanos").prop("checked")) {
            data = JSON.parse(DatosCalendarios[0]);
            fechac = Fecha_cumple(fecha);
            for (var x = 0; x < data.length; x++) {
                if ($.trim(data[x].fecha) == $.trim(fechac)) {
                    if (span != "")
                        span += "<br>";
                    span += "<a href='#' title='Cumpleaños... No se le olvide felicitar a su compañero(a).' class='fc-event-title bg-success'><i class='icon-happy'></i>" + data[x].evento + "</a>";
                }
            }
        }
    }

    if (hora == "") {
        if ($("#CCulminacion").prop("checked")) {
            data = JSON.parse(DatosCalendarios[1]);
            for (var x = 0; x < data.length; x++) {
                if ($.trim(data[x].fecha) == $.trim(fecha)) {
                    if (span != "")
                        span += "<br>";
                    span += "<a href='#' title='Culminación de contraro. Recuerde hablar con RR-HH.' class='fc-event-title bg-warning'><i class='icon-user-block'></i>" + data[x].evento + "</a>";
                }
            }
        }
    }

    if (hora == "") {
        if ($("#CFestivos").prop("checked")) {
            data = JSON.parse(DatosCalendarios[2]);
            for (var x = 0; x < data.length; x++) {
                if ($.trim(data[x].fecha) == $.trim(fecha)) {
                    if (span != "")
                        span += "<br>";
                    span += "<a title='Felicidades... Tienes un día no laboral.' class='fc-event-title bg-danger'><i class='icon-accessibility'></i>" + data[x].evento + "</a>";
                    break;
                }
            }
        }
    }


    data = JSON.parse(DatosCalendarios[3]);
    for (var x = 0; x < data.length; x++) {
        if ($("#CEventos").prop("checked") && data[x].tipo == "Evento") {
            if (hora == "" && (data[x].horainicio == "00:00" || TipoAgenda == 1)) {
                if ($.trim(fecha) >= $.trim(data[x].fechainicio) && $.trim(fecha) <= $.trim(data[x].fechafinal)) {
                    if (span != "")
                        span += "<br>";
                    span += "<a href='javascript:VerEvento(" + data[x].id + ")' title='" + data[x].descripcion + (data[x].idcliente > 0 ? "\nCliente: " + data[x].cliente : "") + (data[x].horainicio != "00:00" ? " \nHorario: " + data[x].horainicio + " - " + data[x].horafinal : "") + "' class='fc-event-title " + (data[x].tipo == "Evento" ? " bg-info " : " bg-primary ") + "'><i class='icon-clipboard'></i>" + data[x].evento + "</a>&nbsp;&nbsp;&nbsp" +
                        (data[x].usuario == data[x].idusuario ? "<button class='btn-primary' title='Editar " + data[x].tipo + "' type='button' onclick=\"EditarEvento(" + data[x].id + ")\"><span data-icon='&#xe0c0;'></span></button>" : "");
                    break;
                }
            } else {
                if (data[x].horainicio != "00:00") {
                    if ($.trim(fecha) >= $.trim(data[x].fechainicio) && $.trim(fecha) <= $.trim(data[x].fechafinal) && $.trim(hora) >= $.trim(data[x].horainicio) && $.trim(hora) <= $.trim(data[x].horafinal)) {
                        if (span != "")
                            span += "<br>";
                        span += "<a href='javascript:VerEvento(" + data[x].id + ")' title='" + data[x].descripcion + (data[x].idcliente > 0 ? "\nCliente: " + data[x].cliente : "") + (data[x].horainicio != "00:00" ? " \nHorario: " + data[x].horainicio + " - " + data[x].horafinal : "") + "' class='fc-event-title " + (data[x].tipo == "Evento" ? " bg-info " : " bg-primary ") + "'><i class='icon-clipboard'></i>" + data[x].evento + "</a>&nbsp;&nbsp;&nbsp" +
                            (data[x].usuario == data[x].idusuario ? "<button class='btn-primary' title='Editar " + data[x].tipo + "' type='button' onclick=\"EditarEvento(" + data[x].id + ")\"><span data-icon='&#xe0c0;'></span></button>" : "");
                        break;
                    }
                }
            }
        }
        
        if ($("#CActividades").prop("checked") && data[x].tipo == "Actividad") {
            if (hora == "" && (data[x].horainicio == "00:00" || TipoAgenda == 1)) {
                if ($.trim(fecha) >= $.trim(data[x].fechainicio) && $.trim(fecha) <= $.trim(data[x].fechafinal)) {
                    if (span != "")
                        span += "<br>";
                    span += "<a href='javascript:VerEvento(" + data[x].id + ")' title='" + data[x].descripcion + (data[x].idcliente > 0 ? "\nCliente: " + data[x].cliente : "") + (data[x].horainicio != "00:00" ? "\nHorario: " + data[x].horainicio + " - " + data[x].horafinal : "") + "' class='fc-event-title " + (data[x].tipo == "Evento" ? " bg-info " : " bg-primary ") + "'><i class='icon-clipboard'></i>" + data[x].evento + "</a>&nbsp;&nbsp;&nbsp" +
                        (data[x].usuario == data[x].idusuario ? "<button class='btn-primary' title='Editar " + data[x].tipo + "' type='button' onclick=\"EditarEvento(" + data[x].id + ")\"><span data-icon='&#xe0c0;'></span></button>" : "");
                    break;
                }
            } else {
                if (data[x].horainicio != "00:00") {
                    if ($.trim(fecha) >= $.trim(data[x].fechainicio) && $.trim(fecha) <= $.trim(data[x].fechafinal) && $.trim(hora) >= $.trim(data[x].horainicio) && $.trim(hora) <= $.trim(data[x].horafinal)) {
                        if (span != "")
                            span += "<br>";
                        span += "<a href='javascript:VerEvento(" + data[x].id + ")' title='" + data[x].descripcion + (data[x].idcliente > 0 ? "\nCliente: " + data[x].cliente : "") + (data[x].horainicio != "00:00" ? " \nHorario: " + data[x].horainicio + " - " + data[x].horafinal : "") + "' class='fc-event-title " + (data[x].tipo == "Evento" ? " bg-info " : " bg-primary ") + "'><i class='icon-clipboard'></i>" + data[x].evento + "</a>&nbsp;&nbsp;&nbsp" +
                            (data[x].usuario == data[x].idusuario ? "<button class='btn-primary' title='Editar " + data[x].tipo + "' type='button' onclick=\"EditarEvento(" + data[x].id + ")\"><span data-icon='&#xe0c0;'></span></button>" : "");
                        break;
                    }
                }
            }
        }
    }

    resultado += span + '</div>';

    return resultado;
}

function CargarAgenda(tipo) {
    var resultado = "";
    var dia = 0;

    TipoAgenda = tipo;

    for (var x = 1; x <= 3; x++) {
        if (tipo == x)
            $("#OpcionAgenda" + x).addClass("fc-state-active")
        else
            $("#OpcionAgenda" + x).removeClass("fc-state-active")
    }


    switch (tipo) {
        case 1:
            var semana = 0;
            var month_a = month;
            var Anio_a = Anio;
            var day_a = 1;

            if (month_a == 1) {
                month_a = 12;
                Anio_a = Anio_a - 1;
            } else
                month_a = month_a - 1;

            fechaant = new Date(Anio_a + "/" + month_a + "/" + day_a);

            var ultimoDia = new Date(Fecha.getFullYear(), Fecha.getMonth() + 1, 0).getDate();
            var ultimoDiaMA = new Date(fechaant.getFullYear(), fechaant.getMonth() + 1, 0).getDate();

            var fechadia = new Date(Anio + "/" + month + "/01");
            var diames = fechadia.getUTCDay();

            FechaBDesde = Fecha.getFullYear() + "-" + ((Fecha.getMonth() + 1)*1 < 10 ? "0" : "") + (Fecha.getMonth() + 1) + "-01";
            FechaBHasta = Fecha.getFullYear() + "-" + ((Fecha.getMonth() + 1) * 1 < 10 ? "0" : "") + (Fecha.getMonth() + 1) + "-" + ultimoDia;

            Obtenerdatos();
                
            $("#TituloCalendario").html(meses[month] + " " + Anio);

            resultado = "<table width='100%' class='table table-bordered'><thead><tr>";
            for (var x = 0; x <= 6; x++) {
                resultado += "<th width='14%'>" + dias[x] + "</th>";
            }

            var fechadia = '';
            var classactual = "";
            for (var x = (1 - diames); x <= ultimoDia; x++) {
                classactual = "";
                if (semana == 0 && dia == 0)
                    resultado += "<tr class='fc-week fc-first'>";
                if (semana == 7) {
                    resultado += "</tr><tr>";
                    semana = 0;
                }
                dia = x;
                if (x < 1) {
                    dia = ultimoDiaMA + x;
                    fechadia = Fecha_calendario(Anio_a, month_a, dia);
                } else
                    fechadia = Fecha_calendario(Anio, month, dia);

                if (fechadia == FechaActual)
                    classactual = "bg-amarilloclaro";

                resultado += "<td ondblclick =\"CrearEvento('" + fechadia + "','')\"" + (x <= 0 ? " style='opacity: 0.3'" : "") + ' class="fc-day fc-widget-content fc-future ' + classactual + (semana == 0 ? " bg-rojoclaro " : "") + '" > <div style="min-height: 112px;"><div class="fc-day-number text-right">' + dia + '</div>' + BuscarDatos(fechadia, "") + '</div></td > ';
                semana++;
            }
            break;
        case 2:

            var diames = Fecha.getUTCDay();
            FechaIniSem = new Date(Fecha.getFullYear() + "/" + (Fecha.getMonth() + 1) + "/" + Fecha.getDate());
            FechaIniSem.setDate(FechaIniSem.getDate() - diames);

            var month_ini = FechaIniSem.getMonth() + 1;
            var day_ini = FechaIniSem.getDate();
            var Anio_ini = FechaIniSem.getFullYear();

            var me = 0;
            var di = 0;
            var an = 0;

            FechaCab = new Date(FechaIniSem.getFullYear() + "/" + (FechaIniSem.getMonth() + 1) + "/" + FechaIniSem.getDate());
            FechaFin = new Date(FechaIniSem.getFullYear() + "/" + (FechaIniSem.getMonth() + 1) + "/" + FechaIniSem.getDate());
            FechaFin.setDate(FechaFin.getDate() + 6);

            FechaBDesde = FechaIniSem.getFullYear() + "-" + (FechaIniSem.getMonth() + 1) + "-" + FechaIniSem.getDate();
            FechaBHasta = FechaFin.getFullYear() + "-" + (FechaFin.getMonth() + 1) + "-" + FechaFin.getDate();

            Obtenerdatos();
            var classactual = "";
            resultado = "<table width='100%' class='table table-bordered'><thead><tr><th>&nbsp;</th>";
            var fechasemana = [];
            for (var x = 0; x <= 6; x++) {
                dm = FechaCab.getUTCDay();
                me = FechaCab.getMonth() + 1;
                di = FechaCab.getDate();
                an = FechaCab.getFullYear();
                fechasemana.push(Fecha_calendario(an, me, di));
                resultado += "<th width='14%'>" + dias[dm] + "<br>" + di + "/" + me + "/" + an + " </th>";
                if (x != 6)
                    FechaCab.setDate(FechaCab.getDate() + 1);
            }
            
            $("#TituloCalendario").html(meses[month_ini] + " " + day_ini + "/" + Anio_ini + " - " + meses[me] + " " + di + "/" + an);

            var hora2 = "";
            var hora = "";
            for (var x = 0; x <= 23; x++) {
                resultado += "<tr class='fc-week fc-first'>";
                hora = (x < 10 ? "0" : "") + x + ":00";
                hora2 = hora;
                if (x == 0) {
                    hora = "Todo el día";
                    hora2 = "";
                } 
                resultado += "<td>" + hora + "</td>";
                for (var y = 0; y <= 6; y++) {
                    classactual = "";
                    fechadia = fechasemana[y];
                    if (fechadia == FechaActual)
                        classactual = "class='bg-amarilloclaro'";
                    resultado += "<td " + classactual + " ondblclick =\"CrearEvento('" + fechadia + "','" + hora2 + "')\">" + BuscarDatos(fechadia, hora2) + "</td>";
                }
                resultado += "</tr>";
            }
            break;
        case 3:
            var classactual = "";
            dm = Fecha.getUTCDay();
            me = Fecha.getMonth() + 1;
            di = Fecha.getDate();
            an = Fecha.getFullYear();
            fechadia = Fecha_calendario(an, me, di);
            if (fechadia == FechaActual)
                classactual = "class='bg-amarilloclaro'";
            resultado = "<table width='100%' class='table table-bordered'><thead><tr>";
            resultado += "<th colspan='2' style='text-align:center'>" + dias[dm] + "<br>" + di + "/" + an + "</th>";
            
            $("#TituloCalendario").html(dias[dm] + "<br>" + di + " de " + meses[me] + " de " + an);

            var hora = "";
            var hora2 = "";
            for (var x = 0; x <= 23; x++) {
                resultado += "<tr class='fc-week fc-first'>";
                hora = (x < 10 ? "0" : "") + x + ":00";
                hora2 = hora;
                if (x == 0) {
                    hora = "Todo el día";
                    hora2 = "";
                } 
                resultado += "<td width='10%'>" + hora + "</td>";
                resultado += "<td " + classactual + " ondblclick =\"CrearEvento('" + fechadia + "','" + hora2 + "')\">" + BuscarDatos(fechadia, hora2) + "</td>";
                resultado += "</tr>";
            }
            break;
                        

    }
    resultado += "</thead></table>";
    $("#Calendario").html(resultado);
       
    
}

function VerEvento(id) {
    var datos = LlamarAjax("Configuracion","opcion=VerEvento&IdEvento=" + id);
    var data = JSON.parse(datos);

    $("#TID").html(id);
    $("#TRegistro").html(data[0].registro);
    $("#TTipo").html(data[0].tipo);
    $("#TEstado").html(data[0].estado);
    $("#TVista").html(data[0].empleados);
    $("#TTitulo").html(data[0].titulo);
    $("#TClientes").html(data[0].cliente);
    $("#TDescripcion").html(data[0].descripcion);
    $("#TFechaInicio").html(data[0].fechainicio + (data[0].horainicio == "00:00" ? "" : " " + data[0].horainicio));
    $("#TFechaFinal").html(data[0].fechafinal + (data[0].horafinal == "00:00" ? "" : " " + data[0].horafinal));
    $("#TFechaRecordatorio").html(data[0].fecharecordatorio);
    $("#TArchivo").html((data[0].archivo > 0 ? "<a href='javascript:VerArchivo(" + id + ")'>Ver Archivo</a>" : ""));

    if (data[0].visitas == "/0/")
        $("#btnagregartarea").removeClass("hidden");
    else {
        if ($.trim(data[0].visitas).indexOf("/" + data[0].usuario + "/") >= 0)
            $("#btnagregartarea").removeClass("hidden");
        else
            $("#btnagregartarea").addClass("hidden");
    }
    $("#ModalVerEvento").modal("show");
}

function AgregarTarea() {
    $("#IdTarea").val("0");
    $("#Tarea").val("");
    $("#IdEvenTarea").val($("#TID").html());
    $("#TituloTarea").html($("#TTitulo").html());

    var datos = LlamarAjax("Configuracion","opcion=VerTareas&Idevento=" + $("#IdEvenTarea").val());
    var datajson = JSON.parse(datos);
    $('#TablaEventoTarea').DataTable({
        data: datajson,
        bProcessing: true,
        bDestroy: true,
        paging: true,
        ordering: true,
        info: false,
        searching: false,
        columns: [
            { "data": "id" },
            { "data": "registro" },
            { "data": "tarea" },
            { "data": "eliminar" }
        ],
        "lengthMenu": [[8], [8]],
        "language": {
            "url": LenguajeDataTable
        }
    });

    $("#ModalTarea").modal("show");

}

$("#TablaEventoTarea> tbody").on("dblclick", "tr", function () {
    var row = $(this).parents("td").context.cells;
    $("#IdTarea").val(row[0].innerText);
    $("#Tarea").val(row[2].innerText);
});

function NuevaTarea() {
    $("#IdTarea").val("0");
    $("#Tarea").val("");
}

function EliminarTarea(id) {
        
    

    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: ValidarTraduccion('¿Seguro que desea eliminar esta tarea?'),
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Eliminar'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                $.post("Configuracion/EliminarTarea", "IdTarea=" + id)
                    .done(function (data) {
                        datos = data.split("|");
                        if (datos[0] == "0") {
                            AgregarTarea();
                            resolve()
                        } else {
                            reject(datos[1]);
                        }
                    })
            })
        }
    }]);
};

function GuardarTarea() {
    var idtarea = $("#IdTarea").val();
    var tarea = $.trim($("#Tarea").val());
    var id = $("#IdEvenTarea").val();

    if (tarea == "") {
        $("#Tarea").focus()
        swal("Acción Cancelada", "Debe de ingresar una tarea", "warning");
        return false;
    }

    var datos = LlamarAjax("Configuracion","opcion=GuardarTarea&IdEvento=" + id + "&IdTarea=" + idtarea + "&Tarea=" + tarea).split("|");
    if (datos[0] == "0") {
        $("#Tarea").focus();
        AgregarTarea();
        swal("", datos[1], "success");
    } else
        swal("Acción Cancelada", datos[1], "warning");
}

function EditarEvento(id) {
    var datos = LlamarAjax("Configuracion","opcion=VerEvento&IdEvento=" + id);
    var data = JSON.parse(datos);

    $("#IdEvento").val(id);
    $("#RegistroEvento").html('<b>Registro: ' + data[0].registro + '</b>');
    $("#Tipo").val(data[0].tipo).trigger("change");
    $("#Estado").val(data[0].estado).trigger("change");;
    $("#Titulo").val(data[0].titulo);
    $("#Cliente").val(data[0].idcliente);
    $("#Descripcion").val(data[0].descripcion);
    $("#HoraInicio").val((data[0].horainicio == "00:00" ? "" :  data[0].horainicio));
    $("#FechaInicio").val(data[0].fechainicio);
    $("#HoraFinal").val((data[0].horafinal == "00:00" ? "" : data[0].horafinal));
    $("#FechaFinal").val(data[0].fechafinal);
    $("#FechaRecordatorio").val(data[0].fecharecordatorio);

    var visitas = data[0].visitas.split("/");
    for (var x = 0; x < visitas.length; x++) {
        if ($.trim(visitas[x]) != "")
            $("#Vistas option[value=" + visitas[x] + "]").prop("selected", true);
    }
    $("#Vistas").trigger("change");
    
    $("#ModalEvento").modal({ backdrop: 'static' }, 'show');
}

function CrearEvento(fecha, hora) {
    $("#IdEvento").val("0");
    $("#Titulo").val("");
    $("#Descripcion").val("");
    $("#Archivo").val("");
    $("#Cliente").val("0").trigger("change");
    $("#Vistas").val("").trigger("change");

    $("#Tipo").val("Evento").trigger("change");
    $("#Estado").val("Activo").trigger("change");

    $("#FechaInicio").val(fecha);
    $("#HoraInicio").val(hora);
    $("#FechaFinal").val(fecha);
    $("#HoraFinal").val(hora);
    $("#FechaRecordatorio").val(fecha);
    $("#RegistroEvento").html("");
    $("#ModalEvento").modal({ backdrop: 'static' }, 'show');
}

function Fecha_calendario(anio, mes, dia) {
    return anio + '-' + (mes < 10 ? '0' : '') + mes  + '-' + (dia < 10 ? '0' : '') + dia;
}

function Fecha_cumple(fecha) {
    fecha = fecha.split("-");
    return fecha[1] + '-' + fecha[2];
}

$("#FormEvento").submit(function (e) {
    e.preventDefault();
    var fechainicio = $("#FechaInicio").val();
    var horainicio = $("#HoraInicio").val();
    var fechafinal = $("#FechaFinal").val();
    var horafinal = $("#HoraFinal").val();
    var parametros = $("#FormEvento").serialize();
    var fecharecordatorio = $("#FechaRecordatorio").val();

    if (fechainicio > fechafinal) {
        $("#FechaInicio").focus();
        swal("Acción Cancelada", "La fecha inicial no puede ser mayor a la fecha final", "warning");
        return false;
    }
    if (fechainicio < FechaActual) {
        $("#FechaInicio").focus();
        swal("Acción Cancelada", "La fecha inicial debe ser mayor o igual a " + fechaactual, "warning");
        return false;
    }
    if (horainicio != "" && horafinal == "") {
        $("#HoraFinal").focus();
        swal("Acción Cancelada", "Debe de ingresar la hora final", "warning");
        return false;
    }
    if (horafinal != "" && horainicio == "") {
        $("#HoraInicio").focus();
        swal("Acción Cancelada", "Debe de ingresar la hora inicial", "warning");
        return false;
    }
    if (fecharecordatorio > fechainicio) {
        $("#FechaRecordatorio").focus();
        swal("Acción Cancelada", "La fecha de recordatorio debe ser mayor o igual a la fecha de inicio", "warning");
        return false;
    }

    var datos = LlamarAjax("Configuracion","opcion=GuardarEvento&" + parametros).split("|");
    if (datos[0] == "0") {
        CargarAgenda(TipoAgenda);
        $("#ModalEvento").modal("hide");
        swal("", datos[1], "success");
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
});

function GuardarDocumento(id) {
    var documento = document.getElementById(id);
    if ($.trim(documento.value) != "") {

        var file = documento.files[0];
        var data = new FormData();
        data.append('documento', file);
        var resultado = '';
        var direccion = url + "Configuracion/AdjuntarAgenda";
        $.ajax({
            url: direccion,
            type: 'POST',
            contentType: false,
            data: data,
            processData: false,
            async: false,
            success: function (datos) {
                datos = datos.split("|");
                if (datos[0] != "0") {
                    swal(ValidarTraduccion("Error al cargar el archivo"), "", "warning");
                } else {
                    if (datos[2] != "XX")
                        swal(ValidarTraduccion(datos[2]), "", "warning");
                }
            }
        });
    }
}

function VerArchivo(id) {
    if (id == 0)
        id = $("#IdEvento").val() * 1;
    if (id == 0)
        return false;
    var archivo = LlamarAjax("Configuracion","opcion=VerArchivoEvento&IdEvento=" + id) * 1;
    if (archivo > 0)
        window.open(url + "ArchivoAgenda/" + id + ".pdf");
}

function EliminarArchivo() {

    var id = $("#IdEvento").val() * 1;
    var tipo = $("#Tipo option:selected").text();
    var titulo = $("#Titulo").val();

    if (id == 0)
        return false;

    
    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: ValidarTraduccion('¿Seguro que desea eliminar el archivo del(a) ' + tipo + ' ' + titulo + '?'),
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Eliminar'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                $.post("Configuracion/EliminarArchivoEvento", "IdEvento=" + id)
                    .done(function (data) {
                        datos = data.split("|");
                        if (datos[0] == "0") {
                            resolve()
                        } else {
                            reject(datos[1]);
                        }
                    })
            })
        }
    }]);
};

function FiltroAgenda() {
    CargarAgenda(TipoAgenda);
}

function ConsularEventos() {

    var tipo = $("#CoTipo").val();
    var estado = $("#CoEstado").val();
    var cliente = $("#CoCliente").val()*1;
    var fechad = $("#CoFechaDesde").val();
    var fechah = $("#CoFechaHasta").val();
    var titulo = $.trim($("#CoTitulo").val());
    var descripcion = $.trim($("#CoDescripcion").val());
       

    ActivarLoad();
    setTimeout(function () {
        var parametros = "tipo=" + tipo + "&cliente=" + cliente + "&estado=" + estado + "&titulo=" + titulo +
            "&fechad=" + fechad + "&fechah=" + fechah + "&descripcion=" + descripcion;
        var datos = LlamarAjax("Configuracion","opcion=ConsultarEventos&" + parametros);
        DesactivarLoad();

        var datajson = JSON.parse(datos);
        $('#TablaCoCalendario').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "editar"},
                { "data": "id", "className": "text-XX" },
                { "data": "tipo" },
                { "data": "estado" },
                { "data": "titulo" },
                { "data": "cliente" },
                { "data": "descripcion" },
                { "data": "visitas" },
                { "data": "fechainicio" },
                { "data": "fechafinal" },
                { "data": "fecharecordatorio" },
                { "data": "registro" }
            ],

            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy', 'colvis'
            ]
        });
    }, 15);
}

$("#TablaCoCalendario> tbody").on("dblclick", "tr", function () {
    var row = $(this).parents("td").context.cells;
    VerEvento(row[1].innerText);
});

function BuscarFecha() {
    swal({
        title: 'Buscador',
        text: 'Ingrese la fecha que desea buscar',
        html: '<input type="date" id="FechaBuscar" >',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Ir...'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                var fecha = $("#FechaBuscar").val();
                if (fecha != "") {
                    fecha = fecha.replace(/-/g, "/");
                    Fecha = new Date(fecha);
                    Fecha.setHours(0, 0, 0, 0);
                    month = Fecha.getMonth() + 1;
                    day = Fecha.getDate();
                    Anio = Fecha.getFullYear();
                    CargarAgenda(TipoAgenda);
                    resolve();
                } else {
                    reject(ValidarTraduccion('Debe de ingresar una fecha'));
                }
            })
        }
    });
}

CargarAgenda(TipoAgenda);


$('select').select2();
DesactivarLoad();


