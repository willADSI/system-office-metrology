﻿var Ingreso = 0;
var IdRecibido = 0;
var Foto = 0;
var ErrorEnter = 0;

var idusuario = localStorage.getItem('idusuario');


d = new Date();
month = d.getMonth() + 1;
day = d.getDate();

var Anio = $.trim(d.getFullYear());
Anio = $.trim(Anio.substr(2, 2))


output = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-' +
    (day < 10 ? '0' : '') + day;

$("#FechaInicio").val(output);
$("#FechaFin").val(output);
$("#Ubicacion").html(ComboUbicacion());

$("#Especialista").html(CargarCombo(19, 0));
$("#Especialista").val(idusuario).trigger("change");

$("#Asesor").html(CargarCombo(13, 1, "", "1"));
localStorage.setItem("Ingreso", "0");

function LimpiarTodo() {
    LimpiarDatos();
    $("#Ingreso").val("");
}

function LimpiarDatos() {
    Ingreso = 0;
    IdRecibido = 0;
    Foto = 0;
    localStorage.setItem("Ingreso", "0");
    $("#btnguardar").removeClass("hidden");
    $("#Equipo").val("");
    $("#Marca").val("");
    $("#Modelo").val("");
    $("#Serie").val("");
    $("#Magnitud").val("");
    $("#Observacion").val("");
    $("#Observaciones").val("");
    $("#Observaesp").val("");
    $("#Remision").val("");
    $("#Rango").val("");
    $("#Asesor").val("").trigger("change");
    $("#registrorecibiring").html("");

    $("#Usuario").val("");
    $("#FechaIng").val("");
    $("#Tiempo").val("");
    $("#Ubicacion").val("").trigger("change");
    
}


function ConsultarRecibidoIng() {

    var fecini = $("#FechaInicio").val();
    var fecfin = $("#FechaFin").val();
    var especialista = $("#Especialista").val() * 1;

    if (fecini == "" || fecfin == "") {
        $("#FechaInicio").focus();
        swal("Acción Cancelada", "Debeb de seleccionar fecha inicio y fecha final", "warning");
        return false;
    }
                
    ActivarLoad();
    setTimeout(function () {
        var parametros = "fecini=" + fecini + "&fecfin=" + fecfin + "&especialista=" + especialista;
        var datos = LlamarAjax("Laboratorio","opcion=TablaRecibidoIng&" + parametros);
        DesactivarLoad();
        var datajson = JSON.parse(datos);
        $('#TablaReportar').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "fila" },
                { "data": "ubicacion" },
                { "data": "ingreso" },
                { "data": "equipo" },
                { "data": "observaciones" },
                { "data": "usuario" },
                { "data": "fecha" }
            ],

            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy','colvis'
            ]
        });
    }, 15);
}

ConsultarRecibidoIng();


function BuscarIngreso(numero, code, tipo) {
    var mensaje = "";
    if ((Ingreso != (numero*1)) && ($("#Equipo").val() != ""))
        LimpiarDatos();
    if ((code.keyCode == 13 || tipo == "0") && ErrorEnter == 0) {
        if (numero * 1 > 0) {
            var datos = LlamarAjax("Laboratorio","opcion=BuscarIngresoRecbIng&ingreso=" + numero).split("|");
            if (datos[0] != "[]") {
                var data = JSON.parse(datos[0]);

                if (data[0].sitio == "SI") {
                    swal("Acción Cancelada", "No se puede recibir un ingreso realziado en sitio", "warning");
                    return false;
                }
                               
                                
                if (data[0].recibidocome * 1 == 0) {

                    if (data[0].recibidolab * 1 == 1) {
                        mesaje = "<br> Este ingreso debe de salir de laboratorio";
                    }

                    if (data[0].reportado * 1 == 1) {
                        mensaje = "<br>Este ingreso fue reportado por laboratorio" + mensaje;
                    }
                } 


                
                if ((data[0].certificado * 1 == 1) || (data[0].informetecnico * 1 == 1)) {
                    $("#Ingreso").select();
                    swal("Acción Cancelada", "Este ingreso posee certificado o informe técnico por el especialista", "warning");
                    ErrorEnter = 1;
                    return false;
                }
                                
                if (mensaje != "") {
                    $("#Ingreso").select();
                    ErrorEnter = 1;
                    swal("Acción Cancelada", mensaje, "warning");
                    if (PermisioEspecial == 0) {
                        return false;
                    }
                }
            
                data = JSON.parse(datos[1]);
                IdRemision = data[0].id * 1;
                Ingreso = data[0].ingreso * 1;
                Foto = data[0].fotos * 1;
                localStorage.setItem("Ingreso", Ingreso);
                $("#Equipo").val(data[0].equipo);
                $("#Marca").val(data[0].marca);
                $("#Modelo").val(data[0].modelo);
                $("#Serie").val(data[0].serie);
                $("#Rango").val(data[0].intervalo);
                $("#Remision").val(data[0].remision);
                $("#Magnitud").val(data[0].magnitud);
                $("#Observacion").val(data[0].observacion);
                $("#Observaesp").val((data[0].observaespeci ? data[0].observaespeci : ""));

                $("#Usuario").val(data[0].usuarioing);
                $("#FechaIng").val(data[0].fecharecing);
                $("#Tiempo").val(data[0].tiempo);

                if (data[0].id) {

                    swal("Acción Cancelada", "Este ingreso ya fue recibido", "warning");
                    ErrorEnter = 1;
                    IdRecibido = data[0].id;
                    Certificado = data[0].numero;
                    $("#Asesor").val(data[0].idusuarioent).trigger("change");
                    $("#Observaciones").val(data[0].observacioning);
                    $("#registrorecibiring").html("<b>Recibido el día " + data[0].fecha + " por el asesor " + data[0].usuariorecing + "</b>");
                    $("#btnguardar").addClass("hidden");
                    $("#Ubicacion").val(data[0].ubicacion).trigger("change");

                } else {
                    $("#Asesor").focus();
                }
                
            } else {
                $("#Ingreso").select();
                $("#Ingreso").focus();
                ErrorEnter = 1;
                swal("Acción Cancelada", "El Ingreso número " + numero + " no se encuentra Registrado/Calibrado", "warning");
                return false;
            }
        }
    } else
        ErrorEnter = 0;
}


function Guardar() {
    var asesor = $("#Asesor").val()*1;
    var observacion = $.trim($("#Observaciones").val());
    var ubicacion = $("#Ubicacion").val();
    var fecha = $("#FechaIng").val();
    if (Ingreso == 0)
        return false;
    var mensaje = "";
    if (observacion == "") {
        $("#Observaciones").focus()
        mensaje = "<br> Debe de ingresar la observación para la devolución del equipo" + mensaje;
    }
    if (asesor == 0) {
        $("#Asesor").focus();
        mensaje = "<br> Debe de seleccionar el especialista que entrega el equipo" + mensaje;
    }
    
    if (mensaje != "") {
        swal("Acción Cancelada", mensaje, "warning");
        return false;
    }

    var parametros = "noautorizado=1&ingreso=" + Ingreso + "&usuario=" + asesor + "&observacion=" + observacion + "&fecha=" + fecha + "&ubicacion=" + ubicacion;
    var datos = LlamarAjax("Laboratorio","opcion=RecibirIng&" + parametros).split("|");
    if (datos[0] == "0") {
        LimpiarTodo();
        ConsultarRecibidoIng();
        $("#Ingreso").focus();
        swal("", datos[1], "success");
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
}

function VerCertificado() {
    if (Ingreso == 0)
        return false;
    $("#titulodetalle").html("CERTIFICADO DEL INGRESO NRO " + Ingreso);
    
    var datos = LlamarAjax("Laboratorio/DetalleIngreso", "opcion=6&ingreso=" + Ingreso);
    $("#detallemovimiento").html(datos);

    $("#modalDetalleIng").modal("show");
    
}

function ImprimirCertificado(numero) {
    window.open(url + "Certificados/" + numero + ".pdf?id=" + NumeroAleatorio());
}

function VerFotos() {
    if (Ingreso == 0)
        return false;
    CargarModalFoto(Ingreso, 0,1);
}

function EliminarRecIngresoNO() {
    if (Ingreso == 0)
        return false;
    var mensaje = "";

    swal({
        title: 'Advertencia',
        text: '¿Desea eliminar el recibido del ingreso número ' + Ingreso + '?',
        input: 'text',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Eliminar'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',

        inputValidator: function (value) {
            return new Promise(function (resolve, reject) {
                if (value) {
                    var datos = LlamarAjax("Laboratorio","opcion=EliminarRecibido&opciones=Recibir Logistica&observacion=" + value + "&ingreso=" + Ingreso)
                    datos = datos.split("|");
                    if (datos[0] == "0") {
                        mensaje = datos[1];
                        resolve()
                    } else {
                        reject(datos[1]);
                    }

                } else {
                    reject(ValidarTraduccion('Debe de ingresar un observación'));
                }
            })
        }
    }).then(function (result) {
        LimpiarTodo();
        $("#Ingreso").focus();
        swal({
            type: 'success',
            html: mensaje
        })
    });
}

$('select').select2();
DesactivarLoad();

