﻿d = new Date();
month = d.getMonth() + 1;
day = d.getDate();

var Anio = $.trim(d.getFullYear());
Anio = $.trim(Anio.substr(2, 2))



output = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-01';

output2 = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-' +
    (day < 10 ? '0' : '') + day;

$("#FechaDesde").val(output);
$("#FechaHasta").val(output2);

$("#Magnitud").html(CargarCombo(1, 1));
$("#Equipo").html(CargarCombo(2, 1));
$("#Marca").html(CargarCombo(3, 1));
$("#Cliente").html(CargarCombo(9, 1));
$("#Modelo").html(CargarCombo(5, 1, "", "-1"));
$("#Intervalo").html(CargarCombo(6, 1, "", "-1"));
$("#Version").html(CargarCombo(75, 1));

$("#Usuario").html(CargarCombo(13, 1));

var PermisoEditar = PerGeneSistema("CERTIFICADO EDITAR APROBADO") * 1;

var AprobarCer = 0;
var EditarCer = 0;
var ImprimirCertificado = "";

function TipoAprobacion(aprobar, editar) {
    AprobarCer = aprobar;
    EditarCer = editar;
}

function BuscarCertificados() {
    var magnitud = $("#Magnitud").val() * 1;
    var equipo = $("#Equipo").val();
    var modelo = $("#Modelo").val();
    var marca = $("#Marca").val() * 1;
    var intervalo = $("#Intervalo").val();
    var ingreso = $("#Ingreso").val() * 1;
    var serie = $.trim($("#CSerie").val());
    var cliente = $("#Cliente").val() * 1;
    var certificado = $.trim($("#Certificado").val());
    var usuario = $("#Usuario").val() * 1;
    var version = $("#Version").val() * 1;
    var generado = $("#Generado").val();


    var estado = $("#Estado").val();
    var fechad = $("#FechaDesde").val();
    var fechah = $("#FechaHasta").val();

    if (fechad == "" && fechah == "") {
        swal("Acción Cancelada", ValidarTraduccion("debe de ingresar una fecha o números de días"), "warning");
        return false;
    }

    ActivarLoad();
    setTimeout(function () {
        var parametros = "certificado=" + certificado + "&cliente=" + cliente + "&ingreso=" + ingreso + "&estado=" + estado +
            "&fechad=" + fechad + "&fechah=" + fechah + "&magnitud=" + magnitud + "&equipo=" + equipo + "&marca=" + marca + "&modelo=" + modelo +
            "&intervalo=" + intervalo + "&serie=" + serie + "&usuario=" + usuario + "&version=" + version + "&generado=" + generado;
        var datos = LlamarAjax("Laboratorio","opcion=TablaCertificados&" + parametros);
        DesactivarLoad();
        var datajson = JSON.parse(datos);
        $('#TablaCertificados').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "opciones" },
                { "data": "numero" },
                { "data": "cliente" },
                { "data": "ingreso" },
                { "data": "fechaapro" },
                { "data": "nombrecer" },
                { "data": "direccioncer" },
                { "data": "observacion" },
                { "data": "proximacali" },
                { "data": "equipo" },
                { "data": "estado", "className": "text-XX" },
                { "data": "fechaexp" },
                { "data": "treportado" },
                { "data": "tcertificado" },
                { "data": "generado" },
                { "data": "tecnico" },
                { "data": "impreso" },
                { "data": "sticker" },
                { "data": "aprobado" },
                { "data": "anulado" }
            ],

            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy', 'colvis'
            ],
            'rowCallback': function (row, data, index) {
                if (data.estado == "Anulado") {
                    $(row).find('td:eq(10)').css('color', 'red');
                }
                else if (data.estado == "Aprobado") {
                    $(row).find('td:eq(10)').css('color', 'green');
                }
            }
        });
    }, 15);
}

function CambioTiempoCali(tiempo, tipo) {
    var fecha = $("#" + tipo + "NFecha").val().split("-");
    if (tiempo * 1 == 0)
        $("#" + tipo + "NProxima").val("");
    else {
        $("#" + tipo + "NProxima").val((fecha[0] * 1 + 1) + "-" + fecha[1] + "-" + fecha[1]);
    }
}

$("#TablaCertificados > tbody").on("dblclick", "tr", function () {
    var row = $(this).parents("td").context.cells;

    if (PermisoEditar == 0) {
        var estado = $.trim(row[10].innerText);
        if (estado != "Registrado") {
            swal("Acción Cancelada", "No se puede editar un certificado en estado " + estado, "warning");
            return false;
        }
    }

    var generado = $.trim(row[14].innerText);
    if (generado == "EXCEL") {
        swal("Acción Cancelada", "No se puede editar un certificado generado por la plantilla de excel", "warning");
        return false;
    }

    var fecha = row[10].innerText.split(' ');
    fecha = CambiarCaracter(fecha[0], '/', '-');

    $("#NCertificado").val(row[1].innerText);
    $("#NFecha").val(fecha);
    $("#NIngreso").val(row[3].innerText);
    $("#NEquipo").html(row[9].innerText);
    $("#NCliente").val(row[2].innerText);

    $("#NNombre").val(row[5].innerText);
    $("#NDireccion").val(row[6].innerText);
    $("#NObservacion").val(row[7].innerText);
    $("#NProxima").val(row[8].innerText);

    $("#ModalCertificado").modal("show");
});

function ImprimirCertificadoPDF(numero) {
    window.open(url + "Certificados/" + numero + ".pdf");
}

function ReeImprimirCertificado(datos) {
    var data = JSON.parse(datos);
    var revision = data[0].revision;
    var ingreso = data[0].ingreso;
    var numero = data[0].item;
    var magnitud = data[0].idmagnitud * 1;
    var version = data[0].idversion * 1;
    var parametros = "ingreso=" + ingreso + "&numero=" + numero + "&revision=" + revision + "&tipo=1&opcion=2";
    switch (magnitud) {
        case 6:
            if (version == 6)
                datos = LlamarAjax("RpPlantillas","opcion=RpPlanillaPT1&" + parametros);
            else
                datos = LlamarAjax("RpPlantillas","opcion=RpPlanillaPT1_V07&" + parametros);
            break;
        case 2: {
            datos = LlamarAjax("RpPlantillas","opcion=RpPlanillaPresion1&" + parametros);
        }
    }
    datos = datos.split("|");
    if (datos[0] == "0") {
        window.open(url + "DocumPDF/" + datos[1]);
    } else {
        if (datos[0] == "9")
            window.open(url + "Certificados/" + datos[1] + ".pdf");
        else
            swal("", ValidarTraduccion(datos[1]), "warning");
    }
}

function VerPdfCertificado() {
    window.open(url + "Certificados/" + ImprimirCertificado + ".pdf");
}

$("#FomrEdiCertificado").submit(function (e) {
    e.preventDefault();
    ActivarLoad();
    setTimeout(function () {
        var parametros = $("#FomrEdiCertificado").serialize() + "&Aprobar=0&Editar=1";
        var datos = LlamarAjax("Laboratorio/EditarCertificado", parametros).split("|");
        if (datos[0] == "0") {
            BuscarCertificados();
            $("#ModalCertificado").modal("hide");
            ReeImprimirCertificado(datos[2])
            DesactivarLoad();
            swal("", datos[1], "success");
        } else {
            DesactivarLoad();
            swal("Acción Cancelada", datos[1], "warning");
        }
    }, 15);

    return false;
})

function AprobarCertificado(certificado, estado) {
    ModalAprobarCertificado(certificado, 0, estado);
}

function ModalAprobarCertificado(certificado, usuario, estado) {

    var parametros = "certificado=" + certificado + "&cliente=0&ingreso=0&estado=&fechad=&fechah=&magnitud=0&equipo=&marca=0&modelo=&intervalo=&serie=&usuario=0&version=0";
    var datos = LlamarAjax("Laboratorio","opcion=TablaCertificados&" + parametros);
    var data = JSON.parse(datos);

    var fecha = data[0].fechaexp.split(' ');
    fecha = CambiarCaracter(fecha[0], '/', '-');
    var aprobado = $.trim(data[0].aprobado).replace(/<br>/g, '-');
    var anulado = $.trim(data[0].anulado).replace(/<br>/g, '-');

    $("#ANUsuarioApro").val(usuario);
    $("#ANCertificado").val(certificado);
    $("#ANFecha").val(fecha);
    $("#ANIngreso").val(data[0].ingreso);
    $("#ANEquipo").html(data[0].equipo.replace(/<br>/g, '-'));
    $("#ANCliente").val(data[0].cliente);

    $("#ANNombre").val(data[0].nombrecer);
    $("#ANDireccion").val(data[0].direccioncer);
    $("#ANObservacion").val(data[0].observacion);
    $("#ANProxima").val(data[0].proximacali);
    $("#ANObservacionAnula").val(data[0].observacionanula);
    ImprimirCertificado = data[0].imprimir;
    var idcertificado = data[0].id;

    $("#ANObservacionAnula").prop("disabled", false);
    $("#btnreemplazar").removeClass("hidden");
    $(".boton").removeClass("hidden");

    if (data[0].generado == "EXCEL")
        $("#btnreemplazar").addClass("hidden");

    if (data[0].generado == "EXCEL" || estado != "Registrado") {
        $("#ANNombre").prop("disabled", true);
        $("#ANDireccion").prop("disabled", true);
        $("#ANObservacion").prop("disabled", true);
        $("#ANProxima").prop("disabled", true);
        $("#btnactualizar").addClass("hidden");
        $("#ANCambio").prop("disabled", true);
    } else {
        if (estado == "Registrado") {
            $("#ANNombre").prop("disabled", false);
            $("#ANDireccion").prop("disabled", false);
            $("#ANObservacion").prop("disabled", false);
            $("#ANProxima").prop("disabled", false);
            $("#btnactualizar").removeClass("hidden");
            $("#ANCambio").prop("disabled", false);
        }
    }
    $("#NResultados").html("");
    var parametros = "idcertificado=0";
    if (aprobado != "" || anulado != "")
        parametros = "idcertificado=" + idcertificado + "&tipo=" + estado;
    datos = LlamarAjax("Laboratorio","opcion=ItemsAprobar&" + parametros);
    var data = JSON.parse(datos);
    var resultado = "";
    var activo = "";
    for (var x = 0; x < data.length; x++) {
        activo = (data[x].aprobacion * 1 == 0 ? "" : "checked disabled")
        resultado += "<tr>" +
            "<td>" + data[x].descripcion + "</td>" +
            "<td><input type='checkbox' class='form-control' " + activo + " name = 'NOpciones[]' required > <input type='hidden' name='NItems[]' value='" + data[x].descripcion + "'></td></tr >";
    }
    $("#TBItems").html(resultado);

    if (estado == "Aprobado") {
        $("#NResultados").html("Aprobado: " + aprobado);
        $("#btnaprobar, #btnactualizar").addClass("hidden");
    }

    if (estado == "Anulado") {
        $("#NResultados").html("Anulado: " + anulado);
        $(".boton").addClass("hidden");
        $("#ANObservacionAnula").prop("disabled", true);
    }


    $("#ModalAprobarCertificado").modal("show");

}


$("#RevisarCertificado").click(function () {

	var aprobado = $("#NResultados").html();
	var observacion = $.trim($("#ANObservacionAnula").val());

	if (aprobado != "") {
		swal("Acción Cancelada", "No se puede revisar un certificado anulado ó aprobado", "warning");
		return false;
	}

	if (observacion == "") {
		$("#ANObservacionAnula").focus();
		swal("Acción Cancelada", "Debe de ingresar una observación de revisión", "warning");
		return false;
	}


	var items = document.getElementsByName("NItems[]");
	var opciones = document.getElementsByName("NOpciones[]");
	var a_items = "";
	var noseleccionado = 0;

	for (var x = 0; x < opciones.length; x++) {
		if (opciones[x].checked == false) {
			noseleccionado++;
			if (a_items != "")
				a_items += ",";
			a_items += items[x].value;
		}
	}

	if (noseleccionado == 0) {
		swal("Acción Cancelada", "Debe de haber un item no seleccionado para guardar la revisión... Si todos los items están correctos debe de aprobar el certificado", "warning");
		return false;
	}

	var certificado = $("#ANCertificado").val();
	var ingreso = $("#ANIngreso").val() * 1;
	var equipo = $("#ANEquipo").html();
	var mensaje = '¿Seguro que desea aplicar la revisión del certificado número ' + certificado + ', del equipo ' + equipo + '?';
	var resultado = "";
	var parametros = "certificado=" + certificado + "&a_items=" + a_items + "&observacion=" + observacion + "&ingreso=" + ingreso;
	var asesor = "";

	swal.queue([{
		title: ValidarTraduccion('Advertencia'),
		text: mensaje,
		type: 'question',
		showLoaderOnConfirm: true,
		showCancelButton: true,
		confirmButtonText: ValidarTraduccion('Revisar'),
		cancelButtonText: ValidarTraduccion('Cancelar'),
		confirmButtonClass: 'btn btn-success',
		cancelButtonClass: 'btn btn-danger',
		preConfirm: function () {
			return new Promise(function (resolve, reject) {
				$.post(url_servidor + "Laboratorio","opcion=RevisionCertificado&" + parametros)
					.done(function (data) {
						datos = data.split("|");
						if (datos[0] == "0") {
							$("#ModalAprobarCertificado").modal("hide");
							resultado = datos[1];
							asesor = datos[2];

							resolve()
						} else {
							reject(datos[1]);
						}
					})
			})
		}
	}]).then(function (result) {
		BuscarCertificados();

		idusuario = "/" + asesor + "/";
		var mensaje = "Buen día AMIGO, Debe corregir del certificado: " + certificado + ", los siguientes <b>items:</b><br> " +
			a_items + "<br><br><b>Observación:</b><br>" + observacion.toLowerCase();
		sendMessage(3, mensaje, idusuario);
		ArchivoChat = "";

		if (result) {
			swal({
				type: 'success',
				html: resultado
			})
		}
	})

	$(".swal2-content").html(mensaje);

});


function HistoricoRevision(certificado) {
    if (certificado == "")
        certificado = $("#ANCertificado").val();
    var datos = LlamarAjax("Laboratorio","opcion=HistorialRevisionCertificado&certificado=" + certificado);
    if (datos == "[]") {
        swal("Acción Cancelada", "Este certificado no ha sido revisado", "warning");
        return false;
    }
    var data = JSON.parse(datos);
    var resultado = "";
    for (var x = 0; x < data.length; x++) {
        resultado += "<tr>" +
            "<td>" + (x + 1) + "</td>" +
            "<td>" + data[x].fecha + "</td>" +
            "<td>" + data[x].items + "</td>" +
            "<td>" + data[x].observacion + "</td>" +
            "<td>" + data[x].nombrecompleto + "</td></tr>";
    }
    $("#TBRevision").html(resultado);
    $("#ModalRevisiones").modal("show");
}

$("#FomrAprobarCertificado").submit(function (e) {
    e.preventDefault();
    var certificado = $("#ANCertificado").val();
    var equipo = $("#ANEquipo").html();
    var parametros = $("#FomrAprobarCertificado").serialize() + "&Aprobar=" + AprobarCer + "&Editar=" + EditarCer;
    var mensaje = '¿Seguro que desea aprobar el certificado número ' + certificado + ', del equipo ' + equipo + '?';
    var resultado = "";
    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: ValidarTraduccion(mensaje),
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Aprobar'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                $.post("Laboratorio/EditarCertificado", parametros)
                    .done(function (data) {
                        datos = data.split("|");
                        if (datos[0] == "0") {
                            $("#ModalAprobarCertificado").modal("hide");
                            resultado = datos[1];
                            if (EditarCer == 1) {
                                ReeImprimirCertificado(datos[2]);
                            }
                            resolve()
                        } else {
                            reject(datos[1]);
                        }
                    })
            })
        }
    }]).then(function (result) {
        BuscarCertificados();
        if (result) {
            swal({
                type: 'success',
                html: resultado
            })
        }
    })

    $(".swal2-content").html(mensaje);
})

function AnularCertificado(reemplazar) {
    var certificado = $("#ANCertificado").val();
    var usuario = $("#ANUsuarioApro").val();
    var cliente = $("#ANCliente").val();
    var observacion = $.trim($("#ANObservacionAnula").val());
    var resultado = "";
    var aprobado = $("#NResultados").html();

    if (observacion == "" && aprobado == "") {
        $("#ANObservacionAnula").focus();
        swal("Acción Cancelada", "Debe de ingresar una observación de anulación o reemplazo", "warning");
        return false;
    }

    if (aprobado == "") {
        var items = document.getElementsByName("NItems[]");
        var opciones = document.getElementsByName("NOpciones[]");
        var opcion = "";

        var a_items = "";
        var a_opciones = "";


        for (var x = 0; x < opciones.length; x++) {
            opcion = (opciones[x].checked ? "1" : "0");
            if (a_items != "") {
                a_items += ",";
                a_opciones += ",";
            }
            a_items += items[x].value;
            a_opciones += opcion;
        }
    }

    swal({
        title: 'Advertencia',
        text: '¿' + ValidarTraduccion('Desea ' + (reemplazar == 0 ? 'Anular' : 'Reemplazar') + ' el Certificado') + ' ' + certificado + ' ' + ValidarTraduccion('del cliente') + ' ' + cliente,
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion((reemplazar == 0 ? 'Anular' : 'Reemplazar')),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                $.post("Laboratorio/AnularCertificado", "certificado=" + certificado + "&observacion=" + observacion + "&reemplazar=" + reemplazar + "&Items=" + a_items + "&Opciones=" + a_opciones + "&nusuario=" + usuario)
                    .done(function (data) {
                        datos = data.split("|");
                        if (datos[0] == "0") {
                            resultado = datos[1];
                            $("#ModalAprobarCertificado").modal("hide");
                            if (reemplazar == 1)
                                ReeImprimirCertificado(datos[2])
                            resolve()
                        } else {
                            reject(datos[1]);
                        }

                    })
            })
        }
    }).then(function (result) {
        BuscarCertificados();
        swal({
            type: 'success',
            html: datos[1]
        })
    })
}

$('select').select2();
DesactivarLoad();
