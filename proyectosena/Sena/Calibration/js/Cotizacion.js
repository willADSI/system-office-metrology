﻿document.onkeydown = function (e) {

    var contenedor = $('#tabcotizacion a[href="#tabcrear"]');
    tecla = (document.all) ? e.keyCode : e.which;
    if (e.altKey == false) {
        switch (tecla) {
            case 27:
                if ($("#modalCambioPrecio").css("display") == "block") {
                    $("#modalCambioPrecio").modal("hide");
                    return false;
                }
                if ($("#modalAprobarRepor").css("display") == "block") {
                    $("#modalAprobarRepor").modal("hide");
                    return false;
                }
                if ($("#enviar_reporte").css("display") == "block") {
                    $("#enviar_reporte").modal("hide");
                    return false;
                } 
                if ($("#modalMetodo").css("display") == "block") {
                    $("#modalMetodo").modal("hide");
                    return false;
                }
                if ($("#ModalSolEquipo").css("display") == "block") {
                    $("#ModalSolEquipo").modal("hide");
                    return false;
                }
                if ($("#modalPrecios").css("display") == "block") {
                    $("#modalPrecios").modal("hide");
                    return false;
                }
                if ($("#ModalArticulos").css("display") == "block") {
                    $("#ModalArticulos").modal("hide");
                    return false;
                }
                if ($("#ModalEstadoCuenta").css("display") == "block") {
                    $("#ModalEstadoCuenta").modal("hide");
                    return false;
                }
                if ($("#modalClientes").css("display") == "block") {
                    $("#modalClientes").modal("hide");
                    return false;
                }
               
                if ($("#ModalOtroServicio").css("display") == "block") {
                    $("#ModalOtroServicio").modal("hide");
                    return false;
                }
                if ($("#modalIntervalo").css("display") == "block") {
                    $("#modalIntervalo").modal("hide");
                    return false;
                }
               
                if ($("#enviar_cotizacion").css("display") == "block") {
                    $("#enviar_cotizacion").modal("hide");
                    return false;
                }
                if ($("#modalRelIngreso").css("display") == "block") {
                    $("#modalRelIngreso").modal("hide");
                    return false;
                }
                if ($("#modalPrecioSer").css("display") == "block") {
                    $("#modalPrecioSer").modal("hide");
                    return false;
                }
                if ($("#modalAprobar").css("display") == "block") {
                    $("#modalAprobar").modal("hide");
                    return false;
                }
                if ($("#modalCambioEmpresa").css("display") == "block") {
                    $("#modalCambioEmpresa").modal("hide");
                    return false;
                }
               
                if ($("#modalIngresoCot").css("display") == "block") {
                    $("#modalIngresoCot").modal("hide");
                    return false;
                }
                if ($("#modalDetAsesor").css("display") == "block") {
                    $("#modalDetAsesor").modal("hide");
                    return false;
                }
                break;
            case 112://F1
                ManualAyuda(2);
                return false;
                break;
            case 113://F2
                
                if ($("#tabclienteasesor").css("display") == "block") {
                    ConsularAsesor();
                    return false;
                }
                if ($("#tabreportes").css("display") == "block") {
                    ConsularReportes();
                    return false;
                }
                if ($("#tabconsultar").css("display") == "block") {
                    ConsultarCotizaciones();
                    return false;
                }
                if ($("#tabingresos").css("display") == "block") {
                    ConsularIngresos();
                    return false;
                }
                if ($("#ModalArticulos").css("display") == "block") {
                    LimpiarServicio()
                    return false;
                }
                if ($("#ModalOtroServicio").css("display") == "block") {
                    LimpiarOtroServicio();
                    return false;
                }
                LimpiarTodo();
                return false;
                break;
            case 114://F3
                ModalClientes();
                return false;
                break;
            case 115://F4
                if ($("#ModalArticulos").css("display") == "block") {
                    CargarEditarPrecios();
                    return false;
                }
                FBuscarServicio();
                return false;
                break;
            case 117://F6
                if ($("#ModalArticulos").css("display") == "block") {
                    VerSolicitud();
                    return false;
                }
                CargarModalOServicio();
                return false;
                break;
            case 118://F7
                AgregarIngresoCot();
                return false;
                break;
            case 119://F8
                ModalRemision();
                return false;
                break;
            case 120: //F9
                if ($("#ModalEstadoCuenta").css("display") == "block") {
                    EnviarEstadoCuenta();
                }
                EnviarCotizacion();
                return false;
                break;
            case 121://F10
                if ($("#tabseparar").css("display") == "block") {
                    SepararCotizacion();
                    return false;
                }
                if ($("#modalCambioPrecio").css("display") == "block") {
                    CambiarPrecio();
                    return false;
                }
                if ($("#modalAprobarRepor").css("display") == "block") {
                    GuardarAproRepo();
                    return false;
                }
                if ($("#enviar_reporte").css("display") == "block") {
                    btnenviarreporte.click();
                    return false;
                }
                if ($("#modalMetodo").css("display") == "block") {
                    GuardarMetodo();
                    return false;
                }
                if ($("#modalPrecios").css("display") == "block") {
                    GuardarPrecio();
                    return false;
                }
                if ($("#ModalArticulos").css("display") == "block") {
                    AgregarServicio();
                    return false;
                }
                if ($("#ModalOtroServicio").css("display") == "block") {
                    AgregarOtroServicio();
                    return false;
                }
                if ($("#modalCambioEmpresa").css("display") == "block") {
                    CambiarCliente();
                    return false;
                }
                if ($("#enviar_cotizacion").css("display") == "block") {
                    $("#btnenviarcotizacion").click();
                    return false;
                }
                if ($("#modalAprobar").css("display") == "block") {
                    GuardarAprobacion();
                    return false;
                }
                GuardarCotizacion();
                return false;
                break;
            case 122: //F11
                LlamarImprimir();
                return false;
                break;
            case 123: //F12
                ImprimirServicio(0);
                return false;
                break;
        }
    } else {
        switch (tecla) {
            case 114://F3
                AplicarDescuento();
                return false;
                break;
            case 117://F6
                ReemplazarCotizacion();
                return false;
                break;
            case 118://F7
                ImportarCotizacion();
                return false;
                break;
            case 119://F8
                AnularCotizacion();
                return false;
                break;
            case 120://F9
                ModalCambioCliente();
                return false;
                break;
            case 121: //F10
                AprobarCotizacion();
                return false;
                break;
            case 123: //F12
                VerEstadoCuenta();
                return false;
                break;
        }
    }
    
}

var OpcionEquipo = 0;
var CanItems = 0;
var TablaPrecio = 0;
var DescuentoCli = 0;
var DescuentoCliente = 0;
var IdCliente = 0;
var IdClienteRel = 0;
var CotizacionRel = 0;
var DocumentoCli = "";
var TipoCliente = 0;
var PlazoPago = 0;
var TipoPrecio = 0;
var IdPrecio = 0;
var PrecioServicio = 0;
var Remision = 0;
var IdRemision = 0;
var IdCotizacion = 0;
var IdServicio = 0;
var Cotizacion = 0;
var Estado = "Temporal";
var IntervaloAnt = "";
var NroIngreso = 0;
var IVAServicio = 0;
var DiasEntrega = 0;
var ExpressServ = 0;
var SitioServ = 0;
var IdSede = 0;
var IdContacto = 0;
var PagoCertificado = 'NO';
var RequiereSolicitud = 'NO';
var Habitual = 'NO';
var CorreoEmpresa = "";
var Pdf_Cotizacion = "";
var totales = 0;
var subtotal = 0;
var descuento = 0;
var exento = 0;
var gravable = 0;
var iva = 0;
var CotizacionesAnt = 0;
var CotizacionSep = 0;
var SeleccionIngreso = 0;
var DescuentoSer = 0;
var Solicitud = 0;
var ValorIva = 0;
var UrlAlertnativa = "http://190.144.188.100:82/"

var retefuente = 0;
var CliReteFuente = 0;
var reteica = 0;
var CliReteIca = 0;
var reteiva = 0;
var CliReteIva = 0;



var movil = 0;
if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
    movil = 1;
}

var colores = ['rgb(255, 99, 132)', 'rgb(255, 159, 64)', 'rgb(255, 205, 86)', 'rgb(75, 192, 192)', 'rgb(54, 162, 235)', 'rgb(153, 102, 255)', 'rgb(2, 254, 21)', 'rgb(6, 252, 245)', 'rgb(240, 20, 10)', 'rgb(254, 246, 2)'];

var grafica = document.getElementById('GraficaCot').getContext('2d');
var mygrafica = null;


d = new Date();
month = d.getMonth() + 1;
day = d.getDate();

var Anio = $.trim(d.getFullYear());
Anio = $.trim(Anio.substr(2, 2))
var AprobarCotiza = "";

output = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-01';

output2 = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-' +
    (day < 10 ? '0' : '') + day;

$("#CFechaDesde").val(output);
$("#CFechaHasta").val(output2);
$("#ICFechaDesde").val(output);
$("#ICFechaHasta").val(output2);
$("#CRFechaDesde").val(output);
$("#CRFechaHasta").val(output2);
$("#AFechaDesde").val(output);
$("#AFechaHasta").val(output2);

var datos = LlamarAjax("Configuracion","opcion=CargaComboInicial&tipo=2").split("||");

$("#Magnitud, #CMagnitud, #AMagnitud, #ICMagnitud, #CRMagnitud").html(datos[0]);
$("#CEquipo, #ICEquipo, #CREquipo, #EquipoMetodo").html(datos[1]);
$("#Marca, #CMarca, #ICMarca, #CRMarca").html(datos[2]);
$("#Servicio, #CServicio").html(datos[3]);
$("#IMedida, #PMedida").html(datos[4]);
$("#CCliente, #ACliente, #ICCliente, #CRCliente, #CaCliente").html(datos[5]);
$("#CModelo, #ICModelo, #CRModelo").html(datos[6]);
$("#CIntervalo, #ICIntervalo, #CRIntervalo").html(datos[7]);
$("#OServicio").html(datos[8]);
$("#CMetodo").html(datos[9]);
$("#CPais").html(datos[10]);
$("#CDepartamento").html(datos[11]);
$("#CCiudad").html(datos[12]);
$("#CUsuario, #AAsesor, #ICUsuario, #CRUsuario, #CAsesor").html(datos[13]);
$("#Validez").html(CargarCombo(81, 0));

$("#AAsesor").val(localStorage.getItem('idusuario'))

$("#Magnitud").val("").trigger("change");


tb = $('input');
$(tb).keypress(enter2tab);

tb = $('select');
$(tb).keypress(enter2tab);

function enter2tab(e) {
    if (e.keyCode == 13) {
        id = $(this).attr("id");
        
        cb = parseInt($(this).attr('tabindex'));

        if (id == "Cliente") {
            if ($.trim($(this).val()) != "") {

                BuscarClienteCot($(this).val(),0,0,0);
                return false;
            } 
        }

        if (id == "Remision") {
            if ($.trim($(this).val()) != "") {
                BuscarRemisiom($(this).val());
                return false;
            }
        }


                
        if ($(':input[tabindex=\'' + (cb + 1) + '\']') != null) {
            $(':input[tabindex=\'' + (cb + 1) + '\']').focus();
            $(':input[tabindex=\'' + (cb + 1) + '\']').select();
            e.preventDefault();

            return false;
        }

        if ($(':select[tabindex=\'' + (cb + 1) + '\']') != null) {
            $(':select[tabindex=\'' + (cb + 1) + '\']').focus();
            $(':select[tabindex=\'' + (cb + 1) + '\']').select();
            e.preventDefault();

            return false;
        }
    }
}


function LimpiarServicio() {
    IdServicio = 0;
    IVAServicio = 0;
    PrecioServicio = 0;
    DiasEntrega = 0;
        NroIngreso = 0;
    IntervaloAnt = 0;
    $("#DetIdSolicitud").val("0");

    $("#Magnitud").val("").trigger("change");
    $("#Marca").val("").trigger("change");
    $("#Servicio").val("").trigger("change");
    $("#Ingreso").val("");
    $("#Serie").val("");
    $("#Calibracion").val("");
    $("#Certificado").val("").trigger("change");
    $("#Declaracion").val("");
    $("#Punto").val("");
    $("#Observacion").val("");
    $("#Direccion").val("SEDE");
    $("#Cantidad").val("1");
    $("#Entrega").val("0");
    $("#NombreCa").val($("#NombreCliente").val());
    $("#PreciodeVenta").val("0");
    $("#Costo").val("0");
    $("#SubTotal").val("0");
    $("#CalibracionAdic").val("");
    $("#PrecioAdic").val("0");

    $("#Magnitud").attr("disabled", false);
    $("#Equipo").attr("disabled", false);
    $("#Marca").attr("disabled", false);
    $("#Modelo").attr("disabled", false);
    $("#Intervalo").attr("disabled", false);
    $("#Intervalo2").attr("disabled", false);
    $("#Ingreso").attr("disabled", false);
    $("#Serie").attr("disabled", false);
    $("#Cantidad").attr("disabled", false);

    $("#Magnitud").focus();
    
}

function FBuscarServicio() {
    if (IdCliente > 0) {
        if (RequiereSolicitud == "SI" && Habitual == "NO" &&  CanItems == 0 && Solicitud == 0) {
            swal("Acción Cancelada", "Debe ingresar la solicitud del cliente", "warning");
            return false;
        }
        ModalArticulos();
    } 
}

function ModalArticulos() {

    ActivarLoad();
    setTimeout(function () {

        if ((Estado == 'Temporal' || Estado == "Cotizado") && (IdCliente > 0)) {
            if (IdSede == 0) {
                $("#Sede").focus();
                swal("Acción Cancelada", "Debe seleccionar la sede del cliente", "warning");
                DesactivarLoad();
                return false;
            }
            if (IdContacto == 0) {
                $("#Contacto").focus();
                swal("Acción Cancelada", "Debe seleccionar el contacto del cliente", "warning");
                DesactivarLoad();
                return false;
            }
            LimpiarServicio();
            $("#ModalArticulos").modal({ backdrop: 'static' }, 'show');
            setTimeout(function () {
                $("#Magnitud").focus();
            }, 500);
        }
        DesactivarLoad();
    }, 15);
}

function CargarModelos(marca) {
    $("#Modelo").html(CargarCombo(5, 1, "", marca));
    BuscarPrecio();
}

function CargarDiaEntrega() {
    var servicio = $("#Servicio").val() * 1;
    DiasEntrega = 0;
    ExpressServ = 0;
    SitioServ = 0;
    IVAServicio = 0
    if (servicio > 0) {

        var datos = LlamarAjax("Cotizacion","opcion=DiasEntregaSer&servicio=" + servicio).split("|");
        DiasEntrega = datos[0] * 1;
        IVAServicio = ValorIva > 0 ? (datos[1] * 1) : 0;
        ExpressServ = datos[2] * 1;
        SitioServ = datos[3] * 1;
        DescuentoSer = datos[4] * 1;
        if (DescuentoSer == 0)
            $("#Descuento").val("0");
    }
    $("#Entrega").val(DiasEntrega);
}

function CargarOtrosIngresos(servicio) {
    datos = 0;
    var idsolicitud = $("#DetIdSolicitud").val() * 1;
    if (servicio * 1 > 0)
        datos = LlamarAjax("Cotizacion", "opcion=VerificarServicio&servicio=" + (servicio * 1)) * 1;
    if (datos == 1) {
        TipoPrecio = 2;
        $("#Metodo").val("").trigger("change");
        $("#Punto").val("");
        $("#NombreCa").val("");
        $("#Direccion").val("");
        $("#Calibracion").val("");
        $("#Declaracion").val("");
        $("#Certificado").val("").trigger("change");
        
        $("#Metodo").prop("disabled", true);
        $("#Punto").prop("disabled", true);
        $("#NombreCa").prop("disabled", true);
        $("#Direccion").prop("disabled", true);
        $("#Calibracion").prop("disabled", true);
        $("#Certificado").prop("disabled", true);
        $("#Declaracion").prop("disabled", true);
    } else {
        if (datos == 3)
            TipoPrecio = 4;
        if (datos == 2)
            TipoPrecio = 2;
        if (idsolicitud == 0) {
            $("#NombreCa").val($("#NombreCliente").val());
            $("#Direccion").val("SEDE");
        }
        if (datos == 0)
            $("#Metodo").prop("disabled", false);
        else
            $("#Metodo").prop("disabled", true);

        $("#Punto").prop("disabled", false);
        $("#NombreCa").prop("disabled", false);
        $("#Direccion").prop("disabled", false);
        $("#Calibracion").prop("disabled", false);
        $("#Certificado").prop("disabled", false);
        $("#Declaracion").prop("disabled", false);

    }

}

function CargarProveedores(servicio) {
    VerTipoPrecio();
    CargarDiaEntrega();
    CargarOtrosIngresos(servicio);
    BuscarPrecio();
}

function CambioIntervalo(intervalo) {
    IntervaloAnt = intervalo;
    BuscarPrecio();
}

function VerTipoPrecio() {
    var ingreso = $("#Ingreso").val() * 1;
    var servicio = $("#Servicio").val() * 1;
    var magnitud = $("#Magnitud").val() * 1;
    var datos = LlamarAjax("Cotizacion","opcion=TipoPrecio&magnitud=" + magnitud + "&servicio=" + servicio).split("|");
    TipoPrecio = datos[0]*1;

    switch (TipoPrecio) {
        case 1:
            $("#Proveedor").html("");
            $("#Costo").val("0");
            $("#Proveedor").attr("disabled", true);
            $("#Intervalo").val(IntervaloAnt).trigger("change");
            break;
        case 2:
            $("#Proveedor").attr("disabled", true);
            if (ingreso == 0) {
                $("#Intervalo").val("0").trigger("change");
            }
            $("#Proveedor").html("");
            $("#Costo").val("0");
            break;
        case 3:
            $("#Proveedor").html(CargarCombo(7, 1));
            $("#Proveedor").attr("disabled", false);
            TipoPrecio = datos[1] * 1;
            break;
    }
}



function CargarIntervalos(magnitud) {
    var intervalo = CargarCombo(6, 0, "", magnitud);
    $("#Intervalo").html("<option value='0' idioma='VER ESPECIFICACIONES'>VER ESPECIFICACIONES</option>" + intervalo);
    $("#Intervalo2").html("<option value=''>--Seleccione--</option>" + intervalo);
    if (OpcionEquipo == 0) {
        if (magnitud * 1 > 0)
            $("#Equipo").html(CargarCombo(2, 1, "", magnitud));
        else
            $("#Equipo").html(CargarCombo(58, 1));
    }
    VerTipoPrecio();
    BuscarPrecio();
}

function MagnitudEquipo(equipo) {
    equipo = equipo * 1;
    $("#Metodo").html(CargarCombo(17, 1, "", equipo));
    var datos = LlamarAjax("Cotizacion","opcion=MagnitudEquipo&id=" + equipo);
    if (datos != "[]") {
        var data = JSON.parse(datos);
        OpcionEquipo = 1;
        if (data[0].idmagnitud * 1 != $("#Magnitud").val()*1)
            $("#Magnitud").val(data[0].idmagnitud).trigger("change");
        OpcionEquipo = 0;
    }
    
    VerTipoPrecio();
    BuscarPrecio();
}

function BuscarPrecio() {
    var modelo = $("#Modelo").val() * 1;
    var marca = $("#Marca").val() * 1;
    var servicio = $("#Servicio").val() * 1;
    var magnitud = $("#Magnitud").val() * 1;
    var proveedor = $("#Proveedor").val() * 1;
    var equipo = $("#Equipo").val() * 1;
    var intervalo = $("#Intervalo").val() * 1;
    var cantidad = $("#Cantidad").val()*1;
    
    PrecioServicio = 0;
    IdPrecio = 0;
    var descuento = 0;

    if (proveedor > 0) {
        $("#Descuento").val("0");
    } else {
        descuento = DescuentoCli;
        $("#Descuento").val(DescuentoCli);
    }
    
    $("#PreciodeVenta").val("0");
    $("#Costo").val("0");
    
    $("#SubTotal").val("0");

    //alert("magnitud=" + magnitud + " servicio=" + servicio + " equipo=" + equipo + " intervalo=" + intervalo + " marca=" + marca + " modelo=" + modelo);

    if (magnitud == 0 || servicio == 0 || equipo == 0)
        return false;
        
    switch (TipoPrecio) {
        case 1:
            if (intervalo == 0)
                return false;
            break;
        case 2:
            if (marca == 0 || modelo == 0)
                return false;
            break;
    }

    
    var parametros = "magnitud=" + magnitud + "&equipo=" + equipo + "&marca=" + marca + "&modelo=" + modelo + "&intervalo=" + intervalo +
        "&precio=" + TablaPrecio + "&proveedor=" + proveedor + "&servicio=" + servicio + "&tipoprecio=" + TipoPrecio;
    var datos = LlamarAjax('Cotizacion', 'opcion=PrecioServicio&'+parametros);
    datos = datos.split("|");
    if (datos[0] == "0") {
        PrecioServicio = datos[2] * 1;
        IdPrecio = datos[1] * 1;
        $("#PreciodeVenta").val(formato_numero((PrecioServicio), _DE, _CD, _CM, ""));
        $("#SubTotal").val(formato_numero((PrecioServicio * cantidad) - ((PrecioServicio * cantidad) * descuento / 100), _DE, _CD, _CM, ""));
        $("#Costo").val(formato_numero(datos[5], _DE, _CD, _CM, ""));
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }

}

function CalcularPreServ(caja) {
    ValidarTexto(caja, 1);
    var proveedor = $("#Proveedor").val() * 1;
    var descuento = proveedor == 0 ? DescuentoCli : 0;
    var cantidad = caja.value * 1;
    $("#SubTotal").val(formato_numero((PrecioServicio * cantidad) - (PrecioServicio * cantidad * descuento / 100), _DE, _CD, _CM, ""));
}
    

function GuardarPrecio() {
    var modelo = $("#Modelo").val() * 1;
    var marca = $("#Marca").val() * 1;
    var servicio = $("#Servicio").val() * 1;
    var magnitud = $("#Magnitud").val() * 1;
    var proveedor = $("#Proveedor").val() * 1;
    var equipo = $("#Equipo").val() * 1;
    var intervalo = $("#Intervalo").val() * 1;
    var medida = $("#IdMedida").val() * 1;

    var desde = NumeroDecimal($("#MDesde").val());
    var hasta = NumeroDecimal($("#MHasta").val());

    var precio1 = NumeroDecimal($("#Precio1").val()) * 1;
    var precio2 = NumeroDecimal($("#Precio2").val()) * 1;
    var precio3 = NumeroDecimal($("#Precio3").val()) * 1;
    var precio4 = NumeroDecimal($("#Precio4").val()) * 1;
    var precio5 = NumeroDecimal($("#Precio5").val()) * 1;
    var precio6 = NumeroDecimal($("#Precio6").val()) * 1;
    var precio7 = NumeroDecimal($("#Precio7").val()) * 1;
    var precio8 = NumeroDecimal($("#Precio8").val()) * 1;
    var precio9 = NumeroDecimal($("#Precio9").val()) * 1;
    var precio10 = NumeroDecimal($("#Precio10").val()) * 1;
    var costo = NumeroDecimal($("#MCosto").val()) * 1;

    if (precio1 == 0) {
        $("#Precio1").focus();
        swal("Acción Cancelada", "Debe de ingresar el valor del precio 1", "warning");
        return false;
    }

    if (proveedor > 0 && costo == 0) {
        $("#MCosto").focus();
        swal("Acción Cancelada", "Debe de ingresar el valor del costo", "warning");
        return false;
    }

    if (TipoPrecio == 1) {
        if (hasta == 0) {
            swal("Acción Cancelada", "Debe de ingresar el rango desde y hasta del intervalo", "warning");
            return false;
        }
    }

    var parametros = "Precio_Id=" + IdPrecio + "&MTipoPrecio=" + TipoPrecio + "&MServicio=" + servicio + "&MMagnitud=" + magnitud + "&MEquipo=" + equipo + "&MMedida=" + medida + "&MMarca=" + marca + "&MModelo=" + modelo +
        "&MDesde=" + desde + "&MHasta=" + hasta + "&MProveedor=" + proveedor + "&MPrecio1=" + precio1 + "&MPrecio2=" + precio2 + "&MPrecio3=" + precio3 + "&MPrecio4=" + precio4 + "&MPrecio5=" + precio5 +
        "&MPrecio6=" + precio6 + "&MPrecio7=" + precio7 + "&MPrecio8=" + precio8 + "&MPrecio9=" + precio9 + "&MPrecio10=" + precio10 + "&MCosto=" + costo;
    var datos = LlamarAjax("Configuracion","opcion=GuardarPrecios&"+ parametros).split("|");
    if (datos[0] == "0") {
        swal("", "Precio Actualizado con éxito", "success");
        $("#modalPrecios").modal("hide");
        BuscarPrecio();
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
}

function AgregarServicio() {
    var idsolicitud = $("#DetIdSolicitud").val() * 1;
    var modelo = $("#Modelo").val() * 1;
    var marca = $("#Marca").val() * 1;
    var servicio = $("#Servicio").val() * 1;
    var magnitud = $("#Magnitud").val() * 1;
    var proveedor = $("#Proveedor").val() * 1;
    var equipo = $("#Equipo").val() * 1;
    var intervalo = $("#Intervalo").val() * 1;
    var intervalo2 = $("#Intervalo2").val() * 1;
    var ingreso = $("#Ingreso").val() * 1;
    var metodo = $("#Metodo").val()*1;
    var observacion = $.trim($("#Observacion").val());
    var calibracion = $.trim($("#Calibracion").val());
    var punto = $.trim($("#Punto").val());
    var certificado = $.trim($("#Certificado").val());
    var serie = $.trim($("#Serie").val());
    var declaracion = $.trim($("#Declaracion").val());
    var direccion = $.trim($("#Direccion").val());
    var nombreca = $.trim($("#NombreCa").val());
    var nombreca = $.trim($("#NombreCa").val());
    var calibracionadic = $.trim($("#CalibracionAdic").val());
    var precioadic = NumeroDecimal($("#PrecioAdic").val())

    direccion = direccion.replace(/&/g, '%26');
    nombreca = nombreca.replace(/&/g, '%26');
    calibracionadic = calibracionadic.replace(/&/g, '%26');

    var cantidad = $("#Cantidad").val() * 1;
    var entrega = $("#Entrega").val() * 1;
    var descuento = $("#Descuento").val() * 1;
    var costo = NumeroDecimal($("#Costo").val());

    if (proveedor > 0)
        descuento = 0;

    var tiposervicio = LlamarAjax("Cotizacion", "opcion=VerificarServicio&servicio=" + servicio) * 1;

    var mensaje = "";

    if (precioadic > 0 && calibracionadic == "")
        mensaje = "<br> Debe de ingresar la descripción de la calibración adicional" + mensaje;

    if (tiposervicio == 0 || tiposervicio == 2) {
        if (certificado == "") {
            $("#Certificado").focus();
            mensaje = "<br> Seleccione el tipo de impresión del certificado";
        }
        if (serie == "" && ingreso > 0) {
            $("#Serie").focus();
            mensaje = "<br> Ingrese la serie del equipo " + mensaje;
        }
        if (declaracion == "") {
            $("#Declaracion").focus();
            mensaje = "<br> Ingrese la declaración de conformidad del certificado " + mensaje;
        }
        if (punto == "") {
            $("#Punto").focus();
            mensaje = "<br> Ingrese el punto específico de la calibracion " + mensaje;
        }

        if (metodo == 0 && tiposervicio == 0) {
            $("#Metodo").focus();
            mensaje = "<br> Ingrese el método de la calibracion " + mensaje;
        }

        
    } else {
        if (observacion == "") {
            $("#Observacion").focus();
            mensaje = "<br> Debe de ingresar la observación del servicio " + mensaje;
        }
    }

    if (servicio == 0) {
        $("#Servicio").focus();
        mensaje = "<br> Ingrese el servicio del equipo " + mensaje;
    }
            

    if (cantidad == 0) {
        $("#Cantidad").focus();
        mensaje = "<br> Ingrese la cantidad de equipo(s) " + mensaje;
    }

        
    switch (TipoPrecio) {
        case 3:
            if (proveedor == 0) {
                $("#Proveedor").focus();
                mensaje = "<br> Ingrese el proveedor del servicio " + mensaje;
            }
            if (costo == 0) {
                mensaje = "<br> Debe de configurar el costo del servicio " + mensaje;
            }
            if (modelo == 0) {
                $("#Modelo").focus();
                mensaje = "<br> Ingrese el modelo del equipo " + mensaje;
            }
            if (marca == 0) {
                $("#Marca").focus();
                mensaje = "<br> Ingrese la Marca del Equipo " + mensaje;
            }
            break;
        case 2:
            if (modelo == 0) {
                $("#Modelo").focus();
                mensaje = "<br> Ingrese el modelo del equipo " + mensaje;
            }
            if (marca == 0) {
                $("#Marca").focus();
                mensaje = "<br> Ingrese la Marca del Equipo " + mensaje;
            }
            break;
        case 3:
            if ($("#Intervalo").val() == "") {
                $("#Intervalo").focus();
                mensaje = "<br> Ingrese el intervalo del quipo " + mensaje;
            }
            break;
    }


    if ($("#Proveedor").prop("disabled") == false) {
        
        if (proveedor == 0) {
            $("#Proveedor").focus();
            mensaje = "<br> Ingrese el proveedor del servicio " + mensaje;
        }
        if (costo == 0)
            mensaje = "<br> Debe de ingresar el costo del servicio " + mensaje;
    }

    if (equipo == 0) {
        $("#Equipo").focus();
        mensaje = "<br> Ingrese el tipo de equipo " + mensaje;
    }

    if (magnitud == 0) {
        $("#Magnitud").focus();
        mensaje = "<br> Ingrese la magnitud del equipo " + mensaje;
    }

    if (mensaje != "") {
        swal("Acción Cancelada", mensaje, "warning");
        return false;
    }

    if (PrecioServicio == 0 && TipoPrecio != 4) {
        swal("Acción Cancelada", "El servicio no tiene precio configurado", "warning");
        return false;
    }

    if (tiposervicio == 0) {
        if (entrega == 0) {
            swal("Acción Cancelada", "El servicio no tiene día de entregra configurado", "warning");
            return false;
        }
    }

    if (metodo == 0)
        metodo = -1;
    var lote = $("#ServicioLote").val() * 1;
    var parametro = "cliente=" + IdCliente + "&id=" + IdServicio + "&lote=" + lote + "&equipo=" + equipo + "&modelo=" + modelo + "&intervalo2=" + intervalo2 +
        "&observacion=" + observacion + "&serie=" + serie + "&ingreso=" + NroIngreso + "&cantidad=" + cantidad + "&intervalo=" + intervalo +
        "&servicio=" + servicio + "&metodo=" + metodo + "&punto=" + punto + "&calibracion=" + calibracion + "&declaracion=" + declaracion + "&nombreca=" + nombreca +
        "&direccion=" + direccion + "&certificado=" + certificado + "&descuento=" + descuento + "&precio=" + PrecioServicio + "&entrega=" + entrega + "&proveedor=" + proveedor + "&iva=" + IVAServicio +
        "&express=" + ExpressServ + "&sitio=" + SitioServ + "&cotizacion=" + IdCotizacion + "&remision=" + Remision + "&sede=" + IdSede + "&contacto=" + IdContacto + "&pagocertificado=" + PagoCertificado +
        "&idsolicitud=" + idsolicitud + "&costo=" + costo + "&calibracionadic=" + calibracionadic + "&precioadic=" + precioadic;
    var datos = LlamarAjax("Cotizacion", "opcion=GuardarTmpCotizacion&" + parametro);
    datos = datos.split("|");
    if (datos[0] == "0") {
        Estado = "Temporal";
        $("#Estado").val(Estado);
        swal("", datos[1], "success");
        TablaIngreso(IdCliente, 0, IdCotizacion, 0, 0);
        IdServicio = datos[2] * 1;
        LimpiarServicio();
        if (datos[1] == "Servicio actualizado") {
            $("#ModalArticulos").modal("hide");
        }
    } else
        swal("Acción Cancelada", datos[1], "warning");
}

function LlamarImprimir() {
    var numero = $("#Cotizacion").val() * 1;
    ImprimirCotizacion(numero, 0);
}

function ImprimirCotizaAnt() {
    if (CotizacionesAnt > 0) {
        ImprimirCotizacion(CotizacionesAnt, 0);
    }
}

function ImprimirServicio(descargar) {
	Pdf_Cotizacion = "";
	ActivarLoad();
    setTimeout(function () {
		if (Cotizacion * 1 != 0) {
			var datos = LlamarAjax("Configuracion", "opcion=GenerarPDF&tipo=Cotizacion_Servicio&documento=" + Cotizacion).split("||");
			DesactivarLoad();
			if (datos[0] == "0"){
				if (descargar == 0)
					window.open(url_cliente + "DocumPDF/" + datos[1]);
				else{
					$("#resultadopdfcotizacion").attr("src", url_cliente + "DocumPDF/" + datos[1]);
					Pdf_Cotizacion = datos[1];
				}
			}else{
				swal("Acción Cancelada",datos[1],"warning");
			}
		}
	},15);
}

function CambioCotizacion(opcion) {
    
    if (opcion == "Servicio")
        ImprimirServicio(1);
    else
        ImprimirCotizacion(Cotizacion, 1);

}


function ImprimirCotizacion(cotizacion, descargar) {
    Pdf_Cotizacion = ""
    ActivarLoad();
    setTimeout(function () {
		if (cotizacion * 1 != 0) {
			var datos = LlamarAjax("Configuracion", "opcion=GenerarPDF&tipo=Cotizacion&documento=" + cotizacion).split("||");
			DesactivarLoad();
			if (datos[0] == "0"){
				if (descargar == 0)
					window.open(url_cliente + "DocumPDF/" + datos[1]);
				else{
					$("#resultadopdfcotizacion").attr("src", url_cliente + "DocumPDF/" + datos[1]);
					Pdf_Cotizacion = datos[1];
				}
			}else{
				swal("Acción Cancelada",datos[1],"warning");
			}
		}
	},15);
}


function AgregarOtroServicio() {

    var modelo = 0;
    var servicio = $("#OServicio").val() * 1;
    var ingreso = $("#OIngreso").val() * 1;
    var cantidad = NumeroDecimal($("#OCantidad").val()) * 1;
    var proveedor = 0;
    var equipo = 0;
    var intervalo = 0;
    var metodo = 0;
    var observacion = $.trim($("#OObservacion").val());
    var calibracion = "NULL";
    var punto = "NULL";
    var certificado = "NULL";
    var serie = "NULL";
    var declaracion = "NULL";
    var direccion = "NULL";
    var precio = NumeroDecimal($("#OPrecio").val())*1;

        
    var mensaje = "";

    if (observacion == "") {
        $("#OObservacion").focus();
        mensaje = "<br> Debe de ingresar la observación " + mensaje;
    }

    if (cantidad == 0) {
        $("#OCantidad").focus();
        mensaje = "<br> Debe de ingresar la cantidad del servicios " + mensaje;
    }

    if (precio == 0) {
        $("#OPrecio").focus();
        mensaje = "<br> Debe de ingresar el precio del servicios " + mensaje;
    }

    if (servicio == 0) {
        $("#OServicio").focus();
        mensaje = "<br> Seleccione el servicio " + mensaje;
    }

    if ($.trim($("#OIngreso").val()) == "") {
        $("#OIngreso").focus();
        mensaje = "<br> Ingrese el número de ingreso" + mensaje;
    }

    if (mensaje != "") {
        swal("Acción Cancelada", mensaje, "warning");
        return false;
    }

    observacion = observacion.replace(/&/g, '%26');
        
    var parametro = "cliente=" + IdCliente + "&id=" + IdServicio + "&equipo=" + equipo + "&modelo=" + modelo +
        "&observacion=" + observacion + "&serie=" + serie + "&ingreso=" + ingreso + "&cantidad=" + cantidad + "&intervalo=" + intervalo + "&intervalo2=0" +
        "&servicio=" + servicio + "&metodo=" + metodo + "&punto=" + punto + "&calibracion=" + calibracion + "&declaracion=" + declaracion +
        "&direccion=" + direccion + "&certificado=" + certificado + "&descuento=" + DescuentoCli + "&precio=" + precio + "&entrega=0&proveedor=" + proveedor + "&iva=" + ValorIva +
        "&express=0&sitio=0" + SitioServ + "&cotizacion=" + IdCotizacion + "&remision=" + Remision + "&sede=" + IdSede + "&contacto=" + IdContacto + "&idsolicitud=0&lote=0&costo=0&calibracionadic=&precioadic=0";
    var datos = LlamarAjax("Cotizacion", "opcion=GuardarTmpCotizacion&" + parametro);
    datos = datos.split("|");
    if (datos[0] == "0") {
        $("#ModalOtroServicio").modal("hide");
        Estado = "Temporal";
        $("#Estado").val(Estado);
        swal("", datos[1], "success");
        TablaIngreso(IdCliente, 0, IdCotizacion, 0, 0);
        IdServicio = datos[2] * 1;
    } else
        swal("Acción Cancelada", datos[1], "warning");
}



function GuardarIntervalo() {
    var magnitud = $("#Magnitud").val() * 1;
    var medida = $("#IMedida option:selected").text();
    var desde = $.trim($("#IDesde").val());
    var hasta = $.trim($("#IHasta").val());

    var mensaje = "";

  
    if ($.trim($("#IHasta").val()) == "") {
        $("#IHasta").focus();
        mensaje = "<br> *Ingrese el rango hasta" + mensaje;
    }
    if ($.trim($("#IDesde").val()) == "") {
        $("#IDesde").focus();
        mensaje = "<br> *Ingrese el rango desde" + mensaje;
    }

    if (medida == "--Seleccione--") {
        $("#IMedida").focus();
        mensaje = "<br> *Seleccione una unidad de medida" + mensaje;
    }

    if (mensaje != "") {
        swal("Verifique los siguientes campos", mensaje, "warning");
        return false;
    }

    var parametros = "magnitud=" + magnitud + "&medida=" + medida + "&desde=" + desde + "&hasta=" + hasta;
    var datos = LlamarAjax("Cotizacion","opcion=GuardarIntervalo&"+ parametros);
    if (datos * 1 > 0) {
        $("#Intervalo").html(CargarCombo(6, 1, "", magnitud));
        $("#Intervalo").val(datos).trigger("change");
        $("#modalIntervalo").modal("hide");
    } else {
        swal("Acción Cancelada", "Ya existe un rango de intervalo con estas descripciones", "warning");
    }
}

function CargarEditarPrecios() {

    var modelo = $("#Modelo").val() * 1;
    var marca = $("#Marca").val() * 1;
    var servicio = $("#Servicio").val() * 1;
    var magnitud = $("#Magnitud").val() * 1;
    var proveedor = $("#Proveedor").val() * 1;
    var equipo = $("#Equipo").val() * 1;
    var intervalo = $("#Intervalo").val() * 1;


    if (magnitud == 0 || servicio == 0 || equipo == 0)
        return false;


    switch (TipoPrecio) {
        case 1:
            if (intervalo == 0) {
                swal("Acción Cancelada", "Debe seleccionar un intervalo", "warning");
                return false;
            }
                
            break;
        case 2:
            if (marca == 0 || modelo == 0) {
                swal("Acción Cancelada", "Debe seleccionar una marca y modelo", "warning");
                return false;
            }
            break;
        case 3:
            if (marca == 0 || modelo == 0 || proveedor == 0) {
                swal("Acción Cancelada", "Debe seleccionar una marca, modelo y proveedor", "warning");
                return false;
            }
            break;
    }

    ActivarLoad();
    setTimeout(function () {

        $("#divMedida").html("");
        $("#IdMedida").val(0);
        $("#MDesde").prop("readonly", true);
        $("#MDesde").val("0");
        $("#MHasta").prop("readonly", true);
        $("#MHasta").val("0");
        $("#MCosto").prop("readonly", true);

        if (proveedor > 0)
            $("#MCosto").prop("readonly", false);

        if (TipoPrecio == 1) {
            var datos = LlamarAjax("Cotizacion","opcion=MedidaIntervalo&id=" + intervalo);
            var data = JSON.parse(datos);
            $("#divMedida").html(data[0].medida);
            $("#IdMedida").val(data[0].id);
            $("#MDesde").prop("readonly", false);
            $("#MDesde").val("");
            $("#MHasta").prop("readonly", false);
            $("#MHasta").val("");
        }
        
        $("#divMagnitud").html($("#Magnitud option:selected").text());
        $("#divEquipo").html($("#Equipo option:selected").text());
        $("#divMarca").html($("#Marca option:selected").text());
        $("#divModelo").html($("#Modelo option:selected").text());
        $("#divServicio").html($("#Servicio option:selected").text());
        $("#divProveedor").html($("#Proveedor option:selected").text());
        $("#divIntervalo").html($("#Intervalo option:selected").text());

        $("#Precio1").val("");
        $("#Precio2").val("");
        $("#Precio3").val("");
        $("#Precio4").val("");
        $("#Precio5").val("");
        $("#Precio6").val("");
        $("#Precio7").val("");
        $("#Precio8").val("");
        $("#Precio9").val("");
        $("#Precio10").val("");
        $("#MCosto").val("");
        
        if (IdPrecio > 0) {
            var datos = LlamarAjax("Cotizacion", "opcion=TablaPrecioServicio&id=" + IdPrecio);
            if (datos != "[]") {
                var data = JSON.parse(datos);
                $("#Precio1").val(formato_numero(data[0].precio1, _DE, _CD, _CM, ""));
                $("#Precio2").val(formato_numero(data[0].precio2, _DE, _CD, _CM, ""));
                $("#Precio3").val(formato_numero(data[0].precio3, _DE, _CD, _CM, ""));
                $("#Precio4").val(formato_numero(data[0].precio4, _DE, _CD, _CM, ""));
                $("#Precio5").val(formato_numero(data[0].precio5, _DE, _CD, _CM, ""));
                $("#Precio6").val(formato_numero(data[0].precio6, _DE, _CD, _CM, ""));
                $("#Precio7").val(formato_numero(data[0].precio7, _DE, _CD, _CM, ""));
                $("#Precio8").val(formato_numero(data[0].precio8, _DE, _CD, _CM, ""));
                $("#Precio9").val(formato_numero(data[0].precio9, _DE, _CD, _CM, ""));
                $("#Precio10").val(formato_numero(data[0].precio10, _DE, _CD, _CM, ""));
                $("#MCosto").val(formato_numero(data[0].costo, _DE, _CD, _CM, ""));
                $("#MDesde").val(formato_numero(data[0].desde, _DE, _CD, _CM, ""));
                $("#MHasta").val(formato_numero(data[0].hasta, _DE, _CD, _CM, ""));
            }
        }
        DesactivarLoad();
        $("#modalPrecios").modal("show");
    }, 15);
}

function CambioSede(sede) {
    IdSede = sede;
}

function AgregarMetodo() {

    var equipo = $("#Equipo").val() * 1;
    if (equipo == 0) {
        $("#Equipo").focus();
        swal("Acción Cancelada", "Debe seleccionar primero un equipo", "warning");
        return false;
    }

    ActivarLoad();
    setTimeout(function () {
        var datos = LlamarAjax('Configuracion','opcion=TablaConfiguracion&tipo=9&id=0&estado=1');
        var datasedjson = JSON.parse(datos);
        DesactivarLoad();
        $('#TablaMetodosCot').DataTable({
            data: datasedjson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "id" },
                { "data": "descripcion" },
                { "data": "equipos" },
            ],
            "language": {
                "url": LenguajeDataTable
            },
            "lengthMenu": [[5], [5]],   
        });

        $("#EquipoMetodo").val($("#Equipo option:selected").text());
        $("#modalMetodo").modal("show");
        $("#MMetodo").val("");
        setTimeout(function () {
            $("#MMetodo").focus();
        }, 500);
    }, 15);
}

$("#TablaMetodosCot > tbody").on("dblclick", "tr", function (e) {
    var row = $(this).parents("td").context.cells;
    $("#MMetodo").val(row[1].innerText);
    $("#MMetodo").focus();
});

function GuardarMetodo() {
    var equipo = $("#Equipo").val() * 1;
    metodo = $.trim($("#MMetodo").val())
    if (metodo == "") {
        $("#MMetodo").focus();
        swal("Acción Cancelada", "Debe ingresar el método de calibración del equipo", "warning");
        return false;
    }

    ActivarLoad();
    setTimeout(function () {
        var parametros = "equipo=" + equipo + "&descripcion=" + metodo;
        var datos = LlamarAjax("Cotizacion", "opcion=GuardarMetodo&"+ parametros).split("|");
        DesactivarLoad();
        if (datos[0] == "0") {
            $("#Metodo").html(CargarCombo(17, 1, "", equipo));
            $("#Metodo").val(datos[2]).trigger("change");
            $("#Metodo").focus();
            $("#modalMetodo").modal("hide");
        } else {
            $("#MMetodo").focus();
            swal("Acción Cancelada", datos[1], "warning");
        }
    }, 15);
}

function DatosContacto(contacto) {
    $("#Email").val("");
    $("#Telefonos").val("");
    if (contacto * 1 == 0)
        return false;
    var datos = LlamarAjax("Cotizacion","opcion=DatosContacto&id=" + contacto);
    IdContacto = contacto;
    if (datos != "[]") {
        var data = JSON.parse(datos);
        CorreoEmpresa = data[0].email;
        $("#Email").val(data[0].email);
        $("#Telefonos").val(data[0].telefonos);
    }
}

function AgregarIntervalo() {

    if ($("#Intervalo").prop("disabled") == true)
        return false;
        
    var magnitud = $("#Magnitud").val() * 1;
    if (magnitud == 0) {
        $("#Magnitud").focus();
        swal("Acción Cancelada", "Debe de seleccionar una magnitud", "warning");
        return false;
    }

    $("#IDesde").val("");
    $("#IHasta").val("");
    $("#IMedida").val("").trigger("change");

    $("#modalIntervalo").modal("show");

}
    

function AgregarOpcion(tipo, mensaje, id) {

    var magnitud = $("#Magnitud").val() * 1;
    var marca = $("#Marca").val() * 1
       
    switch (tipo) {

        case 1:
            if (magnitud == 0) {
                $("#Magnitud").focus();
                swal("Acción Cancelada", "Debe de seleccionar una magnitud", "warning");
                return false;
            }
            break;
        case 3:
            if (marca == 0) {
                $("#Marca").focus();
                swal("Acción Cancelada", "Debe de seleccionar una marca", "warning");
                return false;
            }
            break;
    }
            
    swal({
        title: 'Agregar Opción',
        text: mensaje,
        input: 'text',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Agregar'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',

        inputValidator: function (value) {
            return new Promise(function (resolve, reject) {
                if ($.trim(value)) {
                    var datos = LlamarAjax("Cotizacion","opcion=GuardarOpcion&tipo=" + tipo + "&magnitud=" + magnitud + "&marca=" + marca + "&descripcion=" + value + "&tipoconcepto=0");
                    datos = datos.split("|");
                    if (datos[0] != "XX") {
                        $("#" + id).html(datos[1]);
                        $("#" + id).val(datos[0]).trigger("change");
                        resolve();
                    } else {
                        reject(datos[1]);
                    }

                } else {
                    reject(ValidarTraduccion('Debe de ingresar la descripción de ' + mensaje));
                }
            })
        }
    }).then(function (result) {
        $("#" + id).focus();
        BuscarPrecio();
    })

}

function LimpiarDatos() {

    if (DocumentoCli != "")
        DesbloquearCliente(DocumentoCli, 0);
    TablaPrecio = 0;
    DescuentoCli = 0;
    IdCliente = 0;
    DocumentoCli = "";
    TipoCliente = 0;
    PlazoPago = 0;
    TipoPrecio = 0;
    CorreoEmpresa = "";
    IdPrecio = 0;
    IdRemision = 0;
    PrecioServicio = 0;
    OpcionEquipo = 0;   
    Remision = 0;
    IdCotizacion = 0;
    Cotizacion = 0;
    Estado = "Temporal";
    PagoCertificado = 'NO';
    CotizacionesAnt = 0;
    CanItems = 0;
    IdContacto = 0;
    IdSede = 0;
    Solicitud = 0;
    RequiereSolicitud = "NO";
    totales = 0;
    subtotal = 0;
    descuento = 0;
    exento = 0;
    gravable = 0;
    iva = 0;

    $("#Solicitud").val("");
    $("#NombreCliente").val("");
    $("#Cotizacion").val("");
    $("#Remision").val("");
    $("#Remision").prop("disabled", false);
    $("#Cotizacion").prop("disabled", false);
    $("#Estado").val(Estado);
    $("#Sede").html("");
    $("#Contacto").html("");
    $("#SaldoClienteDev").html("");
    $("#Validez").html(CargarCombo(81, 0));

    $("#Revision").val("1");
    $("#CotizacionesAnt").val("");
    $("#ObserReemplazo").val("");

    $(".boton").removeClass("hidden");
    $(".boton3").removeClass("hidden");

    $("#Email").val("");
    $("#Telefonos").val("");
    $("#TipoCliente").val("");
    $("#TablaPrecio").val("");
    $("#PlazoPago").val("");
    $("#Observaciones").val("");
    $("#CotizacionesAnt").val("");

    $("#LimpiarServicio").removeClass("hidden");

    $("#tsubtotal").val("0");
    $("#tdescuento").val("0");
    $("#tbaseimponible").val("0");
    $("#texcento").val("0");
    $("#tiva").val("0");
    $("#ttotalpagar").val("0");

    $("#bodyCotizacion").html("");

}

function ValidarCliente(documento) {
    if ($.trim(DocumentoCli) != "") {
        if (DocumentoCli != documento)
            LimpiarDatos();
    }
}

function CalcularPOSer(caja) {
    ValidarTexto(caja, 1);
    var cantidad = NumeroDecimal($("#OCantidad").val()) * 1;
    var precio = NumeroDecimal($("#OPrecio").val()) * 1;
    $("#OSubTotal").val(formato_numero((cantidad * precio) - ((cantidad * precio) * DescuentoCli / 100), 0,_CD,_CM,""));
}

function CalcularPrecioSer(caja) {
    ValidarTexto(caja, 1);
    $("#SubTotalSer").val(formato_numero(NumeroDecimal(caja.value) - (NumeroDecimal(caja.value) * DescuentoCli / 100), 0,_CD,_CM,""));
}

function SelecccionarFila(id, solicitud, lote) {

    ActivarLoad();
    setTimeout(function () {

        var datos = LlamarAjax("Cotizacion", "opcion=BuscarServicio&id=" + id + "&lote=" + lote);
        var data = JSON.parse(datos);

        DesactivarLoad();

        if (data[0].idserviciodep * 1 > 0 && data[0].idserviciodep * 1 != 99999)
            return false;

        IdServicio = data[0].id * 1;
        DescuentoCli = data[0].descuento * 1;

        if (data[0].idequipo != 0) {
            $("#Descuento").val(DescuentoCli);
            $("#ServicioLote").val(lote);
            $("#DetIdSolicitud").val(data[0].idsolicitud);
            $("#Equipo").val(data[0].idequipo).trigger("change");
            $("#Marca").val(data[0].idmarca).trigger("change");
            $("#Modelo").val(data[0].idmodelo).trigger("change");
            $("#NombreCa").val(data[0].nombreca);
            IntervaloAnt = data[0].idintervalo;
            $("#Intervalo").val(data[0].idintervalo).trigger("change");
            $("#Intervalo2").val((data[0].idintervalo2 > 0 ? data[0].idintervalo : "")).trigger("change");
            $("#Servicio").val((data[0].idservicio ? data[0].idservicio : "")).trigger("change");
            $("#Proveedor").val(data[0].idproveedor).trigger("change");
            $("#Ingreso").val(data[0].ingreso);
            NroIngreso = data[0].ingreso;
            $("#Serie").val(data[0].serie);
            $("#Metodo").val(data[0].metodo * 1 > 0 ? data[0].metodo : "").trigger("change");
            $("#Calibracion").val(data[0].proxima);
            $("#Certificado").val(data[0].certificado).trigger("change");
            $("#Declaracion").val(data[0].declaracion);
            $("#Punto").val(data[0].punto);
            $("#Observacion").val(data[0].observacion);
            $("#Direccion").val((data[0].direccion ? data[0].direccion : "SEDE"));
            DiasEntrega = data[0].entrega * 1;
            IVAServicio = ValorIva > 0 ? (data[0].iva * 1) : 0;
            $("#Cantidad").val(data[0].cantidad);
            $("#Entrega").val(DiasEntrega);

            $("#PreciodeVenta").val(formato_numero(data[0].precio, 0,_CD,_CM,""));
            PrecioServicio = data[0].precio * 1;

            var cantidad = data[0].cantidad * 1;
            $("#SubTotal").val(formato_numero((PrecioServicio * cantidad) - (PrecioServicio * cantidad * DescuentoCli / 100), 0, _CD, _CM, ""));

            $("#CalibracionAdic").val(data[0].calibracionadic);
            $("#PrecioAdic").val(formato_numero(data[0].precioadic, _DE, _CD, _CM, ""));

            if (data[0].ingreso * 1 > 0) {
                $("#Magnitud").attr("disabled", true);
                $("#Equipo").attr("disabled", true);
                $("#Marca").attr("disabled", true);
                $("#Modelo").attr("disabled", true);
                $("#Intervalo").attr("disabled", true);
                $("#Intervalo2").attr("disabled", true);
                $("#Ingreso").attr("disabled", true);
                $("#Serie").attr("disabled", true);
                $("#Cantidad").attr("disabled", true);
            } else {
                $("#Magnitud").attr("disabled", false);
                $("#Equipo").attr("disabled", false);
                $("#Marca").attr("disabled", false);
                $("#Modelo").attr("disabled", false);
                $("#Intervalo").attr("disabled", false);
                $("#Intervalo2").attr("disabled", false);
                $("#Ingreso").attr("disabled", false);
                $("#Serie").attr("disabled", false);
                $("#Cantidad").attr("disabled", false);
            }

            $("#ModalArticulos").modal({ backdrop: 'static', keyboard: false }, 'show');


        } else {
            $("#OServicio").prop("disabled", false);
            $("#OObservacion").prop("disabled", false);
            $("#OServicio").val(data[0].idservicio).trigger("change");
            $("#OIngreso").val(data[0].ingreso);
            $("#OCantidad").val(data[0].cantidad);
            $("#OObservacion").val(data[0].observacion);
            $("#ODescuento").val(DescuentoCli);
            $("#OPrecio").val(formato_numero(data[0].precio, 0,_CD,_CM,""));
            $("#OSubTotal").val(formato_numero((data[0].precio * 1) - (data[0].precio * 1 * DescuentoCli / 100), 0,_CD,_CM,""));
            $("#ModalOtroServicio").modal("show");
        }
    }, 15);

    

}

function CargarModalOServicio() {

    if ((Estado == 'Temporal' || Estado == "Cotizado") && (IdCliente > 0)) {

        if (IdSede == 0) {
            $("#Sede").focus();
            swal("Acción Cancelada", "Debe seleccionar la sede del cliente", "warning");
            return false
        }
        if (IdContacto == 0) {
            $("#Contacto").focus();
            swal("Acción Cancelada", "Debe seleccionar el contacto del cliente", "warning");
            return false
        }

        if (RequiereSolicitud == "SI" && CanItems == 0 && Habitual == "NO" && Solicitud == 0) {
            swal("Acción Cancelada", "Debe ingresar la solicitud del cliente", "warning");
            return false;
        }
            
        LimpiarOtroServicio();
        $("#ModalOtroServicio").modal("show");
        setTimeout(function () {
            $("#OServicio").focus();
        }, 500);
    }
    
}

function LimpiarOtroServicio() {
    IdServicio = 0;
    DiasEntrega = 0;
    ExpressServ = 0;
    SitioServ = 0;
    DescuentoCli = DescuentoCliente;
    $("#OCantidad").val("1");
    $("#OPrecio").val("");
    $("#OIngreso").val("0");
    $("#OSubTotal").val("");
    $("#OServicio").val("").trigger("change");
    $("#OServicio").prop("disabled",false);
    $("#OObservacion").val("");
    $("#ODescuento").val(DescuentoCli);
}

function AgregarPrecio(x, id) {
    var datos = LlamarAjax("Cotizacion", "opcion=BuscarServicio&id=" + id + "&lote=0");
    var data = JSON.parse(datos);
    if ((Estado == 'Temporal' || Estado == "Cotizado") && (IdCliente > 0)) {
        IdServicio = id;
        $("#OServicio").prop("disabled", true);
        $("#OObservacion").prop("disabled", true);
        $("#OIngreso").val(data[0].ingreso);
        $("#OServicio").val(data[0].idservicio).trigger("change");
        $("#OServicio").val(data[0].idservicio).trigger("change");
        $("#OCantidad").val(data[0].cantidad);
        $("#OObservacion").val(data[0].observacion);
        $("#ODescuento").val(DescuentoCli);
        $("#OPrecio").val(formato_numero(data[0].precio, 0,_CD,_CM,""));
        $("#OSubTotal").val(formato_numero((data[0].precio * 1) - (data[0].precio * 1 * DescuentoCli / 100), 0,_CD,_CM,""));
        $("#ModalOtroServicio").modal("show");
    }
}


function GuardarPrecioSer() {
    var id = $("#IdSerPre").val() * 1;
    var precio = NumeroDecimal($("#PrecioSer").val()) * 1;
    var observacion = $.trim($("#ObservacionPre").val());

    if (precio <= 0) {
        $("#PrecioSer").focus();
        swal("Acción Cancelada", "Debe de ingresar el precio del servicio", "warning");
        return false;
    }

    var parametros = "id=" + id + "&precio=" + precio + "&descuento=" + DescuentoCli + "&observacion=" + observacion;
    var datos = LlamarAjax("Cotizacion","opcion=GuardarPrecioSer&" + parametros).split("|");
    if (datos[0] == "0") {
        TablaIngreso(IdCliente, 0, IdCotizacion, 0, 0);
        $("#modalPrecioSer").modal("hide");
        swal("", datos[1], "success");
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
}
    

function CargarTablaIngreso(datostabla) {
    
    var resultado = "";
    $("#bodyCotizacion").html("");

    totales = 0;
    subtotal = 0;
    descuento = 0;
    exento = 0;
    gravable = 0;

    iva = 0;
    CanItems = 0;
    var solicitud = "";
    
    if (datostabla != "[]") {
        var data = JSON.parse(datostabla);
        IdSede = data[0].idsede;
        IdContacto = data[0].idcontacto;
        $("#Sede").val(data[0].idsede).trigger("change");
        $("#Contacto").val(data[0].idcontacto).trigger("change");
                    
        
        $("#Remision").prop("disabled", true);
        if (data[0].remision * 1 > 0) {
            $("#LimpiarServicio").addClass("hidden");
        }
        
        for (var x = 0; x < data.length; x++) {
            if (data[x].remision * 1 > 0 && Remision == 0) {
                Remision = data[x].remision;
                $("#Remision").val(Remision);
            }
            solicitud = "&nbsp;";
            if (data[x].idsolicitud * 1 > 0) {
                Solicitud = data[x].solicitud;
                solicitud = "<b>" + data[x].solicitud + "</b><br>" + data[x].fechasoli + "<br>" + data[x].serviciosoli;
            } 
                
            CanItems += 1;
            resultado += "<tr " + (x % 2 == 1 ? " class='bg-gris' " : "") + "ondblclick='SelecccionarFila(" + data[x].id + "," + data[x].idsolicitud + "," + data[x].lote + ")'>" +
                "<td title='" + data[x].orden + "'>" + (x + 1) + "</td>" +
                "<td>" + solicitud + "</td>" +
                "<td>" + (data[x].idequipo != 0 ? "<b> " + data[x].ingreso + " </b > <br>Fecha " + data[x].fechaing + "<br>Usuario " + data[x].usuario : "&nbsp;") + "</td>" +
                "<td>" + (data[x].idequipo != 0 ? "<b>" + data[x].magnitud + "</b><br>" + data[x].equipo + "<br>" + data[x].intervalos +
                    "<br><b>" + data[x].marca + "</b><br>" + data[x].modelo + "<br><b>Serie:</b>" + data[x].serie : "&nbsp;") + "</td>" +
                "<td><div name='Reportes[]'>" + (data[x].reporte ? data[x].reporte : "&nbsp;") + "</div></td>" +
                "<td>" + (data[x].servicio ? data[x].servicio : "&nbsp;") +
                (data[x].proveedor ? "<br><b>Proveedor:</b>" + data[x].proveedor : "&nbsp;") +
                (data[x].metodo ? "<br><b>Método:</b>" + data[x].metodo : "&nbsp;") +
                (data[x].calibracionadic ? "<br><b>Adicional:</b>" + data[x].calibracionadic : "&nbsp;") +
                (data[x].observacion ? "<br><b>Obser:</b>" + data[x].observacion : "&nbsp;") + "</td>" +
                "<td>" + (data[x].punto ? "<b>Punto de calibración: </b>" + data[x].punto : "&nbsp;") +
                (data[x].nombreca ? "<br><b>A nombre: </b>" + data[x].nombreca : "&nbsp;") +
                (data[x].direccion ? "<br><b>Dirección: </b>" + data[x].direccion : "&nbsp;") +
                (data[x].declaracion ? "<br><b>Declaracion de conformidad:</b>" + data[x].declaracion : "&nbsp;") +
                (data[x].certificado ? "<br><b>Certificado:</b>" + data[x].certificado : "&nbsp;") +
                (data[x].proxima ? "<br><b>Próxima calibración:</b>" + data[x].proxima : "&nbsp;") + "</td>" +
                "<td align='center' class='text-XX'>" + (data[x].entrega ? data[x].entrega + " días" : "&nbsp;") + "</td> " +
                "<td align='center' class='text-XX'>" + (data[x].idserviciodep > 0 || data[x].precio > 0 ? "<a href=\"javascript:ActualizarPrecios(" + data[x].id + "," + data[x].precio + "," + data[x].descuento + "," + data[x].lote + "," + data[x].cantidad + ")\">" + (data[x].precio ? formato_numero(data[x].precio, _DE, _CD, _CM, "") : "Agregar Precio") + "</a>" : (data[x].precio ? formato_numero(data[x].precio, _DE, _CD, _CM, "") : "&nbsp;")) + "<input type='hidden' value='" + data[x].precio + "' name='PrecioCoti[]'></td> " +
                "<td align='center' class='text-XX'>" + data[x].descuento + "</td> " +
                "<td align='center' class='text-XX'>" + data[x].cantidad + "</td>" +
                "<td align='center' class='text-XX'>" + formato_numero((data[x].precio * data[x].cantidad) - (data[x].precio * data[x].cantidad * data[x].descuento / 100), 0, _CD, _CM, "") + "</td> " +
                "<td align='center' class='text-XX'>" + data[x].iva + "<input type='hidden' value='" + data[x].idequipo + "' name='tempequipo[]'>" +
                "<input type='hidden' value='" + data[x].idservicio + "' name='tempservicio[]'>" +
                "<input type='hidden' name='Metodos[]' value='" + data[x].idmetodo + "'></td>" +
                "<input type='hidden' name='Ingresos[]' value='" + data[x].ingreso + "'></td>" +
                "<input type='hidden' name='Otros[]' value='" + data[x].otro + "'></td>" +

                "<td align='center'>" + (data[x].cantidad == 1 ? "<button class='btn btn-primary' title='Dublicar Registro' type='button' onclick=\"DuplicarRegistro(" + data[x].id + ")\"><span data-icon='&#xe064;'></span></button><br>" : "") +
                "<button class='btn btn-danger' title='Eliminar Registro' type='button' onclick=\"EliminarCotizacion(" + data[x].id + "," + data[x].lote + ",'" + escape(data[x].equipo + " " + data[x].marca + " " + data[x].modelo) + "'," + data[x].cantidad + ")\"><span data-icon='&#xe0d8;'></span></button><br>" +
                (data[x].idequipo > 0 ? "<button class='btn btn-success' title='Ver Fotos' type='button' onclick='CargarModalFoto(" + data[x].ingreso + "," + data[x].fotos + ",1)'><span data-icon='&#xe2c7;'></span></button>" : "&nbsp;") +
                "<br><button class='btn btn-warning' title='Cambiar Datos del Certificado' type='button' onclick=\"CambioCertificado(" + data[x].id + "," + data[x].lote + ",'" + data[x].equipo + " " + data[x].marca + " " + data[x].modelo + "'," + data[x].ingreso + ")\"><span data-icon='&#xe07f;'></span></button></td></tr>";

            subtotal += (data[x].precio * data[x].cantidad)
            descuento += Math.trunc(data[x].precio * data[x].cantidad * data[x].descuento / 100);
            gravable += Math.trunc((data[x].precio * data[x].cantidad) - (data[x].precio * data[x].cantidad * data[x].descuento / 100));
            iva += Math.trunc(((data[x].precio * data[x].cantidad) - (data[x].precio * data[x].cantidad * data[x].descuento / 100))*data[x].iva/100);
                        
        }
    }

    $("#Solicitud").val(Solicitud); 
    $("#bodyCotizacion").html(resultado);

    retefuente = Math.trunc((subtotal - descuento) * CliReteFuente / 100);
    reteica = Math.trunc((subtotal - descuento) * CliReteIca / 100);
    reteiva = Math.trunc(iva * CliReteIva / 100);

    totales = subtotal - descuento + iva - retefuente - reteica - reteiva;


    $("#tsubtotal").val(formato_numero(subtotal, _DE, _CD, _CM, ""));
    $("#tdescuento").val(formato_numero(descuento, _DE,_CD,_CM,""));
    $("#tbaseimponible").val(formato_numero(gravable, _DE,_CD,_CM,""));
    $("#texcento").val(formato_numero(exento, _DE,_CD,_CM,""));
    $("#tiva").val(formato_numero(iva, _DE,_CD,_CM,""));
    $("#ttotalpagar").val(formato_numero(totales, _DE, _CD, _CM, ""));

    $("#tretefuente").val(formato_numero(retefuente, _DE, _CD, _CM, ""));
    $("#treteiva").val(formato_numero(reteiva, _DE, _CD, _CM, ""));
    $("#treteica").val(formato_numero(reteica, _DE, _CD, _CM, ""));

}

function CambioCertificado(id, lote, equipo, ingreso) {
    $("#IdCertificado").val(id);
    $("#LoteCertificado").val(lote);
    $("#IngresoCer").val(ingreso);
    $("#EquipoCer").val(equipo);
    $("#NombreCer").val("");
    $("#DireccionCer").val("");
    $("#modalCambioCertificado").modal("show");
}

function CambiarCertificado() {
    var id = $("#IdCertificado").val() * 1;
    var lote = $("#LoteCertificado").val() * 1;
    var nombre = $.trim($("#NombreCer").val());
    var direccion = $.trim($("#DireccionCer").val());
    var parametros = "nombre=" + nombre + "&direccion=" + direccion + "&id=" + id + "&lote=" + lote;
    var datos = LlamarAjax("Cotizacion","opcion=CambiarCertificado&" + parametros).split("|");
    if (datos[0] == "0") {
        $("#modalCambioCertificado").modal("hide");
        swal("", datos[1], "success");
        TablaIngreso(IdCliente, 0, IdCotizacion, 0, 0);
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
}


function GuardarCotizacion() {

    
    var observacion = $.trim($("#Observaciones").val());
    
    var mensaje = "";
        
    if ($("#bodyCotizacion").html() != "") {
        if (IdCliente != 0) {
            if (Solicitud == 0 && RequiereSolicitud == "SI" && Habitual == "NO" && IdCotizacion == 0) {
                $("#Solicitud").focus();
                mensaje = "<br> Debe de ingresar la solicitud del cliente"
            }
            if (IdContacto == 0) {
                $("#Contacto").focus();
                mensaje = "<br> Debe de seleccionar un contacto del cliente" + mensaje
            }
            if (IdSede == 0) {
                $("#Sede").focus();
                mensaje = "<br> Debe de seleccionar una sede del cliente " + mensaje;
            }

            if (mensaje != "") {
                swal("Acción Cancelada", mensaje, "warning");
                return false;
            }

            var validez = $("#Validez").val();
            var precios = document.getElementsByName("PrecioCoti[]");
            var otroprecios = document.getElementsByName("Otros[]");
            var canequipos = 0;
            for (var x = 0; x < precios.length; x++) {
                if ((precios[x].value * 1 == 0) && (otroprecios[x].value*1 != 3)) 
                    canequipos += 1;
            }
                        
            if (canequipos > 0) {
                swal("Acción Cancelada", "Hay " + canequipos + " servicios que debe de agregar el precio", "warning");
                return false;
            }

            var parametros = "id=" + IdCotizacion + "&cliente=" + IdCliente + "&sede=" + IdSede + "&contacto=" + IdContacto + "&observaciones=" + observacion +
                "&total=" + totales + "&subtotal=" + subtotal + "&descuento=" + descuento + "&gravable=" + gravable +
                "&exento=" + exento + "&iva=" + iva + "&cotizacionant=0&solicitud=" + Solicitud + "&reteiva=" + reteiva + "&retefuente=" + retefuente + "&reteica=" + reteica + "&validez=" + validez;
            var datos = LlamarAjax("Cotizacion", "opcion=GuardarCotizacion&" + parametros);
            datos = datos.split("|");
            if (datos[0] == "0") {
                IdCotizacion = datos[3]*1;
                Cotizacion = datos[2]*1;
                $("#Cotizacion").val(Cotizacion);
                Estado = "Cotizado";
                $("#Estado").val(Estado);
                $("#Cotizacion").attr("disabled", true);
                $("#Remision").attr("disabled", true);
                ImprimirCotizacion(Cotizacion, 0);
                //TablaTemporalIngreso(IdCliente);
                swal("", datos[1] + " " + datos[2], "success");
            } else {
                swal("Acción Cancelada", datos[1], "warning");
            }

        }
    }
}



function TablaIngreso(cliente, IdRemision, cotizacion, sede, contacto) {
    $("#bodyIngreso").html("");
    var datos = LlamarAjax("Cotizacion","opcion=TemporalIngresoCotiz&cliente=" + cliente + "&remision=" + IdRemision + "&buscar=0&cotizacion=" + cotizacion + "&sede=" + sede + "&contacto=" + contacto + "&numero=" + Remision).split("|");
    var resultado = "";
    var totales = 0;
    
    if (datos[0] == "1") {
        swal("Acción Cancelada", datos[1], "warning");
        return false;
    }
        
    if (datos[0] == "9") {

        swal.queue([{
            title: ValidarTraduccion('Advertencia'),
            text: ValidarTraduccion('Hay datos temporales cargados en este cliente, ¿Desea eliminarlos?'),
            type: 'question',
            showLoaderOnConfirm: true,
            showCancelButton: true,
            confirmButtonText: ValidarTraduccion('Si'),
            cancelButtonText: ValidarTraduccion('No'),
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            allowOutsideClick: false,

            preConfirm: function (result) {
                
                                    
                return new Promise(function (resolve, reject) {
                    $.post("Cotizacion","opcion=TemporalIngresoCotiz&cliente=" + cliente + "&remision=" + IdRemision + "&buscar=1&cotizacion=" + cotizacion + "&sede=" + sede + "&contacto=" + contacto + "&numero=" + Remision)
                        .done(function (data) {
                            datos = data;
                            resolve()
                        })
                })
            }
        }]).then(function (data) {
            CargarTablaIngreso(datos);
        }, function (dismiss) {
            if (dismiss === 'cancel') { // you might also handle 'close' or 'timer' if you used those
                LimpiarTodo();
            } else {
                throw dismiss;
            }
        })

        return false;
                
    }

    CargarTablaIngreso(datos[1]);
    
}

function AplicarDescuento() {

    if ((IdCliente == 0) || ($("#bodyCotizacion").html() == ""))
        return false;

    if (Estado != "Temporal" && Estado != "Cotizado") {
        swal("Acción Cancelada", "No se puede aplicar un descuento a una cotización con estado " + Estado, "warning");
        return false;
    }

    var datos = LlamarAjax("Cotizacion","opcion=AplicarDescuento&cliente=" + IdCliente + "&id=" + IdCotizacion).split("|");
    if (datos[0] == "0") {
        swal("", datos[1], "success");
        DescuentoCli = datos[2]*1;
        $("#Descuento").val(DescuentoCli);
        TablaIngreso(IdCliente, IdRemision, IdCotizacion, IdSede, IdContacto)
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
}


function EliminarCotizacion(id, lote, equipo, cantidad) {

    var equipo = unescape(equipo);

    if (CanItems == 1 && Estado != "Temporal") {
        swal("Acción Cancelada", "No se puede eliminar el único registro de la cotizacion", "warning");
        return false;
    }

    if (Estado != "Temporal" && Estado != "Cotizado" ) {
        swal("Acción Cancelada", "No se puede eliminar una cotización con estado " + Estado, "warning");
        return false;
    }

    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: ValidarTraduccion('¿Seguro que desea eliminar el item de cotización del equipo ' + equipo + '?'),
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Eliminar'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                $.post(url_servidor+ "Cotizacion", "opcion=EliminarRegCotizacion&id=" + id + "&cotizacion=" + IdCotizacion + "&cantidad=" + cantidad + "&lote=" + lote)
                    .done(function (data) {
                        datos = data.split("|");
                        if (datos[0] == "0") {
                            Estado = "Temporal";
                            $("#Estado").val(Estado);
                            TablaIngreso(IdCliente, 0, IdCotizacion, 0, 0);
                            resolve()
                        } else {
                            reject(datos[1]);
                        }
                    })
            })
        }
    }]);
};

function ReemplazarCotizacion() {

    if ($("#bodyCotizacion").html() != "" && IdCotizacion > 0) {

        if (Estado != "POR REEMPLAZAR" && Estado != "Cerrado" && Estado != "Aprobado" && Estado != "Rechazado") {
            swal("Acción Cancelada", "No se puede reemplazar una cotizacíón en estado " + Estado, "warning");
            return false;
        }

        swal({
            title: 'Advertencia',
            text: '¿Desea reemplazar la cotización número ' + Cotizacion + ' del cliente ' + $("#NombreCliente").val() + '?',
            input: 'text',
            showLoaderOnConfirm: true,
            showCancelButton: true,
            confirmButtonText: ValidarTraduccion('Reemplazar'),
            cancelButtonText: ValidarTraduccion('Cancelar'),
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',

            inputValidator: function (value) {
                return new Promise(function (resolve, reject) {
                    if (value) {
                        var datos = LlamarAjax("Cotizacion","opcion=ReemplazarCotizacion&cotizacion=" + IdCotizacion + "&observaciones=" + value)
                        datos = datos.split("|");
                        if (datos[0] == "0") {
                            resultado = datos[1];
                            resolve()
                        } else {
                            reject(datos[1]);
                        }

                    } else {
                        reject(ValidarTraduccion('Debe de ingresar un observación'));
                    }
                })
            }
        }).then(function (result) {
            BuscarCotizacion(resultado);
            $("#Cliente").focus();
            swal({
                type: 'success',
                html: 'Cotización Reemplazada con el número ' + resultado
            })
        });
    }
}

function AnularCotizacion() {

    if (IdCotizacion > 0) {

        if (Estado != "Cotizado" && Estado != "Cerrado") {
            swal("Acción Cancelada", "No se puede anular una cotizacíón en estado " + Estado, "warning");
            return false;
        }

        swal({
            title: 'Advertencia',
            text: '¿Desea Anular la cotización número ' + Cotizacion + ' del cliente ' + $("#NombreCliente").val() + '?',
            input: 'text',
            showLoaderOnConfirm: true,
            showCancelButton: true,
            confirmButtonText: ValidarTraduccion('Anular'),
            cancelButtonText: ValidarTraduccion('Cancelar'),
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',

            inputValidator: function (value) {
                return new Promise(function (resolve, reject) {
                    if (value) {
                        var datos = LlamarAjax("Cotizacion","opcion=AnularCotizacion&cotizacion=" + IdCotizacion + "&observaciones=" + value)
                        datos = datos.split("|");
                        if (datos[0] == "0") {
                            resultado = datos[1];
                            resolve()
                        } else {
                            reject(datos[1]);
                        }

                    } else {
                        reject(ValidarTraduccion('Debe de ingresar un observación'));
                    }
                })
            }
        }).then(function (result) {
            $("#Cliente").focus();
            $("#Estado").val("Anulado");
            Estado = "Anulado";
            swal({
                type: 'success',
                html:  resultado
            })
        });
    }
}

function PdfSolicitud(solicitud) {
    if (solicitud == 0)
        solicitud = Solicitud;
    if (solicitud == 0)
        return false;
    var parametros = "solicitud=" + solicitud;
    ActivarLoad();
    setTimeout(function () {
        var datos = LlamarAjax("Solicitud","opcion=RpSolicitud&" + parametros);
        DesactivarLoad();
        datos = datos.split("|");
        if (datos[0] == "0") {
            window.open(url + "DocumPDF/" + datos[1]);
        } else {
            swal("", datos[1], "warning");
        }
    }, 15);
}

function ImprimirSolicitud() {
    if (Solicitud == 0)
        swal({
            title: 'Por favor',
            text: 'Ingrese el número de la solicitud',
            input: 'number',
            showLoaderOnConfirm: true,
            showCancelButton: true,
            confirmButtonText: ValidarTraduccion('Imprimir'),
            cancelButtonText: ValidarTraduccion('Cancelar'),
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            inputValidator: function (value) {
                return new Promise(function (resolve, reject) {
                    if (value) {
                        PdfSolicitud(value);
                        resolve();
                    } else {
                        reject(ValidarTraduccion('Debe de ingresar un número de solicitud'));
                    }
                })
            }
        });
    else {
        PdfSolicitud(Solicitud);
    }
}

function AgregarSolicitud() {

    if (IdCliente == 0 || IdCotizacion > 0 || CanItems > 0)
        return false;
    if (IdSede == 0 || IdContacto == 0) {
        swal("Acción Cancelada", "Debe seleccionar la  sede y contacto", "warning");
        return false;
    }

    if ($("#bodyCotizacion").html() != "") {
        swal("Acción Cancelada", "No se puede agregar una solicitud, porque este cliente posee registros", "warning");
        return false;
    }
        
    swal({
        title: 'Advertencia',
        text: 'Ingrese el número de la solicitud del cliente ' + $("#NombreCliente").val(),
        input: 'number',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Agregar'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',

        inputValidator: function (value) {
            return new Promise(function (resolve, reject) {
                if (value) {
                    var datos = LlamarAjax("Cotizacion","opcion=AgregarSolicitud&solicitud=" + value + "&cliente=" + IdCliente + "&idsede=" + IdSede + "&idcontacto=" + IdContacto)
                    datos = datos.split("|");
                    if (datos[0] == "0") {
                        Solicitud = value;
                        $("#Solicitud").val(Solicitud);
                        resolve()
                    } else {
                        reject(datos[1]);
                    }

                } else {
                    reject(ValidarTraduccion('Debe de ingresar un número de solicitud'));
                }
            })
        }
    }).then(function (result) {
        $("#Cliente").focus();
        var datostab = LlamarAjax("Cotizacion","opcion=TemporalIngresoCotiz&cliente=" + IdCliente + "&remision=0&buscar=1&cotizacion=0&sede=" + IdSede + "&contacto=" + IdContacto + "&numero=0").split("|");
        CargarTablaIngreso(datostab[1]);
        swal({

            type: 'success',
            html: 'Solicitud agregada con éxito'
        })
    });
}

function ImportarCotizacion() {

    if (IdCliente == 0)
        return false;
    if (IdSede == 0 || IdContacto == 0) {
        swal("Acción Cancelada", "Debe seleccionar la  sede y contacto", "warning");
        return false;
    }

    if (Estado != "Cotizado" && Estado != "Temporal") {
        swal("Acción Cancelada", "No se puede importar datos a una cotizacíón en estado " + Estado, "warning");
        return false;
    }

    swal({
        title: 'Advertencia',
        text: 'Ingrese el número de Cotización que desea importar',
        input: 'number',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Importar'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',

        inputValidator: function (value) {
            return new Promise(function (resolve, reject) {
                if (value) {
                    var datos = LlamarAjax("Cotizacion","opcion=ImportarCotizacion&cotizacion=" + value + "&cliente=" + IdCliente + "&idsede=" + IdSede + "&idcontacto=" + IdContacto + "&idcotizacion=" + IdCotizacion + "&nombre=" + $("#NombreCliente").val()) 
                    datos = datos.split("|");
                    if (datos[0] == "0") {
                        resolve()
                    } else {
                        reject(datos[1]);
                    }

                } else {
                    reject(ValidarTraduccion('Debe de ingresar un número de cotizacion'));
                }
            })
        }
    }).then(function (result) {
        $("#Cliente").focus();
        var datostab = LlamarAjax("Cotizacion","opcion=TemporalIngresoCotiz&cliente=" + IdCliente + "&remision=0&buscar=1&cotizacion=0&sede=" + IdSede + "&contacto=" + IdContacto + "&numero=0").split("|");
        CargarTablaIngreso(datostab[1]);
        swal({

            type: 'success',
            html: 'Cotización importada con éxito'
        })
    });
}


function LimpiarTodo() {

    var cliente = IdCliente;
        
    ActivarLoad();
    setTimeout(function () {

        if ($("#bodyCotizacion").html() != "" && IdCotizacion == 0) {
            DesactivarLoad();
            swal.queue([{
                title: ValidarTraduccion('Advertencia'),
                text: ValidarTraduccion('Hay datos temporales cargados en este cliente, ¿Desea eliminarlos?'),
                type: 'question',
                showLoaderOnConfirm: true,
                showCancelButton: true,
                confirmButtonText: ValidarTraduccion('Si'),
                cancelButtonText: ValidarTraduccion('No'),
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        $.post("Cotizacion/EliminarTmpCotizacion", "cliente=" + cliente)
                            .done(function (data) {
                                resolve()
                            })
                    })
                }
            }]);

        }
        Cotizacion = 0;
        IdCotizacion = 0;
        LimpiarDatos();
        LimpiarServicio();
        DesactivarLoad();
        $("#Cliente").val("");
        $("#Cliente").focus();
    }, 15);
}

function GuardarRelaIngreso(remision, ingreso) {

    var id = $("#idrelacion").val();
    var parametro = "cotizacion=" + CotizacionRel + "&remision=" + remision + "&id=" + id + "&ingreso=" + ingreso;
    var datos = LlamarAjax("Cotizacion","opcion=RelaIngresoCoti"+ parametro).split("|");
    if (datos[0] == "0") {
        BuscarRelCotizacion(CotizacionRel, "0", 1);
        $("#modalRelIngreso").modal("hide");
        swal("", datos[1], "success");
    } else
        swal("Acción Cancelada", datos[1], "warning");
}


function RelacionarIngreso(ingreso, cotizacion, id, equipo, idequipo, serie, magnitud, nequipo, intervalo) {

    if (ingreso * 1 > 0) {
        swal("Acción Cancelada", "Este registro de cotización ya posee un ingreso asignado", "warning");
        return false;
    }

    $("#cotrelnum").html(cotizacion);
    $("#idrelacion").val(id);
    $("#RelEquipo").html(equipo);
    
    var parametros = "idcliente=" + IdClienteRel + "&tipo=5&magnitud=" + magnitud + "&equipo=" + nequipo + "&intervalo=" + intervalo;
    var datos = LlamarAjax('Cotizacion','opcion=ModalIngreso&'+ parametros);
    var resultado = '<table class="table display  table-bordered" id="tablamodalrelingreso"><thead><tr><th><span idioma="Remisión">Remisión</span></th><th><span idioma="Ingresos">Ingresos</span></th><th><span idioma="Fecha">Fecha</span></th><th><span idioma="Equipo">Equipo</span></th><th><span idioma="serie">serie</span></th><th><span idioma="Asesor">Asesor</span></th><th><span idioma="Observación">Observación</span></th></tr></thead><tbody>';
    if (datos != "[]") {
        var data = JSON.parse(datos);
        for (var x = 0; x < data.length; x++) {
            resultado += "<tr ondblclick='GuardarRelaIngreso(" + data[x].remision + "," + data[x].ingreso + ")' " + (data[x].idequipo == idequipo ? "style='background-color:blue; color:white'" : "") + ">" +
                "<td>" + data[x].remision + "</td>" +
                "<td>" + data[x].ingreso + "</td>" +
                "<td>" + data[x].fecha + "</td>" +
                "<td>" + data[x].equipo + "</td>" +
                "<td>" + data[x].serie + "</td>" +
                "<td>" + data[x].usuario + "</td>" +
                "<td>" + data[x].observacion + "</td></tr>";
        }
    } else {
        swal("Acción Cancelada", "Este cliente no posee ingresos pendientes por cotizar", "warning");
        return false;
    }
    resultado += "</tbody></table>";
    $("#divtablaingreso").html(resultado);
    ActivarDataTable("tablamodalrelingreso", 5);
    $("#modalRelIngreso").modal("show");
}

function ConsularIngresos() {


    var magnitud = $("#ICMagnitud").val() * 1;
    var equipo = $("#ICEquipo").val();
    var modelo = $("#ICModelo").val();
    var marca = $("#ICMarca").val() * 1;
    var intervalo = $("#ICIntervalo").val();
    var ingreso = $("#ICIngreso").val() * 1;
    var serie = $.trim($("#ICSerie").val());

    var recepcion = $("#ICRecepcion").val();

    var remision = $("#ICRemision").val() * 1;
    var cotizacion = $("#ICSerie").val() * 1;
    var cliente = $("#ICCliente").val() * 1;
    var usuario = $("#ICUsuario").val() * 1;

    var fechad = $("#ICFechaDesde").val();
    var fechah = $("#ICFechaHasta").val();

    if (fechad == "" && fechah == "") {
        swal("", ValidarTraduccion("debe de ingresar una fecha o números de días"), "warning");
        return false;
    }

    ActivarLoad();
    setTimeout(function () {
        var parametros = "remision=" + remision + "&cliente=" + cliente + "&ingreso=" + ingreso + 
            "&fechad=" + fechad + "&fechah=" + fechah + "&magnitud=" + magnitud + "&equipo=" + equipo + "&marca=" + marca + "&modelo=" + modelo +
            "&intervalo=" + intervalo + "&serie=" + serie + "&usuario=" + usuario + "&recepcion=" + recepcion;
        var datos = LlamarAjax("Cotizacion", "opcion=ConsulIngSCotiza&"+ parametros);
        DesactivarLoad();

        var datajson = JSON.parse(datos);
        $('#TablaRemisiones').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "ingreso", "className": "text-XX" },
                { "data": "remision" },
                { "data": "recepcion" },
                { "data": "cliente" },
                { "data": "sede" },
                { "data": "equipo" },
                { "data": "marca" },
                { "data": "rango" },
                { "data": "observacion" },
                { "data": "fecha" },
                { "data": "usuario" },
                { "data": "tiempo" }
            ],

            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy','colvis'
            ],
            "colReorder": true
        });
    }, 15);
}

function ConsularAsesor() {


    var magnitud = $("#AMagnitud").val() * 1;
    var cliente = $("#ACliente").val() * 1;
    var asesor = $("#AAsesor").val() * 1;

    var fechad = $("#AFechaDesde").val();
    var fechah = $("#AFechaHasta").val();

    if (asesor == 0 && cliente == 0) {
        swal("", ValidarTraduccion("Debe de ingresar el asesor comercial ó cliente"), "warning");
        return false;
    }

    if (fechad == "" && fechah == "") {
        swal("", ValidarTraduccion("debe de ingresar la fecha desde y hasta"), "warning");
        return false;
    }

    ActivarLoad();
    setTimeout(function () {
        var parametros = "Cliente=" + cliente + "&Asesor=" + asesor +
            "&fechad=" + fechad + "&fechah=" + fechah + "&Magnitud=" + magnitud;
        var datos = LlamarAjax("Cotizacion", "opcion=ConsultarClienteAsesor&"+parametros);
        DesactivarLoad();

        var datajson = JSON.parse(datos);
        $('#TablaAsesores').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "fila" },
                { "data": "documento" },
                { "data": "nombrecompleto" },
                { "data": "telefonos" },
                { "data": "email" },
                { "data": "equipos", "className": "text-center text-XX" },
                { "data": "cotizados", "className": "text-center text-XX" },
                { "data": "reportados", "className": "text-center text-XX" },
                { "data": "certificado", "className": "text-center text-XX" },
                { "data": "salida", "className": "text-center text-XX" },
                { "data": "pendientes", "className": "text-center text-XX" }
            ],

            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy','colvis'
            ]
        });
    }, 15);
}

function DetalleCliente(tipo, cliente) {

    var magnitud = $("#AMagnitud").val() * 1;
    var asesor = $("#AAsesor").val() * 1;

    var fechad = $("#AFechaDesde").val();
    var fechah = $("#AFechaHasta").val();

    switch (tipo) {
        case 1:
            $("#DetAsesorModal").html("Total de Equipos");
            break;
        case 2:
            $("#DetAsesorModal").html("Equipos Cotizados");
            break;
        case 3:
            $("#DetAsesorModal").html("Equipos Reportados");
            break;
        case 4:
            $("#DetAsesorModal").html("Equipos con Certificados ó Informe");
            break;
        case 5:
            $("#DetAsesorModal").html("Equipos con Devolución");
            break;
        case 6:
            $("#DetAsesorModal").html("Equipos Pendientes");
            break;
    }

    ActivarLoad();
    setTimeout(function () {
        var parametros = "Cliente=" + cliente + "&Asesor=" + asesor +
            "&fechad=" + fechad + "&fechah=" + fechah + "&Magnitud=" + magnitud + "&Tipo=" + tipo;
        var datos = LlamarAjax("Cotizacion","opcion=ModalDetIngresoAse&" + parametros);
        DesactivarLoad();

        var datajson = JSON.parse(datos);
        var tabla = $('#Tabladetmodalasesor').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            paging: true,
            ordering: true,
            columns: [
                { "data": "remision" },
                { "data": "ingreso" },
                { "data": "fecha" },
                { "data": "equipo" },
                { "data": "serie" },
                { "data": "usuario" },
                { "data": "observacion" },
                { "data": "tiempo" }
            ],

            "lengthMenu": [5],

            "language": {
                "url": LenguajeDataTable
            }
        });
    }, 15);

    $("#modalDetAsesor").modal("show");
}

function ConsularReportes() {
    var magnitud = $("#CRMagnitud").val() * 1;
    var equipo = $("#CREquipo").val() * 1;
    var modelo = $("#CRModelo").val() * 1;
    var marca = $("#CRMarca").val() * 1;
    var intervalo = $("#CRIntervalo").val() * 1;
    var ingreso = $("#CRIngreso").val() * 1;
    var serie = $.trim($("#CRSerie").val());
    var asesor = $("#CAsesor").val() * 1;
    var noaprobado = 0;
    var enviado = 0;

    if ($("#CNAprobado").prop("checked"))
        noaprobado = 1;

    if ($("#CEnviado").prop("checked"))
        enviado = 1;

    var cotizado = $("#CRCotizado").val() * 1;

    var remision = 0;
    var estado = $("#CRAprobado").val();
    var cliente = $("#CRCliente").val() * 1;
    var usuario = $("#CRUsuario").val() * 1;

    var fechad = $("#CRFechaDesde").val();
    var fechah = $("#CRFechaHasta").val();

    if (fechad == "" && fechah == "") {
        swal("", ValidarTraduccion("debe de ingresar una fecha o números de días"), "error");
        return false;
    }

    ActivarLoad();
    setTimeout(function () {
        var parametros = "remision=" + remision + "&cliente=" + cliente + "&ingreso=" + ingreso +
            "&fechad=" + fechad + "&fechah=" + fechah + "&magnitud=" + magnitud + "&equipo=" + equipo + "&marca=" + marca + "&modelo=" + modelo +
            "&intervalo=" + intervalo + "&serie=" + serie + "&usuario=" + usuario + "&cotizado=" + cotizado + "&estado=" + estado + "&asesor=" + asesor + "&enviado=" + enviado + "&noaprobado=" + noaprobado;
        var datos = LlamarAjax("Cotizacion","opcion=ConsulIngReportado&"+ parametros);
        DesactivarLoad();

        var datajson = JSON.parse(datos);
        $('#TablaReportes').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "enviar" },
                { "data": "ingreso" },
                { "data": "plantilla" },
                { "data": "remision" },
                { "data": "cotizado" },
                { "data": "cliente" },
                { "data": "telefono" },
                { "data": "equipo" },
                { "data": "marca" },
                { "data": "rango" },
                { "data": "fecha" },
                { "data": "observacion" },
                { "data": "reporte" },
                { "data": "fechaenvio" },
                { "data": "correoenvio" },
                { "data": "estado" },
                { "data": "aprobadopor" },
                { "data": "fecaprobacion" },
                { "data": "observacionapro" },
                { "data": "usuario" },
                { "data": "tiempo" },
                { "data": "idreporte" }
            ],

            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy', 'colvis'
            ]
        });
    }, 15);
}

$("#TablaReportes > tbody").on("dblclick", "tr", function (e) {
    var row = $(this).parents("td").context.cells;
    var id = row[21].innerText;
    var datos = LlamarAjax("Laboratorio","opcion=DatosReporte&id=" + id).split("|");
    if (datos[0] == "0") {
        var data = JSON.parse(datos[1]);
        $("#RAIngreso").val(data[0].ingreso);
        $("#RAContacto").val(data[0].contacto);
        $("#RACliente").val(data[0].cliente);
        $("#RADireccion").val(data[0].sede);
        $("#RAAjuste").val(data[0].ajuste);
        $("#RASuministro").val(data[0].suministro);
        $("#RAObservacion").val(data[0].observacion);
        $("#IdReporte").val(data[0].idreporte);

        $("#OpcionAproRepor").val("1").trigger("change");
        $("#UsuarioRepor").val("");
        $("#CedulaRepor").val("");
        $("#CargoRepor").val("");
        $("#ObservacionRepor").val("");

        $("#modalAprobarRepor").modal("show");
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }


});

function EnvioAjuste(ingreso, reporte, principal, cliente, correocontacto, plantilla, magnitud) {
    CorreoEmpresa = principal;
    Empresa = cliente;
    $("#NroReporte").val(reporte);
    $("#sprcotizacion").html(ingreso);
    $("#RPeCliente").val(cliente);
    $("#RPePlantilla").val(plantilla);
    $("#RPeMagnitud").val(magnitud);
    $("#RPeCorreo").val(principal + (correocontacto != "" ? ";" + correocontacto : ""));
    $("#RPObserEnvio").val("");
    $("#enviar_reporte").modal("show");

}

$("#formenviarreporte").submit(function (e) {

    e.preventDefault();
    var observacion = $.trim($("#RPObserEnvio").val())
    var correo = $.trim($("#RPeCorreo").val());
    var ingreso = $("#RAIngreso").val()*1;
    a_correo = correo.split(";");
    var reporte = $("#NroReporte").val() * 1;
    var plantilla = $("#RPePlantilla").val();
    var magnitud = $("#RPeMagnitud").val();
    for (var x = 0; x < a_correo.length; x++) {
        if (ValidarCorreo($.trim(a_correo[x])) == 2) {
            $("#RPeCorreo").focus();
            swal(ValidarTraduccion("Acción Cancelada"), ValidarTraduccion("Formato de correo inválido") + ":" + a_correo[x], "warning");
            return false;
        }
    }

    ActivarLoad();
    setTimeout(function () {
        var parametros = "ingreso=" + ingreso + "&reporte=" + reporte + "&principal=" + CorreoEmpresa + " &e_email=" + correo + " &cliente=" + Empresa + " &observacion=" + observacion + "&plantilla=" + plantilla + "&magnitud=" + magnitud;
        var datos;
        if (reporte == 0)
            datos = LlamarAjax("Laboratorio","opcion=EnvioCotizacion&" + parametros);
        else
            datos = LlamarAjax("Laboratorio","opcion=EnvioReporte&" + parametros);
        DesactivarLoad();
        datos = datos.split("|");
        if (datos[0] == "0") {
            swal("", ValidarTraduccion(datos[1]) + " " + datos[2], "success");
            $("#enviar_reporte").modal("hide");

        } else
            swal("Acción Cancelada", datos[1], "warning");
    }, 15);

    return false;
});

function GuardarAproRepo() {

    var ingreso = $("#RAIngreso").val();
    var id = $("#IdReporte").val();
    var tipo = $("#OpcionAproRepor").val();
    var observacion = $.trim($("#ObservacionRepor").val());

    var usuario = $.trim($("#UsuarioRepor").val());
    var cedula = $.trim($("#CedulaRepor").val());
    var cargo = $.trim($("#CargoRepor").val());

    var fecha = $("#FechaRepor").val();
    var hora = $("#HoraRepor").val();

    var archivo = $.trim($("#ArchivoApro").val());


    if (fecha == "") {
        $("#FechaRepor").focus();
        swal("Acción Cancelada", "Debe de ingresar la fecha que aprueba o reprueba el ajuste/suministro", "warning");
        return false;
    } else {
        if (fecha > output2) {
            $("#FechaRepor").focus();
            swal("Acción Cancelada", "La fecha no puede ser mayor a la fecha actual", "warning");
            return false;
        }
    }

    if (hora == "") {
        $("#HoraRepor").focus();
        swal("Acción Cancelada", "Debe de ingresar la hora que aprueba o reprueba el ajuste/suministro", "warning");
        return false;
    }

    if (usuario == "") {
        $("#UsuarioRepor").focus();
        swal("Acción Cancelada", "Debe de ingresar el usuario que aprueba o reprueba el ajuste/suministro", "warning");
        return false;
    }

    if (cargo == "") {
        $("#CargoRepor").focus();
        swal("Acción Cancelada", "Debe de ingresar el cargo que aprueba o reprueba el ajuste/suministro", "warning");
        return false;
    }

    if (observacion == "") {
        $("#ObservacionRepor").focus();
        swal("Acción Cancelada", "Debe de ingresar una observación", "warning");
        return false;
    }

    if (archivo == "") {
        $("#ArchivoApro").focus();
        swal("Acción Cancelada", "Debe de ingresar el archivo pdf de la aprobación del cliente", "warning");
        return false;
    }

    var datos = LlamarAjax("Laboratorio", "opcion=&AprobarReporte&ingreso=" + ingreso + "&id=" + id + "&opcion=" + tipo + "&observacion=" + observacion +
        "&usuario=" + usuario + "&cargo=" + cargo + "&cedula=" + cedula + "&fecha=" + fecha + "&hora=" + hora)
    datos = datos.split("|");
    if (datos[0] == "0") {
        ConsularReportes();
        NotificacionesCotiApro();
        $("#modalAprobarRepor").modal("hide");
        swal("", datos[1], "success");
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
}

function EliminarRelaCotizacion(id, equipo, ingreso, cotizacion) {

    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: ValidarTraduccion('¿Seguro que desea eliminar el ingreso ' + ingreso + ' del equipo ' + equipo + ' de la cotización número ' + cotizacion + '?'),
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Si'),
        cancelButtonText: ValidarTraduccion('No'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                $.post("Cotizacion/ElimIngresoCoti", "id=" + id + "&cotizacion=" + cotizacion + "&ingreso=" + ingreso)
                    .done(function (data) {
                        datos = data.split("|");
                        if (datos[0] == "0") {
                            BuscarRelCotizacion(cotizacion, "0", 1);
                            resolve()
                        } else {
                            reject(datos[1]);
                        }
                    })
            })
        }
    }]);

}

function CambioColorSep(color, fila) {
    var coloressep = ["", "", "#224390", "#367111", "#5E0E0F", "#079787", "#959A05", "#05DEDE", "#810385", "#874504","#056078"]
    if (color == 1)
        $("#FilaSep" + fila).css("color", "black");
    else
        $("#FilaSep" + fila).css("color", coloressep[color]);
}

function BuscarRelCotizacion(cotizacion, code, tipo) {
    if ((code.keyCode == 13  || tipo == 1) && ErrorEnter == 0)  {
        ActivarLoad();
        setTimeout(function () {
            var parametros = "cotizacion=" + cotizacion;
            var datos = LlamarAjax('Cotizacion','opcion=BuscarCotizacion&'+parametros);
            var resultado = "";
            if (datos != "[]") {
                var data = JSON.parse(datos);
                $("#ReCliente").val(data[0].cliente);
                $("#ReSede").val(data[0].sede);
                $("#ReContacto").val(data[0].contacto);
                IdClienteRel = data[0].idcliente;
                CotizacionRel = cotizacion;

                datos = LlamarAjax("Cotizacion", "opcion=TablaCotizacion&cotizacion=" + data[0].id);
                data = JSON.parse(datos);

                for (var x = 0; x < data.length; x++) {
                    resultado += "<tr " + (x % 2 == 1 ? " class='bg-gris' " : "") + " ondblclick=\"RelacionarIngreso(" + data[x].ingreso + "," + cotizacion + "," + data[x].id + ",'<b>Magnitud: </b>" + data[x].magnitud + " <b>Equipo: </b> " + data[x].equipo + " <br> <b>Marca: </b>" + data[x].marca + " <b> Modelo: </b> " + data[x].modelo + " <br> <b>Rango:</b> " + data[x].intervalos + " <b> Serie:</b> " + data[x].serie + "'," + data[x].idequipo + ",'" + data[x].serie + "','" + data[x].magnitud + "','" + data[x].equipo + "','" + data[x].intervalos + "')\">" +
                        "<td>" + (x + 1) + "</td>" +
                        "<td>" + (data[x].idequipo > 0 ? "<b> " + data[x].ingreso + " </b > <br>Fecha " + data[x].fechaing + "<br>Usuario " + data[x].usuario : "&nbsp;") + "</td>" +
                        "<td>" + (data[x].idequipo > 0 ? "<b>" + data[x].magnitud + "</b><br>" + data[x].equipo + "<br>" + data[x].intervalos +
                            "<br><b>" + data[x].marca + "</b><br>" + data[x].modelo + "<br><b>Serie:</b>" + data[x].serie : "&nbsp;") + "</td>" +
                        "<td>" + (data[x].servicio ? data[x].servicio : "&nbsp;") +
                        (data[x].proveedor ? "<br><b>Proveedor:</b>" + data[x].proveedor : "&nbsp;") +
                        (data[x].metodo ? "<br><b>Método:</b>" + data[x].metodo : "&nbsp;") +
                        (data[x].observacion ? "<br><b>Obser:</b>" + data[x].observacion : "&nbsp;") + "</td>" +
                        "<td>" + (data[x].punto ? "<b>Punto de calibración: </b>" + data[x].punto : "&nbsp;") +
                        (data[x].nombreca ? "<br><b>A nombre: </b>" + data[x].nombreca : "&nbsp;") +
                        (data[x].direccion ? "<br><b>Dirección: </b>" + data[x].direccion : "&nbsp;") +
                        (data[x].declaracion ? "<br><b>Declaracion de conformidad:</b>" + data[x].declaracion : "&nbsp;") +
                        (data[x].certificado ? "<br><b>Certificado:</b>" + data[x].certificado : "&nbsp;") +
                        (data[x].proxima ? "<br><b>Próxima calibración:</b>" + data[x].proxima : "&nbsp;") + "</td>" +
                        "<td align='center' class='text-XX'>" + (data[x].entrega ? data[x].entrega + " días" : "&nbsp;") + "</td> " +
                        "<td align='center' class='text-XX'>" + (data[x].precio ? formato_numero(data[x].precio, 0,_CD,_CM,"") : "&nbsp;") + "</td> " +
                        "<td align='center' class='text-XX'>" + data[x].descuento + "</td> " +
                        "<td align='center' class='text-XX'>" + data[x].cantidad + "</td>" +
                        "<td align='center' class='text-XX'>" + formato_numero((data[x].precio * data[x].cantidad) - (data[x].precio * data[x].cantidad * data[x].descuento / 100), 0,_CD,_CM,"") + "</td> " +
                        "<td align='center' class='text-XX'>" + data[x].iva + "<input type='hidden' value='" + data[x].idequipo + "' name='tempequipo[]'>" +
                        "<input type='hidden' value='" + data[x].idservicio + "' name='tempservicio[]'></td>" +
                        "<td align='center'>" + (data[x].relacionado == 1 && data[x].ingreso > 0 ? "<button class='btn btn-danger' title='Eliminar Relación' type='button' onclick=\"EliminarRelaCotizacion(" + data[x].id + ",'" + data[x].equipo + " " + data[x].marca + " " + data[x].modelo + "'," + data[x].ingreso + "," + CotizacionRel + ")\"><span data-icon='&#xe0d8;'></span></button><br>" : "&nbsp;") + "</tr>";
                }

                $("#BRCotizacion").html(resultado);
                DesactivarLoad();

            } else {
                DesactivarLoad();
                ErrorEnter = 1;
                $("#RCotizacion").select();
                $("#RCotizacion").focus();

                swal("Acción Cancelada", "Esta cotización no se encuentra registrada", "warning");
                return false;
            }
        }, 15);
    } else {
        ErrorEnter = 0;
    }
}

function SepararCotizacion() {

    if ($("#BRSeparar").html() == "")
        return false;

    var ids = document.getElementsByName("idseparados[]");
    var combos = document.getElementsByName("ComSeparador[]");
    var encontrado = 0;
    var arreglo = [];
    var a_combos = [];
    var a_ids = [];

    for (var x = 0; x < combos.length; x++) {
        /*if (a_combos != "") {
            a_combos += ",";
            a_ids += ",";
        }*/

        a_combos.push(combos[x].value);
        a_ids.push(ids[x].value);
        for (var y = 0; y < arreglo.length; y++) {
            if (arreglo[y] != combos[x].value) {
                encontrado = 1;
                break;
            }
        }
        arreglo.push(combos[x].value);
    }

    if (encontrado == 0) {
        swal("Acción Cancelada", "Debe separar los items de la cotización", "warning");
        return false
    }
    var datos = null;    
    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: ValidarTraduccion('¿Seguro que desea separar la cotizacion número ' + CotizacionSep + '?'),
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Separar'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                $.post(url_servidor + "Cotizacion","opcion=SepararCotizacion&cotizacion=" + CotizacionSep + "&a_ids=" + a_ids + "&a_combos=" + a_combos)
                    .done(function (data) {
                        datos = data.split("|");
                        if (datos[0] == "0") {
                            resolve()
                        } else {
                            reject(datos[1]);
                        }
                    })
            })
        }
    }]).then(function (result) {
        $("#SeCliente").val("");
        $("#SeSede").val("");
        $("#SeContacto").val("");
        $("#SeCotizacion").val("");
        $("#BRSeparar").html("");
        CotizacionSep = 0;
        var cotizaciones = datos[2].split(",");
        for (var x = 0; x < cotizaciones.length; x++) {
            ImprimirCotizacion(cotizaciones[x], 0);
        }
        swal({
            type: 'success',
            html: datos[1] + "<br>" + datos[2]
        })
    });
}

function BuscarSepCotizacion(cotizacion, code, tipo) {
    if ((code.keyCode == 13 || tipo == 1) && ErrorEnter == 0) {
        ActivarLoad();
        setTimeout(function () {
            var parametros = "cotizacion=" + cotizacion;
            var datos = LlamarAjax('Cotizacion','opcion=BuscarCotizacion&'+parametros);
            var resultado = "";
            if (datos != "[]") {
                var data = JSON.parse(datos);
                $("#SeCliente").val(data[0].cliente);
                $("#SeSede").val(data[0].sede);
                $("#SeContacto").val(data[0].contacto);
                CotizacionSep = cotizacion;

                var combo = "";

                datos = LlamarAjax("Cotizacion","opcion=TablaCotizacion&cotizacion=" + data[0].id);
                data = JSON.parse(datos);


                for (var x = 0; x < data.length; x++) {

                    combo = "<select onchange='CambioColorSep(this.value," + x + ")' class='form-control text-XX14' style='width:55px' id='ComSeparador" + x + "' name='ComSeparador[]'>";
                    for (var y = 1; y <= 10; y++){
                        combo += "<option value='" + y + "'>" + y + "</option>";
                    }
                    combo += "</select>"


                    resultado += "<tr id='FilaSep" + x + "' " + (x % 2 == 1 ? " class='bg-gris' " : "") + ">" +
                        "<td>" + combo + "</td>" +
                        "<td>" + (x + 1) + "</td>" +
                        "<td>" + (data[x].idequipo > 0 ? "<b> " + data[x].ingreso + " </b > <br>Fecha " + data[x].fechaing + "<br>Usuario " + data[x].usuario : "&nbsp;") + "</td>" +
                        "<td>" + (data[x].idequipo > 0 ? "<b>" + data[x].magnitud + "</b><br>" + data[x].equipo + "<br>" + data[x].intervalos +
                            "<br><b>" + data[x].marca + "</b><br>" + data[x].modelo + "<br><b>Serie:</b>" + data[x].serie : "&nbsp;") + "</td>" +
                        "<td>" + (data[x].servicio ? data[x].servicio : "&nbsp;") +
                        (data[x].proveedor ? "<br><b>Proveedor:</b>" + data[x].proveedor : "&nbsp;") +
                        (data[x].metodo ? "<br><b>Método:</b>" + data[x].metodo : "&nbsp;") +
                        (data[x].observacion ? "<br><b>Obser:</b>" + data[x].observacion : "&nbsp;") + "</td>" +
                        "<td>" + (data[x].punto ? "<b>Punto de calibración: </b>" + data[x].punto : "&nbsp;") +
                        (data[x].nombreca ? "<br><b>A nombre: </b>" + data[x].nombreca : "&nbsp;") +
                        (data[x].direccion ? "<br><b>Dirección: </b>" + data[x].direccion : "&nbsp;") +
                        (data[x].declaracion ? "<br><b>Declaracion de conformidad:</b>" + data[x].declaracion : "&nbsp;") +
                        (data[x].certificado ? "<br><b>Certificado:</b>" + data[x].certificado : "&nbsp;") +
                        (data[x].proxima ? "<br><b>Próxima calibración:</b>" + data[x].proxima : "&nbsp;") + "</td>" +
                        "<td align='center' class='text-XX'>" + (data[x].entrega ? data[x].entrega + " días" : "&nbsp;") + "</td> " +
                        "<td align='center' class='text-XX'>" + (data[x].precio ? formato_numero(data[x].precio, 0, _CD, _CM, "") : "&nbsp;") + "</td> " +
                        "<td align='center' class='text-XX'>" + data[x].descuento + "</td> " +
                        "<td align='center' class='text-XX'>" + data[x].cantidad + "</td>" +
                        "<td align='center' class='text-XX'>" + formato_numero((data[x].precio * data[x].cantidad) - (data[x].precio * data[x].cantidad * data[x].descuento / 100), 0, _CD, _CM, "") + "</td> " +
                        "<td align='center' class='text-XX'>" + data[x].iva + "<input type='hidden' value='" + data[x].id + "' name='idseparados[]'></td></tr>";
                }

                $("#BRSeparar").html(resultado);
                DesactivarLoad();

            } else {
                DesactivarLoad();
                ErrorEnter = 1;
                $("#RCotizacion").select();
                $("#RCotizacion").focus();

                swal("Acción Cancelada", "Esta cotización no se encuentra registrada", "warning");
                return false;
            }
        }, 15);
    } else {
        ErrorEnter = 0;
    }
}

function BuscarCotizacion(numero) {
        
    if (Cotizacion == numero)
        return false;

    ActivarLoad();
    setTimeout(function () {

        LimpiarDatos();
        LimpiarServicio();
        $("#Cliente").val("");
        $("#Cliente").focus();
        if ((numero * 1) == 0)
            return false;
        
        var parametros = "cotizacion=" + numero;
        var datos = LlamarAjax('Cotizacion','opcion=BuscarCotizacion&'+parametros);
        DesactivarLoad();
        if (datos != "[]") {
            var datacot = JSON.parse(datos);
            IdCotizacion = datacot[0].id;
            CotizacionesAnt = datacot[0].cotizacionant;
            Cotizacion = datacot[0].cotizacion;
            Remision = datacot[0].remision;
            Estado = datacot[0].estado;
            IdSede = datacot[0].idsede;
            IdContacto = datacot[0].idcontacto;
            if (Estado != "Temporal" && Estado != "Cotizado") {
                CliReteFuente = datacot[0].retefuente;
                CliReteIva = datacot[0].reteiva;
                CliReteIca = datacot[0].reteica;
            }
            $("#Cliente").val(datacot[0].documento);
            BuscarClienteCot(datacot[0].documento, 0, IdSede, IdContacto);
            $("#Cotizacion").val(Cotizacion);
            $("#Estado").val(Estado);
            $("#Revision").val(datacot[0].revision);
            $("#Validez").val(datacot[0].validez).trigger("change");
            $("#CotizacionesAnt").val(datacot[0].anterior);
            $("#ObserReemplazo").val(datacot[0].obserreemplazo);
            Solicitud = datacot[0].solicitud;
            $("#Solicitud").val(datacot[0].solicitud);
            $("#Observaciones").val(datacot[0].observacion);
                       
            
            if (Estado != "Cotizado" && Estado != "Temporal") {
                $(".boton").addClass("hidden");
            }

            if (Estado == "Anulado" || Estado == "Reemplazado") {
                $(".boton3").addClass("hidden");
            }


        } else {
            $("#Cotizacion").focus();
            swal("Acción Cancelada", "Cotización número " + numero + " no registrada", "warning");
        }
    },15);
}

$("#tablamodalremision > tbody").on("click", "tr", function (e) {

    var row = $(this).parents("td").context.cells;
    var numero = row[0].innerText;
    $("#modalRemision").modal("hide");
    $("#Remision").val(numero);
    BuscarRemisiom(numero);
});



$("#tablamodalingresocot > tbody").on("dblclick", "tr", function (e) {

    var objeto = this;

    ActivarLoad();
    setTimeout(function () {
        var row = $(objeto).parents("td").context.cells;
        var numero = row[1].innerText;
        var remision = row[0].innerText;
        var parametros = "ingreso=" + numero + "&cliente=" + IdCliente + "&sede=" + IdSede + "&contacto=" + IdContacto + "&idcotizacion=" + IdCotizacion + "&remision=" + remision;
        var datos = LlamarAjax("Cotizacion", "opcion=AgregarIngreso&" + parametros).split("|");
        DesactivarLoad();
        if (datos[0] == "0") {
            TablaIngreso(IdCliente, 0, IdCotizacion, IdSede, IdContacto);
            AgregarIngreso();
            swal("", datos[1], "success");
        } else {
            swal("Acción Cancelada", datos[1], "warning");
        }
        
    }, 15);
    
});

function DuplicarRegistro(id) {
    var datos = LlamarAjax("Cotizacion","opcion=DuplicarRegistro&id=" + id).split("|");
    if (datos[0] == "0") {
        Estado = "Temporal";
        $("#Estado").val(Estado);
        swal("", datos[1], "success");
        TablaIngreso(IdCliente, 0, IdCotizacion, 0, 0);
    } else
        swal("Acción Cancelada", datos[1], "warning");
}


function AgregarIngresoCot() {
    if (IdCliente == 0 || (Estado != "Temporal" && Estado != "Cotizado"))
        return false;

    if (IdSede == 0) {
        $("#Sede").focus();
        swal("Acción Cancelada", "Debe seleccionar la sede del cliente", "warning");
        return false;
    }
    if (IdContacto == 0) {
        $("#Contacto").focus();
        swal("Acción Cancelada", "Debe seleccionar el contacto del cliente", "warning");
        return false;
    }

    if (RequiereSolicitud == "SI" && CanItems == 0 && Habitual == "NO" && Solicitud == 0) {
        swal("Acción Cancelada", "Debe ingresar la solicitud del cliente", "warning");
        return false;
    }

    localStorage.setItem("idcliente", IdCliente);
    CargarModalIngreso(0,"cot");
    $("#modalIngresoCot").modal("show");
}

function ModalRemision() {
    $("#modalRemision").modal("show");
}


function BuscarRemisiom(numero) {

    if (Remision == numero)
        return false;
    LimpiarDatos();
    if ((numero * 1) == 0)
        return false;
        
    var parametros = "remision=" + numero;
    var datos = LlamarAjax('Cotizacion', 'opcion=BuscarRemision&'+parametros);
    
    if (datos != "[]") {
        var dataremi = JSON.parse(datos);
        if (dataremi[0].estado != "Ingresado" && dataremi[0].estado != "Cerrado") {
            $("#Remision").val("");
            $("#Remision").focus();
            swal("Acción Cancelada", "No se puede cargar la remisión número " + numero + " <br> con estado " + dataremi[0].estado, "warning");
            return false;
        }
        RequiereSolicitud = dataremi[0].req_solicitud;
        if (RequiereSolicitud == "SI" && Habitual == "NO") {
            swal("Acción Cancelada", "Debe ingresar la solicitud del cliente", "warning");
            LimpiarTodo();
            return false;
        }

        IdRemision = dataremi[0].id;
        Remision = dataremi[0].remision;
        IdSede = dataremi[0].idsede * 1;
        IdContacto = dataremi[0].idcontacto * 1;
        IdCliente = dataremi[0].idcliente * 1;
        $("#Cliente").val(dataremi[0].documento);
        BuscarClienteCot(dataremi[0].documento, 1, IdSede, IdContacto);
        $("#Remision").val(Remision);
        $("#Estado").val(Estado);
        TablaIngreso(IdCliente, IdRemision, -1, IdSede, IdContacto);
    } else {
        $("#Remision").focus();
        swal("Acción Cancelada", "Remisión número " + numero + " no registrada", "warning");
    }
}

function BuscarClienteCot(documento, buscar, sede, contacto) {

    if ($.trim(documento) == "")
        return false;

    if (DocumentoCli == documento)
        return false;

    DocumentoCli = documento;
                
    ActivarLoad();
    setTimeout(function () {
                
        var parametros = "documento=" + $.trim(documento) + "&bloquear=1";
        var datos = LlamarAjax('Cotizacion','opcion=BuscarCliente&'+ parametros);
        datos = datos.split("|");
        if (datos[0] == "0") {
            var data = JSON.parse(datos[1]);
            IdCliente = data[0].id;
            TablaPrecio = data[0].tablaprecio;
            DescuentoCli = data[0].descuento;
            DescuentoCliente = DescuentoCli;
            PlazoPago = data[0].plazopago;
            TipoCliente = data[0].tipocliente;
            PagoCertificado = data[0].pagcertificado;
            RequiereSolicitud = data[0].req_solicitud;
            Habitual = data[0].habitual;
            ValorIva = data[0].valor*1;
            if (Estado == "Temporal" || Estado == "Cotizado") {
                CliReteFuente = data[0].porretefuente;
                CliReteIva = data[0].porreteiva;
                CliReteIca = data[0].porreteica;
            }
            $("#TipoCliente").val(TipoCliente);
            $("#PlazoPago").val(PlazoPago);
            $("#TablaPrecio").val(data[0].precio);
            $("#Descuento").val(DescuentoCli);
            $("#NombreCliente").val(data[0].nombrecompleto);
            $("#Contacto").html(datos[2]);
            $("#Sede").html(datos[3]);
            $("#Sede").focus();

            if (sede > 0)
                $("#Sede").val(sede).trigger("change");
            if (contacto > 0)
                $("#Contacto").val(contacto).trigger("change");

            SaldoActual(IdCliente, 1);
        
            $("#SaldoClienteDev").html(SaldoTotal(IdCliente));
                                                
            if (buscar == 0) {
                TablaIngreso(IdCliente, 0, IdCotizacion, 0, 0);
            }
                

            DesactivarLoad();

        } else {
            DesactivarLoad();
        
            if (datos[0] == "8") {
                swal({
                    title: 'Advertencia',
                    text: datos[1] + "<br><br>... ¿Desea desbloquearlo?",
                    type: 'question',
                    showLoaderOnConfirm: true,
                    showCancelButton: true,
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    confirmButtonText: ValidarTraduccion('Desbloquear'),
                    cancelButtonText: ValidarTraduccion('Cancelar'),
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    preConfirm: function () {
                        return new Promise(function (resolve, reject) {
                            DesbloquearCliente(documento, 0);
                            DocumentoCli = "";
                            BuscarClienteCot(documento, 0,0,0);
                            resolve();
                        })
                    }
                }).then(function (result) {
                    if (result) {
                        swal({
                            type: 'success',
                            html: "Cliente Desbloqueado"
                        })
                    }
                },

                    function (dismiss) {
                        if (dismiss == 'cancel') {
                            LimpiarTodo();
                            DocumentoCli = "";
                            $("#Cliente").val("");
                            $("#Cliente").focus();
                            return false;
                        }
                    });

                $(".swal2-content").html(datos[1] + "<br><br>... ¿Desea desbloquearlo?");
            }
            else {
                if (datos[0] == "5") {
                    LimpiarTodo();
                    DocumentoCli = "";
                    $("#Cliente").val("");
                    $("#Cliente").focus();
                    swal("Acción Cancelada", datos[1], "warning");
                } else
                    swal("Acción Cancelada", "El documento " + documento + ", no se encuentra registrado como cliente", "warning");
            }
        }
    },15);
}


function EnviarCotizacion() {

    if (Cotizacion == 0)
        return false;

    if (Estado != "Cerrado" && Estado != "Aprobado" && Estado != "Cotizado") {
        swal("Acción Cancelada", "No se puede enviar una cotización en estado " + Estado, "warning");
        return false;
    }

    if (CorreoEmpresa == "") {
        swal("Acción Cancelada", "Cliente no posee ningún correo principal registrado", "warning");
        return false;
    }

    var revision = $("#Revision").val() * 1;
    var cansinreporte = 0;

    var equipos = document.getElementsByName("tempequipo[]");
    var servicios = document.getElementsByName("tempservicio[]");
    var ingresos = document.getElementsByName("Ingresos[]");
    var reportes = document.getElementsByName("Reportes[]");
    var canequipos = 0;
    for (var x = 0; x < equipos.length; x++) {
        if ((equipos[x].value * 1 > 0) && (servicios[x].value * 1 == 0)) {
            canequipos += 1;
        }
        if (revision > 1) {
            if (ingresos[x].value * 1 > 0 && $.trim(reportes[x].innerHTML) == "")
                cansinreporte++;
        }

    }

    if (cansinreporte > 0 && Estado == "Cotizado") {
        swal("Acción Cancelada", "No se puede cerrar la cotización porque hay " + cansinreporte + " equipos(s) sin reportar en el laboratorio", "warning");
        return false;
    }

    if (canequipos > 0) {
        swal("Acción Cancelada", "No se puede cerrar la cotización porque hay " + canequipos + " equipos(s) sin cotizar", "warning");
        return false;
    }
    
    if (canequipos > 0 && Estado == "Cotizado") {
        swal.queue([{
            title: ValidarTraduccion('Advertencia'),
            text: ValidarTraduccion('Hay ' + canequipos + ' equipo(s) sin cotizar, ¿Desea enviar y cerrar la cotización?'),
            type: 'question',
            showLoaderOnConfirm: true,
            showCancelButton: true,
            confirmButtonText: ValidarTraduccion('Si'),
            cancelButtonText: ValidarTraduccion('No'),
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            preConfirm: function () {
                return new Promise(function (resolve, reject) {
                    $("#eCliente").val($("#NombreCliente").val());
                    $("#eCorreo").val($("#Email").val());

                    $("#TipoEnvio").val("Cotizacion").trigger("change");

                    $("#spcotizacion").html(Cotizacion);

                    $("#enviar_cotizacion").modal("show");
                    resolve()
                })
            }
        }]);
    } else {
        $("#eCliente").val($("#NombreCliente").val());
        $("#eCorreo").val($("#Email").val());
        $("#TipoEnvio").val("Cotizacion").trigger("change");
        $("#Adjunto1").val("");
        $("#Adjunto2").val("");
        $("#spcotizacion").html(Cotizacion);
        $("#enviar_cotizacion").modal("show");
    }
   
}

$("#formenviarcotizacion").submit(function (e) {

    e.preventDefault();
    var observacion = $.trim($("#ObserEnvio").val())
    var correo = $.trim($("#eCorreo").val());
    var tipoenvio = $("#TipoEnvio").val();
    a_correo = correo.split(";");

    if (Pdf_Cotizacion == "") {
        $("#TipoEnvio").focus();
        swal(ValidarTraduccion("Acción Cancelada"), ValidarTraduccion("Debe de seleccionar un tipo de cotizacion válido"), "warning");
        return false;
    }

    for (var x = 0; x < a_correo.length; x++) {
        if (ValidarCorreo($.trim(a_correo[x])) == 2) {
            $("#eCorreo").focus();
            swal(ValidarTraduccion("Acción Cancelada"), ValidarTraduccion("Formato de correo onválido") + ":" + a_correo[x], "warning");
            return false;
        }
    }

    var equipos = document.getElementsByName("tempequipo[]");
    var servicios = document.getElementsByName("tempservicio[]");
    var canequipos = 0;
    for (var x = 0; x < equipos.length; x++) {
        if ((equipos[x].value * 1 > 0) && (servicios[x].value * 1 == 0)) {
            canequipos += 1;
        }
    }

    ActivarLoad();
    setTimeout(function () {
        var parametros = "cotizacion=" + Cotizacion + "&principal=" + CorreoEmpresa + "&e_email=" + correo + " &archivo=" + Pdf_Cotizacion + " &cliente=" + $("#eCliente").val() + "&id=" + IdCotizacion + "&observacion=" + observacion + "&canequipos=" + canequipos + "&remision=" + Remision + "&tipoenvio=" + tipoenvio;
        var datos = LlamarAjax("Cotizacion", "opcion=EnvioCotizacion&" + parametros);
        DesactivarLoad();
        datos = datos.split("|");
        if (datos[0] == "0") {
            swal("", ValidarTraduccion(datos[1]) + " " + datos[2], "success");
            $("#enviar_remision").modal("hide");
            Cli_Correo = $("#eCorreo").val();
            Estado = "Cerrado";
            $("#Estado").val(Estado);
            $("#enviar_cotizacion").modal("hide");
        } else
            swal("Acción Cancelada", datos[1], "warning");
    }, 15);

    return false;
});

$("#tablamodalclientecot > tbody").on("click", "tr", function (e) {
    var row = $(this).parents("td").context.cells;
    var numero = row[1].innerText;
    $("#modalClientesCot").modal("hide");
    $("#Cliente").val(numero);
    BuscarClienteCot(numero,0,0,0);
});



function ConsultarCotizaciones() {


    var magnitud = $("#CMagnitud").val() * 1;
    var equipo = $("#CEquipo").val();
    var modelo = $("#CModelo").val();
    var marca = $("#CMarca").val() * 1;
    var intervalo = $("#CIntervalo").val();
    var ingreso = $("#CIngreso").val() * 1;
    var serie = $.trim($("#CSerie").val());

    var remision = $("#CRemision").val() * 1;
    var cotizacion = $("#CCotizacion").val() * 1;
    var solicitud = $("#CSolicitud").val() * 1;
    var cliente = $("#CCliente").val() * 1;
    var pais = $("#CPais").val() * 1;
    var departamento = $("#CDepartamento").val() * 1
    var ciudad = $("#CCiudad").val() * 1;
    var usuario = $("#CUsuario").val() * 1;
    var servicio = $("#CServicio").val() * 1;
    var metodo = $("#CMetodo").val() * 1;
    var estado = $("#CEstado").val();
    var cingreso = $("#CSIngreso").val();

    var fechad = $("#CFechaDesde").val();
    var fechah = $("#CFechaHasta").val();

    if (fechad == "" && fechah == "") {
        swal("", ValidarTraduccion("debe de ingresar una fecha o números de días"), "warning");
        return false;
    }
//Andres
    ActivarLoad();
    setTimeout(function () {
        var parametros = "remision=" + remision + "&cotizacion=" + cotizacion + "&cliente=" + cliente + "&ingreso=" + ingreso + 
            "&fechad=" + fechad + "&fechah=" + fechah + "&magnitud=" + magnitud + "&equipo=" + equipo + "&marca=" + marca + "&modelo=" + modelo +
            "&intervalo=" + intervalo + "&serie=" + serie + "&pais=" + pais + "&departamento=" + departamento + "&ciudad=" + ciudad + "&usuario=" + usuario +
            "&metodo=" + metodo + "&servicio=" + servicio + "&solicitud=" + solicitud + "&estado=" + estado + "&cingreso=" + cingreso;
        var datos = LlamarAjax("Cotizacion","opcion=ConsultarCotizaciones&"+parametros).split("||");
        DesactivarLoad();
        //Fin Codigo

        var datajson = JSON.parse(datos[0]);
        $('#TablaCotizaciones').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "imprimir" },
                { "data": "cotizacion", "className": "text-XX" },
                { "data": "solicitud" },
                { "data": "estado" },
                { "data": "documento" },
                { "data": "cliente" },
                { "data": "sede" },
                { "data": "contacto" },
                { "data": "departamento" },
                { "data": "ciudad" },
                { "data": "telefonos" },
                { "data": "email" },
                { "data": "cantidad" },
                { "data": "itemingresado" },
                { "data": "subtotal", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "descuento", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "iva", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "total", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "fecha" },
                { "data": "usuario" },
                { "data": "reemplazado" },
                { "data": "aprobado" },
                { "data": "rechazado" },
                { "data": "anula" },
                { "data": "envio" }
            ],

            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy','colvis'
            ],
            "colReorder": true
        });

        var etiqueta = ['Cotizado', 'Facturado', 'Diferencia'];

        var config = {
            type: 'bar',
            data: {
                datasets: [],
                labels: []
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                },
                responsive: true,
                tooltips: {
                    enabled: true
                },
                legend: {
                    position: 'top',
                    display: (movil == 1 ? false : true)
                }
            }
        }

        var newDataset1 = {
            label: etiqueta[0],
            backgroundColor: colores[0],
            data: [],
        };

        var newDataset2 = {
            label: etiqueta[1],
            backgroundColor: colores[3],
            data: [],
        };
                
        var newDataset3 = {
            type: 'line',
            label: etiqueta[2],
            fill: false,
            backgroundColor: colores[4],
            borderColor: colores[4],
            data: [],
        };

        var data = JSON.parse(datos[1]);
        tabla = "";
        tcotizado = 0;
        tfacturado = 0;
        
        for (var x = 0; x < data.length; x++) {
            tcotizado += data[x].cotizado;
            tfacturado += data[x].facturado;
            porcentaje = data[x].facturado * 100 / data[x].cotizado;
            tabla += "<tr>" +
                "<td>" + data[x].fecha + "</td>" +
                "<td align='right'>" + formato_numero(data[x].cotizado, 0,_CD,_CM,"") + "</td>" +
                "<td align='right'><b>(" + formato_numero(porcentaje, _DE,_CD,_CM,"") + "%)</b> " + formato_numero(data[x].facturado, 0,_CD,_CM,"") + "</td>" +
                "<td align='right'><b>(" + formato_numero(100 - porcentaje, _DE,_CD,_CM,"") + "%)</b> " + formato_numero(data[x].cotizado - data[x].facturado, 0,_CD,_CM,"") + "</td></tr>";
            config.data.labels.push(data[x].fecha);

            newDataset1.data.push(data[x].cotizado);
            newDataset2.data.push(data[x].facturado);
            newDataset3.data.push(data[x].cotizado - data[x].facturado);
        }
        porcentaje = tfacturado * 100 / tcotizado;
        tabla += "<tr>" +
            "<td align='right' class='text-info text-XX'>TOTALES</td>" +
            "<td align='right' class='text-info text-XX'>" + formato_numero(tcotizado, 0,_CD,_CM,"") + "</td>" +
            "<td align='right' class='text-info text-XX'><b>(" + formato_numero(porcentaje, _DE,_CD,_CM,"") + "%)  " + formato_numero(tfacturado, 0,_CD,_CM,"") + "</td>" +
            "<td align='right' class='text-info text-XX'><b>(" + formato_numero(100-porcentaje, _DE,_CD,_CM,"") + "%)  " + formato_numero(tcotizado - tfacturado, 0,_CD,_CM,"") + "</td></tr>";
        $("#TablaCot").html(tabla);

        config.data.datasets.push(newDataset1);
        config.data.datasets.push(newDataset2);
        config.data.datasets.push(newDataset3);

        if (mygrafica != null) {
            mygrafica.destroy();
        }
        mygrafica = new Chart(grafica, config);
        mygrafica.update();
    }, 15);
}

$("#TablaCotizaciones > tbody").on("dblclick", "tr", function () {
    var row = $(this).parents("td").context.cells;
    var numero = row[1].innerText;
    $('#tabcotizacion a[href="#tabcrear"]').tab('show')
    BuscarCotizacion(numero);
});

function GuardarDocumento(id, tipo) {
    var documento = document.getElementById(id);
    if ($.trim(documento.value) != "") {

        var file = documento.files[0];
        var data = new FormData();
        data.append('documento', file);
        var resultado = '';
        var direccion = url + "Cotizacion/" + (tipo == 3 ?  "AdjuntarExcel" : "AdjuntarArchivo" + tipo);
        $.ajax({
            url: direccion,
            type: 'POST',
            contentType: false,
            data: data,
            processData: false,
            async: false,
            success: function (datos) {
                datos = datos.split("|");
                if (datos[0] != "0") {
                    swal(ValidarTraduccion("Error al cargar el archivo"), "", "warning");
                } else {
                    if (datos[2] != "XX")
                        swal(ValidarTraduccion(datos[2]), "", "warning");
                }
            }
        });
    }
}

function VerEstadoCuenta() {
    var correo = $("#Email").val();
    if (ValidarCorreo(correo) == 2) {
        $("#Contacto").focus();
        swal("Acción Cancelada", "Debe de seleccionar un contacto con un correo válido", "warning");
        return false;
    }
    var cliente = $("#NombreCliente").val();
    EstadoCuenta(IdCliente, cliente, CorreoEmpresa, TipoCliente, PlazoPago, CorreoEmpresa);
}

function VerSolicitud() {
    var id = $("#DetIdSolicitud").val() * 1;
    if (id > 0) {
        var datos = LlamarAjax("Cotizacion", "opcion=DetalleSolicitud&id=" + id);
        var data = JSON.parse(datos);
        $("#SolMagnitud").html(data[0].magnitud);
        $("#SolEquipo").html(data[0].equipo);
        $("#SolMarca").html(data[0].marca);
        $("#SolModelo").html(data[0].modelo);
        $("#SolIntervalo").html(data[0].intervalo);
        $("#SolSerie").html(data[0].serie);
        $("#SolIngreso").html(data[0].ingreso);
        $("#SolCantidad").html(data[0].cantidad);
        $("#SolAcreditacion").html(data[0].acreditado);
        $("#SolSitio").html(data[0].sitio);
        $("#SolExpress").html(data[0].express);
        $("#SolSubContratado").html(data[0].subcontratado);
        $("#SolPunto").html(data[0].punto);
        $("#SolMetodo").html(data[0].metodo);
        $("#SolNombreCa").html(data[0].nombreca);
        $("#SolDireccion").html(data[0].direccion);
        $("#SolObservacion").html(data[0].observacion);
        $("#SolDeclaracion").html(data[0].declaracion);
        $("#SolCertificado").html(data[0].certificado);
        $("#SolProxima").html(data[0].proxima);
        $("#ModalSolEquipo").modal({ backdrop: 'static', keyboard: false }, "show");
    } else {
        swal("Acción Cancelada", "Este servicio no viene de una solicitud de cliente", "warning");
    }
}

function ModalCambioCliente() {
    if (Cotizacion == 0)
        return false;

    $("#AcCliente").html($("#NombreCliente").val() + " (" + $("#Cliente").val() + ")");
    $("#AcDireccion").html($("#Sede option:selected").text());
    $("#AcContacto").html($("#Contacto option:selected").text());

    $("#CaCliente").val(IdCliente).trigger("change");
    $("#CaContacto").val($("#Contacto").val()).trigger("change");
    $("#CaDireccion").val($("#Sede").val()).trigger("change");

    $("#modalCambioEmpresa").modal("show");
}

function CambioSedeContacto(cliente) {
    $("#CaContacto").html("");
    $("#CaDireccion").html("");
    if (cliente * 1 == 0)
        return false;
    var datos = LlamarAjax("Cotizacion" , "opcion=BuscarSedeCliente&cliente=" + cliente).split("|");
    $("#CaContacto").html(datos[0]);
    $("#CaDireccion").html(datos[1]);
}

function CambiarCliente() {
    var cliente = $("#CaCliente").val() * 1;
    var direccion = $("#CaDireccion").val() * 1;
    var contacto = $("#CaContacto").val() * 1;

    var mensaje = "";

    if (contacto == 0) {
        $("#CaContacto").focus();
        mensaje = "Debe seleccionar el contacto del cliente ";
    }

    if (direccion == 0) {
        $("#CaDireccion").focus();
        mensaje = "Debe seleccionar la dirección del cliente <br>" + mensaje;
    }

    if (cliente == 0) {
        $("#CaCliente").focus();
        mensaje = "Debe seleccionar un cliente <br> " + mensaje;
    }

    if (mensaje != "") {
        swal("Acción Cancelada", mensaje, "warning");
        return false;
    }

    var datos = LlamarAjax("Cotizacion", "opcion=CambioSedeCot&id=" + IdCotizacion + "&sede=" + direccion + "&contacto=" + contacto + "&cliente=" + cliente).split("|");
    if (datos[0] == "0") {
        swal("", datos[1], "success");
        $("#modalCambioEmpresa").modal("hide");
        var numero = Cotizacion;
        LimpiarTodo();
        IdCotizacion = 0;
        Cotizacion = 0;
        $("#Cotizacion").val(numero);
        BuscarCotizacion(numero);
    } else
        swal("Acción Cancelada", datos[1], "warning");
}

function AprobarCotizacion() {

    if (IdCotizacion > 0) {
        if (Estado != "POR REEMPLAZAR" && Estado != "Cerrado") {
            swal("Acción Cancelada", "No se puede aprobar una cotización en estado " + Estado, "warning");
            return false;
        }
        if (Estado == "POR REEMPLAZAR") {
            $("#operacionap").val("Rechazado").trigger("change");
            $("#operacionap").prop("disabled", false);
        } else
            $("#operacionap").val("Aprobado").trigger("change");

        $("#Adjunto1").val("");
        $("#usuarioap").val("");
        $("#cedulaap").val("");
        $("#fechaap").val("");
        $("#horaap").val("");
        $("#cargoap").val("");
        $("#observacionap").val("");
        $("#modalAprobar").modal("show");
    }
}

function GuardarAprobacion() {
    var operacion = $("#operacionap").val();
    var usuario = $.trim($("#usuarioap").val());
    var cargo = $.trim($("#cargoap").val());
    var cedula = $.trim($("#cedulaap").val());
    var fecha = $.trim($("#fechaap").val());
    var hora = $.trim($("#horaap").val());
    var observacion = $.trim($("#observacionap").val());
    var archivo = $.trim($("#Adjunto3").val());
    var mensaje = "";

    if (archivo == "") {
        $("#Adjunto3").focus();
        mensaje = "<br> Debe de ingresar el archivo pdf de la aprobación del cliente " + mensaje;
    }

    if (operacion == "Rechazado" && observacion == "") {
        $("#observacionap").focus();
        mensaje = "<br> Debe de ingresar la observación por la cual rechaza la cotización";
    }

    if (hora == "") {
        $("#horaap").focus();
        mensaje = "<br> Debe de ingresar la hora que aprueba/rechaza la cotización " + mensaje;
    }

    if (fecha == "") {
        $("#fechaap").focus();
        mensaje = "<br> Debe de ingresar la fecha que aprueba/rechaza la cotización " + mensaje;
    } else {
        if (fecha > output2) {
            $("#FechaRepor").focus();
            swal("Acción Cancelada", "La fecha no puede ser mayor a la fecha actual", "warning");
            return false;
        }
    }

    if (cargo == "") {
        $("#cargoap").focus();
        mensaje = "<br> Debe de ingresar el cargo de la persona que aprueba/rechaza la cotización " + mensaje;
    }
    
    if (usuario == "") {
        $("#usuarioap").focus();
        mensaje = "<br> Debe de ingresar el nombre completo de la persona que aprueba/rechaza la cotización " + mensaje;
    }
    if (mensaje != "") {
        swal("Acción Cancelada", mensaje, "warning");
        return false;
    }

    var parametros = "cotizacion=" + Cotizacion + "&usuario=" + usuario + "&cedula=" + cedula +
        "&cargo=" + cargo + "&observacion=" + observacion + "&operacion=" + operacion + "&fecha=" + fecha + "&hora=" + hora;
        var datos = LlamarAjax("Cotizacion","opcion=AprobarCotizacion"+parametros).split("|");
    if (datos[0] == "0") {
        $("#modalAprobar").modal("hide");
        Estado = operacion;
        $("#Estado").val(Estado);
        swal("", datos[1], "success");
        EscucharMensaje(datos[1]);
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
}

function VerCotizacionAprobada() {
    if (Estado == "Aprobado") {
        var datos = LlamarAjax("Cotizacion" , "opcion=UsurioAprobarCotizacion&cotizacion=" + Cotizacion);
        var data = JSON.parse(datos);

        if (data[0].idusuario * 1 > 0) {
            if (data[0].archivo_aprueba == "1")
                window.open(url + "CotizacionAprobada/" + Cotizacion + ".pdf");
            else {
                var orden = data[0].archivo_aprueba.split("|");
                window.open(url + "OrdenCompra/" + orden[1] + ".pdf");
            }
        }            
        else
            swal("Acción Cancelada", "Esta cotización fue aprobada por la página web", "warning");
    }
}

function DescripcionPreciosCot() {
    var datos = LlamarAjax("Cotizacion", "opcion=DescripcionPrecios");
    var data = JSON.parse(datos);
    for (var x = 0; x < data.length; x++) {
        $("#PLaPrecioCot" + data[x].id).html(data[x].descripcion);
    }
}

DescripcionPreciosCot();

function RecargarCotizacion(tipo) {
    switch (tipo) {
        case 1:
            $("#Servicio, #CServicio").html(CargarCombo(4,1));
            break;
        case 2:
            var equipo = $("#Equipo").val();
            $("#Metodo").html(CargarCombo(17, 1, "", equipo));

    } 
}


/*if (localStorage.getItem('ModuloCotizacion') != 2) {
    MantenSesion(0);
    localStorage.setItem('ModuloCotizacion', 1);
}*/

function ActualizarPrecios(id, precio, descuento, lote, cantidad) {
    $("#IdPreCambio").val(id);
    $("#LoteCambio").val(lote);
    $("#CantidadCam").val(cantidad);
    $("#PrecioCam").val(formato_numero(precio, _DE, _CD, _CM, ""));
    $("#DescuentoCam").val(descuento);
    CalcularPrecioCam("");
    $("#modalCambioPrecio").modal("show");
}

function CalcularPrecioCam(caja) {
    if (caja != "")
        ValidarTexto(caja, 1);
    var precio = NumeroDecimal($("#PrecioCam").val());
    var descuento = NumeroDecimal($("#DescuentoCam").val());
    var cantidad = NumeroDecimal($("#CantidadCam").val());
    $("#SubTotalCam").val(formato_numero((precio * cantidad) - ((precio*cantidad) * descuento / 100), _DE, _CD, _CM, ""));
}

function CambiarPrecio() {
    var id = $("#IdPreCambio").val() * 1;
    var lote = $("#LoteCambio").val() * 1;
    var precio = NumeroDecimal($("#PrecioCam").val());
    var descuento = NumeroDecimal($("#DescuentoCam").val());
    var parametros = "Cotizacion=" + Cotizacion + "&precio=" + precio + "&descuento=" + descuento + "&id=" + id + "&lote=" + lote;
    var datos = LlamarAjax("Cotizacion", "opcion=ActualizarPrecio&" + parametros).split("|");
    if (datos[0] == "0") {
        $("#modalCambioPrecio").modal("hide");
        swal("", datos[1], "success");
        TablaIngreso(IdCliente, 0, IdCotizacion, 0, 0);
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
}

function ActualizarIngresoCo() {
    if (Estado != "Temporal" && Estado != "Cotizado") {
        swal("Acción Cancelada", "No se puede actualizar un ingreso con cotización en estado " + Estado, "warning");
        return false;
    }

    var ingreso = $("#Ingreso").val() * 1;

    if (ingreso == 0) {
        swal("Acción cancelada", "Este item no posee un ingreso asociado", "warning");
        return false;
    }


    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: ValidarTraduccion('¿Seguro que desea actualizar el ingreso número ' + ingreso + '?'),
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Actualizar'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                $.post("Cotizacion/ActualizarIngresoCot", "ingreso=" + ingreso + "&idcotizacion=" + IdCotizacion)
                    .done(function (data) {
                        datos = data.split("|");
                        if (datos[0] == "0") {
                            Estado = "Temporal";
                            $("#Estado").val(Estado);
                            var data = JSON.parse(datos[2]);
                            $("#Magnitud").val(data[0].idmagnitud).trigger("change");
                            $("#Equipo").val(data[0].idequipo).trigger("change");
                            $("#Marca").val(data[0].idmarca).trigger("change");
                            $("#Modelo").val(data[0].idmodelo).trigger("change");
                            $("#Intervalo").val(data[0].idintervalo).trigger("change");
                            $("#Serie").val(data[0].serie);
                            BuscarPrecio();
                            resolve()
                        } else {
                            reject(datos[1]);
                        }
                    })
            })
        }
    }]);
}


$('select').select2();
DesactivarLoad();

