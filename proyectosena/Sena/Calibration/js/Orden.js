﻿//CargarFilas();


d = new Date();
month = d.getMonth() + 1;
day = d.getDate();

var Anio = $.trim(d.getFullYear());
Anio = $.trim(Anio.substr(2, 2))


output = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-01';

output2 = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-' +
    (day < 10 ? '0' : '') + day;

$("#CFechaDesde").val(output);
$("#CFechaHasta").val(output2);

var IdCliente = 0;
var totales = 0;
var Orden = 0;
var DocumentoCli = "";
var TotalSeleccion = 0;
var Ingresos = "";

$("#CMagnitud").html(CargarCombo(1, 1));
$("#CEquipo").html(CargarCombo(2, 1));
$("#CMarca").html(CargarCombo(3, 1));
$("#CCliente").html(CargarCombo(9, 1));
$("#CModelo").html(CargarCombo(5, 1, "", "-1"));
$("#CIntervalo").html(CargarCombo(6, 1, "", "-1"));
$("#CUsuario").html(CargarCombo(13, 1));

tb = $('input');
$(tb).keypress(enter2tab);

tb = $('select');
$(tb).keypress(enter2tab);

function enter2tab(e) {
    if (e.keyCode == 13) {
        id = $(this).attr("id");
        
        cb = parseInt($(this).attr('tabindex'));

        if (id == "Cliente") {
            if ($.trim($(this).val()) != "") {

                BuscarCliente($(this).val());
                $("#Sede").focus();
                return false;
            } 
        }
                
        if ($(':input[tabindex=\'' + (cb + 1) + '\']') != null) {
            $(':input[tabindex=\'' + (cb + 1) + '\']').focus();
            $(':input[tabindex=\'' + (cb + 1) + '\']').select();
            e.preventDefault();

            return false;
        }

        if ($(':select[tabindex=\'' + (cb + 1) + '\']') != null) {
            console.log("1");
            $(':select[tabindex=\'' + (cb + 1) + '\']').focus();
            $(':select[tabindex=\'' + (cb + 1) + '\']').select();
            e.preventDefault();

            return false;
        }
    }
}




function LimpiarTodo() {
    LimpiarDatos();
    $("#Cliente").val("");
    $("#Cliente").focus();
}

LimpiarTodo();

function LimpiarDatos() {

    IdCliente = 0;
    DocumentoCli = "";
    TipoCliente = 0;
    PlazoPago = 0;
    TipoPrecio = 0;
    totales = 0;
    TotalSeleccion = 0;
    Orden = 0;
    
    $("#NombreCliente").val("");
    $("#Orden").val("");
    $("#Direccion").val("");

    $("#Orden").attr("disabled",false);
    
    $("#Ciudad").val("");
    $("#Departamento").val("");
    
    $("#PlazoPago").val("0");
    $("#Totales").val("0");

    $("#bodyOrden").html("");

    $("#Email").val("");
    $("#Telefonos").val("");
    $("#TipoCliente").val("");

    $("#OArchivo").val("");
    $("#ONumero").val("");
    $("#OTipo").val("").trigger("change");
    $("#OSeleccion").val("");
        
}

function ValidarCliente(documento) {
    if ($.trim(DocumentoCli) != "") {
        if (DocumentoCli != documento) {
            DocumentoCli = "";
            LimpiarDatos();
        }
    }
}


function BuscarCliente(documento) {

    if ($.trim(documento) == "")
        return false;

    if (DocumentoCli == documento)
        return false;

    DocumentoCli = documento;
    
    var parametros = "documento=" + $.trim(documento);
    var datos = LlamarAjax('Cotizacion','opcion=BuscarCliente&'+parametros);
    datos = datos.split("|");
    if (datos[0] == "0") {
        var data = JSON.parse(datos[1]);
        DesactivarLoad();
        IdCliente = data[0].id;
        PlazoPago = data[0].plazopago;
        TipoCliente = data[0].tipocliente;
        $("#TipoCliente").val(TipoCliente);
        $("#NombreCliente").val(data[0].nombrecompleto);
        $("#Ciudad").val(data[0].ciudad);
        $("#Departamento").val(data[0].departamento);
        $("#Direccion").val(data[0].direccion);
        $("#Email").val(data[0].email);
        $("#Telefonos").val(data[0].telefono + " / " + data[0].celular);
        TablaOrden(IdCliente, 0);
        if (totales == 0) {
            $("#Cliente").select();
            $("#Cliente").focus();
            swal("Acción Cancelada", "Este cliente no posee ingresos aprobados pendientes por órden de compra", "warning")
        } else {
            SaldoActual(IdCliente, 1);
        }
        
    }else{
        DocumentoCli = "";
        $("#Cliente").val("");
        $("#Cliente").focus();
        swal("Acción Cancelada", "El documento " + documento + ", no se encuentra registrado como cliente", "warning");
    }
    
}

function AgregarOrden() {
    if (IdCliente == 0)
        return false;
    if (TotalSeleccion == 0) {
        swal("Acción Cancelada", "Debe de seleccionar por lo mínimo un ingreso", "warning");
        return false;
    }

    Ingresos = "";
    var ing = document.getElementsByName("IngresoSel[]");
    var selec = document.getElementsByName("Seleccionado[]");
    
    for (var x = 0; x < ing.length; x++) {
        if (selec[x].value * 1 > 0) {
            if (Ingresos == "") {
                Ingresos = ing[x].value;
            } else {
                Ingresos += "," + ing[x].value;
            }
        } 
    }
    $("#OSeleccion").val(Ingresos);
    $("#modalOrden").modal("show");
}


function SelecccionarFila(x, numero) {
   
    var selec = $("#Seleccionado" + x).val() * 1;
    if (selec == 0) {
        $("#fila-" + x).addClass("bg-gris");
        $("#Seleccionado" + x).val("1");
        TotalSeleccion += 1;
    } else{
        $("#fila-" + x).removeClass("bg-gris");
        $("#Seleccionado" + x).val("0");
        TotalSeleccion -= 1;
    } 
}

function BuscarOrden(orden) {
    orden = $.trim(orden);
    if (orden == Orden)
        return false;
    if (orden == "") {
        LimpiarTodo();
        return false;
    }
    Orden = orden;
    $("#Orden").val(Orden);
    var datos = LlamarAjax("Cotizacion","opcion=BuscarOrdenCompra&orden=" + Orden + "&idcliente=" + IdCliente);
    if (datos == "XX") {
        LimpiarTodo();
        $("#Orden").focus();
        swal("Acción Cancelada", "Este número de órden de compra no se encuentra registrado", "warning");
        return false;
    }

    BuscarCliente($.trim(datos));
    TablaOrden(IdCliente, 0);
}

function ConsularOrdenes() {


    var magnitud = $("#CMagnitud").val() * 1;
    var equipo = $("#CEquipo").val();
    var modelo = $("#CModelo").val();
    var marca = $("#CMarca").val() * 1;
    var intervalo = $("#CIntervalo").val();
    var ingreso = $.trim($("#CIngreso").val());
    var serie = $.trim($("#CSerie").val());

    var orden = $.trim($("#COrden").val());

    var remision = $("#CRemision").val() * 1;
    var cliente = $("#CCliente").val() * 1;
    var usuario = 0;
    var tipo = $("#CTipo").val();
    var fechad = $("#CFechaDesde").val();
    var fechah = $("#CFechaHasta").val();

    if (fechad == "" && fechah == "") {
        swal("Acción Cancelada", ValidarTraduccion("debe de ingresar una fecha o números de días"), "warning");
        return false;
    }

    ActivarLoad();
    setTimeout(function () {
        var parametros = "remision=" + remision + "&cliente=" + cliente + "&ingresos=" + ingreso +
            "&fechad=" + fechad + "&fechah=" + fechah + "&magnitud=" + magnitud + "&equipo=" + equipo + "&marca=" + marca + "&modelo=" + modelo +
            "&intervalo=" + intervalo + "&serie=" + serie + "&usuario=" + usuario + "&orden=" + orden + "&tipo=" + tipo;
        var datos = LlamarAjax("Cotizacion","opcion=ConsultarOrden&"+ parametros);
        DesactivarLoad();

        var datajson = JSON.parse(datos);
        $('#TablaOrdenes').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "fila" },
                { "data": "ingreso" },
                { "data": "remision" },
                { "data": "cliente" },
                { "data": "equipo" },
                { "data": "observacion" },
                { "data": "ncotizacion" },
                { "data": "factura" },
                { "data": "tipo" },
                { "data": "numero" },
                { "data": "fechacom" },
                { "data": "usuariocompra" },
                { "data": "ver" },
                { "data": "eliminar" },
                
            ],

            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy','colvis'
            ]
        });
    }, 15);
}

function TablaOrden(cliente, tipo) {
    $("#bodyOrden").html("");
    var datos = LlamarAjax("Cotizacion","opcion=TablaOrdenCompra&cliente=" + cliente + "&orden=" + Orden + "&tipo=" + tipo);
    var resultado = "";
    totales = 0;
    var estado = "";
    if (datos != "[]") {
        eval("data=" + datos);
        for (var x = 0; x < data.length; x++) {
            switch (data[x].estado) {
                case "Cerrado":
                    estado = "<label class='bg-warning'>" + data[x].estado + "</label>";
                    break;
                case "Aprobado":
                    estado = "<label class='bg-success'>" + data[x].estado + "</label>";
                    break;
                case "POR REEMPLAZAR":
                    estado = "<label class='bg-danger'>" + data[x].estado + "</label>";
                    break;
                case "Reemplazado":
                    estado = "<label class='bg-danger'>" + data[x].estado + "</label>";
                    break;
                default:
                    estado = "<label class='bg-default'>" + data[x].estado + "</label>";
                    break;
            }
            resultado += "<tr id='fila-" + x + "' ondblclick=\"SelecccionarFila(" + x + "," + data[x].numero + ")\">" +
                "<td>" + (x + 1) + "</td>" +
                "<td><b>" + data[x].ingreso + "</b><br>" + data[x].fechaing + "<br>" + data[x].usuarioing + "</td>" +
                "<td>" + data[x].equipo + "</td>" +
                "<td>" + data[x].servicio + "</label></td> " +
                "<td>" + estado + "</label></td> " +
                "<td align='center' class='text-XX'>" + data[x].cantidad + "</td>" +
                "<td>" + (data[x].cotizacion ? data[x].cotizacion : "0") + "</td>" +
                "<td>" + data[x].ordencompra + "</td>" +
                "<td>" + data[x].observacion + "<input type='hidden' value='0' name='Seleccionado[]' id='Seleccionado" + x + "'><input type='hidden' name='IngresoSel[]' value='" + data[x].ingreso + "'></td></tr>";
            totales += data[x].cantidad; 

        }
    }

    $("#bodyOrden").html(resultado);
    $("#Totales").val(totales);
}

function VerOrdenCompra(orden) {
    window.open(url_cliente + "OrdenCompra/" + orden + ".pdf");
}

function GuardarOrden() {
    var mensaje = "";
    var archivo = $("#OArchivo").val();
    var numero = $.trim($("#ONumero").val());
    var tipo = $("#OTipo").val();
    var ingresos = $("#OSeleccion").val();
    var fecha = $("#OFecha").val();
    var hora = $("#OHora").val();
    var usuario = $.trim($("#OUsuario").val());
    var cedula = $.trim($("#OCedula").val());
    var cargo = $.trim($("#OCargo").val());

    if (archivo == "") {
        $("#OArchivo").focus();
        mensaje = "<br> Debe seleccionar el archivo PDF" + mensaje; 
    }

    if (numero == "") {
        $("#ONumero").focus();
        mensaje = "<br> Debe de ingresar el número de órden de compra" + mensaje;
    }

    if (tipo == "") {
        $("#OTipo").focus();
        mensaje = "<br> Debe de seleccionar el tipo de órden de compra" + mensaje;
    }

    if (fecha == "") {
        $("#OFecha").focus();
        mensaje = "<br> Debe de seleccionar la fecha de la órden de compra" + mensaje;
    }

    if (hora == "") {
        $("#OHora").focus();
        mensaje = "<br> Debe de seleccionar la hora de la órden de compra" + mensaje;
    }

    if (mensaje != "") {
        swal("Acción Cancelada", mensaje, "warning");
        return false;
    }

    var parametros = "numero=" + numero + "&tipo=" + tipo + "&ingresos=" + ingresos + "&idcliente=" + IdCliente +
        "&fecha=" + fecha + "&hora=" + hora + "&usuario=" + usuario + "&cedula=" + cedula + "&cargo=" + cargo;
    var datos = LlamarAjax("Cotizacion","opcion=GuardarOrden&"+ parametros).split("|");

    if (datos[0] == "0") {
        $("#OSeleccion").val("");
        $("#OTipo").val("").trigger("change");
        $("#ONumero").val("");
        $("#OArchivo").val("");
        $("#modalOrden").modal("hide");
        Orden = numero;
        TablaOrden(IdCliente, 1);
        Orden = 0;
        swal("", datos[1], "success");
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }

}
function EliminarOrden(id, equipo, numero, ingreso) {

    if (numero * 1 == 0)
        return false;
    
    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: ValidarTraduccion('¿Seguro que desea eliminar la Orden de Compra Nro ' + numero + ' del equipo ' + equipo + '?'),
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Si'),
        cancelButtonText: ValidarTraduccion('No'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                $.post(url_servidor+"Cotizacion","opcion=EliminarOrden&id=" + id + "&ingreso=" + ingreso)
                    .done(function (data) {
                        datos = data.split("|");
                        if (datos[0] == "0") {
                            ConsularOrdenes();
                            resolve()
                        } else {
                            reject(datos[1]);
                        }
                    })
            })
        }
    }]);
};

function EliminarTodaOrden() {

    if (Orden == 0)
        return false;

    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: ValidarTraduccion('¿Seguro que desea eliminar toda la Orden de Compra Nro ' + Orden + '?'),
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Si'),
        cancelButtonText: ValidarTraduccion('No'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                $.post("Cotizacion/EliminarTodaOrden", "orden=" + Orden + "&idcliente=" + IdCliente)
                    .done(function (data) {
                        datos = data.split("|");
                        if (datos[0] == "0") {
                            LimpiarDatos();
                            $("#Cliente").val("");
                            $("#Cliente").focus();
                            resolve()
                        } else {
                            reject(datos[1]);
                        }
                    })
            })
        }
    }]);
};
   
$("#tablamodalclienteord > tbody").on("click", "tr", function (e) {
    var row = $(this).parents("td").context.cells;
    var numero = row[1].innerText;
    $("#modalClientesOrd").modal("hide");
    $("#Cliente").val(numero);
    BuscarCliente(numero);
});


$('select').select2();
DesactivarLoad();

