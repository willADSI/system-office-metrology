﻿d = new Date();
month = d.getMonth() + 1;
day = d.getDate();

var Anio = $.trim(d.getFullYear());
var table;

var movil = 0;
if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
    movil = 1;
}

var colores = ['rgb(255, 99, 132)', 'rgb(255, 159, 64)', 'rgb(255, 205, 86)', 'rgb(75, 192, 192)', 'rgb(54, 162, 235)', 'rgb(153, 102, 255)', 'rgb(2, 254, 21)', 'rgb(6, 252, 245)', 'rgb(240, 20, 10)', 'rgb(254, 246, 2)'];

var grafica1 = document.getElementById('Grafica1').getContext('2d');
var grafica2 = document.getElementById('Grafica2').getContext('2d');
var grafica3 = document.getElementById('Grafica3').getContext('2d');
var grafica4 = document.getElementById('Grafica4').getContext('2d');

var mygrafica1 = null;
var mygrafica2 = null;
var mygrafica3 = null;
var mygrafica4 = null;

output = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-01';

output2 = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-' +
    (day < 10 ? '0' : '') + day;

$("#CFechaDesde").val(output);
$("#CFechaHasta").val(output2);

$("#CRFechaDesde").val(output);
$("#CRFechaHasta").val(output2);

var opciones = "<option value=''>--Seleccione--</option>";
for (var x = Anio; x >= 2018; x--) {
    opciones += "<option value='" + x + "'>" + x + "</option>";
}
$("#Anio").html(opciones);
$("#Cuenta").html(CargarCombo(25, 1));
$("#CCuenta").html(CargarCombo(25, 1));
$("#CBanco").html(CargarCombo(24, 1));
$("#MOperacion").html(CargarCombo(31, 1));

function GuardarDocumento() {
    var documento = document.getElementById("documento");
    if ($.trim(documento.value) != "") {
        var file = documento.files[0];
        var data = new FormData();
        data.append('documento', file);
        var resultado = '';
        var direccion = url + "Cotizacion/AdjuntarExcel";
        $.ajax({
            url: direccion,
            type: 'POST',
            contentType: false,
            data: data,
            processData: false,
            async: false,
            success: function (datos) {
                datos = datos.split("|");
                if (datos[0] != "0") {
                    swal(ValidarTraduccion("Error al cargar el archivo"), "", "error");
                } else {
                    if (datos[2] != "XX")
                        swal(ValidarTraduccion(datos[2]), "", "error");
                }
            }
        });
    }
}

function SubirArchivo() {

    var cuenta = $("#Cuenta").val() * 1;
    var anio = $("#Anio").val() * 1;
    var mes = $("#Mes").val() * 1;

    var tipoarchivo = LlamarAjax("Caja","opcion=TipoArchivoBanco&cuenta=" + cuenta);
    var mensaje = "";

   
    if (mes == 0) {
        $("#Mes").focus();
        mensaje = "Debe seleccionar el mes<br>" + mensaje;
    }
    if (anio == 0) {
        $("#Anio").focus();
        mensaje = "Debe seleccionar el Año<br>" + mensaje;
    }
    if (cuenta == 0) {
        $("#Anio").focus();
        mensaje = "Debe seleccionar una cuenta bancaria<br>" + mensaje;
    } else {
        if (tipoarchivo == "XX")
            mensaje = "No se encuentra configurado el tipo de archivo del banco para importar";
    }

    if (mensaje != "") {
        swal("Acción Cancelada", mensaje, "warning");
        return false;
    }

    var valor = "";

    if (tipoarchivo.lastIndexOf("Texto") >= 0) {
        valor = '<h3>' + ValidarTraduccion("Seleccione el archivo de texto") + '</h3><font size="2px">Columnas (Fecha[yyyy-MM-dd], Ingreso, factura(sólo números)</font><br><input type="file" class="form-control" id="documento" onchange="GuardarDocumento()" name="documento" accept="text/*" required />';
    } else {
        valor = '<h3>' + ValidarTraduccion("Seleccione el archivo de excel") + '</h3><font size="2px">Columnas (Fecha[yyyy-MM-dd], Ingreso, factura(sólo números)</font><br><input type="file" class="form-control" id="documento" onchange="GuardarDocumento()" name="documento" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" required />';
    }
    
    swal.queue([{
        html: valor,
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Importar'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                var datos = LlamarAjax("Caja","opcion=ImportarArchivo&cuenta=" + cuenta + "&anio=" + anio + "&mes=" + mes + "&tipoarchivo=" + tipoarchivo).split("|");
                if (datos[0] == "0") {
                    TablaMovimientos();
                    swal(datos[1]);
                    resolve();
                }
                else
                    reject(datos[1]);
            })
        }
    }]);
}

function TablaMovimientos() {
    var cuenta = $("#Cuenta").val() * 1;
    var anio = $("#Anio").val() * 1;
    var mes = $("#Mes").val() * 1;
    var resultado = "";
    $("#TBMovimiento").html("");

    if (cuenta == 0 || anio == 0 || mes == 0)
        return false;
    var datos = LlamarAjax("Caja","opcion=TablaMovimientos&tipo=0&cuenta=" + cuenta + "&anio=" + anio + "&mes=" + mes);
    if (datos != "[]") {
        var data = JSON.parse(datos);
        for (var x = 0; x < data.length; x++) {
            resultado += "<tr id='fila-" + x + "' " + (data[x].movimiento ? (data[x].operacion == "PAGOS POR CONFIRMAR" ? "class='bg-amarillo'" : "class='bg-celeste' ") : "") + " ondblclick='EditarConcepto(" + data[x].id + ")'>" +
                "<td class='text-center'><b>" + (x + 1) + "</b>" + (data[x].movimiento ? "" : "<br><input class='form-control' type='checkbox' value='" + data[x].id + "' name='seleccion[]' id='seleccionado" + x + "' onchange='SeleccionarFila(" + x + ")'>") + "</td>" +
                "<td>" + (data[x].movimiento ? data[x].movimiento : "") + "</td>" +
                "<td>" + (data[x].operacion ? data[x].operacion : "") + "</td>" +
                "<td>" + data[x].fecha + "</td>" +
                "<td>" + data[x].referencia + "</td>" +
                "<td>" + data[x].descripcion + "</td>" +
                "<td>" + data[x].oficina + "</td>" +
                "<td align='right' class='text-XX2'>" + formato_numero(data[x].entrada, 2, ".", ",", "",1) + "</td>" +
                "<td align='right' class='text-XX2'>" + formato_numero(data[x].salida, 2, ".", ",", "",1) + "</td>" +
                "<td>" + data[x].adicional + "</td>" +
                "<td align='center'>" + (data[x].movimiento ? "<button class='eliminar btn btn-danger' title='Eliminar Ingreso' type='button' onclick=\"EliminarMovimiento(" + data[x].id + ",'" + data[x].descripcion + "'," + data[x].monto + ")\"><span data-icon='&#xe0d8;'></span></button>" : "&nbsp;") + "</td></tr>"; 
        }
    }

    $("#TBMovimiento").html(resultado);
        
}

function Consolidacion() {


    var banco = $("#CBanco").val() * 1;
    var cuenta = $("#CCuenta").val() * 1;
    

    var fechad = $("#CRFechaDesde").val();
    var fechah = $("#CRFechaHasta").val();
    var graficar = $("#CGraficar ").val()*1;
    var tipo = $("#TipoGrafica").val();

        
    if (fechad == "" && fechah == "") {
        swal("", ValidarTraduccion("debe de ingresar un rango de fechas"), "warning");
        return false;
    }

    ActivarLoad();
    setTimeout(function () {
        var parametros = "banco=" + banco + "&cuenta=" + cuenta + "&fechad=" + fechad + "&fechah=" + fechah;
        var datos = LlamarAjax("Caja","opcion=TablaMovConsolidado&"+ parametros);
        DesactivarLoad();

        var datajson = JSON.parse(datos);
        table = $('#TablaConsolidado').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "fila" },
                { "data": "cuenta" },
                { "data": "movimiento" },
                { "data": "operacion" },
                { "data": "fecha" },
                { "data": "referencia" },
                { "data": "descripcion" },
                { "data": "oficina" },
                { "data": "entrada", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "salida", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') }
            ],

            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy','colvis'
            ]
        });
    

        console.log(table);

        var entrada = 0;
        var salida = 0;
        var etiqueta = ['Entrada', 'Salida','Ganancia'];

        var tentrada = 0;
        var tsalida = 0;

        var config1 = {
            type: 'bar',
            data: {
                datasets: [],
                labels: []
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                },
                responsive: true,
                tooltips: {
                    enabled: true
                },
                legend: {
                    position: 'top',
                    display: (movil == 1 ? false : true)
                }
            }
        }

        var config2 = {
            type: 'bar',
            data: {
                datasets: [],
                labels: []
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                },
                responsive: true,
                tooltips: {
                    enabled: true
                },
                legend: {
                    position: 'top',
                    display: (movil == 1 ? false : true)
                }
            }
        }

        var config3 = {
            type: 'bar',
            data: {
                datasets: [],
                labels: []
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                },
                responsive: true,
                tooltips: {
                    enabled: true
                },
                legend: {
                    position: 'top',
                    display: (movil == 1 ? false : true)
                }
            }
        }

        var config4 = {
            type: 'bar',
            data: {
                datasets: [],
                labels: []
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                },
                responsive: true,
                tooltips: {
                    enabled: true
                },
                legend: {
                    position: 'top',
                    display: (movil == 1 ? false : true)
                }
            }
        }

        table.rows().data().each(function (datos, index) {
            entrada += datos.entrada * 1;
            salida += datos.salida * 1;
        });

        for (var x = 1; x <= 4; x++)
            $("#DivGrafica" + x).addClass("hidden");

        
        $("#TEntrada").val(formato_numero(entrada, 0,_CD,_CM,""));
        $("#TSalida").val(formato_numero(salida, 0,_CD,_CM,""));
        $("#TTotal").val(formato_numero(entrada + salida, 0,_CD,_CM,""));

        if (graficar == 1) {
            var parametros = "banco=" + banco + "&cuenta=" + cuenta + "&fechad=" + fechad + "&fechah=" + fechah + "&tipo=" + tipo;
            var datos = LlamarAjax("Caja","opcion=TablaMovimientosResumen&"+ parametros);
            var nrocuenta = 0;
            var NroGrafica = 0;
            var tabla = "";
            if (datos != "[]") {
                var data = JSON.parse(datos);
                for (var x = 0; x < data.length; x++) {
                    if (nrocuenta != data[x].id * 1) {
                        if (x != 0) {

                            tabla += "<tr>" +
                                "<td align='right' class='text-info'>TOTALES</td>" +
                                "<td align='right' class='text-info'>" + formato_numero(tentrada, 0,_CD,_CM,"") + "</td>" +
                                "<td align='right' class='text-info'><b>" + formato_numero(tsalida, 0,_CD,_CM,"") + "</td>" +
                                "<td align='right' class='text-info'><b>" + formato_numero(tentrada - tsalida, 0,_CD,_CM,"") + "</td></tr>";
                            $("#Tabla" + NroGrafica).html(tabla);
                            switch (NroGrafica) {
                                case 1:
                                    config1.data.datasets.push(newDataset3);
                                    config1.data.datasets.push(newDataset1);
                                    config1.data.datasets.push(newDataset2);

                                    if (mygrafica1 != null) {
                                        mygrafica1.destroy();
                                    }

                                    mygrafica1 = new Chart(grafica1, config1);
                                    mygrafica1.update();
                                    break;
                                case 2:
                                    config2.data.datasets.push(newDataset3);
                                    config2.data.datasets.push(newDataset1);
                                    config2.data.datasets.push(newDataset2);

                                    if (mygrafica2 != null) {
                                        mygrafica2.destroy();
                                    }

                                    mygrafica2 = new Chart(grafica2, config2);
                                    mygrafica2.update();
                                    break;
                                case 3:
                                    config3.data.datasets.push(newDataset3);
                                    config3.data.datasets.push(newDataset1);
                                    config3.data.datasets.push(newDataset2);

                                    if (mygrafica3 != null) {
                                        mygrafica3.destroy();
                                    }

                                    mygrafica3 = new Chart(grafica3, config3);
                                    mygrafica3.update();
                                    break;
                                case 4:
                                    config4.data.datasets.push(newDataset3);
                                    config4.data.datasets.push(newDataset1);
                                    config4.data.datasets.push(newDataset2);

                                    if (mygrafica4 != null) {
                                        mygrafica4.destroy();
                                    }

                                    mygrafica4 = new Chart(grafica4, config4);
                                    mygrafica4.update();
                                    break;
                            }
                        }
                        NroGrafica++;
                        nrocuenta = data[x].id * 1;
                        $("#DivGrafica" + NroGrafica).removeClass("hidden");
                        $("#SpanGrafica" + NroGrafica).html("Grafica Consolidada de Movimientos del banco " + data[x].cuenta);
                        var newDataset1 = {
                            label: etiqueta[0],
                            backgroundColor: colores[0],
                            data: [],
                        };

                        var newDataset2 = {
                            label: etiqueta[1],
                            backgroundColor: colores[4],
                            data: [],
                        };

                        var newDataset3 = {
                            type: 'line',
                            label: etiqueta[2],
                            fill: false,
                            backgroundColor: colores[6],
                            borderColor: colores[6],
                            data: [],
                        };
                                                
                        tabla = "";
                        tentrada = 0;
                        tsalida = 0;
                    }
                    resultado = data[x].entrada - Math.abs(data[x].salida);
                    tentrada += data[x].entrada;
                    tsalida += Math.abs(data[x].salida);
                    tabla += "<tr>" +
                        "<td>" + data[x].fecha + "</td>" +
                        "<td align='right'>" + formato_numero(data[x].entrada, 0,_CD,_CM,"") + "</td>" +
                        "<td align='right'>" + formato_numero(Math.abs(data[x].salida), 0,_CD,_CM,"") + "</td>" +
                        "<td align='right'>" + formato_numero(resultado, 0,_CD,_CM,"") + "</td></tr>";

                    switch (NroGrafica) {
                        case 1:
                            config1.data.labels.push(data[x].fecha);
                            break;
                        case 2:
                            config2.data.labels.push(data[x].fecha);
                            break;
                        case 3:
                            config3.data.labels.push(data[x].fecha);
                            break;
                        case 4:
                            config4.data.labels.push(data[x].fecha);
                            break;
                    }

                    newDataset1.data.push(data[x].entrada);
                    newDataset2.data.push(Math.abs(data[x].salida));
                    newDataset3.data.push(data[x].entrada- Math.abs(data[x].salida));

                }

                tabla += "<tr>" +
                    "<td align='right' class='text-info'>TOTALES</td>" +
                    "<td align='right' class='text-info'>" + formato_numero(tentrada, 0,_CD,_CM,"") + "</td>" +
                    "<td align='right' class='text-info'><b>" + formato_numero(tsalida, 0,_CD,_CM,"") + "</td>" +
                    "<td align='right' class='text-info'><b>" + formato_numero(tentrada - tsalida, 0,_CD,_CM,"") + "</td></tr>";
                $("#Tabla" + NroGrafica).html(tabla);
                switch (NroGrafica) {
                    case 1:
                        config1.data.datasets.push(newDataset3);
                        config1.data.datasets.push(newDataset1);
                        config1.data.datasets.push(newDataset2);

                        if (mygrafica1 != null) {
                            mygrafica1.destroy();
                        }

                        mygrafica1 = new Chart(grafica1, config1);
                        mygrafica1.update();
                        break;
                    case 2:
                        config2.data.datasets.push(newDataset3);
                        config2.data.datasets.push(newDataset1);
                        config2.data.datasets.push(newDataset2);

                        if (mygrafica2 != null) {
                            mygrafica2.destroy();
                        }

                        mygrafica2 = new Chart(grafica2, config2);
                        mygrafica2.update();
                        break;
                    case 3:
                        config3.data.datasets.push(newDataset3);
                        config3.data.datasets.push(newDataset1);
                        config3.data.datasets.push(newDataset2);

                        if (mygrafica3 != null) {
                            mygrafica3.destroy();
                        }

                        mygrafica3 = new Chart(grafica3, config3);
                        mygrafica3.update();
                        break;
                    case 4:
                        config4.data.datasets.push(newDataset3);
                        config4.data.datasets.push(newDataset1);
                        config4.data.datasets.push(newDataset2);

                        if (mygrafica4 != null) {
                            mygrafica4.destroy();
                        }

                        mygrafica4 = new Chart(grafica4, config4);
                        mygrafica4.update();
                        break;
                }

            }

        }

    }, 15);
}

function EditarConcepto(id) {
    var datos = LlamarAjax("Caja","opcion=DetalleMovimientos&id=" + id);
    var data = JSON.parse(datos);
    $("#MId").val(id);
    $("#MOperacion").val("").trigger("change");
    $("#MMovimiento").val("");
    $("#MFecha").val(data[0].fecha);
    $("#MReferencia").val(data[0].referencia);
    $("#MDescripcion").val(data[0].descripcion);
    $("#MEntrada").val(formato_numero(data[0].entrada, 0,_CD,_CM,""));
    $("#MSalida").val(formato_numero(data[0].salida,0,".",",",""));
    if (data[0].operacion)
        $("#MOperacion").val(data[0].operacion).trigger("change");
    if (data[0].movimiento)
        $("#MMovimiento").val(data[0].movimiento);

    $("#ModalMoviPago").modal("show");
    
}

function GuardarEdiMovimiento() {
    var id = $("#MId").val();
    var operacion = $("#MOperacion").val();
    var movimiento = $.trim($("#MMovimiento").val());
    if (operacion == "") {
        $("#MOperacion").focus();
        swal("Acción Cancelada", "Debe seleccionar la operación del movimiento", "warning");
        return false;
    }
    if (movimiento == "") {
        $("#MMovimiento").focus();
        swal("Acción Cancelada", "Debe ingresar la descripción del movimiento", "warning");
        return false;
    }
    var datos = LlamarAjax("Caja","opcion=EditarMovimientos&id=" + id + "&movimiento=" + movimiento + "&operacion=" + operacion).split("|");
    if (datos[0] == "0") {
        $("#ModalMoviPago").modal("hide");
        TablaMovimientos();
        swal("", datos[1], "success");
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
}

function EliminarMovimiento(id, descripcion, monto) {
    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: ValidarTraduccion('¿Seguro que desea eliminar el movimiento ' + descripcion + ' por un monto de ' + formato_numero(monto,0,".",",","") + '?'),
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Si'),
        cancelButtonText: ValidarTraduccion('No'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                $.post(url_servidor+"Caja","opcion=EditarMovimientos&id=" + id + "&movimiento=&operacion=")
                    .done(function (data) {
                        datos = data.split("|");
                        if (datos[0] == "0") {
                            TablaMovimientos();
                            resolve()
                        } else {
                            reject(datos[1]);
                        }
                    })
            })
        }
    }]);
}

function ExportarExcel() {
    //Creamos un Elemento Temporal en forma de enlace
    var tmpElemento = document.createElement('a');
    // obtenemos la información desde el div que lo contiene en el html
    // Obtenemos la información de la tabla
    var data_type = 'data:application/vnd.ms-excel';
    var tabla_div = document.getElementById('TablaMovimiento');
    var tabla_html = tabla_div.outerHTML.replace(/ /g, '%20');
    tmpElemento.href = data_type + ', ' + tabla_html;
    //Asignamos el nombre a nuestro EXCEL
    tmpElemento.download = 'Movimientos.xls';
    // Simulamos el click al elemento creado para descargarlo
    tmpElemento.click();
}

$('select').select2();
DesactivarLoad();
