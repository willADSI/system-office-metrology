﻿var Factura = 0;
var Estado = 'Pendiente';
var IdCliente = 0;
var IdNota = 0;
var Nota = 0;
var totales = 0;
var subtotal = 0;
var descuento = 0;
var exento = 0;
var gravable = 0;
var iva = 0;
var retefuente = 0;
var reteiva = 0;
var reteica = 0;
var totalfila = 0;
var SaldoFac = 0;
var CliReteFuente = 0;
var CliReteIva = 0;
var CliReteIca = 0;

d = new Date();
month = d.getMonth() + 1;
day = d.getDate();

var Anio = $.trim(d.getFullYear());
Anio = $.trim(Anio.substr(2, 2));

output = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-01';

output2 = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-' +
    (day < 10 ? '0' : '') + day;

$("#BCliente, #CCliente").html(CargarCombo(9, 1));
$("#CAsesor").html(CargarCombo(13, 1));

$("#BFechaDesde").val(output);
$("#BFechaHasta").val(output2);
$("#CFechaDesde").val(output);
$("#CFechaHasta").val(output2);

var output = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-01';

var output2 = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-' +
    (day < 10 ? '0' : '') + day;

function CalcularPreTabla(caja, x) {
    ValidarTexto(caja, 1);
    var precio = NumeroDecimal($("#tprecio" + x).val()) * 1;
    var cantidad = $("#tcantidad" + x).val() * 1;
    var descuento = $("#tdescuento" + x).val() * 1;

    var subtotal = (precio * cantidad) - (precio * cantidad * descuento / 100);

    $("#tsubtotal" + x).val(formato_numero(subtotal, 0,_CD,_CM,""));

    CalcularTotal();

}

function SeleccionarTodoNC() {

    if ($("#seleccionatodonc").prop("checked")) {
        for (var x = 0; x < totalfila; x++) {
            $("#fila-" + x).addClass("bg-gris");
            $("#seleccionado" + x).prop("checked", "checked");
        }

    } else {
        for (var x = 0; x < totalfila; x++) {
            $("#fila-" + x).removeClass("bg-gris");
            $("#seleccionado" + x).prop("checked", "");
        }
    }
    CalcularTotal();
}

function SeleccionarFila(x) {

    if ($("#seleccionado" + x).prop("checked")) {
        $("#fila-" + x).addClass("bg-gris");
    } else {
        $("#fila-" + x).removeClass("bg-gris");
    }

    CalcularTotal();

}


function CalcularTotal() {

    totales = 0;
    subtotal = 0;
    descuento = 0;
    gravable = 0;
    iva = 0;

    var ingresos = document.getElementsByName("seleccion[]");
    var precios = document.getElementsByName("Precios[]");
    var ivas = document.getElementsByName("IVA[]");
    var cantidades = document.getElementsByName("Cantidades[]");
    var descuentos = document.getElementsByName("Descuentos[]");

    for (var x = 0; x < ingresos.length; x++) {
        if (ingresos[x].checked) {
            subtotal += (NumeroDecimal(precios[x].value) * NumeroDecimal(cantidades[x].value));
            descuento += (NumeroDecimal(precios[x].value) * NumeroDecimal(cantidades[x].value) * descuentos[x].value / 100);
            gravable += (NumeroDecimal(precios[x].value) * NumeroDecimal(cantidades[x].value)) - (NumeroDecimal(precios[x].value) * NumeroDecimal(cantidades[x].value) * descuentos[x].value / 100);
            iva += ((NumeroDecimal(precios[x].value) * NumeroDecimal(cantidades[x].value)) - (NumeroDecimal(precios[x].value) * NumeroDecimal(cantidades[x].value) * descuentos[x].value / 100)) * ivas[x].value / 100;
        }
    }

    retfuente = Math.trunc((subtotal - descuento) * CliReteFuente / 100);
    retica = Math.trunc((subtotal - descuento) * CliReteIca / 100);
    retiva = Math.trunc(iva * CliReteIva / 100);

    totales = subtotal - descuento + iva - retfuente - retica - retiva;

    $("#tsubtotal").val(formato_numero(subtotal, _DE, _CD, _CM, ""));
    $("#tdescuento").val(formato_numero(descuento, _DE,_CD,_CM,""));
    $("#tbaseimponible").val(formato_numero(gravable, _DE,_CD,_CM,""));
    $("#texcento").val(formato_numero(exento, _DE,_CD,_CM,""));
    $("#tiva").val(formato_numero(iva, _DE,_CD,_CM,""));
    $("#ttotalpagar").val(formato_numero(totales, _DE,_CD,_CM,""));
    $("#tretefuente").val(formato_numero(retfuente, _DE,_CD,_CM,""));
    $("#treteiva").val(formato_numero(retiva, _DE,_CD,_CM,""));
    $("#treteica").val(formato_numero(retica, _DE,_CD,_CM,""));
    $("#ttotalpagar").val(formato_numero(totales, _DE,_CD,_CM,""));

}

function LimpiarTodo() {
    LimpiarDatos();
}


function LimpiarDatos() {
    $("#seleccionatodonc").prop("checked", "");
    Factura = 0;
    Nota = 0;
    IdNomina = 0;
    PlazoPago = 0;
    TipoPrecio = 0;
    totales = 0;
    subtotal = 0;
    descuento = 0;
    exento = 0;
    gravable = 0;
    iva = 0;
    retefuente = 0;
    reteiva = 0;
    reteica = 0;
    IdCliente = 0;
    SaldoFac = 0;
    
    TotalSeleccion = 0;
    
    $("#Cliente").val("");
    $("#NombreCliente").val("");
    $("#Direccion").val("");
    $("#SaldoClienteDev").html("");
    $("#divAnulaciones").html("");

    $("#Factura").val("");
    $("#Nota").val("");

    $("#Ciudad").val("");

    $("#Totales").val("0");
    $("#Factura").val("");

    $("#bodyNotaCre").html("");

    $("#Email").val("");
    $("#Telefonos").val("");
    $("#TipoCliente").val("");

    $("#tsubtotal").val("0");
    $("#tdescuento").val("0");
    $("#tbaseimponible").val("0");
    $("#texcento").val("0");
    $("#tiva").val("0");
    $("#retefuente").val("0");
    $("#reteiva").val("0");
    $("#reteica").val("0");
    $("#ttotalpagar").val("0");
    Estado = "Temporal";
    $("#Estado").val(Estado);

    $("#TipoCliente").val("");
    $("#PlazoPago").val("0");
    $("#TablaPrecio").val("");
    $("#FechaNot").val(output2);
    $("#Saldo").val("0");
    $("#FechaReg").val("");
    $("#UsuarioReg").val("");
    $("#Observaciones").val("");
    $("#Factura").prop("disabled", false);
    $("#Nota").prop("disabled", false);
}

function BuscarFacturaNotaCre(factura) {
    if (Factura == factura)
        return false;
    
    LimpiarDatos();
    
    ActivarLoad();
    setTimeout(function () {
        var datos = LlamarAjax("Caja","opcion=FacturasNotaCredito&Factura=" + factura).split("|");
        DesactivarLoad();
        if (datos[0] == "0") {
            if (datos[1] != "[]") {
                Factura = factura;
                $("#Factura").val(factura);
                var data = JSON.parse(datos[1]);
                SaldoFac = data[0].saldo * 1;
                if (data[0].saldo * 1 <= 0) {
                    LimpiarTodo();
                    $("#Factura").focus();
                    swal("Acción Cancelada", "El saldo de la factura es menor o igual a cero", "warning");
                    Factura = 0;
                    return false;
                }
                var resultado = "";
                totalfila = 0;
                for (var x = 0; x < data.length; x++) {
                    totalfila++;
                    resultado += "<tr id='fila-" + x + "'>" +
                        "<td class='text-center'><b>" + (x + 1) + "</b><br><input class='form-control' type='checkbox' name='seleccion[]' id='seleccionado" + x + "' onchange='SeleccionarFila(" + x + ")'></td>" +
                        "<td><div name='Descripcion[]'>" + data[x].descripcion + "</div></td>" +
                        "<td class='text-right text-XX'><input type='text' class='text-center sinborde' onfocus='FormatoEntrada(this, 1)' value='" + data[x].cantidad + "' onblur='FormatoSalida(this,0)' id='tcantidad" + x + "' onkeyup='CalcularPreTabla(this," + x + ")' name='Cantidades[]' style='width:99%'</td>" +
                        "<td class='text-right text-XX'><input type='text' class='text-right sinborde' onfocus='FormatoEntrada(this, 1)' onblur='FormatoSalida(this,0)' onkeyup='CalcularPreTabla(this," + x + ")' value='" + formato_numero(data[x].precio, 0,_CD,_CM,"") + "' id='tprecio" + x + "' name='Precios[]' style='width:99%'></td>" +
                        "<td class='text-right text-XX'><input type='text' class='text-right sinborde' onfocus='FormatoEntrada(this, 1)' onblur='FormatoSalida(this,0)' onkeyup='CalcularPreTabla(this," + x + ")' value='" + data[x].descuento + "' id='tdescuento" + x + "' name='Descuentos[]' style='width:99%'></td>" +
                        "<td class='text-right text-XX'><input type='text' class='text-right sinborde' disabled value='" + formato_numero(data[x].iva, 0,_CD,_CM,"") + "' id='tiva" + x + "' name='IVA[]' style='width:99%'></td>" +
                        "<td class='text-right text-XX'><input type='text' class='text-right sinborde' disabled value='" + formato_numero(data[x].subtotal, 0,_CD,_CM,"") + "' id='tsubtotal" + x + "' name='Subtotales[]' style='width:99%'></td></tr>";
                }
                var cliente = data[0].idcliente;
                SaldoActual(cliente, 1);
                PlazoPago = data[0].plazopago;
                TipoCliente = data[0].tipocliente;
                CorreoEmpresa = data[0].email;
                CliReteFuente = data[0].cliretefuente;
                CliReteIva = data[0].clireteiva;
                CliReteIca = data[0].clireteica;
                $("#SaldoClienteDev").html(SaldoTotal(cliente));
                $("#bodyNotaCre").html(resultado);
                $("#Factura").prop("disabled", true);
                $("#Cliente").val(data[0].documento);
                $("#NombreCliente").val(data[0].cliente);
                $("#Ciudad").val(data[0].ciudad);
                $("#Telefonos").val(data[0].telefono);
                $("#Direccion").val(data[0].direccion);
                $("#Email").val(data[0].email);
                $("#Fecha").val(output2);
                $("#Saldo").val(formato_numero(SaldoFac,0,".",",",""));

                $("#TipoCliente").val(TipoCliente);
                $("#PlazoPago").val(PlazoPago);
                $("#TablaPrecio").val(data[0].tbprecio);

            } else {
                $("#Factura").val("");
                $("#Factura").focus();
                swal("Acción Cancelada", "La factura número " + factura + ", no se encuentra registrada en el sistema", "warning");
                Factura = 0;
            }
        } else {
            swal("Acción Cancelada", datos[1], "warning");
        }
    }, 15);
}

function BuscarNota(nota) {
    if (Nota == nota)
        return false;

    LimpiarDatos();

    ActivarLoad();
    setTimeout(function () {
        var datos = LlamarAjax("Caja","opcion=BuscarNotaCredito&Nota=" + nota).split("|");
        DesactivarLoad();
        if (datos[0] == "0") {
            if (datos[1] != "[]") {
                Nota = nota;
                $("#Nota").val(Nota);
                $("#Nota").prop("disabled", true);
                $("#Factura").prop("disabled", true);
                var data = JSON.parse(datos[1]);
                SaldoFac = data[0].saldo * 1;
                var resultado = "";
                totalitems = 0;
                for (var x = 0; x < data.length; x++) {
                    resultado += "<tr id='fila-" + x + "'>" +
                        "<td class='text-center'><b>" + (x + 1) + "</td>" +
                        "<td><div name='Descripcion[]'>" + data[x].descripcion + "</div></td>" +
                        "<td class='text-center text-XX'>" + data[x].cantidad + "</td>" +
                        "<td class='text-right text-XX'>" + formato_numero(data[x].precio, 0,_CD,_CM,"") +"</td>" +
                        "<td class='text-right text-XX'>" + data[x].descuento + "</td>" +
                        "<td class='text-right text-XX'>" + formato_numero(data[x].iva, 0,_CD,_CM,"") + "</td>" +
                        "<td class='text-right text-XX'>" + formato_numero(data[x].subtotal, 0,_CD,_CM,"") + "</td></tr>";
                    
                    subtotal += (data[x].precio * data[x].cantidad);
                    descuento += (data[x].precio * data[x].cantidad) * data[x].descuento / 100;
                    gravable += (data[x].precio * data[x].cantidad) - (data[x].precio * data[x].cantidad * data[x].descuento / 100);
                    iva += ((data[x].precio * data[x].cantidad) - (data[x].precio * data[x].cantidad * data[x].descuento)) * data[x].iva / 100;

                    totales = subtotal - descuento + iva;

                    $("#tsubtotal").val(formato_numero(subtotal, 0,_CD,_CM,""));
                    $("#tdescuento").val(formato_numero(descuento, 0,_CD,_CM,""));
                    $("#tbaseimponible").val(formato_numero(gravable, 0,_CD,_CM,""));
                    $("#texcento").val(formato_numero(exento, 0,_CD,_CM,""));
                    $("#tiva").val(formato_numero(iva, 0,_CD,_CM,""));
                    $("#ttotalpagar").val(formato_numero(totales, 0,_CD,_CM,""));
                }
                IdCliente = data[0].idcliente;
                SaldoActual(IdCliente, 1);
                PlazoPago = data[0].plazopago;
                TipoCliente = data[0].tipocliente;
                CorreoEmpresa = data[0].email;
                Estado = data[0].estado;
                $("#Estado").val(Estado);
                $("#SaldoClienteDev").html(SaldoTotal(IdCliente));
                $("#bodyNotaCre").html(resultado);
                $("#Factura").val(data[0].factura);
                $("#Cliente").val(data[0].documento);
                $("#NombreCliente").val(data[0].cliente);
                $("#Ciudad").val(data[0].ciudad);
                $("#Telefonos").val(data[0].telefono);
                $("#Direccion").val(data[0].direccion);
                $("#Email").val(data[0].email);
                $("#Fecha").val(output2);
                $("#Saldo").val(formato_numero(SaldoFac, 0,_CD,_CM,""));
                $("#divAnulaciones").html(data[0].anulado);
                $("#FechaReg").val(data[0].fechareg);
                $("#UsuarioReg").val(data[0].usuario);
                $("#Observaciones").val(data[0].observacion);
                $("#FechaNot").val(data[0].fecha);

                $("#TipoCliente").val(TipoCliente);
                $("#PlazoPago").val(PlazoPago);
                $("#TablaPrecio").val(data[0].tbprecio);

                


            } else {
                $("#Nota").val("");
                $("#Nota").focus();
                swal("Acción Cancelada", "La nota de crédito número " + nota + ", no se encuentra registrada en el sistema", "warning");
                Nota = 0;
            }
        } else {
            swal("Acción Cancelada", datos[1], "warning");
        }
    }, 15);
}

function ImprimirNotaCre(numero) {
    if (numero * 1 == 0)
        numero = Nota;
    if (numero * 1 != 0) {
        ActivarLoad();
        setTimeout(function () {
            var parametros = "Nota=" + numero;
            var datos = LlamarAjax("Caja","opcion=RpNotaCredito&"+ parametros);
            DesactivarLoad();
            datos = datos.split("|");
            if (datos[0] == "0") {
                window.open("DocumPDF/" + datos[1]);
            } else {
                swal("", ValidarTraduccion(datos[1]), "warning");
            }
        }, 15);
    }
}

function GuardarNotaCre() {
    if (Factura == 0)
        return false;

    if (Nota > 0) {
        swal("Acción Cancelada", "Para editar esta nota de crédito debe anularla", "warning");
        return false;
    }

    var seleccionado = 0;
    var observaciones = $.trim($("#Observaciones").val());
    var fecha = $("#FechaNot").val();

    var ingresos = document.getElementsByName("seleccion[]");
    var descripcion = document.getElementsByName("Descripcion[]");
    var precios = document.getElementsByName("Precios[]");
    var cantidades = document.getElementsByName("Cantidades[]");
    var descuentos = document.getElementsByName("Descuentos[]");
    var subtotales = document.getElementsByName("Subtotales[]");
    var ivas = document.getElementsByName("IVA[]");

    var a_precio = "array[";
    var a_descuento = "array[";
    var a_iva = "array[";
    var a_cantidad = "array[";
    var a_subtotal = "array[";
    var a_descripcion = "array[";

    var mensaje = "";

    if (fecha == "") {
        $("#FechaNot").focus();
        swal("Acción Cancelada", "Debe de ingresar la fecha de la nota de crédito", "warning");
        return false;
    }


    for (var x = 0; x < ingresos.length; x++) {
        if (ingresos[x].checked) {
            if (NumeroDecimal(subtotales[x].value) == 0) {
                $("#tprecio" + x).focus();
                swal("Acción Cancelada", "Debe de Ingresar la cantidad y el valor unitario del item número " + (x + 1), "warning");
                return false;
            }
            if (a_descripcion == "array[") {
                a_descripcion += "'" + $.trim(descripcion[x].innerHTML) + "'";
                a_precio += NumeroDecimal(precios[x].value);
                a_descuento += $.trim(descuentos[x].value);
                a_iva += $.trim(ivas[x].value);
                a_cantidad += NumeroDecimal(cantidades[x].value);
                a_subtotal += NumeroDecimal(subtotales[x].value);

            } else {
                a_descripcion += ",'" + $.trim(descripcion[x].innerHTML) + "'";
                a_precio += "," + NumeroDecimal(precios[x].value);
                a_descuento += "," + $.trim(descuentos[x].value);
                a_iva += "," + $.trim(ivas[x].value);
                a_cantidad += "," + NumeroDecimal(cantidades[x].value);
                a_subtotal += "," + NumeroDecimal(subtotales[x].value);
            }
            seleccionado += 1;
        }
    }
    a_precio += "]";
    a_cantidad += "]";
    a_descuento += "]";
    a_iva += "]";
    a_subtotal += "]";
    a_descripcion += "]";

    if (seleccionado == 0) {
        swal("Acción Cancelada", "Debe de seleccionar por lo mínimo un registro", "warning");
        return false;
    }

    totales = Math.trunc(totales);
    SaldoFac = Math.trunc(SaldoFac);
        
    if (totales > (SaldoFac+5)) {
        swal("Acción Cancelada", "El total de la nota de crédito supera el saldo actual de la factura", "warning");
        return false;
    }

    mensaje = "<br><b>¿DESEA GUARDAR LA NOTA DE CRÉDITO?</b>";

    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: mensaje,
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Guardar'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                var parametros = "factura=" + Factura + "&observacion=" + observaciones +
                    "&subtotal=" + subtotal + "&descuento=" + descuento + "&gravable=" + gravable + "&iva=" + iva + "&total=" + totales + "&exento=" + exento +
                    "&reteiva=" + reteiva + "&retefuente=" + retefuente + "&reteica=" + reteica + 
                    "&a_precio=" + a_precio + "&a_descuento=" + a_descuento + "&a_subtotal=" + a_subtotal +
                    "&a_cantidad=" + a_cantidad + "&a_descripcion=" + a_descripcion + "&a_iva=" + a_iva + "&fechanot=" + fecha;
                $.post(url_servidor+"Caja","opcion=GuardarNotaCredito&"+ parametros)
                    .done(function (data) {
                        datos = data.split("|");
                        if (datos[0] == 0) {
                            swal("", datos[1] + " " + datos[2], "success");
                            BuscarNota(datos[2]);
                            resolve()
                        } else {
                            reject(datos[1]);
                        }
                    })
            })
        }
    }]);

    $(".swal2-content").html(mensaje);

}

function AnularNotaCre() {

    if (Nota == 0)
        return false;

    if (Estado == "Anulado") {
        swal("Acción Cancelada", "Esta nota de crédito ya fue anulada", "warning");
        return false;
    }

    var resultado = "";
    swal({
        title: 'Advertencia',
        text: '¿' + ValidarTraduccion('Desea Anular la Nota de Crédito') + ' ' + Nota + ' ' + ValidarTraduccion('del cliente') + ' ' + $("#NombreCliente").val(),
        input: 'text',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Anular'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',

        inputValidator: function (value) {
            return new Promise(function (resolve, reject) {
                if (value) {
                    var datos = LlamarAjax("Caja","opcion=AnularNotaCredito&Nota=" + Nota + "&Observaciones=" + value)
                    datos = datos.split("|");
                    if (datos[0] == "0") {
                        resultado = Salida;
                        resolve()
                    } else {
                        reject(datos[1]);
                    }

                } else {
                    reject(ValidarTraduccion('Debe de ingresar una observación'));
                }
            })
        }
    }).then(function (result) {
        LimpiarTodo();
        swal({
            type: 'success',
            html: 'Nota de crédito número ' + resultado + ' anulada con éxito'
        })
    })

}

function ConsularNotasCre() {

    var nota = $("#CNota").val() * 1;
    var factura = $("#CFactura").val() * 1;
    var cliente = $("#CCliente").val() * 1;
    var usuario = $("#CAsesor").val() * 1;
    var estado = $("#CEstado").val();
    
    var fechad = $("#CFechaDesde").val();
    var fechah = $("#CFechaHasta").val();

    if (fechad == "" || fechah == "") {
        swal("Acción Cancelada", ValidarTraduccion("debe de ingresar fecha de inicio y fecha final"), "warning");
        return false;
    }

    ActivarLoad();
    setTimeout(function () {
        var parametros = "nota=" + nota + "&factura=" + factura + "&cliente=" + cliente + "&fechad=" + fechad + "&fechah=" + fechah +
            "&usuario=" + usuario + "&estado=" + estado;
        var datos = LlamarAjax("Caja","opcion=ConsultarNotaCre&"+ parametros);
        DesactivarLoad();

        var datajson = JSON.parse(datos);
        $('#TablaNotasCredito').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "imprimir" },
                { "data": "nota", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "factura", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "fecha" },
                { "data": "fechareg" },
                { "data": "cliente" },
                { "data": "telefono" },
                { "data": "ciudad" },
                { "data": "email" },
                { "data": "subtotal", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "descuento", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "iva", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "reteiva", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "reteica", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "retefuente", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "total", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "estado" },
                { "data": "usuario" },
                { "data": "anulado" },
                { "data": "enviado" }
            ],

            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy','colvis'
            ]
        });
    }, 15);
}

$("#TablaNotasCredito > tbody").on("dblclick", "tr", function (e) {
    var row = $(this).parents("td").context.cells;
    var numero = row[1].innerText.replace(",", "");
    $('#tabnotascre a[href="#tabcrear"]').tab('show')
    $("#Nota").val(numero);
    BuscarNota(numero);
});

function ModalFacturas() {
    $("#modalConsultarFacturas").modal("show");
}

function CargarModalFacturas() {

    var factura = $("#BFactura").val() * 1;
    var cliente = $("#BCliente").val() * 1;
    
    var fechad = $("#BFechaDesde").val();
    var fechah = $("#BFechaHasta").val();

    if (fechad == "" || fechah == "") {
        swal("Acción Cancelada", ValidarTraduccion("debe de ingresar fecha de inicio y fecha final"), "warning");
        return false;
    }

    ActivarLoad();
    setTimeout(function () {
        var parametros = "Factura=" + factura + "&Cliente=" + cliente + "&FechaDesde=" + fechad + "&FechaHasta=" + fechah;
        var datos = LlamarAjax("Caja","opcion=ModalFacturas&"+ parametros);
        DesactivarLoad();

        var datajson = JSON.parse(datos);
        $('#TablaModalFacturas').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "factura", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "cliente" },
                { "data": "fecha" },
                { "data": "estado" },
                { "data": "total", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "abonado", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "saldo", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "envio" },
                { "data": "anula" }
            ],

            "language": {
                "url": LenguajeDataTable
            },
            "lengthMenu": [[5], [5]],
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy','colvis'
            ]
        });
    }, 15);
}

$("#TablaModalFacturas > tbody").on("dblclick", "tr", function (e) {
    var row = $(this).parents("td").context.cells;
    var numero = row[0].innerText.replace(",", "");
    $("#Factura").val(numero);
    BuscarFacturaNotaCre(numero);
    $("#modalConsultarFacturas").modal("hide");
});

function VerEstadoCuenta() {
    var cliente = $("#NombreCliente").val();
    EstadoCuenta(IdCliente, cliente, CorreoEmpresa, TipoCliente, PlazoPago, CorreoEmpresa);
}

$('select').select2();
DesactivarLoad();
