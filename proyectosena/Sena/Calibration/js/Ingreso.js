﻿var DescuentoCli = 0;
var IdCliente = 0;
var IdRemision = 0;
var Remision = 0;
var PDFRemision = 0;
var Cotizacion = 0;
var IdIngreso = 0;
var DocumentoCli = "";
var TipoCliente = 0;
var TipoPrecio = 0;
var OpcionEquipo = 0;
var Estado = "Temporal"
var IntervaloAnt = 0;
var totales = 0;
var Pdf_Remision = "";
var CorreoEmpresa = "";
var IngresoSerie = "";

var AutoSerie = [];

var datos = LlamarAjax("Cotizacion","opcion=AutoCompletarSerie");
AutoSerie.splice(0, AutoSerie.length);

var data = JSON.parse(datos);
for (var x = 0; x < data.length; x++) {
    AutoSerie.push(data[x].serie);
}

$("#Serie").autocomplete({ source: AutoSerie, minLength: 2 }).focus(function () {
    $(this).data("uiAutocomplete").search($(this).val());
});

document.onkeydown = function (e) {
    tecla = (document.all) ? e.keyCode : e.which;
    switch (tecla) {
        case 123:
            TomarFoto();
            return false;
            break;
        case 121:
            EliminarFoto();
            return false;
            break;
    }
}

d = new Date();
month = d.getMonth() + 1;
day = d.getDate();

var Anio = $.trim(d.getFullYear());
Anio = $.trim(Anio.substr(2, 2))


output = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-01';

output2 = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-' +
    (day < 10 ? '0' : '') + day;

$("#CFechaDesde").val(output);
$("#CFechaHasta").val(output2);

$("#ICFechaDesde").val(output);
$("#ICFechaHasta").val(output2);

var datos = LlamarAjax("Configuracion","opcion=CargaComboInicial&tipo=2").split("||");

$("#Magnitud, #CMagnitud, #Magnitud, #ICMagnitud").html(datos[0]);
$("#CEquipo, #ICEquipo").html(datos[1]);
$("#Marca, #CMarca, #ICMarca").html(datos[2]);
$("#IMedida").html(datos[4]);
$("#CCliente, #CaCliente, #ICCliente").html(datos[5]);
$("#CModelo, #ICModelo").html(datos[6]);
$("#CIntervalo, #ICIntervalo").html(datos[7]);
$("#CPais").html(datos[10]);
$("#CDepartamento").html(datos[11]);
$("#CCiudad").html(datos[12]);
$("#CUsuario, #ICUsuario").html(datos[13]);
$("#SelAccesorio").html(datos[14]);
$("#SelObservacion").html(datos[15]);


$("#Magnitud").val("").trigger("change");

tb = $('input');
$(tb).keypress(enter2tab);

tb = $('select');
$(tb).keypress(enter2tab);

function enter2tab(e) {
    if (e.keyCode == 13) {
        id = $(this).attr("id");
        
        cb = parseInt($(this).attr('tabindex'));

        if (id == "Cliente") {
            if ($.trim($(this).val()) != "") {
                BuscarCliente($(this).val());
                return false;
            } 
        }

        if (id == "Remision") {
            if ($.trim($(this).val()) != "") {
                BuscarRemisiom($(this).val());
                return false;
            }
        }
                
        if ($(':input[tabindex=\'' + (cb + 1) + '\']') != null) {
            $(':input[tabindex=\'' + (cb + 1) + '\']').focus();
            $(':input[tabindex=\'' + (cb + 1) + '\']').select();
            e.preventDefault();

            return false;
        }

        if ($(':select[tabindex=\'' + (cb + 1) + '\']') != null) {
            console.log("1");
            $(':select[tabindex=\'' + (cb + 1) + '\']').focus();
            $(':select[tabindex=\'' + (cb + 1) + '\']').select();
            e.preventDefault();

            return false;
        }
    }
}



function FBuscarServicio() {
    if (IdCliente > 0) {
        Contadores(2);
        IdIngreso = 0;
        $("#spanagregar").html("Agregar Equipo");
        LimpiarIngreso();
        ModalArticulos();
    }
}

function ModalArticulos() {
    LimpiarIngreso();
    $("#Serie").val("");
    $("#ModalArticulos").modal({ backdrop: 'static', keyboard: false }, "show");
}


function CargarModelos(marca) {
    $("#Modelo").html(CargarCombo(5, 1, "", marca));
    CantidadEquipos();
}


function CargarIntervalos(magnitud) {
    var combomagnitud = CargarCombo(6, 0, "", magnitud);
    $("#Intervalo").html("<option value='0' idioma='VER ESPECIFICACIONES'>VER ESPECIFICACIONES</option>" + combomagnitud);
    $("#Intervalo2, #Intervalo3").html("<option value='' idioma='--Seleccione--'>--Seleccione--</option>" + combomagnitud);
    VerTipoPrecio(magnitud*1);
    if (OpcionEquipo == 0) {
        if (magnitud*1 > 0)
            $("#Equipo").html(CargarCombo(2, 1, "", magnitud));
        else
            $("#Equipo").html(CargarCombo(58, 1));
    }
        
    CantidadEquipos();
}

function MagnitudEquipo(equipo) {
    var datos = LlamarAjax("Cotizacion","opcion=MagnitudEquipo&id=" + equipo);
    if (datos != "[]") {
        var data = JSON.parse(datos);
        OpcionEquipo = 1;
        if (data[0].idmagnitud * 1 != $("#Magnitud").val()*1)
            $("#Magnitud").val(data[0].idmagnitud).trigger("change");
        OpcionEquipo = 0;
    }
    CantidadEquipos();
}

function CambioSedeContacto(cliente) {
    $("#CaContacto").html("");
    $("#CaDireccion").html("");
    if (cliente * 1 == 0)
        return false;
    var datos = LlamarAjax("Cotizacion","opcion=BuscarSedeCliente&cliente=" + cliente).split("|");
    $("#CaContacto").html(datos[0]);
    $("#CaDireccion").html(datos[1]);
}

function ModalCambioCliente() {

    if (Remision == 0)
        return false;

    $("#AcCliente").html($("#NombreCliente").val() + " (" + $("#Cliente").val() + ")");
    $("#AcDireccion").html($("#Sede option:selected").text());
    $("#AcContacto").html($("#Contacto option:selected").text());

    $("#CaCliente").val(IdCliente).trigger("change");
    $("#CaContacto").val($("#Contacto").val()).trigger("change");
    $("#CaDireccion").val($("#Sede").val()).trigger("change");

    $("#modalCambioEmpresa").modal("show");
}

function CambiarCliente() {
    var cliente = $("#CaCliente").val() * 1;
    var direccion = $("#CaDireccion").val() * 1;
    var contacto = $("#CaContacto").val() * 1;

    var mensaje = "";

    if (contacto == 0) {
        $("#CaContacto").focus();
        mensaje = "Debe seleccionar el contacto del cliente ";
    }

    if (direccion == 0) {
        $("#CaDireccion").focus();
        mensaje = "Debe seleccionar la dirección del cliente <br>" + mensaje;
    }

    if (cliente == 0) {
        $("#CaCliente").focus();
        mensaje = "Debe seleccionar un cliente <br> " + mensaje;
    }

    if (mensaje != "") {
        swal("Acción Cancelada", mensaje, "warning");
        return false;
    }

    var datos = LlamarAjax("Cotizacion","opcion=CambioSede&id=" + IdRemision + "&sede=" + direccion + "&contacto=" + contacto + "&cliente=" + cliente).split("|");
    if (datos[0] == "0") {
        swal("", datos[1], "success");
        $("#modalCambioEmpresa").modal("hide");
        var numero = Remision;
        LimpiarTodo();
        $("#Remision").val(numero);
        BuscarRemisiom(numero);
    } else
        swal("Acción Cancelada", datos[1], "warning");
}



function GuardarIntervalo() {
    var magnitud = $("#Magnitud").val() * 1;
    var medida = $("#IMedida option:selected").text();
    var desde = $.trim($("#IDesde").val());
    var hasta = $.trim($("#IHasta").val());
    var numero = $("#NumIntervalo").val() * 1;
    if (numero == 1)
        numero = "";

    var mensaje = "";
        
    if ($.trim($("#IHasta").val()) == "") {
        $("#IHasta").focus();
        mensaje = "<br> *Ingrese el rango hasta" + mensaje;
    }
    if ($.trim($("#IDesde").val()) == "") {
        $("#IDesde").focus();
        mensaje = "<br> *Ingrese el rango desde" + mensaje;
    }

    if (medida == "--Seleccione--") {
        $("#IMedida").focus();
        mensaje = "<br> *Seleccione una unidad de medida" + mensaje;
    }

    if (mensaje != "") {
        swal("Verifique los siguientes campos", mensaje, "warning");
        return false;
    }

    var parametros = "magnitud=" + magnitud + "&medida=" + medida + "&desde=" + desde + "&hasta=" + hasta;
    var datos = LlamarAjax("Cotizacion","opcion=GuardarIntervalo&" + parametros);
    if (datos * 1 > 0) {
        $("#Intervalo" + numero).html(CargarCombo(6, 1, "", magnitud));
        $("#Intervalo" + numero).val(datos).trigger("change");
        $("#modalIntervalo").modal("hide");
    } else {
        swal("Acción Cancelada", "Ya existe un rango de intervalo con estas descripciones", "warning");
    }
}


function DatosContacto(contacto) {
    $("#Email").val("");
    $("#Telefonos").val("");
    if (contacto * 1 == 0)
        return false;
    var datos = LlamarAjax("Cotizacion","opcion=DatosContacto&id=" + contacto);
    if (datos != "[]") {
        var data = JSON.parse(datos);
        $("#Email").val(data[0].email);
        $("#Telefonos").val(data[0].telefonos);
    }
}

function AgregarIntervalo(numero) {

    if ($("#Magnitud").prop("disabled") == true)
        return false; 
        
    var magnitud = $("#Magnitud").val() * 1;
    if (magnitud == 0) {
        $("#Magnitud").focus();
        swal("Acción Cancelada", "Debe de seleccionar una magnitud", "warning");
        return false;
    }

    $("#IDesde").val("");
    $("#IHasta").val("");
    $("#IMedida").val("").trigger("change");
    $("#NumIntervalo").val(numero);
    $("#modalIntervalo").modal("show");

}
    

function AgregarOpcion(tipo, mensaje, id) {

    var magnitud = $("#Magnitud").val() * 1;
    var marca = $("#Marca").val() * 1

    if ($("#Magnitud").prop("disabled") == true)
        return false; 
       
    switch (tipo) {

        case 1:
            if (magnitud == 0) {
                $("#Magnitud").focus();
                swal("Acción Cancelada", "Debe de seleccionar una magnitud", "warning");
                return false;
            }
            break;
        case 3:
            if (marca == 0) {
                $("#Marca").focus();
                swal("Acción Cancelada", "Debe de seleccionar una marca", "warning");
                return false;
            }
            break;
    }
            
    swal({
        title: 'Agregar Opción',
        text: mensaje,
        input: 'text',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Agregar'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',

        inputValidator: function (value) {
            return new Promise(function (resolve, reject) {
                if ($.trim(value)) {
                    var datos = LlamarAjax("Cotizacion","opcion=GuardarOpcion&tipo=" + tipo + "&magnitud=" + magnitud + "&marca=" + marca + "&descripcion=" + value + "&tipoconcepto=0");
                    datos = datos.split("|");
                    if (datos[0] != "XX") {
                        $("#" + id).html(datos[1]);
                        $("#" + id).val(datos[0]).trigger("change");
                        resolve();
                    } else {
                        reject(datos[1]);
                    }

                } else {
                    reject(ValidarTraduccion('Debe de ingresar la descripción de ' + mensaje));
                }
            })
        }
    }).then(function (result) {
        $("#" + id).focus();
    })

}

function Contadores(tipo) {
    var datos = LlamarAjax("Cotizacion","opcion=Contadores&tipo=2");
    $("#Ingreso").val(datos);
}

function LimpiarTodo() {
    LimpiarDatos();
    LimpiarIngreso();
    $("#Cliente").val("");
}

function LimpiarDatos() {

    IdIngreso = 0;
    TablaPrecio = 0;
    DescuentoCli = 0;
    PDFRemision = 0;
    IdCliente = 0;
    DocumentoCli = "";
    TipoCliente = 0;
    PlazoPago = 0;
    TipoPrecio = 0;
    OpcionEquipo = 0;
    IdRemision = 0;
    Remision = 0;
    Cotizacion = 0;
    Estado = "Temporal"
    Pdf_Remision = "";
    totales = 0;
    CorreoEmpresa = "";
       

    $("#NombreCliente").val("");
    $("#Cotizacion").val("");
    $("#Remision").val("");

    $("#Cotizacion").attr("disabled",false);
    $("#Remision").attr("disabled", false);
    $("#Recepcion").val("").trigger("change");
    $("#Estado").val(Estado);
    $("#Sede").html("");
    $("#Contacto").html("");
    $("#FechaReg").val("");
    $("#UsuarioReg").val("");
    $("#OArchivo").val("");

    $("#PlazoPago").val("0");
    $("#Totales").val("0");

    $("#bodyIngreso").html("");

    $("#Email").val("");
    $("#Telefonos").val("");
    $("#TipoCliente").val("");
    $("#Observaciones").val("");
    
}

function ValidarCliente(documento) {
    if ($.trim(DocumentoCli) != "") {
        if (DocumentoCli != documento) {
            DocumentoCli = "";
            LimpiarDatos();
        }
    }
}

function VerRemision() {
    if (PDFRemision == 0)
        return false;
    window.open(url_cliente + "RemisionCliente/" + Remision + ".pdf");
}

function BuscarRemisiom(numero) {
    if (Remision == numero)
        return false;
    LimpiarTodo();
    if ((numero * 1) == 0)
        return false;

    var parametros = "remision=" + numero;
    var datos = LlamarAjax("Cotizacion","opcion=BuscarRemision&" + parametros);

    IdRemision = 0;
    Remision = 0;
    
    if (datos != "[]") {

        
        var data = JSON.parse(datos);
        IdRemision = data[0].id;
        Estado = data[0].estado;
        PDFRemision = data[0].pdf * 1;
        $("#Cliente").val(data[0].documento);
        BuscarCliente(data[0].documento, 1);
        Remision = data[0].remision;
        $("#Remision").val(Remision);
        $("#Estado").val(Estado);
        $("#Sede").val(data[0].idsede).trigger("change");
        $("#Contacto").val(data[0].idcontacto).trigger("change");
        $("#Recepcion").val(data[0].recepcion).trigger("change");
        $("#FechaReg").val(data[0].fecha);
        $("#UsuarioReg").val(data[0].usuario);
        $("#Observaciones").val(data[0].observacion);
    } else {
        $("#Remision").focus();
        swal("Acción Cancelada", "Remisión número " + numero + " no registrada", "warning");
    }
}

function BuscarCliente(documento, tipo) {

    if ($.trim(documento) == "")
        return false;

    if (DocumentoCli == documento)
        return false;

    if (tipo == 0)
        LimpiarDatos();
    
    DocumentoCli = documento;
        
    var parametros = "documento=" + $.trim(documento);
    var datos = LlamarAjax("Cotizacion","opcion=BuscarCliente&" + parametros);
    datos = datos.split("|");
    if (datos[0] == "0") {
        var data = JSON.parse(datos[1]);
        IdCliente = data[0].id;
        TablaPrecio = data[0].tablaprecio;
        DescuentoCli = data[0].descuento;
        PlazoPago = data[0].plazopago;
        TipoCliente = data[0].tipocliente;
        CorreoEmpresa = data[0].email;
        $("#TipoCliente").val(TipoCliente);
        $("#Descuento").val(DescuentoCli);
        $("#NombreCliente").val(data[0].nombrecompleto);
        $("#Contacto").html(datos[2]);
        $("#Sede").html(datos[3]);
        $("#Sede").focus();

        TablaTemporalIngreso();
                
        DesactivarLoad();

    } else {
        DocumentoCli = "";
        $("#Cliente").val("");
        $("#Cliente").focus();
        swal("Acción Cancelada", "El documento " + documento + ", no se encuentra registrado como cliente", "warning");
    }
    
}

function LimpiarIngresoTodo() {
    LimpiarIngreso();
    $("#Serie").val("");
}

function LimpiarIngreso() {
    IngresoSerie = "";
    $("#Magnitud").val("").trigger("change");
    $("#Marca").val("").trigger("change");
    $("#Observacion").val("");
    $("#Cantidad").val("");
    $("#Accesorio").val("");
    $("#Sitio").val("NO").trigger("change");
    $("#Garantia").val("NO").trigger("change");
    $("#Convenio").val("0").trigger("change");
    $("#SelAccesorio").val("").trigger("change");
    $("#SelObservacion").val("").trigger("change");

    $("#Serie").prop("disabled", false);
    $("#Magnitud").prop("disabled", true);
    $("#Equipo").prop("disabled", true);
    $("#Modelo").prop("disabled", true);
    $("#Marca").prop("disabled", true);
    $("#Intervalo").prop("disabled", true);
    $("#Intervalo2").prop("disabled", true);
    $("#Intervalo3").prop("disabled", true);
    $("#SelAccesorio").prop("disabled", true);
    $("#SelObservacion").prop("disabled", true);
    $("#Observacion").prop("disabled", true);
    $("#Cantidad").prop("disabled", true);
    $("#Accesorio").prop("disabled", true);

    $("#Sitio").prop("disabled", true);
    $("#Garantia").prop("disabled", true);
    $("#Convenio").prop("disabled", true);

}

function ConsularIngresos() {
  
    var magnitud = $("#ICMagnitud").val() * 1;
    var equipo = $("#ICEquipo").val();
    var modelo = $("#ICModelo").val();
    var marca = $("#ICMarca").val() * 1;
    var intervalo = $("#ICIntervalo").val();
    var ingreso = $.trim($("#ICIngreso").val());
    var serie = $.trim($("#ICSerie").val());
    var sitio = $("#ICSitio").val();
    var garantia = $("#ICGarantia").val();
    var convenio = $("#ICConvenio").val();
    var recepcion = $("#ICRecepcion").val();

    var remision = $("#ICRemision").val() * 1;
    var cliente = $("#ICCliente").val() * 1;
    var usuario = $("#ICUsuario").val() * 1;

    var fechad = $("#ICFechaDesde").val();
    var fechah = $("#ICFechaHasta").val();

    if (fechad == "" && fechah == "") {
        swal("Acción Cancelada", ValidarTraduccion("debe de ingresar un rango de fecha"), "warning");
        return false;
    }

    ActivarLoad();
    setTimeout(function () {
        var parametros = "remision=" + remision + "&cliente=" + cliente + "&ingresos=" + ingreso + "&convenio=" + convenio +
            "&fechad=" + fechad + "&fechah=" + fechah + "&magnitud=" + magnitud + "&equipo=" + equipo + "&marca=" + marca + "&modelo=" + modelo +
            "&intervalo=" + intervalo + "&serie=" + serie + "&usuario=" + usuario + "&recepcion=" + recepcion + "&sitio=" + sitio + "&garantia=" + garantia;
        var datos = LlamarAjax("Cotizacion","opcion=ConsultarIngresoDet&" + parametros);
        DesactivarLoad();

        var datajson = JSON.parse(datos);
        $('#TablaIngresos').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "ingreso", "className": "text-XX" },
                { "data": "remision" },
                { "data": "recepcion" },
                { "data": "cliente" },
                { "data": "direccion" },
                { "data": "contacto" },
                { "data": "magnitud" },
                { "data": "equipo" },
                { "data": "marca" },
                { "data": "modelo" },
                { "data": "rango1" },
                { "data": "rango2" },
                { "data": "rango3" },
                { "data": "serie" },
                { "data": "garantia" },
                { "data": "sitio" },
                { "data": "convenio" },
                { "data": "accesorio" },
                { "data": "observacion" },
                { "data": "fecha" },
                { "data": "usuario" },
                { "data": "ncotizacion" },
                { "data": "cotizado", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "factura" },
                { "data": "facturado", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "ajuste", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "suministro", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') }
            ],

            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy','colvis'
            ],
            "colReorder": true
        });
    }, 15);
}

function ConsularRemisiones() {
    
    var magnitud = $("#CMagnitud").val() * 1;
    var equipo = $("#CEquipo").val();
    var modelo = $("#CModelo").val();
    var marca = $("#CMarca").val() * 1;
    var intervalo = $("#CIntervalo").val();
    var ingreso = $("#CIngreso").val() * 1;
    var serie = $.trim($("#CSerie").val());

    var recepcion = $("#CRecepcion").val();

    var remision = $("#CRemision").val()*1;
    var cotizacion = $("#CCotizacion").val() * 1;
    var cliente = $("#CCliente").val() * 1;
    var pais = $("#CPais").val()*1;
    var departamento = $("#CDepartamento").val()*1
    var ciudad = $("#CCiudad").val() * 1;
    var usuario = $("#CUsuario").val() * 1;
        
    var estado = $("#CEstado").val();
    var fechad = $("#CFechaDesde").val();
    var fechah = $("#CFechaHasta").val();
    
    if (fechad == "" && fechah == "") {
        swal("Acción Cancelada", ValidarTraduccion("debe de ingresar una fecha o números de días"), "warning");
        return false;
    }

    ActivarLoad();
    setTimeout(function () {
        var parametros = "remision=" + remision + "&cotizacion=0&cliente=" + cliente + "&ingreso=" + ingreso + "&estado=" + estado +
            "&fechad=" + fechad + "&fechah=" + fechah + "&magnitud=" + magnitud + "&equipo=" + equipo + "&marca=" + marca + "&modelo=" + modelo +
            "&intervalo=" + intervalo + "&serie=" + serie + "&pais=" + pais + "&departamento=" + departamento + "&ciudad=" + ciudad + "&usuario=" + usuario + "&recepcion=" + recepcion;
        var datos = LlamarAjax("Cotizacion","opcion=ConsultarRemision&" + parametros);
        DesactivarLoad();

        var datajson = JSON.parse(datos);
        $('#TablaRemisiones').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "imprimir" },
                { "data": "remision", "className": "text-XX" },
                { "data": "recepcion" },
                { "data": "cotizacion" },
                { "data": "estado" },
                { "data": "documento" },
                { "data": "cliente" },
                { "data": "sede" },
                { "data": "contacto" },
                { "data": "departamento" },
                { "data": "ciudad" },
                { "data": "telefonos" },
                { "data": "email" },
                { "data": "totales", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "fecha" },
                { "data": "usuario" }
            ],
                        
            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy','colvis'
            ],
            "colReorder": true
        });
    }, 15);
}

function CambioSede() {
    if (IdRemision != 0) {
        var mensaje = "";
        var sede = $("#Sede").val() * 1;
        var contacto = $("#Contacto").val() * 1;

        if (contacto == 0) {
            $("#Contacto").focus();
            mensaje = "<br> Debe de seleccionar un contacto del cliente"
        }
        if (sede == 0) {
            $("#Sede").focus();
            mensaje = "<br> Debe de seleccionar una sede del cliente " + mensaje;
        }
        
        if (mensaje != "") {
            swal("Acción Cancelada", mensaje, "warning");
            return false;
        }

        var parametros = "id=" + IdRemision + "&sede=" + sede + "&contacto=" + contacto;
        var datos = LlamarAjax("Cotizacion","opcion=CambioSede&" + parametros);
        datos = datos.split("|");

        if (datos[0] == "0") {
            swal("", "Cambio de sede y contacto actualizado", "success");
        } else {
            swal("Acción Cancelada", datos[1], "warning");
        }
    }
}


function CantidadEquipos() {
    var equipo = $("#Equipo").val() * 1;
    var modelo = $("#Modelo").val() * 1;
    var intervalo = $("#Intervalo").val() * 1;
    $("#TEquipos").val("0");

    if (equipo > 0 && (modelo > 0 || intervalo > 0)) {
        var parametros = "cliente=" + IdCliente + "&id=" + IdRemision + "&equipo=" + equipo + "&modelo=" + modelo + "&intervalo=" + intervalo;
        var datos = LlamarAjax("Cotizacion","opcion=EquiposAgregados&" + parametros);
        $("#TEquipos").val(datos);
    }
}



$("#AgregarIngreso").click(function () {

	var equipo = $("#Equipo").val() * 1;
	var modelo = $("#Modelo").val() * 1;
	var marca = $("#Marca").val() * 1;
	var intervalo = $("#Intervalo").val() * 1;
	var intervalo2 = $("#Intervalo2").val() * 1;
	var intervalo3 = $("#Intervalo3").val() * 1;
	var observacion = $.trim($("#Observacion").val()).replace("&", "%26");
	var ingreso = $("#Ingreso").val() * 1;
	var serie = $.trim($("#Serie").val()).replace("&", "%26");
	var accesorio = $.trim($("#Accesorio").val());
	var sitio = $.trim($("#Sitio").val());
	var convenio = $("#Convenio").val()*1;
	var garantia = $.trim($("#Garantia").val());
	var cantidad = 1;

	var mensaje = "";


	if (ingreso == 0) {
		mensaje = "<br> Debe de ingresar un número de ingreso";
		$("#Ingreso").focus();
	}
			
	if (observacion == "") {
		mensaje = "<br> Debe de ingresar la observación del equipo " + mensaje;
		$("#Observacion").focus();
	}

	if (serie == "") {
		mensaje = "<br> Debe de ingresar la serie del equipo " + mensaje;
		$("#Serie").focus();
	}
		   
	if (modelo == 0) {
		mensaje = "<br> Debe de seleccionar un modelo " + mensaje;
		$("#Modelo").focus();
	}
	if (marca == 0) {
		mensaje = "<br> Debe de seleccionar una marca " + mensaje;
		$("#Marca").focus();
	}
	if (equipo == 0) {
		mensaje = "<br> Debe de seleccionar un equipo " + mensaje;
		$("#Equipo").focus();
	}
   
	if (mensaje != "") {
		swal("Acción Cancelada", mensaje, "warning");
		return false;
	}

	var parametro = "cliente=" + IdCliente + "&id=" + IdIngreso + "&equipo=" + equipo + "&modelo=" + modelo + "&convenio=" + convenio +
		"&observacion=" + observacion + "&serie=" + serie + "&ingreso=" + ingreso + "&cantidad=" + cantidad + "&intervalo=" + intervalo + "&intervalo2=" + intervalo2 + "&intervalo3=" + intervalo3 +
		"&remision=" + IdRemision + "&cotizacion=" + Cotizacion + "&sitio=" + sitio + "&garantia=" + garantia + "&accesorio=" + accesorio;

	var datos = LlamarAjax("Cotizacion","opcion=GuardarTmpIngreso&" + parametro);
	datos = datos.split("|");
	if (datos[0] == "0") {
		$("#Serie").focus();
		Estado = "Temporal";
		$("#Estado").val(Estado);
		swal("", datos[1], "success");
		Contadores(2);
		CantidadEquipos();
		IdIngreso = 0;
		$("#Observacion").val("");
		$("#Accesorio").val("");
		$("#Serie").val("");
		sendMessage(2,'');
	} else
		swal("Acción Cancelada", datos[1], "warning");

});


function ValidarSerie(serie) {
    if (serie != IngresoSerie) {
        $("#Magnitud").prop("disabled", true);
        $("#Equipo").prop("disabled", true);
        $("#Modelo").prop("disabled", true);
        $("#Marca").prop("disabled", true);
        $("#Intervalo").prop("disabled", true);
        $("#Intervalo2").prop("disabled", true);
        $("#Intervalo3").prop("disabled", true);
        $("#SelAccesorio").prop("disabled", true);
        $("#SelObservacion").prop("disabled", true);
        $("#Observacion").prop("disabled", true);
        $("#Cantidad").prop("disabled", true);
        $("#Accesorio").prop("disabled", true);
        $("#Sitio").prop("disabled", true);
        $("#Garantia").prop("disabled", true);
        $("#Convenio").prop("disabled", true);
        if ($("#Equipo").val() * 1 > 0 || $("#Marca").val() * 1 > 0 || $("#Modelo").val() * 1 > 0 || $("#Intervalo").val() * 1 > 0) {
            LimpiarIngreso();
            IngresoSerie = "";
        }
    }
}

function CambiarSerie() {

    if ($("#Serie").prop("disabled") == false)
        return false;

    var ingreso = $("#Ingreso").val();
    var serie = $.trim($("#Serie").val());
            
    swal({
        title: 'Cambio de serie',
        input: 'text',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Cambiar'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',

        inputValidator: function (value) {
            return new Promise(function (resolve, reject) {
                if (value) {
                    if ($.trim(value) == "")
                        reject(ValidarTraduccion('Debe de ingresar una serie'));
                    else {
                        var datos = LlamarAjax("Cotizacion","opcion=CambioSerie&ingreso=" + ingreso + "&serie=" + $.trim(value) + "&cliente=" + IdCliente)
                        if (datos[0] == "0") {
                            $("#Serie").val($.trim(value));
                            resolve()
                        } else {
                            reject("Esta seria se encuentra activa en el laboratorio");
                        }
                    }
                } else {
                    reject(ValidarTraduccion('Debe de ingresar una serie'));
                }
            })
        }
    })

    $(".swal2-input").val(serie);

}

function BuscarSerie(serie) {

    serie = $.trim(serie);
    if (serie == IngresoSerie || serie == "")
        return false;

    IngresoSerie = serie;

    $("#Magnitud").prop("disabled", false);
    $("#Equipo").prop("disabled", false);
    $("#Modelo").prop("disabled", false);
    $("#Marca").prop("disabled", false);
    $("#Intervalo").prop("disabled", false);
    $("#Intervalo2").prop("disabled", false);
    $("#Intervalo3").prop("disabled", false);
    $("#SelAccesorio").prop("disabled", false);
    $("#SelObservacion").prop("disabled", false);
    $("#Observacion").prop("disabled", false);
    $("#Cantidad").prop("disabled", false);
    $("#Accesorio").prop("disabled", false);
    $("#Sitio").prop("disabled", false);
    $("#Garantia").prop("disabled", false);
    $("#Convenio").prop("disabled", false);

    ActivarLoad();
    setTimeout(function () {
        var datos = LlamarAjax("Cotizacion","opcion=BuscarIngreso&ingreso=0&serie=" + serie + "&cliente=" + IdCliente);
        if (datos != "[]") {
            var data = JSON.parse(datos);
            $("#Serie").prop("disabled", true);
            $("#Equipo").val(data[0].idequipo).trigger("change");
            $("#Marca").val(data[0].idmarca).trigger("change");
            $("#Modelo").val(data[0].idmodelo).trigger("change");
            $("#Intervalo").val(data[0].idintervalo).trigger("change");
            $("#Intervalo2").val(data[0].idintervalo2).trigger("change");
            $("#Intervalo3").val(data[0].idintervalo3).trigger("change");
            $("#Ingreso").val(data[0].ingreso);
            $("#Serie").focus();
            $("#spanagregar").html("Editar Equipo");
            $("#Accesorio").val(data[0].accesorio);
            $("#Sitio").val(data[0].sitio).trigger("change");
            $("#Garantia").val(data[0].garantia).trigger("change");
            $("#Convenio").val(data[0].convenio).trigger("change");
            $("#ModalArticulos").modal({ backdrop: 'static', keyboard: false }, "show");
        }
        DesactivarLoad();
    }, 15);
}

function SelecccionarFila(ingreso) {

    ActivarLoad();
    setTimeout(function () {
        var datos = LlamarAjax("Cotizacion","opcion=BuscarIngreso&ingreso=" + ingreso);
        eval("data=" + datos);

        
        IdIngreso = data[0].id * 1;
        $("#Equipo").val(data[0].idequipo).trigger("change");
        $("#Marca").val(data[0].idmarca).trigger("change");
        $("#Modelo").val(data[0].idmodelo).trigger("change");
        $("#Intervalo").val(data[0].idintervalo).trigger("change");
        $("#Intervalo2").val(data[0].idintervalo2).trigger("change");
        $("#Intervalo3").val(data[0].idintervalo3).trigger("change");
        $("#Serie").val(data[0].serie);
        $("#Ingreso").val(ingreso);
        $("#Observacion").val(data[0].observacion);
        $("#Serie").focus();
        $("#spanagregar").html("Editar Equipo");
        $("#Accesorio").val(data[0].accesorio);
        $("#Sitio").val(data[0].sitio).trigger("change");
        $("#Garantia").val(data[0].garantia).trigger("change");
        $("#Convenio").val(data[0].convenio).trigger("change");

        $("#Serie").prop("disabled", true);
        $("#Magnitud").prop("disabled", false);
        $("#Equipo").prop("disabled", false);
        $("#Modelo").prop("disabled", false);
        $("#Marca").prop("disabled", false);
        $("#Intervalo").prop("disabled", false);
        $("#Intervalo2").prop("disabled", false);
        $("#Intervalo3").prop("disabled", false);
        $("#SelAccesorio").prop("disabled", false);
        $("#SelObservacion").prop("disabled", false);
        $("#Observacion").prop("disabled", false);
        $("#Cantidad").prop("disabled", false);
        $("#Accesorio").prop("disabled", false);
        $("#Sitio").prop("disabled", false);
        $("#Garantia").prop("disabled", false);
        $("#Convenio").prop("disabled", false);

        $("#ModalArticulos").modal({ backdrop: 'static', keyboard: false }, "show");
        DesactivarLoad();
    }, 15);

}



function LlamarImprimir() {
    var numero = $("#Remision").val() * 1;
    ImprimirRemision(numero, Estado,0);
}

function VerTipoPrecio(magnitud) {
    var datos = LlamarAjax("Cotizacion","opcion=TipoPrecio&magnitud=" + magnitud + "&servicio=0");
    TipoPrecio = datos * 1;

    switch (TipoPrecio) {
        case 1:
            $("#Intervalo").attr("disabled", false);
            break;
        case 2:
            $("#Intervalo").attr("disabled", true);
            $("#Intervalo").html("<option value='0'>VER ESPECIFICACIONES</option>");
            break;
    }
}


function ImprimirRemision(remision, estado, tipo) {
    if (remision * 1 != 0) {
        if (estado == "Temporal") {
            swal("Acción Cancelada", "No se puede imprimir una remisión en estado temporal", "warning");
            return false;
        }
        ActivarLoad();
        setTimeout(function () {
            var datos = LlamarAjax("Configuracion", "opcion=GenerarPDF&tipo=Remision&documento=" + remision).split("||");
            DesactivarLoad();
            if (datos[0] == "0") {
                if (tipo == 0)
                    window.open(url_cliente + "DocumPDF/" + datos[1]);
                else {
                    $("#resultadopdfremision").attr("src", url_cliente + "Adjunto/DocumPDF/" + datos[1] + "?id=" + NumeroAleatorio());
                    Pdf_Remision = datos[1];
                }

            } else {
                swal("Acción Cancelada", ValidarTraduccion(datos[1]), "warning");
            }
            
        }, 15);
    }
}

function EliminarIngreso(id, equipo, ingreso, estado) {

    if (totales == 1 && estado != "Temporal") {
        swal("Acción Cancelada", "No se puede eliminar el único ingreso de la remisión", "warning");
        return false;
    }

    if (estado != "Temporal" && estado != "Ingresado") {
        swal("Acción Cancelada", "No se puede eliminar un ingreso con estado " + estado, "warning");
        return false;
    }

    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: ValidarTraduccion('¿Seguro que desea eliminar el ingreso número ' + ingreso + ' del equipo ' + equipo + '?'),
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Eliminar'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                $.post(url_servidor + "Cotizacion","opcion=EliminarIngreso&id=" + id + "&ingreso=" + ingreso + "&equipo=" + equipo + "&remision=" + IdRemision)
                    .done(function (data) {
                        datos = data.split("|");
                        if (datos[0] == "0") {
                            Estado = "Temporal";
                            $("#Estado").val(Estado);
                            setMessage(2);
                            resolve()
                        } else {
                            reject(datos[1]);
                        }
                    })
            })
        }
    }]);
};

$("#TablaRemisiones > tbody").on("dblclick", "tr", function () {
    var row = $(this).parents("td").context.cells;
    var numero = row[1].innerText;
    $('#tabingresos a[href="#tabcrear"]').tab('show')
    IdRemision = 0;
    Remision = 0;
    BuscarRemisiom(numero);
});

$("#TablaIngresos > tbody").on("dblclick", "tr", function () {
    var row = $(this).parents("td").context.cells;
    var numero = row[1].innerText;
    $('#tabingresos a[href="#tabcrear"]').tab('show')
    IdRemision = 0;
    Remision = 0;
    BuscarRemisiom(numero);
});

   
$("#tablamodalclienteing > tbody").on("click", "tr", function (e) {
    var row = $(this).parents("td").context.cells;
    var numero = row[1].innerText;
    $("#modalClientesIng").modal("hide");
    $("#Cliente").val(numero);
    BuscarCliente(numero,0);
});

$("#formenviarremision").submit(function (e) {

    var correo = $.trim($("#eCorreo").val());
    a_correo = correo.split(";");

    for (var x = 0; x < a_correo.length; x++) {
        if (ValidarCorreo($.trim(a_correo[x])) == 2) {
            $("#eCorreo").focus();
            swal(ValidarTraduccion("Acción Cancelada"), ValidarTraduccion("Formato de correo onválido") + ":" + a_correo[x], "warning");
            return false;
        }
    }

    ActivarLoad();
    setTimeout(function () {
        var parametros = "remision=" + Remision + "&principal=" + CorreoEmpresa + "&e_email=" + correo + " &archivo=" + Pdf_Remision + " &cliente=" + $("#eCliente").val() + "&id=" + IdRemision;
        var datos = LlamarAjax("Cotizacion","opcion=EnvioRemision&" + parametros);
        DesactivarLoad();
        datos = datos.split("|");
        if (datos[0] == "0") {
            swal("", ValidarTraduccion(datos[1]) + " " + CorreoEmpresa + " " + correo, "success");
            $("#enviar_remision").modal("hide");
            Cli_Correo = $("#eCorreo").val();
            Estado = "Cerrado";
            $("#Estado").val(Estado);
            TablaTemporalIngreso();


        } else
            swal("Acción Cancelada", datos[1], "warning");

    }, 15);

    return false;
});

function EnviarIngreso() {

    if (Remision == 0)
        return false;

    if (Estado == "Temporal") {
        swal("Acción Cancelada", "No se puede enviar una remisión en estado temporal", "warning");
        return false;
    }

    if (CorreoEmpresa == "") {
        swal("Acción Cancelada", "Cliente no posee ningún correo principal registrado", "warning");
        return false;
    }

    $("#eCliente").val($("#NombreCliente").val());
    $("#eCorreo").val($("#Email").val());

    ImprimirRemision(Remision, Estado, 1);

    $("#spremision").html(Remision);

    $("#enviar_remision").modal("show");

}

function GuardarIngreso() {
    var sede = $("#Sede").val() * 1;
    var contacto = $("#Contacto").val() * 1;
    var observacion = $.trim($("#Observaciones").val());
    var totales = $("#Totales").val() * 1;
    var recepcion = $.trim($("#Recepcion").val());
    var archivo = $.trim($("#OArchivo").val());
    
    var mensaje = "";
    
    var remisiones = document.getElementsByName("IdRemision[]");
    
    for (var x = 0; x < remisiones.length; x++) {
		if (remisiones[x].value*1 > 0 && IdRemision == 0){
			swal("Acción Cancelada","No se puede guardar esta remisión porque fue guardada en otra computadora","warning");
			return false;
		}
	}

    if ($("#bodyIngreso").html() != "") {
        if (IdCliente != 0) {

            if (IdRemision == 0 && archivo == "") {
                $("#OArchivo").focus();
                mensaje = "<br> Debe de examinar el archivo pdf de la remisión del cliente";
            }

            if (contacto == 0) {
                $("#Contacto").focus();
                mensaje = "<br> Debe de seleccionar un contacto del cliente" + mensaje;
            }
            if (sede == 0) {
                $("#Sede").focus();
                mensaje = "<br> Debe de seleccionar una sede del cliente " + mensaje;
            }
            if (recepcion == "") {
                $("#Recepcion").focus();
                mensaje = "<br> Debe de seleccionar una recepción de documento" + mensaje;
            }

            if (mensaje != "") {
                swal("Acción Cancelada", mensaje, "warning");
                return false;
            }

            var parametros = "id=" + IdRemision + "&cliente=" + IdCliente + "&sede=" + sede + "&contacto=" + contacto + "&observaciones=" + observacion + "&totales=" + totales + "&recepcion=" + recepcion;
            var datos = LlamarAjax("Cotizacion","opcion=GuardarIngreso&" + parametros);
            datos = datos.split("|");
            if (datos[0] == "0") {
                IdRemision = datos[3];
                Remision = datos[2];
                $("#Remision").val(datos[2]);
                Estado = "Ingresado";
                $("#Estado").val(Estado);
                $("#Serie").prop("disabled",true);
                $("#Cotizacion").attr("disabled", true);
                $("#Remision").attr("disabled", true);
                sendMessage(2);
                ImprimirRemision(Remision, Estado, 0);
                swal("", datos[1] + " " + datos[2] + ( datos[4] != "" ? "<br>" + datos[4] : ""), "success");
                if (archivo != "")
                    PDFRemision = 1;
            } else {
                swal("Acción Cancelada", datos[1], "warning");
            }
        }
    }
}

function InicializarCamara(ingreso, fotos) {
    CargarFotos(ingreso);
    $("#FotoIngreso").html(ingreso);
    $("#modal_camara").modal("show");
}

function EliminarFoto() {

    ingreso = $("#FotoIngreso").html() * 1;
    datos = LlamarAjax("Cotizacion", "opcion=EliminarFoto&ingreso=" + ingreso).split("|");
    if (datos[1] != "X") {
        CargarFotos(ingreso, datos[1]);
        CambiarFoto2(datos[1], ingreso);
    }
}

function CargarFotos(ingreso) {
    var fotos = LlamarAjax("Cotizacion","opcion=CantidadFoto&ingreso=" + ingreso) * 1;
    var contenedor = "";
    if (fotos > 0) {
        contenedor = "<div class='row'><div class='col-xs-12 col-sm-12 text-center'>";
        for (var x = 1; x <= fotos; x++) {
            contenedor += "<button class='btn btn-primary' type='button' onclick='CambiarFoto2(" + x + "," + ingreso + ")'>" + x + "</button>&nbsp;";
        }
        contenedor += "</div></hr><div class='col-xs-12 col-sm-12 text-center'>" +
            "<img id='FotoIngreso2' src='" + url_cliente +  "Adjunto/imagenes/ingresos/" + ingreso + "/1.jpg?id=" + NumeroAleatorio() + "' width='90%'></div></div>";
    }
    $("#fotocam").html(contenedor);
}

function GuardarFotoIng(ingreso, archivo) {

        
    var parametros = "opcion=SubirFoto&archivo=" + archivo + "&ingreso=" + ingreso + "&tipo_imagen=2";
    var data = LlamarAjax("Configuracion",parametros).split("|");
    if (data[0] == "0"){
		CargarFotos(ingreso, data[3]);
		CambiarFoto2(data[3], ingreso);
	}else{
		swal("Acción Cancelada",data[1],"success");
	}
}


function CambiarFoto2(numero, Ingreso) {
    $("#FotoIngreso2").attr("src", url_cliente + "Adjunto/imagenes/ingresos/" + Ingreso + "/" + numero + ".jpg?id=" + NumeroAleatorio());
}

function TomarFoto() {
    Webcam.snap(function (data_uri) {
        //var raw_image_data = data_uri.replace(/^data\:image\/\w+\;base64\,/, '');
        GuardarFotoIng($("#FotoIngreso").html(), data_uri);
    });   
}


Webcam.attach('#my_camera');

function AgregarAcceObser(valor, tipo) {
    if (valor == "")
        return false;
    if (tipo == 1) {
        texto = $("#Accesorio").val();
        id = "#Accesorio";
    } else {
        texto = $("#Observacion").val();
        id = "#Observacion";
    }

    if (texto.indexOf("*" + valor + "*") < 0) {
        if ($(id).val() == "")
            $(id).val("*" + valor + "*");
        else
            $(id).val($(id).val() + valor + "*");
    }
}


$("#Referencia").focus();
$('select').select2();
DesactivarLoad();
