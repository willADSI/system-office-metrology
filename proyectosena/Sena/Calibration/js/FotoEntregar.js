﻿var Devolucion = 0;
var Fotos = 0;
var ErrorEntrega = 0;
var url = $("#urlprincipal").val();
$("#EmpresaEnvio").html(CargarCombo(21, 1));

var canvafirma = document.getElementById("padfirmarent");
initPad(canvafirma);


function LimpiarTodo() {
    LimpiarDatos();
    $("#Devolucion").val("");
}

function CambiarFotoEntrega(numero) {
    $("#FotoDevolucion").attr("src", url + "imagenes/FotoEntrega/" + Devolucion + "/" + numero + ".jpg");
}

function CargarFotosEntrega() {
    var contenedor = "";
    contenedor = "<div class='row'><div class='col-xs-12 col-sm-12'>";
    for (var x = 1; x <= Fotos; x++) {
        contenedor += "<a class='btn btn-primary' href='javascript:CambiarFotoEntrega(" + x + ")'>" + x + "</a>&nbsp;";
    }
    contenedor += "</div>" + 
            "<div class='col-xs-12 col-sm-12'><img id='FotoDevolucion' " + (Fotos > 0 ? "src='" + url + "imagenes/FotoEntrega/" + Devolucion + "/1.jpg'" : "") + " width='90%'></div></div>";
    $("#AlbunFotosEnt").html(contenedor);
}

function LimpiarDatos() {
    Devolucion = 0;
    Fotos = 0;
        
    $("#Cliente").val("");
    $("#Contacto").val("");
    $("#Direccion").val("");
    $("#Cantidad").val("");
    $("#Fecha").val("");
    $("#Asesor").val("");
    $("#Observacion").val("");
    $("#AlbunFotosDev").html("");

}

function GuardarFirma() {
    var firma = canvafirma.toDataURL("image/png");
    $.ajax({
        type: 'POST',
        url: url + "Cotizacion/GuardarFirma",
        data: "firma=" + firma + "&Devolucion=" + Devolucion,
        success: function (msg) {
            respuesta = "si";
        }
    });
}


function EntregarDevolucion() {
    if (Devolucion == 0)
        return false;
    var recibio = $.trim($("#Recibio").val());
    var cargo = $.trim($("#Cargo").val());
    var identificacion = $.trim($("#Identificacion").val());
    var telefonos = $.trim($("#Telefonos").val());
    var observacion = $.trim($("#ObservacionCliente").val());
    var empresaenvio = $.trim($("#EmpresaEnvio").val());
    var guia = $.trim($("#NumeroGuia").val());

    var mensaje = "";

    if (empresaenvio > 0 && guia == "") {
        $("#NumeroGuia").focus();
        mensaje = "<br>Debe de ingresar la guía de envío " + mensaje;
    }

    if (observacion == "") {
        $("#ObservacionCliente").focus();
        mensaje = "<br>Debe de ingresar la observación del cliente " + mensaje;
    }

    if (telefonos == "") {
        $("#Telefonos").focus();
        mensaje = "<br>Debe de ingresar un teléfono " + mensaje;
    }
        
    if (cargo == "") {
        $("#Cargo").focus();
        mensaje = "<br>Debe de ingresar el cargo de quién recibió" + mensaje;
    }

    if (cargo == "") {
        $("#Recibio").focus();
        mensaje = "<br>Debe de ingresar quién recibió" + mensaje;
    }

    if (mensaje != "") {
        swal("Acción Cancelada", mensaje, "warning");
        return false;
    }

    if (localStorage.getItem("FirmaCertificado") == "") {
        swal("Acción Cancelada", "Debe primero firmar el cliente", "warning");
        return false;
    }

    ActivarLoad();
    setTimeout(function () {
        GuardarFirma();

        var parametros = "recibio=" + recibio + "&identificacion=" + identificacion + "&cargo=" + cargo + "&telefonos=" + telefonos +
            "&observacion=" + observacion + "&devolucion=" + Devolucion + "&empresaenvio=" + empresaenvio + "&guia=" + guia;
        var datos = LlamarAjax("Cotizacion/EntregarDevolucionFoto", parametros).split("|");
        DesactivarLoad();
        if (datos[0] == "0") {
            swal("", datos[1], "success");
        } else
            swal("Acción Cancelada", datos[1], "warning");
    }, 15);
}




function CargarVueltas(cliente) {
    var datos = LlamarAjax("Cotizacion/BuscarVueltasPendientes", "cliente=" + cliente + "&tipo=1&pendiente=1&entregar=1");
    var Resultado = "";
    if (datos != "[]") {
        var data = JSON.parse(datos);
        for (var x = 0; x < data.length; x++) {
            Resultado += "<tr id='FilaRD-" + x + "' class='Seleccionado'>" +
                "<td><button class='btn btn-success' title='Ejecutar Vuelta' type='button' onclick='EjecutarVuelta(" + data[x].id + ")'><span data-icon='&#xe147;'></span></button></td>" +
                "<td>" + data[x].fechaapro + "</td>" +
                "<td>" + data[x].estado + "</td>" +
                "<td>" + data[x].actividad + "</td></tr>";
        }
    }
    $("#TablaVueltas").html(Resultado);
}

function BuscarDevolucion(numero, code, tipo) {
    if (Devolucion == (numero * 1))
        return false;
    if ((Devolucion != (numero * 1)) && ($("#Equipo").val() != ""))
        LimpiarDatos();
    if (numero * 1 == 0)
        return false;
    if ((code.keyCode == 13 || tipo == 1) && ErrorEntrega == 0) {
        if (numero * 1 > 0) {
            var datos = LlamarAjax("Cotizacion/BuscarDevolucion?devolucion=" + numero);
            if (datos != "[]") {
                var data = JSON.parse(datos);
                var entregado = data[0].entregado * 1;
                if (entregado == 1) {
                    swal("Acción Cancelada", "El Devolucion número " + numero + " ya fue entregada", "warning");
                    ErrorEntrega = 1;
                    $("#Devolucion").select();
                    $("#Devolucion").focus();
                    return false;
                }
                Devolucion = data[0].devolucion * 1;
                Fotos = data[0].fotoentrega * 1;
                
                $("#Cliente").val(data[0].cliente);
                $("#Contacto").val(data[0].contacto);
                $("#Direccion").val(data[0].sede);
                $("#Cantidad").val(data[0].cantidad);
                $("#Fecha").val(data[0].fecha);
                $("#Asesor").val(data[0].usuario);
                $("#Observacion").val(data[0].observacion);

                $("#Recibio").focus();

                CargarFotosEntrega();

                CargarVueltas(data[0].idcliente);

            } else {
                swal("Acción Cancelada", "El Devolucion número " + numero + " no se encuentra registrado", "warning");
                ErrorEntrega = 1;
                $("#Devolucion").select();
                $("#Devolucion").focus();
                
                
                
                return false;
            }
        }
    } else
        ErrorEntrega = 0;
}
    

function TomarFoto() {

    if (Devolucion > 0) {
        Webcam.snap(function (data_uri) {
            //var raw_image_data = data_uri.replace(/^data\:image\/\w+\;base64\,/, '');
            GuardarFoto(Devolucion, data_uri);
        });
    }
    
}

function EliminarFoto() {
    if (Devolucion > 0) {
        datos = LlamarAjax("Cotizacion/EliminarFotoDev", "devolucion=" + Devolucion).split("|");
        if (datos[1] != "X") {
            Fotos = datos[1]*1;
            CargarFotos(Devolucion, datos[1]);
            CambiarFoto2(datos[1], Devolucion);
        }
    }
}

function GuardarFoto(devolucion, archivo) {
    ActivarLoad();
    setTimeout(function () {
        $.ajax({
            type: 'POST',
            url: url + "Cotizacion/GuardarFotoEntrega",
            data: "archivo=" + archivo + "&Devolucion=" + devolucion,
            async: true,
            success: function (data) {
                Fotos = data * 1;
                CargarFotosEntrega();
                CambiarFotoEntrega(data);
            }
        });
        DesactivarLoad();
    }, 15);
}


function SubirFoto() {
    if (Devolucion > 0) {

        swal.queue([{
            html: '<h3>' + ValidarTraduccion("Seleccione la foto que desea subir de la Devolucion número " + Devolucion) + '</h3><input type="file" class="form-control" id="documento" onchange="GuardarDocumento()" name="documento" accept="image/*" required />',
            showLoaderOnConfirm: true,
            showCancelButton: true,
            confirmButtonText: ValidarTraduccion('OK'),
            cancelButtonText: ValidarTraduccion('No'),
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            preConfirm: function () {
                return new Promise(function (resolve, reject) {
                    resolve();
                })
            }
        }]);
    }
}

Webcam.set({
    width: 400,
    height: 480,
    dest_width: 780,
    dest_height: 960,
    image_format: 'png',
    jpeg_quality: 100
});
Webcam.attach('#my_cameraent');

function EjecutarVuelta(id) {
    var cliente = $("#Cliente option:selected").text();
    var datos = LlamarAjax("Configuracion/DescripcionVuelta", "id=" + id).split("||");
    var vuelta = datos[0];
    var observacion = datos[1];

    $("#IdEjecutar").val(id);
    $("#EjeVuelta").val("").trigger("change");
    $("#DVuelta").val(vuelta);
    $("#DCliente").val(cliente);
    $("#OVuelta").val(observacion);

    $("#modalEjecutar").modal("show");

}

function GuaEjecVueltaFoto() {
    var id = $("#IdEjecutar").val() * 1;
    var ejecutada = $("#EjeVuelta").val();
    var observacion = $.trim($("#OVuelta").val());

    if (ejecutada == "") {
        $("#EjeVuelta").focus();
        swal("Acción Cancelada", "Debe seleccionar si la vuelta fue ejecutada", "warning");
        return false;
    } else {
        if (ejecutada == "NO" && observacion == "") {
            $("#OVuelta").focus();
            swal("Acción Cancelada", "Si la vuelta no fue ejecutada debe de ingresar una observación", "warning");
            return false;
        }
        if (ejecutada == "RECHAZADA" && observacion == "") {
            $("#OVuelta").focus();
            swal("Acción Cancelada", "Si la vuelta se rechaza debe de ingresar una observación", "warning");
            return false;
        }
    }

    var datos = LlamarAjax("Configuracion/OperacionVuelta", "id=" + id + "&operacion=" + 4 + "&fecha=2019-01-01 00:00&ejecutado=" + ejecutada + "&observacion=" + observacion).split("|");
    if (datos[0] == "0") {
        BuscarVueltasPendientes();
        $("#AlbunFotosRecoger").html("");
        $("#modalEjecutar").modal("hide");
        swal("", datos[1], "success");
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
}



$('select').select2();
DesactivarLoad();
