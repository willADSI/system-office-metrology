﻿d = new Date();
month = d.getMonth() + 1;
day = d.getDate();

var output = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-' +
    (day < 10 ? '0' : '') + day;

var Anio = $.trim(d.getFullYear());
Anio = $.trim(Anio.substr(2, 2))

var Anio = $.trim(d.getFullYear());

var porretefuente = 0;
var porreteica = 0;
var porreteiva = 0;

var opciones = "<option value=''>--Seleccione--</option>";
for (var x = Anio; x >= 2018; x--) {
    opciones += "<option value='" + x + "'>" + x + "</option>";
}
$("#MAnio").html(opciones);

var Guia = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-' +
    (day < 10 ? '0' : '') + day + '-' + d.getHours() + '-' + d.getMinutes() + '-' + d.getSeconds();


document.onkeydown = function (e) {
    tecla = (document.all) ? e.keyCode : e.which;

    switch (tecla) {
        case 112: //F1
            ModalClientes();
            return false;
            break;
        case 113: //F9
            CargarModaltarjeta();
            return false;
            break;
        case 114: //F10
            CargarModalCheque();
            return false;
            break;
        case 115: //F11
            CargarModalConsig()
            return false;
            break;
        case 117: //F12
            LimpiarPagos();
            return false;
            break;
        case 118: //f2
            LimpiarTodo();
            return false;
            break;
        case 119: //f10
            ModalRecibo();
            return false;
            break;
        case 120: //f3
            GuardarRecibo();
            return false;
            break;
        case 121: //f4
            EnviarRecibo();
            return false;
            break;
        case 122: //f6
            ImprimirRecibo(1);
            return false;
            break;
        case 123: //f8
            AnularRecibo();
            return false;
            break;
    }
    
}

$("#ChFecha").val(output);
$("#ttipo").html(CargarCombo(23, 1));
$("#con_cuenta, #MCuenta").html(CargarCombo(25, 1));

$("#ChBanco").html(CargarCombo(24, 1));

$("#ttipo").val("");

function LlamarModalMovimientos() {
    if (Abonado > 0 || TotalSeleccionado > 0 || NombreCliente == "")
        return false;
    CargarModalMovimientos();
    $("#ModalMovBancario").modal("show");
}

function CargarModalMovimientos() {

    var cuenta = $("#MCuenta").val()*1;
    var anio = $("#MAnio").val()*1;
    var mes = $("#MMes").val()*1;

    var datos = LlamarAjax("Caja","opcion=TablaMovimientos&tipo=1&cuenta=" + cuenta + "&anio=" + anio + "&mes=" + mes);
    var datajson = JSON.parse(datos);
    var tabla = $('#MTablaMovimiento').DataTable({
        data: datajson,
        bProcessing: true,
        bDestroy: true,
        paging: true,
        ordering: true,
        info: false,
        searching: true,
        columns: [
            { "data": "id" },
            { "data": "fecha" },
            { "data": "referencia" },
            { "data": "descripcion" },
            { "data": "oficina" },
            { "data": "entrada", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') }
        ],

        "language": {
            "url": LenguajeDataTable
        }

    });
}

var IdCliente = 0;
var Almacen = localStorage.getItem("almacen") * 1;
var Cli_Correo = "";
var CliReteIva = 0;
var CliReteIca = 0;
var CliReteFuente = 0;
var NombreCliente = "";
var TotalFactura = 0;
var Recaudado = 0;
var SaldoFac = 0;
var SaldoAbo = 0;
var Abonado = 0;
var RetIVA = 0;
var RetCREE = 0;
var RetFuente = 0;
var RetICA = 0;
var Descuento = 0;
var OIngreso = 0;
var OEgreso = 0;
var TotalSeleccionado = 0;
var TotalSelSubtotal = 0;
var TotalSelecIva = 0;
var NRecibo = 0;
var Efectivo = 0;
var DiferenciaMenor = 0;
var Tarjeta = 0;
var Cheque = 0;
var Consignado = 0;
var Anticipo = 0;
var Facturas = "";
var CantCheque = 0;
var TotalCheques = 0;
var EditTotalCheques = 0;
var DocumentoCli = "";
var Movimiento = 0;
var ClienteAnticipo = 0;

var MCuenta = 0;

var PlazoPago = "0";
var TipoCliente = "";
var CorreoEmpresa = "";

var ErrorCli = 0;
var c_Efectivo = document.getElementById('Efectivo');
var c_DiferenciaMenor = document.getElementById('DiferenciaMenor');
var c_Tarjeta = document.getElementById('Tarjeta');
var CantCheque = document.getElementById('ChCantidad');
var c_Cheque = document.getElementById('Cheque');
var c_VRecaudado = document.getElementById('VRecaudado');
var c_Consignado = document.getElementById('Consignacion');

var c_RetIVA = document.getElementById('RetIVA');
var c_RetCREE = document.getElementById('RetCREE');
var c_RetFuente = document.getElementById('RetFuente');
var c_RetICA = document.getElementById('RetICA');
var c_Descuento = document.getElementById('Descuento');

var c_OIngreso = document.getElementById('OIngreso');
var c_OEgreso = document.getElementById('OEgreso');
var Diferencia = 0;


function chCambioPersona(tipo) {
    if (tipo == "N") {
        $("#ChNombre").val("");
        $("#ChCedula").val("");
        $("#ChTelefono").val("");
        $("#ChDireccion").val("");
    } else {
        $("#ChNombre").val($("#ChNombre").prop("inicial"));
        $("#ChCedula").val($("#ChCedula").prop("inicial"));
        $("#ChTelefono").val($("#ChTelefono").prop("inicial"));
        $("#ChDireccion").val($("#ChDireccion").prop("inicial"));
    }
}



function AnularRecibo() {

    if (NRecibo == 0)
        return false;

    var resultado = "";
    swal({
        title: 'Advertencia',
        text: '¿' + ValidarTraduccion('Desea Anular el Recibo') + ' ' + NRecibo + ' ' + ValidarTraduccion('del cliente') + ' ' + NombreCliente,
        input: 'text',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Anular'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',

        inputValidator: function (value) {
            return new Promise(function (resolve, reject) {
                if (value) {
                    var datos = LlamarAjax("Caja","opcion=AnularRecibo&recibo=" + NRecibo + "&observaciones=" + value)
                    resultado = NRecibo;
                    datos = datos.split("|");
                    if (datos[0] == "0") {
                        resolve()
                    } else {
                        reject(datos[1]);
                    }

                } else {
                    reject(ValidarTraduccion('Debe de ingresar una observación'));
                }
            })
        }
    }).then(function (result) {
        Limpiar();
        IdCliente = 0;
        $("#Cliente").val("");
        $("#Cliente").focus();
        swal({
            type: 'success',
            html: 'Recibo Nro ' + resultado + ' anulado con éxito'
        })
    })

}

function LimpiarTarjeta() {
    $("#tnumero").val("").trigger("change");
    $("#Tarjeta").val("");
    $("#ttipo").val("").trigger("change");
    $("#tcodseguridad").val("");
    $("#tfvencimiento").val("");
    $("#tautoriza").val("");
    CalcularTotal();
}

function GuardarCheque(parametros) {
    var cantidad = $("#ChCantidad").val() * 1;
    var valor = NumeroDecimal($("#Cheque").val());
    var autorizado = $.trim($("#ChAutorizado").val()).toUpperCase();
    var autorizadopor = $.trim($("#ChAutorizacion").val()).toUpperCase();

    if (cantidad == 0) {
        $("#ChCantidad").focus()
        swal(ValidarTraduccion(ValidarTraduccion("Acción Cancelada")), "Debe de ingresar la cantidad de cheques", "warning");
        return false;
    }

    if (valor == 0) {
        $("#Cheque").focus()
        swal(ValidarTraduccion(ValidarTraduccion("Acción Cancelada")), "Debe de ingresar el valor en cheques", "warning");
        return false;
    }

    if (noVericarCheque == 0) {
        if ((autorizado.substr(0, 2) != "D-") && (autorizado.substring(1, 2) != "C-") && (autorizado.substring(1, 2) != "F-")) {
            $("#ChAutorizado").select();
            $("#ChAutorizado").focus();
            swal(ValidarTraduccion(ValidarTraduccion("Acción Cancelada")), "D-(nombre autorizador): para Datacheque <br> C- (nombre autorizador): para Covinoc <br> F - (nombre autorizador): para Fenalcheque", "warning");
            return false;
        }

        if (autorizado == "" || autorizadopor == "") {

            if (autorizado == "") {
                $("#ChAutorizado").select();
                $("#ChAutorizado").focus();
            } else {
                $("#ChAutorizacion").select();
                $("#ChAutorizacion").focus();
            }
            swal(ValidarTraduccion(ValidarTraduccion("Acción Cancelada")), "Debe de completar la autorización del cheque", "warning");
            return false;
        }
    }

    parametros += "&Guia=" + Guia + "&Valor=" + valor;

    var datos = LlamarAjax("Caja","opcion=GuardarCheque&"+ parametros);
    datos = datos.split("|");
    if (datos[0] == "0") {
        swal("", datos[1], "success");
        CargarTablaCheque();
        LimpiarCheque();
        
    } else {
        swal("", datos[1], "error");
    }
}

function EliminarTemporalCheque(id,numero) {

    swal({
        title: '¿Eliminar el cheque número ' + numero + '?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Sí',
        cancelButtonText: 'No'

    }).then(function () {

        var parametros = "id=" + id+"&guia=";
        var datos = LlamarAjax("Caja","opcion=EliminarCheque&"+ parametros);
        datos = datos.split("|");
        if (datos[0] == "0") {
            CargarTablaCheque();
            LimpiarCheque();
        } else {
            $.jGrowl(ValidarTraduccion(datos[1]), { life: 6000, theme: 'growl-warning', header: '' });
        }

    }, function (dismiss) {
        // dismiss can be 'cancel', 'overlay',
        // 'close', and 'timer'
        if (dismiss === 'cancel') {
        }
    })

}

function EditarTemporalCheque(id) {
    var datos = LlamarAjax("Caja","opcion=TablaCheque&id=" + id);
    eval("data=" + datos);

    $("#id_cheque").val(data[0].id);
    $("#ChAutorizado").val(data[0].AutorizadoPor)
    $("#ChAutorizacion").val(data[0].Autoriza)
    EditTotalCheques = data[0].Valor * 1;
    $("#Cheque").val(formato_numero(data[0].Valor, "0", ".", ",", ""));
    $("#ChCantidad").val("1");
    if (data[0].chqEstado == "F") {
        $("#ChTipoC_B").attr("checked", false);
        $("#ChTipoC_C").attr("checked", false);
    } else {
        if (chqEstado == "P") 
            $("#ChTipoC_B").attr("checked", true);
        else
            $("#ChTipoC_B").attr("checked", false);
    }
        
    $("#ChPersona_J").attr("checked", true);

    $("#ChNombre").val(data[0].Girador);
    $("#ChCedula").val(data[0].Cedula);
    $("#ChTelefono").val(data[0].Telefono);
    $("#ChDireccion").val(data[0].Direccion);
    $("#ChFecha").val(data[0].chqFechaCheque.substr(0, 10));

    $("#ChCuenta").val(data[0].Cuenta);
    $("#ChNumero").val(data[0].Numero);
    $("#ChBanco").val(data[0].chqCodBanco).trigger('change');
    $("#ChPlaza").val(data[0].chqCodPlaza).trigger('change');
    
}

function CargarTablaCheque() {
    var datos = LlamarAjax("Caja","opcion=TablaCheque&Guia=" + Guia);
    CargarTablaJsonTitulo(datos, "tablacheque", "divtabcheque");
    CalcularTotal('No');
}

function CargarModalAnticipo(tipo) {
    if (NRecibo > 0 || IdCliente == 0)
        return false;

    if (IdCliente != ClienteAnticipo) {
        ClienteAnticipo = IdCliente;

        var datos = LlamarAjax("Caja","opcion=ModalAnticiposCaja&cliente=" + IdCliente);
        if (datos == "[]") {
            if (tipo == 1)
                swal("Acción Cancelada", "Este cliente no posee anticipos pendientes", "warning");
            return false;
        }
        var data = JSON.parse(datos);
        var resultado = "";
        for (var x = 0; x < data.length; x++) {
            resultado += "<tr>" +
                "<td>" + data[x].anticipo + "<input type='hidden' name='NumAnticipo[]' value='" + data[x].anticipo + "'></td>" +
                "<td>" + data[x].fecha + "</td>" +
                "<td>" + data[x].concepto + "</td>" +
                "<td align='right'><input type='text' class='form-control text-right text-XX' value='" + formato_numero(data[x].monto, 0, ".", ",") + "' id='MonAnticipo" + x + "' disabled name='MonAnticipo[]'></a></td>" +
                "<td align='right'><input type='text' class='form-control text-right text-XX' value='" + formato_numero(data[x].saldo, 0, ".", ",") + "' id='SaldoAnticipo" + x + "' disabled name='SaldoAnticipo[]'></td>" +
                "<td align='right'><input type='text' class='form-control text-right text-XX' value='' onfocus='FormatoEntrada(this, 1)' onblur='FormatoSalida(this, 0, 1)' id='UsarAnticipo" + x + "' name='UsarAnticipo[]' onkeyup='CalcularAnticipo(this, " + x + ")'></td>" +
                "<td align='right'><input type='text' class='form-control text-right text-XX' value='" + formato_numero(data[x].saldo, 0, ".", ",") + "' id='RestanteAnticipo" + x + "' name='RestanteAnticipo[]' disabled></td></tr>";
        }
        $("#BodyAnticipo").html(resultado);
    }
    $("#ModalAnticipo").modal("show");

    if (tipo == 0)
        swal("", "Cliente posee anticipos pendientes", "success");

}

function AplicarAnticipo() {
    var a_anticipo = document.getElementsByName("UsarAnticipo[]");
    Anticipo = 0;
    for (var x = 0; x < a_anticipo.length; x++) {
        Anticipo += NumeroDecimal(a_anticipo[x].value);
    }
    if (Anticipo == 0) {
        swal("Acción Cancelada", "Debe de ingresar por lo mínimo un anticipo", "warning");
        return false;
    }
    $("#Anticipo").val(formato_numero(Anticipo, "0", ".", ",", ""));
    CalcularTotal('No');
    $("#ModalAnticipo").modal("hide");
}

function EliminarAnticipo() {
    var a_anticipo = document.getElementsByName("UsarAnticipo[]");
    Anticipo = 0;
    for (var x = 0; x < a_anticipo.length; x++) {
        a_anticipo[x].value = 0;
    }
    $("#Anticipo").val(formato_numero(Anticipo, "0", ".", ",", ""));
    CalcularTotal('No');
    $("#ModalAnticipo").modal("hide");
}


function CalcularAnticipo(Caja, x) {
    ValidarTexto(Caja, 1);
    var saldo = NumeroDecimal($("#SaldoAnticipo" + x).val());
    var utilizar = NumeroDecimal(Caja.value);
    var restante = saldo - utilizar;
    if (restante < 0) {
        $("#UsarAnticipo" + x).val("");
        $("#UsarAnticipo" + x).focus();
        $("#RestanteAnticipo" + x).val(formato_numero(saldo, 0,_CD,_CM,""));
        swal("Acción Cancelada", "El monto a utilizar debe ser menor o igual al saldo del anticipo", "warning");
        return false;
    }
    $("#RestanteAnticipo" + x).val(formato_numero(restante, 0,_CD,_CM,""));
    

}

function LimpiarPagos() {

    if (NRecibo > 0)
        return false;

    LimpiarTarjeta();
    LimpiarConsignacion();
    var parametros = "id=0" + "&guia=" + Guia;
    //var datos = LlamarAjax("Caja/EliminarCheque", parametros);
    CargarTablaCheque();
    LimpiarCheque();
    $("#Efectivo").val("0")
    
    Guia = Guia = d.getFullYear() + '-' +
        (month < 10 ? '0' : '') + month + '-' +
        (day < 10 ? '0' : '') + day + '-' + d.getHours() + '-' + d.getMinutes() + '-' + d.getSeconds();
    CalcularTotal();

}

function LimpiarCheque() {
    chCambioPersona('J');
    $("#ChAutorizado").val("")
    $("#ChAutorizacion").val("")
    $("#Cheque").val("0");
    EditTotalCheques = 0;
    CalcularTotal('No');
    $("#Cheque").val(formato_numero(SaldoAbo, "0", ".", ",", ""));
    $("#ChCantidad").val("1");
    CalcularTotal('No');
    $("#ChTipoC_B").attr("checked", false);
    $("#ChTipoC_C").attr("checked", false);

    $("#ChPersona_J").attr("checked", true);
    chCambioPersona('J');
    
    $("#ChCuenta").val("");
    $("#ChNumero").val("");

    $("#ChBanco").val("").trigger('change');
    $("#ChPlaza").val("").trigger('change');
    $("#id_cheque").val("0")

}

$("#formCheques").submit(function (e) {

    e.preventDefault();
    var Parametros = $("#formCheques").serialize() 
    GuardarCheque(Parametros);

})

function LimpiarTodo() {
    Limpiar();
    $("#Cliente").val("");
    IdCliente = 0;
}

function Limpiar() {
    $("#NombreCliente").html("");
    NRecibo = 0;
    NombreCliente = "";
    DocumentoCli = "";
    Movimiento = 0;
    Diferencia = 0;
    Anticipo = 0;
    ClienteAnticipo = 0;

    RetIVA = 0;
    RetCREE = 0;
    RetFuente = 0;
    RetICA = 0;
    Descuento = 0;

    OIngreso = 0;
    OEgreso = 0;

    Efectivo = 0;
    Tarjeta = 0;
    Cheque = 0;
    Consignado = 0;
    TotalCheques = 0;
    Abonado = 0;
    Recaudado = 0;

    SaldoFac = 0;
    SaldoAbo = 0;

    Abonado = 0;

    $("#Detalle_Movimiento").html("");
    $("#divAnulaciones").addClass("hidden");
    $("#btnImprimir").addClass("hidden");
    $("#btnImprimirmov").addClass("hidden");
    $("#btnEnviar").addClass("hidden");
    $("#btnAnular").addClass("hidden");
    $("#BtnGuardarRecibo").removeClass("hidden");
    $("#SDiferencia").val("");
    EditTotalCheques = 0;
    TotalSeleccionado = 0;
    TotalSelSubtotal = 0;
    TotalSelecIva = 0;
    $("#SaldoClienteDev").html("");

    LimpiarConsignacion();
    $("#chTotales").html("0");
    $("#divtabcheque").html("");
    LimpiarCheque();
        
    EditTotalCheques = 0;
    TotalSeleccionado = 0;

    PlazoPago = "0";
    TipoCliente = "";
    CorreoEmpresa = "";
    
    NombreCliente = "";
    Guia = Guia = d.getFullYear() + '-' +
        (month < 10 ? '0' : '') + month + '-' +
        (day < 10 ? '0' : '') + day + '-' + d.getHours() + '-' + d.getMinutes() + '-' + d.getSeconds();

    $("#tnumero").val("");
    $("#tvalor").val("");
    $("#ttipo").val("");
    $("#tcodseguridad").val("");
    $("#tfvencimiento").val("");
    $("#tautoriza").val();

    $("#SSubTotal").val("");
    $("#SIVA").val("");

    Facturas = "";

    $("#RetIVA").val("0");
    $("#Anticipo").val("0");
    $("#RetCREE").val("0");
    $("#RetICA").val("0");
    $("#RetFuente").val("0");
    $("#TTarjeta").val("0");
    $("#Tarjeta").val("0");
    $("#Efectivo").val("0");
    $("#DiferenciaMenor").val("0")
    $("#Consignacion").val("0");
    $("#Cheque").val("0");
    $("#TConsignacion").val("0");
    $("#TCheque").val("0");
    $("#Descuento").val("0");
    $("#OIngreso").val("0");
    $("#OEgreso").val("0");


    $("#RetIVA").prop("disabled", false);
    $("#RetCREE").prop("disabled", false);
    $("#RetICA").prop("disabled", false);
    $("#RetFuente").prop("disabled", false);
    $("#Efectivo").prop("disabled", false);
    $("#DiferenciaMenor").prop("disabled", false);
    $("#Descuento").prop("disabled", false);
    $("#OEgreso").prop("disabled", false);
    $("#VRecaudado").prop("disabled", false);
    $("#Concepto").prop("disabled", false);
    $("#Observacion").val("");
    
    $("#TFactura").val("0");
    $("#SFactura").val("0");
    $("#VRecibido").val("0");
    $("#SPagar").val("0");
    $("#VRecaudado").val("0");
    $("#Concepto").val("CANCELACIÓN DE FACTURA");

    $("#bodyfacturacion").html("");
    $("#Recibo").html("0");
    $("#NombreCliente").html("");
    $("#STotal").html("0");

    $("#ObsAnulacion").html("");
    $("#TbContabilidadRC").html("");
    $("#TDebito").html("0");
    $("#TCredito").html("0");
    $("#TDiferencia").html("0");
    $("#TbContabilidadRC").html("");

}

$("#Cliente").bind('keypress', function (e) {
    if (e.keyCode == 13) {
        $("#Cliente").blur();
    }
})


function SeleccionarFila(pos, valor, numfac, subtotal, iva, abonado) {
    if (NRecibo > 0)
        return false
    if (Recaudado == 0 && Anticipo == 0) {
        $.jGrowl(ValidarTraduccion("Debe ingresar primero lo recaudado " + Recaudado), { life: 4000, theme: 'growl-warning', header: '' });
        return false;
    }

    if ($("#selecc" + pos).val() == "0") {
        //if (TotalSeleccionado > (Recaudado + RetIVA + RetCREE + RetFuente + RetICA + Descuento + OEgreso - OIngreso))
        //    return false;
        $("#tr-" + pos).addClass("bg-success");
        $("#selecc" + pos).val("1");
        TotalSeleccionado += Math.round(valor);
        TotalSelSubtotal += Math.round(subtotal);
        if (abonado == 0) {
            RetIVA += Math.trunc(iva * porreteiva / 100);
            RetICA += Math.trunc(subtotal * porreteica / 100);
            RetFuente += Math.trunc(subtotal * porretefuente / 100);
        }
        TotalSelecIva += iva;
        if (Facturas == "")
            Facturas = numfac
        else
            Facturas += "," + numfac;

    } else {
        $("#tr-" + pos).removeClass("bg-success");
        $("#selecc" + pos).val("0");
        TotalSeleccionado -= Math.round(valor);
        TotalSelSubtotal -= Math.round(subtotal);
        TotalSelecIva -= iva;
        if (abonado == 0) {
            RetIVA -= Math.trunc(iva * porreteiva / 100);
            RetICA -= Math.trunc(subtotal * porreteica / 100);
            RetFuente -= Math.trunc(subtotal * porretefuente / 100);
        }
        if ($.trim(Facturas).indexOf(",") >= 0) {
            arrego = Facturas.split(",");
            Facturas = "";
            for (var x = 0; x < arrego.length; x++) {
                if (arrego[x] * 1 != numfac * 1) {
                    if (Facturas == "")
                        Facturas = arrego[x]
                    else
                        Facturas += "," + arrego[x];
                }
            }
        } else
            Facturas = "";
    }

    
        
    $("#STotal").html(formato_numero(TotalSeleccionado, 0, ".", ","));
    $("#SSubTotal").html(formato_numero(TotalSelSubtotal, 0, ".", ","));
    $("#SIVA").html(formato_numero(TotalSelecIva, 0, ".", ","));

    $("#RetIVA").val(formato_numero(RetIVA, 0, ".", ","));
    $("#RetICA").val(formato_numero(RetICA, 0, ".", ","));
    $("#RetFuente").val(formato_numero(RetFuente, 0, ".", ","));

    CalcularTotal('No');
    ContabilizarRecibo();
}

function TablaFacturasRecibo() {

    var parametros = "recibo=" + NRecibo;
    var datos = LlamarAjax('Caja','opcion=BuscarRecibo&'+ parametros).split("||");
    var TotalFactura = 0;
    var resultado = "";
    
    if (datos[0] != "[]") {
        eval("data=" + datos[0]);
        $("#Concepto").val(data[0].concepto);
        $("#Observacion").val(data[0].observacion);
        $("#Anticipo").val(formato_numero(data[0].anticipo, 0, ".", ","));
        $("#VRecibido").val(formato_numero(data[0].recaudado, 0, ".", ","));
        $("#VRecaudado").val(formato_numero(data[0].recaudado, 0, ".", ","));
        $("#Detalle_Movimiento").html(data[0].movimiento + formato_numero(data[0].monto, 0, ".", ",", ""));

        Recaudado = data[0].recaudado;
        Anticipo = data[0].anticipo;
        Facturas = "";
        if (data[0].fechaanul) {
            $("#divAnulaciones").removeClass("hidden");
            $("#ObsAnulacion").html("Usuario que Anuló: " + data[0].usuarioanula + ", Fecha: " + data[0].fechaanul + ", Observación: " + data[0].observaanula);
            $("#btnAnular").addClass("hidden");
        } else {
            $("#btnAnular").removeClass("hidden");
            $("#divAnulaciones").addClass("hidden");
        }
        for (var x = 0; x < data.length; x++) {

            if (Facturas == "")
                Facturas = data[x].factura
            else
                Facturas += "," + data[x].factura;

            resultado += "<tr>" +
                "<td><a class='text-XX' href='javascript:ImprimirFactura(" + data[x].factura + ")'>" + (x + 1) + "</a></td>" +
                "<td>" + data[x].factura + "</td>" +
                "<td>" + data[x].fecha + "</td>" +
                "<td>" + data[x].vencimiento + "</td>" +
                "<td align='right'>" + formato_numero(data[x].subtotal, 0, ".", ",") + "</td>" +
                "<td align='right'>" + formato_numero(data[x].descuentofac, 0, ".", ",") + "</td>" + 
                "<td align='right'>" + formato_numero(data[x].iva, 0, ".", ",") + "</td>" + 
                "<td align='right'>" + formato_numero(data[x].total, 0, ".", ",") + "</td>" +
                "<td align='right'>" + formato_numero(data[x].abonado, 0, ".", ",") + "</td>" +
                "<td align='right'>" + formato_numero(data[x].saldo, 0, ".", ",") + "</td>" +
                "<td align='right'>" + formato_numero(data[x].retefuentefac, 0, ".", ",") + "</td>" +
                "<td align='right'>" + formato_numero(data[x].reteivafac, 0, ".", ",") + "</td>" +
                "<td align='right'>" + formato_numero(data[x].reteicafac, 0, ".", ",") + "</td>" +
                "<td align='right'>" + data[x].asesor + "</td></tr>";
            TotalFactura += data[x].abonado * 1;
            
            if (data[x].efectivo * 1 > 0) {
                $("#Efectivo").val(formato_numero(data[x].efectivo, 0, ".", ","));
                Efectivo = data[x].efectivo;
            }
            if (data[x].tarjeta * 1 > 0) {
                $("#TTarjeta").val(formato_numero(data[x].tarjeta, 0, ".", ","));
                Tarjeta = data[x].tarjeta;
            }
            if (data[x].cheque * 1 > 0) {
                $("#TCheque").val(formato_numero(data[x].cheque, 0, ".", ","));
                Cheque = data[x].cheque;
            }
            if (data[x].consignacion * 1 > 0) {
                $("#TConsignacion").val(formato_numero(data[x].consignacion, 0, ".", ","));
                Consignado = data[x].consignacion;
            }
                
            if (data[x].descuento * 1 > 0)
                $("#Descuento").val(formato_numero(data[x].descuento, 0, ".", ","));
            if (data[x].retefuente * 1 > 0) {
                $("#RetFuente").val(formato_numero(data[x].retefuente, 0, ".", ","));
                RetFuente = data[x].retefuente;
            }
            if (data[x].reteica * 1 > 0) {
                $("#RetICA").val(formato_numero(data[x].reteica, 0, ".", ","));
                RetICA = data[x].reteica;
            }
            if (data[x].reteiva * 1 > 0) {
                $("#RetIVA").val(formato_numero(data[x].reteiva, 0, ".", ","));
                RetIVA = data[x].reteiva;
            }
            if (data[x].retecree * 1 > 0)
                $("#RetCREE").val(formato_numero(data[x].retecree, 0, ".", ","));
            if (data[x].oegreso * 1 > 0) {
                $("#OEgreso").val(formato_numero(data[x].oegreso, 0, ".", ","));
                OEgreso = data[x].oegreso;
            }
            if (data[x].oingreso * 1 > 0) {
                $("#OIngreso").val(formato_numero(data[x].oingreso, 0, ".", ","));
                OIngreso = data[x].oingreso;
            }
            if (data[x].diferenciamenor * 1 > 0) {
                $("#DiferenciaMenor").val(formato_numero(data[x].diferenciamenor, 0, ".", ","));
                DiferenciaMenor = data[x].diferenciamenor;
            }
        }
    }

    var data = JSON.parse(datos[1]);
    for (var x = 0; x < data.length; x++) {
        switch (data[x].tipopago) {
            case "CONSIGNACION":
                MCuenta = data[x].idcuenta;
            default:
        }
    }

    $("#bodyfacturacion").html(resultado);
    $("#TFactura").val(formato_numero(TotalFactura, 0, ".", ","));
    $("#STotal").html(formato_numero(TotalFactura, 0, ".", ","));

    ContabilizarRecibo();

}

function ImprimirMovil() {
    if (NRecibo > 0) {
        ActivarLoad();
        setTimeout(function () {
            var datos = LlamarAjax("Caja","opcion=GenerarReciboTexto&recibo=" + NRecibo);
            DesactivarLoad();
            datos = datos.split("|");
            if (datos[0] == "0") {
                window.open("Documpdf/" + datos[1])
            } else {
                swal(ValidarTraduccion(ValidarTraduccion("Acción Cancelada")), datos[1], "error");
            }
        }, 15);
    }
}

function ImprimirRecibo() {
    if (NRecibo > 0) {
        ActivarLoad();
        setTimeout(function () {
            var datos = LlamarAjax("Caja","opcion=ImprimirRecibo&recibo=" + NRecibo);
            DesactivarLoad();
            datos = datos.split("|");
            if (datos[0] == "0") {
                window.open("Documpdf/" + datos[1])
            } else {
                swal(ValidarTraduccion(ValidarTraduccion("Acción Cancelada")), datos[1], "error");
            }
        }, 15);
    }
}

function EnviarRecibo() {
    if (NRecibo > 0) {
        swal({
            title: 'Correo Electrónico',
            text: NombreCliente,
            input: 'email',
            showCancelButton: true,
            confirmButtonText: 'Enviar',
            showLoaderOnConfirm: true,
            inputValue: Cli_Correo,
            preConfirm: (email) => {
                return new Promise((resolve, reject) => {

                    setTimeout(() => {

                        var datos = LlamarAjax("Caja","opcion=ImprimirRecibo&recibo=" + NRecibo);
                        DesactivarLoad();
                        datos = datos.split("|");
                        var Archivo = "";
                        if (datos[0] == "0") {
                            Archivo = datos[1];
                        } else {
                            reject(datos[1]);
                        }

                        var parametros = "recibo=" + NRecibo + "&e_email=" + email + "&archivo=" + Archivo + "&cliente=" + NombreCliente + "&codigo=" + IdCliente;
                        var datos = LlamarAjax("Caja","opcion=EnvioRecibo&"+ parametros);
                        datos = datos.split("|");
                        if (datos[0] == "0") {
                            resolve()
                        } else {
                            reject(datos[1]);
                        }
                           
                    }, 15);
                    
                })
            },
            allowOutsideClick: () => !swal.isLoading()
        }).then((result) => {
            if (result) {
                
                swal({
                    type: 'success',
                    title: 'Recibo Nro ' + NRecibo +  ' Enviado',
                    html: 'Correo Electrónico: ' + result
                })
            }
        })
    }
}

function TablaFacturasPendientes() {
    var parametros = "cliente=" + IdCliente + "&tipo=1";
    var datos = LlamarAjax('Caja','opcion=FacturasPendientes&'+ parametros);
    var resultado = "";
    var clase = "";
    var diavencido = "";
    TotalFactura = 0;
    if (datos != "[]") {
        eval("data=" + datos);
        for (var x = 0; x < data.length; x++) {
            clase = ""
            diavencido = "";
            if (data[x].dias * 1 > 0) {
                clase = "class='text-danger text-bold'";
                diavencido = "<br>(" + data[x].dias + ") días";
            }
                
            resultado += "<tr " + clase + " onclick = 'SeleccionarFila(" + x + "," + data[x].saldo + "," + data[x].factura + "," + (data[x].subtotal - data[x].descuento) + "," + data[x].iva + "," + (data[x].total - data[x].saldo) + ")' id = 'tr-" + x + "' > " +
                "<td><a class='text-XX' href='javascript:ImprimirFactura(" + data[x].factura + ")'>" + (x + 1) + "</a></td>" +
                "<td><div name='TablaFactura[]'>" + data[x].factura + "</div></td>" +
                "<td>" + data[x].fecha + "</td>" +
                "<td>" + data[x].vencimiento + diavencido + "</td>" +
                "<td align='right'><div>" + formato_numero(data[x].subtotal, 0, ".", ",") + "</div></td>" + 
                "<td align='right'>" + formato_numero(data[x].descuento, 0, ".", ",") + "</td>" +
                "<td align='right'><div>" + formato_numero(data[x].iva, 0, ".", ",") + "</div></td>" + 
                "<td align='right' class='bg-celeste'><div name='TablaValor[]'>" + formato_numero(data[x].total, 0, ".", ",") + "</div></td>" + 
                "<td align='right'>" + formato_numero(data[x].total - data[x].saldo, 0, ".", ",") + "</td>" +
                "<td align='right' class='bg-celeste'><div class='text-XX'>" + formato_numero(data[x].saldo, 0, ".", ",") + "</div><input type='hidden' value='0' name='selecc[]' id='selecc" + x + "'/></td>" + 
                "<td align='right' class='bg-gris'>" + formato_numero(data[x].retefuente, 0, ".", ",") + "</td>" +
                "<td align='right' class='bg-gris'>" + formato_numero(data[x].reteiva, 0, ".", ",") + "</td>" +
                "<td align='right' class='bg-gris'>" + formato_numero(data[x].reteica, 0, ".", ",") + "</td>" +
                "<td align='center'>" + data[x].asesor + "</td></tr>";
            TotalFactura += data[x].saldo * 1;
        }
    }
    $("#bodyfacturacion").html(resultado);
    $("#TFactura").val(formato_numero(TotalFactura, 0, ".", ","));
    $("#SFactura").val(formato_numero(TotalFactura, 0, ".", ","));
    

    CalcularTotal('No');

}

/*$("#VRecaudado, #Descuento, #OIngreso, #OEgreso, #RetFuente, #RetIVA, #RetCREE, #RetICA").keypress(function (e) {
    if (TotalSeleccionado > 0)
        return false;
})*/


function ImprimirFactura(factura) {
    ActivarLoad();
    setTimeout(function () {
        var parametros = "factura=" + factura;
        var datos = LlamarAjax("Facturacion","opcion=RpFactura&"+ parametros);
        DesactivarLoad();
        datos = datos.split("|");
        if (datos[0] == "0") {
            window.open("DocumPDF/" + datos[1]);
        } else {
            swal("", ValidarTraduccion(datos[1]), "warning");
        }
    }, 15);
}

function AgregarRetencio() {
    var tipo = $("#RTipoRetencion").val() * 1;
    switch (tipo) {
        case 1: //IVA
            RetIVA = NumeroDecimal($("#RRetencion").val())
            $("#RetIVA").val($("#RRetencion").val());
            CliReteIva = $("#RPorcentaje").val() * 1;
            porreteiva = LlamarAjax("Compras","opcion=PorcentajeRetencion&retencion=" + CliReteIva) * 1;
            break;
        case 2: //FUENTE
            RetFuente = NumeroDecimal($("#RRetencion").val())
            $("#RetFuente").val($("#RRetencion").val());
            CliReteFuente = $("#RPorcentaje").val() * 1;
            porretefuente = LlamarAjax("Compras","opcion=PorcentajeRetencion&retencion=" + CliReteFuente) * 1;
            break;
        case 3: //ICA
            RetICA = NumeroDecimal($("#RRetencion").val())
            $("#RetICA").val($("#RRetencion").val());
            CliReteIca = $("#RPorcentaje").val() * 1;
            porreteica = LlamarAjax("Compras","opcion=PorcentajeRetencion&retencion=" + CliReteIca) * 1;
            break;
    }
    CalcularTotal('No');
    ContabilizarRecibo();
    $("#modalRetencion").modal("hide");
}

function CalcularRetencion(Caja) {
    if (Caja)
        ValidarTexto(Caja, 3);

    var retencion = NumeroDecimal($("#RPorcentaje").val());
    var porcentaje = LlamarAjax("Compras","opcion=PorcentajeRetencion&retencion=" + retencion) * 1;
    var valor = NumeroDecimal($("#RSubTotal").val());
    $("#RRetencion").val(formato_numero(Math.trunc(valor * porcentaje / 100), 0, _CD, _CM, ""));
}

function CambioRetencionCaja() {
    CalcularRetencion();
}

function LlamarRetencion(tipo, descripcion) {
    if (NRecibo > 0)
        return false;
    if (TotalSeleccionado > 0) {

        switch (tipo) {

            case 1: //IVA
                $("#RSubTotal").val(formato_numero(TotalSelecIva, _DE, _CD, _CM, ""));
                $("#RPorcentaje").html(CargarCombo(90, 7, "", "ReteIVA"));
                $("#RPorcentaje").val(CliReteIva).trigger("change");
                $("#RPorCliente").html(porreteiva);
                break;
            case 2: //FUENTE
                $("#RSubTotal").val(formato_numero(TotalSelSubtotal, _DE, _CD, _CM, ""));
                $("#RPorcentaje").html(CargarCombo(90, 7, "", "ReteFuente"));
                $("#RPorcentaje").val(CliReteFuente).trigger("change");
                $("#RPorCliente").html(porretefuente);
                break;
            case 3: //ICA
                $("#RSubTotal").val(formato_numero(TotalSelSubtotal, _DE, _CD, _CM, ""));
                $("#RPorcentaje").html(CargarCombo(90, 7, "", "ReteICA"));
                $("#RPorcentaje").val(CliReteIca).trigger("change");
                $("#RPorCliente").html(porreteica);
                break;
        }
        $("#RTipoRetencion").val(tipo);
        $("#desretencion").html(descripcion);
        $("#modalRetencion").modal("show");
    }
}


$("#VRecaudado, #Descuento, #OIngreso, #OEgreso, #RetFuente, #RetIVA, #RetCREE, #RetICA").blur(function (e) {
    if (TotalSeleccionado > 0)
        switch (this.id) {
            case "VRecaudado":
                $("#VRecaudado").val(formato_numero(Recaudado, 0, ".", ","));
                break;
            case "Descuento":
                $("#Descuento").val(formato_numero(Descuento, 0, ".", ","));
                break;
            case "OIngreso":
                $("#OIngreso").val(formato_numero(OIngreso, 0, ".", ","));
                break;
            case "OEgreso":
                $("#OEgreso").val(formato_numero(OEgreso, 0, ".", ","));
                break;
            case "RetFuente":
                $("#RetFuente").val(formato_numero(RetFuente, 0, ".", ","));
                break;
            case "RetICA":
                $("#RetICA").val(formato_numero(RetICA, 0, ".", ","));
                break;
            case "RetCREE":
                $("#RetCREE").val(formato_numero(RetCREE, 0, ".", ","));
                break;
            case "RetIVA":
                $("#RetIVA").val(formato_numero(RetIVA, 0, ".", ","));
                break;
        }
        
})

function ValidarTextoCaja(caja, tipo) {
    if ((caja.id == "VRecaudado") && ((Abonado > 0) || (TotalSeleccionado > 0))) {

        $("#VRecaudado").val(formato_numero(Recaudado, 0, ".", ","));
        return false;
    }

    if (caja.id == "VRecaudado")
        Recaudado = NumeroDecimal(caja.value);
        
    ValidarTexto(caja, tipo);
    CalcularTotal(caja);
        
}

function CargarModaltarjeta() {
    if (NRecibo > 0)
        return false
    if ($("#NombreCliente").html() == "")
        return false;
    if (Recaudado > 0)
        $("#ModalTarjeta").modal("show");
    else {
        $("#VRecaudado").focus();
        $.jGrowl(ValidarTraduccion("Debe de ingresar el valor a recaudar"), { life: 4000, theme: 'growl-warning', header: '' });
    }
}

function CargarModalCheque() {
    if (NRecibo > 0)
        return false
    if ($("#NombreCliente").html() == "")
        return false;
    if (Recaudado > 0) {
        LimpiarCheque();
        CargarTablaCheque();
        $("#ModalCheque").modal("show");
    }else {
        $("#VRecaudado").focus();
        $.jGrowl(ValidarTraduccion("Debe de ingresar el valor a recaudar"), { life: 4000, theme: 'growl-warning', header: '' });
    }
}

function CargarVerPagos(tipo, forma) {
    
    if (NRecibo == 0)
        return false;
    if ((tipo == "C") && (NumeroDecimal($("#TCheque").val()) * 1 == 0))
        return false;
    if ((tipo == "T") && (NumeroDecimal($("#TTarjeta").val()) * 1 == 0))
        return false;
    if ((tipo == "CO") && (NumeroDecimal($("#TConsignacion").val()) * 1 == 0))
        return false;
    if ((tipo == "A") && (NumeroDecimal($("#Anticipo").val()) * 1 == 0))
        return false;

    $("#VerChequeRecibo").html(LlamarAjax("Caja","opcion=FormaPagoRecibo&recibo=" + NRecibo + "&tipo=" + tipo))
    $("#TituloVerPagos").html(forma);
    $("#ModalVerCheque").modal("show");
    
}

function CargarModalConsig() {
    if (NRecibo > 0)
        return false
    if ($("#NombreCliente").html() == "")
        return false;
    if (Recaudado == 0) {
        $("#VRecaudado").focus();
        $.jGrowl(ValidarTraduccion("Debe de ingresar el valor a recaudar"), { life: 4000, theme: 'growl-warning', header: '' });
        return false;
    }

    
        
    if (Abonado == 0 || Abonado == Consignado) {
        $("#ModalConsignacion").modal("show");
        if (Consignado == 0)
            Consignado = Recaudado;
            
        $("#Consignacion").val(formato_numero(Consignado, "0", ".", ",", ""))
        CalcularTotal('No');
        $("#ModalConsignacion").modal("show");
        
    } else {
        swal(ValidarTraduccion(ValidarTraduccion("Acción Cancelada")), ValidarTraduccion("La consignación debe de ser por el monto total a recaudar "), "warning")
    }
}

function CalcularTotal(caja) {
    Diferencia = 0;
    RetIVA = NumeroDecimal(c_RetIVA.value) * 1;
    RetCREE = NumeroDecimal(c_RetCREE.value) * 1;
    RetFuente = NumeroDecimal(c_RetFuente.value) * 1;
    RetICA = NumeroDecimal(c_RetICA.value) * 1;
    Descuento = NumeroDecimal(c_Descuento.value) * 1;

    OIngreso = NumeroDecimal(c_OIngreso.value) * 1;
    OEgreso = NumeroDecimal(c_OEgreso.value) * 1;

    if (Recaudado > 0) {
        Efectivo = NumeroDecimal(c_Efectivo.value) * 1;

        DiferenciaMenor = NumeroDecimal(c_DiferenciaMenor.value) * 1;
        Tarjeta = NumeroDecimal(c_Tarjeta.value) * 1;
        Cheque = NumeroDecimal(c_Cheque.value) * 1;
        Consignado = NumeroDecimal(c_Consignado.value) * 1;
        TotalCheques = NumeroDecimal($("#chTotales").html()) * 1;
        if (isNaN(TotalCheques))
            TotalCheques = 0;
        Abonado = Tarjeta + Efectivo + Consignado + Cheque + TotalCheques - EditTotalCheques;
        Recaudado = NumeroDecimal(c_VRecaudado.value) * 1;

        SaldoFac = Recaudado + Anticipo - TotalSeleccionado + RetIVA + RetCREE + RetFuente + RetICA + Descuento + OEgreso - OIngreso - DiferenciaMenor;
        Diferencia = TotalSeleccionado - Recaudado - Anticipo - RetIVA - RetCREE - RetFuente - RetICA - Descuento - OEgreso + OIngreso + DiferenciaMenor;

        //$.jGrowl(TotalSeleccionado + " - " + Recaudado + " - " +  Anticipo  + " - " + RetIVA + " - " + RetCREE + " - " +  RetFuente + " - " + RetICA + " - " + Descuento + " - " +  OEgreso + " + " + OIngreso, { life: 4000, theme: 'growl-warning', header: '' });

        if (SaldoFac < 0)
            SaldoFac = 0;
        SaldoAbo = Recaudado - Abonado;
        if (caja != 'No') {
            if (SaldoAbo < 0) {
                $.jGrowl(ValidarTraduccion("Lo abonado en") + " " + caja.id + " " + ValidarTraduccion("no puede ser mayor a lo recaudado"), { life: 4000, theme: 'growl-warning', header: '' });
                caja.value = "";
                CalcularTotal(caja)
            }
        }

        Abonado = Tarjeta + Efectivo + Consignado + TotalCheques;
        SaldoAbo = Recaudado - Abonado;

    } else {

        Efectivo = 0;

        DiferenciaMenor = 0;
        Tarjeta = 0;
        Cheque = 0;
        Consignado = 0;
        TotalCheques = 0;
        Abonado = 0;
        Recaudado = 0;

        SaldoFac = 0;
        Diferencia = 0;

        SaldoAbo = 0;
    }
      
   
   
    $("#VRecibido").val(formato_numero(Abonado, 0, ".", ","));
    $("#SFactura").val(formato_numero(SaldoFac, 0, ".", ","));
    $("#SPagar").val(formato_numero(SaldoAbo, 0, ".", ","));
    $("#TTarjeta").val($("#Tarjeta").val());
    $("#TCheque").val(formato_numero(TotalCheques, "0", ".", ",", ""));
    $("#SDiferencia").val(formato_numero(Diferencia, "0", ".", ",", ""));
    $("#TConsignacion").val($("#Consignacion").val())

    
    
}


function LimpiarConsignacion() {

    $("#con_fecha").val("");
    $("#con_tipo").val("").trigger("change");
    $("#con_cuenta").val("").trigger("change");
    $("#con_numero").val("");
    $("#Consignacion").val("");
    CalcularTotal('No');
}

function AplicarDiferencia() {
    var diferencia = NumeroDecimal($("#SDiferencia").val());
    if (IdCliente == 0 || NRecibo > 0 || diferencia >= 0)
        return false;
    OIngreso = diferencia * -1;
    Diferencia = 0;
    $("#OIngreso").val(formato_numero(OIngreso, "0", ".", ",", ""));
    CalcularTotal('No');
    ContabilizarRecibo();
}

function QuitarDiferencia() {
    if (IdCliente == 0 || NRecibo > 0 || OIngreso == 0)
        return false;
    OIngreso = 0;
    $("#OIngreso").val(formato_numero(OIngreso, "0", ".", ",", ""));
    CalcularTotal('No');
    ContabilizarRecibo();
}

//$.connection.hub.start().done(function () {

    $('#BtnGuardarRecibo').click(function () {

        var numtarj = $("#tnumero").val() * 1;
        var valtarj = NumeroDecimal($("#Tarjeta").val()) * 1;
        var tiptarj = $("#ttipo").val() * 1;
        var auttarj = $.trim($("#tautoriza").val());
        var con_fecha = $("#con_fecha").val();
        var con_tipo = $("#con_tipo").val();
        var con_cuenta = $("#con_cuenta").val();
        var con_numero = $("#con_numero").val() * 1;
        var Observacion = $.trim($("#Observacion").val());

        if (Consignado > 0) {
            if (con_cuenta == "") {
                $("#ModalConsignacion").modal("show");
                $("#con_cuenta").focus();
                swal(ValidarTraduccion(ValidarTraduccion("Acción Cancelada")), ValidarTraduccion("Debe de Seleccionar la Cuenta a Consignar"), "error");
                return false
            }

            if (con_numero == 0) {
                $("#ModalConsignacion").modal("show");
                $("#con_numero").focus();
                swal(ValidarTraduccion(ValidarTraduccion("Acción Cancelada")), "Debe de Ingresar el número de control de la consignación", "error");
                return false
            }

            if (con_fecha == "") {
                $("#ModalConsignacion").modal("show");
                $("#con_fecha").focus();
                swal(ValidarTraduccion(ValidarTraduccion("Acción Cancelada")), ValidarTraduccion("Debe de Ingresar la Fecha de Consignación"), "error");
                return false
            }
        }

        if (valtarj > 0) {
            if (tiptarj == 0) {
                $("#ModalTarjeta").modal("show");
                swal(ValidarTraduccion(ValidarTraduccion("Acción Cancelada")), ValidarTraduccion("Debe de ingresar la entidad de la tarjeta"), "error");
                return false
            }
            if ($.trim(numtarj).length < 4) {
                $("#ModalTarjeta").modal("show");
                swal(ValidarTraduccion(ValidarTraduccion("Acción Cancelada")), ValidarTraduccion("El número de la tarjeta debe ser de 4 dígistos mínimos"), "error");
                return false
            }
            if (auttarj == "") {
                $("#ModalTarjeta").modal("show");
                swal(ValidarTraduccion(ValidarTraduccion("Acción Cancelada")), ValidarTraduccion("Debe de ingresar la autorización de la tarjeta"), "error");
                return false
            }
        }


        if (($("#NombreCliente").html() == "") || (NRecibo != 0))
            return false;

        if ($("#Concepto").val() == "") {
            $("#Concepto").focus();
            swal(ValidarTraduccion("Acción Cancelada"), ValidarTraduccion("Debe de ingresar el concepto del recibo"), "warning");
            return false;
        }


        if (Recaudado <= 0 && Anticipo == 0) {
            $("#VRecaudado").focus();
            swal(ValidarTraduccion("Acción Cancelada"), ValidarTraduccion("Debe de ingresar el monto recaudado o anticipo"), "warning");
            return false;
        }


        if (Math.abs(SaldoAbo) > 0) {
            swal(ValidarTraduccion("Acción Cancelada"), ValidarTraduccion("Debe de completar el saldo por pagar"), "warning");
            return false;
        }

        if (Diferencia <= -1) {
            swal(ValidarTraduccion("Acción Cancelada"), ValidarTraduccion("No puede haber diferencia negativa en el comprobante de pago"), "warning");
            return false;
        }

        if (SaldoFac > 1 && Facturas != "") {
            swal(ValidarTraduccion("Acción Cancelada"), "Debe de completar el saldo por abonar a factura(s)", "warning");
            return false;
        } else {
            if (SaldoFac > 1) {
                var a_fac = document.getElementsByName("TablaFactura[]");
                var a_val = document.getElementsByName("TablaValor[]");
                for (var x = 0; x < a_fac.length; x++) {
                    if (SeleccionarFila(x, (NumeroDecimal(a_val[x].innerHTML) * 1), a_fac[x].innerHTML,0) == false) {
                        break;
                    }
                }
            }
        }

        var cuenta = document.getElementsByName("Cuentas[]");
        var debito = document.getElementsByName("Debitos[]");
        var credito = document.getElementsByName("Creditos[]");
        var concepto = document.getElementsByName("Conceptos[]");

        var a_cuenta = "array[";
        var a_debito = "array[";
        var a_credito = "array[";
        var a_concepto = "array[";
        
        for (var x = 0; x < cuenta.length; x++) {
            if (cuenta[x].value * 1 == 0) {
                DesactivarLoad();
                swal("Acción Cancelada", "Todas las cuentas contables deben de estar asignadas", "warning");
                return false;
            }

            if (a_cuenta != "array[") {
                a_cuenta += ",";
                a_debito += ",";
                a_credito += ",";
                a_concepto += ",";
            }

            a_cuenta += cuenta[x].value;
            a_debito += debito[x].value;
            a_credito += credito[x].value;
            a_concepto += "'" + concepto[x].value + "'";

        }

        a_cuenta += "]";
        a_debito += "]";
        a_credito += "]";
        a_concepto += "]";

        var diferencia = NumeroDecimal($("#TDiferencia").html());
        if (diferencia != 0) {
            DesactivarLoad();
            swal("Acción Cancelada", "El comprobante contable se encuentra descuadrado", "warning");
            return false;
        }

        var tdebito = NumeroDecimal($("#TDebito").html());
        var tcredito = NumeroDecimal($("#TCredito").html());
                       

        var numanticipo = document.getElementsByName("NumAnticipo[]");
        var monanticipo = document.getElementsByName("UsarAnticipo[]");
        var salanticipo = document.getElementsByName("SaldoAnticipo[]");

        var a_numanticipo = "array[";
        var a_monanticipo = "array[";
        var a_salanticipo = "array[";

        for (var x = 0; x < numanticipo.length; x++) {

            if (a_numanticipo != "array[") {
                a_numanticipo += ",";
                a_monanticipo += ",";
                a_salanticipo += ",";
            }

            a_numanticipo += numanticipo[x].value;
            a_monanticipo += NumeroDecimal(monanticipo[x].value);
            a_salanticipo += NumeroDecimal(salanticipo[x].value);

        }

        if (a_numanticipo == "array[") {
            a_numanticipo += "0]";
            a_monanticipo += "0]";
            a_salanticipo += "0]";
        } else {
            a_numanticipo += "]";
            a_monanticipo += "]";
            a_salanticipo += "]";
        }

        ActivarLoad();
        setTimeout(function () {
            var parametros = "Cliente=" + IdCliente + "&Facturas=" + Facturas + "&Concepto=" + $("#Concepto").val() + "&Recaudado=" + Recaudado + "&Efectivo=" + Efectivo + 
                "&RetIVA=" + RetIVA + "&RetCRE=" + RetCREE + "&RetICA=" + RetICA + "&RetFuente=" + RetFuente + "&Descuento=" + Descuento + "&OIngresos=" + OIngreso + "&OEgresos=" + OEgreso +
                "&numtarj=" + numtarj + "&valtarj=" + valtarj + "&tiptarj=" + tiptarj + "&auttarj=" + auttarj + "&Guia=" + Guia + "&DiferenciaMenor=" + DiferenciaMenor + 
                "&confecha=" + con_fecha + "&contipo=" + con_tipo + "&concuenta=" + con_cuenta + "&connumero=" + con_numero + "&Consignacion=" + Consignado +
                "&NombreCliente=" + NombreCliente + "&Movimiento=" + Movimiento + "&Observacion=" + Observacion + "&Anticipo=" + Anticipo +
                "&NumAnticipo=" + a_numanticipo + "&MontoAnticipo=" + a_monanticipo + "&SaldoAnticipo=" + a_salanticipo +
                "&a_cuenta=" + a_cuenta + "&a_debito=" + a_debito + "&a_credito=" + a_credito + "&a_concepto=" + a_concepto + "&tdebito=" + tdebito + "&tcredito=" + tcredito;
            var datos = LlamarAjax("Caja","opcion=GuardarRecibo&"+ parametros);
            DesactivarLoad();
            datos = datos.split("|");
            if (datos[0] == "0") {
                NAnticipo = datos[4] * 1;
                swal("", ValidarTraduccion(datos[1]) + " " + datos[2] + (NAnticipo > 0 ? "<br>Anticipo número " + NAnticipo + " generado" : ""), "success");
                NRecibo = datos[2] * 1;

                $("#Recibo").html(NRecibo);
                $("#btnImprimir").removeClass("hidden")
                $("#btnImprimirmov").removeClass("hidden")
                $("#btnEnviar").removeClass("hidden")
                $("#btnAnular").removeClass("hidden")
                
                asesores = datos[5];
                var mensaje = "Buen día Asesor Comercial, El cliente " + NombreCliente + ", a abonado las facturas " + Facturas + ", por un valor recaudado de " + Recaudado;
                //chat.server.send(usuariochat, mensaje, '', asesores);
                EscucharMensaje("Buen día Asesor Comercial, El cliente " + NombreCliente + ", a abonado las facturas " + Facturas);
                ArchivoChat = "";

            } else {
                swal(ValidarTraduccion("Acción Cancelada"), datos[1], "warning");
            }
        }, 15);
    });
    
//});

function ModalRecibo() {
    $("#modalConsultarRecibos").modal("show");
}

function BuscarRecibo(recibo, cliente) {

    $("#modalConsultarRecibos").modal("hide");
    ActivarLoad();
    setTimeout(function () {
        $("#Cliente").val(cliente);
        $("#Recibo").html(recibo);
        IdCliente = 0;
        DocumentoCli = "";
        BuscarCliente(cliente, recibo);
        DesactivarLoad();
        $("#btnImprimir").removeClass("hidden");
        $("#btnImprimirmov").removeClass("hidden");
        $("#btnEnviar").removeClass("hidden");
        $("#BtnGuardarRecibo").addClass("hidden");

        $("#RetIVA").prop("disabled",true);
        $("#RetCREE").prop("disabled", true);
        $("#RetICA").prop("disabled", true);
        $("#RetFuente").prop("disabled", true);
        $("#Efectivo").prop("disabled", true);
        $("#DiferenciaMenor").prop("disabled", true);
        $("#Descuento").prop("disabled", true);
        $("#OEgreso").prop("disabled", true);
        $("#VRecaudado").prop("disabled", true);
        $("#Concepto").prop("disabled", true);
    }, 15);

}


function CargarModalRecibos() {

    var recibo = $("#BNRecibo").val() * 1;
    var documento = $("#BDocumento").val() * 1;
    var factura = $("#BFactura").val() * 1;
    var cliente = $.trim($("#BCliente").val());
    var estado = $("#BEstado").val();
    var fechad = $("#BFechaDesde").val();
    var fechah = $("#BFechaHasta").val();
    var dias = $("#BUltimoDia").val() * 1;

    if (fechad == "" && fechah == "" && dias == 0) {
        swal("Alerta", "debe de ingresar una fecha o números de días ", "error");
        return false;
    }


    var resultado = '<table class="table display  table-bordered" id="tablamodalrecibos"><thead><tr><th width= "8%" > <span idioma="Nro Recibo">Nro Recibo</span></th><th><span idioma="Código">Cliente</span></th><th width="20%"><span idioma="Cliente">Cliente</span></th><th><span idioma="Fecha">Fecha</span></th><th><span idioma="Abonado">Abonado</span></th><th><span idioma="Anulado">Anulado</span></th><th><span idioma="Total">Total</th></tr></thead><tbody>';
    ActivarLoad();

    setTimeout(function () {
        var parametros = "recibo=" + recibo + "&factura=" + factura + "&documento=" + documento + "&cliente=" + cliente + "&estado=" + estado + "&fechad=" + fechad + "&fechah=" + fechah + "&dias=" + dias;
        var datos = LlamarAjax('Caja','opcion=ModalRecibos&'+ parametros);
        DesactivarLoad();
        var datajson = JSON.parse(datos);
        $('#tablamodalrecibos').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "caja" },
                { "data": "documento" },
                { "data": "cliente" },
                { "data": "fecha" },
                { "data": "estado" },
                { "data": "recaudado", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "usuario" },
                { "data": "anulado" },
                { "data": "fechaenvio" },
                { "data": "total" }
            ],

            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy','colvis'
            ]
        });
    }, 15);
}

$("#tablamodalrecibos > tbody").on("dblclick", "tr", function () {
    var row = $(this).parents("td").context.cells;
    var recibo = row[0].innerText;
    var cliente = row[1].innerText;
    LimpiarTodo();
    BuscarRecibo(recibo, cliente); 
});


$("#tablamodalclientecaj > tbody").on("click", "tr", function (e) {
    var row = $(this).parents("td").context.cells;
    var numero = row[1].innerText;
    $("#modalClientesCaj").modal("hide");
    $("#Cliente").val(numero);
    BuscarCliente(numero, 0);
});

function ValidarCliente(documento) {

    if (ErrorCli == 0) {
        if ($.trim(DocumentoCli) != "") {
            if (DocumentoCli != documento) {
                DocumentoCli = "";
                Limpiar();
            }
        }
    } else {
        ErrorCli = 0;
    }
    
}


function BuscarCliente(documento, recibo) {

        
    NRecibo = recibo;
    $("#Recibo").html(recibo);

    if ($.trim(documento) == "")
        return false;

    if (DocumentoCli == documento)
        return false;

    DocumentoCli = documento;

    ActivarLoad();

    setTimeout(function () {

        var parametros = "documento=" + documento;
        var datos = LlamarAjax('Cotizacion','opcion=BuscarCliente&'+ parametros);
        datos = datos.split("|");
        DesactivarLoad();
        
        if (datos[0] == "0") {
            eval("data=" + datos[1]);
            IdCliente = data[0].id * 1;

            PlazoPago = data[0].plazopago;
            TipoCliente = data[0].tipocliente;
            CorreoEmpresa = data[0].email;

            $("#ChNombre").val(data[0].nombrecompleto);
            $("#ChNombre").prop("inicial", data[0].nombrecompleto);
            $("#ChCedula").val(data[0].documento);
            $("#ChCedula").prop("inicial", data[0].documento);
            $("#ChTelefono").val((data[0].telefono ? data[0].telefono : "") + ", " + (data[0].celular ? data[0].celular : ""));
            $("#ChTelefono").prop("inicial", (data[0].telefono ? data[0].telefono : "") + ", " + (data[0].celular ? data[0].celular : ""));
            $("#ChDireccion").val(data[0].direccion);
            $("#ChDireccion").prop("inicial", data[0].direccion);
            $("#Moneda").html("Pesos");

            CliReteIva = data[0].reteiva*1;
            CliReteIca = data[0].reteica * 1;
            CliReteFuente = data[0].retefuente * 1

            porretefuente = data[0].porretefuente * 1;
            porreteica = data[0].porreteica * 1;
            porreteiva = data[0].porreteiva * 1

            Cli_Correo = (data[0].email ? data[0].email : "");
            $("#NombreCliente").html(data[0].nombrecompleto + "<br>" + data[0].direccion + " (" + data[0].ciudad + ") Tel: " + (data[0].telefono ? data[0].telefono : "") + ", " + (data[0].celular ? data[0].celular : "") + ", " + Cli_Correo);
            NombreCliente = data[0].nombrecompleto;
            if (NRecibo == 0)
                TablaFacturasPendientes();
            else
                TablaFacturasRecibo();

            $("#SaldoClienteDev").html(SaldoTotal(IdCliente));

            if (NRecibo == 0)
                CargarModalAnticipo(0);

        } else {
            ErrorCli = 1;
            $("#Cliente").select();
            $("#Cliente").focus();
            swal("Acción Cancelada", "Cliente no Registrado", "warning");
            IdCliente = 0;
        }
        
    }, 15);
}


$("#MTablaMovimiento > tbody").on("dblclick", "tr", function () {
    var row = $(this).parents("td").context.cells;
    Movimiento = row[0].innerText;
    var descripcion = "<b>Fecha:</b> " + row[1].innerText + ", <b>Referencia:</b> " + row[2].innerText + ", <b>Descripción:</b> " + row[3].innerText + ", <b>Monto:</b> " + row[5].innerText;
    $("#Detalle_Movimiento").html(descripcion);
    Recaudado = NumeroDecimal(row[5].innerText);
    Consignado = NumeroDecimal(row[5].innerText);
    $("#VRecaudado").val(row[5].innerText)
    $("#con_cuenta").val($("#MCuenta").val()).trigger("change");
    $("#con_numero").val(row[2].innerText);
    $("#con_fecha").val(row[1].innerText);
    $("#con_tipo").val("E").trigger("change");
    $("#ModalMovBancario").modal("hide");
    CargarModalConsig();
    ContabilizarRecibo();
});

function VerEstadoCuenta() {
    EstadoCuenta(IdCliente, NombreCliente, CorreoEmpresa, TipoCliente, PlazoPago, CorreoEmpresa);
}

function ActualizarRetencio() {
    var tipo = $("#RTipoRetencion").val() * 1;
    var idretencion = $("#RPorcentaje").val() * 1;
    var datos = LlamarAjax("Facturacion","opcion=ActualizarRetencion&tipo=" + tipo + "&idretencion=" + idretencion + "&cliente=" + IdCliente).split("|");
    if (datos[0] == "0") {
        swal("", datos[1], "success");
    }
    else
        swal("Acción Cancelada", datos[1], "warning");
}

function ContabilizarRecibo() {

    if (IdCliente == 0)
        return false;
        
    ActivarLoad();
    setTimeout(function () {
       
        var banco = $("#con_cuenta").val() * 1;
        if (IdCliente == 0) {
            DesactivarLoad();
            return false;
        }
                            
        var parametros = "&cliente=" + IdCliente + "&recaudado=" + Recaudado + "&reteiva=" + RetIVA + "&retefuente=" + RetFuente + "&reteica=" + RetICA +
            "&deposito=" + Consignado + "&banco_dep=" + banco + "&efectivo=" + Efectivo + "&anticipo=" + Anticipo + "&buscar=" + NRecibo +
            "&idreteiva=" + CliReteIva + "&idretefuente=" + CliReteFuente + "&idreteica=" + CliReteIca + "&facturas=" + Facturas +
            "&descuento=" + Descuento + "&otroegreso=" + OEgreso + "&otroingreso=" + Math.abs(OIngreso) + "&diferencia=" + DiferenciaMenor;
        var datos = LlamarAjax("Caja","opcion=Contabilizar_ReciboCaja"+ parametros).split("||");
        DesactivarLoad();
        if (datos[0] == "0") {
            var data = JSON.parse(datos[1]);
            if (data[0]._error * 1 == 0) {
                
                var resultado = "";

                var tdebito = 0;
                var tcredito = 0;

                for (var x = 0; x < data.length; x++) {
                    resultado += "<tr " + (data[x]._idcuenta * 1 == 0 ? " class='bg-danger' " : "") + ">" +
                        "<td>" + data[x]._cuenta + "</td>" +
                        "<td class='text-right'>" + formato_numero(data[x]._debito, _DE, _CD, _CM, "") + "</td>" +
                        "<td class='text-right'>" + formato_numero(data[x]._credito, _DE, _CD, _CM, "") + 
                        "<input name='Debitos[]' type='hidden' value='" + data[x]._debito + "'>" + 
                        "<input name='Creditos[]' type='hidden' value='" + data[x]._credito + "'>" + 
                        "<input name='Cuentas[]' type='hidden' value='" + data[x]._idcuenta + "'>" + 
                        "<input name='Conceptos[]' type='hidden' value='" + data[x]._concepto + "'></td></tr>";
                    tdebito += data[x]._debito;
                    tcredito += data[x]._credito;
                }

                $("#TDebito").html(formato_numero(tdebito, _DE, _CD, _CM, ""))
                $("#TCredito").html(formato_numero(tcredito, _DE, _CD, _CM, ""))
                $("#TDiferencia").html(formato_numero(tdebito - tcredito, _DE, _CD, _CM, ""))

                $("#TbContabilidadRC").html(resultado);
            } else {
                swal("Acción Cancelada", data[0]._mensaje, "warning");
            }
        } else
            swal("Acción Cancelada", datos[1], "warning");
    }, 15);
}

function AnularContabilizacion() {
    if (Factura == 0)
        return false;
    var resultado = "";
    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: ValidarTraduccion('¿Seguro que desea anular la contabilización de la factura número ' + Factura + '?'),
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Anular'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                $.post(url_servidor+"Facturacion","opcion=AnularContabilizacion&factura=" + Factura)
                    .done(function (data) {
                        datos = data.split("|");
                        if (datos[0] == "0") {
                            resultado = datos[1];
                            resolve();
                        } else {
                            reject(datos[1]);
                        }
                    })
            })
        }
    }]).then(function (result) {
        Estado = "Facturado";
        $("#Estado").val(Estado);
        $("#btneliminarconta").addClass("hidden");
        $("#btncontabilizar").removeClass("hidden");
        BuscarFactura(Factura);
        swal({
            type: 'success',
            html: resultado
        })
    });
} 



$('select').select2();
DesactivarLoad();
