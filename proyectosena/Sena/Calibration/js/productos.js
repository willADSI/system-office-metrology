function productos() {
    var tbody = $('#lista_productos tbody');
    var fila_contenido = tbody.find('tr').first().html();
    //Agregar fila nueva. 
    $('#lista_productos').on('click', '.button_agregar_producto', function () {
        var fila_nueva = $('<tr><td><input id="concepto1" type="text" name="concepto[]" onKeyUp="CalcularTotal(this, 1)" class="nombre" placeholder="CUENTA" /></td><td><input id="concepto2" type="text" name="concepto[]" class="nombre" placeholder="CONCEPTO" onKeyUp="CalcularTotal(this, 2)" /></td><td><input id="concepto3" type="text" name="concepto[]" class="nombre" placeholder="BENEFICIARIO" onKeyUp="CalcularTotal(this, 3)"/></td><td><input id="concepto4" type="text" name="concepto[]" class="nombre" placeholder="DEBITO" onKeyUp="CalcularTotal(this, 4)" /></td>--><td><input id="concepto5" type="text" name="concepto[]" class="nombre" placeholder="CREDITO" onKeyUp="CalcularTotal(this, 5)" /></td><td><input id="concepto6" type="text" name="concepto[]" class="nombre" placeholder="CENTRO" onKeyUp="CalcularTotal(this, 6)" /></td><td><button align="right" type="button" class="btn btn-glow btn-danger button_eliminar_producto"> Eliminar </button></td></tr>');
        var nFilas = $("#lista_productos tr").length;
        var nColumnas = $("#lista_productos tr:last td").length;
        var msg = + nFilas;
        var msg1 = + nColumnas;
        fila_nueva.append(fila_contenido);
        $("#concepto1").attr('id', 'concepto' + msg).attr('name', 'concepto' + '[' + msg + ']').attr('onKeyUp', 'CalcularTotal(this,' + msg + ')');
        tbody.append(fila_nueva);
        var msg = "Filas: " + nFilas + " - Columnas: " + nColumnas;
        //alert(msg);
        //alert(msg1); 

    });


    //Eliminar fila.
    $('#lista_productos').on('click', '.button_eliminar_producto', function () {
        $(this).parents('tr').eq(0).remove();
    });
}

productos();
$('select').select2();
DesactivarLoad();
