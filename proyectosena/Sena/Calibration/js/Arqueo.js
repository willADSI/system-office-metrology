﻿
var permisoprincipal = localStorage.getItem("Menu_Caja") * 1;

if (permisoprincipal == 0) {
    MenuPrincipal();
}


d = new Date();
month = d.getMonth() + 1;
day = d.getDate();

output = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-' +
    (day < 10 ? '0' : '') + day;

$("#CFecha").val(output);

var usuario = localStorage.getItem("codusu");
var fecha = output;
var numero = 0;

$("#CTercero").html(CargarCombo(80, 1));

$("#CNumero").val(numero);

Consultar();

var TVentas = 0;
var TTarjeta = 0;

var ConsignarMO = 0;
var ConsignarLO = 0;
var ConsignarVA = 0;
var ConsignarLC = 0;

var TIngresos = 0;
var TEgresos = 0;
var TOIngreso = 0;
var TOEgresos = 0;
var TConsignar = 0;
var TCCEmpleado = 0;
var TAnticipo = 0;
var TReteICA = 0;
var TReteFuenteCre = 0;
var TReteFuente = 0;
var TAnticipoCre = 0;
var TDevoDesc = 0;
var TSaldoAnt = 0;
var TCajaMenor = 0;

var BPla = ["", "", "", ""];
var BBan = ["", "", "", ""];
var BEfe = [0, 0, 0, 0];
var BChe = [0, 0, 0, 0];

function DetalleRecibo() {
    var parametros = "usuarios=" + usuario
    var datos = LlamarAjax(url+"Arqueo/DetallePaol")
}

function ImprimirArqueo() {
    ActivarLoad();
    setTimeout(function () {
        var datos = LlamarAjax(url + "Arqueo/ImprimirArqueo", "numero=" + numero + "&Usuario=" + usuario);
        datos = datos.split("|");
        if (datos[0] == "0") {
            window.open(url + "Documpdf/" + datos[1])
        } else {
            swal(ValidarTraduccion(ValidarTraduccion("Acción Cancelada")), ValidarTraduccion(datos[1]), "error");
        }
        DesactivarLoad();
    }, 15);

}

function GuardarBanco() {

    var cheque = NumeroDecimal($("#CCheque").val()) * 1;
    var efectivo = NumeroDecimal($("#CEfectivo").val()) * 1;
    var planilla = $.trim($("#CPlanilla").val());
        
    var banco = $("#NroBanco").val() * 1;
        
    if ((efectivo == 0 && cheque == 0) || planilla == "") {
        $("CEfectivo").focus();
        swal(ValidarTraduccion("Acción Cancelada"), ValidarTraduccion("Debe de ingresar todos los valores") + " <br> " + "(" + ValidarTraduccion("efectivo  ó cheque y planilla") + ")", "warning");
        return false;
    }

    BPla[banco] = planilla;
    BEfe[banco] = efectivo;
    BChe[banco] = cheque;



    $("#VBanco" + banco).val(formato_numero(efectivo + cheque, 0,_CD,_CM,""));
    $("#modalBanco").modal("hide");

    CalcularTotal();

}

function CalcularTotal() {

    TIngresos = TVentas + TReteFuenteCre + TAnticipoCre + TOIngreso + TSaldoAnt;
    TEgresos = TAnticipo + TReteFuente + TOEgresos + TCCEmpleado + TCajaMenor + TReteICA + TDevoDesc + BEfe[0] + BEfe[1] + BEfe[2] + BEfe[3] + BChe[0] + BChe[1] + BChe[2] + BChe[3] + TTarjeta;
    TConsignar = TIngresos - TEgresos;
                
    $("#TConsignar").val(formato_numero(TConsignar, 0, ".",",",""));
    $("#TIngresos").val(formato_numero(TIngresos, 0, ".",",",""));
    $("#TEgresos").val(formato_numero(TIngresos, 0, ".",",",""));

}

function GuardarArqueo() {

    var title = ValidarTraduccion("Advertencia");

    if (TConsignar < 0)
        title = ValidarTraduccion("Cierre en Negativo") + " " + formato_numero(TConsignar, 0, ".",",", "");

    var Mensaje = ValidarTraduccion("Seguro que desea realizar el cierre de") + ": " + ValidarTraduccion("Fecha") + ": " + InvertirFecha(fecha, "-") + " \n " + ValidarTraduccion("Usuario") + ": " + usuario;  

    var Parametros = "NoCierre=" + numero + "&AnticiposCre=" + TAnticipoCre + "&RetFueCre=" + TReteFuenteCre +
        "&ReteFuente=" + TReteFuente + "&ReteICA=" + TReteICA + "&Anticipos=" + TAnticipo + "&CxCEmp=" + TCCEmpleado +
        "&PorConsignar=" + TConsignar + "&BEfe=" + BEfe + "&BChe=" + BChe + "&BPla=" + BPla + "&Usuario=" + usuario + "&fecha=" + fecha;  
    
    
    swal.queue([{
        title: title,
        text: '¿' + Mensaje + "?",
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Si'),
        cancelButtonText: ValidarTraduccion('No'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',

        preConfirm: function () {
            return new Promise(function (resolve) {
                $.post(url + "Arqueo/GuardarArqueo", Parametros)
                    .done(function (data) {
                        data = data.split("|");
                        swal.insertQueueStep(ValidarTraduccion(data[1]));
                        if (data[0] * 1 == 0) {
                            $("#Gua_Arqueo").addClass("hidden");
                            $("#Imp_Arqueo").removeClass("hidden");
                            numero = data[2] * 1;
                        }
                        resolve()
                    })
            })
        }
    }])
    
}

function ValidarBanco(caja) {

    ValidarTexto(caja, 1);
    var banco = $("#NroBanco").val()*1;

    var cheque = NumeroDecimal($("#CCheque").val())*1;
    var efectivo = NumeroDecimal($("#CEfectivo").val()) * 1;
        
    $("#CTotal").val(formato_numero(cheque + efectivo, 0,_CD,_CM,""));
        
}

function ValidarArqueo(caja) {

    ValidarTexto(caja, 1);

    switch (caja.id) {
        case "TAnticipoCre":
            TAnticipoCre = NumeroDecimal(caja.value) * 1;
            break;
        case "TReteFuente":
            TReteFuente = NumeroDecimal(caja.value) * 1;
            break;
        case "TAnticipo":
            TAnticipo = NumeroDecimal(caja.value) * 1;
            break;
        case "TReteICA":
            TReteICA = NumeroDecimal(caja.value) * 1;
            break;
        case "TReteFuenteCre":
            TReteFuenteCre = NumeroDecimal(caja.value) * 1;
            break;
        case "TCCEmpleado":
            TCCEmpleado = NumeroDecimal(caja.value) * 1;
            break;
    }
         
    CalcularTotal();
}

function LimpiarTercero() {

    $("#IdTercero").val("");
    $("#TNit").val("");
    $("#TNombre").val("");
    $("#TDocumento").val("NIT").trigger("value");

    $("#TControl").val("");
}

function GuardarTercero() {
    
    var id = $("#IdTercero").val()*1
    var nit = $.trim($("#TNit").val());
    var nombre = $.trim($("#TNombre").val());
    var documento = $.trim($("#TDocumento").val());
    var verificar = $("#TControl").val();

    
    if (nit == "" || nombre == "") {
        $("#TNit").focus();
        swal(ValidarTraduccion("Acción Cancelada"), ValidarTraduccion("Debe de ingresar todos los valores") + " <br> " + "(" + ValidarTraduccion("Nit/Cédula y Nombre Completo") + ")", "warning");
        return false;
    }

    if ((documento != "CC") && (verificar == "")) {
        $("#TControl").focus();
        swal(ValidarTraduccion("Acción Cancelada"), ValidarTraduccion("Debe de ingresar un dígito de control válido"), "warning");
        return false;
    }

    var parametros = "id=" + id + "&Nit=" + nit + "&Nombre=" + nombre + "&opcion=1" + "&Documento=" + documento + "&Verificador=" + verificar;
    var datos = LlamarAjax(url + "Arqueo/RegistroTerceros", parametros);
    datos = datos.split("|");
    if (datos[0] == "0") {
        swal("", ValidarTraduccion(datos[1]), "success");
        $("#IdTercero").val(datos[2]);
        CargarTablaTerceros();
        $("#CTercero").html(CargarCombo(80, 1));
    } else
        swal(ValidarTraduccion("Acción Cancelada"), ValidarTraduccion(datos[1]), "warning");
}

function LimpiarCajaMenor() {
    $("#IdCajaMenor").val("0");
    $("#CTercero").val("").trigger("change");
    $("#CConcepto").val("").trigger("change");
    $("#CFactura").val("");
    $("#CCantidad").val("");
    $("#CValor").val("");
    $("#CIva").val("");
    $("#CReteIca").val("");
    $("#CReteFuente").val("");
}

function GuardarCajaMenor() {

    if (numero > 0) {
        swal(ValidarTraduccion("Acción Cancelada"), ValidarTraduccion("Ya se realizó el cierre para esta fecha y usuario"), "warning");
        return false;
    }

    if ($("#usuario_arquelo").html() == "") {
        swal(ValidarTraduccion("Acción Cancelada"), ValidarTraduccion("No hay movimientos en el arqueo para esta fecha y usuario"), "warning");
        return false;
    }
    
    var id = $("#IdCajaMenor").val() * 1
    var tipo = $("#NumTipRegistro").val() * 1

    var tercero = $("#CTercero").val();
    var concepto = $("#CConcepto").val();
    var factura = $.trim($("#CFactura").val());
    var cantidad = NumeroDecimal($("#CCantidad").val()) * 1;
    var valor = NumeroDecimal($("#CValor").val()) * 1;
    var iva = NumeroDecimal($("#CIva").val()) * 1;
    var reteica = NumeroDecimal($("#CReteIca").val()) * 1;
    var retefuente = NumeroDecimal($("#CReteFuente").val()) * 1;

    var mensaje = "";

    if (tercero == "")
        mensaje += "<br>" + ValidarTraduccion("Ingrese un tercero");
    if (concepto == "")
        mensaje += "<br>" + ValidarTraduccion("Ingrese un concepto");
    if (factura == "")
        mensaje += "<br>" + ValidarTraduccion("Ingrese una factura");
    if (cantidad == 0)
        mensaje += "<br>" + ValidarTraduccion("Ingrese una cantidad");
    if (valor == 0)
        mensaje += "<br>" + ValidarTraduccion("Ingrese un valor");

    if (mensaje != "") {
        swal(ValidarTraduccion("Acción Cancelada"), ValidarTraduccion("Verifique los siguientes campos:<br>") + mensaje, "warning");
        return false;
    }
        
    var parametros = "id=" + id + "&Concepto=" + concepto + "&Tercero=" + tercero + "&opcion=1" + "&Factura=" + factura + "&Cantidad=" + cantidad +
        "&Valor=" + valor + "&IVA=" + iva + "&ReteIca=" + reteica + "&ReteFuente=" + retefuente + "&tipo=" + tipo + "&fecha=" + fecha + "&Usuario=" + usuario;
    var datos = LlamarAjax(url + "Arqueo/RegistroCajaMenor", parametros);
    datos = datos.split("|");
    if (datos[0] == "0") {
        swal("", ValidarTraduccion(datos[1]), "success");
        $("#IdCajaMenor").val(datos[2]);
        CargarTablaCajaMenor(tipo);
        LimpiarCajaMenor();

    } else
        swal(ValidarTraduccion("Acción Cancelada"), ValidarTraduccion(datos[1]), "warning");
}

function CambioDocumento(documento) {
    if (documento == "CC")
        $("#TControl").prop("disabled", true);
    else
        $("#TControl").prop("disabled", false);
}


function CargarTablaTerceros() {
    //TablaTerceros
    datos = LlamarAjax(url + "Arqueo/TablaTerceros", "");
    var datajson = JSON.parse(datos);
    var table = $('#TablaTerceros').DataTable({
        data: datajson,
        bProcessing: true,
        bDestroy: true,
        columns: [
            { "data": "id" },
            { "data": "Documento" },
            { "data": "Nit" },
            { "data": "Verificador" },
            { "data": "Nombre" },
            { "defaultContent": "<button class='editar btn-primary'><i class='glyphicon glyphicon-edit' title='Editar Tercero'></button>" },
            { "defaultContent": "<button class='eliminar btn-danger'><i class='glyphicon glyphicon-remove' title='Eliminar Tercero'></button>" }
        ],

        "language": {
            "url": LenguajeDataTable
        }

    });

    data_editar_tercero("#TablaTerceros tbody", table);
}

var data_editar_tercero = function (tbody, table) {

    $(tbody).on("click", "button.editar", function () {
        var data = table.row($(this).parents("tr")).data();
        $("#TNit").val(data.Nit);
        $("#TNombre").val(data.Nombre);
        $("#IdTercero").val(data.id);
        $("#TDocumento").val(data.Documento).trigger("change");
        $("#TControl").val(data.Verificador);
        return false;
    })

    
    $(tbody).on("click", "button.eliminar", function () {
        var data = table.row($(this).parents("tr")).data();
        swal.queue([{
            title: 'Advertencia',
            text: '¿' + ValidarTraduccion('Desea eliminar el tercero') + ' ' + data.Nombre + '?',
            type: 'question',
            showLoaderOnConfirm: true,
            showCancelButton: true,
            confirmButtonText: ValidarTraduccion('Si'),
            cancelButtonText: ValidarTraduccion('No'),
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            preConfirm: function () {
                return new Promise(function (resolve) {
                    $.post(url + "Arqueo/RegistroTerceros", "id=" + data.id + "&opcion=2")
                        .done(function (data) {
                            data = data.split("|");
                            if (data[0] == "0") {
                                CargarTablaTerceros();
                                $("#CTercero").html(CargarCombo(80, 1));
                            }
                            swal.insertQueueStep(ValidarTraduccion(data[1]));
                            resolve()
                        })
                })
            }
        }])
        return false;
    })

    $(tbody).on("click", "td", function () {
        var data = table.row($(this).parents("tr")).data();
        var tercero = data.Nombre + " (" + data.Nit + (data.Documento == 'CC' ? '' : data.Verificador) + ")";
        $("#CTercero").val(tercero).trigger("change");
        $("#modalTerceros").modal("hide");
    })
}

function CargarTablaCajaMenor(tipo) {
    //TablaTerceros
    $("#CConcepto").html(CargarCombo(81, 1, "", tipo));
    datos = LlamarAjax(url + "Arqueo/TablaCajaMenor", "tipo=" + tipo + "&fecha=" + fecha + "&usuario=" + usuario);
    datos = datos.split("|");
    var datajson = JSON.parse(datos[0]);
    var table = $('#TablaEgresos').DataTable({
        data: datajson,
        bProcessing: true,
        bDestroy: true,
        columns: [
            { "data": "id" },
            { "data": "Concepto" },
            { "data": "Tercero" },
            { "data": "Factura" },
            { "data": "Cantidad" },
            { "data": "Valor" },
            { "data": "IVA" },
            { "data": "ReteICA" },
            { "data": "ReteFuente" },
            { "defaultContent": (numero == 0 ? "<button class='editar btn-primary'><i class='glyphicon glyphicon-edit' title='Editar Tercero'></button>" : "") },
            { "defaultContent": (numero == 0 ? "<button class='eliminar btn-danger'><i class='glyphicon glyphicon-remove' title='Eliminar Tercero'></button>" : "") }
        ],

        "language": {
            "url": LenguajeDataTable
        }

    });

    data_editar_cajamenor("#TablaEgresos tbody", table);

    $("#TotalRCaja").html(formato_numero(datos[1] * 1, 0,_CD,_CM,""));
    switch (tipo) {

        case 1:
            TOIngreso = datos[1] * 1;
            $("#TOIngreso").val(formato_numero(datos[1] * 1, 0,_CD,_CM,""));
            
            break;
        case 2:
            TCajaMenor = datos[1] * 1;
            $("#TCajaMenor").val(formato_numero(datos[1] * 1, 0,_CD,_CM,""));
            
            break;
        case 3:
            TOEgresos = datos[1] * 1;
            $("#TOEgresos").val(formato_numero(datos[1] * 1, 0,_CD,_CM,""));
            
            break;
    }
    CalcularTotal();
}

var data_editar_cajamenor = function (tbody, table) {

    $(tbody).on("click", "button.editar", function () {
        var data = table.row($(this).parents("tr")).data();
        $("#IdCajaMenor").val(data.id);
        $("#CTercero").val(data.Tercero).trigger("change");
        $("#CConcepto").val(data.Concepto).trigger("change");
        $("#CFactura").val(data.Factura);
        $("#CCantidad").val(data.Cantidad);
        $("#CValor").val(formato_numero(data.Valor,0,".",",",""));
        $("#CIva").val(formato_numero(data.IVA, 0,_CD,_CM,""));
        $("#CReteIca").val(formato_numero(data.ReteICA, 0,_CD,_CM,""));
        $("#CReteFuente").val(formato_numero(data.ReteFuente, 0,_CD,_CM,""));
        return false;
    })


    $(tbody).on("click", "button.eliminar", function () {
        var data = table.row($(this).parents("tr")).data();
        var tipo = $("#NumTipRegistro").val() * 1;
        swal.queue([{
            title: 'Advertencia',
            text: '¿' + ValidarTraduccion('Desea eliminar este movimiento') + '?',
            type: 'question',
            showLoaderOnConfirm: true,
            showCancelButton: true,
            confirmButtonText: ValidarTraduccion('Si'),
            cancelButtonText: ValidarTraduccion('No'),
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            preConfirm: function () {
                return new Promise(function (resolve) {
                    $.post(url + "Arqueo/RegistroCajaMenor", "id=" + data.id + "&opcion=2")
                        .done(function (data) {
                            data = data.split("|");
                            if (data[0] == "0") {
                                CargarTablaCajaMenor(tipo);
                                $("#CTercero").html(CargarCombo(80, 1));
                            }
                            swal.insertQueueStep(ValidarTraduccion(data[1]));
                            resolve()
                        })
                })
            }
        }])
        return false;
    })
        
}

function LlamarTerceros() {
    if (numero == 0) {
        CargarTablaTerceros();
        $("#modalTerceros").modal("show");
    }
}

function LlamarRCaja(tipo, descripcion) {

    
    if (usuario == "") {
        swal(ValidarTraduccion("Acción Cancelada"), ValidarTraduccion("No hay movimientos en el arqueo para esta fecha y usuario"), "warning");
        return false;
    }
    
        
    $("#NumTipRegistro").val(tipo);
    CargarTablaCajaMenor(tipo)
    $("#TipoRegistro").html(descripcion);
    $("#modalRCaja").modal("show");

    if (numero > 0) {
        $("#Gua_CajaMenor").addClass("hidden");
        $("#Lim_CajaMenor").addClass("hidden");
    } else {
        $("#Gua_CajaMenor").removeClass("hidden");
        $("#Lim_CajaMenor").removeClass("hidden");
    }

}

function LlamarBanco(banco) {

    if ($("#NoCierre").html() == "")
        return false;

    $("#NroBanco").val(banco);

    if (numero == 0) {
        $("#CEfectivo").prop("readonly", false);
        $("#CCheque").prop("readonly", false);
        $("#CPlanilla").prop("readonly", false);
    } else {
        $("#CEfectivo").prop("readonly", true);
        $("#CCheque").prop("readonly", true);
        $("#CPlanilla").prop("readonly", true);
    }

    $("#TBancoConsig").html($("#banco" + banco).html());
    $("#CEfectivo").val(formato_numero(BEfe[banco], 0,_CD,_CM,""));
    $("#CCheque").val(formato_numero(BChe[banco], 0,_CD,_CM,""));
    $("#CPlanilla").val(BPla[banco]);
    $("#CTotal").val(formato_numero((BEfe[banco] * 1) + (BChe[banco]*1),0,".",",",""));
    $("#modalBanco").modal("show");
}

function Consultar() {
    
    fecha = $("#CFecha").val();
    numero = $("#CNumero").val() * 1;
    
    ActivarLoad();
    setTimeout(function () {
        var parametros = "Fecha=" + fecha + "&Numero=" + numero;
        var datos = LlamarAjax(url + "Arqueo/Arqueo", parametros);
        datos = datos.split("|");
        DesactivarLoad();
        if (datos[0] != "[]") {

            if (numero > 0) {
                $("#Gua_Arqueo").addClass("hidden");
                $("#Imp_Arqueo").removeClass("hidden");

                $("#TAnticipoCre").prop("disabled", true);
                $("#TReteFuente").prop("disabled", true);
                $("#TReteICA").prop("disabled", true);
                $("#TReteFuenteCre").prop("disabled", true);
                $("#TAnticipo").prop("disabled", true);
                $("#TCCEmpleado").prop("disabled", true);
                                
            } else {
                $("#Gua_Arqueo").removeClass("hidden");
                $("#Imp_Arqueo").addClass("hidden");

                $("#TAnticipoCre").prop("disabled", false);
                $("#TReteFuente").prop("disabled", false);
                $("#TReteICA").prop("disabled", false);
                $("#TReteFuenteCre").prop("disabled", false);
                $("#TAnticipo").prop("disabled", false);
                $("#TCCEmpleado").prop("disabled", false);
            }

            $('#modalConsultar').modal('hide');
            var data = JSON.parse(datos[0]);
            $("#fecha_arqueo").html(InvertirFecha(data[0].FechaArqueo, "-"));
            fecha = data[0].FechaArqueo
            $("#usuario_arqueo").html(usuario);
            var titulo = data;
            for (var i in titulo) {
                for (var j in data[i]) {
                    if (data[i][j]) {
                        $("#" + j).html(formato_numero(data[i][j], "0", ".", ",", ""));
                        $("#" + j).val(formato_numero(data[i][j], "0", ".", ",", ""));
                    }
                    else{
                        $("#" + j).val("");
                        $("#" + j).html("0");
                    }
                }
            }

            TOIngreso = data[0].OIngresos;
            TOEgresos = data[0].OEgresos;
            TCCEmpleado = data[0].CxCEmp;
            TAnticipo = data[0].Anticipos;
            TReteICA = data[0].ReteIca;
            TReteFuenteCre = data[0].RetFueCre;
            TReteFuente = data[0].ReteFuente;
            TAnticipoCre = data[0].AnticiposCre;
            TDevoDesc = data[0].DevDesVen;
            TSaldoAnt = data[0].SaldoAnt;
            TCajaMenor = data[0].ReCaMenor;
            
            $("#TSaldoAnt").val(formato_numero(TSaldoAnt,0,".",",",""));
            $("#TCajaMenor").val(formato_numero(TCajaMenor, 0,_CD,_CM,""));
            $("#TVentas").val(formato_numero(TVentas, 0,_CD,_CM,""));
            $("#TDevoDesc").val(formato_numero(TDevoDesc, 0,_CD,_CM,""));
            $("#TAnticipoCre").val(formato_numero(TAnticipoCre, 0,_CD,_CM,""));
            $("#TReteFuente").val(formato_numero(TReteFuente, 0,_CD,_CM,""));
            $("#TOIngreso").val(formato_numero(TOIngreso, 0, ".", ",",""));
            $("#TReteICA").val(formato_numero(TReteICA, 0,_CD,_CM,""));
            $("#TReteFuenteCre").val(formato_numero(TReteFuenteCre, 0,_CD,_CM,""));
            $("#TAnticipo").val(formato_numero(TAnticipo, 0,_CD,_CM,""));
            $("#TCCEmpleado").val(formato_numero(TCCEmpleado, 0,_CD,_CM,""));
            $("#TOEgresos").val(formato_numero(TOEgresos, 0,_CD,_CM,""));

            var ventaslo = (data[0].LOCreditoEsp * 1) + (data[0].LOCredito * 1) + (data[0].LOConEfec * 1) + (data[0].LOConCheq * 1) + (data[0].LOConTarj * 1)
            $("#TotalVentasLO").val(formato_numero(ventaslo, "0", ".", ",", ""));

            var ventasmo = (data[0].MOCreditoEsp * 1) + (data[0].MOCredito * 1) + (data[0].MOConEfec * 1) + (data[0].MOConCheq * 1) + (data[0].MOConTarj * 1)
            $("#TotalVentasMO").val(formato_numero(ventasmo, "0", ".", ",", ""));

            var ventasva = (data[0].VACreditoEsp * 1) + (data[0].VACredito * 1) + (data[0].VAConEfec * 1) + (data[0].VAConCheq * 1) + (data[0].VAConTarj * 1)
            $("#TotalVentasVA").val(formato_numero(ventasva, "0", ".", ",", ""));

            var ventasLC = (data[0].LCCreditoEsp * 1) + (data[0].LCCredito * 1) + (data[0].LCConEfec * 1) + (data[0].LCConCheq * 1) + (data[0].LCConTarj * 1)
            $("#TotalVentasLC").val(formato_numero(ventasLC, "0", ".", ",", ""));

            ConsignarMO = (data[0].MOConEfec * 1) + (data[0].MOConCheq * 1); 
            ConsignarLO = (data[0].LOConEfec * 1) + (data[0].LOConCheq * 1);
            ConsignarVA = (data[0].VAConEfec * 1) + (data[0].VAConCheq * 1);
            ConsignarLC = (data[0].LCConEfec * 1) + (data[0].LCConCheq * 1);

            TTarjeta = (data[0].LOConTarj * 1) + (data[0].LCConTarj * 1) + (data[0].MOConTarj * 1) + (data[0].VAConTarj * 1);

            $("#ConsignarMO").val(formato_numero(ConsignarMO, "0", ".", ",", ""));
            $("#ConsignarLO").val(formato_numero(ConsignarLO, "0", ".", ",", ""));
            $("#ConsignarVA").val(formato_numero(ConsignarVA, "0", ".", ",", ""));
            $("#ConsignarLC").val(formato_numero(ConsignarLC, "0", ".", ",", ""));
            
            TVentas = (data[0].LOConTarj * 1) + (data[0].LOConEfec * 1) + (data[0].LOConCheq * 1) +
                (data[0].LCConTarj * 1) + (data[0].LCConEfec * 1) + (data[0].LCConCheq * 1) +
                (data[0].MOConTarj * 1) + (data[0].MOConEfec * 1) + (data[0].MOConCheq * 1) +
                (data[0].VAConTarj * 1) + (data[0].VAConEfec * 1) + (data[0].VAConCheq * 1);
            
            $("#TVentas").val(formato_numero(TVentas, "0", ".", ",", ""));
                        
            if (datos[1] != "[]") {
                var data = JSON.parse(datos[1]);
                for (var x = 0; x < data.length; x++) {
                    $("#banco" + x).html(data[x].NomBanco);
                }
            }

            var DescripcionTar = "";

            if (datos[2] != "[]") {
                var data = JSON.parse(datos[2]);
                for (var x = 0; x < data.length; x++) {
                    if (DescripcionTar == "")
                        DescripcionTar = data[x].Tarjeta + "(" + formato_numero(data[x].Monto, 0,_CD,_CM,"") + ")"
                    else
                        DescripcionTar += ", " + data[x].Tarjeta + "(" + formato_numero(data[x].Monto, 0,_CD,_CM,"") + ")"; 
                }
            }

            $("#DesTarjeta").html(DescripcionTar);

            for (var x = 0; x <= 3; x++) {
                BPla[x] = "";
                BEfe[x] = 0;
                BChe[x] = 0;
                $("#VBanco" + x).val("");
            }

            if (datos[3] != "[]") {
                var data = JSON.parse(datos[3]);
                for (var x = 0; x < data.length; x++) {
                    BPla[data[x].NoBanco * 1] = data[x].Planilla;
                    BEfe[data[x].NoBanco * 1] = data[x].Cheque;
                    BChe[data[x].NoBanco * 1] = data[x].Efectivo;
                    $("#VBanco" + x).val(formato_numero(data[x].Cheque + data[x].Efectivo, 0,_CD,_CM,""));
                }
            }
                        
            CalcularTotal();
            
        } else {
            swal(ValidarTraduccion("Acción Cancelada"), ValidarTraduccion("No hay movimientos en el arqueo para esta fecha y usuario"), "warning");
            $("#NoCierre").html("");
            $("#fecha_arqueo").html("");
            $("#usuario_arqueo").html("");
            fecha = "";
            usuario = "";
            numero = 0;
            $("#Gua_Arqueo").addClass("hidden");
            $("#Imp_Arqueo").addClass("hidden");
        }
    }, 15);
}


tb = $('input');
$(tb).keypress(enter2tab);

tb = $('select');
$(tb).keypress(enter2tab);

function enter2tab(e) {
    id = $(this).attr("id");
    if ((e.keyCode == 13) && ((id != "Codigo") || (id != "CodigoB"))) {
        cb = parseInt($(this).attr('tabindex'));
        if ($(':input[tabindex=\'' + (cb + 1) + '\']') != null) {
            $(':input[tabindex=\'' + (cb + 1) + '\']').focus();
            $(':input[tabindex=\'' + (cb + 1) + '\']').select();
            e.preventDefault();
            return false;
        }
    }
}


$('body').removeClass('nav-md');
$('body').addClass('nav-sm');

$('select').select2();
/*setTimeout(function () {
    $('#modalConsultar').modal('show');
}, 1000);*/