﻿var CodCliente = 0;
var Cli_Correo = "";
var CliReteIva = 0;
var CliReteIca = 0;
var CliReteFuente = 0;
var ErrorCli = 0;
var DocumentoCli = "";
var NombreCliente = "";
var TotalFactura = 0;
var Recaudado = 0;
var SaldoFac = 0;
var SaldoAbo = 0;
var Abonado = 0;
var RetIVA = 0;
var RetCREE = 0;
var RetFuente = 0;
var RetICA = 0;
var Descuento = 0;
var OIngreso = 0;
var OEgreso = 0;
var TotalSeleccionado = 0;
var TotalSelSubtotal = 0;
var TotalSelecIva = 0;
var NRecibo = 0;
var Efectivo = 0;
var Tarjeta = 0;
var Cheque = 0;
var Consignado = 0;
var Facturas = "";
var CantCheque = 0;
var TotalCheques = 0;
var EditTotalCheques = 0;
var tipoMovimiento = "";
var Diferencia = 0;


var c_VRecaudado = document.getElementById('VRecaudado');

var c_RetIVA = document.getElementById('RetIVA');
var c_RetCREE = document.getElementById('RetCREE');
var c_RetFuente = document.getElementById('RetFuente');
var c_RetICA = document.getElementById('RetICA');
var c_Descuento = document.getElementById('Descuento');

var c_OIngreso = document.getElementById('OIngreso');
var c_OEgreso = document.getElementById('OEgreso');

document.onkeydown = function (e) {
    tecla = (document.all) ? e.keyCode : e.which;

    switch (tecla) {
        case 112: //F1
            ModalClientes();
            return false;
            break;
        case 113: //F2
            LimpiarTodo();
            return false;
            break;
        case 114: //f3
            ModalRecibo();
            return false;
            break;
        case 115: //f4
            GuardarRecibo();
            return false;
            break;
        case 117: //f16
            EnviarNota();
            return false;
            break;
        case 118: //f7
            ImprimirNota(1);
            return false;
            break;
        case 119: //f7
            AnularNota();
            return false;
            break;
    }

}

function ImprimirNota() {
    if (NRecibo > 0) {

        ActivarLoad();
        setTimeout(function () {
            if (tipoMovimiento == "NC")
                var datos = LlamarAjax("Caja/ImprimirNotaCre", "recibo=" + NRecibo);
            else
                var datos = LlamarAjax("Caja/ImprimirNotaDeb", "recibo=" + NRecibo);
            DesactivarLoad();
            datos = datos.split("|");
            if (datos[0] == "0") {
                window.open("Documpdf/" + datos[1])
            } else {
                swal("Acción Cancelada", datos[1], "error");
            }
        }, 15);
    }
}


function CargarConcepto(tipo) {
    $("#Concepto").html(LlamarAjax("Caja/CombosConcepto", "tipo=" + tipo));
    if (tipo == "C") 
        tipoMovimiento = "NC";
    else
        tipoMovimiento = "ND";
}

function AnularNota() {

    if (NRecibo == 0)
        return false;

    var descripcion = "Nota de Crédito"
    if (tipoMovimiento == "ND")
        descripcion = "Nota de Débito"

    var resultado = "";
    swal({
        title: 'Advertencia',
        text: '¿' + ValidarTraduccion('Desea Anular la ') + ' ' + ValidarTraduccion(descripcion) + " " + NRecibo + ' ' + ValidarTraduccion('del cliente') + ' ' + NombreCliente,
        input: 'text',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Si'),
        cancelButtonText: ValidarTraduccion('No'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',

        inputValidator: function (value) {
            return new Promise(function (resolve, reject) {
                if (value) {
                    var datos = LlamarAjax("Caja/AnularNota", "numero=" + NRecibo + "&observaciones=" + value + "&tipo=" + tipoMovimiento)
                    datos = datos.split("|");
                    if (datos[0] == "0") {
                        resultado = NRecibo;
                        resolve()
                    } else {
                        reject(datos[1]);
                    }

                } else {
                    reject(ValidarTraduccion('Debe de ingresar un observación'));
                }
            })
        }
    }).then(function (result) {
        Limpiar();
        CodCliente = 0;
        $("#Cliente").val("");
        $("#Cliente").focus();
        swal({
            type: 'success',
            html: descripcion + ' Nro ' + resultado + ' anulado con éxito'
        })
    })

}

function BuscarNota(recibo, cliente, tipo) {
        
    if (tipo == "NCr") {
        $("#nota_c").prop("checked", true);
        CargarConcepto('C')
    } else {
        $("#nota_d").prop("checked", true);
        CargarConcepto('D')
    }

    
    $("#modalConsultarNotas").modal("hide");
    ActivarLoad();
    setTimeout(function () {
        $("#Cliente").val(cliente);
        $("#Recibo").html(recibo);
        CodCliente = 0;
        BuscarCliente(cliente, recibo);
        DesactivarLoad();
        $("#btnImprimir").removeClass("hidden")
        $("#btnEnviar").removeClass("hidden")
        $("#btnAnular").removeClass("hidden")
        $("#btnGuardar").addClass("hidden");

        $("#nota_c").prop("disabled", true);
        $("#nota_d").prop("disabled", true);

        $("#RetIVA").prop("disabled", true);
        $("#RetCREE").prop("disabled", true);
        $("#RetICA").prop("disabled", true);
        $("#RetFuente").prop("disabled", true);
        $("#Efectivo").prop("disabled", true);
        $("#Descuento").prop("disabled", true);
        $("#OIngreso").prop("disabled", true);
        $("#OEgreso").prop("disabled", true);
        $("#VRecaudado").prop("disabled", true);
        $("#Concepto").prop("disabled", true);
    }, 15);

}

function CargarModalNotas() {

    var recibo = $("#NNRecibo").val() * 1;
    var tipo = $("#NTipo").val();
    var codigo = $("#NCodigo").val() * 1;
    var cliente = $.trim($("#NCliente").val());
    var estado = $("#NEstado").val();
    var fechad = $("#NFechaDesde").val();
    var fechah = $("#NFechaHasta").val();
    var dias = $("#NUltimoDia").val() * 1;

    if (fechad == "" && fechah == "" && dias == 0) {
        swal("Alerta", "debe de ingresar una fecha o números de días ", "error");
        return false;
    }


    var resultado = '<table class="table display  table-bordered" id="tablamodalnotas"><thead><tr><th width= "8%" > <span idioma="Nro Recibo">Nro Recibo</span></th><th width= "8%" > <span idioma="Tipo">Tipo</span></th><th><span idioma="Código">Cliente</span></th><th width="20%"><span idioma="Cliente">Cliente</span></th><th><span idioma="Fecha">Fecha</span></th><th><span idioma="Abonado">Abonado</span></th><th><span idioma="Anulado">Anulado</span></th><th><span idioma="Total">Total</th></tr></thead><tbody>';
    ActivarLoad();

    setTimeout(function () {
        var Anulado = "";
        var parametros = "recibo=" + recibo + "&tipo=" + tipo + "&codigo=" + codigo + "&cliente=" + cliente + "&estado=" + estado + "&fechad=" + fechad + "&fechah=" + fechah + "&dias=" + dias;
        var datos = LlamarAjax('Caja/ModalNotas', parametros);

        DesactivarLoad();
        if (datos != "[]") {
            eval("data=" + datos);
            for (var x = 0; x < data.length; x++) {
                Anulado = "<label class='label label-success'>NO</label>";
                if (data[x].HRcAnulado * 1 == 1)
                    Anulado = "<label class='label label-danger'>SI</label>";
                resultado += "<tr ondblclick=\"BuscarNota(" + data[x].HRcReciboCaja + "," + data[x].HRcCodCliente + ")\">" +
                    "<td>" + data[x].HRcReciboCaja + "</td>" +
                    "<td>" + data[x].Tipo + "</td>" +
                    "<td>" + data[x].HRcCodCliente + "</td>" +
                    "<td><a class='btn-default' href=\"javascript:BuscarNota(" + data[x].HRcReciboCaja + "," + data[x].HRcCodCliente + ",'" + data[x].Tipo + "')\">" + data[x].HRcNombreCli + "</a></td>" +
                    "<td>" + data[x].HRcFechaSistema + "</td>" +
                    "<td align='right'>" + formato_numero(data[x].VrAbonado, 0, ".", ",") + "</td>" +
                    "<td>" + Anulado + "</td>" +
                    "<td>" + data[x].TOTAL + "</td></tr>";
            }
        }

        resultado += "</tbody></table>"
        $("#divmodalnotas").html(resultado);
        ActivarDataTable("tablamodalnotas", 1);


    }, 15);
}

function ModalClientes() {
    $("#modalClientes").modal("show");
    $("#CValor").focus();
}

function ModalRecibo() {
    $("#modalConsultarNotas").modal("show");
}

function SeleccionarCliente(cliente) {
    $("#modalClientes").modal("hide");
    CodCliente = 0;
    $("#Cliente").val(cliente);
    $("#Cliente").blur();
}

$("#tablamodalcliente > tbody").on("click", "tr", function (e) {
    var row = $(this).parents("td").context.cells;
    var numero = row[1].innerText;
    $("#modalClientes").modal("hide");
    $("#Cliente").val(numero);
    BuscarCliente(numero, 0);
});

$("#Cliente").bind('keypress', function (e) {
    if (e.keyCode == 13) {
        $("#Cliente").blur();
    }
})

CargarConcepto('C')

function ValidarCliente(documento) {

    if (ErrorCli == 0) {
        if ($.trim(DocumentoCli) != "") {
            if (DocumentoCli != documento) {
                DocumentoCli = "";
                Limpiar();
            }
        }
    } else {
        ErrorCli = 0;
    }

}


function BuscarCliente(documento, recibo) {


    NRecibo = recibo;
    $("#Recibo").html(recibo);

    if ($.trim(documento) == "")
        return false;

    if (DocumentoCli == documento)
        return false;

    DocumentoCli = documento;

    ActivarLoad();

    setTimeout(function () {

        var parametros = "documento=" + documento;
        var datos = LlamarAjax('Cotizacion/BuscarCliente', parametros);
        datos = datos.split("|");
        DesactivarLoad();

        if (datos[0] == "0") {
            eval("data=" + datos[1]);
            IdCliente = data[0].id * 1;

            PlazoPago = data[0].plazopago;
            TipoCliente = data[0].tipocliente;
            CorreoEmpresa = data[0].email;

            $("#ChNombre").val(data[0].nombrecompleto);
            $("#ChNombre").prop("inicial", data[0].nombrecompleto);
            $("#ChCedula").val(data[0].documento);
            $("#ChCedula").prop("inicial", data[0].documento);
            $("#ChTelefono").val((data[0].telefono ? data[0].telefono : "") + ", " + (data[0].celular ? data[0].celular : ""));
            $("#ChTelefono").prop("inicial", (data[0].telefono ? data[0].telefono : "") + ", " + (data[0].celular ? data[0].celular : ""));
            $("#ChDireccion").val(data[0].direccion);
            $("#ChDireccion").prop("inicial", data[0].direccion);
            $("#Moneda").html("Pesos");
            CliReteIva = data[0].reteiva * 1;
            CliReteIca = data[0].reteica * 1;
            CliReteFuente = data[0].retefuente * 1
            Cli_Correo = (data[0].email ? data[0].email : "");
            $("#NombreCliente").html(data[0].nombrecompleto + "<br>" + data[0].direccion + " (" + data[0].ciudad + ") Tel: " + (data[0].telefono ? data[0].telefono : "") + ", " + (data[0].celular ? data[0].celular : "") + ", " + Cli_Correo);
            NombreCliente = data[0].nombrecompleto;
            if (NRecibo == 0)
                TablaFacturasPendientes();
            else
                TablaFacturasRecibo();

            $("#SaldoClienteDev").html(SaldoTotal(IdCliente));

        } else {
            ErrorCli = 1;
            $("#Cliente").select();
            $("#Cliente").focus();
            swal("Acción Cancelada", "Cliente no Registrado", "warning");
            IdCliente = 0;
        }

    }, 15);
}


function LimpiarTodo() {
    Limpiar();
    $("#Cliente").val("");
    CodCliente = "";
}

function TablaFacturasRecibo() {

    var tipo = 'C';
    if ($("#nota_d").prop("checked"))
        tipo = 'D';

    var parametros = "recibo=" + NRecibo + "&tipo=" + tipo;
    var datos = LlamarAjax('Caja/BuscarNota', parametros);
    var TotalFactura = 0;
    if (datos != "[]") {
        eval("data=" + datos);
        if (data[0].HRcAnulado * 1 == 1) {
            $("#divAnulaciones").removeClass("hidden");
            $("#ObsAnulacion").html("Usuario que Anuló: " + data[0].uanula + ", Fecha: " + data[0].fanula + ", Observación: " + data[0].ranula);
            $("#btnAnular").addClass("hidden");
        } else {
            $("#btnAnular").removeClass("hidden");
            $("#divAnulaciones").addClass("hidden");
        }
        for (var x = 0; x < data.length; x++) {
            resultado += "<tr>" +
                "<td>" + (x + 1) + "</td>" +
                "<td>" + data[x].HRcNumero + "</td>" +
                "<td>" + data[x].HRcCantFact + "</td>" +
                "<td>" + data[x].Fecha + "</td>" +
                "<td>" + data[x].Vencimiento + "</td>" +
                "<td align='right'>0</td>" +
                "<td align='right'>" + formato_numero(data[x].HRcVrAbonado, 0, ".", ",") + "</td></tr>";
            TotalFactura += data[x].HRcVrAbonado * 1;
            $("#Concepto").val(data[x].conc_codigo).trigger("change");
            $("#VRecibido").val(formato_numero(data[x].HRcVrRecibido, 0, ".", ","));
            $("#VRecaudado").val(formato_numero(data[x].HRcVrRecibido, 0, ".", ","));
            if (data[x].HRcEfectivo * 1 > 0)
                $("#Efectivo").val(formato_numero(data[x].HRcEfectivo, 0, ".", ","));
            if (data[x].HRcTarjeta * 1 > 0)
                $("#TTarjeta").val(formato_numero(data[x].HRcTarjeta, 0, ".", ","));
            if (data[x].HRcCheque * 1 > 0)
                $("#TCheque").val(formato_numero(data[x].HRcCheque, 0, ".", ","));
            if (data[x].HRcConsignacion * 1 > 0)
                $("#TConsignacion").val(formato_numero(data[x].HRcConsignacion, 0, ".", ","));
            if (data[x].HRcDescuentos * 1 > 0)
                $("#Descuento").val(formato_numero(data[x].HRcDescuentos, 0, ".", ","));
            if (data[x].HRcRetencion * 1 > 0)
                $("#RetFuente").val(formato_numero(data[x].HRcRetencion, 0, ".", ","));
            if (data[x].HRcIca * 1 > 0)
                $("#RetICA").val(formato_numero(data[x].HRcIca, 0, ".", ","));
            if (data[x].HRcIva * 1 > 0)
                $("#RetIVA").val(formato_numero(data[x].HRcIva, 0, ".", ","));
            if (data[x].HRcCree * 1 > 0)
                $("#RetCREE").val(formato_numero(data[x].HRcCree, 0, ".", ","));
            if (data[x].HRcOEgresos * 1 > 0)
                $("#OIngreso").val(formato_numero(data[x].HRcOEgresos, 0, ".", ","));
            if (data[x].HRcOIngresos * 1 > 0)
                $("#OEgreso").val(formato_numero(data[x].HRcOIngresos, 0, ".", ","));


        }
    }

    $("#bodyfacturacion").html(resultado);
    $("#TFactura").val(formato_numero(TotalFactura, 0, ".", ","));
    $("#STotal").html(formato_numero(TotalFactura, 0, ".", ", "));
}

function ValidarTextoCaja(caja, tipo) {
    if ((caja.id == "VRecaudado") && ((Abonado > 0) || (TotalSeleccionado > 0))) {

        $("#VRecaudado").val(formato_numero(Recaudado, 0, ".", ","));
        return false;
    }

    ValidarTexto(caja, tipo);
    CalcularTotal(caja);
}

function GuardarNota() {

    var tipo = 'C';
    var observacion = $.trim($("#Observacion").val());
    if ($("#nota_d").prop("checked"))
        tipo = 'D';

    if (($("#NombreCliente").html() == "") || (NRecibo != 0))
        return false;

    if ($("#Concepto").val()*1 == 0) {
        $("#Concepto").focus();
        swal("Acción Cancelada", "Debe de seleccionar el concepto de la nota", "warning");
        return false;
    }

    if (observacion == "") {
        $("#Observacion").focus();
        swal("Acción Cancelada", "Debe de ingresar la observación  de la nota", "warning");
        return false;
    }
    
    if (Recaudado <= 0) {
        $("#VRecaudado").focus();
        swal("Acción Cancelada", "Debe de ingresar el monto recaudado", "warning");
        return false;
    }


    if (SaldoAbo > 0) {
        
        swal("Acción Cancelada", "Debe de completar el saldo por pagar", "warning");
        return false;
    }

    if (tipoMovimiento == "NC") {
        if (SaldoFac > 0) {
            swal("Acción Cancelada", "Debe de completar el saldo por abonar a factura(s)", "warning");
            return false;
        }
    }

    if (Diferencia <= -1) {
        swal(ValidarTraduccion("Acción Cancelada"), ValidarTraduccion("No puede haber diferencia negativa en el comprobante de pago"), "warning");
        return false;
    }

            
    ActivarLoad();
    setTimeout(function () {
        var parametros = "Cliente=" + CodCliente + "&Facturas=" + Facturas + "&Concepto=" + $("#Concepto").val() + "&Recaudado=" + Recaudado + "&Observacion=" + observacion + "&RetIVA=" +
            RetIVA + "&RetCRE=" + RetCREE + "&RetICA=" + RetICA + "&RetFuente=" + RetFuente + "&Descuento=" + Descuento + "&OIngresos=" + OIngreso + "&OEgresos=" + OEgreso + "&tipo=" + tipo; 
        var datos = LlamarAjax("Caja/GuardarNota", parametros);
        DesactivarLoad();
        datos = datos.split("|");
        if (datos[0] == "0") {
            swal("", ValidarTraduccion(datos[1]) + " " + datos[2], "success");
            NRecibo = datos[2] * 1;
            $("#Recibo").html(NRecibo);
            $("#btnImprimir").removeClass("hidden")
            $("#btnEnviar").removeClass("hidden")
            $("#btnAnular").removeClass("hidden")
        } else {
            swal("Acción Cancelada", datos[1], "error");
        }
    }, 15);

}

function CalcularTotal(caja) {

    Diferencia = 0;
    RetIVA = NumeroDecimal(c_RetIVA.value) * 1;

    if (tipoMovimiento == "NC") {
        RetCREE = NumeroDecimal(c_RetCREE.value) * 1;
        RetFuente = NumeroDecimal(c_RetFuente.value) * 1;
        RetICA = NumeroDecimal(c_RetICA.value) * 1;
        Descuento = NumeroDecimal(c_Descuento.value) * 1;

        OIngreso = NumeroDecimal(c_OIngreso.value) * 1;
        OEgreso = NumeroDecimal(c_OEgreso.value) * 1;
    } else {
        RetCREE = 0
        RetFuente = 0
        RetICA = 0
        Descuento = 0

        OIngreso = 0
        OEgreso = 0

        $("#RetFuente").val("0");
        $("#RetICA").val("0");
        $("#RetCREE").val("0");
        $("#Descuento").val("0");
        $("#OIngreso").val("0");
        $("#OEgreso").val("0");
    }

    
    
    Recaudado = NumeroDecimal(c_VRecaudado.value) * 1;

    SaldoFac = Recaudado - TotalSeleccionado + RetIVA + RetCREE + RetFuente + RetICA + Descuento + OEgreso - OIngreso;
    Diferencia = TotalSeleccionado - Recaudado - RetIVA - RetCREE - RetFuente - RetICA - Descuento - OEgreso + OIngreso;

    //$.jGrowl(ValidarTraduccion(Recaudado + "-" + TotalSeleccionado + "+" + RetIVA + "+" + RetCREE + "+" + RetFuente + "+" + RetICA + "+" + Descuento + "+" + OEgreso + "-" + OIngreso), { life: 4000, theme: 'growl-warning', header: '' });

    if (SaldoFac < 0)
        SaldoFac = 0;
    
        
    $("#VRecibido").val(formato_numero(Abonado, 0, ".", ","));
    $("#SFactura").val(formato_numero(SaldoFac, 0, ".", ","));
    $("#SDiferencia").val(formato_numero(Diferencia, 0, ".", ","));

    
        
}

function TablaFacturasPendientes() {
    var parametros = "cliente=" + IdCliente +"&tipo=2";
    var datos = LlamarAjax('Caja/FacturasPendientes', parametros);
    var resultado = "";
    var clase = "";
    var diavencido = "";
    TotalFactura = 0;
    if (datos != "[]") {
        eval("data=" + datos);
        for (var x = 0; x < data.length; x++) {
            clase = ""
            diavencido = "";
            if (data[x].dias * 1 > 0) {
                clase = "class='text-danger text-bold'";
                diavencido = "<br>(" + data[x].dias + ") días";
            }

            resultado += "<tr " + clase + " onclick = 'SeleccionarFila(" + x + "," + data[x].saldo + "," + data[x].factura + "," + (data[x].subtotal - data[x].descuento) + "," + data[x].iva + ")' id = 'tr-" + x + "' > " +
                "<td><a class='text-XX' href='javascript:ImprimirFactura(" + data[x].factura + ")'>" + (x + 1) + "</a></td>" +
                "<td><div name='TablaFactura[]'>" + data[x].factura + "</div></td>" +
                "<td>" + data[x].fecha + "</td>" +
                "<td>" + data[x].vencimiento + diavencido + "</td>" +
                "<td align='right'><div name = TablaValor[]>" + formato_numero(data[x].subtotal, 0, ".", ",") + "</div></td>" +
                "<td align='right'>" + formato_numero(data[x].descuento, 0, ".", ",") + "</td>" +
                "<td align='right'><div name = TablaValor[]>" + formato_numero(data[x].iva, 0, ".", ",") + "</div></td>" +
                "<td align='right'><div name = TablaValor[]>" + formato_numero(data[x].total, 0, ".", ",") + "</div></td>" +
                "<td align='right'>" + formato_numero(data[x].total - data[x].saldo, 0, ".", ",") + "</td>" +
                "<td align='right'><div class='text-XX' name = TablaValor[]>" + formato_numero(data[x].saldo, 0, ".", ",") + "</div><input type='hidden' value='0' name='selecc[]' id='selecc" + x + "'/></td>" +
                "<td align='center'>" + data[x].asesor + "</td></tr>";
            TotalFactura += data[x].saldo * 1;
        }
    }
    $("#bodyfacturacion").html(resultado);
    $("#TFactura").val(formato_numero(TotalFactura, 0, ".", ","));
    $("#SFactura").val(formato_numero(TotalFactura, 0, ".", ","));

}

function SeleccionarFila(pos, valor, numfac, subtotal, iva) {
    if (NRecibo > 0)
        return false
    if (Recaudado == 0) {
        $.jGrowl(ValidarTraduccion("Debe ingresar primero lo recaudado"), { life: 4000, theme: 'growl-warning', header: '' });
        return false;
    }

    if ($("#selecc" + pos).val() == "0") {
        if (TotalSeleccionado > (Recaudado + RetIVA + RetCREE + RetFuente + RetICA + Descuento + OEgreso - OIngreso))
            return false;
        $("#tr-" + pos).addClass("bg-success");
        $("#selecc" + pos).val("1");
        TotalSeleccionado += valor;
        TotalSelSubtotal += subtotal;
        TotalSelecIva += iva;
        if (Facturas == "")
            Facturas = numfac
        else
            Facturas += "," + numfac;

    } else {
        $("#tr-" + pos).removeClass("bg-success");
        $("#selecc" + pos).val("0");
        TotalSeleccionado -= valor;
        TotalSelSubtotal -= subtotal;
        TotalSelecIva -= iva;

        if ($.trim(Facturas).indexOf(",") >= 0) {
            arrego = Facturas.split(",");
            Facturas = "";
            for (var x = 0; x < arrego.length; x++) {
                if (arrego[x] * 1 != numfac * 1) {
                    if (Facturas == "")
                        Facturas = arrego[x]
                    else
                        Facturas += "," + arrego[x];
                }
            }
        } else
            Facturas = "";
    }

    RetIVA = TotalSelecIva * CliReteIva / 100;
    RetICA = TotalSelSubtotal * CliReteIca / 100;
    RetFuente = TotalSelSubtotal * CliReteFuente / 100;

    $("#STotal").html(formato_numero(TotalSeleccionado, 0, ".", ","));
    $("#SSubTotal").html(formato_numero(TotalSelSubtotal, 0, ".", ","));
    $("#SIVA").html(formato_numero(TotalSelecIva, 0, ".", ","));

    $("#RetIVA").val(formato_numero(RetIVA, 0, ".", ","));
    $("#RetICA").val(formato_numero(RetICA, 0, ".", ","));
    $("#RetFuente").val(formato_numero(RetFuente, 0, ".", ","));

    CalcularTotal('No');
}

function Limpiar() {

    $("#NombreCliente").html("");
    NRecibo = 0;
    NombreCliente = "";
    DocumentoCli = "";
    $("#btnImprimir").addClass("hidden");
    $("#btnEnviar").addClass("hidden");
    $("#btnAnular").addClass("hidden");
    $("#btnGuardar").removeClass("hidden");
    TotalSeleccionado = 0;
    NombreCliente = "";
    Diferencia = 0;

    $("#nota_c").prop("disabled", false);
    $("#nota_d").prop("disabled", false);
    
    $("#tnumero").val("");
    $("#tvalor").val("");
    $("#ttipo").val("");
    $("#tcodseguridad").val("");
    $("#tfvencimiento").val("");
    $("#tautoriza").val();
    $("#divAnulaciones").addClass("hidden");

    Facturas = "";

    $("#RetIVA").val("0");
    $("#RetCREE").val("0");
    $("#RetICA").val("0");
    $("#RetFuente").val("0");
    $("#Descuento").val("0");
    $("#OIngreso").val("0");
    $("#OEgreso").val("0");


    $("#RetIVA").prop("disabled", false);
    $("#RetCREE").prop("disabled", false);
    $("#RetICA").prop("disabled", false);
    $("#RetFuente").prop("disabled", false);
    $("#Descuento").prop("disabled", false);
    $("#OIngreso").prop("disabled", false);
    $("#OEgreso").prop("disabled", false);
    $("#VRecaudado").prop("disabled", false);
    $("#Concepto").prop("disabled", false);


    $("#TFactura").val("0");
    $("#SFactura").val("0");
    $("#VRecibido").val("0");
    $("#SPagar").val("0");
    $("#VRecaudado").val("0");
    $("#Concepto").val("").trigger("change");

    $("#bodyfacturacion").html("");
    $("#Recibo").html("0");
    $("#NombreCliente").html("");
    $("#STotal").html("0");
}

function ImprimirFactura(factura) {
    ActivarLoad();
    setTimeout(function () {
        var parametros = "factura=" + factura;
        var datos = LlamarAjax("Facturacion/RpFactura", parametros);
        DesactivarLoad();
        datos = datos.split("|");
        if (datos[0] == "0") {
            window.open("DocumPDF/" + datos[1]);
        } else {
            swal("", ValidarTraduccion(datos[1]), "warning");
        }
    }, 15);
}

function AgregarRetencio() {
    var tipo = $("#RTipoRetencion").val() * 1;
    switch (tipo) {
        case 1: //IVA
            RetIVA = NumeroDecimal($("#RRetencion").val())
            $("#RetIVA").val($("#RRetencion").val());
            break;
        case 2: //FUENTE
            RetFuente = NumeroDecimal($("#RRetencion").val())
            $("#RetFuente").val($("#RRetencion").val());
            break;
        case 3: //ICA
            RetICA = NumeroDecimal($("#RRetencion").val())
            $("#RetICA").val($("#RRetencion").val());
            break;
    }
    CalcularTotal('No');
    $("#modalRetencion").modal("hide");
}

function CalcularRetencion(Caja) {

    ValidarTexto(Caja, 3);

    var porcentaje = NumeroDecimal($("#RPorcentaje").val());
    var valor = NumeroDecimal($("#RSubTotal").val());
    $("#RRetencion").val(formato_numero(valor * porcentaje / 100, 0,_CD,_CM,""));
}

function LlamarRetencion(tipo, descripcion) {
    if (TotalSeleccionado > 0) {

        switch (tipo) {
            case 1: //IVA
                $("#RPorCliente").html(CliReteIva);
                $("#RPorcentaje").val(CliReteIva);
                $("#RSubTotal").val(formato_numero(TotalSelecIva, 0,_CD,_CM,""));
                $("#RRetencion").val(formato_numero(TotalSelecIva * CliReteIva / 100, _DE,_CD,_CM,""));
                break;
            case 2: //FUENTE
                $("#RPorCliente").html(CliReteFuente);
                $("#RPorcentaje").val(CliReteFuente);
                $("#RSubTotal").val(formato_numero(TotalSelSubtotal, 0,_CD,_CM,""));
                $("#RRetencion").val(formato_numero(TotalSelSubtotal * CliReteFuente / 100, _DE,_CD,_CM,""));

                break;
            case 3: //ICA
                $("#RPorCliente").html(CliReteIca);
                $("#RPorcentaje").val(CliReteIca);
                $("#RSubTotal").val(formato_numero(TotalSelSubtotal, 0,_CD,_CM,""));
                $("#RRetencion").val(formato_numero(TotalSelSubtotal * CliReteIca / 100, _DE,_CD,_CM,""));
                break;
        }
        $("#RTipoRetencion").val(tipo);
        $("#desretencion").html(descripcion);
        $("#modalRetencion").modal("show");

    }
}


$('select').select2();
DesactivarLoad();
