﻿
d = new Date();
month = d.getMonth() + 1;
day = d.getDate();

output = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-' +
    (day < 10 ? '0' : '') + day;

$("#FechaI").val(output);
$("#FechaF").val(output);

$("#Usuarios").html(CargarCombo(4, 1));
$("#Almacen").html(CargarCombo(1, 1));
$("#Almacen").val($("#menu_idalmacen").val());
$("#Almacen").focus();
var nfilas = 0;
var OpcionReporte = 1;

$("form").submit(function (e) {
    e.preventDefault();
})

function Reporte(opcion) {
    
    var almacen = $("#Almacen").val();
    var usuario = $("#Usuarios").val();
    var fechaf = $("#FechaF").val();
    var fechad = $("#FechaI").val();

    var ordenado = 0;
    var anulado = 0;
       

    $("#resultadopdf").removeAttr("src");

    ActivarLoad();
    setTimeout(function () {
        var parametros = "fechad=" + fechad + "&fechah=" + fechaf + "&Almacen=" + almacen + "&usuario=" + usuario + "&opcion=" + opcion; 
        var datos = LlamarAjax(url + "Arqueo/PagosRecibidos", parametros);
        datos = datos.split("|");
        if (datos[0] == "0") {
            switch (opcion) {
                case 1:
                    CargarTabla(datos[1]);
                    $("#pdf").removeClass("active");
                    $("#opcion2").removeClass("active");
                    $("#consultar").addClass("active");
                    $("#opcion1").addClass("active");
                    break;
                case 2:
                    $("#consultar").removeClass("active");
                    $("#opcion1").removeClass("active");
                    $("#pdf").addClass("active");
                    $("#opcion2").addClass("active");

                    $("#resultadopdf").attr("src", url + "DocumPDF/" + datos[1]);
                    break;

            }
        } else
            swal("Alerta", datos[1], "warning");
        DesactivarLoad();
    }, 15);
}

function CargarTabla(datos) {
    if (datos != "[]") {
        var datajson = JSON.parse(datos);
        $('#tablaconsulta').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "HRcReciboCaja" },
                { "data": "HrcNumero" },
                { "data": "Fecha" },
                { "data": "HRcVrFactura" },
                { "data": "HRcVrAbonado" },
                { "data": "Saldo" },
                { "data": "Pedido" },
                { "data": "HrcCodCliente" },
                { "data": "HrcNombreCli" },
                { "data": "usuario" }
            ],

            "columnDefs": [
                { "type": "numeric-comma", targets: 3 }
            ],

            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv',  'copy'
            ]
        });
    }

}


tb = $('input');
$(tb).keypress(enter2tab);

tb = $('select');
$(tb).keypress(enter2tab);

function enter2tab(e) {
    id = $(this).attr("id");
    if ((e.keyCode == 13) && ((id != "Codigo") || (id != "CodigoB"))) {
        cb = parseInt($(this).attr('tabindex'));
        if ($(':input[tabindex=\'' + (cb + 1) + '\']') != null) {
            $(':input[tabindex=\'' + (cb + 1) + '\']').focus();
            $(':input[tabindex=\'' + (cb + 1) + '\']').select();
            e.preventDefault();
            return false;
        }
    }
}

$('select').select2();