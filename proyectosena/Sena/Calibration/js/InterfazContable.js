﻿
d = new Date();
month = d.getMonth() + 1;
day = d.getDate();

output = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-' +
    (day < 10 ? '0' : '') + day;

$("#Fechad").val(output);
$("#Fechah").val(output);

$("#Almacen").html(CargarCombo(1, 1));
$("#Almacen").val($("#menu_idalmacen").val());

var nfilas = 0;
var OpcionReporte = 1;


function Reporte(opcion) {
    
    var tipo = $("#Tipo").val()*1;
    var almacen = $("#Almacen").val() * 1;
    var fechah = $("#Fechah").val();
    var fechad = $("#Fechad").val();

    if (tipo != 3) {
        $("#tablafactura").removeClass("hidden");
        $("#tablatercero").addClass("hidden");
    } else {
        $("#tablafactura").addClass("hidden");
        $("#tablatercero").removeClass("hidden");
    }

    document.getElementById('preloader').style.display = 'block';
    setTimeout(function () {
        var parametros = "fechad=" + fechad + "&fechah=" + fechah + "&tipo=" + tipo + "&CV=" + almacen;
        var datos = LlamarAjax(url + "Interfaz/InterfazContable", parametros);
        if (datos != "[]") {
            var datajson = JSON.parse(datos);
            if (tipo != 3) {
                $("#tablafactura").removeClass("hidden");
                $("#tablatercero").addClass("hidden");
                $('#tablafactura').DataTable({
                    data: datajson,
                    bProcessing: true,
                    bDestroy: true,
                    columns: [
                       
                        { "data": "tipodocumento" },
                        { "data": "nrodocumento" },
                        { "data": "fecha" },
                        { "data": "cuenta" },
                        { "data": "concepto" },
                        { "data": "CentroCosto" },
                        { "data": "Valor" },
                        { "data": "Naturaleza" },
                        { "data": "IdentidadTercero" },
                        { "data": "DV" },
                        { "data": "NombreTercero" },
                        { "data": "Ciudad" },
                        { "data": "Pais" },
                        { "data": "Cuentabancaria" },
                        { "data": "Docfuente" },
                        { "data": "CodigoNegocio" },
                        { "data": "NombreNegocio" },
                        { "data": "CodigoTercero" },
                        { "data": "Base" },
                        { "data": "Inversion" },
                        { "data": "Obligacionfinanciera" },
                        { "data": "Clasemov" }
                    ],

                    "columnDefs": [
                        { "type": "numeric-comma", targets: 3 }
                    ],

                    "language": {
                        "url": LenguajeDataTable
                    },
                    dom: 'Bfrtip',
                    buttons: [
                        'excel', 'csv',  'copy'
                    ]
                });
            } else {

                $("#tablafactura").addClass("hidden");
                $("#tablatercero").removeClass("hidden");
                $('#tablatercero').DataTable({
                    data: datajson,
                    bProcessing: true,
                    bDestroy: true,
                    columns: [
                        { "data": "Documento" },
                        { "data": "DV" },
                        { "data": "CLASE" },
                        { "data": "Nombre" },
                        { "data": "Direccion" },
                        { "data": "TELEFONO" },
                        { "data": "FAX" },
                        { "data": "Ciudad" },
                        { "data": "Nombre de la Ciudad"},
                        { "data": "Apartado" },
                        { "data": "CORREO ELECTRONICO" },
                        { "data": "Apellido1" },
                        { "data": "Apellido2" },
                        { "data": "Nombre1" },
                        { "data": "Nombre2" },
                        { "data": "NATURALEZA" },
                        { "data": "ACTIVIDAD ECONOMICA" },
                        { "data": "CODIGO AREA" },
                        { "data": "NUMERO MOVIL" }
                    ],

                    "columnDefs": [
                        { "type": "numeric-comma", targets: 3 }
                    ],

                    "language": {
                        "url": LenguajeDataTable
                    },
                    dom: 'Bfrtip',
                    buttons: [
                        'excel', 'csv',  'copy'
                    ]
                });
            }
        }
        document.getElementById('preloader').style.display = 'none';
    }, 15);

}

tb = $('input');
$(tb).keypress(enter2tab);

tb = $('select');
$(tb).keypress(enter2tab);

function enter2tab(e) {
    id = $(this).attr("id");
    if ((e.keyCode == 13) && ((id != "Codigo") || (id != "CodigoB"))) {
        cb = parseInt($(this).attr('tabindex'));
        if ($(':input[tabindex=\'' + (cb + 1) + '\']') != null) {
            $(':input[tabindex=\'' + (cb + 1) + '\']').focus();
            $(':input[tabindex=\'' + (cb + 1) + '\']').select();
            e.preventDefault();
            return false;
        }
    }
}

/*

 { "data": "tipodocumento" },
                        { "data": "fecha" },
                        { "data": "nrodocumento" },
                        { "data": "cuenta" },
                        { "data": "concepto" },
                        { "data": "CentroCosto" },
                        { "data": "Valor" },
                        { "data": "Naturaleza" },
                        { "data": "IdentidadTercero" },
                        { "data": "DV" },
                        { "data": "CodigoNegocio" },
                        { "data": "NombreNegocio" },
                        { "data": "NroCheque" },
                        { "data": "CodigoTercero" },
                        { "data": "Cuentabancaria" },
                        { "data": "idVendedor" },
                        { "data": "FormaPago" },
                        { "data": "Base" },
                        { "data": "NroOperacion" },
                        { "data": "FechaCheque" },
                        { "data": "Inversion" },
                        { "data": "Obligacionfinanciera" }

columns: [
    
],*/

$('select').select2();