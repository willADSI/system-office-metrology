﻿
d = new Date();
month = d.getMonth() + 1;
day = d.getDate();

output = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-' +
    (day < 10 ? '0' : '') + day;

$("#RFechaI").val(output);
$("#RFechaF").val(output);

var nfilas = 0;
var OpcionReporte = 1;

$("form").submit(function (e) {
    e.preventDefault();
})

function Reporte(opcion) {

    var cliente = $.trim($("#RCliente").val());
    var codcli = $("#RCodigo").val() * 1;
    var consignacion = $("#RNumero").val() * 1;
    var nit = $.trim($("#RNit").val())
    var asesor = $("#RAsesor").val() * 1;
    var distrito = $("#RDistrito").val() * 1;
    var ciudad = $("#RCiudad").val() * 1;
    var fechaf = $("#RFechaF").val();
    var fechad = $("#RFechaI").val();

    var resumido = 0;
    var anulado = 0;

    if ($("#RResumido").prop("checked"))
        resumido = 1

    if ($("#RAnulado").prop("checked"))
        anulado = 1


    $("#resultadopdf").removeAttr("src");
    $("#divtablaconsulta").html("");
    ActivarLoad();
    setTimeout(function () {
        var parametros = "fechad=" + fechad + "&fechah=" + fechaf + "&consignacion=" + consignacion + "&codcli=" + codcli + "&cliente=" + cliente + "&nit=" + nit + "&asesor=" + asesor + "&anulado=" + anulado + "&resumido=" + resumido + "&opcion=" + opcion + "&distrito=" + distrito + "&ciudad=" + ciudad;
        var datos = LlamarAjax(url + "Consignacion/ImprimirRepConsignacion", parametros);
        datos = datos.split("|");
        if (datos[0] == "0") {
            switch (opcion) {
                case 1:
                    CargarTabla(datos[1]);
                    break;
                case 2:
                    window.open(url + "DocumPDF/" + datos[1]);
                    break;
            }
        } else
            swal("Alerta", datos[1], "warning");
        DesactivarLoad();
    }, 15);
}

function CargarTabla(datos) {
    if (datos != "[]") {
        CargarTablaJsonTituloRep(datos, "tablaconsulta", "divtablaconsulta");
    }
}


tb = $('input');
$(tb).keypress(enter2tab);


function CarCombFact() {

    var datos = LlamarAjax(url + "Consignacion/CombosReportes", "");
    data = datos.split("|");

    $("#RAsesor").html(data[0]);
    $("#RDistrito").html(data[1]);
    $("#RCiudad").html(data[3]);
    
}

CarCombFact();

function ImprimirCons(pedido) {
    var parametros = "pedido=" + pedido + "&factura=0";
    var datos = LlamarAjax(url + "Facturacion/ReporteFactura", parametros);
    datos = datos.split("|");
    if (datos[0] == "0") {
        window.open(url + "DocumPDF/" + datos[1]);
    } else {
        swal("", ValidarTraduccion(datos[1]), "warning");
    }
}

tb = $('select');
$(tb).keypress(enter2tab);

function enter2tab(e) {
    id = $(this).attr("id");
    if ((e.keyCode == 13) && ((id != "Codigo") || (id != "CodigoB"))) {
        cb = parseInt($(this).attr('tabindex'));
        if ($(':input[tabindex=\'' + (cb + 1) + '\']') != null) {
            $(':input[tabindex=\'' + (cb + 1) + '\']').focus();
            $(':input[tabindex=\'' + (cb + 1) + '\']').select();
            e.preventDefault();
            return false;
        }
    }
}

$('select').select2();
