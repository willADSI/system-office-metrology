﻿document.onkeydown = function (e) {
    tecla = (document.all) ? e.keyCode : e.which;
    switch (tecla) {
        case 112:
            RelacionarIngreso();
            return false;
            break;
        case 112:
            return false;
            RelacionarSolicitud()
            break;
    }
}

d = new Date();
month = d.getMonth() + 1;
day = d.getDate();

var Anio = $.trim(d.getFullYear());
Anio = $.trim(Anio.substr(2, 2))

output = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-01';

output2 = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-' +
    (day < 10 ? '0' : '') + day;

$("#FechaDesde").val(output);
$("#FechaHasta").val(output2);

$("#Asesor").html(CargarCombo(13, 1));
$("#Cliente").html(CargarCombo(9, 1));
$("#OAsesor").html($("#Asesor").html());

$.connection.hub.start().done(function () {

    $('#GuardarRelacion').click(function () {

        var remisiones = $("#OSeleccion").val();
        var observacion = $("#OObservacion").val();
        var asesor = $("#OAsesor").val() * 1;

        if (asesor == 0) {
            $("#OAsesor").focus();
            swal("Acción Cancelada", "Debe de seleccionar un asesor comercial", "warning");
            return false;
        }

        ActivarLoad();
        setTimeout(function () {
            var parametros = "remisiones=" + remisiones + "&asesor=" + asesor;
            var datos = LlamarAjax("Cotizacion/RelacionarRemision", parametros).split("|");
            if (datos[0] == "0") {
                EnviarCorreo(1, remisiones, asesor, observacion)
                DesactivarLoad();
                swal("", datos[1], "success");
                $("#modalRelacion").modal("hide");
                $("#Relacionado").val("-1").trigger("change");
                Consultar();

                idusuario = "/" + asesor + "/";
                var mensaje = "Buen día AMIGO, Se le ha asignado las siguientes remisiones: " + remisiones + ". " + observacion.toLowerCase();
                chat.server.send(usuariochat, mensaje, '', idusuario);
                ArchivoChat = "";
            } else {
                swal("Acción Cancelada",datos[1], "warning")
            }
        }, 15);
    });

    $('#EliminarRelacion').click(function () {
        
        var remision = $("#EliRemision").val()*1;
        var asesor = $("#EliAsero").val() * 1;

        ActivarLoad();
        setTimeout(function () {
            var datos = LlamarAjax("Cotizacion/EliminarRelacionRem", "remision=" + remision).split("|");
            if (datos[0] == "0") {
                EnviarCorreo(2, remision, asesor, "")
                swal("", datos[1], "success");
                $("#Relacionado").val("-1").trigger("change");
                Consultar();
                idusuario = "/" + asesor + "/";
                $("#modalEliminarRel").modal("hide");
                var mensaje = "Buen día AMIGO, Se le ha eliminado la asignado de la remisión " + remision;
                chat.server.send(usuariochat, mensaje, '', idusuario);
                ArchivoChat = "";
            } else {
                swal("Acción Cancelada", datos[1], "warning")
            }
        }, 15);
    });
});

function LlamarEliminarRela(remision, asesor, idasesor) {
    var mensaje = "¿Seguro que desea eliminar la relación de la remisión número " + remision + " del asesor " + asesor + "?";
    $("#detallemensaje").html(mensaje);
    $("#EliRemision").val(remision);
    $("#EliAsero").val(idasesor)
    $("#modalEliminarRel").modal("show");
}

function EnviarCorreo(tipo, remisiones, asesor, observacion) {
    LlamarAjax("Cotizacion/EnvioRelacionRemision", "remisiones=" + remisiones + "&asesor=" + asesor + "&tipo=" + tipo + "&observacion=" + observacion);
}



function RelacionarIngreso() {
    remisiones = "";
    var selec = document.getElementsByName("Remisiones[]");
    for (var x = 0; x < selec.length; x++) {
        if (selec[x].checked) {
            if (remisiones == "") {
                remisiones = selec[x].value;
            } else {
                remisiones += "," + selec[x].value;
            }
        }
    }
    if (remisiones == "") {
        swal("Acción Cancelada", "Debe de seleccionar por lo mínimo una remisión", "warning");
        return false;
    }

    $("#OSeleccion").val(remisiones);
    $("#modalRelacion").modal("show");
}

function SeleccionarFila(x) {
    if ($("#seleccionado" + x).prop("checked")) {
        $("#fila-" + x).addClass("bg-gris");
    } else {
        $("#fila-" + x).removeClass("bg-gris");
    }
}

function Consultar() {
    var remision = $("#Remision").val() * 1;
    var ingresos = $.trim($("#Ingreso").val());
    var relacionado = $("#Relacionado").val() * 1;
    var cliente = $("#Cliente").val()*1;
    var asesor = $("#Asesor").val()*1;
    var fechad = $("#FechaDesde").val();
    var fechah = $("#FechaHasta").val();
    var resultado = "";

    ActivarLoad();
    setTimeout(function () {
        var parametros = "remision=" + remision + "&ingresos=" + ingresos + "&cliente=" + cliente + "&relacionado=" + relacionado + "&fechad=" + fechad + "&fechah=" + fechah + "&usuario=" + asesor;
        var datos = LlamarAjax("Cotizacion/TablaRelaRemision", parametros);
        DesactivarLoad();
        if (datos != "[]") {
            var data = JSON.parse(datos);
            for (var x = 0; x < data.length; x++) {
                resultado += "<tr id='fila-" + x + "' ondblclick='LlamarIngresos(" + data[x].remision + ")'>" +
                    "<td class='text-center'><b>" + (x + 1) + "</b>" + ($.trim(data[x].asesor) == "" && data[x].cotizacion == 0 ?  "<br><input class='form-control' type='checkbox' value='" + data[x].remision + "' name='Remisiones[]' id='seleccionado" + x + "' onchange='SeleccionarFila(" + x + ")'>" : "") + "</td>" +
                    "<td align='center'><a class='text-XX' href='javascript:ImprimirRemision(" + data[x].remision + ")'>" + data[x].remision + "</a></td>" +
                    "<td align='center'><a class='text-XX' href='javascript:ImprimirCotizacion(" + data[x].cotizacion + ")'>" + data[x].cotizacion + "</a>" + "<br>" + data[x].estadoco + "</td>" +
                    "<td>" + data[x].cliente + "</td>" +
                    "<td>" + data[x].sede + "</td>" +
                    "<td>" + data[x].contacto + "</td>" +
                    "<td>" + data[x].fecha + "</td>" +
                    "<td>" + data[x].usuario + "</td>" +
                    "<td>" + data[x].totales + "</td>" +
                    "<td>" + data[x].tiempo + "</td>" +
                    "<td>" + data[x].asesor + "</td>" +
                    "<td>" + (data[x].fechaasignacion ? data[x].fechaasignacion : "") + "</td>" +
                    "<td align='center'>" + (data[x].asesor != "--" ? "<button class='EliminarRelacion btn btn-danger' title='Eliminar relación' type='button' onclick=\"LlamarEliminarRela(" + data[x].remision + ",'" + data[x].asesor + "'," + data[x].idasesor + ")\"><span data-icon='&#xe0d8;'></span></button>" : "&nbsp;") + "</td><tr>";
            }
        }
        $("#TablaRelacion").html(resultado);
    }, 15);
}

function ConsultarSolcitud() {
    var solicitud = $("#SSolicitud").val() * 1;
    var relacionado = $("#SRelacionado").val() * 1;
    var cliente = $("#SCliente").val() * 1;
    var asesor = $("#SAsesor").val() * 1;
    var fechad = $("#SFechaDesde").val();
    var fechah = $("#SFechaHasta").val();
    var resultado = "";

    ActivarLoad();
    setTimeout(function () {
        var parametros = "solicitud=" + solicitud + "&cliente=" + cliente + "&relacionado=" + relacionado + "&fechad=" + fechad + "&fechah=" + fechah + "&usuario=" + asesor;
        var datos = LlamarAjax("Cotizacion/TablaRelaSolicitud", parametros);
        DesactivarLoad();
        if (datos != "[]") {
            var data = JSON.parse(datos);
            for (var x = 0; x < data.length; x++) {
                resultado += "<tr id='fila-" + x + "' ondblclick='LlamarIngresos(" + data[x].remision + ")'>" +
                    "<td class='text-center'><b>" + (x + 1) + "</b>" + (data[x].asesor == " " && data[x].cotizacion == 0 ? "<br><input class='form-control' type='checkbox' value='" + data[x].remision + "' name='Remisiones[]' id='seleccionado" + x + "' onchange='SeleccionarFila(" + x + ")'>" : "") + "</td>" +
                    "<td align='center'><a class='text-XX' href='javascript:ImprimirRemision(" + data[x].remision + ")'>" + data[x].remision + "</a></td>" +
                    "<td align='center'><a class='text-XX' href='javascript:ImprimirCotizacion(" + data[x].cotizacion + ")'>" + data[x].cotizacion + "</a>" + "<br>" + data[x].estadoco + "</td>" +
                    "<td>" + data[x].cliente + "</td>" +
                    "<td>" + data[x].sede + "</td>" +
                    "<td>" + data[x].contacto + "</td>" +
                    "<td>" + data[x].fecha + "</td>" +
                    "<td>" + data[x].usuario + "</td>" +
                    "<td>" + data[x].totales + "</td>" +
                    "<td>" + data[x].tiempo + "</td>" +
                    "<td>" + data[x].asesor + "</td>" +
                    "<td>" + (data[x].fechaasignacion ? data[x].fechaasignacion : "") + "</td>" +
                    "<td align='center'>" + (data[x].asesor != "--" ? "<button class='EliminarRelacion btn btn-danger' title='Eliminar relación' type='button' onclick=\"LlamarEliminarRela(" + data[x].remision + ",'" + data[x].asesor + "'," + data[x].idasesor + ")\"><span data-icon='&#xe0d8;'></span></button>" : "&nbsp;") + "</td><tr>";
            }
        }
        $("#TablaRelacion").html(resultado);
    }, 15);
}


function ImprimirRemision(remision) {
    ActivarLoad();
    setTimeout(function () {
        var parametros = "remision=" + remision;
        var datos = LlamarAjax("Cotizacion/RpRemision", parametros);
        DesactivarLoad();
        datos = datos.split("|");
        if (datos[0] == "0") {
            window.open("DocumPDF/" + datos[1]);
        } else {
            swal("", ValidarTraduccion(datos[1]), "warning");
        }
    }, 15);
}

function ImprimirCotizacion(cotizacion) {
    if (cotizacion * 1 == 0)
        return false;
    ActivarLoad();
    setTimeout(function () {
        var parametros = "cotizacion=" + cotizacion;
        var datos = LlamarAjax("Cotizacion/RpCotizacion", parametros);
        DesactivarLoad();
        datos = datos.split("|");
        if (datos[0] == "0") {
            window.open("DocumPDF/" + datos[1]);
        } else {
            swal("", ValidarTraduccion(datos[1]), "warning");
        }
    }, 15);
}

function LlamarIngresos(cotizacion) {
    localStorage.setItem("idcliente", cotizacion);
    CargarModalIngreso(4);
    $("#modalIngreso").modal("show");
}

$('select').select2();
DesactivarLoad();
