﻿var Ingreso = 0;
var IdReporte = 0;
var Error = 0;
var Foto = 0;

localStorage.setItem("Ingreso", "0");

function LimpiarTodo() {
    LimpiarDatos();
    $("#Ingreso").val("");
}

function LimpiarDatos() {
    Ingreso = 0;
    IdReporte = 0;
    Foto = 0;
    $("#Equipo").val("");
    $("#Marca").val("");
    $("#Modelo").val("");
    $("#Serie").val("");
    $("#Magnitud").val("");
    $("#Observacion").val("");
    $("#Remision").val("");
    $("#Rango").val("");
    localStorage.setItem("Ingreso", "0");

    $("#Usuario").val("");
    $("#FechaIng").val("");
    $("#Tiempo").val("");

    $("#BodyEstados").html("");
    $("#AlbunFotos").html("");
        
}


function ConsularReportes() {
            
    ActivarLoad();
    setTimeout(function () {
        var parametros = "";
        var datos = LlamarAjax("Laboratorio","opcion=TablaReportarIngreso&"+ parametros);
        DesactivarLoad();
        var datajson = JSON.parse(datos);
        $('#TablaReportar').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "fila" },
                { "data": "ingreso" },
                { "data": "equipo" },
                { "data": "fecha" }
            ],

            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy','colvis'
            ]
        });
    }, 15);
}


function BuscarIngreso(numero, code, tipo) {
    if ((Ingreso != (numero*1)) && ($("#Equipo").val() != ""))
        LimpiarDatos();
    if ((code.keyCode == 13 || tipo == "0") && Error == 0) {
        if (numero * 1 > 0) {
            var datos = LlamarAjax("Laboratorio","opcion=BuscarEstadoIngreso&ingreso=" + numero + "&plantilla=0").split("|");
            if (datos[0] != "[]") {
                var data = JSON.parse(datos[0]);
                IdRemision = data[0].id * 1;
                Ingreso = data[0].ingreso * 1;
                localStorage.setItem("Ingreso", Ingreso);
                Fotos = data[0].fotos * 1;
                $("#Equipo").val(data[0].equipo);
                $("#Marca").val(data[0].marca);
                $("#Modelo").val(data[0].modelo);
                $("#Serie").val(data[0].serie);
                $("#Observacion").val(data[0].observacion);
                $("#Accesorio").val(data[0].accesorio);
                $("#Rango").val(data[0].intervalo);
                $("#Remision").val(data[0].remision);
                $("#Magnitud").val(data[0].magnitud);
                                
                $("#Usuario").val(data[0].usuarioing);
                $("#FechaIng").val(data[0].fechaing);

                CargarFotos();
                var resultado = "";
                if (datos[1] != "[]") {
                    var datarep = JSON.parse(datos[1]);

                    for (var x = 0; x < datarep.length; x++) {
                        resultado += "<tr>" +
                            "<td>" + (x + 1) + "</td>" +
                            "<td>" + datarep[x].descripcion + "</td>" +
                            "<td>" + datarep[x].usuario + "</td>" +
                            "<td>" + datarep[x].fecha+ "</td>" +
                            "<td>" + datarep[x].tiempo + "</td></tr>";
                    }
                }
                $("#BodyEstados").html(resultado);
            } else {
                $("#Ingreso").select();
                $("#Ingreso").focus();
                Error = 1;
                swal("Acción Cancelada", "El Ingreso número " + numero + " no se encuentra registrado", "warning");
                return false;
            }
        }
    } else
        Error = 0;
}

function CambiarFoto2(numero) {
    $("#FotoIngreso").attr("src", "imagenes/ingresos/" + Ingreso + "/" + numero + ".jpg");
}

function CargarFotos() {
    var contenedor = "";
    contenedor = "<div class='row'><div class='col-xs-12 col-sm-11'>" +
        "<img id='FotoIngreso' " + (Fotos > 0 ? "src='" + "imagenes/ingresos/" + Ingreso + "/1.jpg'" : "") + " width='70%'></div><div class='col-xs-12 col-sm-1'>";
    for (var x = 1; x <= Fotos; x++) {
        contenedor += "<button class='btn btn-primary' type='button' onclick='CambiarFoto2(" + x + ")'>" + x + "</button>&nbsp;";
    }
    contenedor += "</div></div>";
    $("#AlbunFotos").html(contenedor);
}
   
$('select').select2();
DesactivarLoad();
