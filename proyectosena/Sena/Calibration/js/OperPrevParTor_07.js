﻿d = new Date();
month = d.getMonth() + 1;
day = d.getDate();

var Anio = $.trim(d.getFullYear());
Anio = $.trim(Anio.substr(2, 2))
var PermisioEspecial = PerGeneSistema("PERMISO ADMINISTRADOR LABORATORIO");

output = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-01';

var FechaCal = d.getFullYear() + "-" + (month < 10 ? '0' : '') + month + "-" + (day < 10 ? "0" : "") + day;

var Ingreso = 0;
var ErrorCalculo = 0;
var IdOperacion = 0;
var IdInstrumento = 0;
var ErrorEnter = 0;
var Fotos = 0;
var NroReporte = 1;
var RangoHasta = 0;
var RangoDesde = 0;
var GuardaTemp = 0;
var CorreoEmpresa = "";
var AplicarTemporal = 0;
var InfoTecIngreso = 0;
var Factor = 0;
var IdVersion = 7;
var asesor = "";
var Habitual = "NO";
var Sitio = "";
var Garantia = "NO";
var AprobarCotizacion = "SI";
var FechaAproCoti = "";

var EstadoVersion = LlamarAjax("Configuracion/Estado_VersionDocumento", "id=" + IdVersion) * 1;


localStorage.setItem("Ingreso", "0");

function GenerarCertificado() {
    localStorage.setItem("CerIngreso", Ingreso);
    LlamarOpcionMenu('Laboratorio/Par_Torsional/07/PlaCertiParTor_07', 3, '');
}

function LimpiarParcial() {
    LimpiarDatos();
    $("#IngresoParTor").val("");
    GuardaTemp = 1;
    Ingreso = 0;
}



function LimpiarTodo() {
    GuardaTemp = 0;
    ErrorEnter = 0;
    
    var revision = $("#Revision").val() * 1;
    localStorage.removeItem("Pre-" + Ingreso + "-" + revision);
    localStorage.removeItem("Table-" + Ingreso + "-" + revision)
    Ingreso = 0;
    $("#Revision").val("1").trigger("change");
    $("#Puntos").val("5").trigger("change");
    $("#IngresoParTor").val("");
    GuardaTemp = 1;
}

function LimpiarPlantilla() {
    GuardaTemp = 0;
    LimpiarDatos();
    $("#IngresoParTor").val("");
    GuardaTemp = 1;
}

function LimpiarDatos() {
    ErrorCalculo = 0;
    IdRemision = 0;
    AplicarTemporal = 0;
    Ingreso = 0;
    localStorage.setItem("Ingreso", Ingreso);
    Fotos = 0
    InfoTecIngreso = 0;
    NroReporte = 1;
    $("#Solicitante").html("");
    $("#Certificado").html("");
    $("#Direccion").html("");
    $("#FechaIng").html("");
    $("#FechaCal").html("");

    $("#TIngreso").html("");
    $("#Medida").html("");
    $("#ValorCon").html("");
    $("#UnidadCon").html("");

    $(".medida").html("");

    $("#Equipo").html("");
    $("#Marca").html("");
    $("#Modelo").html("");
    $("#Serie").html("");
    $("#Remision").val("");
    $("#Usuario").val("");
    $("#FechaRec").val("");
    $("#Tiempo").val("");
    $("#Proxima").html("");

    $("#CMetodo").val("");
    $("#CPunto").val("");
    $("#CServicio").val("");
    $("#CObservCerti").val("");
    $("#CProxima").val("");
    $("#CEntrega").val("");
    $("#CAsesorComer").val("");

    $("#RangoMedida_sc").html("");
    $("#RangoMedida_sch").html("");
    $("#MedResolucion").html("");
    RangoHasta = 0;

    $("#Descripcion").val("");
    $("#Indicacion").val("").trigger("change");
    $("#Clase").val("").trigger("change");
    $("#Clasificacion").val("").trigger("change");
    $("#Tolerancia").val("4");
    $("#Resolucion").val("");

    $("#Tolerancia_sch").val("");
    $("#Resolucion_sch").val("");

    $("#PorLectura1").val("20");
    $("#Serie11").val("");
    $("#Serie21").val("");
    $("#Serie31").val("");

    $("#PorLectura2").val("60");
    $("#Serie12").val("");
    $("#Serie22").val("");
    $("#Serie32").val("");

    $("#PorLectura3").val("100");
    $("#Serie13").val("");
    $("#Serie23").val("");
    $("#Serie33").val("");

    
    $("#Usuario").val("");
    $("#FechaRec").val("");
    $("#Tiempo").val("");
    
    $("#tbodycondicion").html("");
    $("#TBSerie").html("");
    $("#TBCalculo").html("");
    $("#ValorCero").html("0");

    $("#Puntos").val("5").trigger("change");
           
    IdInstrumento = 0;

    for (var x = 1; x <= 5; x++) {
        $("#IdOperacion" + x).val("0");
        $("#Observaciones" + x).val("");
        $("#Conclusion" + x).val("");
        $("#Ajuste" + x).val("");
        $("#Suministro" + x).val("");
        $("#registroingreso" + x).html("");
        $("#Concepto" + x).val("").trigger("change");
    }
}


function GeneralCertificado() {
    if (GuardaTemp == 1 || IdOperacion == 0) {
        swal("Acción Cancelada", "Debe primero guardar el reporte", "warning");
        return false;
    }

    if ((concepto != 2) && (concepto != 1)) {
        swal("Acción Cancelada", "No se puede generar certificado cuando no está apto a calibrar o en subcontrataciones", "warning");
        return false;
    }

    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: '¿Seguro que desea generar el informe técnico del ingreso número ' + Ingreso + '?',
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Si'),
        cancelButtonText: ValidarTraduccion('No'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                $.post("Laboratorio/InformeTecnicoDev", "ingreso=" + Ingreso + "&reporte=0")
                    .done(function (data) {
                        datos = data.split("|");
                        if (datos[0] == "0") {
                            $("#NroInforme" + reporte).val(datos[2]);
                            ImprimirInforme();
                            resolve()
                        } else {
                            reject(datos[1]);
                        }
                    })
            })
        }
    }]);
}

function EnviarCotizacion() {

    var concepto = $("#Concepto").val() * 1;


    if (Ingreso == 0)
        return false;
    if (GuardaTemp == 1 || IdOperacion == 0) {
        swal("Acción Cancelada", "Debe primero guardar el reporte", "warning");
        return false;
    }

    $("#eCliente").val($("#Solicitante").html());
    $("#eCorreo").val(CorreoEmpresa);


    if (concepto == 2) {
        $("#spcotizacion").html(Ingreso);
        $("#enviar_cotizacion").modal("show");
    } else {
        swal("Acción Cancelada", "Solo se podra enviar cotizaciones cuando el ingreso requiera ajuste o suministro", "warning");
    }
    
}

$("#formenviarcotizacion").submit(function (e) {

    e.preventDefault();
    var observacion = $.trim($("#ObserEnvio").val())
    var correo = $.trim($("#eCorreo").val());
    a_correo = correo.split(";");

    if (ValidarCorreo(CorreoEmpresa) == 2) {
        swal(ValidarTraduccion("Acción Cancelada"), ValidarTraduccion("El correo electrónico del contacto no es válido ") + ":" + CorreoEmpresa, "warning");
        //return false;
    }

    for (var x = 0; x < a_correo.length; x++) {
        if (ValidarCorreo($.trim(a_correo[x])) == 2) {
            $("#eCorreo").focus();
            swal(ValidarTraduccion("Acción Cancelada"), ValidarTraduccion("Formato de correo electrónico no válido") + ":" + a_correo[x], "warning");
            return false;
        }
    }

    
    ActivarLoad();
    setTimeout(function () {
        var parametros = "magnitud=6&ingreso=" + Ingreso + "&principal=" + CorreoEmpresa + "&e_email=" + correo + "&observacion=" + observacion;
        var datos = LlamarAjax("Laboratorio/EnvioCotizacion", parametros);
        DesactivarLoad();
        datos = datos.split("|");
        if (datos[0] == "0") {
            swal("", ValidarTraduccion(datos[1]) + " " + datos[2], "success");
            $("#enviar_cotizacion").modal("hide");
        } else
            swal("Acción Cancelada", datos[1], "warning");
    }, 15);

    return false;
});


function LlamarFotoDet(ingreso, fotos) {
    if (fotos == 0)
        return false
    CargarModalFoto(ingreso, fotos, 1);
}


function ConsularReportesIngresos() {
            
    ActivarLoad();
    setTimeout(function () {
        var cliente = $("#ClienteIngreso").val() * 1;
        var tipo = $("#TipoTabla").val() * 1;
        var parametros = "magnitud=6&cliente=" + cliente + "&tipobusqueda=" + tipo;
        var datos = LlamarAjax("Laboratorio/TablaReportarIngreso", parametros);
        DesactivarLoad();
        var datajson = JSON.parse(datos);
        $('#TablaReportar').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "fila" },
                { "data": "ingreso" },
                { "data": "plantilla" },
                { "data": "equipo" },
                { "data": "fecha" },
                { "data": "tiempo" }
            ],

            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy','colvis'
            ]
        });
    }, 15);
}


function CambioCondicion(tipo) {
        
    var resultado = "";
    if ($.trim(tipo) != "") {
        if (tipo == "TIPO I")
            tipo = "/1/";
        else
            tipo = "/2/";

        var datos = LlamarAjax("Laboratorio/CambioCondicion", "ingreso=" + Ingreso + "&magnitud=6" + "&tipo=" + tipo);

        if (datos != "[]") {
            var datacon = JSON.parse(datos);
            var valores = "";
            var opcion = 0;
            var radio = "";
            for (var x = 0; x < datacon.length; x++) {
                valores = datacon[x].opcion.split("!!");
                opcion = valores[0] * 1;
                switch (opcion) {
                    case 0:
                        radio = "<td><input type='radio' onchange='GuardarTemporal(1)' required class='form-control' name='Opcion" + x + "' id ='CSi" + x + "'></td>" +
                            "<td><input type='radio' onchange='GuardarTemporal(1)' required class='form-control' name='Opcion" + x + "' id ='CNo" + x + "'></td>" +
                            "<td><input type='radio' onchange='GuardarTemporal(1)' required class='form-control' name='Opcion" + x + "' id ='CNA" + x + "'></td>";
                        break;
                    case 1:
                        radio = "<td><input type='radio' onchange='GuardarTemporal(1)' required checked class='form-control' name='Opcion" + x + "' id ='CSi" + x + "'></td>" +
                            "<td><input type='radio' onchange='GuardarTemporal(1)' required class='form-control' name='Opcion" + x + "' id ='CNo" + x + "'></td>" +
                            "<td><input type='radio' onchange='GuardarTemporal(1)' required class='form-control' name='Opcion" + x + "' id ='CNA" + x + "'></td>";
                        break;
                    case 2:
                        radio = "<td><input type='radio' onchange='GuardarTemporal(1)' required class='form-control' name='Opcion" + x + "' id ='CSi" + x + "'></td>" +
                            "<td><input type='radio' onchange='GuardarTemporal(1)' required checked class='form-control' name='Opcion" + x + "' id ='CNo" + x + "'></td>" +
                            "<td><input type='radio' onchange='GuardarTemporal(1)' required class='form-control' name='Opcion" + x + "' id ='CNA" + x + "'></td>";
                        break;
                    case 3:
                        radio = "<td><input type='radio' onchange='GuardarTemporal(1)' required class='form-control' name='Opcion" + x + "' id ='CSi" + x + "'></td>" +
                            "<td><input type='radio' onchange='GuardarTemporal(1)' required class='form-control' name='Opcion" + x + "' id ='CNo" + x + "'></td>" +
                            "<td><input type='radio' onchange='GuardarTemporal(1)' required checked class='form-control' name='Opcion" + x + "' id ='CNA" + x + "'></td>";
                        break;
                }
                resultado += "<tr>" +
                    "<td class='bg-gris'>" + datacon[x].descripcion + "</td>" + radio +
                    "<td><input class='form-control sinbordecon bg-amarillo' name='CObservacion[]' value='" + valores[1] + "'>" +
                    "<input type='hidden' value='" + datacon[x].id + "' name='Ids[]'><input type='hidden' value='" + datacon[x].descripcion + "' name='Descripciones[]'></td></tr>";
            }
        }
    }
            
    $("#tbodycondicion").html(resultado);
    BuscarDescripcionEquipo();
    GuardarTemporal(1);
}


ConsularReportesIngresos();

$.connection.hub.start().done(function () {

    $('#IngresoParTor').keyup(function (event) {
        numero = this.value*1;
        if (numero == -1)
            numero = Ingreso;
                
        if ((Ingreso != (numero * 1)) && ($("#Equipo").html() != "")) {
            GuardaTemp = 1;
            LimpiarDatos();
            Ingreso = 0;
            ErrorEnter = 0;
        }
        if (event.which == 13) {
            event.preventDefault();

            if (Ingreso == numero)
                return false;
            BuscarIngreso(numero);
        }
    });
});

function BuscarIngreso(numero) {
    var revision = $("#Revision").val() * 1

    if (ErrorEnter == 0) {
        if (numero * 1 > 0) {
            GuardaTemp = 0;
            var datos = LlamarAjax("Laboratorio/BuscarIngresoOperacion", "magnitud=6&ingreso=" + numero + "&medida=N·m&revision=" + revision + "&version=" + IdVersion).split("|");
            if (datos[0] != "[]") {
                var data = JSON.parse(datos[0]);

                if (data[0].idmagnitud != "6") {
                    swal("Acción Cancelada", "El ingreso número " + numero + " no pertenece a la magnitud <br><b>PAR TORSIONAL</b>", "warning");
                    ErrorEnter = 1;
                    $("#IngresoParTor").select();
                    $("#IngresoParTor").focus();
                    return false;
                }

                IdOperacion = 0;
                NumReporte = 0;
                if (datos[1] != "[]") {
                    var dataope = JSON.parse(datos[1]);
                    IdOperacion = dataope[0].id;
                    $("#Puntos").val(dataope[0].puntos).trigger("change");
                    swal("Advertencia", "Este ingreso ya se le registró la operación previa", "info")
                    $("#registroingreso").html("<b>Registado el día " + dataope[0].fecha + " por el técnico " + dataope[0].usuario + "</b>");
                }

                if (IdOperacion == 0) {
                    if (EstadoVersion == 0) {
                        ErrorEnter = 1;
                        swal("Acción Cancelada", "Esta versión ha caducado, solo se puede usar para consultar reportas ya realizados", "warning");
                        LimpiarTodo();
                        $("#IngresoParTor").select();
                        $("#IngresoParTor").focus();
                        return false;
                    }
                }

                IdRemision = data[0].id * 1;
                Ingreso = data[0].ingreso * 1;
                localStorage.setItem("Ingreso", Ingreso);
                Fotos = data[0].fotos * 1;
                asesor = "/" + data[0].asesor + "/";
                Sitio = data[0].sitio;
                Garantia = data[0].garantia;
                Habitual = data[0].habitual;
                FechaAproCoti = $.trim(data[0].fechaaprocoti);
                AprobarCotizacion = data[0].aprobar_cotizacion;
                $("#Solicitante").html(data[0].cliente);
                $("#Certificado").html(data[0].certificado);
                $("#Direccion").html(data[0].direccion);
                $("#FechaIng").html(data[0].fechaing);
                $("#FechaCal").html(FechaCal);
                CorreoEmpresa = data[0].email;

                $("#TIngreso").html(data[0].ingreso);
                $("#Medida").html(data[0].medida);
                $("#ValorCon").html(data[0].medidacon);
                Factor = data[0].medidacon;
                $("#UnidadCon").html("1 " + data[0].medida + " =");

                $(".medida").html(data[0].medida);

                $("#Equipo").html(data[0].equipo);
                $("#Marca").html(data[0].marca);
                $("#Modelo").html(data[0].modelo);
                $("#Proxima").html(data[0].proxima);
                $("#Serie").html(data[0].serie);
                $("#Remision").val(data[0].remision);
                InfoTecIngreso = data[0].informetecnico;


                $("#RangoMedida_sc").html("(" + data[0].desde + " a " + data[0].hasta + ")" );
                $("#RangoMedida_sch").html("(" + (data[0].desde*-1) + " a " + (data[0].hasta*-1) + ")");
                $("#MedResolucion").html(data[0].medida);
                RangoHasta = data[0].hasta * 1;
                RangoDesde = data[0].desde * 1;

                $("#Descripcion").val((data[0].descripcion ? data[0].descripcion : ""));
                $("#Tolerancia").val((data[0].tolerancia ? data[0].tolerancia : ""));
                $("#Resolucion").val((data[0].resolucion ? data[0].resolucion : ""));
                $("#Tolerancia_sch").val((data[0].tolerancia ? data[0].tolerancia_sch : ""));
                $("#Resolucion_sch").val((data[0].resolucion ? data[0].resolucion_sch : ""));
                $("#Indicacion").val((data[0].indicacion ? data[0].indicacion : "")).trigger("change");
                $("#Clase").val((data[0].clase ? data[0].clase : "")).trigger("change");
                $("#Clasificacion").val((data[0].clasificacion ? data[0].clasificacion : "")).trigger("change");
                
                IdInstrumento = (data[0].idinstrumento ? data[0].idinstrumento : 0);

                $("#Usuario").val(data[0].usuarioing);
                $("#FechaRec").val(data[0].fecharec);
                $("#Tiempo").val(data[0].tiempo);

                $("#Plantilla").html(datos[3]);

                //MODAL DE COTIZACIONES

                if (datos[2] != "[]") {
                    var dataco = JSON.parse(datos[2]);
                    if (IdOperacion == 0) {
                        if (Habitual == "NO" && AprobarCotizacion == "SI" && Sitio == "NO" && Garantia == "NO" && FechaAproCoti == "") {
                            if (dataco[0].estado != "Aprobado" && dataco[0].estado != "POR REEMPLAZAR") {
                                ErrorEnter = 1;
                                swal("Acción Cancelada", "La cotizacíon número " + dataco[0].cotizacion + " no ha sido aprobada por el cliente", "warning");
                                chat.server.send(usuariochat, "Buen días amigo. se requiere la aprobación de la cotización número " + dataco[0].cotizacion + " para proceder a la calibración", '', asesor);
                                if (PermisioEspecial == 0) {
                                    LimpiarDatos();
                                    return false;
                                }
                            }
                        }
                    }
                    for (var x = 0; x < dataco.length; x++) {
                        if (x == 0) {
                            $("#CMetodo").val(dataco[x].metodo);
                            $("#CPunto").val(dataco[x].punto);
                            $("#CServicio").val(dataco[x].servicio);
                            $("#CObservCerti").val(dataco[x].observacion);
                            $("#CProxima").val(dataco[x].proxima);
                            $("#CEntrega").val(dataco[x].entrega);
                            $("#CAsesorComer").val(dataco[x].asesor);
                        } else {
                            $("#CMetodo").val("<br>" + dataco[x].metodo);
                            $("#CPunto").val("<br>" + dataco[x].punto);
                            $("#CServicio").val("<br>" + dataco[x].servicio);
                            $("#CObservCerti").val("<br>" + dataco[x].observacion);
                            $("#CProxima").val("<br>" + dataco[x].proxima);
                            $("#CEntrega").val("<br>" + dataco[x].entrega);
                            $("#CAsesorComer").val("<br>" + dataco[x].asesor);
                        }
                    }
                } else {
                    if (IdOperacion == 0) {
                        if (Habitual == "NO" && AprobarCotizacion == "SI" && Sitio == "NO" && Garantia == "NO" && FechaAproCoti == "") {
                            ErrorEnter = 1;
                            swal("Acción Cancelada", "Este ingreso no ha sido cotizado por el asesor comercial", "warning");
                            chat.server.send(usuariochat, "Buen días amigo. se requiere realizar la cotizacion y aprobación del ingreso número " + Ingreso + " para proceder a la calibración", '', asesor);
                            if (PermisioEspecial == 0) {
                                LimpiarDatos();
                                return false;
                            }
                        }
                    }
                }

                if (IdOperacion == 0) {
                    if (localStorage.getItem("Pre-" + Ingreso + "-" + revision)) {
                        var data = JSON.parse(localStorage.getItem("Pre-" + Ingreso + "-" + revision));
                        $("#Descripcion").val(data[0].descripcion);
                        $("#Tolerancia").val(data[0].tolerancia);
                        $("#Resolucion").val(data[0].resolucion);
                        $("#Tolerancia_sch").val(data[0].tolerancia_sch);
                        $("#Resolucion_sch").val(data[0].resolucion_sch);
                        $("#Indicacion").val(data[0].indicacion).trigger("change");
                        $("#Clase").val(data[0].clase).trigger("change");
                        $("#Clasificacion").val(data[0].clasificacion).trigger("change");
                        $("#Puntos").val(data[0].puntos).trigger("change");
                    }
                    if (localStorage.getItem("Table-" + Ingreso + "-" + revision))
                        $("#tbodycondicion").html(localStorage.getItem("Table-" + Ingreso + "-" + revision));
                    GuardaTemp = 1;
                    
                } else {
                    AplicarTemporal = 1;
                }



            } else {
                $("#IngresoParTor").select();
                $("#IngresoParTor").focus();
                ErrorEnter = 1;
                swal("Acción Cancelada", "El Ingreso número " + numero + " no se ha recibido en el laboratorio", "warning");
                return false;
            }
        }
    } else
        ErrorEnter = 0;
}
  


function BuscarDescripcionEquipo() {
    var clase = $("#Clase").val();
    var tipo = $("#Clasificacion").val();
    $("#Descripcion").html("");
    $("#Lecturas").val("");
    if (clase != "" && tipo != "") {
        var datos = LlamarAjax("Laboratorio/BuscarDescripcionEquipo", "tipo=" + tipo + "&clase=" + clase).split("||");
        if (datos != "XX") {
            $("#Descripcion").html(datos[0]);
            $("#Lecturas").val(datos[1]);
        }
    }
    GuardarTemporal();
}

function GuardarTemporal(tabla) {
    var puntos = $("#Puntos").val() * 1;
    var revision = $("#Revision").val() * 1;
    if (Ingreso > 0 && GuardaTemp == 1) {
        
        var archivo = "[{";
        archivo += '"tolerancia":"' + $("#Tolerancia").val() + '","resolucion":"' + $("#Resolucion").val() + '",' +
            '"tolerancia_sch":"' + $("#Tolerancia_sch").val() + '","resolucion_sch":"' + $("#Resolucion_sch").val() + '",' +
            '"descripcion":"' + $("#Descripcion").val() + '","clase":"' + $("#Clase").val() + '","indicacion":"' + $("#Indicacion").val() + '",' +
            '"clasificacion":"' + $("#Clasificacion").val() + '",' +
            '"idinstrumento":"' + IdInstrumento + '",' + 
            '"id":"' + IdOperacion + '","ingreso":"' + Ingreso + '","puntos":"' + puntos + '"';
        archivo += '}]';
                
        if (tabla) {
            var id = document.getElementsByName("Ids[]");
            var observacion = document.getElementsByName("CObservacion[]");
            var descripcion = document.getElementsByName("Descripciones[]");

            var opcion = 0;
            var resultado = "";
            var radio = "";

            for (var x = 0; x < id.length; x++) {
                opcion = 0;
                if ($("#CSi" + x).prop("checked"))
                    opcion = 1;
                else {
                    if ($("#CNo" + x).prop("checked"))
                        opcion = 2;
                    else {
                        if ($("#CNA" + x).prop("checked"))
                            opcion = 3;
                    }
                }

                switch (opcion) {
                    case 0:
                        radio = "<td><input type='radio' onchange='GuardarTemporal(1)' required class='form-control' name='Opcion" + x + "' id ='CSi" + x + "'></td>" +
                            "<td><input type='radio' onchange='GuardarTemporal(1)' required class='form-control' name='Opcion" + x + "' id ='CNo" + x + "'></td>" +
                            "<td><input type='radio' onchange='GuardarTemporal(1)' required class='form-control' name='Opcion" + x + "' id ='CNA" + x + "'></td>";
                        break;
                    case 1:
                        radio = "<td><input type='radio' onchange='GuardarTemporal(1)' required checked class='form-control' name='Opcion" + x + "' id ='CSi" + x + "'></td>" +
                            "<td><input type='radio' onchange='GuardarTemporal(1)' required class='form-control' name='Opcion" + x + "' id ='CNo" + x + "'></td>" +
                            "<td><input type='radio' onchange='GuardarTemporal(1)' required class='form-control' name='Opcion" + x + "' id ='CNA" + x + "'></td>";
                        break;
                    case 2:
                        radio = "<td><input type='radio' onchange='GuardarTemporal(1)' required class='form-control' name='Opcion" + x + "' id ='CSi" + x + "'></td>" +
                            "<td><input type='radio' onchange='GuardarTemporal(1)' required checked class='form-control' name='Opcion" + x + "' id ='CNo" + x + "'></td>" +
                            "<td><input type='radio' onchange='GuardarTemporal(1)' required class='form-control' name='Opcion" + x + "' id ='CNA" + x + "'></td>";
                        break;
                    case 3:
                        radio = "<td><input type='radio' onchange='GuardarTemporal(1)' required class='form-control' name='Opcion" + x + "' id ='CSi" + x + "'></td>" +
                            "<td><input type='radio' onchange='GuardarTemporal(1)' required class='form-control' name='Opcion" + x + "' id ='CNo" + x + "'></td>" +
                            "<td><input type='radio' onchange='GuardarTemporal(1)' required checked class='form-control' name='Opcion" + x + "' id ='CNA" + x + "'></td>";
                        break;
                }
                resultado += "<tr>" +
                    "<td class='bg-gris'>" + descripcion[x].value + "</td>" + radio +
                    "<td><input class='form-control sinbordecon bg-amarillo' name='CObservacion[]' value='" + observacion[x].value + "'>" +
                    "<input type='hidden' value='" + id[x].value + "' name='Ids[]'><input type='hidden' value='" + descripcion[x].value + "' name='Descripciones[]'></td></tr>";
            }
            localStorage.setItem("Table-" + Ingreso + "-" + revision, resultado);
            console.log(localStorage.getItem("Table-" + Ingreso + "-" + revision))
        }


        localStorage.setItem("Pre-" + Ingreso + "-" + revision, archivo);

        $.jGrowl("Guardando Temporal", { life: 1500, theme: 'growl-warning', header: '' });
    } else {
        if (AplicarTemporal == 1)
            GuardaTemp = 1;
    }
}

function CambioConcepto(concepto, tipo) {
    var prefijo = (tipo == 0 ? "" : tipo);

    $("#Suministro" + prefijo).prop("disabled", true);
    $("#Ajuste" + prefijo).prop("disabled", true);
    $("#Conclusion" + prefijo).prop("disabled", true);
    $("#Observacion" + prefijo).prop("disabled", false);

    $("#Observacion" + prefijo).addClass("bg-amarillo");
    $("#Ajuste" + prefijo).removeClass("bg-amarillo");
    $("#Suminstro" + prefijo).removeClass("bg-amarillo");
    $("#Conclusion" + prefijo).removeClass("bg-amarillo");

    $("#Documento" + prefijo).removeClass("bg-amarillo");
    $("#NumFoto" + prefijo).removeClass("bg-amarillo");
    $("#Documento" + prefijo).prop("disabled", true);
    $("#NumFoto" + prefijo).prop("disabled", true);

    if (concepto == "2") {
        $("#Suministro" + prefijo).prop("disabled", false);
        $("#Ajuste" + prefijo).prop("disabled", false);
        $("#Observacion" + prefijo).prop("disabled", true);
        $("#Conclusion" + prefijo).prop("disabled", true);

        $("#Observacion" + prefijo).removeClass("bg-amarillo");
        $("#Ajuste" + prefijo).addClass("bg-amarillo");
        $("#Suministro" + prefijo).addClass("bg-amarillo");
        $("#Conclusion" + prefijo).removeClass("bg-amarillo");

    } else {
        if (concepto == "3" || concepto == "5") {
            $("#Suministro" + prefijo).prop("disabled", true);
            $("#Ajuste" + prefijo).prop("disabled", true);
            $("#Observacion" + prefijo).prop("disabled", false);
            $("#Conclusion" + prefijo).prop("disabled", false);

            $("#Observacion" + prefijo).addClass("bg-amarillo");
            $("#Ajuste" + prefijo).removeClass("bg-amarillo");
            $("#Suministro" + prefijo).removeClass("bg-amarillo");
            $("#Conclusion" + prefijo).addClass("bg-amarillo");
        
            $("#Documento" + prefijo).addClass("bg-amarillo");
            $("#NumFoto" + prefijo).removeClass("bg-amarillo");
            $("#Documento" + prefijo).prop("disabled", false);
            $("#NumFoto" + prefijo).prop("disabled", false);
        }
    }

    if (tipo == 0)
        GuardarTemporal();

}


$.connection.hub.start().done(function () {
    $("#FormPrevia").submit(function (e) {
        e.preventDefault();

        var id = document.getElementsByName("Ids[]");
        var observacion = document.getElementsByName("CObservacion[]");

        var a_id = "";
        var a_observacion = "";
        var a_opcion = "";
        var opcion = 0;

        for (var x = 0; x < id.length; x++) {
            if ($("#CSi" + x).prop("checked"))
                opcion = 1;
            else {
                if ($("#CNo" + x).prop("checked"))
                    opcion = 2
                else {
                    if ($("#CNA" + x).prop("checked"))
                        opcion = 3
                }
            }
            if (a_id == "") {
                a_id += id[x].value;
                a_observacion = observacion[x].value + "";
                a_opcion += opcion;
            } else {
                a_id += "|" + id[x].value;
                a_observacion += "|" + observacion[x].value + "";
                a_opcion += "|" + opcion;
            }
        }
        var parametros = $("#FormPrevia").serialize().replace(/\%2C/g, "") + "&a_id=" + a_id + "&a_observacion=" + a_observacion + "&a_opcion=" + a_opcion + "&idinstrumento=" + IdInstrumento +
            "&id=" + IdOperacion + "&concepto=&Descripcion=" + $("#Descripcion").html() + "&idconcepto=10&ValorCero=0&Version=" + IdVersion;
        var datos = LlamarAjax("Laboratorio/GuarOperParTor", parametros).split("|");
        if (datos[0] == "0") {
            IdOperacion = datos[2];
            IdInstrumento = datos[3];
            GuardaTemp = 0;
            var plantilla = $("#Plantilla").val() * 1;
            var revision = $("#Revision").val() * 1;
            localStorage.removeItem("Pre-" + Ingreso + "-" + revision);
            localStorage.removeItem("Table-" + Ingreso + "-" + revision);
            swal("", datos[1], "success");
            ConsularReportesIngresos();
        } else {
            swal("Acción Cancelada", datos[1], "warning");
        }
    });
});

function VistaInforme(reporte) {

    if (Ingreso == 0)
        return false;

    var concepto = 0;
    if (reporte > 0) {
        concepto = $("#Concepto" + reporte).val() * 1;
    }

    ActivarLoad();
    setTimeout(function () {
        var parametros = "ingreso=" + Ingreso + "&tipo=2&concepto=" + concepto;
        var datos = LlamarAjax("Laboratorio/RpInformeTecnico", parametros);
        DesactivarLoad();
        datos = datos.split("|");
        if (datos[0] == "0") {
            window.open(url + "DocumPDF/" + datos[1]);
        } else {
            swal("", ValidarTraduccion(datos[1]), "warning");
        }

    }, 15);
}

function ImprimirInforme(reporte) {
        
    if (Ingreso == 0)
        return false;

    var concepto = 0;
    if (reporte > 0) {
        concepto = $("#Concepto" + reporte).val() * 1;
    }

    ActivarLoad();
    setTimeout(function () {
        var parametros = "ingreso=" + Ingreso + "&tipo=1&concepto=" + concepto;
        var datos = LlamarAjax("Laboratorio/RpInformeTecnico", parametros);
        DesactivarLoad();
        datos = datos.split("|");
        if (datos[0] == "0") {
            window.open("DocumPDF/" + datos[1]);
        } else {
            swal("", ValidarTraduccion(datos[1]), "warning");
        }

    }, 15);
}

function InformeTecnico(tipo) {

    var concepto = $("#Concepto").val() * 1;
    var reporte = 0;
    var plantilla = $("#Plantilla").val() * 1;
    var id = IdOperacion;
    var temporal = GuardarTemporal;
    if (tipo > 0) {
        concepto = $("#Concepto" + tipo).val() * 1;
        plantilla = 1;
        id = $("#IdOperacion" + tipo).val() * 1;
        temporal = 0;
        reporte = NroReporte;
    }  
    if (Ingreso == 0)
        return false;

    if (temporal == 1 || id == 0) {
        swal("Acción Cancelada", "Debe primero guardar el reporte", "warning");
        return false;
    }
        
    if (concepto != 3 && concepto != 5) {
        swal("Acción Cancelada", "Solo se podrá generar informes cuando el ingreso no está apto para calibrar ó para mantenimiento y limpieza", "warning");
        return false;
    }

    if (InfoTecIngreso == 1) {

        ImprimirInforme(0);
        return false;
    }

    var mensaje = '¿Seguro que desea generar el informe técnico del ingreso número ' + Ingreso + '?';
    if (concepto == 5)
        mensaje = '¿Seguro que desea generar el informe de mantenimiento y limpieza del ingreso número ' + Ingreso + '?';

    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: mensaje,
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Si'),
        cancelButtonText: ValidarTraduccion('No'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                $.post("Laboratorio/InformeTecnicoDev", "ingreso=" + Ingreso + "&reporte=" + reporte + "&plantilla=" + plantilla)
                    .done(function (data) {
                        datos = data.split("|");
                        if (datos[0] == "0") {
                            $("#NroInforme" + reporte).val(datos[2]);
                            ImprimirInforme();
                            resolve()
                        } else {
                            reject(datos[1]);
                        }
                    })
            })
        }
    }]);
};
    
function VerFotos() {
    if (Ingreso == 0)
        return false;
    CargarModalFoto(Ingreso, Fotos,1);
}

function ReportarEquipo() {
    if (Ingreso == 0)
        return false;
    $("#Plantilla").val("").trigger("change");
    $('#tabreportaringreso a[href="#Reporte1"]').tab('show')
    NroReporte = 1;
    $("#ReportarEquipo").modal("show");
    $("#Concepto1").focus();
}

function CambioPlantilla(plantilla) {
    plantilla = plantilla * 1;

    IdOperacion = 1;
    $("#IdReporte" + IdOperacion).val("0");
    $("#Concepto" + IdOperacion).val("").trigger("change");
    $("#Ajuste" + IdOperacion).val("");
    $("#Suministro" + IdOperacion).val("");
    $("#Observacion" + IdOperacion).val("");
    $("#Conclusion" + IdOperacion).val("");
    $("#NroInforme" + IdOperacion).val("");
    $("#registroingreso" + IdOperacion).html("");

    if (plantilla == 0)
        return false;

    var datos = LlamarAjax("Laboratorio/BuscarReportesLab", "ingreso=" + Ingreso + "&plantilla=" + plantilla);
    if (datos != "[]") {
        var dataope = JSON.parse(datos);
        for (var x = 0; x < dataope.length; x++) {
            IdOperacion = dataope[x].nroreporte * 1;
            $("#IdReporte" + IdOperacion).val(dataope[x].id);
            $("#Concepto" + IdOperacion).val(dataope[x].idconcepto).trigger("change");
            $("#Ajuste" + IdOperacion).val(dataope[x].ajuste);
            $("#Suministro" + IdOperacion).val(dataope[x].suministro);
            $("#Observacion" + IdOperacion).val(dataope[x].observaciones);
            $("#Conclusion" + IdOperacion).val(dataope[x].conclusion);
            $("#NroInforme" + IdOperacion).val(dataope[0].informetecnico)
            $("#registroingreso" + IdOperacion).html("<b>Registrado el día " + dataope[x].fecha + " por el técnico " + dataope[x].usuario + "</b>" + dataope[x].aprobado);
            if (dataope[x].idconcepto * 1 == 5 || dataope[x].idconcepto * 1 == 3)
                MostrarFoto(IdOperacion, 1);
        }
    }
}

$("#formreportar").submit(function (e) {
    e.preventDefault();
    var idconcepto = $("#RConcepto").val() * 1;
    var concepto = $("#RConcepto option:selected").text();
    var ajuste = $.trim($("#RAjuste").val());
    var suministro = $.trim($("#RSuministro").val());

    if (idconcepto == 2 && ajuste == "" && suministro == "") {
        $("#RAjuste").focus();
        swal("Acción Cancelada", "Debe ingresar la descripción del ajuste y/ó suministro", "warning");
        return false;
    }

    var parametros = $("#formreportar").serialize() + "&concepto=" + concepto + "&idconcepto=" + idconcepto + "&ingreso=" + Ingreso + "&fecha=" + $("#FechaRec").val();
    var datos = LlamarAjax("Laboratorio/ReportarIngresoOpera", parametros).split("|");
    if (datos[0] == "0") {
        swal("", datos[1], "success");
        $("#IdOperacion").val(datos[2]);
        var numero = Ingreso;
        Ingreso = 0;
        ConsularReportesIngresos();
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
});

function ModalCotizacion() {
    if (Ingreso == 0)
        return false;
    $("#modalcotizacion").modal("show");
}

function EnviarReporte(reporte) {

    var id = $("#IdOperacion" + reporte).val() * 1;
    if (Ingreso == 0 || id == 0)
        return false;



    if (id == 0) {
        swal("Acción Cancelada", "Debe primero guardar el reporte", "warning");
        return false;
    }

    var concepto = $("#Concepto").val() * 1;
    if (reporte > 0)
        concepto = $("#Concepto" + reporte).val() * 1;
    if (concepto != 2) {
        swal("Acción Cancelada", "Solo se podrá enviar reportes técnicos cuando el ingreso requiere ajuste o suministro", "warning");
        return false;
    }

    var datos = LlamarAjax('Cotizacion/ModalEmpresa', "ingreso=" + Ingreso);
    var data = JSON.parse(datos);

    CorreoEmpresa = data[0].email;
    Empresa = data[0].cliente;

    if (ValidarCorreo(CorreoEmpresa) == 2) {
        swal("Acción Cancela", "el contacto " + data[0].contacto + " no posee correo válido registrado <br>Llame a comercial para registrar el correo del contacto", "warning");
        return false;
    }
    $("#rCliente").val(Empresa);
    $("#rCorreo").val(CorreoEmpresa);

    $("#rObserEnvio").val("");
    $("#rspreporte").html(Ingreso);
    $("#rReporte").val(reporte);
    $("#enviar_reporte").modal("show");
}

$.connection.hub.start().done(function () {

    $(".guardarreporte").click(function () {
        var reporte = NroReporte;
        var IdOperacion = $("#IdReporte" + reporte).val() * 1;
        var idconcepto = $("#Concepto" + reporte).val() * 1;
        var ajuste = $.trim($("#Ajuste" + reporte).val());
        var suministro = $.trim($("#Suministro" + reporte).val());
        var observacion = $.trim($("#Observacion" + reporte).val());
        var conclusion = $.trim($("#Conclusion" + reporte).val());
        var concepto = $("#Concepto" + reporte + " option:selected").text();
        var fecha = $("#FechaRec").val();
        var plantilla = $("#Plantilla").val() * 1;
        if (Ingreso == 0)
            return false;
        var mensaje = "";

        if ((idconcepto == 3 || idconcepto == 4) && observacion == "") {
            $("#Observacion" + reporte).focus();
            mensaje = "<br> Debe de ingresar la observación del por qué no se puede calibrar";
        }
        if (idconcepto == 3 && conclusion == "") {
            $("#Conclusion" + reporte).focus();
            mensaje = "<br> Debe de ingresar la conclusión del por qué no se puede calibrar" + mensaje;
        }

        if (idconcepto == 5 && conclusion == "") {
            $("#Conclusion" + reporte).focus();
            mensaje = "<br> Debe de ingresar la conclusión del informe de mantenimiento" + mensaje;
        }

        if (idconcepto == 2 && ajuste == "" && suministro == "") {
            $("#Ajuste" + reporte).focus();
            mensaje = "<br> Debe de ingresar la observación del juste o suministro" + mensaje;
        }
        if (idconcepto == 0) {
            $("#Concepto" + reporte).focus()
            mensaje = "<br> Debe de seleccionar un concepto " + mensaje;
        }
        if (plantilla == 0) {
            $("#Plantilla").focus()
            mensaje = "<br> Debe de seleccionar una plantilla" + mensaje;
        }


        if (mensaje != "") {
            swal("Acción Cancelada", mensaje, "warning");
            return false;
        }

        var parametros = "ingreso=" + Ingreso + "&idconcepto=" + idconcepto + "&concepto=" + concepto + "&ajuste=" + ajuste + "&suministro=" + suministro + "&id=" + IdOperacion + "&observacion=" + observacion + "&reporte=" + reporte + "&fecha=" + fecha + "&conclusion=" + conclusion + "&plantilla=" + plantilla;
        var datos = LlamarAjax("Laboratorio/ReportarIngreso", parametros).split("|");
        if (datos[0] == "0") {
            notireportados.server.send(Ingreso);
            ConsularReportesIngresos();
            if (idconcepto == 1 || idconcepto == 4) {
                LimpiarTodo();
                $("#IngresoParTor").focus();
         
            } else {
                $("#IdOperacion" + reporte).val(datos[2]);
            }
            swal("", datos[1], "success");
        } else {
            swal("Acción Cancelada", datos[1], "warning");
        }
    });

});

$("#formenviarreporte").submit(function (e) {

    e.preventDefault();
    var observacion = $.trim($("#rObserEnvio").val())
    var correo = $.trim($("#rCorreo").val());
    a_correo = correo.split(";");
    var reporte = $("#rReporte").val() * 1;
    var plantilla = $("#Plantilla").val() * 1;

    for (var x = 0; x < a_correo.length; x++) {
        if (ValidarCorreo($.trim(a_correo[x])) == 2) {
            $("#rCorreo").focus();
            swal(ValidarTraduccion("Acción Cancelada"), ValidarTraduccion("Formato de correo inválido") + ":" + a_correo[x], "warning");
            return false;
        }
    }

    ActivarLoad();
    setTimeout(function () {
        var parametros = "ingreso=" + Ingreso + "&reporte=" + reporte + "&principal=" + CorreoEmpresa + " &e_email=" + correo + " &cliente=" + Empresa + " &observacion=" + observacion + "&plantilla=" + plantilla;
        var datos = LlamarAjax("Laboratorio/EnvioReporte", parametros);
        DesactivarLoad();
        datos = datos.split("|");
        if (datos[0] == "0") {
            swal("", ValidarTraduccion(datos[1]) + " " + datos[2], "success");
            $("#enviar_reporte").modal("hide");
        } else
            swal("Acción Cancelada", datos[1], "warning");
    }, 15);

    return false;
});

function MostrarFoto(reporte, numero) {
    var informe = $("#NroInforme" + reporte).val() * 1;
    $("#FotoMan" + reporte).attr("src", url + "imagenes/informemantenimineto/" + informe + "/" + numero + ".jpg");
}


function GuardarDocumento(reporte) {
    var documento = document.getElementById("Documento" + reporte);
    var informe = $("#NroInforme" + reporte).val() * 1;
    var numero = $("#NumFoto" + reporte).val() * 1;


    if ($.trim(documento.value) != "") {

        if (informe == 0) {
            swal("Acción Cancelada", "Debe generar primero el informe técnico", "warning");
            return false;
        }

        var file = documento.files[0];
        var data = new FormData();
        data.append('documento', file);
        var direccion = url + "Cotizacion/AdjuntarFoto";
        $.ajax({
            url: direccion,
            type: 'POST',
            contentType: false,
            data: data,
            processData: false,
            async: false,
            success: function (datos) {
                datos = datos.split("|");
                if (datos[0] == "0") {
                    datos = LlamarAjax("Laboratorio/SubirFoto", "ingreso=" + Ingreso + "&informe=" + informe + "&numero=" + numero + "&foto=" + datos[1]).split("|");
                    swal(datos[1]);
                    if (datos[0] == "0")
                        MostrarFoto(reporte, numero)
                } else {
                    swal(ValidarTraduccion(datos[1]), "", "error");
                }
            }
        });
    }
}

$('select').select2();

$("#Concepto").select2('destroy'); 