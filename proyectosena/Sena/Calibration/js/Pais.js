﻿$("#Pais").html(CargarCombo(18, 1));

function LimpiarPais() {
    $("#Pais").val("").trigger("change");
}

function CambioDepartamento(pais) {
    pais = pais * 1;
    if (isNaN(pais)) {
        $("#Departamento").html("");
    }
    else
        $("#Departamento").html(CargarCombo(11, 1, "", pais));
    $("#Ciudad").html("");
    TablaPais();
}

function CambioCiudad(departamento) {
    departamento = departamento * 1;
    if (isNaN(departamento))
        $("#Ciudad").html("");
    else
        $("#Ciudad").html(CargarCombo(12, 1, "", departamento));
    TablaPais();
}

function GuardarPais() {
    var idpais = $("#Pais").val() * 1;
    var idciudad = $("#Ciudad").val() * 1;
    var iddepartamento = $("#Departamento").val() * 1;

    if (isNaN(idpais))
        idpais = 0;
    if (isNaN(iddepartamento))
        iddepartamento = 0;
    if (isNaN(idciudad))
        idciudad = 0;

    var pais = $.trim($("#Pais option:selected").text());
    var departamento = $.trim($("#Departamento option:selected").text());
    var ciudad = $.trim($("#Ciudad option:selected").text());

    if (pais == "") {
        $("#Pais").focus();
        swal("Acción Cancelada", "Debe seleccionar o ingresar un país", "warning");
        return false;
    }

    if (ciudad != "" && departamento == "") {
        $("#Departamento").focus();
        swal("Acción Cancelada", "Debe seleccionar o ingresar un departamento", "warning");
        return false;
    }

    var parametros = "idpais=" + idpais + "&pais=" + pais + "&iddepartamento=" + iddepartamento + "&departamento=" + departamento +
        "&idciudad=" + idciudad + "&ciudad=" + ciudad;

    ActivarLoad();
    setTimeout(function () {
        var datos = LlamarAjax("Configuracion/GuardarPais", parametros).split("||");
        if (datos[0] == "0") {
            
            if (idpais == 0 && pais != "") {
                $("#Pais").html(datos[5]);
                $("#Pais").val(datos[2]).trigger("change");
                idpais = datos[2];
            }
            if (iddepartamento == 0 && departamento != "") {
                $("#Departamento").html(datos[6]);
                $("#Departamento").val(datos[3]).trigger("change");
                iddepartamento = datos[3];
            }
            if (idciudad == 0 && ciudad != "") {
                $("#Ciudad").html(datos[7]);
                $("#Ciudad").val(datos[4]).trigger("change");
                idciudad = datos[4];
            }
            TablaPais();
            swal("", datos[1], "success");
        } else
            swal("Acción Cancelada", datos[1], "warning");
        DesactivarLoad();
    }, 15);
}

function EliminarOpcion(opcion) {

    var pais = $("#Pais").val() * 1;
    var ciudad = $("#Ciudad").val() * 1;
    var departamento = $("#Departamento").val() * 1;
    var parametros = "opcion=" + opcion;

    if (isNaN(pais))
        pais = 0;
    if (isNaN(departamento))
        departamento = 0;
    if (isNaN(ciudad))
        ciudad = 0;

    var des_pais = $.trim($("#Pais option:selected").text());
    var des_departamento = $.trim($("#Departamento option:selected").text());
    var des_ciudad = $.trim($("#Ciudad option:selected").text());

    mensaje = "";

    switch (opcion) {

        case 1:
            if (pais == 0) {
                return false;
            } else {
                parametros += "&id=" + pais;
                mensaje = "el país " + des_pais;
            }
                
            break;
        case 2:
            if (departamento == 0) {
                return false;
            } else {
                parametros += "&id=" + departamento;
                mensaje = "el departamento " + des_departamento;
            }
            break;
        case 3:
            if (ciudad == 0) {
                return false;
            } else {
                parametros += "&id=" + ciudad;
                mensaje = "la ciudad " + des_ciudad;
            }
            break;
    }

    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: ValidarTraduccion('¿Seguro que desea eliminar ' + mensaje + '?'),
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Eliminar'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                $.post("Configuracion/EliminarPais", parametros)
                    .done(function (data) {
                        datos = data.split("||");
                        if (datos[0] == "0") {
                            TablaPais();
                            switch (opcion) {
                                case 1:
                                    $("#Pais").html(CargarCombo(18, 1));
                                    break;
                                case 2:
                                    CambioDepartamento(pais);
                                    break;
                                case 3:
                                    CambioCiudad(departamento);
                                    break;
                            }
                            resolve();
                        } else {
                            reject(datos[1]);
                        }
                    })
            })
        }
    }]);
}

function ActualizarOpcion(opcion, mensaje, id) {

    var pais = $("#Pais").val() * 1;
    var ciudad = $("#Ciudad").val() * 1;
    var departamento = $("#Departamento").val() * 1;

    if (isNaN(pais))
        pais = 0;
    if (isNaN(departamento))
        departamento = 0;
    if (isNaN(ciudad))
        ciudad = 0;

    var des_pais = $.trim($("#Pais option:selected").text());
    var des_departamento = $.trim($("#Departamento option:selected").text());
    var des_ciudad = $.trim($("#Ciudad option:selected").text());

    var valor = "";
    var idvalor = 0;

    switch (opcion) {

        case 1:
            if (pais == 0) {
                return false;
            } else {
                valor = des_pais;
                idvalor = pais;
            }
                
            break;
        case 2:
            if (departamento == 0) {
                return false;
            } else {
                valor = des_departamento;
                idvalor = departamento;
            }
                
            break;
        case 3:
            if (ciudad == 0) {
                return false;
            } else {
                idvalor = ciudad;
                valor = des_ciudad;
            }
                
            break;
    }

    swal({
        title: 'Actualizar ' + mensaje,
        input: 'text',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Actualizar'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',

        inputValidator: function (value) {
            return new Promise(function (resolve, reject) {
                if ($.trim(value)) {
                    var datos = LlamarAjax("Configuracion/ActualizarOpcion", "opcion=" + opcion + "&id=" + idvalor + "&descripcion=" + value + "&iddepartamento=" + departamento + "&idpais=" + pais);
                    datos = datos.split("||");
                    if (datos[0] == "0") {
                        $("#" + id).html(datos[2]);
                        $("#" + id).val(idvalor).trigger("change");
                        TablaPais();
                        resolve();
                    } else {
                        reject(datos[1]);
                    }

                } else {
                    reject(ValidarTraduccion('Debe de ingresar la descripción de ' + mensaje));
                }
            })
        }
    }).then(function (result) {
        $("#" + id).focus();
    });

    $(".swal2-input").val(valor);

}

function TablaPais() {
    ActivarLoad();
    setTimeout(function () {
        var pais = $("#Pais").val() * 1;
        var departamento = $("#Departamento").val() * 1;

        if (isNaN(pais))
            pais = 0;
        if (isNaN(departamento))
            departamento = 0;
        
        var parametros = "pais=" + pais + "&departamento=" + departamento;
        var datos = LlamarAjax('Configuracion/TablaPais', parametros);
        var dataconjson = JSON.parse(datos);
        $('#TablaPais').DataTable({
            data: dataconjson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "idpais" },
                { "data": "pais" },
                { "data": "iddepartamento" },
                { "data": "departamento" },
                { "data": "idciudad" },
                { "data": "ciudad" },
            ],
            "language": {
                "url": LenguajeDataTable
            }
        });
    }, 15);
    DesactivarLoad();
}

TablaPais();

$("#TablaPais > tbody").on("dblclick", "tr", function () {
    var row = $(this).parents("td").context.cells;
    var pais = row[0].innerText;
    var departamento = row[2].innerText;
    var ciudad = row[4].innerText;
    $("#Pais").val(pais).trigger("change");
    $("#Departamento").val(departamento).trigger("change");
    $("#Ciudad").val(ciudad).trigger("change");
});


$('select').select2();
DesactivarLoad();

$("#Pais").select2({ tags: true });
$("#Ciudad").select2({ tags: true });
$("#Departamento").select2({ tags: true });