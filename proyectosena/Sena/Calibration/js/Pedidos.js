//CargarFilas();


d = new Date();
month = d.getMonth() + 1;
day = d.getDate();

var Anio = $.trim(d.getFullYear());
Anio = $.trim(Anio.substr(2, 2))

    
output = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-' +
    (day < 10 ? '0' : '') + day;

var urlfoto = localStorage.getItem("urlfoto");
var urlfirma = localStorage.getItem("urlfirma");

$("#Marca").html(CargarCombo(2, 1));
$("#MarcaF").html($("#Marca").html());
$("#PedDespacho").html(CargarCombo(17, 1));

var canvas = document.getElementById('pad1');
initPad(canvas);

var decimales = 0;
var DesMaxUsu = 0
var VendedorCalle = localStorage.getItem("VendedorCalle") * 1;
var ValorFlete = localStorage.getItem("ValorFlete") * 1 
var MinimoFlete = localStorage.getItem("MinimoFlete") * 1 
var CodigoSelFoto = "";
var a_Codigo;
var Cli_Correo;
var a_Producto;
var a_Iva;
var a_Descuento;
var a_DescuentoAnt;
var a_VUnitario;
var a_Cant;
var a_CantAnt;
var a_SubTotal;
var a_CantPedido;
var a_Exist;
var a_DescMax;
var a_VDescuento;
var a_Id;
var a_NoManifiesto;
var a_FechaManifiesto;
var a_Descripcion;
var a_CFab;
var a_Foto;
var a_Gravable;
var a_Excento;
var a_Peso;
var a_Pedido;
var a_Verificado;
var a_ExisInici;
var ValorInicial = 0;
var Estado = "";
var NFactura = 0;
var TotalVarios = 0;
var PedDespacho = 0;
var PedPlazo = 0;
var PedVenOfi = 0;
var PedVenCalle = 0;
var PedTipCliente = 0;
var SiglaMoneda = "";
var TablaPrecioMont = 0;
var Flete = 0;

var clisaldo = 0;
var limite = 0;

var FormEsf = 0;
var FormCil = 0;
var FormAdi = 0;

var TipoEsf = 0;
var TipoCil = 0;
var TipoAdi = 0;


document.onkeydown = function (e) {
    tecla = (document.all) ? e.keyCode : e.which;
    
    switch (tecla) {
        case 112: //F1
            ModalClientes();
            return false;
            break;
        case 113: //F2
            FilaMalla = TotalFila - 1;
            $("#Codigo" + FilaMalla).focus();
            FBuscarMontura();
            return false;
            break;
        case 114: //F3
            FilaMalla = TotalFila - 1;
            $("#Codigo" + FilaMalla).focus();
            if (($.trim($("#NombreCliente").val()) != "") && (NFactura  == "") && ($("#Codigo" + FilaMalla).prop("disabled") == false))
                ModalMonturasFotos();
            return false;
            break;
        case 115: //F4
            MaximizarFoto();
            break;
        case 117: //F6
            EliminarFila();
            return false;
            break;
        
    }
}

function GuardarFirma() {
    var firma = canvas.toDataURL("image/png");
    console.log(firma);

    $.ajax({

        type: 'POST',
        url: url + "Facturacion/GuardarFirma",
        data: '{ "firma" : "' + firma + '","pedido":"' + NPedido + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (msg) {
            firma = "si";
        }
    });
}

function VerEstadoCuenta() {
    if (TipCliente == 2 || TipCliente == 5 || TipCliente == 4) {
        if (document.getElementById('estadocuenta').style.display == "none") {
            document.getElementById('estadocuenta').style.display = 'block';
            $("#estadocuenta2").html($("#estadocuenta").html())
        } else {
            document.getElementById('estadocuenta').style.display = 'none';
            $("#estadocuenta2").html("")
        }
            
    }
}


var cod_moneda = 0;
var NPedido = 0;
var BPedido = 0;
var Pdf_Pedido = "";
var Pdf_Cartera = "";
var Vendedor = "";
var TipCliente = "";
var CaracCliente = 0;
var EstadoCli = "";
var CliDesMon = 0;
var CliDesVar = 0;
var CodCliente = 0
var factor_moneda = 1;
var TabMonPrecio = 0;
var TabVarPrecio = 0;
var OPcionesFacturar = "";


function AddNuevaFila(x) {

    
    var NuevaFila = '<tr id="trfila' + x + '">' +
        '<td><input class="no-borderl text-right" name="Item[]" id="Item' + x + '" value="' + (x + 1) + '" disabled="disabled" readonly="readonly" /></td>' +
        '<td><input type="text" class="no-borderl" name="Codigo[]" id="Codigo' + x + '"  onkeyup="BuscarMontura(this.value, ' + x + ', 0, event)" onblur="BuscarMontura(this.value, ' + x + ', 1, event)" onfocus="ActivarFilaMalla(' + x + ',this)" /></td>' +
        '<td onclick="MaximizarFoto(' + x + ')"><textarea class="no-borderl" name="Descripcion[]" id="Descripcion' + x + '" disabled="disabled" readonly="readonly" style="padding:0px 0px 0px 0px"></textarea></td>' +
        '<td><input type="text" class="no-border" name="VUnitario[]" id="VUnitario' + x + '" disabled="disabled" readonly="readonly" /></td>' +
        '<td><input type="text" class="no-border" name="Cant[]" id="Cant' + x + '" onkeyup="AgregarFila(' + x + ',0,event,this)" onfocus="ActivarFilaMalla(' + x + ',this)" onblur="GuardarTemporal(' + x + ')"  /></td>' +
        '<td><input type="text" class="no-borderama" name="Descuento[]" disabled id="Descuento' + x + '" onkeyup="AgregarFila(' + x + ',1,event,this)" onfocus="ActivarFilaMalla(' + x + ',this)" onblur="GuardarTemporal(' + x + ')"  /></td>' +
        '<td><input type="text" class="no-border" name="SubTotal[]" id="SubTotal' + x + '" disabled="disabled" readonly="readonly" /></td>' +
        '<td><input type="text" class="no-border" name="Iva[]" id="Iva' + x + '" disabled="disabled" readonly="readonly" /></td>' +
        '<td><input type="text" class="no-border" name="Exist[]" id="Exist' + x + '" disabled="disabled" readonly="readonly" /></td>' +
        '<input type="hidden" value="0" name="CostoPromedio[]" id="CostoPromedio' + x + '" />' +
        '<input type="hidden" value="0" name="Gravable[]" id="Gravable' + x + '" />' +
        '<input type="hidden" value="0" name="Excento[]" id="Excento' + x + '" />' +
        '<input type="hidden" value="0" name="Pedido[]" id="Pedido' + x + '" />' +
        '<input type="hidden" value="0" name="DescMax[]" id="DescMax' + x + '" />' +
        '<input type="hidden" value="0" name="CantAnt[]" id="CantAnt' + x + '" />' +
        '<input type="hidden" value="0" name="DescuentoAnt[]" id="DescuentoAnt' + x + '" />' +
        '<input type="hidden" value="0" name="Id[]" id="Id' + x + '" /></td > ' +
        '<input type="hidden" value="0" name="CFab[]" id="CFab' + x + '" /></td > ' +
        '<input type="hidden" value="0" name="Foto[]" id="Foto' + x + '" /></td > ' +
        '<input type="hidden" value="0" name="Peso[]" id="Peso' + x + '" /></td > ' +
        '<input type="hidden" value="0" name="VDescuento[]" id="VDescuento' + x + '" /></td > ' +
        '<input type="hidden" value="0" name="ExisInici[]" id="ExisInici' + x + '" /></td > ' +
        '</tr>';
        
    $("#Malla").append(NuevaFila);

    a_Codigo = document.getElementsByName("Codigo[]");
    a_Producto = document.getElementsByName("Producto[]");
    a_Iva = document.getElementsByName("Iva[]");
    a_Descuento = document.getElementsByName("Descuento[]");
    a_DescuentoAnt = document.getElementsByName("DescuentoAnt[]");
    a_VUnitario = document.getElementsByName("VUnitario[]");
    a_Cant = document.getElementsByName("Cant[]");
    a_CantAnt = document.getElementsByName("CantAnt[]");
    a_SubTotal = document.getElementsByName("SubTotal[]");
    a_Exist = document.getElementsByName("Exist[]");
    a_DescMax = document.getElementsByName("DescMax[]");
    a_VDescuento = document.getElementsByName("VDescuento[]");
    a_Id = document.getElementsByName("Id[]");
    a_Descripcion = document.getElementsByName("Descripcion[]");
    a_CFab = document.getElementsByName("CFab[]");
    a_Foto = document.getElementsByName("Foto[]");
    a_Gravable = document.getElementsByName("Gravable[]");
    a_Excento = document.getElementsByName("Excento[]");
    a_Peso = document.getElementsByName("Peso[]");
    
    a_ExisInici = document.getElementsByName("ExisInici[]");
    

    TotalFila++;

    if ($("#Malla tbody tr").length  > 6)
        $("#BotonosAux").removeClass("hidden");

}


function TraerLO() {

    var esfera = $("#LEsfera").val() * 1;
    var cilindro = $("#LCilindro").val() * 1;
    var adicion = $("#LAdicion").val() * 1;
    var terminado = 0;
    var tablaprecio = $("#TablaPreciosMont").val() * 1;

    tablaprecio = 1;

    if ($("#LTerminado").prop("checked"))
        terminado = 1;

    var parametros = "Terminado=" + terminado + "&Esfera=" + esfera + "&Clindro=" + cilindro + "&Adicion=" + adicion + "&TablaPrecio=" + tablaprecio + "&tipo=" + tipo;
    var datos = LlamarAjax(url + "Facturacion/SeleccionarLente", parametros)

    datos = datos.split("|");

    if (datos[0] == "0") {
        $("#LIzquierdo").val(datos[1]);
        $("#LDerecho").val(datos[2]);
        $("#LPrecio").val(formato_numero(datos[3], decimales, ".", ",", ""));
       
    } else {
        swal("", datos[1], "warning");
        $.jGrowl(ValidarTraduccion(datos[1]), { life: 2500, theme: 'growl-warning', header: '' });

    }
}

function TipoOjos() {
    var lctipo = $("#LTipo").val() * 1;
    SeleccionarLente(lctipo);
}

function FormulaLC(objeto, valor) {
    valor = $.trim(valor);
        
        
    if (objeto == "LCilindro") {
        if (valor.substr(0, 1) == "-")
            TipoCil = valor.substr(1, valor.length) * 1;
        else
            TipoCil = valor * 1;
        $("#" + objeto).val((TipoCil == 0 ? "" : "-") + formato_numero(TipoCil, 2, ".", ",", ""));
    }
    if (objeto == "LEsfera") {
        TipoEsf = valor * 1;
        $("#" + objeto).val(formato_numero(TipoEsf, 2, ".", ",", ""));
        
    }
    if (objeto == "LAdicion") {
        TipoAdi = valor * 1;
        $("#" + objeto).val(formato_numero(TipoAdi, 2, ".", ",", ""));
    }
    if (FormEsf != TipoEsf || FormCil != TipoCil || FormAdi != TipoAdi) {
        $("#LIzquierdo").val("");
        $("#LDerecho").val("");
        $("#LPrecio").val("");
    }

    if (TipoAdi > 0) {
        $("#LTipoOjo").removeClass("hidden");
    } else {
        $("#LTipoOjo").addClass("hidden");
    }

    var lctipo = $("#LTipo").val() * 1;
    if (lctipo > 0) {

        if (FormEsf != TipoEsf || FormCil != TipoCil || FormAdi != TipoAdi) {
            FormEsf = TipoEsf;
            FormCil = TipoCil;
            FormAdi = TipoAdi;
            SeleccionarLente(lctipo);
        }
            
    } else {
        LlenarLentes();
    }
}



    
tb = $('input');
$(tb).keypress(enter2tab);

tb = $('select');
$(tb).keypress(enter2tab);

function CargarModalPagos() {
    if (NFactura != "") {
        $("#modalPagos").modal("show");
    }
}

function ModalMonturas() {
    $("#modalmontura").modal("show");
    CargarModalMonturas();
    $("#Marca").focus();
}


function LimpiarFila(fila) {

    $("#Codigo" + fila).val("");
    $("#Descripcion" + fila).val("");
    $("#VUnitario" + fila).val("");
    $("#Cant" + fila).val("");
    $("#Descuento" + fila).val("");
    $("#Iva" + fila).val("");
    $("#Exist" + fila).val("");
    $("#CantPedido" + fila).val("");
    $("#FechaPedido" + fila).val("");
    $("#PedOriginal" + fila).val("");
    $("#CostoPromedio" + fila).val("");
    $("#DescMax" + fila).val("");
    $("#Descuento" + fila).val("");
    $("#SubTotal" + fila).val("");
            
    $("#NoManifiesto" + fila).val("");
    $("#FechaManifiesto" + fila).val("");
    $("#Foto" + fila).val("");
    $("#CFab" + fila).val("");
    $("#Peso" + fila).val("");
    $("#Pedido" + fila).val("");
    $("#Id" + fila).val("");

}

function EliminarFilaRegistro(id,fila) {
    if (id > 0) {
        var datos = LlamarAjax(url + "Facturacion/EliminarTemporalFila", "id=" + id + "&pedido=" + NPedido + "&tipo=MONT");
        if (datos == "1") {
            $.jGrowl(ValidarTraduccion("Registro eliminado"), { life: 2500, theme: 'growl-error', header: '' });
        } else {
            swal("", "No se puede mondificar un pedido con estado Cerrado/Impreso/Facturado/Anulado", "warning");
        }
        return datos;
    }
}

function EliminarFila() {
    
    var id = $("#Id" + FilaMalla).val() * 1;
    
    if ((FilaMalla + 1) == TotalFila) {
        if ($("#Descripcion" + FilaMalla).val() == "")
            return false;
        else {
            if (EliminarFilaRegistro(id) == "1") {
                LimpiarFila(FilaMalla);
                $("#Codigo" + FilaMalla).prop("disabled", false);
                $("#Codigo" + FilaMalla).focus();
            }
        }
    } else {
        if (EliminarFilaRegistro(id) == "1") {
            $("#trfila" + FilaMalla).remove();
            FilaMalla = FilaMalla - 1
            //TotalFila = TotalFila - 1;
            $("#Codigo" + FilaMalla).focus();
        }
    }

    TotalMalla()

}


function enter2tab(e) {
    if (e.keyCode == 13) {
        id = $(this).attr("id");
        if ((id == "Pedido") || (id == "Cliente"))
            return false;

        cb = parseInt($(this).attr('tabindex'));

        if ($(':input[tabindex=\'' + (cb + 1) + '\']') != null) {
            $(':input[tabindex=\'' + (cb + 1) + '\']').focus();
            $(':input[tabindex=\'' + (cb + 1) + '\']').select();
            e.preventDefault();

            return false;
        }
    }
}

var ErrorMalla = 0;
var FilaMalla = 0;
var TotalFila = 0;
var modal = 0;


function AgregarFila(fila, tipo, code, caja) {

    if (tipo == 0)
        ValidarTexto(caja, 1);
    else
        ValidarTexto(caja, 1);
        
    if ($.trim($("#Descripcion" + fila).val()) == "") {
        $("#Cant" + fila).val("");
        $("#Descuento" + fila).val("");
        return false;
    }
        

    $("#btnCerrar").addClass("hidden");

    if (Estado == "Pendiente" || Estado == "Temporal" || Estado == "") 
        $("#btnguardar").removeClass("hidden");
           
    
    if (((fila + 1) == TotalFila) && ($("#Descripcion" + fila).val() != "")) {
        if (Estado == "Pendiente" || Estado == "Temporal" || Estado == "") 
            AddNuevaFila(TotalFila);
    }

    FilaMalla = fila;
    TotalMalla()

    if (code.keyCode == 13) {
        modal = 1;
        $("#Codigo" + (TotalFila-1)).select();
        $("#Codigo" + (TotalFila-1)).focus();
    }
} 

$("#Cliente").bind('keypress', function (e) {
    if (e.keyCode == 13) {
        $("#Cliente").blur();
    }
})

function ActivarFilaMalla(fila, id) {

    if (((fila + 1) == TotalFila) && ($("#Descripcion" + fila).val() != "")){ 
        if (Estado == "Pendiente" || Estado == "Temporal" || Estado == "") 
            AddNuevaFila(TotalFila);
    }

    FilaMalla = fila;
    id.select();

}



function LimpiarTodo() {

    var Cliente = CodCliente;
    var nped = NPedido;
    
    if ((NPedido == 0) && (TotalFila > 1)) {

        swal.queue([{
            title: 'Advertencia',
            text: '¿' + ValidarTraduccion('Desea eliminar los item de Artículos') + '?',
            type: 'question',
            showLoaderOnConfirm: true,
            showCancelButton: true,
            confirmButtonText: ValidarTraduccion('Si'),
            cancelButtonText: ValidarTraduccion('No'),
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            preConfirm: function () {
                return new Promise(function (resolve) {
                    $.post(url + "Facturacion/EliminarTemporal", "cliente=" + Cliente)
                        .done(function (data) {
                            swal.insertQueueStep(ValidarTraduccion("Artículos Eliminadas"));
                            resolve()
                        })
                })
            }
        }])
        Limpiar()
        
    } else {
        
        Limpiar()
        
    }

    if (nped > 0) {
        TablaTemporalFacturacion(Cliente, 0);
        AddNuevaFila(TotalFila);
        nped = 0;
    }

}

function Limpiar() {

    
    FormEsf = 0;
    FormCil = 0;
    FormAdi = 0;
              
       
    decimales = 0;
    
    NPedido = 0;
    NFactura = "";
    PedDespacho = 0;
    PedPlazo = 0;
    PedVenOfi = 0;
    PedVenCalle = 0;
    PedGuia = 0;
    PedTipCliente = 0;
        
    $("#Pedido").val("");
    $("#Factura").val("");
    $("#NFactura").val("");
    $("#Estado").val("");
    ValorInicial = 0;
    Estado = "";
    $("#tablamodaltoma tbody tr").remove(); 
    $("#Observaciones").val("");
    $("#divAnulaciones").addClass("hidden");
    $('#firmapedido').removeAttr('src');

    $("#btnCerrar").addClass("hidden");
    $("#btnguardar").addClass("hidden");
    localStorage.setItem('FirmaCliente', '0');
        
    BPedido = 0;
        
    Pdf_Cartera = "";
    Pdf_Pedido = "";

    LimpiarMalla();

    AddNuevaFila(0);

    TotalMalla();

    clearCanvas(canvas, ctx)
}

$("#Pedido").keyup(function (e) {
    if (e.keyCode == 13) {
        NPedido = 0;
        $("#Flete").prop("disabled", false);
        $("#Flete").select();
        $("#Flete").focus();
    }
})


function BuscarPedido(pedido) {
    pedido = pedido * 1;
    $("#Pedido").val(pedido);
    $("#modalConsultarPedidos").modal("hide");
    $("#modalConsultarFacturas").modal("hide");
    $("#modalConsultarRemisiones").modal("hide");
       
    
    var datos = LlamarAjax(url + 'Facturacion/BuscarPedido', "pedido=" + pedido);
    if (datos != "[]") {
        eval("datacli=" + datos);
        NPedido = datacli[0].Numero;
        BPedido = NPedido;
        PedTipCliente = datacli[0].tipcli*1;
        PedDespacho = datacli[0].ForEnvio;
        $('#PedDespacho').val(PedDespacho).trigger('change');
        Flete = datacli[0].Flete * 1;
        $("#btnCerrar").addClass("hidden")
        $("#Cliente").val(datacli[0].codcli);
        $("#Flete").val(formato_numero(datacli[0].Flete, decimales, ".", ",", SiglaMoneda));
        NFactura = datacli[0].NumFactura == "0" ? "" : datacli[0].NumFactura
        $("#NFactura").val(NFactura);
        $("#Factura").val(NFactura);
        $("#Estado").val(datacli[0].Estado);
        Estado = datacli[0].Estado;
        $("#DireccionEnv").val(datacli[0].idEnvio)

        $("#FechaEnvio").val(datacli[0].FechaEnvio);
        $("#Guia").val(datacli[0].Guia);
        
        $("#Orden").val(datacli[0].Orden);
        $("#Caja").val(datacli[0].Caja);
        $("#Observaciones").val(datacli[0].Observacion);
        $("#Obsfactura").val(datacli[0].ObsFactura);
        
        if ($.trim(datacli[0].obs_anulacion) != "") {
            $("#divAnulaciones").removeClass("hidden");
            $("#ObsAnulacion").html("Usuario que Anuló: " + datacli[0].usu_anula + ", Fecha: " + datacli[0].fecha_anula + ", Observación: " + datacli[0].obs_anulacion);
        }

        $("#Cliente").blur();
        ValorInicial = datacli[0].Valor * 1;

        TablaTemporalFacturacion(CodCliente, pedido);

        if (Estado == "Pendiente" || Estado == "Temporal")
            AddNuevaFila(TotalFila);
        
        if (Estado == "Pendiente") 
            $("#btnCerrar").removeClass("hidden")
        else
            $("#btnCerrar").addClass("hidden")

        if (Estado == "Pendiente" || Estado == "Temporal" || Estado == "") {
            $("#btnguardar").removeClass("hidden");
        } else {
            $("#btnguardar").addClass("hidden");
        }

        var img = new Image();
        img.src = urlfirma + NPedido + ".jpg";
        var ctx = canvas.getContext("2d");

        clearCanvas(canvas, ctx);

        ctx.drawImage(img, 0, 0);

        img.onload = function () {
            ctx.drawImage(img, 0, 0);
        }

        localStorage.setItem('FirmaCliente', '1');
                               
                
    } else {
        $.jGrowl(ValidarTraduccion("Pedido") + " " + pedido + " " + ValidarTraduccion("no registrado"), { life: 2500, theme: 'growl-warning', header: '' });
        Limpiar();
    }

}

function ValidarCantidadMalla() {
    for (var x = 0; x < TotalFila; x++) {
        if (($("#Descripcion" + x).val() != "") && ($("#Cant" + x).val() * 1 == 0)) {
            $("#Cant" + x).focus();
            swal("", "Escriba la unidades de la montura " + $("#Descripcion" + x).val(), "error");
            return x;
        }
    }
    return 0;
}


function GuardarPedido() {

    var tablaprecio = 1;
    
    if ($.trim($("#NombreCliente").val()) == "")
        return false;
    if (NumeroDecimal($("#ttotalpagar").val()) == 0) {
        swal("Debe seleccionar por lo mínimo una montura")
        return false;
    }

    if ($("#PedDespacho").val()*1 == 0) {
        swal(ValidarTraduccion("Acción Cancelada"), ValidarTraduccion("Debe seleccionar la vía de despacho"), "warning")
        return false;
    }
        
    if (localStorage.getItem("FirmaCliente") == "0") {
        swal(ValidarTraduccion("Acción Cancelada"), ValidarTraduccion("Debe firmar el pedido"), "warning");
        canvas.focus();
        return false;
    }
            

    var pos = ValidarCantidadMalla();
    if (pos != 0) {
        
        return false;
    }

    
       
    parametros = "Cliente=" + CodCliente + "&pedido=" + (NPedido * 1) + "&TotalPagar=" + NumeroDecimal($("#ttotalpagar").val());
    parametros += "&Flete=" + Flete + "&Base=" + NumeroDecimal($("#tbaseimponible").val());
    parametros += "&Iva=" + NumeroDecimal($("#tiva").val()) + "&Descuento=" + NumeroDecimal($("#tdescuento").val());
    parametros += "&FactorCambio=" + factor_moneda; 
    parametros += "&Observaciones=" + $.trim($("#Observaciones").val()) + "&ForEnvio=" + $("#PedDespacho").val();
    parametros += "&Excento=" + NumeroDecimal($("#texcento").val()) + "&TablaPrecio=" + TablaPrecioMont;
    parametros += "&IdEnvio=" + ($("#DireccionEnv").val() * 1);

    
    
    document.getElementById('preloader').style.display = 'block';
    setTimeout(function () {
        var datos = LlamarAjax(url + "Facturacion/GuardarPedido", parametros);
        datos = datos.split("|");
        if (datos[0] == "0"){
            GuardarFirma();
            $("#Pedido").val(datos[2])
            NPedido = datos[2] * 1;
            swal("", ValidarTraduccion(datos[1]) + ' ' + ValidarTraduccion(datos[2]), "success")
            LimpiarMalla();
            TablaTemporalFacturacion(CodCliente, datos[2]);
            AddNuevaFila(TotalFila);
            $("#btnfacturar2").removeClass("hidden");
            $("#btnfacturar3").removeClass("hidden");
            
            $("#btnCerrar").removeClass("hidden");
            $("#btnanularped2").removeClass("hidden");
            $("#btnanularped3").removeClass("hidden");
            if ((Estado == "") || (Estado == "Temporal")) {
                $("#Estado").val(ValidarTraduccion("Pendiente"));
                Estado = "Pendiente";
            }
        }else
            swal("", ValidarTraduccion(datos[1]) + ' ' + ValidarTraduccion(datos[2]), "warning")
        
        document.getElementById('preloader').style.display = 'none';
    }, 15);
    
}

function TablaTemporalFacturacion(cliente, pedido) {
    LimpiarMalla();
    var parametros = "cliente=" + cliente + "&pedido=" + pedido;
    var datos = LlamarAjax(url + 'Facturacion/TablaTemporalFacturacion', parametros);
    if (datos != "[]") {
        eval("data=" + datos);
        for (var x = 0; x < data.length; x++){
            AddNuevaFila(data[x].fila);
            $("#Producto" + data[x].fila).val(data[x].Producto).trigger('change');
            $("#Codigo" + data[x].fila).val(data[x].Codigo);
            $("#Descripcion" + data[x].fila).val(data[x].Descripcion);
            $("#VUnitario" + data[x].fila).val(formato_numero(data[x].VUnitario, decimales, ".", ",", ""));
            $("#Cant" + data[x].fila).val(data[x].Cant);
            $("#Descuento" + data[x].fila).val(data[x].Descuento);
            $("#Iva" + data[x].fila).val(data[x].Iva);
            $("#Exist" + data[x].fila).val(data[x].Existencia);
            $("#CostoPromedio" + data[x].fila).val(data[x].CostoPromedio);
            $("#DescMax" + data[x].fila).val(CliDesMon);
            $("#Id" + data[x].fila).val(data[x].id);
            $("#CantAnt" + data[x].fila).val(data[x].Cant);
            $("#DescuentoAnt" + data[x].fila).val(data[x].Descuento);
            $("#Foto" + data[x].fila).val(data[x].foto);
            $("#CFab" + data[x].fila).val(data[x].cfab);
            $("#Pedido" + data[x].fila).val(data[x].Pedido);
            
            
            TotalFila = data[x].fila;

            if (Estado == "Pendiente" || Estado == "Temporal" || Estado == "") {
                $("#Cant" + TotalFila).prop("readonly", false);
                $("#Descuento" + TotalFila).prop("readonly", false);
            } else {
                $("#Cant" + TotalFila).prop("readonly", true);
                $("#Descuento" + TotalFila).prop("readonly", true);
            }
            
            $("#Codigo" + data[x].fila).prop("disabled", true);
            $("#Producto" + data[x].fila).prop("disabled", true);
        }

        TotalFila++;

        TotalMalla();

    }

    if ((pedido == 0) && (TotalFila > 0)) {
        $("#btnguardar").removeClass("hidden");
    }
}


function BuscarCliente(codigo) {

    if (codigo * 1 == CodCliente) {
        return false;
    }

    CodCliente = codigo * 1;
    Limpiar();
    if (CodCliente == 0) {
        return false;
    }

    document.getElementById('preloader').style.display = 'block';

    setTimeout(function () {

        var parametros = "codigo=" + codigo+"&web=1";
        var datos = LlamarAjax(url + 'Facturacion/BuscarCliente', parametros);
        datos = datos.split("|");

        if (datos[0] == "1") {
            $("#Cliente").select();
            $("#Cliente").focus();
            document.getElementById('preloader').style.display = 'none';
            swal("", ValidarTraduccion(datos[1]), "warning");
            CodCliente = 0;
            
            return false;
        }

        if (datos[1] != "[]") {

            eval("data=" + datos[1]);
            
            TipCliente = (PedTipCliente == 0 ? data[0].TipCli * 1 : PedTipCliente);
            CaracCliente = data[0].CliCodCar * 1;
            decimales = data[0].decimales;
            EstadoCli = data[0].CliEstado*1;
            CliDesMon = data[0].DescMontura * 1;
            Cli_Correo = data[0].CliCompradorMail;
            $("#CorreoElec").val(Cli_Correo);
            $("#NombreCliente").val(data[0].NomRazSoc);
            $("#Direccion").val(data[0].CliDesDir ? data[0].CliDesDir : "");
            $("#Ciudad").val(data[0].NomCiu ? data[0].NomCiu : "");
            $("#Telefono").val(data[0].CliDesTel + "  " + (data[0].CliDEsFax ? data[0].CliDEsFax : ""));
            TablaPrecioMont = data[0].CliPreMont;

            PedDespacho = data[0].CliCodViaDes * 1;
            PedPlazo = data[0].CliPlaPago * 1;

            $('#PedDespacho').val((data[0].CliCodViaDes != "0" ? data[0].CliCodViaDes : "")).trigger('change');
            $('#PedPlazo').val(data[0].Plazos + " dias");

            $("#clisaldo").html(data[0].Saldo - ValorInicial == 0 ? "0" : formato_numero(data[0].Saldo - ValorInicial, decimales, ".", ",", SiglaMoneda));
            limite = data[0].Limite;
            clisaldo = data[0].Saldo - ValorInicial;
            $("#clilimite").html(data[0].Limite == 0 ? "0" : formato_numero(data[0].Limite, decimales, ".", ",", SiglaMoneda));
            $("#clidisponible").html(data[0].Limite - data[0].Saldo == 0 ? "0" : formato_numero(data[0].Limite - data[0].Saldo, decimales, ".", ",", SiglaMoneda));
                        
            
            $("#ECDiaBlo").html(data[0].CliDiaBlo);
            $("#ECLimCre").html($("#clilimite").html());
            $("#ECDiaPre").html(data[0].CliDiaPreBlo);
            $("#ECSalDis").html($("#clidisponible").html());
            $("#ECEstado").html(ValidarTraduccion(data[0].CliDesEst));

            $("#ECCliente").html(CodCliente + "<br>" + data[0].NomRazSoc);

            var resultado = ""
            if (datos[2] != "[]") {
                eval("dataf=" + datos[2])
                for (var x = 0; x < dataf.length; x++) {
                    resultado += "<tr>" +
                        "<td>" + (x + 1) + "</td>" +
                        "<td>" + dataf[x].Factura + "</td>" +
                        "<td>" + dataf[x].Orden + "</td>" +
                        "<td>" + dataf[x].Pedido + "</td>" +
                        "<td>" + dataf[x].FechaPedido + "</td>" +
                        "<td>" + dataf[x].FechaFactura + "</td>" +
                        "<td>" + dataf[x].FechaVencimiento + "</td>" +
                        "<td>" + formato_numero(dataf[x].Valor, decimales, ".", ",", SiglaMoneda) + "</td>" +
                        "<td>" + dataf[x].Tipo + "</td></tr>";
                }
            }
            $("#ECTotal").html(ValidarTraduccion("Total en Saldo") + ": " + formato_numero(data[0].Saldo, decimales, ".", ",", SiglaMoneda));
            $("#bodyEstadoCuenta").html(resultado);

            if (TipCliente == 2 || TipCliente == 5) {
                if (EstadoCli == 0) {
                    CodCliente = 0;
                    Limpiar();
                    $("#modalEstadoCliente").modal("show");
                } else {
                    if (EstadoCli == 1) {
                        $("#modalEstadoCliente").modal("show");
                    }
                }
            }      

                                    
            TablaTemporalFacturacion(codigo, NPedido);
            
            if (Estado == "Pendiente" || Estado == "Temporal" || Estado == "") {
                AddNuevaFila(TotalFila)
                modal = 1;
                $("#Codigo" + (TotalFila-1)).focus();
            }

        } else {
            document.getElementById('preloader').style.display = 'none';
            $.jGrowl(codigo + " " + ValidarTraduccion("Código no registrado"), { life: 2500, theme: 'growl-warning', header: '' });
            $("#Cliente").select();
            $("#Cliente").focus();
            CodCliente = 0;

        }
        document.getElementById('preloader').style.display = 'none';
    }, 15);
}

function GuardarTemporal(fila) {

    var cant = $("#Cant" + fila).val() * 1;
    var cant_a = $("#CantAnt" + fila).val() * 1;

    if ($("#Codigo" + fila).val() * 1 == 0)
        return;

    if (isNaN(cant)) {
        $("#Cant" + fila).val(cant_a);
        
        return false;
    }
        
    
    var pedido = NPedido;

    var desc = $("#Descuento" + fila).val() * 1;
    var desc_a = $("#DescuentoAnt" + fila).val() * 1;

    if (desc == 0)
        $("#Descuento" + fila).val("0");
    
    if ((cant != cant_a) || (desc != desc_a)) {
                        
        parametros = "id=" + ($("#Id" + fila).val() * 1) + "&Cliente=" + CodCliente + "&fila=" + fila;
        parametros += "&Producto=MONT&Codigo=" + $("#Codigo" + fila).val() + "&Descripcion=" + $("#Descripcion" + fila).val();
        parametros += "&VUnitario=" + NumeroDecimal($("#VUnitario" + fila).val()) + "&Descuento=" + ($("#Descuento" + fila).val() * 1) + "&Iva=" + ($("#Iva" + fila).val() * 1) + "&SubTotal=" + NumeroDecimal($("#SubTotal" + fila).val()) + "&Cant=" + cant;
        parametros += "&Existencia=" + ($("#Exist" + fila).val() * 1) + "&CanPedido=0&FechaPedido=&PedidoOriginal=0&CostoPromedio=" + NumeroDecimal($("#CostoPromedio" + fila).val()) + "&cant_ant=" + cant_a;
        parametros += "&VDescuento=" + NumeroDecimal($("#VDescuento" + fila).val()) + "&foto=" + $("#Foto" + fila).val() + "&cfab=" + ($("#CFab" + fila).val() * 1) + "&pedido=" + pedido + "&NoManifiesto=" + $("#NoManifiesto" + fila).val(); 
                
        var datos = LlamarAjax(url + "Facturacion/TemporalFacturacion", parametros);
        datos = datos.split("|");

        if ($.trim(datos[0]) == "-1") {
            swal("", ValidarTraduccion("No se puede modificar este pedido, porque ya fue Cerrado/Impreso/Facturado/Anulado"), "error");
            $("#Cant" + fila).val($("#CantAnt" + fila).val())
            $("#Descuento" + fila).val($("#DescuentoAnt" + fila).val());
            return false;
        }

        if ($("#Producto" + fila).val() != "TRA") {
            if ($.trim(datos[0]) == "-2") {
                $.jGrowl(ValidarTraduccion("Existencia no disponible"), { life: 4500, theme: 'growl-warning', header: '' });
                $("#Cant" + fila).val(cant_a);
                $("#Exist" + fila).val((datos[1] * 1) + (cant_a * 1));
                $("#Cant" + fila).focus();
                TotalMalla();
                return false
            }
        }
        
        if ($.trim(datos[0]) == "0") {
            $.jGrowl(datos[1], { life: 4500, theme: 'growl-warning', header: '' });
            return false
        }

        $("#Id" + fila).val(datos[0]);
        $.jGrowl(ValidarTraduccion("Temporal Guardado"), { life: 2500, theme: 'growl-success', header: '' });
        $("#Estado").val("Temporal");
        $("#Exist" + fila).val((datos[1] * 1) + (cant_a * 1));
        Estado = "Temporal";
        $("#CantAnt" + fila).val($("#Cant" + fila).val())
        $("#DescuentoAnt" + fila).val($("#Descuento" + fila).val());

        $("#btnguardar").removeClass("hidden");
           
    }
}

function ModalConsultarRemisiones() {
    $("#modalConsultarRemisiones").modal("show");
}

function ModalConsultarSeparadas() {
    $("#modalConsultarSeparadas").modal("show");
}

function ModalConsultarFacturas() {
    $("#modalConsultarFacturas").modal("show");
}


function ModalConsultarPedidos() {
    $("#modalConsultarPedidos").modal("show");    
}

function ReporteFacturas() {
    var pedido = $("#FPedido").val() * 1;
    var factura = $("#FFactura").val() * 1;

    var cliente = $.trim($("#FCliente").val());
    var tipo = $("#FTipo").val();
    var estado = $("#FEstado").val();
    var fechad = $("#FFechaDesde").val();
    var fechah = $("#FFechaHasta").val();
    var dias = $("#FUltimoDia").val() * 1;

    if (fechad == "" && fechah == "" && dias == 0) {
        swal("", ValidarTraduccion("debe de ingresar una fecha o números de días"), "error");
        return false;
    }
    document.getElementById('preloader').style.display = 'block';
    setTimeout(function () {
        var labestado = "";
        var parametros = "pedido=" + pedido + "&factura=" + factura + "&cliente=" + cliente + "&estado=" + estado + "&tipo=" + tipo + "&fechad=" + fechad + "&fechah=" + fechah + "&dias=" + dias;
        var datos = LlamarAjax(url + 'Facturacion/ReporteDetFactura', parametros);
        datos = datos.split("|");
        if (datos[0] == "0") {
            window.open(url + "DocumPDF/" + datos[1]);
        } else {
            swal("Acción Cancelada",datos[1],"warning");
        }
    }, 15);
    document.getElementById('preloader').style.display = 'none';


}

function ModalReporteFactura() {
    $("#modalReporteFacturas").modal("show");
}

function CargarModalRepFac(opcion) {

    var grupo = $("#ReGrupo").val() * 1;
    var distrito = $("#ReDistrito").val() * 1;
    var ciudad = $("#ReCiudad").val() * 1;
    var estado = $("#ReEstado").val();
    var asesor = $("#ReAsesor").val() * 1;
    var zventa = $("#ReZVenta").val() * 1;
    var zubicacion = $("#ReZUbicacion").val() * 1;

    var factura = $("#ReFactura").val() * 1;

    var cliente = $("#ReCliente").val() * 1;
    var tipo = $("#ReTipo").val();
    var estado = $("#ReEstado").val();
    var fechad = $("#ReFechaDesde").val();
    var fechah = $("#ReFechaHasta").val();
    var dias = $("#ReUltimoDia").val() * 1;

    if (fechad == "" && fechah == "" && dias == 0) {
        swal("", ValidarTraduccion("debe de ingresar una fecha o números de días"), "error");
        return false;
    }

    document.getElementById('preloader').style.display = 'block';
    setTimeout(function () {
        var parametros = "opcion=" + opcion + "&factura=" + factura + "&estado=" + estado + "&tipo=" + tipo + "&fechad=" + fechad + "&fechah=" + fechah + "&dias=" + dias;
            
        var datos = LlamarAjax(url + "Facturacion/ReporteGenFactura", parametros);
        document.getElementById('preloader').style.display = 'none';

        datos = datos.split("|");

        if (datos[0] == "0") {
            if (opcion == 1) {
                var datajson = JSON.parse(datos[1]);
                $('#tablamodalrepfac').DataTable({
                    data: datajson,
                    bProcessing: true,
                    bDestroy: true,
                    columns: [
                        { "data": "FvnNumero" },
                        { "data": "FvnCodCliente" },
                        { "data": "NomRazSoc" },
                        { "data": "TipoCliente" },
                        { "data": "TipoArticulo" },
                        { "data": "FVnFechaFactura" },
                        { "data": "FvnVencimiento" },
                        { "data": "Estado" },
                        { "data": "FvnDebDescuento" },
                        { "data": "Subtotal" },
                        { "data": "Iva" },
                        { "data": "Flete" },
                        { "data": "Total" },
                        { "data": "Abonado" },
                        { "data": "FVnValor" },
                        { "data": "VMontura" },
                        { "data": "VVarios" },
                        { "data": "VLOftalmicos" },
                        { "data": "VLContacto" },
                        { "data": "VTrabajos" },
                        { "data": "usuario" },
                        { "data": "usu_Anula" },
                        { "data": "obs_anulacion" }
                    ],

                    "columnDefs": [
                        { "type": "numeric-comma", targets: 3 }
                    ],

                    "language": {
                        "url": LenguajeDataTable
                    },
                    dom: 'Bfrtip',
                    buttons: [
                        'excel', 'csv', 'copy'
                    ]
                });
            } else {
                window.open(url + "DocumPDF/" + datos[1]);
            }
        } else {
            swal(datos[1], "", "warning");
        }

    }, 15);

}


function CargarModalFacturas() {
    var pedido = $("#FPedido").val() * 1;
    var factura = $("#FFactura").val() * 1;

    var cliente = $.trim($("#FCliente").val());
    var tipo = $("#FTipo").val();
    var estado = $("#FEstado").val();
    var fechad = $("#FFechaDesde").val();
    var fechah = $("#FFechaHasta").val();
    var dias = $("#FUltimoDia").val() * 1;

    if (fechad == "" && fechah == "" && dias == 0) {
        swal("", ValidarTraduccion("debe de ingresar una fecha o números de días"), "error");
        return false;
    }


    var resultado = '<table class="table table-bordered" id="tablamodalfacturas"><thead><tr><th width="8%"><span idioma="Número">' + ValidarTraduccion('Número') + '</span></th><th><span idioma="Código">' + ValidarTraduccion('Código') + '</span></th><th width="20%"><span idioma="Cliente">' + ValidarTraduccion('Cliente') + '</span></th><th><span idioma="Artículo">' + ValidarTraduccion('Artículo') + '</span></th><th><span idioma="Valor">' + ValidarTraduccion('Valor') + '</span></th><th><span idioma="Pedido">' + ValidarTraduccion('Pedido') + '</span></th><th><span idioma="Facturado">' + ValidarTraduccion('Facturado') + '</span></th><th><span idioma="Vencimiento">' + ValidarTraduccion('Vencimiento') + '</span></th><th><span idioma="Estado">' + ValidarTraduccion('Estado') + '</span></th></tr></thead><tbody>';
    document.getElementById('preloader').style.display = 'block';

    setTimeout(function () {
        var labestado = "";
        var parametros = "pedido=" + pedido + "&factura=" + factura + "&cliente=" + cliente + "&estado=" + estado + "&tipo=" + tipo + "&fechad=" + fechad + "&fechah=" + fechah + "&dias=" + dias;
        var datos = LlamarAjax(url + 'Facturacion/ModalFacturas', parametros);
        if (datos != "[]") {
            eval("data=" + datos);
            for (var x = 0; x < data.length; x++) {
                switch (data[x].Estado) {
                    case "Cancelada":
                        labestado = "<label class='label label-warning'>" + ValidarTraduccion(data[x].Estado) + "</label>";
                        break;
                    case "Facturada":
                        labestado = "<label class='label label-primary'>" + ValidarTraduccion(data[x].Estado) + "</label>";
                        break;
                    case "Anulada":
                        labestado = "<label class='label label-danger'>" + ValidarTraduccion(data[x].Estado) + "</label>";
                        break;
                    case "Despachada":
                        labestado = "<label class='label label-success'>" + ValidarTraduccion(data[x].Estado) + "</label>";
                        break;
                }

                resultado += "<tr onclick=\"BuscarPedido(" + data[x].FVnNoPedido + ")\">" +
                    "<td>" + data[x].FVnNumero + "</td>" +
                    "<td>" + data[x].FVnCodCliente + "</td>" +
                    "<td><a class='btn-default' href=\"javascript:BuscarPedido(" + data[x].FVnNoPedido + ")\">" + data[x].NomRazSoc + "</a></td>" +
                    "<td>" + data[x].FVnTipVenta + "</td>" +
                    "<td align='right'>" + formato_numero(data[x].FVnValor, decimales, ".", ",", SiglaMoneda) + "</td>" +
                    "<td>" + data[x].FVnNoPedido + "</td>" +
                    "<td>" + data[x].FVnFechaFactura + "</td>" +
                    "<td>" + data[x].FVnVencimiento + "</td>" +
                    "<td>" + labestado + "</td></tr>";
            }
        }

        resultado += "</tbody></table>"
        $("#divmodalfacturas").html(resultado);
        document.getElementById('preloader').style.display = 'none';
        ActivarDataTable("tablamodalfacturas", 1);


    }, 15);
}


function CargarModalPedidos() {

    var pedido = $("#MPedido").val() * 1;
    var factura = $("#MFactura").val() * 1;

    var cliente = 0;
    var tipo = $("#MTipo").val();
    var estado = $("#MEstado").val();
    var fechad = $("#MFechaDesde").val();
    var fechah = $("#MFechaHasta").val();
    var dias = $("#MUltimoDia").val() * 1;

    if (fechad == "" && fechah == "" && dias == 0  ) {
        swal("Alerta", "debe de ingresar una fecha o números de días ", "error");
        return false;
    }
     

    var resultado = '<table class="table table-bordered" id="tablamodalmpedidos"><thead><tr><th width="5%"><span idioma="Pedido">Pedido</span></th><th width="5%"> <span idioma="Código">Código</span></th><th><span idioma="Artículo">Artículo</span></th><th><span idioma="Valor">Valor</span></th><th><span idioma="Estado">Estado</span></th><th><span idioma="Tomado">Tomado</span></th><th><span idioma="Cerrado">Cerrado</span></th><th><span idioma="Facturado">Facturado</span></th><th><span idioma="NFactura">NFactura</span></th><th><span idioma="Despachado">Despachado</span></th><th><span idioma="Usuario">Usuario</span></th><th><span idioma="Almacén">Almacén</span></th></tr></thead><tbody>';
    document.getElementById('preloader').style.display = 'block';

    setTimeout(function () {
        var labestado = "";
        var parametros = "pedido=" + pedido + "&factura=" + factura + "&cliente=" + cliente + "&estado=" + estado + "&tipo=" + tipo + "&fechad=" + fechad + "&fechah=" + fechah + "&dias=" + dias;
        var datos = LlamarAjax(url + 'Facturacion/ModalPedidos', parametros);
        if (datos != "[]") {
            eval("data=" + datos);
            for (var x = 0; x < data.length; x++) {
                switch (data[x].Estado) {
                    case "Pendiente":
                        labestado = "<label class='label label-warning'>" + data[x].Estado + "</label>";
                        break;
                    case "Facturado":
                        labestado = "<label class='label label-success'>" + data[x].Estado + "</label>";
                        break;
                    case "Anulado":
                        labestado = "<label class='label label-danger'>" + data[x].Estado + "</label>";
                        break;
                    case "Impreso":
                        labestado = "<label class='label label-info'>" + data[x].Estado + "</label>";
                        break;
                    case "Temporal":
                        labestado = "<label class='label label-default'>" + data[x].Estado + "</label>";
                        break;
                    case "Cerrado":
                        labestado = "<label class='label label-primary'>" + data[x].Estado + "</label>";
                        break;
                    case "Despachado":
                        labestado = "<label class='label label-success'>" + data[x].Estado + "</label>";
                        break;
                }

                resultado += "<tr onclick=\"BuscarPedido(" + data[x].Numero + ")\">" +
                    "<td>" + data[x].Numero + "</td>" +
                    "<td>" + data[x].CodCli + "</td>" +
                    "<td>" + data[x].TipoArticulo + "</td>" +
                    "<td align='right'>" + formato_numero(data[x].Valor, decimales, ".", ",", SiglaMoneda) + "</td>" +
                    "<td>" + labestado + "</td>" +
                    "<td>" + data[x].PeFecTomado + "</td>" +
                    "<td>" + data[x].PeFecEnviado + "</td>" +
                    "<td>" + data[x].PeFecFacturado + "</td>" +
                    "<td>" + data[x].NumFactura + "</td>" +
                    "<td>" + data[x].PeFecEntregado + "</td>" +
                    "<td>" + data[x].Usuario + "</td>" +
                    "<td>" + data[x].almacen_original + "</td></tr>";
            }
        }

        resultado += "</tbody></table>"
        $("#divmodalpedidos").html(resultado);
        document.getElementById('preloader').style.display = 'none';
        ActivarDataTable("tablamodalmpedidos", 1);

        
    }, 15);

}

function BusCodIniFinMont(marca) {
    var datos = LlamarAjax(url + "Facturacion/BusCodIniFin", "marca=" + (marca*1));
    if ($.trim(datos) != "0") {
        datos = datos.split("|");
        $("#SCodDes").val(datos[0]);
        $("#SCodHas").val(datos[1]);
    }

}

function CargarModalSeparadas() {

    
    var cliente = $("#SCliente").val()*1;
    var coddes = $("#SCodDes").val() * 1;
    var codhas = $("#SCodHas").val() * 1;
    var tipo = $("#STipo").val();
    var estado = $("#SEstado").val();
    var fechad = $("#SFechaDesde").val();
    var fechah = $("#SFechaHasta").val();
    var dias = $("#SUltimoDia").val() * 1;

    
    var resultado = '<table class="table table-bordered" id="tablamodalseparadas"><thead><tr><th width="8%"><span idioma="CodCliente">CodCliente</span></th><th><span idioma="Cliente">Cliente</span></th><th width="7%"><span idioma="Código">Código</span></th><th><span idioma="Artículo">Artículo</span></th><th><span idioma="Exist.">Exist.</span></th><th><span idioma="Separadas">Separadas</span></th><th><span idioma="Nro Pedido">Nro Pedido</span></th><th><span idioma="Estado">Estado</span></th><th><span idioma="Fecha">Fecha</span></th></tr></thead><tbody>';
    document.getElementById('preloader').style.display = 'block';

    setTimeout(function () {
        var labestado = "";
        var tseparadas = 0;
        var parametros = "codigod=" + coddes + "&codigoh=" + codhas + "&codcli=" + cliente + "&estado=" + estado + "&tipo=" + tipo + "&fechad=" + fechad + "&fechah=" + fechah + "&dias=" + dias;
        var datos = LlamarAjax(url + 'Facturacion/ModalSeparadas', parametros);
        if (datos != "[]") {
            eval("data=" + datos);
            for (var x = 0; x < data.length; x++) {
                switch (data[x].Estado) {
                    case "Pendiente":
                        labestado = "<label class='label label-warning'>" + data[x].Estado + "</label>";
                        break;
                    case "Facturado":
                        labestado = "<label class='label label-success'>" + data[x].Estado + "</label>";
                        break;
                    case "Anulado":
                        labestado = "<label class='label label-danger'>" + data[x].Estado + "</label>";
                        break;
                    case "Impreso":
                        labestado = "<label class='label label-info'>" + data[x].Estado + "</label>";
                        break;
                    case "Temporal":
                        labestado = "<label class='label label-default'>" + data[x].Estado + "</label>";
                        break;
                    case "Cerrado":
                        labestado = "<label class='label label-primary'>" + data[x].Estado + "</label>";
                        break;
                }

                resultado += "<tr>" +
                    "<td>" + data[x].CodCli + "</td>" +
                    "<td>" + data[x].NomRazSoc + "</td>" +
                    "<td>" + data[x].Codigo + "</td>" +
                    "<td>" + data[x].Montura + "</td>" +
                    "<td>" + data[x].Existencia + "</td>" +
                    "<td>" + data[x].Separadas + "</td>" +
                    "<td>" + data[x].Pedido + "</td>" +
                    "<td>" + labestado + "</td>" +
                    "<td>" + data[x].Fecha + "</td></tr>";
                tseparadas += (data[x].Separadas * 1);
            }
        }

        resultado += "</tbody></table>"
        $("#divmodalseparadas").html(resultado);
        document.getElementById('preloader').style.display = 'none';
        ActivarDataTable("tablamodalseparadas", 1);
        $("#TotalSepadas").html("<br> Total Separadas: " + formato_numero(tseparadas, "0", ",", ".", ""));
        
    }, 15);

}
function CargarModalMonturas() {

    var modelo = $("#Modelo").val();
    var cfab = $("#Marca").val() * 1;
    
    var resultado = '<table class="table table-bordered" id="tablamodalmontura"><thead><tr><th><span idioma="Código">' + ValidarTraduccion('Código') + '</span></th><th><span idioma="Marca">' + ValidarTraduccion('Marca') + '</span></th><th><span idioma="Modelo">' + ValidarTraduccion('Modelo') + '</span></th><th><span idioma="Color">' + ValidarTraduccion('Color') + '</span></th><th><span idioma="Aro">' + ValidarTraduccion('Aro') + '</span></th><th><span idioma="Puente">' + ValidarTraduccion('Puente') + '</span></th><th><span idioma="Grupo">' + ValidarTraduccion('Grupo') + '</span></th><th><span idioma="Material">' + ValidarTraduccion('Material') + '</span></th><th><span idioma="Exist">' + ValidarTraduccion('Exist') + '</span></th><th><span idioma="Precio">' + ValidarTraduccion('Precio') + '</span></th><th><span idioma="Precio">' + ValidarTraduccion('IVA') + '</span></th><th><span idioma="DescMax"></span>' + ValidarTraduccion('DescMax') + '</th></tr></thead><tbody>';
    document.getElementById('preloader').style.display = 'block';

    setTimeout(function () {

        var diferencia = 0
        var labelexixtencia = 0
        var parametros = "CFab=" + cfab + "&modelo=" + modelo + "&tablaprecio=" + TablaPrecioMont;
        var datos = LlamarAjax(url + 'Facturacion/ModalMonturas', parametros);
        if (datos != "[]") {
            eval("data=" + datos);
            for (var x = 0; x < data.length; x++) {
                if (data[x].Existencia <= 0)
                    labelexixtencia = "<label class='label label-danger'>" + data[x].Existencia + "</label>";
                else {
                    labelexixtencia = "<label class='label label-success'>" + data[x].Existencia + "</label>";
                }

                resultado += "<tr onclick=\"SeleccionarMontura(" + data[x].Codigo + "," + FilaMalla + "," + data[x].Existencia + ",1,0)\">" +
                    "<td><a class='btn-default' href=\"javascript:SeleccionarMontura(" + data[x].Codigo + "," + FilaMalla + "," + data[x].Existencia + ",1,0)\">" + data[x].Codigo + "</a></td>" +
                    "<td>" + data[x].Marca + "</td>" +
                    "<td>" + data[x].Modelo + "</td>" +
                    "<td>" + data[x].Color + "</td>" +
                    "<td>" + data[x].Aro + "</td>" +
                    "<td>" + data[x].Puente + "</td>" +
                    "<td>" + data[x].Grupo + "</td>" +
                    "<td>" + data[x].Material + "</td>" +
                    "<td align='center'>" + labelexixtencia + "</td>" +
                    "<td align='right'>" + formato_numero(data[x].Precio, decimales, ".", ",", SiglaMoneda) + "</td>" +
                    "<td align='center'>" + data[x].Iva + "</td>" +
                    "<td align='center'>" + data[x].DescMax + "</td></tr>";
            }
        }

        resultado += "</tbody></table>"
        $("#divmodalmontura").html(resultado);
        ActivarDataTable("tablamodalmontura", 1);

        document.getElementById('preloader').style.display = 'none';
    }, 15);

}


function CerrarPedido() {

    if (Estado == "Anulado") {
        swal("No se puede enviar un pedido anulado");
        return false;
    }

    if (NPedido == 0) {
        return false;
    }
          
    $("#eCliente").val(CodCliente + " " + $("#NombreCliente").val());
    $("#sppedido").html(NPedido)
    $("#eCorreo").val(Cli_Correo)
    
    for (var x = 0; x < a_Codigo.length; x++){
        if (a_Descripcion[x].value != "" && a_Cant[x].value * 1 <= 0) {
            swal(ValidarTraduccion("La fila Nro") + " " + (x + 1) + " " + ValidarTraduccion("la cantidad es") + " <= 0 ");
            return false;
        }

        if (a_Descripcion[x].value != "" && (a_Id[x].value * 1) == 0) {
            swal(ValidarTraduccion("Debe guardar primero el pedido"));
            return false;
        }
    }

    $("#enviar_orden").modal("show");

    ImprimirPedido(1);

}

function ReporteReciboCaja() {
    document.getElementById('preloader').style.display = 'block';
    setTimeout(function () {
        var datos = LlamarAjax(url + "Facturacion/ReporteRecibos", "");
        document.getElementById('preloader').style.display = 'none';
        datos = datos.split("|");
        if (datos[0] == "0")
            window.open(url + "DocumPDF/" + datos[1]);
        else
            swal(ValidarTraduccion("Acción Cancelada"), ValidarTexto(datos[1]), "warning");
    }, 15);
}


function ImprimirPedido(tipo) {

    var pedido = NPedido;
        
    if ((pedido == 0) && (tipo != 4))
        return false;
    
    document.getElementById('preloader').style.display = 'block';
    setTimeout(function () {
        var datos = LlamarAjax(url + "Facturacion/ReportePedido", "pedido=" + pedido);
        if (tipo == 1) {
            $("#resultadopdfpedido").attr("src", url + "DocumPDF/" + datos);
            Pdf_Pedido = datos;
        }
        if (tipo == 2 || tipo == 4) {
            window.open(url + "DocumPDF/" + datos);
        }
        
        document.getElementById('preloader').style.display = 'none';
    }, 15);

}


function TotalMalla() {

    var tsubtotal = 0;
    var tdescuento = 0;
    var tbaseimponible = 0;
    var texcento = 0;
    var nfac = NFactura * 1;
    var tiva = 0;
    var ttotalpagar = 0;
    var cliconsumo = 0;
    var clidisponible = 0;
    var flete = 0;
    var iva = 0;
    var descuento = 0;
    var valordes = 0;
    var existencia = 0;

    var tunidad = 0;
    var tpeso = 0;

    
                
    for (var x = 0; x < a_Codigo.length; x++) {

        
        if ($.trim(a_Codigo[x].value) == "") 
            break;
        
        if ((a_Codigo[x].id == "Codigo" + FilaMalla) && (NFactura == 0)) {
            
            if ((a_DescMax[x].value * 1 == 0 && DesMaxUsu == 0) && a_Descuento[x].value * 1 > 0) {
                $.jGrowl(ValidarTraduccion("Descuento no permitido"), { life: 2500, theme: 'growl-warning', header: '' });
                a_Descuento[x].value = "";
            }

            if (a_Cant[x].value == "0") {
                a_Cant[x].value = "";
            }

            if (a_Descuento[x].value == "0") {
                a_Descuento[x].value = "";
            }

            if (a_Descuento[x].value * 1 > 100) {
                $.jGrowl(ValidarTraduccion("Descuento no puede ser mayor al") + " 100%", { life: 2500, theme: 'growl-warning', header: '' });
                a_Descuento[x].value = "";
            }

            if ((a_Descuento[x].value * 1 > a_DescMax[x].value * 1) && (a_Descuento[x].value * 1 > DesMaxUsu * 1)) {
                $.jGrowl(ValidarTraduccion("Descuento no puede ser mayor a ") + " " + a_DescMax[x].value + "% ", { life: 2500, theme: 'growl-warning', header: '' });
                a_Descuento[x].value = "";
            }
        }
                
        descuento = NumeroDecimal(a_VUnitario[x].value) * a_Cant[x].value * (100 - (a_Descuento[x].value * 1)) / 100;
        valordes = NumeroDecimal(a_VUnitario[x].value) * a_Cant[x].value - descuento;
        iva = descuento * a_Iva[x].value / 100;
        a_VDescuento[x].value = valordes;

        tunidad += a_Cant[x].value * 1;
        tpeso += NumeroDecimal(a_Peso[x].value) * a_Cant[x].value;

        a_SubTotal[x].value = descuento;

        tiva += iva;
        tdescuento += valordes;
        tsubtotal += descuento
        ttotalpagar += descuento + iva;
        if ((TipCliente == 2 || TipCliente == 5 || TipCliente == 4) && (NFactura == "")) {
            cliconsumo = ttotalpagar;
            clidisponible = limite - clisaldo - cliconsumo;
            if (clidisponible < 0) {
                $.jGrowl(ValidarTraduccion("Límite de crédito no disponible"), { life: 2500, theme: 'growl-warning', header: '' });
                a_Cant[FilaMalla].value = "";
                //TotalMalla()
                return false;
            }
        }



        if (iva > 0) {
            a_Gravable[x].value = descuento * 1;
            tbaseimponible += descuento * 1;
        }
        else {
            texcento += descuento * 1;
            a_Excento[x] += descuento * 1;
        }

        a_SubTotal[x].value = a_SubTotal[x].value == 0 ? "0" : formato_numero(a_SubTotal[x].value, decimales, ".", ",", "");

        
    }

    if (NPedido == 0) {
        if (tsubtotal >= MinimoFlete)
            Flete = 0;
        else
            Flete = tsubtotal * ValorFlete / MinimoFlete;

        $("#Flete").val((Flete == 0 ? "0" : formato_numero(Flete, decimales, ".", ",", SiglaMoneda)));
    }

    ttotalpagar += Flete; 
                
    $("#tsubtotal").val((tsubtotal == 0 ? "0" : formato_numero(tsubtotal,decimales,".",",", SiglaMoneda)));
    $("#tdescuento").val((tdescuento == 0 ? "0" : formato_numero(tdescuento, decimales, ".", ",", SiglaMoneda)));
    $("#tbaseimponible").val((tbaseimponible == 0 ? "0" : formato_numero(tbaseimponible, decimales, ".", ",", SiglaMoneda)));
    $("#texcento").val((texcento == 0 ? "0" : formato_numero(texcento, decimales, ".", ",", SiglaMoneda)));
    $("#tiva").val((tiva == 0 ? "0" : formato_numero(tiva, decimales, ".", ",", SiglaMoneda)));
    $("#ttotalpagar").val((ttotalpagar == 0 ? "0" : formato_numero(ttotalpagar, decimales, ".", ",", SiglaMoneda)));
    
        

    if (NFactura == "") {
        $("#cliconsumo").html((ttotalpagar == 0 ? "0" : formato_numero(ttotalpagar, decimales, ".", ",", SiglaMoneda)));
        $("#clidisponible").html((clidisponible == 0 ? "0" : formato_numero(clidisponible, decimales, ".", ",", SiglaMoneda)));
    }
                
}


$("#formenviarpedido").submit(function (e) {
    document.getElementById('preloader').style.display = 'block';
    setTimeout(function () {
        var parametros = "pedido=" + NPedido + "&email=" + $("#eCorreo").val() + "&archivo=" + Pdf_Pedido + "&cliente=" + $("#eCliente").val();
        var datos = LlamarAjax(url + "Facturacion/EnvioPedido", parametros);
        document.getElementById('preloader').style.display = 'none';
        datos = datos.split("|");
        if (datos[0] == "0") {
            $("#enviar_orden").modal("hide");
            $("#btnCerrar").addClass("hidden");
            Cli_Correo = $("#eCorreo").val();
            if ((Estado == "") || (Estado == "Pendiente")) {
                $("#Estado").val(ValidarTraduccion("Cerrado"));
                Estado = "Cerrado";
            }
            swal("", ValidarTraduccion(datos[1]), "success");
        } else
            swal(ValidarTraduccion("Acción Cancelada"), ValidarTraduccion(datos[1]), "warning");
        
    }, 15);
    
    return false;
})

function LimpiarMalla() {
        
    var iva = 0;
    var descuento = 0;
    var existencia = 0;
        
    $("#Malla tbody tr").remove();
    
    $("#tsubtotal").val("0");
    $("#tdescuento").val("0");
    $("#trecibido").val("0");
    $("#tbaseimponible").val("0");
    $("#texcento").val("0");
    $("#tiva").val("0");
    $("#ttotalpagar").val("0");
    
    TotalFila = 0;
    
}



function ModalMaximizarFoto(codigo, descripcion, valor) {

    codigo = codigo.split("|");
    CodigoSelFoto = valor;
        
    $("#imagen11").attr("src", urlfoto + codigo[0] + "/" + codigo[1] + "1.jpg");
    $("#imagen12").attr("src", urlfoto + codigo[0] + "/" + codigo[1] + "2.jpg");
    $("#imagen13").attr("src", urlfoto + codigo[0] + "/" + codigo[1] + "3.jpg");

    $("#imagen13").css("display", "none");
    $("#imagen12").css("display", "none");
    $("#imagen11").css("display", "block");

    $("#desmonturafoto2").html(descripcion);

    $("#divmodalmonturafoto").css("display", "none");
    $("#divmodalmaximizar").css("display", "block");

    $("#BtnSelecFoto").removeClass("hidden");
    

}

function CerraMaxFoto() {
    $("#divmodalmonturafoto").css("display", "block");
    $("#divmodalmaximizar").css("display", "none");
    $("#BtnSelecFoto").addClass("hidden");
}

function MaximizarFoto() {

    var codigo = $("#Codigo" + FilaMalla).val() * 1;
    var Foto = $("#Foto" + FilaMalla).val() * 1;
    var CFab = $("#CFab" + FilaMalla).val() * 1;
    var Descripcion = $.trim($("#Descripcion" + FilaMalla).val());

    if (Descripcion == "")
        return false;
    if (Foto == "0") {
        $.jGrowl("Producto sin foto", { life: 2500, theme: 'growl-warning', header: '' });
        return false
    }

    $("#imagen1").attr("src", urlfoto + CFab + "/" + codigo + "1.jpg");
    $("#imagen2").attr("src", urlfoto + CFab + "/" + codigo + "2.jpg");
    $("#imagen3").attr("src", urlfoto + CFab + "/" + codigo + "3.jpg");

    $("#imagen3").css("display", "none");
    $("#imagen2").css("display", "none");
    $("#imagen1").css("display", "block");

    $("#desmonturafoto").html(Descripcion);
    $("#maximizarfoto").modal('show');

}




function CargarModelosFotos(cfab, modelo) {

    var fab = cfab * 1;

    if (cfab * 1 == 0)
        cfab = $("#MarcaF").val() * 1;

    $("#divmodalmonturafoto").html("");
    $("#divmodalmonturafoto").css("display", "block");
    $("#divmodalmaximizar").css("display", "none");

    if (cfab * 1 == 0 && $.trim(modelo) == "") {
        return false;
    }



    document.getElementById('preloader').style.display = 'block';
    setTimeout(function () {

        var datos = LlamarAjax(url + "Facturacion/ModelosColores", "CFab=" + cfab + "&modelo=" + modelo);
        datos = datos.split("|");

        $("#divmodalmonturafoto").html("");
        var opciones = "<option value=''>--SELECCIONE--</option>";
        var pos = 1;
        var cant = 0;
        var detalles = "<div class='row'>"
        if (datos[0] != "[]") {
            eval("data=" + datos[0]);
            for (var x = 0; x < data.length; x++) {
                if (cant == 4) {
                    detalles += "</div>";
                    detalles += "<div class='form-group row'>";
                    cant = 1;
                } else
                    cant += 1;

                foto = urlfoto + data[x].CFab + "/" + data[x].Codigo + "1.jpg";
                detalles += "<div class='col-xs-6 col-md-3 col-sm-3'><a href=\"javascript:ModalMaximizarFoto('" + data[x].CFab + "|" + data[x].Codigo + "','" + data[x].Modelo + "','" + data[x].CFab + "|" + data[x].Codigo + "|" + data[x].Modelo + "|" + data[x].Existencia + "')\"><center><img class='border_img' src='" + foto + "'/><span class='text-primary'>" + data[x].Modelo + "</span></center></a></div>";
            }

            if (cant != 4)
                detalles += "</div>";

        }
        if (fab > 0) {
            eval("data=" + datos[1]);
            for (var x = 0; x < data.length; x++)
                opciones += "<option class='padOptopn' value='" + data[x].Modelo + "'>" + data[x].Modelo + "</Option>";
            $("#ModeloF").html(opciones);
        }

        $("#divmodalmonturafoto").html(detalles + "</div>");

        document.getElementById('preloader').style.display = 'none';
    }, 15);
}

function SeleccionarMonturaFoto() {
    if (CodigoSelFoto == "")
        return false;
    var cantidad = $("#CantFoto").val() * 1;
    if (cantidad == 0) {
        $("#CantFoto").focus();
        swal("Acción Cancelada", "Debe de ingresar la cantidad a vender", "warning");
        return false;
    }

    var codigo = CodigoSelFoto.split("|");

    if ((cantidad * 1) > (codigo[3]*1)) {
        $("#CantFoto").focus();
        swal("Acción Cancelada", "Existencia no disponible. Solo puede pedir "  + codigo[3] + " montura(s)" , "warning");
        return false;
    }

    
    SeleccionarMontura(codigo[1], FilaMalla, codigo[3], cantidad, 1);
    FilaMalla += 1;
    $("#BtnSelecFoto").addClass("hidden");
}

function SeleccionarCliente(cliente) {
    $("#modalClientes").modal("hide");
    CodCliente = 0;
    $("#Cliente").val(cliente);
    $("#Cliente").blur();
}

function SeleccionarMontura(codigo, fila, existencia, cantidad, cerrar) {
    if (existencia * 1 <= 0) {
        $.jGrowl(ValidarTraduccion("Producto sin existencia"), { life: 2500, theme: 'growl-warning', header: '' });
    } else {
        $("#Codigo" + fila).val(codigo);
        if (cerrar == 0) {
            $("#modalmontura").modal("hide");
            $("#modalmonturafoto").modal("hide");
        }
        BuscarItemMontura(codigo,'MONT');
        $("#Cant" + fila).val(cantidad);
        $("#Cant" + fila).select();
        $("#Cant" + fila).focus();
    }
}

function ValidarPedido() {

    if (Estado != "Pendiente" && Estado != "Impreso") {
        swal("", ValidarTraduccion("No se puede validar un pedido con el estado") + " " + Estado + ", " + ValidarTraduccion("Guarde el pedido"), "warning");
        return false;
    }
    
    if (NPedido == 0)
        return false;

    $("#divtablamodalresultoma").html("");

    var datos = LlamarAjax(url + "Facturacion/ValidarPedido", "pedido=" + NPedido);
    if (datos == "1") {
        swal("", ValidarTraduccion("No se puede Validar este pedido porque ya esta Facturado o Anulado"), "warning");
        return false;
    }
    if (datos == "[]") {
        swal("", ValidarTraduccion("No hay monturas para verificar Pedidos"), "warning");
        return false;
    }
    var diferencia = 0;
    var labestado;
    var resultado = '<table class="table table- bordered" id="tablamodalresultoma"><thead><tr><th width="6%">Nro</th><th width="15%"><span idioma="Código">Código</span></th><th width="64%"><span idioma="Descripción">Descripción</span></th><th width="15%"><span idioma="Resultado">Resultado</span></th></tr></thead><tbody>';                                    
                                
    eval("data=" + datos);
    
    for (var x = 0; x < data.length; x++) {
        diferencia = data[x].Can * 1 - data[x].Veri*1;
        if ((data[x].Can * 1) == 0 && (data[x].Veri * 1) > 0) {
            labestado = "<label class='label label-danger' style='font-size:14px'>" + data[x].Veri + " No Pedido</label>";
        } else {
            if (diferencia == 0) {
                labestado = "<label class='label label-success' style='font-size:14px'>" + data[x].Can + " Verificados</label>";
            } else {
                if (diferencia > 0) {
                    labestado = "<label class='label label-warning' style='font-size:14px'>Faltan " + diferencia + " de " + data[x].Can + "</label >";
                } else {
                    if (diferencia < 0) {
                        labestado = "<label class='label label-info' style='font-size:14px'>Sobra " + (diferencia * -1) + " Pedido " + data[x].Can + "</label > ";
                    } else {
                        if (data[x].Can * 1 == 0 || data[x].Veri > 0) {
                            labestado = "<label class='label label-danger' style='font-size:14px'>" + data[x].Veri + " No Pedido</label>";
                        }
                    }
                }
            }
        }
                            
        resultado += "<tr>" +
            "<td>" + (x+1) + "</a></td>" +
            "<td>" + data[x].Codigo + "</td>" +
            "<td>" + data[x].Descripcion + "</td>" +
            "<td>" + labestado + "</td></tr>";
    }

    resultado += "</tbody></table>";
    $("#divtablamodalresultoma").html(resultado);

    ActivarDataTable("tablamodalresultoma", 6);
    $("#toma1").removeClass("active")
    $("#toma2").addClass("active")

    $("#lectura").removeClass("active in")
    $("#toma").addClass("active in")
    
}




function SeleccionarFila(pos, id, valor) {
        
    if (NFactura > 0)
        return False

            
    if ($("#" + id).prop("checked")) {
        $("#tr-" + pos).addClass("bg-success");
    } else {
        $("#tr-" + pos).removeClass("bg-success");
    }    

    CalcularTotalTra();
    
}



function ReimprimirPedidos(){
    $("#modalReimprimirPedidos").modal("show")
}

function CargarReimprimirPedidos() {

    var estado = $("#RIEstado").val();
    var minuto = $("#RItiempo").val()*1;
    var numdes = $("#RInumdes").val()*1;
    var numhas = $("#RInumhas").val()*1;

    document.getElementById('preloader').style.display = 'block';
    setTimeout(function () {
        var parametros = "pedido=0&numdes=" + numdes + "&numhas=" + numhas + "&estado=" + estado +"&imprimir=1";
        var datos = LlamarAjax(url + "Facturacion/ReportePedido", parametros);
        if (datos == "1") {
            swal("", "No hay pedidos que reportar", "warning");
        } else {
            window.open(url + "DocumPDF/" + datos);
            $("#modalReimprimirPedidos").modal("hide");
        }
        document.getElementById('preloader').style.display = 'none';
    }, 15);
}
    

$("#formenviarcartera").submit(function (e) {
    document.getElementById('preloader').style.display = 'block';
    setTimeout(function () {
        var parametros = "codcli=" + CodCliente + "&email=" + $("#CaCorreo").val() + "&archivo=" + Pdf_Cartera + "&cliente=" + $("#CaCliente").val();
        var datos = LlamarAjax(url + "Facturacion/EnvioCartera", parametros);
        document.getElementById('preloader').style.display = 'none';
        datos = datos.split("|");
        if (datos[0] == "0") {
            swal("Excelente", ValidarTraduccion(datos[1]), "success");
            $("#ModalFacturasCLientes").modal("hide");
            Cli_Correo = $("#eCorreo").val();
            if ((Estado == "") || (Estado == "Pendiente")) {
                $("#Estado").val(ValidarTraduccion("Cerrado"));
                Estado = "Cerrado";
            }
        } else
            swal("Error", datos[1], "error");

    }, 15);

    return false;
    
})

function ModalCarteraCli() {
    if ($("#NombreCliente").val() != "") {
        $("#CaCliente").val(CodCliente + " " + $("#NombreCliente").val());
        $("#CaCorreo").val(Cli_Correo)
        document.getElementById('preloader').style.display = 'block';
        setTimeout(function () {
            var datos = LlamarAjax(url + "Facturacion/Cartera", "cliente=" + CodCliente);
            datos = datos.split("|");
            if (datos[0] == "0") {
                $("#resultadopdcartera").attr("src", url + "DocumPDF/" + datos[1]);
                Pdf_Cartera = datos[1];
                $("#ModalFacturasCLientes").modal("show");
            } else {
                swal("Acción Cancelada",datos[1],"warning")
            }
            document.getElementById('preloader').style.display = 'none';
        }, 15);
    }
}

function ModalEstadoCuenta() {
    if (CodCliente > 0) {
        $("#modalEstadoCliente").modal("show");
    }
}

function ModalEditarPedidos() {
    $("#EPedido").val(NPedido);
    $("#modalEditarPedido").modal("show");
}

function ModalClientes() {
    $("#modalClientes").modal("show");
    $("#CValor").focus();
}

function ModalMonturasFotos() {
    $("#modalmonturafoto").modal("show");
    $("#MarcaF").focus();
}

function CatalogoMontura() {
    if (($.trim($("#NombreCliente").val()) != "") && (NFactura  == "") && ($("#Codigo" + FilaMalla).prop("disabled") == false))
        ModalMonturasFotos();
}

function CargarModelos(CFab) {
    if (CFab * 1 == 0) {
        $("#Modelo").html("");
        return false;
    }
    var datos = LlamarAjax(url + "Base/CargarCombos", "tipo=12&inicial=1&CFab=" + CFab);
    $("#Modelo").html(datos);
}

function CargarGrupos(GrupoVAR) {
    if (GrupoVAR * 1 == 0) {
        $("#Fabricante").html("");
        return false;
    }
    var datos = LlamarAjax(url + "Base/CargarCombos", "tipo=16&inicial=1&CFab=0&GrupoVAR=" + GrupoVAR);
    $("#Fabricante").html(datos);
}

function FBuscarMontura() {
    if (($.trim($("#NombreCliente").val()) != "") && (NFactura == "") && ($("#Codigo" + FilaMalla).prop("disabled") == false)) {
        ModalMonturas();
            
    }
}

function BuscarMontura(codigo, fila, tipo, code) {
    //ErrorMalla
    if (($.trim($("#NombreCliente").val()) != "") && (NFactura * 1 == 0) && ($("#Codigo" + FilaMalla).prop("disabled") == false)) {
        if (code.keyCode == 13  || tipo == 1) {
            FilaMalla = fila;
            if (codigo.trim() == "") {
                if (modal == 0 && tipo == 0) {
                    ModalMonturas()();
                }else
                    modal = 0;
            } else {
                BuscarItemMontura(codigo,'MONT');
            }
        }
    }
}

function SeleccionarLO() {

    var esfera = $("#LEsfera").val();
    var cilindro = $.trim($("#LCilindro").val());
    var adicion = $("#LAdicion").val();
    var precio = $("#LPrecio").val();
    var codigo = $("#LDerecho").val();

    if (cilindro == "")
        cilindro = "0.00";

    var data = $('#LTipo').select2('data')
    var Descripcion = "Efe:" + (esfera *1 >= 0 ? "+" : "") + esfera + " Cil:" + cilindro + (adicion * 1 > 0 ? "Adi:" + adicion : "") + " " + data[0].text + " Lote:";
    $("#Codigo" + FilaMalla).val(codigo);
    $("#Codigo" + FilaMalla).prop("disabled", true);
    $("#Producto" + FilaMalla).prop("disabled", true);
    $("#Descripcion" + FilaMalla).val(Descripcion);
    $("#Descuento" + FilaMalla).val(0);
    $("#Foto" + FilaMalla).val("");
    $("#CFab" + FilaMalla).val("");
    $("#Peso" + FilaMalla).val("0");
    $("#DescMax" + FilaMalla).val("0");
    $("#VUnitario" + FilaMalla).val(precio);
    $("#Cant" + FilaMalla).val("1");
    if (CaracCliente < 6) {
        $("#Iva" + FilaMalla).val(data[0].Iva);
    } else {
        $("#Iva" + FilaMalla).val("0");
    }

    $("#Exist" + FilaMalla).val("10");
    $("#ExisInici" + FilaMalla).val("10");
    $("#CantPedido" + FilaMalla).val("0");
    $("#FechaPedido" + FilaMalla).val("");
    $("#PedOriginal" + FilaMalla).val("");
    $("#CostoPromedio" + FilaMalla).val("0");
    
    $("#Verificado" + FilaMalla).val(0);

    $("#modalLO").modal("hide");
    $("#Cant" + FilaMalla).select();
    $("#Cant" + FilaMalla).focus;
    
}

function BuscarItemMontura(codigo, tipo) {
    var tablaprecio = 0;
    switch (tipo) {
        case "LO":
            if ($("#LTerminado").prop("checked"))
                tablaprecio = $("#TablaPreciosTermi").val() * 1;
            else
                tablaprecio = $("#TablaPreciosTalla").val() * 1;
            break;
        case "MONT":
            tablaprecio = TablaPrecioMont;
            break;
        case "VAR":
            tablaprecio = $("#TablaPreciosVar").val() * 1;
            break;
        case "TRA":
            tablaprecio = $("#TablaPreciosTra").val() * 1;
            break;
    }
    if (tipo != "LO") {
        var datos = LlamarAjax(url + "Facturacion/BuscarDescripcion", "codigo=" + codigo + "&tablaprecio=" + tablaprecio + "&tipo=" + tipo);
        if (datos != "[]") {
            eval("data=" + datos);
            $("#Cant" + FilaMalla).val("1");
            if (tipo != "TRA") {
                if (data[0].Existencia * 1 <= 0) {
                    $.jGrowl(ValidarTraduccion("Producto") + " " + data[0].Marca + " " + data[0].Modelo + " " + data[0].Color + " " + data[0].Aro + "-" + data[0].Puente + " " + ValidarTraduccion("sin existencia"), { life: 2500, theme: 'growl-warning', header: '' });
                    $("#Codigo" + FilaMalla).select();
                    $("#Codigo" + FilaMalla).focus();
                    return false;
                }
            }
            if (tipo == "MONT") {
                $("#Codigo" + FilaMalla).val(data[0].Codigo);
                $("#Descripcion" + FilaMalla).val(data[0].Marca + " " + data[0].Modelo + " " + data[0].Color + " " + data[0].Aro + "-" + data[0].Puente);
                $("#Descuento" + FilaMalla).val(CliDesMon);
                $.jGrowl(ValidarTraduccion("Aplicando el ") + CliDesMon + "%" + ValidarTraduccion(" de descuento"), { life: 2500, theme: 'growl-success', header: '' });

                $("#Foto" + FilaMalla).val(data[0].Foto);
                $("#CFab" + FilaMalla).val(data[0].CFab);
                $("#Peso" + FilaMalla).val(data[0].Peso);
                $("#DescMax" + FilaMalla).val(CliDesMon);

                                
            }

            if (tipo == "VAR") {
                $("#Descripcion" + FilaMalla).val(data[0].Descripcion + " " + data[0].Presentacion);
                $("#Descuento" + FilaMalla).val(CliDesVar);
                $("#DescMax" + FilaMalla).val(CliDesVar);
            }

            if (tipo == "TRA") {
                $("#Descripcion" + FilaMalla).val(data[0].Descripcion);
                $("#Descuento" + FilaMalla).val(CliDesVar);
                $("#DescMax" + FilaMalla).val(CliDesVar);
            }


            $("#VUnitario" + FilaMalla).val(formato_numero(data[0].Precio, decimales, ".", ",", ""));
            if (CaracCliente < 6) {
                $("#Iva" + FilaMalla).val(data[0].Iva);
            } else {
                $("#Iva" + FilaMalla).val("0");
            }

            $("#Exist" + FilaMalla).val(data[0].Existencia);
            $("#ExisInici" + FilaMalla).val(data[0].Existencia);
            $("#CostoPromedio" + FilaMalla).val(data[0].CostoPromedio);
            
            $("#Pedido" + FilaMalla).val("0");

            TotalMalla();

            $("#Codigo" + FilaMalla).prop("disabled", true)
            $("#Producto" + FilaMalla).prop("disabled", true)

            $("#Cant" + FilaMalla).select();
            $("#Cant" + FilaMalla).focus();

        } else {
            $.jGrowl(ValidarTraduccion("Código no registrado") + " " + tipo, { life: 2500, theme: 'growl-warning', header: '' });
            $("#Codigo" + FilaMalla).select();
            $("#Codigo" + FilaMalla).focus();
        }
    } else {
        var codbarra = 0;
        if ($("#CodigoBarras").prop("checked"))
            codbarra = 1;

        var datos = LlamarAjax(url + "Facturacion/DescripcionLC", "codigo=" + codigo + "&TablaPrecio=" + tablaprecio + "&codigobarra=" + codbarra);
        datos = datos.split("|");

        if (datos[0] == "1") {
            $.jGrowl(datos[1], { life: 2500, theme: 'growl-warning', header: '' });
            $("#Codigo" + FilaMalla).select();
            $("#Codigo" + FilaMalla).focus();
            return false;
        }

        $("#Codigo" + FilaMalla).val(datos[1]);
        var Descripcion = "Efe:" + (datos[3] * 1 >= 0 ? "+" : "") + formato_numero(datos[3] * 1, 2, ".", ",", "") + " Cil:" + formato_numero(datos[4] * 1, 2, ".", ",", "") + (datos[5] * 1 > 0 ? "Adi:" + formato_numero(datos[5] * 1, 2, ".", ",", "") : "") + " " + datos[2] + " Lote: " + datos[10];
        $("#Descripcion" + FilaMalla).val(Descripcion);
        $("#Descuento" + FilaMalla).val("0");
        $("#DescMax" + FilaMalla).val("0");

        $("#VUnitario" + FilaMalla).val(formato_numero(datos[6] * 1, decimales, ".", ",", ""));
        $("#Cant" + FilaMalla).val(1);
        $("#Iva" + FilaMalla).val("0");

        $("#Exist" + FilaMalla).val(datos[7]);
        $("#ExisInici" + FilaMalla).val(datos[7]);
        $("#CostoPromedio" + FilaMalla).val(datos[8]);
                
        $("#Pedido" + FilaMalla).val("0");

        TotalMalla();

        $("#Codigo" + FilaMalla).prop("disabled", true)
        $("#Producto" + FilaMalla).prop("disabled", true)

        $("#Cant" + FilaMalla).select();
        $("#Cant" + FilaMalla).focus();

    }






}

$("#imagen1").click(function (e) {

    $("#imagen2").css("display", "block");
    $("#imagen1").css("display", "none");
    $("#imagen3").css("display", "none");
});

$("#imagen2").click(function (e) {
    $("#imagen3").css("display", "block");
    $("#imagen2").css("display", "none");
    $("#imagen1").css("display", "none");

});

$("#imagen3").click(function (e) {
    $("#imagen3").css("display", "none");
    $("#imagen2").css("display", "none");
    $("#imagen1").css("display", "block");
      

});





$("#imagen11").click(function (e) {
    $("#imagen12").css("display", "block");
    $("#imagen11").css("display", "none");;
    $("#imagen13").css("display", "none");;
});

$("#imagen12").click(function (e) {
    $("#imagen13").css("display", "block");
    $("#imagen12").css("display", "none");
    $("#imagen11").css("display", "none");
});

$("#imagen13").click(function (e) {
    $("#imagen13").css("display", "none");
    $("#imagen12").css("display", "none");
    $("#imagen11").css("display", "block");
});

cliente = localStorage.getItem("cliente");
$("#Cliente").val(cliente);

BuscarCliente(cliente);

$("#DireccionEnv").html(LlamarAjax(url + "Facturacion/ComboDireccion", "cliente=" + cliente));

$('select').select2();


