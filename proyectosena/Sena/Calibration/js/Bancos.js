﻿
$("#Cue_Banco").html(CargarCombo(24, 1));
$("#Cue_CuentaContable").html(CargarCombo(83, 1));
$("#Ban_Estado, #Cue_Estado").html(CargarCombo(37, 0));
$("#Cue_Tipo").html(CargarCombo(88, 1));

tb = $('input');
$(tb).keypress(enter2tab);

tb = $('select');
$(tb).keypress(enter2tab); 

function enter2tab(e) {
    if (e.keyCode == 13) {
        id = $(this).attr("id");

        cb = parseInt($(this).attr('tabindex'));

        if (id == "Documento") {
            if ($.trim($(this).val()) != "") {

                BuscarCliente($(this).val());
                $("#Sede").focus();
                return false;
            }
        }

        if ($(':input[tabindex=\'' + (cb + 1) + '\']') != null) {
            $(':input[tabindex=\'' + (cb + 1) + '\']').focus();
            $(':input[tabindex=\'' + (cb + 1) + '\']').select();
            e.preventDefault();

            return false;
        }

        if ($(':select[tabindex=\'' + (cb + 1) + '\']') != null) {
            console.log("1");
            $(':select[tabindex=\'' + (cb + 1) + '\']').focus();
            $(':select[tabindex=\'' + (cb + 1) + '\']').select();
            e.preventDefault();

            return false;
        }
    }
}

function TablaBancos() {
    var datos = LlamarAjax('Configuracion/TablaConfiguracion', "tipo=11&id=0");
    var datasedjson = JSON.parse(datos);
    $('#TablaBancos').DataTable({
        data: datasedjson,
        bProcessing: true,
        bDestroy: true,
        columns: [
            { "data": "id" },
            { "data": "descripcion" },
            { "data": "tipoarchivo" },
            { "data": "consignar" },
            { "data": "desestado" },
            { "data": "eliminar" }
        ],
        "language": {
            "url": LenguajeDataTable
        }
    });
}

function LimpiarBancos() {
    $("#Ban_Formato").val("").trigger("change");
    $("#Ban_Estado").val("1").trigger("change");
    $("#Ban_Nombre").val("")
    $("#Ban_Consignar").val("0")
    $("#Ban_Id").val("0")
}

$("#TablaBancos > tbody").on("dblclick", "tr", function () {
    var row = $(this).parents("td").context.cells;
    var numero = row[0].innerText;
    var datos = LlamarAjax('Configuracion/TablaConfiguracion', "tipo=11&id=" + numero);
    var data = JSON.parse(datos);
    $("#Ban_Formato").val(data[0].tipoarchivo).trigger("change");
    $("#Ban_Estado").val(data[0].estado).trigger("change");
    $("#Ban_Nombre").val(data[0].descripcion);
    $("#Ban_Consignar").val(data[0].consignar);
    $("#Ban_Id").val(data[0].id);
});


$("#formBancos").submit(function (e) {
    e.preventDefault();
    var parametros = $(this).serialize();
    var datos = LlamarAjax("Configuracion/GuardarBanco", parametros).split("|");
    if (datos[0] == "0") {
        TablaBancos();
        $("#Cue_Banco").html(CargarCombo(24, 1));
        LimpiarBancos();
        swal("", datos[1], "success");
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
});

function EliminarBanco(id, cuenta) {
    var accion = 1;
    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: ValidarTraduccion('¿Desea eliminar la cuenta bancaria ' + cuenta + '?'),
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Eliminar'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        allowOutsideClick: false,

        preConfirm: function (result) {
            return new Promise(function (resolve, reject) {
                $.post("Configuracion/EliminarBanco", "banco=" + id)
                    .done(function (data) {
                        datos = data.split("|");
                        if (datos[0] == "0")
                            resolve()
                        else {
                            reject(datos[1]);
                        }

                    })
            })
        }
    }]).then(function (data) {
        LimpiarBancos();
        TablaBancos();
        $("#Cue_Banco").html(CargarCombo(24, 1));
    });
}


//************************CUENTA BANCARIA********************************

function LimpiarCuentas() {
    $("#Cue_Estado").val("1").trigger("change");
    $("#Cue_Banco").val("").trigger("change");
    $("#Cue_Id").val("0")
    $("#Cue_Cuenta").val("");
    $("#Cue_Tipo").val("").trigger("change");
    $("#Cue_CuentaContable").val("").trigger("change");
}

function CambioBanco() {
    TablaCuentas();
}

$("#TablaCuentas > tbody").on("dblclick", "tr", function () {
    var row = $(this).parents("td").context.cells;
    var numero = row[0].innerText;
    var datos = LlamarAjax('Configuracion/TablaConfiguracion', "tipo=12&id=" + numero);
    var data = JSON.parse(datos);
    $("#Cue_Banco").val(data[0].idbanco).trigger("change");
    $("#Cue_Estado").val(data[0].estado).trigger("change");
    $("#Cue_Cuenta").val(data[0].cuenta);
    $("#Cue_Tipo").val(data[0].idtipocuenta).trigger("change");
    $("#Cue_CuentaContable").val(data[0].idcuecontable * 1 > 0 ? data[0].idcuecontable : "").trigger("change");
    $("#Cue_Id").val(data[0].id);
});


$("#formCuentaBancaria").submit(function (e) {
    e.preventDefault();
    var parametros = $(this).serialize();
    var datos = LlamarAjax("Configuracion/GuardarCuentaBanco", parametros).split("|");
    if (datos[0] == "0") {
        LimpiarCuentas();
        swal("", datos[1], "success");
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }

});

   

function TablaCuentas() {
    var banco = $("#Cue_Banco").val() * 1;
    var datos = LlamarAjax('Configuracion/TablaConfiguracion', "tipo=12&id=0&banco=" + banco);
    var datasedjson = JSON.parse(datos);
    $('#TablaCuentas').DataTable({
        data: datasedjson,
        bProcessing: true,
        bDestroy: true,
        columns: [
            { "data": "id" },
            { "data": "banco" },
            { "data": "tipocuenta" },
            { "data": "cuenta" },
            { "data": "cuentacontable" },
            { "data": "desestado" },
            { "data": "eliminar" }
        ],
        "language": {
            "url": LenguajeDataTable
        }
    });
}

function EliminarCuentaBanco(id, cuenta) {
    var accion = 1;
    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: ValidarTraduccion('¿Desea eliminar la cuenta bancaria ' + cuenta + '?'),
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Eliminar'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        allowOutsideClick: false,
        
        preConfirm: function (result) {
            return new Promise(function (resolve, reject) {
                $.post("Configuracion/EliminarBancoCuenta", "cuenta=" + id)
                    .done(function (data) {
                        datos = data.split("|");
                        if (datos[0] == "0")
                            resolve()
                        else {
                            reject(datos[1]);
                        }
                            
                    })
            })
        }
    }]).then(function (data) {
       LimpiarCuentas();
    });
}


TablaBancos();
TablaCuentas();

$('select').select2();
DesactivarLoad();
