﻿d = new Date();
month = d.getMonth() + 1;
day = d.getDate();

var Anio = $.trim(d.getFullYear());
Anio = $.trim(Anio.substr(2, 2))


output = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-01';

output2 = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-' +
    (day < 10 ? '0' : '') + day;

$("#FechaDesde").val(output);
$("#FechaHasta").val(output2);
$("#Magnitud").html(CargarCombo(1, 1));

var ctabla1 = document.getElementById('myTabla1').getContext('2d');
var ctabla2 = document.getElementById('myTabla2').getContext('2d');
var ctabla3 = document.getElementById('myTabla3').getContext('2d');
var ctabla4 = document.getElementById('myTabla4').getContext('2d');
var ctabla5 = document.getElementById('myTabla5').getContext('2d');
var ctabla6 = document.getElementById('myTabla6').getContext('2d');
var ctabla7 = document.getElementById('myTabla7').getContext('2d');
var ctabla10 = document.getElementById('myTabla10').getContext('2d');

var myTabla1 = null;
var myTabla2 = null;
var myTabla3 = null;
var myTabla4 = null;
var myTabla5 = null;
var myTabla6 = null;
var myTabla7 = null;
var myTabla10 = null;

var movil = 0;
if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
    movil = 1;
}

function CargarTablas() {
    var magnitud = $("#Magnitud").val() * 1;
    var fechaini = $("#FechaDesde").val();
    var fechafin = $("#FechaHasta").val();
    var newDataset;

    if (magnitud == 0) {
        $("#Magnitud").focus();
        swal("Acción Cancelada", "Debe seleccionar una magnitud", "warning");
        return false;
    }

    if (fechaini == "" || fechafin == "") {
        $("#fechaini").focus();
        swal("Acción Cancelada", "Debe seleccionar la fecha inicial y fecha final", "warning");
        return false;
    }
            
    ActivarLoad();
    setTimeout(function () {

        var config1 = {
            type: 'pie',
            data: {
                datasets: [],
                labels: []
            },
            options: {
                responsive: true,
                tooltips: {
                    enabled: false
                },
                legend: {
                    position: 'left',
                    display: (movil == 1 ? false : true)
                },

                animation: {
                    duration: 0,
                    easing: "easeOutQuart",
                    onComplete: function () {
                        var ctx = this.chart.ctx;
                        ctx.font = 'bold 14px LatoRegular, Helvetica,sans-serif';
                        ctx.textAlign = 'center';
                        ctx.textBaseline = 'bottom';
                        this.data.datasets.forEach(function (dataset) {
                            for (var i = 0; i < dataset.data.length; i++) {
                                var m = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model,
                                    t = dataset._meta[Object.keys(dataset._meta)[0]].total,
                                    mR = m.innerRadius +
                                        (m.outerRadius - m.innerRadius) / 2,
                                    sA = m.startAngle,
                                    eA = m.endAngle,
                                    mA = sA + (eA - sA) / 2;
                                var x = mR * 1.8 * Math.cos(mA);
                                var y = mR * 1.8 * Math.sin(mA);
                                ctx.fillStyle = '#000';

                                var p = String(dataset.data[i] / total * 100);
                                if (dataset.data[i] > 0) {
                                    ctx.fillText(formato_numero(p, 2, ".", ",", "%"), m.x + x, m.y + y + 5);
                                }
                            }
                        });
                    }
                }

            }
        }

        var config2 = {
            type: 'pie',
            data: {
                datasets: [],
                labels: []
            },
            options: {
                responsive: true,
                tooltips: {
                    enabled: false
                },
                legend: {
                    position: 'left',
                    display: (movil == 1 ? false : true)
                },

                animation: {
                    duration: 0,
                    easing: "easeOutQuart",
                    onComplete: function () {
                        var ctx = this.chart.ctx;
                        ctx.font = 'bold 14px LatoRegular, Helvetica,sans-serif';
                        ctx.textAlign = 'center';
                        ctx.textBaseline = 'bottom';
                        this.data.datasets.forEach(function (dataset) {
                            for (var i = 0; i < dataset.data.length; i++) {
                                var m = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model,
                                    t = dataset._meta[Object.keys(dataset._meta)[0]].total,
                                    mR = m.innerRadius +
                                        (m.outerRadius - m.innerRadius) / 2,
                                    sA = m.startAngle,
                                    eA = m.endAngle,
                                    mA = sA + (eA - sA) / 2;
                                var x = mR * 1.8 * Math.cos(mA);
                                var y = mR * 1.8 * Math.sin(mA);
                                ctx.fillStyle = '#000';

                                var p = String(dataset.data[i] / total * 100);
                                if (dataset.data[i] > 0) {
                                    ctx.fillText(formato_numero(p, 2, ".", ",", "%"), m.x + x, m.y + y + 5);
                                }
                            }
                        });
                    }
                }

            }
        }

        var config3 = {
            type: 'pie',
            data: {
                datasets: [],
                labels: []
            },
            options: {
                responsive: true,
                tooltips: {
                    enabled: false
                },
                legend: {
                    position: 'left',
                    display: (movil == 1 ? false : true)
                },

                animation: {
                    duration: 0,
                    easing: "easeOutQuart",
                    onComplete: function () {
                        var ctx = this.chart.ctx;
                        ctx.font = 'bold 14px LatoRegular, Helvetica,sans-serif';
                        ctx.textAlign = 'center';
                        ctx.textBaseline = 'bottom';
                        this.data.datasets.forEach(function (dataset) {
                            for (var i = 0; i < dataset.data.length; i++) {
                                var m = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model,
                                    t = dataset._meta[Object.keys(dataset._meta)[0]].total,
                                    mR = m.innerRadius +
                                        (m.outerRadius - m.innerRadius) / 2,
                                    sA = m.startAngle,
                                    eA = m.endAngle,
                                    mA = sA + (eA - sA) / 2;
                                var x = mR * 1.8 * Math.cos(mA);
                                var y = mR * 1.8 * Math.sin(mA);
                                ctx.fillStyle = '#000';

                                var p = String(dataset.data[i] / total * 100);
                                if (dataset.data[i] > 0) {
                                    ctx.fillText(formato_numero(p, 2, ".", ",", "%"), m.x + x, m.y + y + 5);
                                }
                            }
                        });
                    }
                }

            }
        }

        var config4 = {
            type: 'pie',
            data: {
                datasets: [],
                labels: []
            },
            options: {
                responsive: true,
                tooltips: {
                    enabled: false
                },
                legend: {
                    position: 'left',
                    display: (movil == 1 ? false : true)
                },

                animation: {
                    duration: 0,
                    easing: "easeOutQuart",
                    onComplete: function () {
                        var ctx = this.chart.ctx;
                        ctx.font = 'bold 14px LatoRegular, Helvetica,sans-serif';
                        ctx.textAlign = 'center';
                        ctx.textBaseline = 'bottom';
                        this.data.datasets.forEach(function (dataset) {
                            for (var i = 0; i < dataset.data.length; i++) {
                                var m = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model,
                                    t = dataset._meta[Object.keys(dataset._meta)[0]].total,
                                    mR = m.innerRadius +
                                        (m.outerRadius - m.innerRadius) / 2,
                                    sA = m.startAngle,
                                    eA = m.endAngle,
                                    mA = sA + (eA - sA) / 2;
                                var x = mR * 1.8 * Math.cos(mA);
                                var y = mR * 1.8 * Math.sin(mA);
                                ctx.fillStyle = '#000';

                                var p = String(dataset.data[i] / total * 100);
                                if (dataset.data[i] > 0) {
                                    ctx.fillText(formato_numero(p, 2, ".", ",", "%"), m.x + x, m.y + y + 5);
                                }
                            }
                        });
                    }
                }

            }
        }

        var config5 = {
            type: 'pie',
            data: {
                datasets: [],
                labels: []
            },
            options: {
                responsive: true,
                tooltips: {
                    enabled: false
                },
                legend: {
                    position: 'left',
                    display: (movil == 1 ? false : true)
                },

                animation: {
                    duration: 0,
                    easing: "easeOutQuart",
                    onComplete: function () {
                        var ctx = this.chart.ctx;
                        ctx.font = 'bold 14px LatoRegular, Helvetica,sans-serif';
                        ctx.textAlign = 'center';
                        ctx.textBaseline = 'bottom';
                        this.data.datasets.forEach(function (dataset) {
                            for (var i = 0; i < dataset.data.length; i++) {
                                var m = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model,
                                    t = dataset._meta[Object.keys(dataset._meta)[0]].total,
                                    mR = m.innerRadius +
                                        (m.outerRadius - m.innerRadius) / 2,
                                    sA = m.startAngle,
                                    eA = m.endAngle,
                                    mA = sA + (eA - sA) / 2;
                                var x = mR * 1.8 * Math.cos(mA);
                                var y = mR * 1.8 * Math.sin(mA);
                                ctx.fillStyle = '#000';

                                var p = String(dataset.data[i] / total * 100);
                                if (dataset.data[i] > 0) {
                                    ctx.fillText(formato_numero(p, 2, ".", ",", "%"), m.x + x, m.y + y + 5);
                                }
                            }
                        });
                    }
                }

            }
        }

        var config6 = {
            type: 'pie',
            data: {
                datasets: [],
                labels: []
            },
            options: {
                responsive: true,
                tooltips: {
                    enabled: false
                },
                legend: {
                    position: 'left',
                    display: (movil == 1 ? false : true)
                },

                animation: {
                    duration: 0,
                    easing: "easeOutQuart",
                    onComplete: function () {
                        var ctx = this.chart.ctx;
                        ctx.font = 'bold 14px LatoRegular, Helvetica,sans-serif';
                        ctx.textAlign = 'center';
                        ctx.textBaseline = 'bottom';
                        this.data.datasets.forEach(function (dataset) {
                            for (var i = 0; i < dataset.data.length; i++) {
                                var m = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model,
                                    t = dataset._meta[Object.keys(dataset._meta)[0]].total,
                                    mR = m.innerRadius +
                                        (m.outerRadius - m.innerRadius) / 2,
                                    sA = m.startAngle,
                                    eA = m.endAngle,
                                    mA = sA + (eA - sA) / 2;
                                var x = mR * 1.8 * Math.cos(mA);
                                var y = mR * 1.8 * Math.sin(mA);
                                ctx.fillStyle = '#000';

                                var p = String(dataset.data[i] / total * 100);
                                if (dataset.data[i] > 0) {
                                    ctx.fillText(formato_numero(p, 2, ".", ",", "%"), m.x + x, m.y + y + 5);
                                }
                            }
                        });
                    }
                }

            }
        }

        var config7 = {
            type: 'pie',
            data: {
                datasets: [],
                labels: []
            },
            options: {
                responsive: true,
                tooltips: {
                    enabled: false
                },
                legend: {
                    position: 'left',
                    display: (movil == 1 ? false : true)
                },

                animation: {
                    duration: 0,
                    easing: "easeOutQuart",
                    onComplete: function () {
                        var ctx = this.chart.ctx;
                        ctx.font = 'bold 14px LatoRegular, Helvetica,sans-serif';
                        ctx.textAlign = 'center';
                        ctx.textBaseline = 'bottom';
                        this.data.datasets.forEach(function (dataset) {
                            for (var i = 0; i < dataset.data.length; i++) {
                                var m = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model,
                                    t = dataset._meta[Object.keys(dataset._meta)[0]].total,
                                    mR = m.innerRadius +
                                        (m.outerRadius - m.innerRadius) / 2,
                                    sA = m.startAngle,
                                    eA = m.endAngle,
                                    mA = sA + (eA - sA) / 2;
                                var x = mR * 1.8 * Math.cos(mA);
                                var y = mR * 1.8 * Math.sin(mA);
                                ctx.fillStyle = '#000';

                                var p = String(dataset.data[i] / total * 100);
                                if (dataset.data[i] > 0) {
                                    ctx.fillText(formato_numero(p, 2, ".", ",", "%"), m.x + x, m.y + y + 5);
                                }
                            }
                        });
                    }
                }

            }
        }

        var config10 = {
            type: 'pie',
            data: {
                datasets: [],
                labels: []
            },
            options: {
                responsive: true,
                tooltips: {
                    enabled: false
                },
                legend: {
                    position: 'left',
                    display: (movil == 1 ? false : true)
                },

                animation: {
                    duration: 0,
                    easing: "easeOutQuart",
                    onComplete: function () {
                        var ctx = this.chart.ctx;
                        ctx.font = 'bold 14px LatoRegular, Helvetica,sans-serif';
                        ctx.textAlign = 'center';
                        ctx.textBaseline = 'bottom';
                        this.data.datasets.forEach(function (dataset) {
                            for (var i = 0; i < dataset.data.length; i++) {
                                var m = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model,
                                    t = dataset._meta[Object.keys(dataset._meta)[0]].total,
                                    mR = m.innerRadius +
                                        (m.outerRadius - m.innerRadius) / 2,
                                    sA = m.startAngle,
                                    eA = m.endAngle,
                                    mA = sA + (eA - sA) / 2;
                                var x = mR * 1.8 * Math.cos(mA);
                                var y = mR * 1.8 * Math.sin(mA);
                                ctx.fillStyle = '#000';

                                var p = String(dataset.data[i] / total * 100);
                                if (dataset.data[i] > 0) {
                                    ctx.fillText(formato_numero(p, 2, ".", ",", "%"), m.x + x, m.y + y + 5);
                                }
                            }
                        });
                    }
                }

            }
        }

        var config11 = {
            type: 'pie',
            data: {
                datasets: [],
                labels: []
            },
            options: {
                responsive: true,
                tooltips: {
                    enabled: false
                },
                legend: {
                    position: 'left',
                    display: (movil == 1 ? false : true)
                },

                animation: {
                    duration: 0,
                    easing: "easeOutQuart",
                    onComplete: function () {
                        var ctx = this.chart.ctx;
                        ctx.font = 'bold 14px LatoRegular, Helvetica,sans-serif';
                        ctx.textAlign = 'center';
                        ctx.textBaseline = 'bottom';
                        this.data.datasets.forEach(function (dataset) {
                            for (var i = 0; i < dataset.data.length; i++) {
                                var m = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model,
                                    t = dataset._meta[Object.keys(dataset._meta)[0]].total,
                                    mR = m.innerRadius +
                                        (m.outerRadius - m.innerRadius) / 2,
                                    sA = m.startAngle,
                                    eA = m.endAngle,
                                    mA = sA + (eA - sA) / 2;
                                var x = mR * 1.8 * Math.cos(mA);
                                var y = mR * 1.8 * Math.sin(mA);
                                ctx.fillStyle = '#000';

                                var p = String(dataset.data[i] / total * 100);
                                if (dataset.data[i] > 0) {
                                    ctx.fillText(formato_numero(p, 2, ".", ",", "%"), m.x + x, m.y + y + 5);
                                }
                            }
                        });
                    }
                }

            }
        }
        
        var colores = ['rgb(255, 99, 132)', 'rgb(255, 159, 64)', 'rgb(255, 205, 86)', 'rgb(75, 192, 192)', 'rgb(54, 162, 235)', 'rgb(153, 102, 255)', 'rgb(2, 254, 21)', 'rgb(6, 252, 245)', 'rgb(240, 20, 10)', 'rgb(254, 246, 2)'];

        var datos = LlamarAjax("Estadisticas","opcion=TablaInformesGerencia&magnitud=" + magnitud + "&fechaini=" + fechaini + "&fechafin=" + fechafin).split("|");
        DesactivarLoad();
        var resultado = "";
        var porcentaje;
        var tporcentaje = 0;
        var total = 0;
        var total2 = 0;
        

       

        //GRAFICA NUMERO UNO

        newDataset = {
            label: 'Data 1',
            backgroundColor: [],
            data: [],
        };

        
        var data = JSON.parse(datos[0]);

        for (var x = 0; x < data.length; x++) {
            if (x<10)
                total += data[x].cantidad * 1;
            total2 += data[x].cantidad * 1;
        }

        $("#TotalMes1").html("<h3>Total General: " + total2 + "</h3>");
                
        for (var x = 0; x < data.length; x++) {
            if (x < 10) {
                porcentaje = data[x].cantidad * 100 / total;
                tporcentaje += porcentaje;
                resultado += "<tr>" +
                    "<td>" + data[x].descripcion + "</td>" +
                    "<td align='center'>" + data[x].cantidad + "</td>" +
                    "<td align='center'>" + formato_numero(porcentaje, 2, ".", ",", "%") + "</td></tr>";
                newDataset.data.push(data[x].cantidad);
                newDataset.backgroundColor.push(colores[x]);
                config1.data.labels.push(data[x].descripcion);
            }
        }

        config1.data.datasets.push(newDataset);

        resultado += "<tr><td align='right'><b>TOTALES:</b></td><td align='center'><b>" + formato_numero(total, 0,_CD,_CM,"") + "</b></td>" +
            "<td align='center'><b>" + formato_numero(tporcentaje, 2, ".", ",", "%") + "</b></td></tr>";
        $("#Tabla1").html(resultado);

                        
        if (myTabla1 != null) {
            myTabla1.destroy();
        }



        myTabla1 = new Chart(ctabla1, config1);
        myTabla1.update();

        //GRAFICA NUMERO DOS

        resultado = "";
        tporcentaje = 0;
        total = 0;
        total2 = 0;
        var newDataset2;

        newDataset2 = {
            label: 'Data 2',
            backgroundColor: [],
            data: [],
        };

        var data = JSON.parse(datos[1]);

        for (var x = 0; x < data.length; x++) {
            if (x < 10)
                total += data[x].cantidad * 1;
            total2 += data[x].cantidad * 1;
        }

        $("#TotalMes2").html("<h3>Total General: " + total2 + "</h3>");

        for (var x = 0; x < data.length; x++) {
            if (x < 10) {
                porcentaje = data[x].cantidad * 100 / total;
                tporcentaje += porcentaje;
                resultado += "<tr>" +
                    "<td>" + data[x].descripcion + "</td>" +
                    "<td align='center'>" + data[x].cantidad + "</td>" +
                    "<td align='center'>" + formato_numero(porcentaje, 2, ".", ",", "%") + "</td></tr>";
                newDataset2.data.push(data[x].cantidad);
                newDataset2.backgroundColor.push(colores[x]);
                config2.data.labels.push(data[x].descripcion);
            }
        }

        config2.data.datasets.push(newDataset2);

        resultado += "<tr><td align='right'><b>TOTALES:</b></td><td align='center'><b>" + formato_numero(total, 0,_CD,_CM,"") + "</b></td>" +
            "<td align='center'><b>" + formato_numero(tporcentaje, 2, ".", ",", "%") + "</b></td></tr>";
        $("#Tabla2").html(resultado);
                
        if (myTabla2 != null) {
            myTabla2.destroy();
        }

        console.log(config2);
               
        myTabla2 = new Chart(ctabla2, config2);
        myTabla2.update();

        //GRAFICA NUMERO TRES

        resultado = "";
        tporcentaje = 0;
        total = 0;
        total2 = 0;
        newDataset = null;

        newDataset = {
            label: 'Data 3',
            backgroundColor: [],
            data: [],
        };

        var data = JSON.parse(datos[2]);

        for (var x = 0; x < data.length; x++) {
            if (x < 10)
                total += data[x].cantidad * 1;
            total2 += data[x].cantidad * 1;
        }

        $("#TotalMes3").html("<h3>Total General: " + total2 + "</h3>");

        for (var x = 0; x < data.length; x++) {
            if (x < 10) {
                porcentaje = data[x].cantidad * 100 / total;
                tporcentaje += porcentaje;
                resultado += "<tr>" +
                    "<td>" + data[x].descripcion + "</td>" +
                    "<td align='center'>" + data[x].cantidad + "</td>" +
                    "<td align='center'>" + formato_numero(porcentaje, 2, ".", ",", "%") + "</td></tr>";
                newDataset.data.push(data[x].cantidad);
                newDataset.backgroundColor.push(colores[x]);
                config3.data.labels.push(data[x].descripcion);
            }
        }

        config3.data.datasets.push(newDataset);

        resultado += "<tr><td align='right'><b>TOTALES:</b></td><td align='center'><b>" + formato_numero(total, 0,_CD,_CM,"") + "</b></td>" +
            "<td align='center'><b>" + formato_numero(tporcentaje, 2, ".", ",", "%") + "</b></td></tr>";
        $("#Tabla3").html(resultado);

        
        if (myTabla3 != null) {
            myTabla3.destroy();
        }
               
        myTabla3 = new Chart(ctabla3, config3);
        myTabla3.update();

        //GRAFICA NUMERO CUATRO

        resultado = "";
        tporcentaje = 0;
        total = 0;
        total2 = 0;
        newDataset = null;

        newDataset = {
            label: 'Data 4',
            backgroundColor: [],
            data: [],
        };

        var data = JSON.parse(datos[3]);

        for (var x = 0; x < data.length; x++) {
            if (x < 10)
                total += data[x].cantidad * 1;
            total2 += data[x].cantidad * 1;
        }

        $("#TotalMes4").html("<h3>Total General: " + total2 + "</h3>");

        for (var x = 0; x < data.length; x++) {
            if (x < 10) {
                porcentaje = data[x].cantidad * 100 / total;
                tporcentaje += porcentaje;
                resultado += "<tr>" +
                    "<td>" + data[x].descripcion + "</td>" +
                    "<td align='center'>" + data[x].cantidad + "</td>" +
                    "<td align='center'>" + formato_numero(porcentaje, 2, ".", ",", "%") + "</td></tr>";
                newDataset.data.push(data[x].cantidad);
                newDataset.backgroundColor.push(colores[x]);
                config4.data.labels.push(data[x].descripcion);
            }
        }

        config4.data.datasets.push(newDataset);

        resultado += "<tr><td align='right'><b>TOTALES:</b></td><td align='center'><b>" + formato_numero(total, 0,_CD,_CM,"") + "</b></td>" +
            "<td align='center'><b>" + formato_numero(tporcentaje, 2, ".", ",", "%") + "</b></td></tr>";
        $("#Tabla4").html(resultado);

        
        if (myTabla4 != null) {
            myTabla4.destroy();
        }
               
        myTabla4 = new Chart(ctabla4, config4);
        myTabla4.update();

        //GRAFICA NUMERO CINCO

        resultado = "";
        tporcentaje = 0;
        total = 0;
        total2 = 0;
        newDataset = null;

        newDataset = {
            label: 'Data 5',
            backgroundColor: [],
            data: [],
        };

        var data = JSON.parse(datos[4]);

        for (var x = 0; x < data.length; x++) {
            if (x < 10)
                total += data[x].cantidad * 1;
            total2 += data[x].cantidad * 1;
        }

        $("#TotalMes5").html("<h3>Total General: " + total2 + "</h3>");

        for (var x = 0; x < data.length; x++) {
            if (x < 10) {
                porcentaje = data[x].cantidad * 100 / total;
                tporcentaje += porcentaje;
                resultado += "<tr>" +
                    "<td>" + data[x].descripcion + "</td>" +
                    "<td align='center'>" + data[x].cantidad + "</td>" +
                    "<td align='center'>" + formato_numero(porcentaje, 2, ".", ",", "%") + "</td></tr>";
                newDataset.data.push(data[x].cantidad);
                newDataset.backgroundColor.push(colores[x]);
                config5.data.labels.push(data[x].descripcion);
            }
        }

        config5.data.datasets.push(newDataset);

        resultado += "<tr><td align='right'><b>TOTALES:</b></td><td align='center'><b>" + formato_numero(total, 0,_CD,_CM,"") + "</b></td>" +
            "<td align='center'><b>" + formato_numero(tporcentaje, 2, ".", ",", "%") + "</b></td></tr>";
        $("#Tabla5").html(resultado);
                
        if (myTabla5 != null) {
            myTabla5.destroy();
        }

        myTabla5 = new Chart(ctabla5, config5);
        myTabla5.update();

        //GRAFICA NUMERO SEIS

        resultado = "";
        tporcentaje = 0;
        total = 0;
        total2 = 0;
        newDataset = null;

        newDataset = {
            label: 'Data 5',
            backgroundColor: [],
            data: [],
        };

        var data = JSON.parse(datos[5]);

        for (var x = 0; x < data.length; x++) {
            if (x < 10)
                total += data[x].cantidad * 1;
            total2 += data[x].cantidad * 1;
        }

        $("#TotalMes6").html("<h3>Total General: " + total2 + "</h3>");

        for (var x = 0; x < data.length; x++) {
            if (x < 10) {
                porcentaje = data[x].cantidad * 100 / total;
                tporcentaje += porcentaje;
                resultado += "<tr>" +
                    "<td>" + data[x].descripcion + "</td>" +
                    "<td align='center'>" + data[x].cantidad + "</td>" +
                    "<td align='center'>" + formato_numero(porcentaje, 2, ".", ",", "%") + "</td></tr>";
                newDataset.data.push(data[x].cantidad);
                newDataset.backgroundColor.push(colores[x]);
                config6.data.labels.push(data[x].descripcion);
            }
        }

        config6.data.datasets.push(newDataset);

        resultado += "<tr><td align='right'><b>TOTALES:</b></td><td align='center'><b>" + formato_numero(total, 0,_CD,_CM,"") + "</b></td>" +
            "<td align='center'><b>" + formato_numero(tporcentaje, 2, ".", ",", "%") + "</b></td></tr>";
        $("#Tabla6").html(resultado);
                
        if (myTabla6 != null) {
            myTabla6.destroy();
        }



        myTabla6 = new Chart(ctabla6, config6);
        myTabla6.update();

        //GRAFICA NUMERO SIETE

        resultado = "";
        tporcentaje = 0;
        total = 0;
        total2 = 0;
        newDataset = null;

        newDataset = {
            label: 'Data 7',
            backgroundColor: [],
            data: [],
        };

        var data = JSON.parse(datos[6]);

        for (var x = 0; x < data.length; x++) {
            if (x < 10)
                total += data[x].cantidad * 1;
            total2 += data[x].cantidad * 1;
        }

        $("#TotalMes7").html("<h3>Total General: " + total2 + "</h3>");

        for (var x = 0; x < data.length; x++) {
            if (x < 10) {
                porcentaje = data[x].cantidad * 100 / total;
                tporcentaje += porcentaje;
                resultado += "<tr>" +
                    "<td>" + data[x].descripcion + "</td>" +
                    "<td align='center'>" + data[x].cantidad + "</td>" +
                    "<td align='center'>" + formato_numero(porcentaje, 2, ".", ",", "%") + "</td></tr>";
                newDataset.data.push(data[x].cantidad);
                newDataset.backgroundColor.push(colores[x]);
                config7.data.labels.push(data[x].descripcion);
            }
        }

        config7.data.datasets.push(newDataset);

        resultado += "<tr><td align='right'><b>TOTALES:</b></td><td align='center'><b>" + formato_numero(total, 0,_CD,_CM,"") + "</b></td>" +
            "<td align='center'><b>" + formato_numero(tporcentaje, 2, ".", ",", "%") + "</b></td></tr>";
        $("#Tabla7").html(resultado);

        if (myTabla7 != null) {
            myTabla7.destroy();
        }

        myTabla7 = new Chart(ctabla7, config7);
        myTabla7.update();

        var datajson = JSON.parse(datos[7]);
        $('#TablaInformes').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "imprimir" },
                { "data": "ingreso" },
                { "data": "cliente" },
                { "data": "equipo" },
                { "data": "informe" },
                { "data": "observacion" }
            ],
            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy','colvis'
            ]
        });

        var datajson = JSON.parse(datos[8]);
        $('#TablaQuejas').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "vimprimir" },
                { "data": "vingreso" },
                { "data": "vcliente" },
                { "data": "vequipo" },
                { "data": "vqueja" },
                { "data": "vcausa" }
            ],
            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy','colvis'
            ]
        });

        //GRAFICA NUMERO 10

        resultado = "";
        tporcentaje = 0;
        total = 0;
        total2 = 0;
        newDataset = null;

        newDataset = {
            label: 'Data 10',
            backgroundColor: [],
            data: [],
        };

        var data = JSON.parse(datos[9]);

        for (var x = 0; x < data.length; x++) {
            total += data[x].cantidad * 1;
        }

        $("#TotalMes10").html("<h3>Total General: " + total + "</h3>");

        for (var x = 0; x < data.length; x++) {
            if (x < 10) {
                porcentaje = data[x].cantidad * 100 / total;
                tporcentaje += porcentaje;
                resultado += "<tr>" +
                    "<td>" + data[x].descripcion + "</td>" +
                    "<td align='center'>" + data[x].cantidad + "</td>" +
                    "<td align='center'>" + formato_numero(porcentaje, 2, ".", ",", "%") + "</td></tr>";
                newDataset.data.push(data[x].cantidad);
                newDataset.backgroundColor.push(colores[x]);
                config10.data.labels.push(data[x].descripcion);
            }
        }

        config10.data.datasets.push(newDataset);

        resultado += "<tr><td align='right'><b>TOTALES:</b></td><td align='center'><b>" + formato_numero(total, 0,_CD,_CM,"") + "</b></td>" +
            "<td align='center'><b>" + formato_numero(tporcentaje, 2, ".", ",", "%") + "</b></td></tr>";
        $("#Tabla10").html(resultado);

        if (myTabla10 != null) {
            myTabla10.destroy();
        }

        myTabla10 = new Chart(ctabla10, config10);
        myTabla10.update();

        //GRAFICA NUMERO 11

        resultado = "";
        tporcentaje = 0;
        total = 0;
        total2 = 0;
        newDataset = null;

        newDataset = {
            label: 'Data 11',
            backgroundColor: [],
            data: [],
        };

        var data = JSON.parse(datos[10]);

        for (var x = 0; x < data.length; x++) {
            total += data[x].cantidad * 1;
        }

        porcentaje = total/data.length;
                
        for (var x = 0; x < data.length; x++) {
            
            
            resultado += "<tr>" +
                "<td>" + data[x].descripcion + "</td>" +
                "<td align='center'>" + data[x].cantidad + "</td>" +
                "<td align='center'>" + data[x].contador + "</td></tr>";
                newDataset.data.push(data[x].cantidad);
                newDataset.backgroundColor.push(colores[x]);
                config11.data.labels.push(data[x].descripcion);
        }

        //config10.data.datasets.push(newDataset);

        resultado += "<tr><td align='right'><b>TOTALES:</b></td><td align='center'><b>" + formato_numero(porcentaje, _DE,_CD,_CM,"") + "</b></td></tr>";
        $("#Tabla11").html(resultado);

        /*if (myTabla10 != null) {
            myTabla10.destroy();
        }

        myTabla10 = new Chart(ctabla10, config10);
        myTabla10.update();*/
        $("#TrabajoNoConforme").val();
        $("#AccionesPreventivas").val();
        $("#AuditoriasInternas").val();
        $("#AuditoriasExternas").val();
        $("#OportunidadesMejora").val();
        $("#Sugerencias").val();
        if (datos[11] != "[]") {
            var data = JSON.parse(datos[11]);
            $("#TrabajoNoConforme").val(data[0].trabajonoconforme);
            $("#AccionesPreventivas").val(data[0].accionespreventivas);
            $("#AuditoriasInternas").val(data[0].auditoriainterna);
            $("#AuditoriasExternas").val(data[0].auditoriaexterna);
            $("#OportunidadesMejora").val(data[0].oportunidadmejora);
            $("#Sugerencias").val(data[0].sugerencias);
        }

        
    
    }, 15);
}

$("#FormInformeGerencia").submit(function (e) {

    e.preventDefault();
        
    var magnitud = $("#Magnitud").val() * 1;
    var fechaini = $("#FechaDesde").val();
    var fechafin = $("#FechaHasta").val();
    var a_fecha = fechaini.split("-");
    var Fecha = new Date(a_fecha[0] + "/" + a_fecha[1] + "/" + a_fecha[2]);
    var ultimoDia = new Date(Fecha.getFullYear(), Fecha.getMonth() + 1, 0).getDate();
               
    if (magnitud == 0) {
        $("#Magnitud").focus();
        swal("Acción Cancelada", "Debe seleccionar una magnitud", "warning");
        return false;
    }

    if (fechaini == "" || fechafin == "") {
        $("#fechaini").focus();
        swal("Acción Cancelada", "Debe seleccionar la fecha inicial y fecha final", "warning");
        return false;
    }

    ActivarLoad();
    setTimeout(function () {
        var datos = LlamarAjax("Estadisticas","opcion=RegistroRevision&", $("#FormInformeGerencia").serialize() + "&magnitud=" + magnitud + "&fechaini=" + fechaini + "&fechafin=" + fechafin + "&ultimoDia=" + ultimoDia).split("|");
        DesactivarLoad();
        if (datos[0] == "0") {
            swal("", ValidarTraduccion(datos[1]), "success");
            ImprimirInforme();
        } else {
            swal("Acción Cancelada", ValidarTraduccion(datos[1]), "warning");
        }

    }, 15);
});

function ImprimirInforme() {
    var magnitud = $("#Magnitud").val() * 1;
    var fechaini = $("#FechaDesde").val();
    var fechafin = $("#FechaHasta").val();

    if (magnitud == 0) {
        $("#Magnitud").focus();
        swal("Acción Cancelada", "Debe seleccionar una magnitud", "warning");
        return false;
    }

    if (fechaini == "" || fechafin == "") {
        $("#fechaini").focus();
        swal("Acción Cancelada", "Debe seleccionar la fecha inicial y fecha final", "warning");
        return false;
    }

    ActivarLoad();
    setTimeout(function () {
        var datos = LlamarAjax("Estadisticas","opcion=RpInformeDireccion&magnitud=" + magnitud + "&fechaini=" + fechaini + "&fechafin=" + fechafin).split("|");
        DesactivarLoad();
        if (datos[0] == "0") {
            window.open("DocumPDF/" + datos[1]);
        } else {
            swal("Acción Cancelada", ValidarTraduccion(datos[1]), "warning");
        }

    }, 15);

}

$('select').select2();
DesactivarLoad();


