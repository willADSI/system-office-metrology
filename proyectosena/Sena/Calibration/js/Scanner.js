﻿function TablaArchivos() {
    var datos = LlamarAjax('SistemaGestion/ArchivosScanner', "");
    var datasedjson = JSON.parse(datos);
    $('#TablaScanner').DataTable({
        data: datasedjson,
        bProcessing: true,
        bDestroy: true,
        columns: [
            { "data": "Nombre" },
            { "data": "Fecha" },
            { "data": "Tamano" }
        ],
        "language": {
            "url": LenguajeDataTable
        },
        "lengthMenu": [30]
    });
}

TablaArchivos();

$("#TablaScanner > tbody").on("dblclick", "tr", function () {

    ActivarLoad();
    var row = $(this).parents("td").context.cells;
    setTimeout(function () {
        var archivo = row[0].innerText;
        var datos = LlamarAjax('SistemaGestion/AbrirArchivosScanner', "archivo=" + archivo).split("|");
        DesactivarLoad();
        if (datos[0] == "0")
            window.open(url + "ArchivoSG/" + datos[1])
        else
            swal("Acción Cancelada", datos[1], "warning");
        
    }, 15);
});

$('select').select2();
DesactivarLoad();
