﻿var IdArticulo = 0;
var IdSede = 0;
var IdContacto = 0;
var ReferenciaArt = "";


$("#Grupo, #MGrupo").html(CargarCombo(47, 1));
$("#MEquipo").html(CargarCombo(48, 1,"","-1"));
$("#MModelo").html(CargarCombo(50, 1,"","-1"));
$("#Marca, #MMarca").html(CargarCombo(49, 1));
$("#Proveedor, #MProveedor").html(CargarCombo(7, 1));
$("#Iva").html(CargarCombo(35, 0));
$("#CuentaContable").html(CargarCombo(83, 1));

tb = $('input');
$(tb).keypress(enter2tab);

tb = $('select');
$(tb).keypress(enter2tab);

function enter2tab(e) {
    if (e.keyCode == 13) {
        id = $(this).attr("id");

        cb = parseInt($(this).attr('tabindex'));

        if (id == "Referencia") {
            if ($.trim($(this).val()) != "") {

                BuscarCliente($(this).val());
                $("#Sede").focus();
                return false;
            }
        }

        if ($(':input[tabindex=\'' + (cb + 1) + '\']') != null) {
            $(':input[tabindex=\'' + (cb + 1) + '\']').focus();
            $(':input[tabindex=\'' + (cb + 1) + '\']').select();
            e.preventDefault();

            return false;
        }

        if ($(':select[tabindex=\'' + (cb + 1) + '\']') != null) {
            console.log("1");
            $(':select[tabindex=\'' + (cb + 1) + '\']').focus();
            $(':select[tabindex=\'' + (cb + 1) + '\']').select();
            e.preventDefault();

            return false;
        }
    }
}

function ValidarArticulo(Caja) {
    Referencia = Caja.value;
    if ($.trim(ReferenciaArt) != "") {
        if (ReferenciaArt != Referencia) {
            ReferenciaArt = "";
            LimpiarDatos();
        }
    }
}

function LimpiarTodo() {
    $("#Referencia").val("");
    ReferenciaArt = "";
    LimpiarDatos();
}


function LimpiarDatos() {
    IdArticulo = 0;
    ReferenciaArt = "";
    $("#TipoInventario").val("").trigger("change");
    $("#Grupo").val("").trigger("change");
    $("#Proveedor").val("").trigger("change");
    $("#Presentacion").val("");
    $("#Descripcion").val("");
    $("#Marca").val("").trigger("change");
    $("#Existencia").val("0");
    $("#IdArticulo").val("0");
    $("#UltimaFactura").val("");
    $("#Fecha").val("");
    $("#CodigoBarras").val("");
    $("#CodigoInterno").val("");
    $("#UltimoCosto").val("0");
    $("#Estado").val("1").trigger("change");
    $("#Iva").val("1").trigger("change");
    $("#Catalogo").val("NO").trigger("change");
    $("#Referencia").val("SI").trigger("change");
    $("#CuentaContable").val("").trigger("change");

    for (var x = 1; x <= 10; x++) {
        $("#Precio" + x).val("0");
    }

}

function BuscarArticulo(id) {

    
    var parametros = "id=" + id;
    var datos = LlamarAjax("Inventarios","opcion=BuscarArticulos&"+ parametros);
    if (datos != "[]") {
        var data = JSON.parse(datos);

        IdArticulo = data[0].id;
        $("#TipoInventario").val(data[0].tipo).trigger("change");
        $("#Grupo").val(data[0].idgrupo).trigger("change");
        $("#Equipo").val(data[0].idequipo).trigger("change");
        $("#CodigoInterno").val(data[0].codigo);
        $("#CodigoBarras").val(data[0].codigobarras);
        $("#Proveedor").val((data[0].proveedor * 1 == 0 ? "" : data[0].proveedor)).trigger("change");
        $("#Presentacion").val(data[0].presentacion);
        $("#Descripcion").val(data[0].descripcion);
        $("#Marca").val(data[0].idmarca).trigger("change");
        $("#Modelo").val(data[0].idmodelo).trigger("change");
        $("#Existencia").val(data[0].existencia);
        $("#IdArticulo").val(data[0].id);
        $("#UltimaFactura").val(data[0].ultimafactura);
        $("#Fecha").val(data[0].fechafac);
        $("#UltimoCosto").val(formato_numero(data[0].ultimocosto,0,".",",",""));
        $("#Estado").val(data[0].estado).trigger("change");
        $("#Iva").val(data[0].idiva).trigger("change");
        $("#Catalogo").val(data[0].catalogo).trigger("change");
        $("#Referencia").val(data[0].referencia).trigger("change");
        $("#CuentaContable").val(data[0].idcuecontable > 0 ? data[0].idcuecontable : "").trigger("change");

        $("#Precio1").val(data[0].precio1*1 > 0 ?  formato_numero(data[0].precio1, 0,_CD,_CM,"") : "0");
        $("#Precio2").val(data[0].precio2 * 1 > 0 ? formato_numero(data[0].precio2, 0, _CD, _CM, "") : "0");
        $("#Precio3").val(data[0].precio3 * 1 > 0 ? formato_numero(data[0].precio3, 0, _CD, _CM, "") : "0");
        $("#Precio4").val(data[0].precio4 * 1 > 0 ? formato_numero(data[0].precio4, 0, _CD, _CM, "") : "0");
        $("#Precio5").val(data[0].precio5 * 1 > 0 ? formato_numero(data[0].precio5, 0, _CD, _CM, "") : "0");
        $("#Precio6").val(data[0].precio6 * 1 > 0 ? formato_numero(data[0].precio6, 0, _CD, _CM, "") : "0");
        $("#Precio7").val(data[0].precio7 * 1 > 0 ? formato_numero(data[0].precio7, 0, _CD, _CM, "") : "0");
        $("#Precio8").val(data[0].precio8 * 1 > 0 ? formato_numero(data[0].precio8, 0, _CD, _CM, "") : "0");
        $("#Precio9").val(data[0].precio9 * 1 > 0 ? formato_numero(data[0].precio9, 0, _CD, _CM, "") : "0");
        $("#Precio10").val(data[0].precio10 * 1 > 0 ? formato_numero(data[0].precio10, 0, _CD, _CM, "") : "0");
    }
}

$("#formArticulos").submit(function (e) {
    e.preventDefault();
    var mensaje = "";
    var ultimocosto = NumeroDecimal($("#UltimoCosto").val());
    var iva = $("#Iva").val()*1;
    var parametros = $("#formArticulos").serialize().replace(/\%2C/g, "");
    if (ultimocosto == 0)
        mensaje += "Debe de ingresar el valor del último costo <br>";
    for (var x = 1; x <= 10; x++) {
        if ($.trim($("#Precio" + x).val()) == "")
            mensaje += "Debe de ingresar el valor del precio número " + x + " <br>";
        parametros += "&Precio" + x + "=" + NumeroDecimal($("#Precio" + x).val());
    }
    parametros += "&UltimoCosto=" + ultimocosto + "&Iva=" + iva;

    if (mensaje != "") {
        swal("Acción Cancelada", mensaje, "warning");
        return false;
    }
    var datos = LlamarAjax("Inventarios","opcion=GuardarArticulos&"+ parametros).split("|");
    if (datos["0"] == "0") {
        IdArticulo = datos[2] * 1;
        $("#IdArticulo").val(IdArticulo);
        $("#CodigoInterno").val(datos[3]);
        LimpiarTodo();
        swal("", datos[1], "success");
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
});

$("#TablaArticulos > tbody").on("dblclick", "tr", function () {
    var row = $(this).parents("td").context.cells;
    var id = row[0].innerText;
    $('#tab-inventario a[href="#tabcrear"]').tab('show');
    BuscarArticulo(id);
});


function ConsultarArticulos() {

    var presentacion =$.trim($("#MPresentacion").val());
    var tipo = $("#MTipo").val();
    var estado = $.trim($("#MEstado").val());

    var grupo = $("#MGrupo").val() * 1;
    var equipo = $("#MEquipo").val() * 1;
    var marca = $("#MMarca").val() * 1;
    var modelo = $("#MModelo").val() * 1;
    var proveedor = $("#MProveedor").val() * 1
        
    ActivarLoad();
    setTimeout(function () {
        var parametros = "presentacion=" + presentacion + "&tipo=" + tipo + "&grupo=" + grupo + "&estado=" + estado + "&marca=" + marca + "&modelo=" + modelo + "&proveedor=" + proveedor + "&equipo=" + equipo;
        var datos = LlamarAjax("Inventarios","opcion=ConsultarArticulos&"+ parametros);
        DesactivarLoad();

        var datajson = JSON.parse(datos);
        $('#TablaArticulos').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "id" },
                { "data": "tipo" },
                { "data": "grupo" },
                { "data": "codigointerno" },
                { "data": "codigobarras" },
                { "data": "equipo" },
                { "data": "marca" },
                { "data": "modelo" },
                { "data": "presentacion" },
                { "data": "existencia" },
                { "data": "proveedor" },
                { "data": "ultimocosto", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "ultimafactura" },
                { "data": "iva" },
                { "data": "estado" },
                { "data": "catalogo" },
                { "data": "referencia" }
            ],

            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy','colvis'
            ]
        });
    }, 15);
}

function CambioReferencia() {
    if (IdArticulo == 0) {
        swal("Acción Cancelada", "Debe seleccionar el cliente", "warning");
        return false;
    }
    $("#SCambioNit").html(ReferenciaArt);
    $("#modalCambioNitCli").modal("show");

}

function CambiarReferencia() {
    var Referencia = $.trim($("#DReferencia").val());
    if (Referencia == "") {
        $("#DReferencia").focus();
        swal("Acción Cancelada", "Debe de ingresar el nuevo NIT/Cédula", "warning");
        return false;
    }

    var datos = LlamarAjax("Inventarios","opcion=CambioNit&Referencia=" + Referencia + "&id=" + IdArticulo + "&tipo=1").split("|");
    if (datos[0] == "0") {
        $("#modalCambioNitCli").modal("hide");
        $("#Referencia").val(Referencia);
        BuscarCliente(Referencia);
        swal("", datos[1], "success");
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
}


function AgregarOpcion(tipo, mensaje, id) {

    var marca = $("#Marca").val() * 1
    var grupo = $("#Grupo").val() * 1

    switch (tipo) {

        case 2:
            if (grupo == 0) {
                $("#Grupo").focus();
                swal("Acción Cancelada", "Debe de seleccionar un grupo", "warning");
                return false;
            }
            break;

        case 4:
            if (marca == 0) {
                $("#Marca").focus();
                swal("Acción Cancelada", "Debe de seleccionar una marca", "warning");
                return false;
            }
            break;
    }

    swal({
        title: 'Agregar Opción',
        text: mensaje,
        input: 'text',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Agregar'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',

        inputValidator: function (value) {
            return new Promise(function (resolve, reject) {
                if ($.trim(value)) {
                    var datos = LlamarAjax("Inventarios","opcion=GuardarOpcion&tipo=" + tipo + "&grupo=" + grupo + "&marca=" + marca + " &descripcion=" + value);
                    datos = datos.split("|");
                    if (datos[0] != "XX") {
                        $("#" + id).html(datos[1]);
                        $("#" + id).val(datos[0]).trigger("change");
                        resolve();
                    } else {
                        reject(datos[1]);
                    }

                } else {
                    reject(ValidarTraduccion('Debe de ingresar la descripción de ' + mensaje));
                }
            })
        }
    }).then(function (result) {
        $("#" + id).focus();
    })
}

function CargarModelos(marca) {
    $("#Modelo").html(CargarCombo(50, 1, "", marca));
}

function DescripcionPrecio() {
    var datos = LlamarAjax("Inventarios","opcion=DescripcionPrecio&");
    var data = JSON.parse(datos);
    for (var x = 0; x < data.length; x++) {
        $("#LaPrecio" + (x + 1)).html("<a href='javascript:CalcularValor(" + (x + 1) + "," + data[x].porcentaje + ")'>" + data[x].precio + "</a>");
        $("#Precio" + (x + 1)).prop("disabled", false);
    }
    for (var x = (data.length+1); x <=10; x++) {
        $("#Precio" + x).prop("disabled", true);
        $("#Precio" + x).val("0");
    }
}

function CalcularValor(pos, porcentaje) {

    if (IdArticulo == 0)
        return false;

    var valoriva = LlamarAjax("Inventarios","opcion=ValorIva&Iva=" + $("#Iva").val());

    $("#Cos").val($("#UltimoCosto").val());
    $("#Por").val(formato_numero(porcentaje, _DE,_CD,_CM,""));
    $("#Pos").val(pos);
    $("#IVA").val(valoriva);
    $("#PreVen").val($("#Precio" + pos).val());
    precio = (NumeroDecimal($("#UltimoCosto").val()) * porcentaje / 100) + NumeroDecimal($("#UltimoCosto").val());
    $("#PreBas").val(formato_numero(precio, 3, ".", ",", ""));
    $("#TitCalPrecio").html($("#Ta_PLaPrecio" + pos).html());
    CalcularResultado("1", 1);
    $("#modalCalPrecio").modal("show");
}

function CalcularResultado(Caja, tipo) {
    if (Caja != "1")
        ValidarTexto(Caja, 1);
    var iva = NumeroDecimal($("#Iva").val());

    var pos = NumeroDecimal($("#Pos").val());
    var costo = NumeroDecimal($("#Cos").val());
    switch (tipo) {
        case 1:
            var valor = NumeroDecimal($("#PreBas").val());
            var porcentaje = (valor * 100 / costo) - 100;
            $("#Por").val(formato_numero(porcentaje, _DE,_CD,_CM,""));
            var precio = valor * (1 + (iva / 100));
            precio = Math.round(precio / 100, 0) * 100;
            $("#PreVen").val(formato_numero(precio, 0,_CD,_CM,""));

            break;
        case 2:
            var valor = NumeroDecimal($("#PreVen").val());
            var precio = valor / (1 + (iva / 100));
            $("#PreBas").val(formato_numero(precio, 3, ".", ",", ""));
            var porcentaje = (precio * 100 / costo) - 100;
            $("#Por").val(formato_numero(porcentaje, _DE,_CD,_CM,""));
            break;
        case 3:
            var porcentaje = NumeroDecimal($("#Por").val());
            var precio = (costo * porcentaje / 100) + costo;
            $("#PreBas").val(formato_numero(precio, 3, ".", ",", ""));
            var precioventa = precio * (1 + (iva / 100));
            precioventa = Math.round(precioventa / 100, 0) * 100;
            $("#PreVen").val(formato_numero(precioventa, 0,_CD,_CM,""));
            break;
    }
}

function AgregarPrecio() {
    var pos = $("#Pos").val();
    var porcentaje = NumeroDecimal($("#Por").val());
    if (porcentaje <= 0) {
        $("#PreVen").focus();
        swal("Acción Cancelada", "El precio base no puede ser menor al costo del producto", "warning");
        return false;
    }
    $("#Precio" + pos).val($("#PreBas").val());
    $("#modalCalPrecio").modal("hide");
}


DescripcionPrecio();

function CargarEquipos(grupo) {
    $("#Equipo").html(CargarCombo(48, 1, "", grupo));
    if (grupo * 1 == 0) {
        $("#CodigoInterno").val("");
        return false;
    } 
    else
        $("#CodigoInterno").val(LlamarAjax("Inventarios","opcion=CodigoInterno&Grupo=" + grupo));
    
    if (IdArticulo == 0) {
        var cuenta = $("#CuentaContable").val() * 1;
        if (cuenta == 0) {
            cuenta = LlamarAjax("Inventarios","opcion=CuentaContable_Grupo&grupo=" + grupo) * 1;
            if (cuenta > 0)
                $("#CuentaContable").val(cuenta).trigger("change");
        }
    }

}

function CalcularPrecio() {
    var datos = LlamarAjax("Inventarios","opcion=DescripcionPrecio&");
    var iva = NumeroDecimal($("#Iva").val());
    var costo = NumeroDecimal($("#UltimoCosto").val());
    var data = JSON.parse(datos);
    var resultado = "";
    for (var x = 0; x < data.length; x++) {
        porcentaje = data[x].porcentaje;
        pos = x + 1;
        precio = (costo * porcentaje / 100) + costo;
        precioventa = precio * (1 + (iva / 100));
        precioventa = Math.round(precioventa / 100, 0) * 100;
        $("#Precio" + pos).val(formato_numero(precioventa, 0,_CD,_CM,""));
    }
}

$('select').select2();
DesactivarLoad();


