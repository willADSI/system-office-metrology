﻿$("#TEquipo").html(CargarCombo(67, 1));
$("#TGrupo, #CGrupo").html(CargarCombo(47, 1));
$("#CEquipo").html(CargarCombo(48, 1, "", "-1"));
$("#CModelo").html(CargarCombo(50, 1, "", "-1"));
$("#CMarca").html(CargarCombo(49, 1));
$("#CPresentacion").html(CargarCombo(66, 1));

$("#Ubicacion").html(CargarCombo(68, 1));
$("#Responsable").html(CargarCombo(13, 1));

var OpcionEquipo = 0;
var CodigoToma = "";
var ErrorEnter = 0;
var UltimoCodigo = "";
var TablaTras = null;
var Traslado = 0;
var IdTraslado = 0;

d = new Date();
month = d.getMonth() + 1;
day = d.getDate();

var Anio = $.trim(d.getFullYear());
Anio = $.trim(Anio.substr(2, 2))
var AprobarCotiza = "";

output = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-01';

output2 = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-' +
    (day < 10 ? '0' : '') + day;

$("#CFechaDesde").val(output);
$("#CFechaHasta").val(output2);

function ActivarBuscarTras() {
        
    var opcion = $("#SpanBuscar").html();
    if (opcion == "Buscar") {
        $("#BuscarArticulo").removeClass("hidden");
        $("#SpanBuscar").html("Ocultar")
    } else {
        $("#BuscarArticulo").addClass("hidden");
        $("#SpanBuscar").html("Buscar")
    }
}

function BuscarComboCodigoTras(opcion) {
    var grupo = $("#TGrupo").val() * 1;
    var equipo = $("#TEquipo").val() * 1;
    var marca = $("#TMarca").val() * 1;
    var modelo = $("#TModelo").val() * 1;
    var presentacion = $.trim($("#TPresentacion").val());
    

    var datos = LlamarAjax("Inventarios/CodigoTomaFisica", "opcion=" + opcion + "&grupo=" + grupo + "&equipo=" + equipo + "&marca=" + marca + "&modelo=" + modelo + "&presentacion=" + presentacion);
    switch (opcion) {
        case 1:
            if (grupo == 0) {
                $("#TEquipo").html("");
                $("#TPresentacion").html("");
                $("#TMarca").html("");
                $("#TModelo").html("");
                $("#TCodigo").html("");
                $("#TEquipo").html(CargarCombo(67, 1));
                return false;
            }
            if (OpcionEquipo == 0) {
                if (datos == "XX")
                    datos = "";
                $("#TEquipo").html(datos);
                $("#TEquipo").trigger("change");
            }
            break;
        case 2:

            if (equipo == 0) {
                $("#TPresentacion").html("");
                $("#TMarca").html("");
                $("#TModelo").html("");
                $("#TCodigo").html("");
                return false;
            }

            datos = datos.split("||");
            var tgrupo = datos[1] * 1;
            if (tgrupo != $("#TGrupo").val() * 1) {
                OpcionEquipo = 1;
                $("#TGrupo").val(datos).trigger("change");
            }
            OpcionEquipo = 0;
            if (OpcionEquipo == 0) {
                if (datos == "XX")
                    datos = "";
                $("#TPresentacion").html(datos[0]);
                $("#TPresentacion").trigger("change");
            }
            break;
        case 3:
            if (datos == "XX")
                datos = "";
            $("#TMarca").html(datos);
            $("#TMarca").trigger("change");
            break;
        case 4:
            if (datos == "XX")
                datos = "";
            $("#TModelo").html(datos);
            $("#TModelo").trigger("change");
            break;
        case 5:
            if (datos == "XX")
                datos = "";
            $("#TCodigo").html(datos);
            $("#TCodigo").trigger("change");
            break;
    }
}

function SeleccionarCodigoTras(codigo) {
    LimpiarDatos();
    if (codigo == "")
        return false;
    $("#Codigo").val(codigo);
    BuscarCodigoTras(codigo, "", 1); 
}

function BuscarCodigoTras(codigo, code, tipo) {
    codigo = $.trim(codigo);
    if (codigo != CodigoToma && $("#CodigoIntB").html() != "")
        LimpiarDatos();
    if ((code.keyCode == 13 || tipo == 1) && ErrorEnter == 0 && codigo != "") {
        var datos = LlamarAjax("Inventarios/BuscarArticulosToma", "codigo=" + codigo);
        if (datos == "[]") {
            ErrorEnter = 1;
            $("#Codigo").focus();
            swal("Acción Cancelada", "El código " + codigo + " no se encuentra registrado en el inventario", "warning");
            return false;
        } 
        var data = JSON.parse(datos);
        if (data[0].estado * 1 == 0) {
            ErrorEnter = 1;
            $("#Codigo").focus();
            swal("Acción Cancelada", "El código " + codigo + " se encuentra inactivo en el inventario", "warning");
            return false;
        }

        if (codigo == data[0].codigointerno)
            CodigoToma = data[0].codigointerno;
        else
            CodigoToma = data[0].codigobarras;

        $("#TTipo").val(data[0].tipo);
        $("#CodigoIntB").html(data[0].codigointerno);
        $("#Existencia").val(data[0].existencia);
        $("#TipoB").html(data[0].tipo);
        $("#CodigoBarB").html(data[0].codigobarras);
        $("#GrupoB").html(data[0].grupo);
        $("#EquipoB").html(data[0].equipo);
        $("#PresentacionB").html(data[0].presentacion);
        $("#MarcaB").html(data[0].marca);
        $("#ModeloB").html(data[0].modelo);
        $("#IdArticuloB").val(data[0].id);
        $("#CostoB").html(formato_numero(data[0].ultimocosto,0,".",",",""));

        $("#Traslado").val("1");
        $("#Traslado").select();
        $("#Traslado").focus();
    } else
        ErrorEnter = 0;

}



function LimpiarDatos() {
    $("#Existencia").val("");
    $("#CodigoIntB").html("");
    $("#TipoB").html("");
    $("#CodigoBarB").html("");
    $("#GrupoB").html("");
    $("#EquipoB").html("");
    $("#PresentacionB").html("");
    $("#MarcaB").html("");
    $("#ModeloB").html("");
    $("#IdArticuloB").val("0");
    $("#CostoB").html("");
}

function ActivarGuardarTras(cantidad, code) {
    var id = $("#IdArticuloB").val() * 1;
    if (cantidad * 1 <= 0 || id == 0)
        return false;
    if (code.keyCode == 13 && ErrorEnter == 0)
        AgregarDetTraslado();
    else
        ErrorEnter = 0
}

function AgregarDetTraslado() {

    var ubicacion = $("#Ubicacion").val() * 1;
    var responsable = $("#Responsable").val() * 1;

    if (ubicacion == 0 || responsable == 0) {
        swal("Acción Cancelada", "Debe de seleccionar una ubicación y responsable", "warning");
        ErrorEnter = 1;
        return false;
    }
    
    var id = $("#IdArticuloB").val() * 1;
    var cantidad = $("#Traslado").val() * 1;
    var toma = $("#numero").val() * 1;
    var codigo = $("#CodigoIntB").html();
    var costo = NumeroDecimal($("#CostoB").html());
    var descripcion = '<b>Tipo:</b> ' + $("#TipoB").html() + ', <b>Grupo:</b> ' + $("#GrupoB").html() + ', <b>Equipo:</b> ' + $("#EquipoB").html() +  ', <b>Presentación:</b> ' + $("#PresentacionB").html() +
        '<b>, Marca:</b> ' + $("#MarcaB").html() + ', <b>Modelo:</b> ' + $("#ModeloB").html();
        if (id == 0)
        return false;

    if (cantidad * 1 <= 0) {
        $("#TFisica1").focus();
        swal("Acción Cancelada", "Debe de ingresar una cantidad mayor a cero", "warning");
        ErrorEnter = 1;
        return false;
    }

    if (UltimoCodigo == CodigoToma) {
        $("#Codigo").focus();
        swal("Acción Cancelada", "Este código ya se ingresó en este traslado", "warning");
        ErrorEnter = 1;
        return false;
    }

    var parametros = "articulo=" + id + "&cantidad=" + cantidad + "&ubicacion=" + ubicacion + "&responsable=" + responsable; 
    var datos = LlamarAjax("Inventarios/GuardarTrasladoDetalle", parametros).split("|");
    if (datos[0] == "0") {

        var fila = TablaTras.data().count() + 1;
        var idtraslado = datos[2];
                
        d = new Date();
        month = d.getMonth() + 1;
        day = d.getDate();
        var horas = d.getHours();
        var minutos = d.getMinutes();
                        
        fecha = d.getFullYear() + '/' +
            (month < 10 ? '0' : '') + month + '/' +
            (day < 10 ? '0' : '') + day + " " + (horas < 10 ? '0' : '') + horas + ":" + (minutos < 10 ? '0' : '') + minutos;

        TablaTras.row.add({
            "fila": fila,
            "codigointerno": codigo,
            "descripcion": descripcion,
            "cantidad": cantidad,
            "valor": costo*cantidad,
            "fecha": fecha,
            "eliminar": "<button class='btn btn-glow btn-danger' title='Eliminar Toma Traslado' type='button' onclick=\"EliminarTraslado(" + idtraslado + ",'" + codigo + "')\"><span data-icon='&#xe0d8;'></span></button>"
        }).draw();
                
        $("#Codigo").val("");
        $("#Codigo").focus();
        ErrorEnter = 0;
        swal("", datos[1], "success");
        UltimoCodigo = CodigoToma;
        LimpiarDatos();
        CodigoToma = "";
    } else {
        ErrorEnter = 1;
        swal("Acción Cancelada", datos[1], "warning");
    }
 
}

function InicializarTraslado() {
    var mensaje = "";
    var ubicacion = $("#Ubicacion").val() * 1;
    var responsable = $("#Responsable").val() * 1;
    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: ValidarTraduccion('¿Seguro que desea inicializar el traslado?... Recuerde que perderá todos los datos'),
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Inicializar'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        allowOutsideClick: false,

        preConfirm: function (result) {

            return new Promise(function (resolve, reject) {
                $.post("Inventarios/InicializarTraslado", "ubicacion=" + ubicacion + "&responsable=" + responsable)
                    .done(function (data) {
                        data = data.split("|");
                        if (data[0] == "0") {
                            mensaje = data[1];
                            UltimoCodigo = "";
                            resolve();
                        } else
                            reject(data[1]);
                    })
            })
        }
    }]).then(function (data) {
        TablaTraslado();
        swal("", mensaje, "success");;
    });
}

function EliminarTraslado(articulo, codigo) {
    var mensaje = "";
    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: ValidarTraduccion('¿Seguro que desea eliminar el ítem de traslado del código ' + codigo + '?'),
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Eliminar'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        allowOutsideClick: false,

        preConfirm: function (result) {

            return new Promise(function (resolve, reject) {
                $.post("Inventarios/EliminarTraslado", "detalle=" + articulo)
                    .done(function (data) {
                        data = data.split("|");
                        if (data[0] == "0") {
                            mensaje = data[1];
                            resolve()
                        } else
                            reject(data[1]);
                    })
            })
        }
    }]).then(function (data) {
        TablaTraslado();
        swal("", mensaje, "success");;
    });
}

function TablaTraslado() {
    var ubicacion = $("#Ubicacion").val() * 1;
    var responsable = $("#Responsable").val() * 1;

    if (responsable == 0 || ubicacion == 0) {
        if (TablaTras != null)
            TablaTras.clear().draw();
        return false;
    }

    ActivarLoad();
    setTimeout(function () {
        var datos = LlamarAjax("Inventarios/TablaTraslado", "Ubicacion=" + ubicacion + "&responsable=" + responsable + "&traslado=" + IdTraslado);
        DesactivarLoad();

        var datajson = JSON.parse(datos);
        TablaTras = $('#TablaTraslado').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "fila" },
                { "data": "codigointerno" },
                { "data": "descripcion" },
                { "data": "cantidad", "className": "text-center text-XX", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "valor", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "fecha" },
                { "data": "eliminar" }
            ],

            "order": [[0, 'desc']],

            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy', 'colvis'
            ]
        });
                
    }, 15);
}

function GuardarTraslado() {

    if (Traslado > 0)
        return false;

    var mensaje = "";
    var ubicacion = $("#Ubicacion").val() * 1;
    var responsable = $("#Responsable").val() * 1;
    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: ValidarTraduccion('¿Seguro que desea cerrar este traslado?'),
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Cerrar'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        allowOutsideClick: false,

        preConfirm: function (result) {

            return new Promise(function (resolve, reject) {
                $.post("Inventarios/GuardarTraslado", "ubicacion=" + ubicacion + "&responsable=" + responsable)
                    .done(function (data) {
                        data = data.split("|");
                        if (data[0] == "0") {
                            mensaje = data[1] + " " + data[2];
                            Traslado = data[2]*1;
                            IdTraslado = data[3] * 1;
                            $("#NumTraslado").val(Traslado);
                            resolve();
                        } else
                            reject(data[1]);
                    })
            })
        }
    }]).then(function (data) {
        $("#Ubicacion").val("").trigger("change");
        $("#Responsable").val("").trigger("change");
        swal("", mensaje, "success");;
    });
}

$('select').select2();
DesactivarLoad();
