﻿d = new Date();
month = d.getMonth() + 1;
day = d.getDate();

var output = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-' +
    (day < 10 ? '0' : '') + day;

var Anio = $.trim(d.getFullYear());
Anio = $.trim(Anio.substr(2, 2))

var Anio = $.trim(d.getFullYear());

var opciones = "<option value=''>--Seleccione--</option>";
for (var x = Anio; x >= 2018; x--) {
    opciones += "<option value='" + x + "'>" + x + "</option>";
}
$("#MAnio").html(opciones);

var Guia = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-' +
    (day < 10 ? '0' : '') + day + '-' + d.getHours() + '-' + d.getMinutes() + '-' + d.getSeconds();


document.onkeydown = function (e) {
    tecla = (document.all) ? e.keyCode : e.which;

    switch (tecla) {
        case 112: //F1
            ModalProveedores();
            return false;
            break;
        case 113: //F9
            CargarModaltarjeta();
            return false;
            break;
        case 114: //F10
            CargarModalCheque();
            return false;
            break;
        case 115: //F11
            CargarModalConsig()
            return false;
            break;
        case 117: //F12
            LimpiarPagos();
            return false;
            break;
        case 118: //f2
            LimpiarTodo();
            return false;
            break;
        case 119: //f10
            ModalRecibo();
            return false;
            break;
        case 120: //f3
            GuardarRecibo();
            return false;
            break;
        case 121: //f4
            EnviarRecibo();
            return false;
            break;
        case 122: //f6
            ImprimirRecibo(1);
            return false;
            break;
        case 123: //f8
            AnularRecibo();
            return false;
            break;
    }
    
}

$("#ChFecha").val(output);
$("#ttipo").html(CargarCombo(23, 1));
$("#con_cuenta, #MCuenta").html(CargarCombo(25, 1));

$("#ChBanco").html(CargarCombo(24, 1));

$("#ttipo").val("");

function LlamarModalMovimientosCP() {
    if (Abonado > 0 || TotalSeleccionado > 0 || NombreProveedor == "")
        return false;
    CargarModalMovimientosCP();
    $("#ModalMovBancarioCP").modal("show");
}

function CargarModalMovimientosCP() {

    var cuenta = $("#MCuenta").val()*1;
    var anio = $("#MAnio").val()*1;
    var mes = $("#MMes").val()*1;

    var datos = LlamarAjax("Caja","opcion=TablaMovimientos&tipo=2&cuenta=" + cuenta + "&anio=" + anio + "&mes=" + mes);
    var datajson = JSON.parse(datos);
    var tabla = $('#MTablaMovimientoCP').DataTable({
        data: datajson,
        bProcessing: true,
        bDestroy: true,
        paging: true,
        ordering: true,
        info: false,
        searching: true,
        columns: [
            { "data": "id" },
            { "data": "fecha" },
            { "data": "referencia" },
            { "data": "descripcion" },
            { "data": "oficina" },
            { "data": "salida", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') }
        ],

        "language": {
            "url": LenguajeDataTable
        }

    });
}

var IdProveedor = 0;
var Almacen = localStorage.getItem("almacen") * 1;
var Pro_Correo = "";
var ProReteIva = 0;
var ProReteIca = 0;
var ProReteFuente = 0;
var NombreProveedor = "";
var TotalCompra = 0;
var Recaudado = 0;
var SaldoFac = 0;
var SaldoAbo = 0;
var Abonado = 0;
var RetIVA = 0;
var RetCREE = 0;
var RetFuente = 0;
var RetICA = 0;
var Descuento = 0;
var OIngreso = 0;
var OEgreso = 0;
var TotalSeleccionado = 0;
var TotalSelSubtotal = 0;
var TotalSelecIva = 0;
var NCPago = 0;
var Efectivo = 0;
var Tarjeta = 0;
var Cheque = 0;
var Consignado = 0;
var Anticipo = 0;
var Compras = "";
var CantCheque = 0;
var TotalCheques = 0;
var EditTotalCheques = 0;
var DocumentoPro = "";
var Movimiento = 0;
var ProveedorAnticipo = 0;

var PlazoPago = "0";
var TipoProveedor = "";
var CorreoProveedor = "";

var ErrorPro = 0;
var c_Efectivo = document.getElementById('Efectivo');
var c_Tarjeta = document.getElementById('Tarjeta');
var CantCheque = document.getElementById('ChCantidad');
var c_Cheque = document.getElementById('Cheque');
var c_VRecaudado = document.getElementById('VRecaudado');
var c_Consignado = document.getElementById('Consignacion');

var c_RetIVA = document.getElementById('RetIVA');
var c_RetCREE = document.getElementById('RetCREE');
var c_RetFuente = document.getElementById('RetFuente');
var c_RetICA = document.getElementById('RetICA');
var c_Descuento = document.getElementById('Descuento');

var c_OIngreso = document.getElementById('OIngreso');
var c_OEgreso = document.getElementById('OEgreso');
var Diferencia = 0;


function chCambioPersona(tipo) {
    if (tipo == "N") {
        $("#ChNombre").val("");
        $("#ChCedula").val("");
        $("#ChTelefono").val("");
        $("#ChDireccion").val("");
    } else {
        $("#ChNombre").val($("#ChNombre").prop("inicial"));
        $("#ChCedula").val($("#ChCedula").prop("inicial"));
        $("#ChTelefono").val($("#ChTelefono").prop("inicial"));
        $("#ChDireccion").val($("#ChDireccion").prop("inicial"));
    }
}



function AnularRecibo() {

    if (NCPago == 0)
        return false;

    var resultado = "";
    swal({
        title: 'Advertencia',
        text: '¿' + ValidarTraduccion('Desea Anular el Recibo') + ' ' + NCPago + ' ' + ValidarTraduccion('del Proveedor') + ' ' + NombreProveedor,
        input: 'text',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Anular'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',

        inputValidator: function (value) {
            return new Promise(function (resolve, reject) {
                if (value) {
                    var datos = LlamarAjax("Caja","opcion=AnularRecibo&recibo=" + NCPago + "&observaciones=" + value)
                    resultado = NCPago;
                    datos = datos.split("|");
                    if (datos[0] == "0") {
                        resolve()
                    } else {
                        reject(datos[1]);
                    }

                } else {
                    reject(ValidarTraduccion('Debe de ingresar una observación'));
                }
            })
        }
    }).then(function (result) {
        Limpiar();
        IdProveedor = 0;
        $("#Proveedor").val("");
        $("#Proveedor").focus();
        swal({
            type: 'success',
            html: 'Recibo Nro ' + resultado + ' anulado con éxito'
        })
    })

}

function LimpiarTarjeta() {
    $("#tnumero").val("").trigger("change");
    $("#Tarjeta").val("");
    $("#ttipo").val("").trigger("change");
    $("#tcodseguridad").val("");
    $("#tfvencimiento").val("");
    $("#tautoriza").val("");
    CalcularTotal();
}

function GuardarCheque(parametros) {
    var cantidad = $("#ChCantidad").val() * 1;
    var valor = NumeroDecimal($("#Cheque").val());
    var autorizado = $.trim($("#ChAutorizado").val()).toUpperCase();
    var autorizadopor = $.trim($("#ChAutorizacion").val()).toUpperCase();

    if (cantidad == 0) {
        $("#ChCantidad").focus()
        swal(ValidarTraduccion(ValidarTraduccion("Acción Cancelada")), "Debe de ingresar la cantidad de cheques", "warning");
        return false;
    }

    if (valor == 0) {
        $("#Cheque").focus()
        swal(ValidarTraduccion(ValidarTraduccion("Acción Cancelada")), "Debe de ingresar el valor en cheques", "warning");
        return false;
    }

    if (noVericarCheque == 0) {
        if ((autorizado.substr(0, 2) != "D-") && (autorizado.substring(1, 2) != "C-") && (autorizado.substring(1, 2) != "F-")) {
            $("#ChAutorizado").select();
            $("#ChAutorizado").focus();
            swal(ValidarTraduccion(ValidarTraduccion("Acción Cancelada")), "D-(nombre autorizador): para Datacheque <br> C- (nombre autorizador): para Covinoc <br> F - (nombre autorizador): para Fenalcheque", "warning");
            return false;
        }

        if (autorizado == "" || autorizadopor == "") {

            if (autorizado == "") {
                $("#ChAutorizado").select();
                $("#ChAutorizado").focus();
            } else {
                $("#ChAutorizacion").select();
                $("#ChAutorizacion").focus();
            }
            swal(ValidarTraduccion(ValidarTraduccion("Acción Cancelada")), "Debe de completar la autorización del cheque", "warning");
            return false;
        }
    }

    parametros += "&Guia=" + Guia + "&Valor=" + valor;

    var datos = LlamarAjax("Caja","opcion=GuardarCheque&"+ parametros);
    datos = datos.split("|");
    if (datos[0] == "0") {
        swal("", datos[1], "success");
        CargarTablaCheque();
        LimpiarCheque();
        
    } else {
        swal("", datos[1], "error");
    }
}

function EliminarTemporalCheque(id,numero) {

    swal({
        title: '¿Eliminar el cheque número ' + numero + '?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Sí',
        cancelButtonText: 'No'

    }).then(function () {

        var parametros = "id=" + id+"&guia=";
        var datos = LlamarAjax("Caja","opcion=EliminarCheque&"+ parametros);
        datos = datos.split("|");
        if (datos[0] == "0") {
            CargarTablaCheque();
            LimpiarCheque();
        } else {
            $.jGrowl(ValidarTraduccion(datos[1]), { life: 6000, theme: 'growl-warning', header: '' });
        }

    }, function (dismiss) {
        // dismiss can be 'cancel', 'overlay',
        // 'close', and 'timer'
        if (dismiss === 'cancel') {
        }
    })

}

function EditarTemporalCheque(id) {
    var datos = LlamarAjax("Caja","opcion=TablaCheque&id=" + id);
    eval("data=" + datos);

    $("#id_cheque").val(data[0].id);
    $("#ChAutorizado").val(data[0].AutorizadoPor)
    $("#ChAutorizacion").val(data[0].Autoriza)
    EditTotalCheques = data[0].Valor * 1;
    $("#Cheque").val(formato_numero(data[0].Valor, "0", ".", ",", ""));
    $("#ChCantidad").val("1");
    if (data[0].chqEstado == "F") {
        $("#ChTipoC_B").attr("checked", false);
        $("#ChTipoC_C").attr("checked", false);
    } else {
        if (chqEstado == "P") 
            $("#ChTipoC_B").attr("checked", true);
        else
            $("#ChTipoC_B").attr("checked", false);
    }
        
    $("#ChPersona_J").attr("checked", true);

    $("#ChNombre").val(data[0].Girador);
    $("#ChCedula").val(data[0].Cedula);
    $("#ChTelefono").val(data[0].Telefono);
    $("#ChDireccion").val(data[0].Direccion);
    $("#ChFecha").val(data[0].chqFechaCheque.substr(0, 10));

    $("#ChCuenta").val(data[0].Cuenta);
    $("#ChNumero").val(data[0].Numero);
    $("#ChBanco").val(data[0].chqCodBanco).trigger('change');
    $("#ChPlaza").val(data[0].chqCodPlaza).trigger('change');
    
}

function CargarTablaCheque() {
    var datos = LlamarAjax("Caja","opcion=TablaCheque&Guia=" + Guia);
    CargarTablaJsonTitulo(datos, "tablacheque", "divtabcheque");
    CalcularTotal('No');
}



function LimpiarPagos() {

    if (NCPago > 0)
        return false;

    LimpiarTarjeta();
    LimpiarConsignacion();
    var parametros = "id=0" + "&guia=" + Guia;
    //var datos = LlamarAjax("Caja/EliminarCheque", parametros);
    CargarTablaCheque();
    LimpiarCheque();
    $("#Efectivo").val("0")
    
    Guia = Guia = d.getFullYear() + '-' +
        (month < 10 ? '0' : '') + month + '-' +
        (day < 10 ? '0' : '') + day + '-' + d.getHours() + '-' + d.getMinutes() + '-' + d.getSeconds();
    CalcularTotal();

}

function LimpiarCheque() {
    chCambioPersona('J');
    $("#ChAutorizado").val("")
    $("#ChAutorizacion").val("")
    $("#Cheque").val("0");
    EditTotalCheques = 0;
    CalcularTotal('No');
    $("#Cheque").val(formato_numero(SaldoAbo, "0", ".", ",", ""));
    $("#ChCantidad").val("1");
    CalcularTotal('No');
    $("#ChTipoC_B").attr("checked", false);
    $("#ChTipoC_C").attr("checked", false);

    $("#ChPersona_J").attr("checked", true);
    chCambioPersona('J');
    
    $("#ChCuenta").val("");
    $("#ChNumero").val("");

    $("#ChBanco").val("").trigger('change');
    $("#ChPlaza").val("").trigger('change');
    $("#id_cheque").val("0")

}

$("#formCheques").submit(function (e) {

    e.preventDefault();
    var Parametros = $("#formCheques").serialize() 
    GuardarCheque(Parametros);

})

function LimpiarTodo() {
    Limpiar();
    $("#Proveedor").val("");
    IdProveedor = "";
}

function Limpiar() {
    $("#NombreProveedor").html("");
    NCPago = 0;
    NombreProveedor = "";
    DocumentoPro = "";
    Movimiento = 0;
    Diferencia = 0;
    Anticipo = 0;
    ProveedorAnticipo = 0;
    $("#Detalle_MovimientoComp").html("");
    $("#divAnulaciones").addClass("hidden");
    $("#btnImprimir").addClass("hidden");
    $("#btnImprimirmov").addClass("hidden");
    $("#btnEnviar").addClass("hidden");
    $("#btnAnular").addClass("hidden");
    $("#btnGuardar").removeClass("hidden");
    $("#SDiferencia").val("");
    EditTotalCheques = 0;
    TotalSeleccionado = 0;
    TotalSelSubtotal = 0;
    TotalSelecIva = 0;
    $("#SaldoProveedorDev").html("");

    LimpiarConsignacion();
    $("#chTotales").html("0");
    $("#divtabcheque").html("");
    LimpiarCheque();
        
    EditTotalCheques = 0;
    TotalSeleccionado = 0;

    PlazoPago = "0";
    TipoProveedor = "";
    CorreoProveedor = "";
    
    NombreProveedor = "";
    Guia = Guia = d.getFullYear() + '-' +
        (month < 10 ? '0' : '') + month + '-' +
        (day < 10 ? '0' : '') + day + '-' + d.getHours() + '-' + d.getMinutes() + '-' + d.getSeconds();

    $("#tnumero").val("");
    $("#tvalor").val("");
    $("#ttipo").val("");
    $("#tcodseguridad").val("");
    $("#tfvencimiento").val("");
    $("#tautoriza").val();

    $("#SSubTotal").val("");
    $("#SIVA").val("");

    Compras = "";

    $("#RetIVA").val("0");
    $("#Anticipo").val("0");
    $("#RetCREE").val("0");
    $("#RetICA").val("0");
    $("#RetFuente").val("0");
    $("#TTarjeta").val("0");
    $("#Tarjeta").val("0");
    $("#Efectivo").val("0");
    $("#Consignacion").val("0");
    $("#Cheque").val("0");
    $("#TConsignacion").val("0");
    $("#TCheque").val("0");
    $("#Descuento").val("0");
    $("#OIngreso").val("0");
    $("#OEgreso").val("0");


    $("#RetIVA").prop("disabled", false);
    $("#RetCREE").prop("disabled", false);
    $("#RetICA").prop("disabled", false);
    $("#RetFuente").prop("disabled", false);
    $("#Efectivo").prop("disabled", false);
    $("#Descuento").prop("disabled", false);
    $("#OEgreso").prop("disabled", false);
    $("#VRecaudado").prop("disabled", false);
    $("#Concepto").prop("disabled", false);
    $("#Observacion").val("");
    
    $("#TCompra").val("0");
    $("#SCompra").val("0");
    $("#VRecibido").val("0");
    $("#SPagar").val("0");
    $("#VRecaudado").val("0");
    $("#Concepto").val("CANCELACIÓN DE FACTURA DE COMPRA");

    $("#bodycompra").html("");
    $("#Recibo").html("0");
    $("#NombreProveedor").html("");
    $("#STotal").html("0");

    $("#ObsAnulacion").html("");

    RetIVA = 0;
    RetCREE = 0;
    RetFuente = 0;
    RetICA = 0;
    Descuento = 0;

    OIngreso = 0;
    OEgreso = 0;
        
    Efectivo = 0;
    Tarjeta = 0;
    Cheque = 0;
    Consignado = 0;
    TotalCheques = 0;
    Abonado = 0;
    Recaudado = 0;

    SaldoFac = 0;
    SaldoAbo = 0;
    
    Abonado = 0;
    
}

$("#Proveedor").bind('keypress', function (e) {
    if (e.keyCode == 13) {
        $("#Proveedor").blur();
    }
})


function SeleccionarFilaCP(pos, valor, numfac, subtotal, iva) {
    if (NCPago > 0)
        return false
    if (Recaudado == 0) {
        $.jGrowl(ValidarTraduccion("Debe ingresar primero lo recaudado"), { life: 4000, theme: 'growl-warning', header: '' });
        return false;
    }
        
    if ($("#selecc" + pos).val() == "0") {
        //if (TotalSeleccionado > (Recaudado + RetIVA + RetCREE + RetFuente + RetICA + Descuento + OEgreso - OIngreso))
        //    return false;
        $("#tr-" + pos).addClass("bg-success");
        $("#selecc" + pos).val("1");
        TotalSeleccionado += valor;
        TotalSelSubtotal += subtotal;
        TotalSelecIva += iva;
        if (Compras == "")
            Compras = numfac
        else
            Compras += "," + numfac;

    } else {
        $("#tr-" + pos).removeClass("bg-success");
        $("#selecc" + pos).val("0");
        TotalSeleccionado -= valor;
        TotalSelSubtotal -= subtotal;
        TotalSelecIva -= iva;
                        
        if ($.trim(Compras).indexOf(",") >= 0) {
            arrego = Compras.split(",");
            Compras = "";
            for (var x = 0; x < arrego.length; x++) {
                if (arrego[x] * 1 != numfac * 1) {
                    if (Compras == "")
                        Compras = arrego[x]
                    else
                        Compras += "," + arrego[x];
                }
            }
        } else
            Compras = "";
    }

    RetIVA = Math.trunc(TotalSelecIva * ProReteIva / 100);
    RetICA = Math.trunc(TotalSelSubtotal * ProReteIca / 100);
    RetFuente = Math.trunc(TotalSelSubtotal * ProReteFuente / 100);
        
    $("#STotal").html(formato_numero(TotalSeleccionado, 0, ".", ","));
    $("#SSubTotal").html(formato_numero(TotalSelSubtotal, 0, ".", ","));
    $("#SIVA").html(formato_numero(TotalSelecIva, 0, ".", ","));

    $("#RetIVA").val(formato_numero(RetIVA, 0, ".", ","));
    $("#RetICA").val(formato_numero(RetICA, 0, ".", ","));
    $("#RetFuente").val(formato_numero(RetFuente, 0, ".", ","));

    CalcularTotal('No');
}

function TablaComprasCPago() {

    var parametros = "cpago=" + NCPago;
    var datos = LlamarAjax('Caja','opcion=BuscarComprobante&'+ parametros);
    var TotalCompra = 0;
    var resultado = "";
    
    if (datos != "[]") {
        eval("data=" + datos);
        $("#Concepto").val(data[0].concepto);
        $("#Observacion").val(data[0].observacion);
        $("#VRecibido").val(formato_numero(data[0].recaudado, 0, ".", ","));
        $("#VRecaudado").val(formato_numero(data[0].recaudado, 0, ".", ","));
        $("#Detalle_MovimientoComp").html(data[0].movimiento + formato_numero(data[0].monto,0,".",",",""));
        
        if (data[0].fechaanul) {
            $("#divAnulaciones").removeClass("hidden");
            $("#ObsAnulacion").html("Usuario que Anuló: " + data[0].usuarioanula + ", Fecha: " + data[0].fechaanul + ", Observación: " + data[0].observaanula);
            $("#btnAnular").addClass("hidden");
        } else {
            $("#btnAnular").removeClass("hidden");
            $("#divAnulaciones").addClass("hidden");
        }
        for (var x = 0; x < data.length; x++) {
            resultado += "<tr>" +
                "<td>" + (x + 1) + "</td>" +
                "<td>" + data[x].tipo + "-" + data[x].compra + "</td>" +
                "<td>" + data[x].factura + "</td>" +
                "<td>" + data[x].fecha + "</td>" +
                "<td>" + data[x].vencimiento + "</td>" +
                "<td align='right'>" + formato_numero(data[x].subtotal, 0, ".", ",") + "</td>" +
                "<td align='right'>" + formato_numero(data[x].descuentofac, 0, ".", ",") + "</td>" + 
                "<td align='right'>" + formato_numero(data[x].iva, 0, ".", ",") + "</td>" + 
                "<td align='right'>" + formato_numero(data[x].total, 0, ".", ",") + "</td>" +
                "<td align='right'>" + formato_numero(data[x].abonado, 0, ".", ",") + "</td>" +
                "<td align='right'>" + formato_numero(data[x].saldo, 0, ".", ",") + "</td>" +
                "<td align='right'>" + formato_numero(data[x].retefuentefac, 0, ".", ",") + "</td>" +
                "<td align='right'>" + formato_numero(data[x].reteivafac, 0, ".", ",") + "</td>" +
                "<td align='right'>" + formato_numero(data[x].reteicafac, 0, ".", ",") + "</td></tr>";
            TotalCompra += data[x].abonado * 1;
            
            if (data[x].efectivo * 1 > 0)
                $("#Efectivo").val(formato_numero(data[x].efectivo, 0, ".", ","));
            if (data[x].tarjeta * 1 > 0)
                $("#TTarjeta").val(formato_numero(data[x].tarjeta, 0, ".", ","));
            if (data[x].cheque * 1 > 0)
                $("#TCheque").val(formato_numero(data[x].cheque, 0, ".", ","));
            if (data[x].consignacion * 1 > 0)
                $("#TConsignacion").val(formato_numero(data[x].consignacion, 0, ".", ","));
            if (data[x].descuento * 1 > 0)
                $("#Descuento").val(formato_numero(data[x].descuento, 0, ".", ","));
            if (data[x].retefuente * 1 > 0)
                $("#RetFuente").val(formato_numero(data[x].retefuente, 0, ".", ","));
            if (data[x].reteica * 1 > 0)
                $("#RetICA").val(formato_numero(data[x].reteica, 0, ".", ","));
            if (data[x].reteiva * 1 > 0)
                $("#RetIVA").val(formato_numero(data[x].reteiva, 0, ".", ","));
            if (data[x].retecree * 1 > 0)
                $("#RetCREE").val(formato_numero(data[x].retecree, 0, ".", ","));
            if (data[x].oegreso * 1 > 0)
                $("#OEgreso").val(formato_numero(data[x].oegreso, 0, ".", ","));
            if (data[x].oingreso * 1 > 0)
                $("#OIngreso").val(formato_numero(data[x].oingreso, 0, ".", ","));

        }
    }

    $("#bodycompra").html(resultado);
    $("#TCompra").val(formato_numero(TotalCompra, 0, ".", ","));
    $("#STotal").html(formato_numero(TotalCompra, 0, ".", ","));
}

function ImprimirMovil() {
    if (NCPago > 0) {
        ActivarLoad();
        setTimeout(function () {
            var datos = LlamarAjax("Caja","opcion=GenerarReciboTexto&recibo=" + NCPago);
            DesactivarLoad();
            datos = datos.split("|");
            if (datos[0] == "0") {
                window.open("Documpdf/" + datos[1])
            } else {
                swal(ValidarTraduccion(ValidarTraduccion("Acción Cancelada")), datos[1], "error");
            }
        }, 15);
    }
}

function ImprimirRecibo() {
    if (NCPago > 0) {
        ActivarLoad();
        setTimeout(function () {
            var datos = LlamarAjax("Caja/ImprimirRecibo", "recibo=" + NCPago);
            DesactivarLoad();
            datos = datos.split("|");
            if (datos[0] == "0") {
                window.open("Documpdf/" + datos[1])
            } else {
                swal(ValidarTraduccion(ValidarTraduccion("Acción Cancelada")), datos[1], "error");
            }
        }, 15);
    }
}

function EnviarRecibo() {
    if (NCPago > 0) {
        swal({
            title: 'Correo Electrónico',
            text: NombreProveedor,
            input: 'email',
            showCancelButton: true,
            confirmButtonText: 'Enviar',
            showLoaderOnConfirm: true,
            inputValue: Pro_Correo,
            preConfirm: (email) => {
                return new Promise((resolve, reject) => {

                    setTimeout(() => {

                        var datos = LlamarAjax("Caja","opcion=ImprimirRecibo&recibo=" + NCPago);
                        DesactivarLoad();
                        datos = datos.split("|");
                        var Archivo = "";
                        if (datos[0] == "0") {
                            Archivo = datos[1];
                        } else {
                            reject(datos[1]);
                        }

                        var parametros = "recibo=" + NCPago + "&e_email=" + email + "&archivo=" + Archivo + "&Proveedor=" + NombreProveedor + "&codigo=" + IdProveedor;
                        var datos = LlamarAjax("Caja","opcion=EnvioRecibo&"+ parametros);
                        datos = datos.split("|");
                        if (datos[0] == "0") {
                            resolve()
                        } else {
                            reject(datos[1]);
                        }
                           
                    }, 15);
                    
                })
            },
            allowOutsidecli: () => !swal.isLoading()
        }).then((result) => {
            if (result) {
                
                swal({
                    type: 'success',
                    title: 'Recibo Nro ' + NCPago +  ' Enviado',
                    html: 'Correo Electrónico: ' + result
                })
            }
        })
    }
}

function TablaComprasPendientesCP() {
    var parametros = "proveedor=" + IdProveedor + "&tipo=1";
    var datos = LlamarAjax('Caja','opcion=FacturasPendientesCP&'+ parametros);
    var resultado = "";
    var clase = "";
    var diavencido = "";
    TotalCompra = 0;
    if (datos != "[]") {
        eval("data=" + datos);
        for (var x = 0; x < data.length; x++) {
            clase = ""
            diavencido = "";
            if (data[x].dias * 1 > 0) {
                clase = "class='text-danger text-bold'";
                diavencido = "<br>(" + data[x].dias + ") días";
            }
                
            resultado += "<tr " + clase + " onclick = \"SeleccionarFilaCP(" + x + "," + data[x].saldo + "," + data[x].id + "," + (data[x].subtotal - data[x].descuento) + "," + data[x].iva + ")\" id = 'tr-" + x + "' > " +
                "<td><a class='text-XX' href=\"javascript:ImprimirCompra(" + data[x].compra + ",'" + data[x].tipo + "')\">" + (x + 1) + "</a></td>" +
                "<td>" + data[x].tipo + "-" + data[x].compra + "<input type='hidden' value='" + data[x].id + "' name='TablaCompra[]'</td>" +
                "<td>" + data[x].factura + "</td>" +
                "<td>" + data[x].fecha + "</td>" +
                "<td>" + data[x].vencimiento + diavencido + "</td>" +
                "<td align='right'><div name = TablaValor[]>" + formato_numero(data[x].subtotal, 0, ".", ",") + "</div></td>" + 
                "<td align='right'>" + formato_numero(data[x].descuento, 0, ".", ",") + "</td>" +
                "<td align='right'><div name = TablaValor[]>" + formato_numero(data[x].iva, 0, ".", ",") + "</div></td>" + 
                "<td align='right' class='bg-celeste'><div name = TablaValor[]>" + formato_numero(data[x].total, 0, ".", ",") + "</div></td>" + 
                "<td align='right'>" + formato_numero(data[x].total - data[x].saldo, 0, ".", ",") + "</td>" +
                "<td align='right' class='bg-celeste'><div class='text-XX' name = TablaValor[]>" + formato_numero(data[x].saldo, 0, ".", ",") + "</div><input type='hidden' value='0' name='selecc[]' id='selecc" + x + "'/></td>" + 
                "<td align='right' class='bg-gris'>" + formato_numero(data[x].retefuente, 0, ".", ",") + "</td>" +
                "<td align='right' class='bg-gris'>" + formato_numero(data[x].reteiva, 0, ".", ",") + "</td>" +
                "<td align='right' class='bg-gris'>" + formato_numero(data[x].reteica, 0, ".", ",") + "</td></tr>";
            TotalCompra += data[x].saldo * 1;
        }
    }
    $("#bodycompra").html(resultado);
    $("#TCompra").val(formato_numero(TotalCompra, 0, ".", ","));
    $("#SCompra").val(formato_numero(TotalCompra, 0, ".", ","));
    

    CalcularTotal('No');

}

function ImprimirCompra(compra, tipo) {
    ActivarLoad();
    setTimeout(function () {
        var parametros = "factura=" + factura;
        var datos = LlamarAjax("Facturacion","opcion=RpFactura&"+ parametros);
        DesactivarLoad();
        datos = datos.split("|");
        if (datos[0] == "0") {
            window.open("DocumPDF/" + datos[1]);
        } else {
            swal("", ValidarTraduccion(datos[1]), "warning");
        }
    }, 15);
}

function AgregarRetencio() {
    var tipo = $("#RTipoRetencion").val() * 1;
    switch (tipo) {
        case 1: //IVA
            RetIVA = NumeroDecimal($("#RRetencion").val())
            $("#RetIVA").val($("#RRetencion").val());
            break;
        case 2: //FUENTE
            RetFuente = NumeroDecimal($("#RRetencion").val())
            $("#RetFuente").val($("#RRetencion").val());
            break;
        case 3: //ICA
            RetICA = NumeroDecimal($("#RRetencion").val())
            $("#RetICA").val($("#RRetencion").val());
            break;
    }
    CalcularTotal('No');
    $("#modalRetencion").modal("hide");
}

function CalcularRetencion(Caja) {

    ValidarTexto(Caja, 3);

    var porcentaje = NumeroDecimal($("#RPorcentaje").val());
    var valor = NumeroDecimal($("#RSubTotal").val());
    $("#RRetencion").val(formato_numero(Math.trunc(valor * porcentaje / 100), 0,_CD,_CM,""));
}

function LlamarRetencion(tipo, descripcion) {
    if (TotalSeleccionado > 0) {

        switch (tipo) {
            case 1: //IVA
                $("#RPorProveedor").html(ProReteIva);
                $("#RPorcentaje").val(ProReteIva);
                $("#RSubTotal").val(formato_numero(TotalSelecIva, 0,_CD,_CM,""));
                $("#RRetencion").val(formato_numero(TotalSelecIva * ProReteIva/100, 0,_CD,_CM,""));
                break;
            case 2: //FUENTE
                $("#RPorProveedor").html(ProReteFuente);
                $("#RPorcentaje").val(ProReteFuente);
                $("#RSubTotal").val(formato_numero(TotalSelSubtotal, 0,_CD,_CM,""));
                $("#RRetencion").val(formato_numero(TotalSelSubtotal * ProReteFuente/100, 0,_CD,_CM,""));
                
                break;
            case 3: //ICA
                $("#RPorProveedor").html(ProReteIca);
                $("#RPorcentaje").val(ProReteIca);
                $("#RSubTotal").val(formato_numero(TotalSelSubtotal, 0,_CD,_CM,""));
                $("#RRetencion").val(formato_numero(TotalSelSubtotal * ProReteIca / 100, 0,_CD,_CM,""));
                break;
        }
        $("#RTipoRetencion").val(tipo);
        $("#desretencion").html(descripcion);
        $("#modalRetencion").modal("show");

    }
}


$("#VRecaudado, #Descuento, #OIngreso, #OEgreso, #RetFuente, #RetIVA, #RetCREE, #RetICA").blur(function (e) {
    if (TotalSeleccionado > 0)
        switch (this.id) {
            case "VRecaudado":
                $("#VRecaudado").val(formato_numero(Recaudado, 0, ".", ","));
                break;
            case "Descuento":
                $("#Descuento").val(formato_numero(Descuento, 0, ".", ","));
                break;
            case "OIngreso":
                $("#OIngreso").val(formato_numero(OIngreso, 0, ".", ","));
                break;
            case "OEgreso":
                $("#OEgreso").val(formato_numero(OEgreso, 0, ".", ","));
                break;
            case "RetFuente":
                $("#RetFuente").val(formato_numero(RetFuente, 0, ".", ","));
                break;
            case "RetICA":
                $("#RetICA").val(formato_numero(RetICA, 0, ".", ","));
                break;
            case "RetCREE":
                $("#RetCREE").val(formato_numero(RetCREE, 0, ".", ","));
                break;
            case "RetIVA":
                $("#RetIVA").val(formato_numero(RetIVA, 0, ".", ","));
                break;
        }
        
})

function ValidarTextoCaja(caja, tipo) {
    if ((caja.id == "VRecaudado") && ((Abonado > 0) || (TotalSeleccionado > 0))) {

        $("#VRecaudado").val(formato_numero(Recaudado, 0, ".", ","));
        return false;
    }

        
    ValidarTexto(caja, tipo);
    CalcularTotal(caja);
        
}

function CargarModaltarjeta() {
    if (NCPago > 0)
        return false
    if ($("#NombreProveedor").html() == "")
        return false;
    if (Recaudado > 0)
        $("#ModalTarjeta").modal("show");
    else {
        $("#VRecaudado").focus();
        $.jGrowl(ValidarTraduccion("Debe de ingresar el valor a recaudar"), { life: 4000, theme: 'growl-warning', header: '' });
    }
}

function CargarModalCheque() {
    if (NCPago > 0)
        return false
    if ($("#NombreProveedor").html() == "")
        return false;
    if (Recaudado > 0) {
        LimpiarCheque();
        CargarTablaCheque();
        $("#ModalCheque").modal("show");
    }else {
        $("#VRecaudado").focus();
        $.jGrowl(ValidarTraduccion("Debe de ingresar el valor a recaudar"), { life: 4000, theme: 'growl-warning', header: '' });
    }
}

function CargarVerPagosCP(tipo, forma) {
    
    if (NCPago == 0)
        return false;
    if ((tipo == "C") && (NumeroDecimal($("#TCheque").val()) * 1 == 0))
        return false;
    if ((tipo == "T") && (NumeroDecimal($("#TTarjeta").val()) * 1 == 0))
        return false;
    if ((tipo == "CO") && (NumeroDecimal($("#TConsignacion").val()) * 1 == 0))
        return false;
    if ((tipo == "A") && (NumeroDecimal($("#Anticipo").val()) * 1 == 0))
        return false;

    $("#VerChequeComprobante").html(LlamarAjax("Caja","opcion=FormaPagoReciboCP&cpago=" + NCPago + "&tipo=" + tipo))
    $("#TituloVerPagos").html(forma);
    $("#ModalVerChequeCP").modal("show");
    
}

function CargarModalConsig() {
    if (NCPago > 0)
        return false
    if ($("#NombreProveedor").html() == "")
        return false;
    if (Recaudado == 0) {
        $("#VRecaudado").focus();
        $.jGrowl(ValidarTraduccion("Debe de ingresar el valor a recaudar"), { life: 4000, theme: 'growl-warning', header: '' });
        return false;
    }

    
        
    if (Abonado == 0 || Abonado == Consignado) {
        $("#ModalConsignacion").modal("show");
        if (Consignado == 0)
            Consignado = Recaudado;
            
        $("#Consignacion").val(formato_numero(Consignado, "0", ".", ",", ""))
        CalcularTotal('No');
        $("#ModalConsignacion").modal("show");
        
    } else {
        swal(ValidarTraduccion(ValidarTraduccion("Acción Cancelada")), ValidarTraduccion("La consignación debe de ser por el monto total a recaudar "), "warning")
    }
}

function CalcularTotal(caja) {
    Diferencia = 0;
    RetIVA = NumeroDecimal(c_RetIVA.value) * 1;
    RetCREE = NumeroDecimal(c_RetCREE.value) * 1;
    RetFuente = NumeroDecimal(c_RetFuente.value) * 1;
    RetICA = NumeroDecimal(c_RetICA.value) * 1;
    Descuento = NumeroDecimal(c_Descuento.value) * 1;

    OIngreso = NumeroDecimal(c_OIngreso.value) * 1;
    OEgreso = NumeroDecimal(c_OEgreso.value) * 1;
      
   
    Efectivo = NumeroDecimal(c_Efectivo.value) * 1;
    Tarjeta = NumeroDecimal(c_Tarjeta.value) * 1;
    Cheque = NumeroDecimal(c_Cheque.value) * 1;
    Consignado = NumeroDecimal(c_Consignado.value) * 1;
    TotalCheques = NumeroDecimal($("#chTotales").html()) * 1;
    if (isNaN(TotalCheques))
        TotalCheques = 0;
    Abonado = Tarjeta + Efectivo + Consignado + Cheque + TotalCheques - EditTotalCheques;
    Recaudado = NumeroDecimal(c_VRecaudado.value) * 1;

    SaldoFac = Recaudado + Anticipo - TotalSeleccionado + RetIVA + RetCREE + RetFuente + RetICA + Descuento + OEgreso - OIngreso;
    Diferencia = TotalSeleccionado - Recaudado - Anticipo - RetIVA - RetCREE - RetFuente - RetICA - Descuento - OEgreso + OIngreso;
    
    if (SaldoFac < 0)
        SaldoFac = 0;
    SaldoAbo = Recaudado - Abonado;
    if (caja != 'No') {
        if (SaldoAbo < 0) {
            $.jGrowl(ValidarTraduccion("Lo abonado en") + " " + caja.id + " " + ValidarTraduccion("no puede ser mayor a lo recaudado"), { life: 4000, theme: 'growl-warning', header: '' });
            caja.value = "";
            CalcularTotal(caja)
        }
    }
     
    Abonado = Tarjeta + Efectivo + Consignado + TotalCheques;
    SaldoAbo = Recaudado - Abonado;

    $("#VRecibido").val(formato_numero(Abonado, 0, ".", ","));
    $("#SCompra").val(formato_numero(SaldoFac, 0, ".", ","));
    $("#SPagar").val(formato_numero(SaldoAbo, 0, ".", ","));
    $("#TTarjeta").val($("#Tarjeta").val());
    $("#TCheque").val(formato_numero(TotalCheques, "0", ".", ",", ""));
    $("#SDiferencia").val(formato_numero(Diferencia, "0", ".", ",", ""));
    $("#TConsignacion").val($("#Consignacion").val())
    
}


function LimpiarConsignacion() {

    $("#con_fecha").val("");
    $("#con_tipo").val("").trigger("change");
    $("#con_cuenta").val("").trigger("change");
    $("#con_numero").val("");
    $("#Consignacion").val("");
    CalcularTotal('No');
}

function GuardarComprobante() {

    if (($("#NombreProveedor").html() == "") || (NCPago != 0))
        return false;
        
    var numtarj = $("#tnumero").val() * 1;
    var valtarj = NumeroDecimal($("#Tarjeta").val()) * 1;
    var tiptarj = $("#ttipo").val() * 1;
    var auttarj = $.trim($("#tautoriza").val());
    var con_fecha = $("#con_fecha").val();
    var con_tipo = $("#con_tipo").val();
    var con_cuenta = $("#con_cuenta").val();
    var con_numero = $("#con_numero").val() * 1;
    var Observacion = $.trim($("#Observacion").val());

    if (Consignado > 0) {
        if (con_cuenta == "") {
            $("#ModalConsignacion").modal("show");
            $("#con_cuenta").focus();
            swal(ValidarTraduccion(ValidarTraduccion("Acción Cancelada")), ValidarTraduccion("Debe de Seleccionar la Cuenta a Consignar"), "error");
            return false
        }

        if (con_numero == 0) {
            $("#ModalConsignacion").modal("show");
            $("#con_numero").focus();
            swal(ValidarTraduccion(ValidarTraduccion("Acción Cancelada")), "Debe de Ingresar el número de control de la consignación", "error");
            return false
        }

        if (con_fecha == "") {
            $("#ModalConsignacion").modal("show");
            $("#con_fecha").focus();
            swal(ValidarTraduccion(ValidarTraduccion("Acción Cancelada")), ValidarTraduccion("Debe de Ingresar la Fecha de Consignación"), "error");
            return false
        }
    }

    if (valtarj > 0) {
        if (tiptarj == 0) {
            $("#ModalTarjeta").modal("show");
            swal(ValidarTraduccion(ValidarTraduccion("Acción Cancelada")), ValidarTraduccion("Debe de ingresar la entidad de la tarjeta"), "error");
            return false
        }
        if ($.trim(numtarj).length < 4) {
            $("#ModalTarjeta").modal("show");
            swal(ValidarTraduccion(ValidarTraduccion("Acción Cancelada")), ValidarTraduccion("El número de la tarjeta debe ser de 4 dígistos mínimos"), "error");
            return false
        }
        if (auttarj == "") {
            $("#ModalTarjeta").modal("show");
            swal(ValidarTraduccion(ValidarTraduccion("Acción Cancelada")), ValidarTraduccion("Debe de ingresar la autorización de la tarjeta"), "error");
            return false
        }
    }

       

    if ($("#Concepto").val() == "") {
        $("#Concepto").focus();
        swal(ValidarTraduccion("Acción Cancelada"), ValidarTraduccion("Debe de ingresar el concepto del comprobante de pago"), "warning");
        return false;
    }
        

    if (Recaudado <= 0 ) {
        $("#VRecaudado").focus();
        swal(ValidarTraduccion("Acción Cancelada"), ValidarTraduccion("Debe de ingresar el monto del comprobante de pago"), "warning");
        return false;
    }
    
    if (Math.abs(SaldoAbo) > 0) {
        swal(ValidarTraduccion("Acción Cancelada"), ValidarTraduccion("Debe de completar el saldo por pagar"), "warning");
        return false;
    }

    if (Diferencia <= -1) {
        swal(ValidarTraduccion("Acción Cancelada"), ValidarTraduccion("No puede haber diferencia negativa en el comprobante de pago"), "warning");
        return false;
    }

    if (SaldoFac > 1 && Compras != "") {
        swal(ValidarTraduccion("Acción Cancelada"), "Debe de completar el saldo por abonar a facturas de compra", "warning");
        return false;
    } else {
        if (SaldoFac > 1) {
            var a_com = document.getElementsByName("TablaCompra[]");
            var a_val = document.getElementsByName("TablaValor[]");
            for (var x = 0; x < a_com.length; x++) {
                if (SeleccionarFila(x, (NumeroDecimal(a_val[x].innerHTML) * 1), a_com[x].innerHTML) == false) {
                    break;
                }
            }
        }
    }

            
    ActivarLoad();
    setTimeout(function () {
        var parametros = "Proveedor=" + IdProveedor + "&Compras=" + Compras + "&Concepto=" + $("#Concepto").val() + "&Recaudado=" + Recaudado + "&Efectivo=" + Efectivo +
            "&RetIVA=" + RetIVA + "&RetCRE=" + RetCREE + "&RetICA=" + RetICA + "&RetFuente=" + RetFuente + "&Descuento=" + Descuento + "&OIngresos=" + OIngreso + "&OEgresos=" + OEgreso +
            "&numtarj=" + numtarj + "&valtarj=" + valtarj + "&tiptarj=" + tiptarj + "&auttarj=" + auttarj + "&Guia=" + Guia +
            "&confecha=" + con_fecha + "&contipo=" + con_tipo + "&concuenta=" + con_cuenta + "&connumero=" + con_numero + "&Consignacion=" + Consignado +
            "&NombreProveedor=" + NombreProveedor + "&Movimiento=" + Movimiento + "&Observacion=" + Observacion;
        var datos = LlamarAjax("Caja","opcion=GuardarComprobante&"+ parametros);
        DesactivarLoad();
        datos = datos.split("|");
        if (datos[0] == "0") {
            swal("", ValidarTraduccion(datos[1]) + " " + datos[2], "success");
            NCPago = datos[2] * 1;
            $("#Comprobante").html(NCPago);
            $("#btnImprimir").removeClass("hidden")
            $("#btnImprimirmov").removeClass("hidden")
            $("#btnEnviar").removeClass("hidden")
            $("#btnAnular").removeClass("hidden")
        } else {
            swal(ValidarTraduccion("Acción Cancelada"), datos[1], "warning");
        }
    }, 15);
    
}

function ModalComprobante() {
    $("#modalConsultarComprobantes").modal("show");
}

function BuscarComprobante(comprobante, Proveedor) {

    $("#modalConsultarRecibos").modal("hide");
    ActivarLoad();
    setTimeout(function () {
        $("#Proveedor").val(Proveedor);
        $("#Comprobante").html(comprobante);
        IdProveedor = 0;
        DocumentoPro = "";
        BuscarProveedorCP(Proveedor, comprobante);
        DesactivarLoad();
        $("#btnImprimir").removeClass("hidden");
        $("#btnImprimirmov").removeClass("hidden");
        $("#btnEnviar").removeClass("hidden");
        $("#btnGuardar").addClass("hidden");

        $("#RetIVA").prop("disabled",true);
        $("#RetCREE").prop("disabled", true);
        $("#RetICA").prop("disabled", true);
        $("#RetFuente").prop("disabled", true);
        $("#Efectivo").prop("disabled", true);
        $("#Descuento").prop("disabled", true);
        $("#OEgreso").prop("disabled", true);
        $("#VRecaudado").prop("disabled", true);
        $("#Concepto").prop("disabled", true);
    }, 15);

}


function CargarModalComprobantes() {

    var cpago = $("#BNComprobante").val() * 1;
    var documento = $("#BDocumento").val() * 1;
    var compra = $("#BCompra").val() * 1;
    var proveedor = $.trim($("#BProveedor").val());
    var estado = $("#BEstado").val();
    var fechad = $("#BFechaDesde").val();
    var fechah = $("#BFechaHasta").val();
    var dias = $("#BUltimoDia").val() * 1;

    if (fechad == "" && fechah == "" && dias == 0) {
        swal("Alerta", "debe de ingresar una fecha o números de días ", "error");
        return false;
    }


    ActivarLoad();

    setTimeout(function () {
        var parametros = "cpago=" + cpago + "&compra=" + compra + "&documento=" + documento + "&proveedor=" + proveedor + "&estado=" + estado + "&fechad=" + fechad + "&fechah=" + fechah + "&dias=" + dias;
        var datos = LlamarAjax('Caja','opcion=ModalComprobantes&'+ parametros);
        DesactivarLoad();
        var datajson = JSON.parse(datos);
        $('#tablamodalComprobantes').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "cpago", "className": "text-XX" },
                { "data": "documento" },
                { "data": "proveedor" },
                { "data": "fecha" },
                { "data": "estado" },
                { "data": "recaudado", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "usuario" },
                { "data": "anulado" },
                { "data": "fechaenvio" },
                { "data": "total" }
            ],

            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy','colvis'
            ]
        });
    }, 15);
}

$("#tablamodalComprobantes > tbody").on("dblclick", "tr", function () {
    var row = $(this).parents("td").context.cells;
    var cpago = row[0].innerText;
    var proveedor = row[1].innerText;
    BuscarComprobante(cpago, proveedor); 
    $("#modalConsultarComprobantes").modal("hide");
});


$("#tablamodalproveedorecp > tbody").on("click", "tr", function (e) {
    var row = $(this).parents("td").context.cells;
    var numero = row[0].innerText;
    $("#modalProveedoresCP").modal("hide");
    $("#Proveedor").val(numero);
    BuscarProveedorCP(numero, 0);
});

function ValidarProveedorCP(documento) {

    if (ErrorPro == 0) {
        if ($.trim(DocumentoPro) != "") {
            if (DocumentoPro != documento) {
                DocumentoPro = "";
                Limpiar();
            }
        }
    } else {
        ErrorPro = 0;
    }
    
}


function BuscarProveedorCP(documento, recibo) {

        
    NCPago = recibo;
    $("#Recibo").html(recibo);

    if ($.trim(documento) == "")
        return false;

    if (DocumentoPro == documento)
        return false;

    DocumentoPro = documento;

    ActivarLoad();

    setTimeout(function () {

        var parametros = "documento=" + documento;
        var datos = LlamarAjax('Facturacion','opcion=BuscarProveedor&'+ parametros);
        datos = datos.split("|");
        DesactivarLoad();
        
        if (datos[0] == "0") {
            eval("data=" + datos[1]);
            IdProveedor = data[0].id * 1;

            PlazoPago = data[0].plazopago;
            CorreoProveedor = data[0].email;

            $("#ChNombre").val(data[0].nombrecompleto);
            $("#ChNombre").prop("inicial", data[0].nombrecompleto);
            $("#ChCedula").val(data[0].documento);
            $("#ChCedula").prop("inicial", data[0].documento);
            $("#ChTelefono").val((data[0].telefono ? data[0].telefono : "") + ", " + (data[0].celular ? data[0].celular : ""));
            $("#ChTelefono").prop("inicial", (data[0].telefono ? data[0].telefono : "") + ", " + (data[0].celular ? data[0].celular : ""));
            $("#ChDireccion").val(data[0].direccion);
            $("#ChDireccion").prop("inicial", data[0].direccion);
            $("#Moneda").html("Pesos");
            ProReteIva = data[0].reteiva*1;
            ProReteIca = data[0].reteica * 1;
            ProReteFuente = data[0].retefuente * 1
            Pro_Correo = (data[0].email ? data[0].email : "");
            $("#NombreProveedor").html(data[0].nombrecompleto + "<br>" + data[0].direccion + " (" + data[0].ciudad + ") Tel: " + (data[0].telefono ? data[0].telefono : "") + ", " + (data[0].celular ? data[0].celular : "") + ", " + Pro_Correo);
            NombreProveedor = data[0].nombrecompleto;
            if (NCPago == 0)
                TablaComprasPendientesCP();
            else
                TablaComprasCPago();

            //$("#SaldoProveedorDev").html(SaldoTotal(IdProveedor));

        } else {
            ErrorPro = 1;
            $("#Proveedor").select();
            $("#Proveedor").focus();
            swal("Acción Cancelada", "Proveedor no Registrado", "warning");
            IdProveedor = 0;
        }
        
    }, 15);
}


$("#MTablaMovimientoCP > tbody").on("dblclick", "tr", function () {
    var row = $(this).parents("td").context.cells;
    Movimiento = row[0].innerText;
    var descripcion = "<b>Fecha:</b> " + row[1].innerText + ", <b>Referencia:</b> " + row[2].innerText + ", <b>Descripción:</b> " + row[3].innerText + ", <b>Monto:</b> " + row[5].innerText;
    $("#Detalle_Movimiento").html(descripcion);
    Recaudado = NumeroDecimal(row[5].innerText);
    Consignado = NumeroDecimal(row[5].innerText);
    $("#VRecaudado").val(row[5].innerText)
    $("#con_cuenta").val($("#MCuenta").val()).trigger("change");
    $("#con_numero").val(row[2].innerText);
    $("#con_fecha").val(row[1].innerText);
    $("#con_tipo").val("E").trigger("change");
    $("#ModalMovBancarioCP").modal("hide");
    CargarModalConsig();
});

function VerEstadoCuenta() {
    EstadoCuenta(IdProveedor, NombreProveedor, CorreoProveedor, TipoProveedor, PlazoPago, CorreoProveedor);
}

function CargarModalProveedoresCP() {

    var Razon = $.trim($("#PRazon").val());
    var Cedula = $.trim($("#PCedula").val());

    var parametros = "Razon=" + Razon + "&Cedula=" + Cedula;
    var datos = LlamarAjax('Cotizacion','opcion=ModalProveedores&'+ parametros);
    var datajson = JSON.parse(datos);
    var tabla = $('#tablamodalproveedorecp').DataTable({
        data: datajson,
        bProcessing: true,
        bDestroy: true,
        paging: true,
        ordering: true,
        info: false,
        searching: false,
        columns: [
            { "data": "documento" },
            { "data": "nombrecompleto" },
            { "data": "direccion" },
            { "data": "pais" },
            { "data": "telefono" },
            { "data": "email" }
        ],
        "lengthMenu": [[5], [5]],
        "language": {
            "url": LenguajeDataTable
        }

    });
}

function ModalProveedoresCP() {
    $("#modalProveedoresCP").modal("show");
    $("#CRazon").focus();
}


$('select').select2();
DesactivarLoad();
