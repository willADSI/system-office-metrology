﻿d = new Date();
month = d.getMonth() + 1;
day = d.getDate();

var Anio = $.trim(d.getFullYear());
Anio = $.trim(Anio.substr(2, 2))


output = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-01';

output2 = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-' +
    (day < 10 ? '0' : '') + day;

$("#FechaDesde").val(output);
$("#FechaHasta").val(output2);

var ctabla1 = document.getElementById('myTabla1').getContext('2d');
var ctabla2 = document.getElementById('myTabla2').getContext('2d');
var ctabla3 = document.getElementById('myTabla3').getContext('2d');
var ctabla4 = document.getElementById('myTabla4').getContext('2d');
var ctabla5 = document.getElementById('myTabla5').getContext('2d');
var ctabla6 = document.getElementById('myTabla6').getContext('2d');
var ctabla7 = document.getElementById('myTabla7').getContext('2d');
var ctabla8 = document.getElementById('myTabla8').getContext('2d');
var ctabla9 = document.getElementById('myTabla9').getContext('2d');
var ctabla10 = document.getElementById('myTabla10').getContext('2d');

var myTabla1 = null;
var myTabla2 = null;
var myTabla3 = null;
var myTabla4 = null;
var myTabla5 = null;
var myTabla6 = null;
var myTabla7 = null;
var myTabla8 = null;
var myTabla9 = null;
var myTabla10 = null;

var movil = 0;
if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
    movil = 1;
}

function CargarGraficas() {
    var fechaini = $("#FechaDesde").val();
    var fechafin = $("#FechaHasta").val();
    var newDataset;

    
    if (fechaini == "" || fechafin == "") {
        $("#fechaini").focus();
        swal("Acción Cancelada", "Debe seleccionar la fecha inicial y fecha final", "warning");
        return false;
    }
        
            
    ActivarLoad();
    setTimeout(function () {

        var config1 = {
            type: 'bar',
            data: {
                datasets: [],
                labels: []
            },
            options: {
                responsive: true,
                tooltips: {
                    enabled: true
                },
                legend: {
                    position: 'top',
                    display: (movil == 1 ? false : true)
                }
            }
        }

        var config2 = {
            type: 'bar',
            data: {
                datasets: [],
                labels: []
            },
            options: {
                responsive: true,
                tooltips: {
                    enabled: false
                },
                legend: {
                    position: 'top',
                    display: (movil == 1 ? false : true)
                },

            }
        }

        var config3 = {
            type: 'bar',
            data: {
                datasets: [],
                labels: []
            },
            options: {
                responsive: true,
                tooltips: {
                    enabled: false
                },
                legend: {
                    position: 'top',
                    display: (movil == 1 ? false : true)
                }


            }
        }

        var config4 = {
            type: 'bar',
            data: {
                datasets: [],
                labels: []
            },
            options: {
                responsive: true,
                tooltips: {
                    enabled: false
                },
                legend: {
                    position: 'top',
                    display: (movil == 1 ? false : true)
                }
            }
        }

        var config5 = {
            type: 'bar',
            data: {
                datasets: [],
                labels: []
            },
            options: {
                responsive: true,
                tooltips: {
                    enabled: false
                },
                legend: {
                    position: 'top',
                    display: (movil == 1 ? false : true)
                }
            }
        }

        var config6 = {
            type: 'bar',
            data: {
                datasets: [],
                labels: []
            },
            options: {
                responsive: true,
                tooltips: {
                    enabled: false
                },
                legend: {
                    position: 'top',
                    display: (movil == 1 ? false : true)
                }
            }
        }

        var config7 = {
            type: 'bar',
            data: {
                datasets: [],
                labels: []
            },
            options: {
                responsive: true,
                tooltips: {
                    enabled: false
                },
                legend: {
                    position: 'top',
                    display: (movil == 1 ? false : true)
                }
            }
        }

        var config8 = {
            type: 'bar',
            data: {
                datasets: [],
                labels: []
            },
            options: {
                responsive: true,
                tooltips: {
                    enabled: false
                },
                legend: {
                    position: 'top',
                    display: (movil == 1 ? false : true)
                }
            }
        }

        var config9 = {
            type: 'bar',
            data: {
                datasets: [],
                labels: []
            },
            options: {
                responsive: true,
                tooltips: {
                    enabled: false
                },
                legend: {
                    position: 'top',
                    display: (movil == 1 ? false : true)
                }
            }
        }

        var config10 = {
            type: 'bar',
            data: {
                datasets: [],
                labels: []
            },
            options: {
                responsive: true,
                tooltips: {
                    enabled: false
                },
                legend: {
                    position: 'top',
                    display: (movil == 1 ? false : true)
                }
            }
        }
                     
        var colores = ['rgb(255, 99, 132)', 'rgb(255, 159, 64)', 'rgb(255, 205, 86)', 'rgb(75, 192, 192)', 'rgb(54, 162, 235)', 'rgb(153, 102, 255)', 'rgb(2, 254, 21)', 'rgb(6, 252, 245)', 'rgb(240, 20, 10)', 'rgb(254, 246, 2)'];

        var datos = LlamarAjax("Estadisticas","opcion=TablaInformesProduccion&fechaini=" + fechaini + "&fechafin=" + fechafin).split("|");
        DesactivarLoad();
      
        //GRAFICA NUMERO UNO
        var etiqueta = ['Ingresos', '', ''];
        for (fila = 1; fila <= 10; fila++) {
            resultado = "";
            porcentaje = 0;
            totaling = 0;
            totalope = 0;
            totalno = 0;

            switch (fila) {
                case 1:
                    etiqueta[1] = "Cotizado"
                    etiqueta[2] = "NO-" + etiqueta[1];
                    break;
                case 2:
                    etiqueta[1] = "Reportado"
                    etiqueta[2] = "NO-" + etiqueta[1];
                    break;
                case 3:
                    etiqueta[1] = "Certificado"
                    etiqueta[2] = "NO-" + etiqueta[1];
                    break;
                case 4:
                    etiqueta[1] = "Informe"
                    etiqueta[2] = "NO-" + etiqueta[1];
                    break;
                case 5:
                    etiqueta[1] = "Orden"
                    etiqueta[2] = "NO-" + etiqueta[1];
                    break;
                case 6:
                    etiqueta[1] = "Facturado"
                    etiqueta[2] = "NO-" + etiqueta[1];
                    break;
                case 7:
                    etiqueta[1] = "Recibido-Ing"
                    etiqueta[2] = "NO-" + etiqueta[1];
                    break;
                case 8:
                    etiqueta[1] = "Salida"
                    etiqueta[2] = "NO-" + etiqueta[1];
                    break;
                case 9:
                    etiqueta[1] = "Entregado"
                    etiqueta[2] = "NO-" + etiqueta[1];
                    break;
                case 10:
                    etiqueta[1] = "Encuesta"
                    etiqueta[2] = "NO-" + etiqueta[1];
                    break;
            }

            var newDataset1 = {
                label: etiqueta[0],
                backgroundColor: colores[0],
                data: [],
            };

            var newDataset2 = {
                label: etiqueta[1],
                backgroundColor: colores[1],
                data: [],
            };

            var newDataset3 = {
                label: etiqueta[2],
                backgroundColor: colores[3],
                data: [],
            };


            var data = JSON.parse(datos[(fila-1)]);

            for (var x = 0; x < data.length; x++) {
                totaling += data[x].ingreso * 1;
                totalope += data[x].cantidad * 1;
                totalno += (data[x].ingreso * 1 - data[x].cantidad * 1);
            }

            for (var x = 0; x < data.length; x++) {
                porcentaje = data[x].cantidad * 100 / totaling;
                resultado += "<tr>" +
                    "<td>" + data[x].descripcion + "</td>" +
                    "<td align='right'>" + data[x].ingreso + "</td>" +
                    "<td align='right'><b>" + data[x].cantidad + "</b>(" + formato_numero(porcentaje, 1, ".", ",", "%") + ")</td>" +
                    "<td align='right'><b>" + (data[x].ingreso - data[x].cantidad) + "</b>(" + formato_numero(100 - porcentaje, 1, ".", ",", "%") + ")</td></tr>";

                newDataset1.data.push(data[x].ingreso);
                newDataset2.data.push(data[x].cantidad);
                newDataset3.data.push(data[x].ingreso - data[x].cantidad);

                    
                switch (fila) {
                    case 1:
                        config1.data.labels.push(data[x].descripcion);
                        break;
                    case 2:
                        config2.data.labels.push(data[x].descripcion);
                        break;
                    case 3:
                        config3.data.labels.push(data[x].descripcion);
                        break;
                    case 4:
                        config4.data.labels.push(data[x].descripcion);
                        break;
                    case 5:
                        config5.data.labels.push(data[x].descripcion);
                        break;
                    case 6:
                        config6.data.labels.push(data[x].descripcion);
                        break;
                    case 7:
                        config7.data.labels.push(data[x].descripcion);
                        break;
                    case 8:
                        config8.data.labels.push(data[x].descripcion);
                        break;
                    case 9:
                        config9.data.labels.push(data[x].descripcion);
                        break;
                    case 10:
                        config10.data.labels.push(data[x].descripcion);
                        break;
                }
            }

            resultado += "<tr>" +
                "<td align='right' class='text-info text-XX'>TOTALES</td>" +
                "<td align='right' class='text-info text-XX'>" + totaling + "</td>" +
                "<td align='right' class='text-info text-XX'><b>" + totalope + "</td>" +
                "<td align='right' class='text-info text-XX'><b>" + totalno + "</td></tr>";

            $("#Tabla" + fila).html(resultado);

            switch (fila) {
                case 1:
                    config1.data.datasets.push(newDataset1);
                    config1.data.datasets.push(newDataset2);
                    config1.data.datasets.push(newDataset3);

                    if (myTabla1 != null) {
                        myTabla1.destroy();
                    }

                    myTabla1 = new Chart(ctabla1, config1);
                    myTabla1.update();
                    break;
                case 2:
                    config2.data.datasets.push(newDataset1);
                    config2.data.datasets.push(newDataset2);
                    config2.data.datasets.push(newDataset3);

                    if (myTabla2 != null) {
                        myTabla2.destroy();
                    }

                    myTabla2 = new Chart(ctabla2, config2);
                    myTabla2.update();
                    break;
                case 3:
                    config3.data.datasets.push(newDataset1);
                    config3.data.datasets.push(newDataset2);
                    config3.data.datasets.push(newDataset3);

                    if (myTabla3 != null) {
                        myTabla3.destroy();
                    }

                    myTabla3 = new Chart(ctabla3, config3);
                    myTabla3.update();
                    break;
                case 4:
                    config4.data.datasets.push(newDataset1);
                    config4.data.datasets.push(newDataset2);
                    config4.data.datasets.push(newDataset3);

                    if (myTabla4 != null) {
                        myTabla4.destroy();
                    }

                    myTabla4 = new Chart(ctabla4, config4);
                    myTabla4.update();
                    break;
                case 5:
                    config5.data.datasets.push(newDataset1);
                    config5.data.datasets.push(newDataset2);
                    config5.data.datasets.push(newDataset3);

                    if (myTabla5 != null) {
                        myTabla5.destroy();
                    }

                    myTabla5 = new Chart(ctabla5, config5);
                    myTabla5.update();
                    break;
                case 6:
                    config6.data.datasets.push(newDataset1);
                    config6.data.datasets.push(newDataset2);
                    config6.data.datasets.push(newDataset3);

                    if (myTabla6 != null) {
                        myTabla6.destroy();
                    }

                    myTabla6 = new Chart(ctabla6, config6);
                    myTabla6.update();
                    break;
                case 7:
                    config7.data.datasets.push(newDataset1);
                    config7.data.datasets.push(newDataset2);
                    config7.data.datasets.push(newDataset3);

                    if (myTabla7 != null) {
                        myTabla7.destroy();
                    }

                    myTabla7 = new Chart(ctabla7, config7);
                    myTabla7.update();
                    break;
                case 8:
                    config8.data.datasets.push(newDataset1);
                    config8.data.datasets.push(newDataset2);
                    config8.data.datasets.push(newDataset3);

                    if (myTabla8 != null) {
                        myTabla8.destroy();
                    }

                    myTabla8 = new Chart(ctabla8, config8);
                    myTabla8.update();
                    break;
                case 9:
                    config9.data.datasets.push(newDataset1);
                    config9.data.datasets.push(newDataset2);
                    config9.data.datasets.push(newDataset3);

                    if (myTabla9 != null) {
                        myTabla9.destroy();
                    }

                    myTabla9 = new Chart(ctabla9, config9);
                    myTabla9.update();
                    break;
                case 10:
                    config10.data.datasets.push(newDataset1);
                    config10.data.datasets.push(newDataset2);
                    config10.data.datasets.push(newDataset3);

                    if (myTabla10 != null) {
                        myTabla10.destroy();
                    }

                    myTabla10 = new Chart(ctabla10, config10);
                    myTabla10.update();
                    break;
            }
        }
            
    }, 15);
}

DesactivarLoad();
