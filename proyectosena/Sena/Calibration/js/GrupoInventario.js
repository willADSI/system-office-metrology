﻿
$("#Equ_Grupo").html(CargarCombo(47, 1));
$("#Mod_Marca").html(CargarCombo(49, 1));
$("#Gru_Estado, #Equ_Estado, #Mar_Estado, #Mod_Estado").html(CargarCombo(37, 0));
$("#Gru_CuentaContable").html(CargarCombo(83, 1));

tb = $('input');
$(tb).keypress(enter2tab);

tb = $('select');
$(tb).keypress(enter2tab); 

function enter2tab(e) {
    if (e.keyCode == 13) {
        id = $(this).attr("id");

        cb = parseInt($(this).attr('tabindex'));

        if (id == "Documento") {
            if ($.trim($(this).val()) != "") {

                BuscarCliente($(this).val());
                $("#Sede").focus();
                return false;
            }
        }

        if ($(':input[tabindex=\'' + (cb + 1) + '\']') != null) {
            $(':input[tabindex=\'' + (cb + 1) + '\']').focus();
            $(':input[tabindex=\'' + (cb + 1) + '\']').select();
            e.preventDefault();

            return false;
        }

        if ($(':select[tabindex=\'' + (cb + 1) + '\']') != null) {
            console.log("1");
            $(':select[tabindex=\'' + (cb + 1) + '\']').focus();
            $(':select[tabindex=\'' + (cb + 1) + '\']').select();
            e.preventDefault();

            return false;
        }
    }
}

function TablaGrupos() {
    var datos = LlamarAjax('Inventarios/TablaConfiguracion', "tipo=1&id=0");
    var datasedjson = JSON.parse(datos);
    $('#TablaGrupos').DataTable({
        data: datasedjson,
        bProcessing: true,
        bDestroy: true,
        columns: [
            { "data": "id" },
            { "data": "abreviatura" },
            { "data": "descripcion" },
            { "data": "cuenta" },
            { "data": "desestado" },
            { "data": "eliminar" }
        ],
        "language": {
            "url": LenguajeDataTable
        }
    });
}

function LimpiarGrupos() {
    $("#Gru_Estado").val("1").trigger("change");
    $("#Gru_Descripcion").val("")
    $("#Gru_CuentaContable").val("").trigger("change");
    $("#Gru_Id").val("0")
    $("#Gru_Inicial").val("");
}

$("#TablaGrupos > tbody").on("dblclick", "tr", function () {
    var row = $(this).parents("td").context.cells;
    var numero = row[0].innerText;
    var datos = LlamarAjax('Inventarios/TablaConfiguracion', "tipo=1&id=" + numero);
    var data = JSON.parse(datos);
    $("#Gru_Estado").val(data[0].estado).trigger("change");
    $("#Gru_Descripcion").val(data[0].descripcion);
    $("#Gru_CuentaContable").val(data[0].idcuecontable).trigger("change");
    $("#Gru_Id").val(data[0].id);
    $("#Gru_Inicial").val(data[0].abreviatura);
});


$("#formGrupo").submit(function (e) {
    e.preventDefault();
    var parametros = $(this).serialize();
    var datos = LlamarAjax("Inventarios/GuardarGrupos", parametros).split("|");
    if (datos[0] == "0") {
        TablaGrupos();
        LimpiarGrupos();
        swal("", datos[1], "success");
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }

});


//************************EQUIPOS********************************

function LimpiarEquipos() {
    $("#Equ_Estado").val("1").trigger("change");
    $("#Equ_Descripcion").val("")
    $("#Equ_Id").val("0")
    $("#Equ_Codigo").val("0");
}

function CambioGrupoEquipo() {
    TablaEquipos();
}

$("#TablaEquipos > tbody").on("dblclick", "tr", function () {
    var row = $(this).parents("td").context.cells;
    var numero = row[0].innerText;
    var datos = LlamarAjax('Inventarios/TablaConfiguracion', "tipo=2&id=" + numero);
    var data = JSON.parse(datos);
    $("#Equ_Grupo").val(data[0].idgrupo).trigger("change");
    $("#Equ_Estado").val(data[0].estado).trigger("change");
    $("#Equ_Descripcion").val(data[0].descripcion);
    $("#Equ_Codigo").val(data[0].codigo);
    $("#Equ_Id").val(data[0].id);
});


$("#formEquipos").submit(function (e) {
    e.preventDefault();
    var parametros = $(this).serialize();
    var datos = LlamarAjax("Inventarios/GuardarEquipos", parametros).split("|");
    if (datos[0] == "0") {
        TablaEquipos();
        LimpiarEquipos();
        swal("", datos[1], "success");
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }

});

   

function TablaEquipos() {
    var grupo = $("#Equ_Grupo").val() * 1;
    var datos = LlamarAjax('Inventarios/TablaConfiguracion', "tipo=2&id=0&grupo=" + grupo);
    var datasedjson = JSON.parse(datos);
    $('#TablaEquipos').DataTable({
        data: datasedjson,
        bProcessing: true,
        bDestroy: true,
        columns: [
            { "data": "id" },
            { "data": "grupo" },
            { "data": "descripcion" },
            { "data": "desestado" },
            { "data": "eliminar" }
        ],
        "language": {
            "url": LenguajeDataTable
        }
    });
}

function EliminarEquipo(id, equipo) {
    var accion = 1;
    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: ValidarTraduccion('¿Desea eliminar el equipo ' + equipo + '?'),
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Eliminar'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        allowOutsideClick: false,
        
        preConfirm: function (result) {
            return new Promise(function (resolve, reject) {
                $.post("Inventarios/EliminarEquipos", "Id=" + id + "&IdReemplazo=0")
                    .done(function (data) {
                        datos = data.split("|");
                        if (datos[0] == "0")
                            resolve()
                        else {
                            accion = 0;
                            resolve();
                        }
                            
                    })
            })
        }
    }]).then(function (data) {
        if (accion == 1) {
            TablaEquipos();
            LimpiarEquipos();
        } else {
            $("#TituloReemplazar").html("Reemplazar Equipo");
            $("#TextoReemplazo").html("Reemplazar equipo " + equipo);
            $("#OpcionReemplazo").html(CargarCombo(58, 1));
            $("#NumeroReemplazo").val(1);
            $("#IdReemplazo").val(id);
            $("#modalReemplazoInv").modal("show");
        }
    });
}

function ReemplazarEquipos() {
    var numero = $("#NumeroReemplazo").val() * 1;
    var idreemplazo = $("#OpcionReemplazo").val() * 1;
    var id = $("#IdReemplazo").val() * 1;
    switch (numero) {
        case 1:
            datos = LlamarAjax("Inventarios/EliminarEquipos", "Id=" + id + "&IdReemplazo=" + idreemplazo).split("|");
            if (datos[0] == "0") {
                TablaEquipos();
                LimpiarEquipos();
                swal("", datos[1], "success");
            } else {
                swal("Acción Cancelada", datos[1], "warning");
            }
            break;
        case 2:
            datos = LlamarAjax("Inventarios/EliminarIntervalo", "Id=" + id + "&IdReemplazo=" + idreemplazo).split("|");
            if (datos[0] == "0") {
                TablaIntervalos();
                LimpiarIntervalos();
                swal("", datos[1], "success");
            } else {
                swal("Acción Cancelada", datos[1], "warning");
            }
            break;
        case 3:
            datos = LlamarAjax("Inventarios/EliminarMarcas", "Id=" + id + "&IdReemplazo=" + idreemplazo).split("|");
            if (datos[0] == "0") {
                TablaMarcas();
                LimpiarMarcas();
                $("#Mod_Marca").html(CargarCombo(3, 1));
                swal("", datos[1], "success");
            } else {
                swal("Acción Cancelada", datos[1], "warning");
            }
            break;
        case 4:
            datos = LlamarAjax("Inventarios/EliminarModelos", "Id=" + id + "&IdReemplazo=" + idreemplazo).split("|");
            if (datos[0] == "0") {
                TablaModelos();
                LimpiarModelos();
                swal("", datos[1], "success");
            } else {
                swal("Acción Cancelada", datos[1], "warning");
            }
            break;
    }
    $("#modalReemplazoInv").modal("hide");
    
}


//************************* MARCAS ******************************

function TablaMarcas() {
    var datos = LlamarAjax('Inventarios/TablaConfiguracion', "tipo=3&id=0");
    var datasedjson = JSON.parse(datos);
    $('#TablaMarcas').DataTable({
        data: datasedjson,
        bProcessing: true,
        bDestroy: true,
        columns: [
            { "data": "id" },
            { "data": "descripcion" },
            { "data": "desestado" },
            { "data": "eliminar" }
        ],
        "language": {
            "url": LenguajeDataTable
        }
    });
}

function LimpiarMarcas() {
    $("#Mar_Id").val("0")
    $("#Mar_Descripcion").val("");
    $("#Mar_Estado").val("1").trigger("change");
}


$("#TablaMarcas> tbody").on("dblclick", "tr", function () {
    var row = $(this).parents("td").context.cells;
    var numero = row[0].innerText;
    var datos = LlamarAjax('Inventarios/TablaConfiguracion', "tipo=3&id=" + numero);
    var data = JSON.parse(datos);
    $("#Mar_Descripcion").val(data[0].descripcion);
    $("#Mar_Estado").val(data[0].estado).trigger("change");
    $("#Mar_Id").val(data[0].id);
});


$("#formMarca").submit(function (e) {
    e.preventDefault();
    var parametros = $(this).serialize();
    var datos = LlamarAjax("Inventarios/GuardarMarcas", parametros).split("|");
    if (datos[0] == "0") {
        TablaMarcas();
        LimpiarMarcas();
        $("#Mod_Marca").html(CargarCombo(3, 1));
        swal("", datos[1], "success");
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }

});

function EliminarMarca(id, marca) {
    var accion = 1;
    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: ValidarTraduccion('¿Desea eliminar la marca ' + marca + '?'),
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Eliminar'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        allowOutsideClick: false,

        preConfirm: function (result) {
            return new Promise(function (resolve, reject) {
                $.post("Inventarios/EliminarMarcas", "Id=" + id + "&IdReemplazo=0")
                    .done(function (data) {
                        datos = data.split("|");
                        if (datos[0] == "0")
                            resolve()
                        else {
                            accion = 0;
                            resolve();
                        }

                    })
            })
        }
    }]).then(function (data) {
        if (accion == 1) {
            TablaMarcas();
            LimpiarMarcas();
            $("#Mod_Marca").html(CargarCombo(3, 1));
        } else {
            $("#TituloReemplazar").html("Reemplazar Marcas");
            $("#TextoReemplazo").html("Reemplazar Marcas " + marca);
            $("#OpcionReemplazo").html(CargarCombo(3, 1));
            $("#NumeroReemplazo").val(3);
            $("#IdReemplazo").val(id);
            $("#modalReemplazoInv").modal("show");
        }
    });
}


//******************************* MODELOS *************************************

function TablaModelos() {
    var datos = LlamarAjax('Inventarios/TablaConfiguracion', "tipo=4&id=0");
    var datasedjson = JSON.parse(datos);
    $('#TablaModelos').DataTable({
        data: datasedjson,
        bProcessing: true,
        bDestroy: true,
        columns: [
            { "data": "id" },
            { "data": "marca" },
            { "data": "descripcion" },
            { "data": "desestado" },
            { "data": "eliminar" }
        ],
        "language": {
            "url": LenguajeDataTable
        }
    });
}

function LimpiarModelos() {
    $("#Mod_Id").val("0")
    $("#Mod_Descripcion").val("");
    $("#Mod_Marca").val("").trigger("change");
    $("#Mod_Estado").val("1").trigger("change");
}


$("#TablaModelos > tbody").on("dblclick", "tr", function () {
    var row = $(this).parents("td").context.cells;
    var numero = row[0].innerText;
    var datos = LlamarAjax('Inventarios/TablaConfiguracion', "tipo=4&id=" + numero);
    var data = JSON.parse(datos);
    $("#Mod_Descripcion").val(data[0].descripcion);
    $("#Mod_Marca").val(data[0].idmarca).trigger("change");
    $("#Mod_Estado").val(data[0].estado).trigger("change");
    $("#Mod_Id").val(data[0].id);
});


$("#formModelos").submit(function (e) {
    e.preventDefault();
    var parametros = $(this).serialize();
    var datos = LlamarAjax("Inventarios/GuardarModelos", parametros).split("|");
    if (datos[0] == "0") {
        TablaModelos();
        LimpiarModelos();
        swal("", datos[1], "success");
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }

});

function EliminarModelo(id, modelo, idmarca) {
    var accion = 1;
    swal.queue([{
        title: ValidarTraduccion('Advertencia'),
        text: ValidarTraduccion('¿Desea eliminar el modelo ' + modelo + '?'),
        type: 'question',
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Eliminar'),
        cancelButtonText: ValidarTraduccion('Cancelar'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        allowOutsideClick: false,

        preConfirm: function (result) {
            return new Promise(function (resolve, reject) {
                $.post("Inventarios/EliminarModelo", "Id=" + id + "&IdReemplazo=0")
                    .done(function (data) {
                        datos = data.split("|");
                        if (datos[0] == "0")
                            resolve()
                        else {
                            accion = 0;
                            resolve();
                        }

                    })
            })
        }
    }]).then(function (data) {
        if (accion == 1) {
            TablaModelos();
            LimpiarModelos();
        } else {
            $("#TituloReemplazar").html("Reemplazar Marcas");
            $("#TextoReemplazo").html("Reemplazar Marcas " + marca);
            $("#OpcionReemplazo").html(CargarCombo(5, 1, "", idmarca));
            $("#NumeroReemplazo").val(4);
            $("#IdReemplazo").val(id);
            $("#modalReemplazoInv").modal("show");
        }
    });
}


TablaGrupos();
TablaEquipos();
TablaMarcas();
TablaModelos();

$('select').select2();
DesactivarLoad();
