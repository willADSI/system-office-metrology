﻿var tabmodgru = null;
var tabpergru = null;
var tabpermisos = null;
var tabsubmenu = null;
var DocumentoUsu = "";
var tabmenu = null;
var tabgruusuario = null;
var IdEmpleado = 0;

$("#Magnitudes").html(CargarCombo(1, 0));
$("#MGrupo").html(CargarCombo(22, 1));
$("#Empleado").html(CargarCombo(29, 7,"","-1"));


function TablaUsuarios() {

    var usuario = $.trim($("#CUsuario").val());
    var documento = $.trim($("#CDocumento").val());
    var nombre = $.trim($("#CNombre").val());
    var estado = $("#CEstado").val();
            
    ActivarLoad();
    setTimeout(function () {
        var parametros = "usuario=" + usuario + "&documento=" + documento + "&nombre=" + nombre + "&estado=" + estado;
        var datos = LlamarAjax("Seguridad","opcion=TablaUsuarios&" + parametros);
        DesactivarLoad();
        var datajson = JSON.parse(datos);
        $('#TablaUsuarios').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "idusu" },
                { "data": "nomusu" },
                { "data": "documento" },
                { "data": "nombrecompleto" },
                { "data": "cargo" },
                { "data": "telefono" },
                { "data": "email" },
                { "data": "correoenvio" },
                { "data": "grupo" }
            ],

            "language": {
                "url": LenguajeDataTable
            }
        });
    }, 15);
}

function TablaGrupos() {

    
    ActivarLoad();
    setTimeout(function () {
        var parametros = "";
        var datos = LlamarAjax("Seguridad", "opcion=TablaGrupos&usuario=0");
        DesactivarLoad();
        var datajson = JSON.parse(datos);
        $('#TablaGrupos').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "idgru" },
                { "data": "codgru" },
                { "data": "desgru" },
                { "data": "estgru" }
            ],
            "language": {
                "url": LenguajeDataTable
            }
        });
    }, 15);
}



function TablaModulos() {


    ActivarLoad();
    setTimeout(function () {
        var parametros = "";
        var datos = LlamarAjax("Seguridad","opcion=TablaModulos&grupo=0");
        DesactivarLoad();
        var datajson = JSON.parse(datos);
        $('#TablaModulos').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "idmod" },
                { "data": "codmod" },
                { "data": "desmod" },
                { "data": "estmod" }
            ],

            "language": {
                "url": LenguajeDataTable
            }
        });
    }, 15);
}

function TablaModulosGrupos(grupo) {


    ActivarLoad();
    setTimeout(function () {
        var parametros = "";
        var datos = LlamarAjax("Seguridad","opcion=TablaModulos&grupo=" + grupo);
        DesactivarLoad();
        var datajson = JSON.parse(datos);
        tabmodgru = $('#TablaModGrupos').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "codmod" },
                { "data": "desmod" },
                { "data": "estmod" },
                { "data": "idmod" }
                
            ],

           

            "language": {
                "url": LenguajeDataTable
            }
        });
    }, 15);
}

function TablaPermisos(modulo, grupo) {


    ActivarLoad();
    setTimeout(function () {
        var parametros = "";
        var datos = LlamarAjax("Seguridad","opcion=TablaPermisos&modulo=" + modulo + "&grupo=" + grupo);
        DesactivarLoad();
        var datajson = JSON.parse(datos);
        tabpermisos = $('#TablaPermisos').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "idmodacc" },
                { "data": "desacc" },
            ],

            "language": {
                "url": LenguajeDataTable
            }
        });
    }, 15);
}

function TablaPermisosGrupos(modulo, grupo) {


    ActivarLoad();
    setTimeout(function () {
        var parametros = "";
        var datos = LlamarAjax("Seguridad","opcion=TablaPermisos&modulo=" + modulo + "&grupo=" + grupo);
        DesactivarLoad();
        var datajson = JSON.parse(datos);
        tabpergru = $('#TablaPerGrupos').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "desacc" },
                { "data": "activo" }
            ],

            "language": {
                "url": LenguajeDataTable
            }
        });
    }, 15);
}

function TablaGrupoUsuario(usuario) {
    var datos = LlamarAjax("Seguridad","opcion=TablaGrupos&usuario=" + usuario);
    DesactivarLoad();
    var datajson = JSON.parse(datos);
    tabgruusuario = $('#TablaGruUsuario').DataTable({
        data: datajson,
        bProcessing: true,
        bDestroy: true,
        paging: false,
        searching: false,
        bLengthChange: false,
        columns: [
            { "data": "idgru" },
            { "data": "codgru" },
            { "data": "desgru" },
            { "data": "estgru" },
            { "data": "activo" }
        ],

        "language": {
            "url": LenguajeDataTable
        }
    });
}

function TablaMenu(grupo) {

    if (tabsubmenu != null) {
        tabsubmenu.rows().remove().draw();
    }

    if (tabmenu != null) {
        tabmenu.rows().remove().draw();
    }

    if (grupo * 1 == 0)
        return false;
    
    ActivarLoad();
    setTimeout(function () {
        var parametros = "";
        var datos = LlamarAjax("Seguridad","opcion=TablaMenu&grupo=" + (grupo*1));
        DesactivarLoad();
        var datajson = JSON.parse(datos);
        tabmenu = $('#TablaMenus').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            paging: false,
            searching: false,
            bLengthChange: false,
            columns: [
                { "data": "idmenu" },
                { "data": "desmenu" },
                { "data": "activo" }
            ],

            "language": {
                "url": LenguajeDataTable
            }
        });
    }, 15);
}

function TablaSubMenu(menu, grupo) {

    ActivarLoad();
    setTimeout(function () {
        var parametros = "";
        var datos = LlamarAjax("Seguridad","opcion=TablaSubMenu&menu=" + menu + "&grupo=" + (grupo * 1));
        DesactivarLoad();
        var datajson = JSON.parse(datos);
        tabsubmenu = $('#TabaSubMenu').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            paging: false,
            searching: false,
            bLengthChange: false,
            columns: [
                { "data": "idsubmenu" },
                { "data": "dessubmenu" },
                { "data": "activo" }
            ],

            "language": {
                "url": LenguajeDataTable
            }
        });
    }, 15);
}

$("#TablaMenus > tbody").on("dblclick", "tr", function () {
    var row = $(this).parents("td").context.cells;
    $("#MenuSubmenu").html(row[1].innerText);
    TablaSubMenu(row[0].innerText, $("#MGrupo").val()*1);
});

$("#TablaModulos > tbody").on("dblclick", "tr", function () {
    var row = $(this).parents("td").context.cells;
    $("#IdModulo").val(row[0].innerText);
    $("#MCodigo").val(row[1].innerText);
    $("#MModulo").val(row[2].innerText);
    $("#MEstado").val(row[3].innerText).trigger("change");

    TablaPermisos(row[0].innerText, 0);

});

$("#TablaPermisos > tbody").on("dblclick", "tr", function () {
    var row = $(this).parents("td").context.cells;
    $("#IdPermiso").val(row[0].innerText);
    $("#Accion").val(row[1].innerText);
});

$("#TablaGrupos > tbody").on("dblclick", "tr", function () {
    var row = $(this).parents("td").context.cells;
    $("#IdGrupo").val(row[0].innerText);
    $("#GCodigo").val(row[1].innerText);
    $("#GGrupo").val(row[2].innerText);
    $("#GEstado").val(row[3].innerText).trigger("change");
    TablaModulosGrupos(row[0].innerText);
    if (tabpergru != null) {
        tabpergru.rows().remove().draw();
    }
});

$("#TablaModGrupos > tbody").on("dblclick", "tr", function () {
    var row = $(this).parents("td").context.cells;
    $("#modpermiso").html(row[1].innerText + " y Grupo " + $("#GGrupo").val());
    var modulo = row[3].innerText;
    var grupo = $("#IdGrupo").val();
    TablaPermisosGrupos(modulo, grupo);

    
});


function LimpiarModulo() {
    $("#IdModulo").val("0");
    $("#MCodigo").val("");
    $("#MModulo").val("");
    $("#MEstado").val("ACTIVO").trigger("change");
    if (tabpermisos != null) {
        tabpermisos.rows().remove().draw();
    }
}

function LimpiarGrupo() {
    $("#IdGrupo").val("0");
    $("#GCodigo").val("");
    $("#GGrupo").val("");
    $("#GEstado").val("ACTIVO").trigger("change");
    if (tabmodgru != null) {
        tabmodgru.rows().remove().draw();
    }
    if (tabpergru != null) {
        tabpergru.rows().remove().draw();
    }
}

function GuardarModulo() {

    var id = $("#IdModulo").val()*1;
    var codigo = $.trim($("#MCodigo").val());
    var modulo = $.trim($("#MModulo").val());
    var estado = $("#MEstado").val();

    
    if (codigo == "") {
        $("#MCodigo").focus();
        swal("Acción Cancelada", "Debe de ingresar el código del módulo", "warning");
        return false;
    }
    if (modulo == "") {
        $("#MModulo").focus();
        swal("Acción Cancelada", "Debe de ingresar la descripción del módulo", "warning");
        return false;
    }

    var parametros = "id=" + id + "&codigo=" + codigo + "&modulo=" + modulo + "&estado=" + estado;
    var datos = LlamarAjax("Seguridad","opcion=GuardarModulo&" + parametros).split("|");
    if (datos[0] == "0") {
        LimpiarModulo();
        TablaModulos();
        swal("", datos[1], "success");
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
}

function GuardarGrupo() {

    var id = $("#IdGrupo").val() * 1;
    var codigo = $.trim($("#GCodigo").val());
    var grupo = $.trim($("#GGrupo").val());
    var estado = $("#GEstado").val();

    if (codigo == "") {
        $("#GCodigo").focus();
        swal("Acción Cancelada", "Debe de ingresar el código del grupo", "warning");
        return false;
    }
    if (grupo == "") {
        $("#GGrupo").focus();
        swal("Acción Cancelada", "Debe de ingresar la descripción del grupo", "warning");
        return false;
    }

    var parametros = "id=" + id + "&codigo=" + codigo + "&grupo=" + grupo + "&estado=" + estado;
    var datos = LlamarAjax("Seguridad","opcion=GuardarGrupo&" + parametros).split("|");
    if (datos[0] == "0") {
        LimpiarGrupo();
        TablaGrupos();
        swal("", datos[1], "success");
        $("#MGrupo").html(CargarCombo(22, 1));
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
}

function LimpiarPermiso() {
    $("#IdPermiso").val("0");
    $("#Lugar").val("");
    $("#Accion").val("");
}

function GuardarPermiso() {

    var id = $("#IdPermiso").val();
    var modulo = $("#IdModulo").val() * 1;
    var accion = $.trim($("#Accion").val());

    if (modulo == 0) {
        swal("Acción Cancelada", "Debe seleccionar un módulo", "warning");
        return false;
    }
        
    if (accion == "") {
        $("#Accion").focus();
        swal("Acción Cancelada", "Debe de ingresar la acción del permiso", "warning");
        return false;
    }

    var parametros = "id=" + id + "&modulo=" + modulo + "&accion=" + accion;
    var datos = LlamarAjax("Seguridad","opcion=GuardarPermisos&" + parametros).split("|");
    if (datos[0] == "0") {
        LimpiarPermiso();
        TablaPermisos(modulo,0);
        swal("", datos[1], "success");
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }
}

function ActivarPermisos(permiso, grupo, objeto) {
    var accion = ($(objeto).prop("checked") ? 1 : 0);
    var parametros = "permiso=" + permiso + "&grupo=" + grupo + "&accion=" + accion;
    LlamarAjax("Seguridad","opcion=ActivarPermisos&" + parametros);
}

function ActivarGrupo(grupo, usuario, objeto) {
    var accion = ($(objeto).prop("checked") ? 1 : 0);
    var parametros = "usuario=" + usuario + "&grupo=" + grupo + "&accion=" + accion;
    LlamarAjax("Seguridad","opcion=ActivarGrupo&" + parametros);
}

function ActivarMenu(menu, grupo, objeto) {
    var accion = ($(objeto).prop("checked") ? 1 : 0);
    var parametros = "menu=" + menu + "&grupo=" + grupo + "&accion=" + accion;
    LlamarAjax("Seguridad","opcion=ActivarMenu&" + parametros);
}

function ActivarSubMenu(submenu, grupo, objeto) {
    var accion = ($(objeto).prop("checked") ? 1 : 0);
    var parametros = "submenu=" + submenu + "&grupo=" + grupo + "&accion=" + accion;
    LlamarAjax("Seguridad","opcion=ActivarSubMenu&" + parametros);
}

TablaUsuarios();
TablaModulos();
TablaGrupos();

$("#formUsuarios").submit(function (e) {
    e.preventDefault();
    var parametros = $("#formUsuarios").serialize() + "&IdEmpleado=" + IdEmpleado;
    var datos = LlamarAjax("Seguridad","opcion=GuardarUsuarios&" + parametros).split("|");
    if (datos[0] == "0") {
        TablaUsuarios();
        swal("", datos[1], "success");
        $("#IdUsuario").val(datos[2]);
        TablaGrupoUsuario(datos[2]);
    } else {
        swal("Acción Cancelada", datos[1], "warning");
    }

    return false;

})

function LimpiarTodo() {
    $("#Documento").val("");
    DocumentoUsu = "";
    LimpiarDatos();
    $("#Documento").focus();
}

function LimpiarDatos() {
    $("#IdUsuario").val("0");
    $("#Nombres").val("");
    $("#Usuario").val("");
    $("#Cargo").val("");
    $("#Telefonos").val("");
    $("#Celular").val("");
    $("#CorreoEnvio").val("");
    $("#ClaveEnvio").val("");
    $("#CorreoPersonal").val("");
    $("#Direccion").val("");
    $("#Adjunto").val("");
    $("#Estado").val("ACTIVO").trigger("change");
    $("#Magnitudes").val("").trigger("change");
    $("#Empleado").val("").trigger("change");
    $("#Super").val("0").trigger("change");
    $("#ImgFirma").attr("src", "");
    IdEmpleado = 0;

    if (tabgruusuario != null)
        tabgruusuario.rows().remove().draw();
}

function BuscarUsuario(documento) {

    if ($.trim(documento) == "")
        return false;

    if (DocumentoUsu == documento)
        return false;
    LimpiarDatos();
    DocumentoUsu = documento;
    var parametros = "documento=" + $.trim(documento);
    var datos = LlamarAjax("Seguridad","opcion=BuscarUsuario&" + parametros);
    if (datos != "[]") {
        var data = JSON.parse(datos);
        $("#IdUsuario").val(data[0].idusu);
        $("#Usuario").val(data[0].nomusu);
        $("#Nombres").val(data[0].nombrecompleto);
        $("#Cargo").val(data[0].cargo);
        $("#Telefonos").val(data[0].telefono);
        $("#Celular").val(data[0].celular);
        $("#CorreoEnvio").val(data[0].correoenvio);
        $("#ClaveEnvio").val(data[0].clavecorreo);
        $("#CorreoPersonal").val(data[0].email);
        $("#Direccion").val(data[0].direccion);
        $("#Estado").val(data[0].estusu).trigger("change");
        $("#Super").val(data[0].supusu).trigger("change");
        $("#Empleado").val(data[0].idempleado).trigger("change");

        var magnitud = data[0].magnitud.split(",");
        for (var x = 0; x < magnitud.length; x++) {
            $("#Magnitudes option[value=" + magnitud[x] + "]").prop("selected", true);
        }
        $("#Magnitudes").trigger("change");
        
        $("#ImgFirma").attr("src", url_cliente + "imagenes/FirmaUsuario/" + data[0].idusu + ".png?id=" + NumeroAleatorio());

        TablaGrupoUsuario(data[0].idusu);
        
    } else {
        var datos = LlamarAjax("Recursohumano","opcion=BuscarEmpleados&" + parametros);
        if (datos != "[]") {
            var data = JSON.parse(datos);
            IdEmpleado = data[0].id;
            $("#IdUsuario").val(0);
            $("#Nombres").val(data[0].nombrecompleto);
            $("#Cargo").val(data[0].cargo);
            $("#Telefonos").val(data[0].telefono);
            $("#Celular").val(data[0].celular);
            $("#CorreoPersonal").val(data[0].email);
            $("#Direccion").val(data[0].direccion);
            $("#Estado").val(data[0].estado).trigger("change");
            $("#Super").val(data[0].supusu).trigger("change");

        }
    }
}

function ValidarUsuario(documento) {
    if ($.trim(DocumentoUsu) != "") {
        if (DocumentoUsu != documento) {
            DocumentoUsu = "";
            LimpiarDatos();
        }
    }
}

function ReiniciarClave() {
    var usuario = $("#IdUsuario").val() * 1;
    var datos = LlamarAjax("Seguridad","opcion=ReiniciarClave&usuario=" + usuario);
    swal("", "Clave reiniciada con éxito", "success")
}

$("#TablaUsuarios> tbody").on("dblclick", "tr", function () {
    var row = $(this).parents("td").context.cells;
    var numero = row[2].innerText;
    $('#tab-usuarios a[href="#tabcrear"]').tab('show');
    $("#Documento").val(numero);
    BuscarUsuario(numero);
});

$('select').select2();
