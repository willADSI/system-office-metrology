﻿d = new Date();
month = d.getMonth() + 1;
day = d.getDate();

var Anio = $.trim(d.getFullYear());
Anio = $.trim(Anio.substr(2, 2))


output = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-01';

output2 = d.getFullYear() + '-' +
    (month < 10 ? '0' : '') + month + '-' +
    (day < 10 ? '0' : '') + day;

$("#ReFechaDesde").val(output);
$("#ReFechaHasta").val(output2);

var movil = 0;
if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
    movil = 1;
}

var datos = LlamarAjax("Configuracion","opcion=CargaComboInicial&tipo=2").split("||");

$("#Magnitud, #CMagnitud, #Magnitud, #ICMagnitud").html(datos[0]);
$("#CEquipo, #ICEquipo").html(datos[1]);
$("#Marca, #CMarca, #ICMarca").html(datos[2]);
$("#IMedida").html(datos[4]);
$("#CCliente, #CaCliente, #ICCliente").html(datos[5]);
$("#CModelo, #ICModelo").html(datos[6]);
$("#CIntervalo, #ICIntervalo").html(datos[7]);
$("#CPais").html(datos[10]);
$("#CDepartamento").html(datos[11]);
$("#CCiudad").html(datos[12]);
$("#CUsuario, #ICUsuario").html(datos[13]);
$("#SelAccesorio").html(datos[14]);
$("#SelObservacion").html(datos[15]);

var colores = ['rgb(255, 99, 132)', 'rgb(255, 159, 64)', 'rgb(255, 205, 86)', 'rgb(75, 192, 192)', 'rgb(54, 162, 235)', 'rgb(153, 102, 255)', 'rgb(2, 254, 21)', 'rgb(6, 252, 245)', 'rgb(240, 20, 10)', 'rgb(254, 246, 2)'];

var grafica = document.getElementById('GraficaFac').getContext('2d');
var mygrafica = null;

function CarCombFact() {

    
    $("#ReAsesor").html(CargarCombo(13, 1));
    $("#ReDepartamento").html(CargarCombo(11, 1));
    $("#ReCliente").html(CargarCombo(9, 1));
    $("#ReCiudad").html(CargarCombo(12, 1));
    $("#ReUsuario").html(CargarCombo(13, 1));

}

CarCombFact();

var nfilas = 0;
var OpcionReporte = 1;

function EnviarCorreo(id, cliente, correo) {
    swal({
        title: 'Advertencia',
        text: 'Envío de solicitud del certificado de retención en la FUENTE, ICA, E IVA',
        input: 'text',
        inputValue: correo,
        showLoaderOnConfirm: true,
        showCancelButton: true,
        confirmButtonText: ValidarTraduccion('Si'),
        cancelButtonText: ValidarTraduccion('No'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',

        inputValidator: function (value) {
            return new Promise(function (resolve, reject) {
                if (value) {
                    if (ValidarCorreo(value) == 2)
                        reject('Debe de ingresar una correo válido');
                    else {
                        var datos = LlamarAjax("Facturacion","opcion=EnvioSoliCert&id=" + id + "&cliente=" + cliente + "&correocli=" + value);
                        datos = datos.split("|");
                        if (datos[0] == "0") {
                            correo = value;
                            resolve()
                        } else {
                            reject(datos[1]);
                        }
                    }
                } else {
                    reject(ValidarTraduccion('Debe de ingresar un correo electrónico'));
                }
            })
        }
    }).then(function (result) {
        swal({
            type: 'success',
            html: 'Correo enviado a ' + correo
        })
    })
}

function Reporte(opcion) {

    $("#TSubTotal").val("0");
    $("#TDescuento").val("0");
    $("#TIva").val("0");
    $("#TTotal").val("0");
    $("#TBono").val("0");

    $("#FacturaResumida").addClass("hidden");
    $("#FacturaDetallada").removeClass("hidden");

    var usuario = $("#ReUsuario").val() * 1;
    var ciudad = $("#ReCiudad").val() * 1;
    var departamento = $("#ReDepartamento").val()*1;
    var asesor = $("#ReAsesor").val() * 1;
    
    var factura = $("#ReFactura").val() * 1;
    var cotizacion = $("#ReFactura").val() * 1;
    var ingreso = $("#ReFactura").val() * 1;
    var orden = $.trim($("#ReFactura").val());

    var cliente = $("#ReCliente").val() * 1;
    var tipo = $("#ReTipo").val();
    var estado = $("#ReEstado").val();
    var fechad = $("#ReFechaDesde").val();
    var fechah = $("#ReFechaHasta").val();
    var dias = $("#ReUltimoDia").val() * 1;

    if (fechad == "" && fechah == "" && dias == 0) {
        swal("", ValidarTraduccion("debe de ingresar una fecha o números de días"), "error");
        return false;
    }

    ActivarLoad();
    setTimeout(function () {
        var parametros = "opciones=" + opcion + "&factura=" + factura + "&orden=" + orden + "&ingreso=" + ingreso + "&cotizacion=" + cotizacion +
            "&cliente=" + cliente + "&estado=" + estado + "&tipo=" + tipo + "&fechad=" + fechad + "&fechah=" + fechah + "&dias=" + dias +
            "&asesor=" + asesor + "&departamento=" + departamento + "&ciudad=" + ciudad + "&usuario=" + usuario;
        var datos = LlamarAjax("Facturacion","opcion=ReporteGenFactura&" + parametros);
        DesactivarLoad();

        datos = datos.split("||");

        if (datos[0] == "0") {
            if (opcion == 1) {
                var datajson = JSON.parse(datos[1]);
                var table = $('#tablamodalrepfac').DataTable({

                    "footerCallback": function (row, data, start, end, display) {
                        var api = this.api(), data;

                        // Remove the formatting to get integer data for summation
                        var intVal = function (i) {
                            return typeof i === 'string' ?
                                i.replace(/[\$,]/g, '') * 1 :
                                typeof i === 'number' ?
                                    i : 0;
                        };

                        // Total over all pages
                        total = api
                            .column(11)
                            .data()
                            .reduce(function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);
                        var subtotal = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
                        // Total over this page
                        for (var x = 8; x <= 17; x++) {

                            subtotal[x] = api
                                .column(x, { page: 'current' })
                                .data()
                                .reduce(function (a, b) {
                                    return intVal(a) + intVal(b);
                                }, 0);

                            // Update footer
                            $(api.column(x).footer()).html(formato_numero(subtotal[x], 0,_CD,_CM,""));
                        }
                        $(api.column(18).footer()).html(formato_numero(total,0,".",",",""));
                    },
                    info: true,
                    bFilter: true,
                    data: datajson,
                    bProcessing: true,
                    bDestroy: true,
                    columns: [
                        { "data": "imprimir" },
                        { "data": "factura", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                        { "data": "orden" },
                        { "data": "cliente" },
                        { "data": "tipo" },
                        { "data": "fecha" },
                        { "data": "vencimiento" },
                        { "data": "fecharad" },
                        { "data": "estado" },
                        { "data": "subtotal", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                        { "data": "descuento", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                        { "data": "iva", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                        { "data": "total", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                        { "data": "abonado", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                        { "data": "saldo", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                        { "data": "retefuente", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                        { "data": "reteiva", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                        { "data": "reteica", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                        { "data": "totalretecion", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                        { "data": "diaspago" },
                        { "data": "tiempo" },
                        { "data": "observacion" },
                        { "data": "departamento" },
                        { "data": "ciudad" },
                        { "data": "telefono" },
                        { "data": "email" },
                        { "data": "usuario" },
                        { "data": "registrorad" },
                        { "data": "recibidorad" },
                        { "data": "usuarioanula" },
                        { "data": "fechaanula" },
                        { "data": "obseranula" },
                        { "data": "usuarioenvio" },
                        { "data": "fechaenvio" },
                        { "data": "correoenvia" }
                    ],
                                        
                    "language": {
                        "url": LenguajeDataTable
                    },
                    dom: 'Bfrtip',
                    buttons: [
                        'excel', 'csv', 'copy','colvis'
                    ]
                });

                var subtotal = 0;
                var descuento = 0;
                var iva = 0;
                var total = 0;
                var bono = 0;
                var anulado = 0;
                var fecha = "";
                var retefuente = 0;
                var reteiva = 0;
                var reteica = 0;
                
                anulado = datos[2] * 1;

                table.rows().data().each(function (datos, index) {
                    if (datos.estado != "Anulado") {
                        subtotal += datos.subtotal * 1;
                        descuento += datos.descuento * 1;
                        iva += datos.iva * 1;
                        retefuente += datos.retefuente * 1;
                        reteiva += datos.reteiva * 1;
                        reteica += datos.reteica * 1;
                    } else {
                        fecha = "";
                        if ($.trim(datos.fechaanula) != "") {
                            fecha = $.trim(datos.fechaanula).split(" ");
                            fecha = CambiarCaracter(fecha[0], '/', '-');
                        }
                        if (fecha > fechah) {
                            subtotal += datos.subtotal * 1;
                            descuento += datos.descuento * 1;
                            iva += datos.iva * 1;
                            retefuente += datos.retefuente*1;
                            reteiva += datos.reteiva*1;
                            reteica += datos.reteica * 1;
                        } 
                    }
                });
                                
                total = subtotal - descuento - anulado + iva;

                descuentoanulada = subtotal - anulado;

                $("#TSubTotal").val(formato_numero(subtotal, 0, _CD, _CM, ""));
                $("#TDescuento").val(formato_numero(descuento, 0, _CD, _CM, ""));
                $("#TIva").val(formato_numero(iva, 0, _CD, _CM, ""));
                $("#TTotal").val(formato_numero(total, 0, _CD, _CM, ""));
                $("#TBono").val(formato_numero(bono, 0, _CD, _CM, ""));
                $("#TAnuladas").val(formato_numero(anulado, 0, _CD, _CM, ""));
                                
                $("#TSubTotalAnuladas").val(formato_numero(descuentoanulada, 0, _CD, _CM, ""));
                $("#TReteFuente").val(formato_numero(retefuente, 0, _CD, _CM, ""));
                $("#TReteIva").val(formato_numero(reteiva, 0, _CD, _CM, ""));
                $("#TReteIca").val(formato_numero(reteica, 0, _CD, _CM, ""));
                $("#TTotalRete").val(formato_numero((total - retefuente - reteiva - reteica), 0, _CD, _CM, ""));



                parametros = "factura=" + factura + "&orden=" + orden + "&cliente=" + cliente + "&estado=" + estado + "&tipo=" + tipo + "&fechad=" + fechad + "&fechah=" + fechah + "&dias=" + dias +
                    "&asesor=" + asesor + "&departamento=" + departamento + "&ciudad=" + ciudad + "&usuario=" + usuario;
                datos = LlamarAjax("Facturacion","opcion=ResumenGraficaFactura&" + parametros);

                var etiqueta = ['Subtotal', 'Descuento','IVA','Saldo','Factura-Descuento-Anul-Mes-Ant','Anulado'];

                var config = {
                    type: 'bar',
                    data: {
                        datasets: [],
                        labels: []
                    },
                    options: {
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true
                                }
                            }]
                        },
                        responsive: true,
                        tooltips: {
                            enabled: true
                        },
                        legend: {
                            position: 'top',
                            display: (movil == 1 ? false : true)
                        }
                    }
                }

                if (datos != "[]") {
                    var data = JSON.parse(datos);

                    var newDataset1 = {
                        label: etiqueta[0],
                        backgroundColor: colores[0],
                        data: [],
                    };

                    var newDataset2 = {
                        label: etiqueta[1],
                        backgroundColor: colores[1],
                        data: [],
                    };

                    var newDataset3 = {
                        label: etiqueta[2],
                        backgroundColor: colores[2],
                        data: [],
                    };

                    var newDataset4 = {
                        label: etiqueta[3],
                        backgroundColor: colores[3],
                        data: [],
                    };

                    var newDataset6 = {
                        label: etiqueta[5],
                        backgroundColor: colores[5],
                        data: [],
                    };

                    var newDataset5 = {
                        type: 'line',
                        label: etiqueta[4],
                        fill: false,
                        lineTension: 0,
                        backgroundColor: colores[4],
                        borderColor: colores[4],
                        data: [],
                    };

                    tabla = "";
                    tsubtotal = 0;
                    tdescuento = 0;
                    tsaldo = 0;
                    tiva = 0;
                    tanulado = anulado;
                    tivaanulado = 0;

                    tabla += "<tr>" +
                        "<td>Meses-Anteriores</td>" +
                        "<td align='right'>0</td>" +
                        "<td align='right'>0</td>" +
                        "<td align='right'>0</td>" +
                        "<td align='right'>" + formato_numero(anulado, 0, _CD, _CM, "") + "</td>" +
                        "<td align='right'>0</td>" +
                        "<td align='right'>0</td>" +
                        "<td align='right'>0</td>" +
                        "<td align='right'>0</td></tr>";

                    for (var x = 0; x < data.length; x++) {
                        tsubtotal += data[x].subtotal;
                        tdescuento += data[x].descuento;
                        tiva += data[x].iva;
                        tivaanulado += data[x].ivaanulado;
                        tsaldo += data[x].saldo;
                        tanulado += data[x].anulado;
                        tabla += "<tr>" +
                            "<td>" + data[x].fecha + "</td>" +
                            "<td align='right'>" + formato_numero(data[x].subtotal, 0,_CD,_CM,"") + "</td>" +
                            "<td align='right'>" + formato_numero(data[x].descuento, 0,_CD,_CM,"") + "</td>" +
                            "<td align='right'>" + formato_numero(data[x].iva, 0,_CD,_CM,"") + "</td>" +
                            "<td align='right'>" + formato_numero(data[x].anulado, 0, _CD, _CM, "") + "</td>" +
                            "<td align='right'>" + formato_numero(data[x].ivaanulado, 0, _CD, _CM, "") + "</td>" +
                            "<td align='right'>" + formato_numero(data[x].subtotal - data[x].descuento + data[x].iva - data[x].anulado, 0,_CD,_CM,"") + "</td>" +
                            "<td align='right'>" + formato_numero(data[x].saldo, 0,_CD,_CM,"") + "</td>" +
                            "<td align='right'>" + formato_numero(data[x].subtotal - data[x].descuento + data[x].iva - data[x].anulado - data[x].saldo, 0,_CD,_CM,"") + "</td></tr>";
                        config.data.labels.push(data[x].fecha);

                        newDataset1.data.push(data[x].subtotal);
                        newDataset2.data.push(data[x].descuento);
                        newDataset3.data.push(data[x].iva);
                        newDataset4.data.push(data[x].saldo);
                        newDataset6.data.push(data[x].anulado);
                        newDataset5.data.push(data[x].subtotal - data[x].descuento - data[x].anulado - data.ivaanulado);

                    }

                    tabla += "<tr>" +
                        "<td align='right' class='text-info text-XX'>TOTALES</td>" +
                        "<td align='right' class='text-info text-XX'>" + formato_numero(tsubtotal, 0,_CD,_CM,"") + "</td>" +
                        "<td align='right' class='text-info text-XX'>" + formato_numero(tdescuento, 0,_CD,_CM,"") + "</td>" +
                        "<td align='right' class='text-info text-XX'>" + formato_numero(tiva, 0,_CD,_CM,"") + "</td>" +
                        "<td align='right' class='text-info text-XX'>" + formato_numero(tanulado, 0, _CD, _CM, "") + "</td>" +
                        "<td align='right' class='text-info text-XX'>" + formato_numero(tivaanulado, 0, _CD, _CM, "") + "</td>" +
                        "<td align='right' class='text-info text-XX'>" + formato_numero(tsubtotal - tdescuento - tanulado - tivaanulado + tiva, 0, _CD, _CM, "") + "</td>" +
                        "<td align='right' class='text-info text-XX'>" + formato_numero(tsaldo, 0, _CD, _CM, "") + "</td>" +
                        "<td align='right' class='text-info text-XX'>" + formato_numero(tsubtotal - tdescuento + tiva - tsaldo - tanulado - tivaanulado, 0, _CD, _CM, "") + "</td></tr>";
                    $("#TablaFac").html(tabla);

                    config.data.datasets.push(newDataset5);
                    config.data.datasets.push(newDataset1);
                    config.data.datasets.push(newDataset2);
                    config.data.datasets.push(newDataset3);
                    config.data.datasets.push(newDataset4);
                    config.data.datasets.push(newDataset6);
                    
                    if (mygrafica != null) {
                        mygrafica.destroy();
                    }

                    mygrafica = new Chart(grafica, config);
                    mygrafica.update();
                }
            } else {
                window.open(url_cliente + "DocumPDF/" + datos[1]);
            }
        } else {
            swal("Acción Cancelada", datos[1], "warning");
        }
    }, 15);
}


function ImprimirFactura(factura) {
    
    ActivarLoad();
    setTimeout(function () {
        var datos = LlamarAjax("Configuracion", "opcion=GenerarPDF&tipo=Factura&documento=" + factura).split("||");
        DesactivarLoad();
        if (datos[0] == "0") {
            window.open(url_cliente + "DocumPDF/" + datos[1]);
        } else {
            swal("Acción Cancelada", ValidarTraduccion(datos[1]), "warning");
        }
    }, 15);
}


function Resumen(opcion) {

    $("#TSubTotal").val("0");
    $("#TDescuento").val("0");
    $("#TIva").val("0");
    $("#TTotal").val("0");
    $("#TBono").val("0");

    $("#FacturaResumida").removeClass("hidden");
    $("#FacturaDetallada").addClass("hidden");

    var usuario = $("#ReUsuario").val() * 1;
    var ciudad = $("#ReCiudad").val() * 1;
    var departamento = $("#ReDepartamento").val() * 1;
    var asesor = $("#ReAsesor").val() * 1;

    var factura = $("#ReFactura").val() * 1;
    var cotizacion = $("#ReFactura").val() * 1;
    var ingreso = $("#ReFactura").val() * 1;
    var orden = $.trim($("#ReFactura").val());

    var cliente = $("#ReCliente").val() * 1;
    var tipo = $("#ReTipo").val();
    var estado = $("#ReEstado").val();
    var fechad = $("#ReFechaDesde").val();
    var fechah = $("#ReFechaHasta").val();
    var dias = $("#ReUltimoDia").val() * 1;

    if (fechad == "" && fechah == "" && dias == 0) {
        swal("", ValidarTraduccion("debe de ingresar una fecha o números de días"), "error");
        return false;
    }

    ActivarLoad();
    setTimeout(function () {
        var parametros = "opciones=" + opcion + "&factura=" + factura + "&orden=" + orden + "&ingreso=" + ingreso + "&cotizacion=" + cotizacion +
            "&cliente=" + cliente + "&estado=" + estado + "&tipo=" + tipo + "&fechad=" + fechad + "&fechah=" + fechah + "&dias=" + dias +
            "&asesor=" + asesor + "&departamento=" + departamento + "&ciudad=" + ciudad + "&usuario=" + usuario;
        var datos = LlamarAjax("Facturacion","opcion=ReporteResuFactura&" + parametros);
        DesactivarLoad();

        datos = datos.split("|");

        if (datos[0] == "0") {
            if (opcion == 1) {
                var datajson = JSON.parse(datos[1]);
                var table = $('#tablamodalfacresumen').DataTable({
                    data: datajson,
                    bProcessing: true,
                    bDestroy: true,
                    columns: [
                        { "data": "enviar" },
                        { "data": "cliente" },
                        { "data": "tipo" },
                        { "data": "subtotal", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                        { "data": "descuento", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                        { "data": "iva", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                        { "data": "total", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                        { "data": "abonado", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                        { "data": "saldo", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                        { "data": "retefuente", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                        { "data": "reteiva", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                        { "data": "reteica", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                        { "data": "totalretecion", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                        { "data": "departamento" },
                        { "data": "ciudad" },
                        { "data": "telefono" },
                        { "data": "email" }
                    ],

                    "columnDefs": [
                        { "type": "numeric-comma", targets: 3 }
                    ],

                    "language": {
                        "url": LenguajeDataTable
                    },
                    dom: 'Bfrtip',
                    buttons: [
                        'excel', 'csv', 'copy','colvis'
                    ]
                });

                var subtotal = 0;
                var descuento = 0;
                var iva = 0;
                var total = 0;
                var bono = 0;
                var anulado = 0;
                var fecha = "";
                var retefuente = 0;
                var reteiva = 0;
                var reteica = 0;

                anulado = datos[2] * 1;

                table.rows().data().each(function (datos, index) {
                    if (datos.estado != "Anulado") {
                        subtotal += datos.subtotal * 1;
                        descuento += datos.descuento * 1;
                        iva += datos.iva * 1;
                        retefuente += datos.retefuente * 1;
                        reteiva += datos.reteiva * 1;
                        reteica += datos.reteica * 1;
                    } else {
                        fecha = "";
                        if ($.trim(datos.fechaanula) != "") {
                            fecha = $.trim(datos.fechaanula).split(" ");
                            fecha = CambiarCaracter(fecha[0], '/', '-');
                        }
                        if (fecha > fechah) {
                            subtotal += datos.subtotal * 1;
                            descuento += datos.descuento * 1;
                            iva += datos.iva * 1;
                            retefuente += datos.retefuente * 1;
                            reteiva += datos.reteiva * 1;
                            reteica += datos.reteica * 1;
                        }
                    }
                });

                total = subtotal - descuento - anulado + iva;

                descuentoanulada = subtotal - anulado;

                $("#TSubTotal").val(formato_numero(subtotal, 0, _CD, _CM, ""));
                $("#TDescuento").val(formato_numero(descuento, 0, _CD, _CM, ""));
                $("#TIva").val(formato_numero(iva, 0, _CD, _CM, ""));
                $("#TTotal").val(formato_numero(total, 0, _CD, _CM, ""));
                $("#TBono").val(formato_numero(bono, 0, _CD, _CM, ""));
                $("#TAnuladas").val(formato_numero(anulado, 0, _CD, _CM, ""));

                $("#TSubTotalAnuladas").val(formato_numero(descuentoanulada, 0, _CD, _CM, ""));
                $("#TReteFuente").val(formato_numero(retefuente, 0, _CD, _CM, ""));
                $("#TReteIva").val(formato_numero(reteiva, 0, _CD, _CM, ""));
                $("#TReteIca").val(formato_numero(reteica, 0, _CD, _CM, ""));
                $("#TTotalRete").val(formato_numero((total - retefuente - reteiva - reteica), 0, _CD, _CM, ""));

            } else {
                window.open(url + "DocumPDF/" + datos[1]);
            }
        } else {
            swal(datos[1], "", "warning");
        }

    }, 15);


}

tb = $('input');
$(tb).keypress(enter2tab);

tb = $('select');
$(tb).keypress(enter2tab);

function enter2tab(e) {
    id = $(this).attr("id");
    if ((e.keyCode == 13) && ((id != "Codigo") || (id != "CodigoB"))) {
        cb = parseInt($(this).attr('tabindex'));
        if ($(':input[tabindex=\'' + (cb + 1) + '\']') != null) {
            $(':input[tabindex=\'' + (cb + 1) + '\']').focus();
            $(':input[tabindex=\'' + (cb + 1) + '\']').select();
            e.preventDefault();
            return false;
        }
    }
}

function FacturasAnuladas() {
    var cliente = $("#ReCliente").val() * 1;
    var fechad = $("#ReFechaDesde").val();
    var fechah = $("#ReFechaHasta").val();
    var dias = $("#ReUltimoDia").val() * 1;

    ActivarLoad();
    setTimeout(function () {
        var parametros = "cliente=" + cliente + "&dias=" + dias + "&fechad=" + fechad + "&fechah=" + fechah;
        var datos = LlamarAjax("Facturacion","opcion=FacturasAnuladas&" + parametros);
        DesactivarLoad();

        var datajson = JSON.parse(datos);
        $('#TablaFacturasAnula').DataTable({
            data: datajson,
            bProcessing: true,
            bDestroy: true,
            columns: [
                { "data": "factura", "className": "text-XX" },
                { "data": "orden" },
                { "data": "cliente" },
                { "data": "fecha" },
                { "data": "subtotal", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "descuento", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "anulado", "className": "text-right text-XX12 text-danger", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "iva", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "retefuente", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "reteiva", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "reteica", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "total", "className": "text-right", render: $.fn.dataTable.render.number(',', '.', 0, '') },
                { "data": "obseranula" },
                { "data": "fechaanula" },
                { "data": "usuario" }
            ],


            "language": {
                "url": LenguajeDataTable
            },
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv', 'copy', 'colvis'
            ],
            "lengthMenu": [[7], [7]],
        });
        $("#ModalFacturasAnuladas").modal("show");
    }, 15);

}

$('select').select2();
DesactivarLoad();
