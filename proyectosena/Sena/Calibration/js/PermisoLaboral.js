﻿var IdEmpleado = localStorage.getItem('EmpleadoSeleccionado') * 1;

if (IdEmpleado > 0) {
    $(".eliminapermiso").remove();
    BuscarEmpleado(IdEmpleado);
}

function LimpiarDatos() {

}

function BuscarEmpleado(empleado) {

    var datos = LlamarAjax("Recursohumano/BuscarEmpleados", "Empleado=" + empleado).split("|");
    var data = JSON.parse(datos[0]);
    $("#IdEmpleado").val(empleado);
    $("#Documento").val(data[0].documento);
    $("#Nombre").val(data[0].nombrecompleto);
    $("#Cargo").val(data[0].cargo);
    $("#Telefono").val(data[0].telefono);
    $("#Celular").val(data[0].celular);
    $("#Correo").val(data[0].email);
    $("#Direccion").val(data[0].direccion);
}

$("#formPermisoLaboral").submit(function (e) {
    e.preventDefault();
    var parametros = $(this).serialize();
    var datos = LlamarAjax("Recursohumano/GuardarPermisolaboral", parametros).split("|");
    if (datos[0] == "0")
        swal("", datos[1], "success");
    else
        swal("Acción Cancelada", datos[1], "warning");
})


$('select').select2();
DesactivarLoad();
