﻿var Ingreso = 0;
var IdRemision = 0;
var Fotos = 0;
var ErrorIngreso = 0;


function LimpiarTodo() {
    LimpiarDatos();
    $("#Ingreso").val("");
}

function CambiarFoto2(numero) {
    $("#FotoIngreso").attr("src", url_cliente +  "Adjunto/imagenes/ingresos/" + Ingreso + "/" + numero + ".jpg?id=" + NumeroAleatorio());
}

function CargarFotos() {
    var contenedor = "";
    var resultado = '<div class="row">';
    var fotos = LlamarAjax("Cotizacion","opcion=CantidadFoto&ingreso=" + Ingreso) * 1;
    if (fotos > 0){
		contenedor = "<div class='row'><div class='col-xs-12 col-sm-11'>" +
			"<img id='FotoIngreso' " + (fotos > 0 ? "src='" + url_cliente + "Adjunto/imagenes/ingresos/" + Ingreso + "/1.jpg?id=" + NumeroAleatorio() + '"' : "") + " width='60%'></div><div class='col-xs-12 col-sm-1'>";
		for (var x = 1; x <= fotos; x++) {
			contenedor += "<button class='btn btn-primary' type='button' onclick='CambiarFoto2(" + x + ")'>" + x + "</button>&nbsp;";
			resultado += '<div class="col-lg-3 col-md-4 col-sm-4"><div class="block">' +
				'<div class="thumbnail">' +
				'   <a href="' + url_cliente + 'Adjunto/imagenes/ingresos/' + Ingreso + '/' + x + '_copia.jpg?id=' + NumeroAleatorio() + 'class="thumb-zoom lightbox" title="Ingreso número ' + Ingreso + "/" + x + '">' +
				'       <img src="' + url_cliente + 'Adjunto/imagenes/ingresos/' + Ingreso + '/' + x + '.jpg?id=' + NumeroAleatorio()  + 'alt="">' +
				'						</a>' +
				'        <div class="caption text-center">' +
				'            <h6>Foto Número <small>' +  x + '</small></h6>' +
				'            <div class="icons-group">' +
				"                <a href=\"javascript:EliminarFotoIndividual(" + Ingreso + "," + x + ")\" title='Eliminar' class='tip'><span data-icon='&#xe0d8;'></span></a>" +
				'            </div>' +
				'        </div>' +
				'</div> ' +
				'</div></div>';
		}
	}
    contenedor += "</div></div>";
    resultado += "</div>";
    $("#AlbunFotosIng").html(contenedor);
	$("#Albun_Detallado").html(resultado);
}
function EliminarFotoIndividual(ingreso, numero){
	if (Ingreso > 0){
		ActivarLoad();
		setTimeout(function () {
			datos = LlamarAjax("Cotizacion", "opcion=EliminarFotoIndividual&ingreso=" + Ingreso + "&numero=" + numero).split("|");
			if (datos[0] == "0") {
				CargarFotos();
				DesactivarLoad();
			}else{
				DesactivarLoad();
				swal("Acción Cancelada",datos[1],"success");
			}
		},15);
	}
}
function LimpiarDatos() {
    IdRemision = 0;
    Ingreso = 0;
    Fotos = 0;
    $("#Equipo").val("");
    $("#Marca").val("");
    $("#Modelo").val("");
    $("#Serie").val("");
    $("#Observacion").val("");
    $("#AlbunFotosIng").html("");
    $("#Accesorio").val("");
    $("#Albun_Detallado").html("");
}

function BuscarIngreso(numero, code, tipo) {
    if ((Ingreso != (numero*1)) && ($("#Equipo").val() != ""))
        LimpiarDatos();
    if ((code.keyCode == 13 || tipo == 1) && ErrorIngreso == 0) {
		ActivarLoad();
		setTimeout(function () {
			if (numero * 1 > 0) {
				var datos = LlamarAjax("Cotizacion","opcion=BuscarIngreso&ingreso=" + numero + "&serie=");
				if (datos != "[]") {
					var data = JSON.parse(datos);
					IdRemision = data[0].id * 1;
					Ingreso = data[0].ingreso * 1;
					$("#Equipo").val(data[0].equipo);
					$("#Marca").val(data[0].marca);
					$("#Modelo").val(data[0].modelo);
					$("#Serie").val(data[0].serie);
					$("#Observacion").val(data[0].observacion);
					$("#Accesorio").val(data[0].accesorio);

					CargarFotos();
					DesactivarLoad();

				} else {
					DesactivarLoad();
					$("#Ingreso").select();
					$("#Ingreso").focus();
					swal("Acción Cancelada", "El Ingreso número " + numero + " no se encuentra registrado", "warning");
					ErrorIngreso = 1;
					return false;
				}
			}
		},15);
    } else
        ErrorIngreso = 0;
}
    
function SubirFoto() {
    if (Ingreso > 0) {
		var resultado = "";
        swal.queue([{
            html: "<h3>" + ValidarTraduccion("Seleccione las fotos que deseas subir del ingreso número " + Ingreso) + "</h3><input type='file' class='form-control' id='documenfotoregistro' multiple onchange=\"GuardarDocumentoAdjunto(this.id,4,'')\" name='documento' accept='image/*' required />",
            showLoaderOnConfirm: true,
            showCancelButton: true,
            confirmButtonText: ValidarTraduccion('Guardar Fotos'),
            cancelButtonText: ValidarTraduccion('Cancelar'),
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            preConfirm: function () {
				return new Promise(function (resolve, reject) {
					if ($("#documenfotoregistro").val() == ""){
						reject("Debe de examinar por lo mínimo un archivo de imagen");
					}else{
						$.post(url_servidor + "Configuracion","opcion=SubirFotoMultiple&tipo_imagen=2&valor=" + Ingreso)
							.done(function (data) {
								datos = data.split("|");
								if (datos[0] == "0") {
									resultado = "<b>Fotos Subidas:</b><br>" + datos[1] + "<br><b>Fotos con error:</b><br>" + datos[2];
									resolve();
								} else {
									reject(datos[1]);
								}
							})
					}
				})
			}
        }]).then(function (data) {
			swal("",resultado,"success");
			CargarFotos();
        });
    }
}

Webcam.attach('#my_cameraing');

function TomarFotoIng() {

    if (Ingreso > 0) {
        Webcam.snap(function (data_uri) {
            //var raw_image_data = data_uri.replace(/^data\:image\/\w+\;base64\,/, '');
            GuardarFotoIngFoto(Ingreso, data_uri);
        });
    }else{
		swal("Acción Cancelada","Debe de seleccionar un ingreso","warning");
	}

}

function GuardarFotoIngFoto(ingreso, archivo) {
	ActivarLoad();
    setTimeout(function () {
		var parametros = "opcion=SubirFoto&archivo=" + archivo + "&ingreso=" + ingreso + "&tipo_imagen=2";
		var data = LlamarAjax("Configuracion",parametros).split("|");
		if (data[0] == "0"){
			CargarFotos();
			CambiarFoto2(data[3], ingreso);
			DesactivarLoad();
		}else{
			DesactivarLoad();
			swal("Acción Cancelada",data[1],"success");
		}
	},15);
}

function EliminarFotoIng() {
	if (Ingreso > 0){
		ActivarLoad();
		setTimeout(function () {
			datos = LlamarAjax("Cotizacion", "opcion=EliminarFoto&ingreso=" + Ingreso).split("|");
			if (datos[1] != "X") {
				CargarFotos();
				CambiarFoto2(datos[1], Ingreso);
				DesactivarLoad();
			}else{
				swal("Acción Cancelada",datos[0],"warning");
				DesactivarLoad();
			}
		},15);
	}
}



$('select').select2();
DesactivarLoad();
